//
//  main.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char* argv[])
{
    // code
    
    // creation of object
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    
    NSString *delegateClassName = NSStringFromClass([AppDelegate class]);
    int result = UIApplicationMain(argc, argv, nil, delegateClassName); // wrap kele 3 lines je macOS madhye main madhye kelya hotya
    
    [pool release];
    
    return(result);
}


