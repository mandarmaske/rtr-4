//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "GLESView.h"
#import "Sphere.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
using namespace vmath;

@implementation GLESView
{
@private
    EAGLContext* eaglContext; // embedded apple graphics library
    
    GLuint defaultFramebuffer;
    GLuint coloRenderbuffer;
    GLuint depthRenderbuffer;
    
    CADisplayLink* displayLink; // CA for Core Animation
    GLint framesPerSecond;
    BOOL isAnimating;
    
    GLuint shaderProgramObject_PV;
    GLuint shaderProgramObject_PF;
    enum
    {
      AMC_ATTRIBUTE_POSITION = 0,
      AMC_ATTRIBUTE_COLOR,
      AMC_ATTRIBUTE_NORMAL,
      AMC_ATTRIBUTE_TEXTURE0
    };

    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_elements;
     
    SphereData sphereData;

    GLuint modelMatrixUniform_pv;
    GLuint viewMatrixUniform_pv;
    GLuint projectionMatrixUniform_pv;

     //Lights

     // per vertex
     GLuint laUniform_pv[3];
     GLuint ldUniform_pv[3];
     GLuint lsUniform_pv[3];
     GLuint lightPositionUniform_pv[3];

     GLuint kaUniform_pv;
     GLuint kdUniform_pv;
     GLuint ksUniform_pv;
     GLuint materialShininessUniform_pv;

     GLuint lightingEnabledUniform_pv;

     // per fragment
     GLuint modelMatrixUniform_pf;
     GLuint viewMatrixUniform_pf;
     GLuint projectionMatrixUniform_pf;

     //Lights
     GLuint laUniform_pf[3];
     GLuint ldUniform_pf[3];
     GLuint lsUniform_pf[3];
     GLuint lightPositionUniform_pf[3];

     GLuint kaUniform_pf;
     GLuint kdUniform_pf;
     GLuint ksUniform_pf;
     GLuint materialShininessUniform_pf;

     GLuint lightingEnabledUniform_pf;

     BOOL bLight;
     GLfloat gbLightAngleZero;
     

     struct Light
     {
         vec4 lightAmbient;
         vec4 lightDiffuse;
         vec4 lightSpecular;
         vec4 lightPosition;
     };
     Light lights[3];

     GLfloat materialAmbient[4];
     GLfloat materialDiffuse[4];
     GLfloat materialSpecular[4];
     GLfloat materialShininess;

     char choosenKey;
    
    int counter;

     mat4 perspectiveProjectionMatrix;
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // 1) Create drawable surface which is called as layer
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        // 2) Set EAGLLayer properties (this is similar to setting pixelformatdistribute properties)
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        // 3) Create eagl context
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // 4) Set this context as current context
        [EAGLContext setCurrentContext : eaglContext];
        
        // 5) Print OpenGLES info
        printf("OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
        printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
        printf("OpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // 6) Create frame buffer
        glGenFramebuffers(1, &defaultFramebuffer);
        
        // 7) Bind With Above frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // 8) Create color render buffer
        glGenRenderbuffers(1, &coloRenderbuffer);
        
        // 9) Bind with above color render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
        
        // 10) Provide storage to render buffer
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        // 11) Set frame buffer according render buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, coloRenderbuffer);
        
        // 12) Declare backing width
        GLint backingWidth;
        
        // 13) Declare backing height
        GLint backingHeight;
        
        // 14) Get Color render buffer width into backing width
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        // 15) Get Color render buffer height into backing height
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        // 16) Create depth buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        
        // 17) bind above depth render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        // 18) Provide storage to depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        // 19) Get
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // 20) check the status of frame buffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // call initialize
        int result = [self initialize];
        
        if(result!=0)
        {
            printf("Result Not Obtained \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        
        framesPerSecond = 60; // this is started from iOS 8.0
        isAnimating = NO;
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

+ (Class)layerClass
{
    // code
    
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);

    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];
    
    // 12) Declare backing width
    GLint backingWidth;
    
    // 13) Declare backing height
    GLint backingHeight;
    
    // 14) Get Color render buffer width into backing width
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    // 15) Get Color render buffer height into backing height
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    // 16) Create depth buffer
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    // 17) bind above depth render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    // 18) Provide storage to depth render buffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    // 19) Get
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    // call our resize here
    [self resize : backingWidth : backingHeight];
    
    // 20) check the status of frame buffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }
    
    // call drawView after resize
    [self drawView : nil]; // or [self drawView : self];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    // call our display and update here
    [self display];
    [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        // create display link
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        // set frames per second for the display link
        [displayLink setPreferredFramesPerSecond : framesPerSecond];
        
        // add this display link to run loop
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        // remove display link from run loop
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    // code
    // vertex shader
    const GLchar* vertexShaderSourceCode_pv =
            "#version 300 core" \
            "\n" \
                "precision highp float;" \
                "in vec4 a_position;" \
                "in vec3 a_normal;" \
                "uniform mat4 u_modelMatrix;" \
                "uniform mat4 u_viewMatrix;" \
                "uniform mat4 u_projectionMatrix;" \
                "uniform vec3 u_la[3];" \
                "uniform vec3 u_ld[3];" \
                "uniform vec3 u_ls[3];" \
                "uniform vec4 u_lightPosition[3];" \
                "uniform vec3 u_ka;" \
                "uniform vec3 u_kd;" \
                "uniform vec3 u_ks;" \
                "uniform float u_materialShininess;" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec3 phong_ads_light;" \
                "void main(void)" \
                "{" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec4 eyeCoordinates = u_viewMatrix*u_modelMatrix*a_position;" \
                "mat3 normalMatrix=mat3(u_viewMatrix*u_modelMatrix);" \
                "vec3 transformedNormals=normalize(normalMatrix*a_normal);" \
                "vec3 viewerVector=normalize(-eyeCoordinates.xyz);" \
                "vec3 ambient[3];" \
                "vec3 lightDirection[3];" \
                "vec3 diffuse[3];" \
                "vec3 reflectionVector[3];" \
                "vec3 specular[3];" \
                "vec3 total[3];" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "ambient[i] = u_la[i] * u_ka; " \
                "lightDirection[i]=normalize(vec3(u_lightPosition[i])-eyeCoordinates.xyz);" \
                "diffuse[i]=u_ld[i]*u_kd*max(dot(lightDirection[i],transformedNormals),0.0);" \
                "reflectionVector[i]=reflect(-lightDirection[i],transformedNormals);" \
                "specular[i]=u_ls[i]*u_ks*pow(max(dot(reflectionVector[i],viewerVector),0.0),u_materialShininess);" \
                "total[i] = ambient[i] + diffuse[i] + specular[i];" \
                "}" \
                "phong_ads_light = total[0] + total[1]+total[2];" \
                "}" \
                "else" \
                "{"
                "phong_ads_light=vec3(1.0,1.0,1.0);" \
                "}" \
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
                "}";

    
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        GLuint vertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);

        glShaderSource(vertexShaderObject_pv, 1, (const GLchar**)&vertexShaderSourceCode_pv, NULL);

        glCompileShader(vertexShaderObject_pv);

        glGetShaderiv(vertexShaderObject_pv, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_pv, infoLogLength, &written, log);
 
                    printf("Vertex Shader Compilation log : %s\n", log);
                    
                    free(log);
                     
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Fragment Shader
       
        const GLchar* fragmentShaderSourceCode_pv =
            "#version 300 core" \
            "\n" \
            "precision highp float;" \
            "in vec3 phong_ads_light;" \
                "out vec4 FragColor;" \
                "void main(void)" \
                "{" \
                "FragColor = vec4(phong_ads_light,1.0);" \
                "}";

        GLuint fragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(fragmentShaderObject_pv, 1, (const GLchar**)&fragmentShaderSourceCode_pv, NULL);

        glCompileShader(fragmentShaderObject_pv);

        status=0;
        infoLogLength=0;
        log = NULL;
       
        glGetShaderiv(fragmentShaderObject_pv, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_pv, infoLogLength, &written, log);
                    
                    printf("Fragment Shader Compilation log : %s\n", log);
                   
                    free(log);
                   
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }
      
        shaderProgramObject_PV = glCreateProgram();
        
        glAttachShader(shaderProgramObject_PV, vertexShaderObject_pv);
        glAttachShader(shaderProgramObject_PV, fragmentShaderObject_pv);

        glBindAttribLocation(shaderProgramObject_PV, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject_PV, AMC_ATTRIBUTE_NORMAL, "a_normal");

        glLinkProgram(shaderProgramObject_PV);

        status = 0;
        infoLogLength = 0;
        log = NULL;

        glGetProgramiv(shaderProgramObject_PV, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_PV, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_PV, infoLogLength, &written, log);
                    
                    printf("Shader Program Link log : %s\n", log);
                   
                    free(log);
                    
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }
        
           modelMatrixUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_modelMatrix");
           viewMatrixUniform_pv= glGetUniformLocation(shaderProgramObject_PV, "u_viewMatrix");
           projectionMatrixUniform_pv= glGetUniformLocation(shaderProgramObject_PV, "u_projectionMatrix");
           laUniform_pv[0]= glGetUniformLocation(shaderProgramObject_PV, "u_la[0]");
           ldUniform_pv[0]= glGetUniformLocation(shaderProgramObject_PV, "u_ld[0]");
           lsUniform_pv[0]= glGetUniformLocation(shaderProgramObject_PV, "u_ls[0]");
           lightPositionUniform_pv[0]= glGetUniformLocation(shaderProgramObject_PV, "u_lightPosition[0]");

           laUniform_pv[1] = glGetUniformLocation(shaderProgramObject_PV, "u_la[1]");
           ldUniform_pv[1] = glGetUniformLocation(shaderProgramObject_PV, "u_ld[1]");
           lsUniform_pv[1] = glGetUniformLocation(shaderProgramObject_PV, "u_ls[1]");
           lightPositionUniform_pv[1] = glGetUniformLocation(shaderProgramObject_PV, "u_lightPosition[1]");

           laUniform_pv[2] = glGetUniformLocation(shaderProgramObject_PV, "u_la[2]");
           ldUniform_pv[2] = glGetUniformLocation(shaderProgramObject_PV, "u_ld[2]");
           lsUniform_pv[2] = glGetUniformLocation(shaderProgramObject_PV, "u_ls[2]");
           lightPositionUniform_pv[2] = glGetUniformLocation(shaderProgramObject_PV, "u_lightPosition[2]");

           kaUniform_pv= glGetUniformLocation(shaderProgramObject_PV, "u_ka");
           kdUniform_pv= glGetUniformLocation(shaderProgramObject_PV, "u_kd");
           ksUniform_pv= glGetUniformLocation(shaderProgramObject_PV, "u_ks");
           materialShininessUniform_pv= glGetUniformLocation(shaderProgramObject_PV, "u_materialShininess");

           lightingEnabledUniform_pv= glGetUniformLocation(shaderProgramObject_PV, "u_lightingEnabled");

        // Per Fragment
        // Vertex Shader
       
        const GLchar* vertexShaderSourceCode_pf =
            "#version 300 core" \
            "\n" \
                "precision highp float;" \
                "in vec4 a_position;" \
                "in vec3 a_normal;" \
                "uniform mat4 u_modelMatrix;" \
                "uniform mat4 u_viewMatrix;" \
                "uniform mat4 u_projectionMatrix;" \
                "uniform vec4 u_lightPosition[3];" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec3 transformedNormals;" \
                "out vec3 lightDirection[3];" \
                "out vec3 viewerVector;" \
                "void main(void)" \
                "{" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec4 eyeCoordinates = u_viewMatrix*u_modelMatrix*a_position;" \
                "mat3 normalMatrix=mat3(u_viewMatrix*u_modelMatrix);" \
                "transformedNormals=normalize(normalMatrix*a_normal);" \
                "viewerVector=normalize(-eyeCoordinates.xyz);" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "lightDirection[i]=normalize(vec3(u_lightPosition[i])-eyeCoordinates.xyz);" \
                "}" \
                "}" \
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
                "}";

        GLuint vertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

        glShaderSource(vertexShaderObject_pf, 1, (const GLchar**)&vertexShaderSourceCode_pf, NULL);

        glCompileShader(vertexShaderObject_pf);

        glGetShaderiv(vertexShaderObject_pf, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_pf, infoLogLength, &written, log);
                    
                    printf("Vertex Shader Compilation log : %s\n", log);
                    
                    free(log);
                    
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Fragment Shader
        
        const GLchar* fragmentShaderSourceCode_pf =
            "#version 300 core" \
            "\n" \
    "precision highp float;" \
    "in vec3 transformedNormals;" \
                "in vec3 lightDirection[3];" \
                "in vec3 viewerVector;" \
                "uniform vec3 u_la[3];" \
                "uniform vec3 u_ld[3];" \
                "uniform vec3 u_ls[3];" \
                "uniform vec3 u_ka;" \
                "uniform vec3 u_kd;" \
                "uniform vec3 u_ks;" \
                "uniform float u_materialShininess;" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec4 FragColor;" \
                "void main(void)" \
                "{" \
                "vec3 phong_ads_light;" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec3 normalised_transformed_normals=normalize(transformedNormals);" \
                "vec3 normalised_viewerVector=normalize(viewerVector);" \
                "vec3 ambient[3];" \
                "vec3 diffuse[3];" \
                "vec3 reflectionVector[3];" \
                "vec3 specular[3];" \
                "vec3 total[3];" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "ambient[i] = u_la[i] * u_ka; " \
                "diffuse[i]=u_ld[i]*u_kd*max(dot(lightDirection[i],normalised_transformed_normals),0.0);" \
                "reflectionVector[i]=reflect(-lightDirection[i],normalised_transformed_normals);" \
                "specular[i]=u_ls[i]*u_ks*pow(max(dot(reflectionVector[i],normalised_viewerVector),0.0),u_materialShininess);" \
                "total[i] = ambient[i] + diffuse[i] + specular[i];" \
                "}" \
                "phong_ads_light = total[0] + total[1]+total[2];" \
                "}" \
                "else" \
                "{"
                "phong_ads_light=vec3(1.0,1.0,1.0);" \
                "}"
                "FragColor = vec4(phong_ads_light,1.0);" \
                "}";

        GLuint fragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(fragmentShaderObject_pf, 1, (const GLchar**)&fragmentShaderSourceCode_pf, NULL);

        glCompileShader(fragmentShaderObject_pf);

        status = 0;
        infoLogLength = 0;
        log = NULL;
        
        glGetShaderiv(fragmentShaderObject_pf, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_pf, infoLogLength, &written, log);

                    printf("Fragment Shader Compilation log : %s\n", log);

                    free(log);
                   
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }
        
        shaderProgramObject_PF = glCreateProgram();

        glAttachShader(shaderProgramObject_PF, vertexShaderObject_pf);
        glAttachShader(shaderProgramObject_PF, fragmentShaderObject_pf);

        glBindAttribLocation(shaderProgramObject_PF, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "a_normal");

        glLinkProgram(shaderProgramObject_PF);

        status = 0;
        infoLogLength = 0;
        log = NULL;

        glGetProgramiv(shaderProgramObject_PF, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_PF, infoLogLength, &written, log);

                    printf("Shader Program Link log : %s\n", log);

                    free(log);

                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

            modelMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_modelMatrix");
            viewMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_viewMatrix");
            projectionMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_projectionMatrix");
            laUniform_pf[0] = glGetUniformLocation(shaderProgramObject_PF, "u_la[0]");
            ldUniform_pf[0] = glGetUniformLocation(shaderProgramObject_PF, "u_ld[0]");
            lsUniform_pf[0] = glGetUniformLocation(shaderProgramObject_PF, "u_ls[0]");//Specular
            lightPositionUniform_pf[0] = glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition[0]");

            laUniform_pf[1] = glGetUniformLocation(shaderProgramObject_PF, "u_la[1]");
            ldUniform_pf[1] = glGetUniformLocation(shaderProgramObject_PF, "u_ld[1]");
            lsUniform_pf[1] = glGetUniformLocation(shaderProgramObject_PF, "u_ls[1]");//Specular
            lightPositionUniform_pf[1] = glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition[1]");

            laUniform_pf[2] = glGetUniformLocation(shaderProgramObject_PF, "u_la[2]");
            ldUniform_pf[2] = glGetUniformLocation(shaderProgramObject_PF, "u_ld[2]");
            lsUniform_pf[2] = glGetUniformLocation(shaderProgramObject_PF, "u_ls[2]");//Specular
            lightPositionUniform_pf[2] = glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition[2]");

            kaUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_ka");
            kdUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_kd");
            ksUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_ks");
            materialShininessUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_materialShininess");

            lightingEnabledUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_lightingEnabled");

        // Declaration of Vertex data array
        Sphere *sphere = new Sphere();
        sphere->getSphereVertexData(&sphereData);

        delete sphere;
        sphere = NULL;
    
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);
       
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_sphere_position);//Array hold karnara

        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*1146, sphereData.vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
      
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1, &vbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.normals, GL_STATIC_DRAW);

        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenBuffers(1, &vbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * 2280, sphereData.elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
        glBindVertexArray(0);

        // here starts OpenGL
        // clear the screen
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
       // depth related changes
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix = mat4::identity();
    
    
    lights[0].lightAmbient[0]=0.0f;
            lights[0].lightAmbient[1]=0.0f;
            lights[0].lightAmbient[2]=0.0f;
            lights[0].lightAmbient[3]=1.0f;
            
            lights[0].lightDiffuse[0]=1.0f;
            lights[0].lightDiffuse[1]=0.0f;
            lights[0].lightDiffuse[2]=0.0f;
            lights[0].lightDiffuse[3]=1.0f;
            
            lights[0].lightSpecular[0]=1.0f;
            lights[0].lightSpecular[1]=0.0f;
            lights[0].lightSpecular[2]=0.0f;
            lights[0].lightSpecular[3]=1.0f;
            
            lights[0].lightPosition[0]=0.0f;
            lights[0].lightPosition[1]=0.0f;
            lights[0].lightPosition[2]=0.0f;
            lights[0].lightPosition[3]=1.0f;
            
            //Light 1
            lights[1].lightAmbient[0]=0.0f;
            lights[1].lightAmbient[1]=0.0f;
            lights[1].lightAmbient[2]=0.0f;
            lights[1].lightAmbient[3]=1.0f;
            
            lights[1].lightDiffuse[0]=0.0f;
            lights[1].lightDiffuse[1]=1.0f;
            lights[1].lightDiffuse[2]=0.0f;
            lights[1].lightDiffuse[3]=1.0f;
            
            lights[1].lightSpecular[0]=0.0f;
            lights[1].lightSpecular[1]=1.0f;
            lights[1].lightSpecular[2]=0.0f;
            lights[1].lightSpecular[3]=1.0f;
            
            lights[1].lightPosition[0]=0.0f;
            lights[1].lightPosition[1]=0.0f;
            lights[1].lightPosition[2]=0.0f;
            lights[1].lightPosition[3]=1.0f;
        
        //Light2
        lights[2].lightAmbient[0]=0.0f;
        lights[2].lightAmbient[1]=0.0f;
        lights[2].lightAmbient[2]=0.0f;
        lights[2].lightAmbient[3]=1.0f;
        
        lights[2].lightDiffuse[0]=0.0f;
        lights[2].lightDiffuse[1]=0.0f;
        lights[2].lightDiffuse[2]=1.0f;
        lights[2].lightDiffuse[3]=1.0f;
        
        lights[2].lightSpecular[0]=0.0f;
        lights[2].lightSpecular[1]=0.0f;
        lights[2].lightSpecular[2]=1.0f;
        lights[2].lightSpecular[3]=1.0f;
        
        lights[2].lightPosition[0]=0.0f;
        lights[2].lightPosition[1]=0.0f;
        lights[2].lightPosition[2]=0.0f;
        lights[2].lightPosition[3]=1.0f;
    
       materialAmbient[0] = 0.0f;
       materialAmbient[1] = 0.0f;
       materialAmbient[2] = 0.0f;
       materialAmbient[3] = 1.0f;
       
       materialDiffuse[0] = 1.0f;
       materialDiffuse[1] = 1.0f;
       materialDiffuse[2] = 1.0f;
       materialDiffuse[3] = 1.0f;
       
       materialSpecular[0] = 1.0f;
       materialSpecular[1] = 1.0f;
       materialSpecular[2] = 1.0f;
       materialSpecular[3] = 1.0f;

       materialShininess = 128.0f;
   
    choosenKey = 'n';
    
    bLight = NO;
    
    gbLightAngleZero = 0.0f;
    
    counter = 1;
    
    return(0);
}

- (void) resize : (int)width : (int)height
{
    // code
    if (height < 0)
        height = 1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (counter == 1 && bLight == YES)
       {
           glUseProgram(shaderProgramObject_PV);
                   
                   // transformations
                   mat4 translationMatrix = mat4::identity();
                   mat4 modelMatrix = mat4::identity();
                   mat4 viewMatrix = mat4::identity();
                   
                   
                   translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
                   modelMatrix = translationMatrix;
                   
                   glUniformMatrix4fv(modelMatrixUniform_pv, 1, GL_FALSE, modelMatrix);
                   glUniformMatrix4fv(viewMatrixUniform_pv, 1, GL_FALSE, viewMatrix);
                   glUniformMatrix4fv(projectionMatrixUniform_pv, 1, GL_FALSE, perspectiveProjectionMatrix);
                   
                  
                   glUniform1i(lightingEnabledUniform_pv, 1);
                           
                           glUniform3fv(laUniform_pv[0], 1, lights[0].lightAmbient);
                           glUniform3fv(ldUniform_pv[0], 1, lights[0].lightDiffuse);
                           glUniform3fv(lsUniform_pv[0], 1, lights[0].lightSpecular);
                           glUniform4fv(lightPositionUniform_pv[0], 1, vmath::vec4(0.0f, 80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero), 1.0f));

                           glUniform3fv(laUniform_pv[1], 1, lights[1].lightAmbient);
                           glUniform3fv(ldUniform_pv[1], 1, lights[1].lightDiffuse);
                           glUniform3fv(lsUniform_pv[1], 1, lights[1].lightSpecular);
                           glUniform4fv(lightPositionUniform_pv[1], 1, vmath::vec4(80*sin(gbLightAngleZero),0.0f, 80*cos(gbLightAngleZero), 1.0f));

                           glUniform3fv(laUniform_pv[2], 1, lights[2].lightAmbient);
                           glUniform3fv(ldUniform_pv[2], 1, lights[2].lightDiffuse);
                           glUniform3fv(lsUniform_pv[2], 1, lights[2].lightSpecular);
                           glUniform4fv(lightPositionUniform_pv[2], 1, vmath::vec4(80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero),0.0f, 1.0f));

                           glUniform3fv(kaUniform_pv, 1, materialAmbient);
                           glUniform3fv(kdUniform_pv, 1, materialDiffuse);
                           glUniform3fv(ksUniform_pv, 1, materialSpecular);

                           glUniform1f(materialShininessUniform_pv, materialShininess);
                  
                   glBindVertexArray(vao_sphere);
                   
                   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);

                   glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
                   
                   glBindVertexArray(0);
       }
       else if (counter == 2 && bLight == YES)
       {
           glUseProgram(shaderProgramObject_PF);
             
                   // transformations
                   mat4 translationMatrix = mat4::identity();
                   mat4 modelMatrix = mat4::identity();
                   mat4 viewMatrix = mat4::identity();
                   
                   
                   translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
                   modelMatrix = translationMatrix;
                   
                   glUniformMatrix4fv(modelMatrixUniform_pf, 1, GL_FALSE, modelMatrix);
                   glUniformMatrix4fv(viewMatrixUniform_pf, 1, GL_FALSE, viewMatrix);
                   glUniformMatrix4fv(projectionMatrixUniform_pf, 1, GL_FALSE, perspectiveProjectionMatrix);
                   
                   
                   glUniform1i(lightingEnabledUniform_pf, 1);

                           glUniform3fv(laUniform_pf[0], 1, lights[0].lightAmbient);
                           glUniform3fv(ldUniform_pf[0], 1, lights[0].lightDiffuse);
                           glUniform3fv(lsUniform_pf[0], 1, lights[0].lightSpecular);
                           glUniform4fv(lightPositionUniform_pf[0], 1, vmath::vec4(0.0f, 80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero), 1.0f));

                           glUniform3fv(laUniform_pf[1], 1, lights[1].lightAmbient);
                           glUniform3fv(ldUniform_pf[1], 1, lights[1].lightDiffuse);
                           glUniform3fv(lsUniform_pf[1], 1, lights[1].lightSpecular);
                           glUniform4fv(lightPositionUniform_pf[1], 1, vmath::vec4(80*sin(gbLightAngleZero),0.0f, 80*cos(gbLightAngleZero), 1.0f));

                           glUniform3fv(laUniform_pf[2], 1, lights[2].lightAmbient);
                           glUniform3fv(ldUniform_pf[2], 1, lights[2].lightDiffuse);
                           glUniform3fv(lsUniform_pf[2], 1, lights[2].lightSpecular);
                           glUniform4fv(lightPositionUniform_pf[2], 1, vmath::vec4(80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero),0.0f, 1.0f));

                           glUniform3fv(kaUniform_pf, 1, materialAmbient);
                           glUniform3fv(kdUniform_pf, 1, materialDiffuse);
                           glUniform3fv(ksUniform_pf, 1, materialSpecular);

                           glUniform1f(materialShininessUniform_pf, materialShininess);
                       
                  
                   glBindVertexArray(vao_sphere);
                   
                   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);

                   glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
                   
                   glBindVertexArray(0);
           
           
       }
       else
       {
            glUseProgram(shaderProgramObject_PF);
             
                   // transformations
                   mat4 translationMatrix = mat4::identity();
                   mat4 modelMatrix = mat4::identity();
                   mat4 viewMatrix = mat4::identity();
                   
                   
                   translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
                   modelMatrix = translationMatrix;
                   
                   glUniformMatrix4fv(modelMatrixUniform_pf, 1, GL_FALSE, modelMatrix);
                   glUniformMatrix4fv(viewMatrixUniform_pf, 1, GL_FALSE, viewMatrix);
                   glUniformMatrix4fv(projectionMatrixUniform_pf, 1, GL_FALSE, perspectiveProjectionMatrix);
                  
                   glUniform1i(lightingEnabledUniform_pf, 0);
                  
                   glBindVertexArray(vao_sphere);
                   
                   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);

                   glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
                   
                   glBindVertexArray(0);
           
       }
    
        glUseProgram(0);
}

- (void) myupdate
{
    // code
    gbLightAngleZero = gbLightAngleZero + 0.05f;

        if (gbLightAngleZero >= 360.0f)
            gbLightAngleZero = gbLightAngleZero - 360.0f;
}

- (void) uninitialize
{
    // code
    if (vbo_sphere_position)
        {
            glDeleteBuffers(1, &vbo_sphere_position);
            vbo_sphere_position = 0;
        }

        if (vbo_sphere_normal)
        {
            glDeleteBuffers(1, &vbo_sphere_normal);
            vbo_sphere_normal = 0;
        }

        if (vbo_sphere_elements)
        {
            glDeleteBuffers(1, &vbo_sphere_elements);
            vbo_sphere_elements = 0;
        }

        if (vao_sphere)
        {
            glDeleteVertexArrays(1, &vao_sphere);
            vao_sphere = 0;
        }

        free(sphereData.vertices);
        free(sphereData.normals);
        free(sphereData.textureCoordinates);
        free(sphereData.elements);

        sphereData.vertices = NULL;
        sphereData.normals = NULL;
        sphereData.textureCoordinates = NULL;
        sphereData.elements = NULL;

    // shader uninitialization
    if (shaderProgramObject_PV)
        {
            // stepF 0
            glUseProgram(shaderProgramObject_PV);

            GLsizei numAttachedShaders;
            
            // stepF 1 : get the no. of attached shaders
            glGetProgramiv(shaderProgramObject_PV, GL_ATTACHED_SHADERS, &numAttachedShaders);

            GLuint* shaderObjects = NULL;

            // stepF 2 : create empty buffer to hold array of  attached shader objects
            shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

            // stepF 3 : allocate enough memory to hold array of attached shader objects
            glGetAttachedShaders(shaderProgramObject_PV, numAttachedShaders, &numAttachedShaders, shaderObjects);

            // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
            for (GLsizei i = 0; i < numAttachedShaders; i++)
            {
                glDetachShader(shaderProgramObject_PV, shaderObjects[i]);
                glDeleteShader(shaderObjects[i]);
                shaderObjects[i] = 0;
            }

            // stepF 5 : free the memory allocated for the buffer
            free(shaderObjects);

            // stepF 6 : unuse the shader program object
            glUseProgram(0);

            // stepF 7 : delete the shader program object
            glDeleteProgram(shaderProgramObject_PV);
            shaderProgramObject_PV = 0;
            
            if (depthRenderbuffer)
            {
                glDeleteRenderbuffers(1, &depthRenderbuffer);
                depthRenderbuffer = 0;
            }
            
            if (coloRenderbuffer)
            {
                glDeleteRenderbuffers(1, &coloRenderbuffer);
                coloRenderbuffer = 0;
            }
            
            if (defaultFramebuffer)
            {
                glDeleteFramebuffers(1, &defaultFramebuffer);
                defaultFramebuffer = 0;
            }
            
            if ([EAGLContext currentContext] == eaglContext)
            {
                [EAGLContext setCurrentContext : nil];
                [EAGLContext release];
                eaglContext = nil;
            }
        }

        if (shaderProgramObject_PF)
        {
            // stepF 0
            glUseProgram(shaderProgramObject_PF);

            GLsizei numAttachedShaders;

            // stepF 1 : get the no. of attached shaders
            glGetProgramiv(shaderProgramObject_PF, GL_ATTACHED_SHADERS, &numAttachedShaders);

            GLuint* shaderObjects = NULL;

            // stepF 2 : create empty buffer to hold array of  attached shader objects
            shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

            // stepF 3 : allocate enough memory to hold array of attached shader objects
            glGetAttachedShaders(shaderProgramObject_PF, numAttachedShaders, &numAttachedShaders, shaderObjects);

            // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
            for (GLsizei i = 0; i < numAttachedShaders; i++)
            {
                glDetachShader(shaderProgramObject_PF, shaderObjects[i]);
                glDeleteShader(shaderObjects[i]);
                shaderObjects[i] = 0;
            }

            // stepF 5 : free the memory allocated for the buffer
            free(shaderObjects);

            // stepF 6 : unuse the shader program object
            glUseProgram(0);

            // stepF 7 : delete the shader program object
            glDeleteProgram(shaderProgramObject_PF);
            shaderProgramObject_PF = 0;
            
            if (depthRenderbuffer)
            {
                glDeleteRenderbuffers(1, &depthRenderbuffer);
                depthRenderbuffer = 0;
            }
            
            if (coloRenderbuffer)
            {
                glDeleteRenderbuffers(1, &coloRenderbuffer);
                coloRenderbuffer = 0;
            }
            
            if (defaultFramebuffer)
            {
                glDeleteFramebuffers(1, &defaultFramebuffer);
                defaultFramebuffer = 0;
            }
            
            if ([EAGLContext currentContext] == eaglContext)
            {
                [EAGLContext setCurrentContext : nil];
                [EAGLContext release];
                eaglContext = nil;
            }
        }
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
    
    if (counter == 1)
       {
           counter++;
       }
       else{
           counter--;
       }
  
}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
            if (bLight == NO)
            {
                bLight = YES;
                counter = 1;
                //choosenKey = 'v';
            }
            else
            {
                bLight = NO;
                //choosenKey = 'n';
            }
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
}

- (void) dealloc
{
    [self uninitialize];
    
    if (displayLink)
    {
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

@end
