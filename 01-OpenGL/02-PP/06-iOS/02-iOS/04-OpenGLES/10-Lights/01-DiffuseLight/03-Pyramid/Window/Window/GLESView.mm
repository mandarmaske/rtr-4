//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
using namespace vmath;

@implementation GLESView
{
    @private
    EAGLContext* eaglContext; // embedded apple graphics library
    
    GLuint defaultFramebuffer;
    GLuint coloRenderbuffer;
    GLuint depthRenderbuffer;
    
    CADisplayLink* displayLink; // CA for Core Animation
    GLint framesPerSecond;
    BOOL isAnimating;
    
    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    GLuint vao_pyramid;
    GLuint vbo_pyramid_position;
    GLuint vbo_pyramid_normal;
    
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;

    GLuint ldUniform;
    GLuint kdUniform;
    GLuint lightpositionUniform;

    GLuint lightingEnableUniform;
    
    GLfloat lightDiffuse[4];
    GLfloat materialDiffuse[4];
    GLfloat lightPosition[4];
    
    GLfloat anglePyramid;

    mat4 perspectiveProjectionMatrix;
    
    int doubleTap;
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // 1) Create drawable surface which is called as layer
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        // 2) Set EAGLLayer properties (this is similar to setting pixelformatdistribute properties)
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        // 3) Create eagl context
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // 4) Set this context as current context
        [EAGLContext setCurrentContext : eaglContext];
        
        // 5) Print OpenGLES info
        printf("OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
        printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
        printf("OpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // 6) Create frame buffer
        glGenFramebuffers(1, &defaultFramebuffer);
        
        // 7) Bind With Above frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // 8) Create color render buffer
        glGenRenderbuffers(1, &coloRenderbuffer);
        
        // 9) Bind with above color render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
        
        // 10) Provide storage to render buffer
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        // 11) Set frame buffer according render buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, coloRenderbuffer);
        
        // 12) Declare backing width
        GLint backingWidth;
        
        // 13) Declare backing height
        GLint backingHeight;
        
        // 14) Get Color render buffer width into backing width
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        // 15) Get Color render buffer height into backing height
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        // 16) Create depth buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        
        // 17) bind above depth render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        // 18) Provide storage to depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        // 19) Get
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // 20) check the status of frame buffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // call initialize
        int result = [self initialize];
        
        if(result!=0)
        {
            printf("Result Not Obtained \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        
        framesPerSecond = 60; // this is started from iOS 8.0
        isAnimating = NO;
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

+ (Class)layerClass
{
    // code
    
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);

    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];
    
    // 12) Declare backing width
    GLint backingWidth;
    
    // 13) Declare backing height
    GLint backingHeight;
    
    // 14) Get Color render buffer width into backing width
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    // 15) Get Color render buffer height into backing height
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    // 16) Create depth buffer
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    // 17) bind above depth render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    // 18) Provide storage to depth render buffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    // 19) Get
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    // call our resize here
    [self resize : backingWidth : backingHeight];
    
    // 20) check the status of frame buffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }
    
    // call drawView after resize
    [self drawView : nil]; // or [self drawView : self];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    // call our display and update here
    [self display];
    [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        // create display link
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        // set frames per second for the display link
        [displayLink setPreferredFramesPerSecond : framesPerSecond];
        
        // add this display link to run loop
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        // remove display link from run loop
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    // code
    const GLchar* vertexShaderSourceCode =
            "#version 300 core" \
            "\n" \
            "in vec4 a_position;" \
            "in vec3 a_normal;" \
            "uniform mat4 u_modelMatrix;" \
            "uniform mat4 u_viewMatrix;" \
            "uniform mat4 u_projectionMatrix;" \
            "uniform vec3 u_ld;" \
            "uniform vec3 u_kd;" \
            "uniform vec4 u_lightPosition;" \
            "uniform int u_lightingEnabled;" \
            "out vec3 diffuse_light_color;" \
            "void main(void)" \
            "{" \
            "if(u_lightingEnabled==1)" \
            "{"
            "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
            "mat3 normalMatrix = mat3(transpose(inverse(u_viewMatrix * u_modelMatrix)));" \
            "vec3 transformedNormals = normalize(normalMatrix * a_normal);" \
            "vec3 lightDirection = normalize(vec3(u_lightPosition - eyeCoordinates));" \
            "diffuse_light_color = u_ld * u_kd * max(dot(lightDirection,transformedNormals),0.0);" \
            "}" \
            "gl_Position = u_projectionMatrix *u_viewMatrix * u_modelMatrix * a_position;" \
            "}";

        // StepC 2 : creating shading object
        GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                    // stepC 5 e : display the log
                    printf("Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode =
            "#version 300 core" \
            "\n" \
            "precision highp float;" \
            "in vec4 a_color_out;" \
            "in vec3 diffuse_light_color;" \
            "uniform highp int u_lightingEnabled;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
            "if(u_lightingEnabled==1)"
            "{" \
                "FragColor = vec4(diffuse_light_color,1.0);" \
            "}" \
            "else" \
            "{" \
                "FragColor = vec4(1.0,1.0,1.0,1.0);" \
            "}" \
            "}";

        // StepC 2 : creating shading object
        GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    printf("Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");

        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    printf("Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);

                }
            }
        }

    modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
        viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
        projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

        ldUniform= glGetUniformLocation(shaderProgramObject, "u_ld");
        kdUniform= glGetUniformLocation(shaderProgramObject, "u_kd");
        lightpositionUniform = glGetUniformLocation(shaderProgramObject, "u_lightPosition");
        lightingEnableUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

        // vao & vbo related code
        //declarations of vertex data arrays
    const GLfloat pyramidPosition[] =
        {
            // front
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,

            // right
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,

            // back
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,

            // left
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f

        };

        const GLfloat pyramidNormal[] =
        {
            0.0f, 0.447214f, 0.894427f,// front-top
            0.0f, 0.447214f, 0.894427f,// front-left
            0.0f, 0.447214f, 0.894427f,// front-right

            0.894427f, 0.447214f, 0.0f, // right-top
            0.894427f, 0.447214f, 0.0f, // right-left
            0.894427f, 0.447214f, 0.0f, // right-right

            0.0f, 0.447214f, -0.894427f, // back-top
            0.0f, 0.447214f, -0.894427f, // back-left
            0.0f, 0.447214f, -0.894427f, // back-right

            -0.894427f, 0.447214f, 0.0f, // left-top
            -0.894427f, 0.447214f, 0.0f, // left-left
            -0.894427f, 0.447214f, 0.0f // left-right

        };

        // pyramid
        // vao
        glGenVertexArrays(1, &vao_pyramid); // vao:- vertex array object
        glBindVertexArray(vao_pyramid);

        // vbo for position
        glGenBuffers(1, &vbo_pyramid_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidPosition), pyramidPosition, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // pyramid normal
        glGenBuffers(1, &vbo_pyramid_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        glBindVertexArray(0);
    
    glClearDepthf(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
    
    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    
    materialDiffuse[0] = 0.5f;
    materialDiffuse[1] = 0.5f;
    materialDiffuse[2] = 0.5f;
    materialDiffuse[3] = 1.0f;

    lightPosition[0] = 0.0f;
    lightPosition[1] = 0.0;
    lightPosition[2] = 2.0f;
    lightPosition[3] = 1.0f;
    
    anglePyramid = 0.0f;
    doubleTap = 0;
    
    return(0);
}

- (void) resize : (int)width : (int)height
{
    // code
    if (height < 0)
        height = 1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);

    // pyramid
        // transformations
        mat4 translationMatrix= mat4::identity();
        mat4 rotationMatrix= mat4::identity();
        mat4 modelMatrix = mat4::identity();
        mat4 viewMatrix = mat4::identity();
        mat4 modelViewProjectionMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); //glTranslatef() is replaced by this line
        rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
        modelMatrix = translationMatrix * rotationMatrix; // order is imp
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelMatrix; // order imp aahe
        
        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        // sending light related uniforms
        if (doubleTap == 1)
        {
            glUniform1i(lightingEnableUniform, 1);

            glUniform3fv(ldUniform, 1, lightDiffuse);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform4fv(lightpositionUniform, 1, lightPosition);
        }
        else
        {
            glUniform1i(lightingEnableUniform, 0);
        }

        glBindVertexArray(vao_pyramid);

        // stepE 2 : draw the desiered graphics/animation
        // here will be magic code

        glDrawArrays(GL_TRIANGLES, 0, 12);

        glBindVertexArray(0);
        // stepE 3 : unuse the shader program object
        
        glUseProgram(0);
}

- (void) myupdate
{
    // code
    anglePyramid = anglePyramid + 0.05f;

        if (anglePyramid >= 360.0f)
            anglePyramid = anglePyramid - 360.0f;
}

- (void) uninitialize
{
    // code
    if (vbo_pyramid_normal)
        {
            glDeleteBuffers(1, &vbo_pyramid_normal);
            vbo_pyramid_normal = 0;
        }

        // deletion/ uninitialization of vbo
        if (vbo_pyramid_position)
        {
            glDeleteBuffers(1, &vbo_pyramid_position);
            vbo_pyramid_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_pyramid)
        {
            glDeleteVertexArrays(1, &vao_pyramid);
            vao_pyramid = 0;
        }
    
    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);
        
        GLsizei numAttachedShaders;
        
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
        
        GLuint* shaderObjects = NULL;
        
        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
        
        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);
        
        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }
        
        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);
        
        // stepF 6 : unuse the shader program object
        glUseProgram(0);
        
        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        
        if (depthRenderbuffer)
        {
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            depthRenderbuffer = 0;
        }
        
        if (coloRenderbuffer)
        {
            glDeleteRenderbuffers(1, &coloRenderbuffer);
            coloRenderbuffer = 0;
        }
        
        if (defaultFramebuffer)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            defaultFramebuffer = 0;
        }
        
        if ([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext : nil];
            [EAGLContext release];
            eaglContext = nil;
        }
    }
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
    doubleTap = doubleTap + 1;
            if(doubleTap > 1)
            {
                doubleTap = 0;
            }
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
}

- (void) dealloc
{
    [self uninitialize];
    
    if (displayLink)
    {
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

@end
