//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
using namespace vmath;

@implementation GLESView
{
    @private
    EAGLContext* eaglContext; // embedded apple graphics library
    
    GLuint defaultFramebuffer;
    GLuint coloRenderbuffer;
    GLuint depthRenderbuffer;
    
    CADisplayLink* displayLink; // CA for Core Animation
    GLint framesPerSecond;
    BOOL isAnimating;
    
    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    // variables for pyramid
    GLuint vao_Pyramid;
 GLuint vbo_Pyramid_position;
 GLuint vbo_Pyramid_normal;

 GLfloat anglePyramid;

 GLuint modelMatrixUniform;
 GLuint viewMatrixUniform;
 GLuint projectionMatrixUniform;

 mat4 perspectiveProjectionMatrix;

 GLuint laUniform[2];            // light ambient
 GLuint ldUniform[2];            // light Diffuse
 GLuint lsUniform[2];            // light specular
 GLuint lightPositionUniform[2];

 GLuint kaUniform;
 GLuint kdUniform;
 GLuint ksUniform;
 GLuint materialEnableUniform;

 GLuint lightingEnableUniform;

 struct Light
 {
    vec4 lightAmbient;
    vec4 lightDiffuse;
    vec4 lightSpecular;
    vec4 lightPosition;
 };

 Light lights[2];

 GLfloat materialAmbient[4];
 GLfloat materialDiffuse[4];
 GLfloat materialSpecular[4];
 GLfloat materialShininess;
    
    BOOL bLight;
    
    int doubleTap;
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // 1) Create drawable surface which is called as layer
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        // 2) Set EAGLLayer properties (this is similar to setting pixelformatdistribute properties)
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        // 3) Create eagl context
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // 4) Set this context as current context
        [EAGLContext setCurrentContext : eaglContext];
        
        // 5) Print OpenGLES info
        printf("OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
        printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
        printf("OpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // 6) Create frame buffer
        glGenFramebuffers(1, &defaultFramebuffer);
        
        // 7) Bind With Above frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // 8) Create color render buffer
        glGenRenderbuffers(1, &coloRenderbuffer);
        
        // 9) Bind with above color render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
        
        // 10) Provide storage to render buffer
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        // 11) Set frame buffer according render buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, coloRenderbuffer);
        
        // 12) Declare backing width
        GLint backingWidth;
        
        // 13) Declare backing height
        GLint backingHeight;
        
        // 14) Get Color render buffer width into backing width
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        // 15) Get Color render buffer height into backing height
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        // 16) Create depth buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        
        // 17) bind above depth render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        // 18) Provide storage to depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        // 19) Get
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // 20) check the status of frame buffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // call initialize
        int result = [self initialize];
        
        if(result!=0)
        {
            printf("Result Not Obtained \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        
        framesPerSecond = 60; // this is started from iOS 8.0
        isAnimating = NO;
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

+ (Class)layerClass
{
    // code
    
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);

    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];
    
    // 12) Declare backing width
    GLint backingWidth;
    
    // 13) Declare backing height
    GLint backingHeight;
    
    // 14) Get Color render buffer width into backing width
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    // 15) Get Color render buffer height into backing height
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    // 16) Create depth buffer
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    // 17) bind above depth render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    // 18) Provide storage to depth render buffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    // 19) Get
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    // call our resize here
    [self resize : backingWidth : backingHeight];
    
    // 20) check the status of frame buffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }
    
    // call drawView after resize
    [self drawView : nil]; // or [self drawView : self];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    // call our display and update here
    [self display];
    [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        // create display link
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        // set frames per second for the display link
        [displayLink setPreferredFramesPerSecond : framesPerSecond];
        
        // add this display link to run loop
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        // remove display link from run loop
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    // code
    const GLchar* vertexShaderSourceCode =
    "#version 300 core" \
    "\n" \
    "precision highp float;" \
    "in vec4 a_position;" \
    "in vec3 a_normal;" \
    "uniform mat4 u_modelMatrix;" \
    "uniform mat4 u_viewMatrix;" \
    "uniform mat4 u_projectionMatrix;" \
    "uniform vec3 u_la[2];" \
    "uniform vec3 u_ld[2];" \
    "uniform vec3 u_ls[2];" \
    "uniform vec4 u_lightPosition[2];" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_materialShininess;" \
    "uniform int u_lightingEnable;" \
    "out vec3 phong_ads_light;" \
    "void main(void)" \
    "{" \
    "if(u_lightingEnable == 1)" \
    "{" \
    "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
    "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
    "vec3 transformedNormals = normalize(normalMatrix * a_normal);" \
    "vec3 viewerVector = normalize(-eyeCoordinates.xyz);" \
    "vec3 ambient[2];" \
    "vec3 lightDirection[2];" \
    "vec3 diffuse[2];" \
    "vec3 reflectionVector[2];" \
    "vec3 specular[2];" \
    "vec3 total[2];"
    "for(int i = 0; i < 2; i++)" \
    "{" \
    "ambient[i] = u_la[i] * u_ka;" \
    "lightDirection[i] = normalize(vec3(u_lightPosition[i]) - eyeCoordinates.xyz);" \
    "diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformedNormals), 0.0);" \
    "reflectionVector[i] = reflect(-lightDirection[i], transformedNormals);" \
    "specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewerVector), 0.0), u_materialShininess);" \
    "total[i] = ambient[i] + diffuse[i] + specular[i];" \
    "}" \
    "phong_ads_light = total[0] + total[1];" \
    "}" \
    "else" \
    "{" \
    "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
    "}" \
    "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
    "}";

GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
glCompileShader(vertexShaderObject);

GLint status;
GLint infoLogLength;
char* log = NULL;
glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

if (status == GL_FALSE)
{
    glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0)
    {
        log = (char*)malloc(infoLogLength);
        if (log != NULL)
        {
            GLsizei written;
            glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
            printf("Vertex Shader Compilation Log : %s \n", log);
            free(log);
            [self uninitialize];
            [self release];
            exit(0);
        }
    }
}

// Fragment Shader
const GLchar* fragmentShaderSourceCode=
    "#version 300 core" \
    "\n" \
    "precision highp float;" \
    "in vec3 phong_ads_light;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "FragColor = vec4(phong_ads_light, 1.0);" \
    "}";

GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
glCompileShader(fragmentShaderObject);

status = 0;
infoLogLength = 0;
log = NULL;

glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

if (status == GL_FALSE)
{
    glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0)
    {
        log = (char*)malloc(infoLogLength);
        if(log != NULL)
        {
            GLsizei written;
            glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
            printf("Fragment Shader Compilation Log : %s \n", log);
            free(log);
            [self uninitialize];
            [self release];
            exit(0);
        }
    }
}

// Shader program object
shaderProgramObject = glCreateProgram();
glAttachShader(shaderProgramObject, vertexShaderObject);
glAttachShader(shaderProgramObject, fragmentShaderObject);

glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");

glLinkProgram(shaderProgramObject);


status = 0;
infoLogLength = 0;
log = NULL;
glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);
if (status == GL_FALSE)
{
    glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 0)
    {
        log = (char*)malloc(infoLogLength);
        if (log != NULL)
        {
            GLsizei written;
            glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
            printf("Shader Program Link Log : %s \n", log);
            free(log);
            [self uninitialize];
            [self release];
            exit(0);
        }
    }
}

modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

laUniform[0] = glGetUniformLocation(shaderProgramObject, "u_la[0]");
ldUniform[0] = glGetUniformLocation(shaderProgramObject, "u_ld[0]");
lsUniform[0] = glGetUniformLocation(shaderProgramObject, "u_ls[0]");
lightPositionUniform[0] = glGetUniformLocation(shaderProgramObject, "u_lightPosition[0]");

laUniform[1] = glGetUniformLocation(shaderProgramObject, "u_la[1]");
ldUniform[1] = glGetUniformLocation(shaderProgramObject, "u_ld[1]");
lsUniform[1] = glGetUniformLocation(shaderProgramObject, "u_ls[1]");
lightPositionUniform[1] = glGetUniformLocation(shaderProgramObject, "u_lightPosition[1]");

kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
materialEnableUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");

lightingEnableUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnable");

// declaration vertex data arrays
const GLfloat pyramidPosition[] =
{
    // front
    0.0f, 1.0f, 0.0f,
    -1.0f, -1.0f, 1.0f,
    1.0f, -1.0f, 1.0f,

    // right
    0.0f, 1.0f, 0.0f,
    1.0f, -1.0f, 1.0f,
    1.0f, -1.0f, -1.0f,

    // back
    0.0f, 1.0f, 0.0f,
    1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,

    // left
    0.0f, 1.0f, 0.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f, 1.0f

};

GLfloat pyramidNormals[] =
{
    0.0f, 0.447214f, 0.894427f,// front-top
    0.0f, 0.447214f, 0.894427f,// front-left
    0.0f, 0.447214f, 0.894427f,// front-right

    0.894427f, 0.447214f, 0.0f, // right-top
    0.894427f, 0.447214f, 0.0f, // right-left
    0.894427f, 0.447214f, 0.0f, // right-right

    0.0f, 0.447214f, -0.894427f, // back-top
    0.0f, 0.447214f, -0.894427f, // back-left
    0.0f, 0.447214f, -0.894427f, // back-right

    -0.894427f, 0.447214f, 0.0f, // left-top
    -0.894427f, 0.447214f, 0.0f, // left-left
    -0.894427f, 0.447214f, 0.0f // left-right

};

// vao and vbo related code

// Pyramid
// vao
glGenVertexArrays(1, &vao_Pyramid);
glBindVertexArray(vao_Pyramid);                                    // Bind Vertex Array Object(vao)

// vbo for position
glGenBuffers(1, &vbo_Pyramid_position);
glBindBuffer(GL_ARRAY_BUFFER, vbo_Pyramid_position);                        // Bind Buffer
glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidPosition), pyramidPosition, GL_STATIC_DRAW);
glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);                    // GL_FALSE means not Normalise
glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

glBindBuffer(GL_ARRAY_BUFFER, 0);                        // Unbind Buffer

// vbo for normal
glGenBuffers(1, &vbo_Pyramid_normal);
glBindBuffer(GL_ARRAY_BUFFER, vbo_Pyramid_normal);                        // Bind Buffer
glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);                    // GL_FALSE means not Normalise
glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

glBindVertexArray(0);                                    // Unbind Vertex Array Object(vao) Pyramid

// depth
glClearDepthf(1.0f);
glEnable(GL_DEPTH_TEST);
glDepthFunc(GL_LEQUAL);

glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

lights[0].lightAmbient = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
lights[0].lightDiffuse = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f);
lights[0].lightSpecular = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f);
lights[0].lightPosition = vmath::vec4(-2.0f, 0.0f, 0.0f, 1.0f);

lights[1].lightAmbient = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
lights[1].lightDiffuse = vmath::vec4(0.0f, 0.0f, 1.0f, 1.0f);
lights[1].lightSpecular = vmath::vec4(0.0f, 1.0f, 0.0f, 1.0f);
lights[1].lightPosition = vmath::vec4(2.0f, 0.0f, 0.0f, 1.0f);

 materialAmbient[0]=0.0f;
 materialAmbient[1]=0.0f;
 materialAmbient[2]=0.0f;
 materialAmbient[3]=1.0f;
 
 materialDiffuse[0]=1.0f;
 materialDiffuse[1]=1.0f;
 materialDiffuse[2]=1.0f;
 materialDiffuse[3]=1.0f;
 
 materialSpecular[0]=1.0f;
 materialSpecular[1]=1.0f;
 materialSpecular[2]=1.0f;
 materialSpecular[3]=1.0f;
 materialShininess=50.0f;

perspectiveProjectionMatrix = mat4::identity();

anglePyramid = 0.0f;
    doubleTap = 0;
    
    return(0);
}

- (void) resize : (int)width : (int)height
{
    // code
    if (height < 0)
        height = 1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);

// Cube
// Transformations
mat4 translationMatrix = mat4::identity();
mat4 rotationMatrix = mat4::identity();
mat4 modelMatrix = mat4::identity();
mat4 viewMatrix = mat4::identity();

 translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);   //glTranslatef() is replaced by this line
    rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);

    modelMatrix = translationMatrix * rotationMatrix;        // Order is very important
    
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    if (doubleTap == 1)
    {
        glUniform1i(lightingEnableUniform, 1);
        
        for (int i = 0; i < 2; i++)
        {
            glUniform3fv(laUniform[i], 1, lights[i].lightAmbient);
            glUniform3fv(ldUniform[i], 1, lights[i].lightDiffuse);
            glUniform3fv(lsUniform[i], 1, lights[i].lightSpecular);
            glUniform4fv(lightPositionUniform[i], 1, lights[i].lightPosition);
            
            //fprintf(gpFile,"light is enabled. \n");
    }

    glUniform3fv(kaUniform, 1, materialAmbient);
    glUniform3fv(kdUniform, 1, materialDiffuse);
    glUniform3fv(ksUniform, 1, materialSpecular);
    glUniform1f(materialEnableUniform, materialShininess);
}
else
{
    glUniform1i(lightingEnableUniform, 0);
}

glBindVertexArray(vao_Pyramid);

glDrawArrays(GL_TRIANGLES, 0, 12);

glBindVertexArray(0);
        
        glUseProgram(0);
}

- (void) myupdate
{
    // code
    anglePyramid = anglePyramid + 0.05f;

        if (anglePyramid >= 360.0f)
            anglePyramid = anglePyramid - 360.0f;
}

- (void) uninitialize
{
    // code
    if (vbo_Pyramid_normal)
        {
            glDeleteBuffers(1, &vbo_Pyramid_normal);
            vbo_Pyramid_normal = 0;
        }
    
    if (vbo_Pyramid_position)
    {
        glDeleteBuffers(1, &vbo_Pyramid_position);
        vbo_Pyramid_position = 0;
    }

        // uninitialization of vertex buffer object(vao)
        if (vao_Pyramid)
        {
            glDeleteVertexArrays(1, &vao_Pyramid);
            vao_Pyramid = 0;
        }

    
    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);
        
        GLsizei numAttachedShaders;
        
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
        
        GLuint* shaderObjects = NULL;
        
        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
        
        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);
        
        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }
        
        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);
        
        // stepF 6 : unuse the shader program object
        glUseProgram(0);
        
        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        
        if (depthRenderbuffer)
        {
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            depthRenderbuffer = 0;
        }
        
        if (coloRenderbuffer)
        {
            glDeleteRenderbuffers(1, &coloRenderbuffer);
            coloRenderbuffer = 0;
        }
        
        if (defaultFramebuffer)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            defaultFramebuffer = 0;
        }
        
        if ([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext : nil];
            [EAGLContext release];
            eaglContext = nil;
        }
    }
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
    doubleTap = doubleTap + 1;
            if(doubleTap > 1)
            {
                doubleTap = 0;
            }
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
}

- (void) dealloc
{
    [self uninitialize];
    
    if (displayLink)
    {
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

@end
