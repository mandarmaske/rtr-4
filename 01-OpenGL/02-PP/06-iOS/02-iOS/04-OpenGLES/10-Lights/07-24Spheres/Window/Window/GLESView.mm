//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "GLESView.h"
#import "Sphere.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
using namespace vmath;

@implementation GLESView
{
    @private
    EAGLContext* eaglContext; // embedded apple graphics library
    
    GLuint defaultFramebuffer;
    GLuint coloRenderbuffer;
    GLuint depthRenderbuffer;
    
    CADisplayLink* displayLink; // CA for Core Animation
    GLint framesPerSecond;
    BOOL isAnimating;
    
    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    // variables for cube
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_elements;
    
    SphereData sphereData;
    
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;

    GLuint laUniform; // light ambient
    GLuint ldUniform; // light diffuse
    GLuint lsUniform; // light specular
    GLuint lightPositionUniform;

    GLuint kaUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint materialShininessUniform;

    GLuint lightingEnabledUniform;

    BOOL bLight;
    
    GLfloat gbLightAmbient[4];
    GLfloat gbLightDiffuse[4];
    GLfloat gbLightSpecular[4];

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;
    
    GLfloat gbLightAngleZero;
    int dcounter;
    BOOL gbFullScreen;
  
    int WIN_WIDTH;
    int WIN_HEIGHT;
    int FULL_WIN_WIDTH;
    int FULL_WIN_HEIGHT;
    
    int count;
    mat4 perspectiveProjectionMatrix;

}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // 1) Create drawable surface which is called as layer
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        // 2) Set EAGLLayer properties (this is similar to setting pixelformatdistribute properties)
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        // 3) Create eagl context
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // 4) Set this context as current context
        [EAGLContext setCurrentContext : eaglContext];
        
        // 5) Print OpenGLES info
        printf("OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
        printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
        printf("OpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // 6) Create frame buffer
        glGenFramebuffers(1, &defaultFramebuffer);
        
        // 7) Bind With Above frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // 8) Create color render buffer
        glGenRenderbuffers(1, &coloRenderbuffer);
        
        // 9) Bind with above color render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
        
        // 10) Provide storage to render buffer
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        // 11) Set frame buffer according render buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, coloRenderbuffer);
        
        // 12) Declare backing width
        GLint backingWidth;
        
        // 13) Declare backing height
        GLint backingHeight;
        
        // 14) Get Color render buffer width into backing width
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        // 15) Get Color render buffer height into backing height
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        // 16) Create depth buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        
        // 17) bind above depth render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        // 18) Provide storage to depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        // 19) Get
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // 20) check the status of frame buffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // call initialize
        int result = [self initialize];
        
        if(result!=0)
        {
            printf("Result Not Obtained \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        
        framesPerSecond = 60; // this is started from iOS 8.0
        isAnimating = NO;
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

+ (Class)layerClass
{
    // code
    
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);

    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];
    
    // 12) Declare backing width
    GLint backingWidth;
    
    // 13) Declare backing height
    GLint backingHeight;
    
    // 14) Get Color render buffer width into backing width
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    // 15) Get Color render buffer height into backing height
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    // 16) Create depth buffer
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    // 17) bind above depth render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    // 18) Provide storage to depth render buffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    // 19) Get
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    // call our resize here
    [self resize : backingWidth : backingHeight];
    
    // 20) check the status of frame buffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }
    
    // call drawView after resize
    [self drawView : nil]; // or [self drawView : self];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    // call our display and update here
    [self display];
    [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        // create display link
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        // set frames per second for the display link
        [displayLink setPreferredFramesPerSecond : framesPerSecond];
        
        // add this display link to run loop
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        // remove display link from run loop
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    // code
    const GLchar* vertexShaderSourceCode =
            "#version 300 core" \
            "\n" \
    "in vec4 a_position;" \
            "in vec3 a_normal;" \
            "uniform mat4 u_modelMatrix;" \
            "uniform mat4 u_viewMatrix;" \
            "uniform mat4 u_projectionMatrix;" \
            "uniform vec4 u_lightPosition;" \
            "uniform int u_lightingEnabled;" \
            "out vec3 transformedNormals;" \
            "out vec3 lightDirection;" \
            "out vec3 viewerVector;" \
            "void main(void)" \
            "{" \
                "if(u_lightingEnabled==1)" \
                "{" \
                    "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
                    "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
                    "transformedNormals = normalMatrix * a_normal;" \
                    "lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" \
                    "viewerVector = -eyeCoordinates.xyz;" \
                "}" \
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
            "}";
    // StepC 2 : creating shading object
        GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                    // stepC 5 e : display the log
                    printf("Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode =
            "#version 300 core" \
            "\n" \
            "precision highp float;" \
            "in vec3 transformedNormals;" \
            "in vec3 lightDirection;" \
            "in vec3 viewerVector;" \
            "uniform vec3 u_la;" \
            "uniform vec3 u_ld;" \
            "uniform vec3 u_ls;" \
            "uniform vec3 u_ka;" \
            "uniform vec3 u_kd;" \
            "uniform vec3 u_ks;" \
            "uniform float u_materialShininess;" \
            "uniform highp int u_lightingEnabled;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
                "vec3 phong_ads_light;" \
                "if(u_lightingEnabled==1)" \
                "{" \
                    "vec3 ambient = u_la * u_ka;" \
                    "vec3 normalized_transformedNormals = normalize(transformedNormals);" \
                    "vec3 normalized_lightDirection = normalize(lightDirection);" \
                    "vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" \
                    "vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" \
                    "vec3 normalized_viewerVector = normalize(viewerVector);" \
                    "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" \
                    "phong_ads_light = ambient + diffuse + specular;" \
                "}" \
                "else" \
                "{" \
                    "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
                "}" \
                "FragColor = vec4(phong_ads_light,1.0);" \
            "}";
        
        // StepC 2 : creating shading object
        GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    printf("Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");
    
        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    printf("Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);

                }
            }
        }

    modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
    viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
    projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

    // light uniform
        laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
        ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
        lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_lightPosition");

        // material uniform
        kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
        kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
        ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
        materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");
    
    lightingEnabledUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

    Sphere *sphere = new Sphere();
    sphere->getSphereVertexData(&sphereData);

    delete sphere;
    sphere = NULL;

// vao & vbo related code
    //declarations of vertex data arrays

// vao
    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    // position vbo
    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // normal vbo
    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.normals, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * 2280, sphereData.elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    
    glClearDepthf(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
    
    gbLightAmbient[0] = 0.0f;
    gbLightAmbient[1] = 0.0f;
    gbLightAmbient[2] = 0.0f;
    gbLightAmbient[3] = 1.0f;

    gbLightDiffuse[0] = 1.0f;
    gbLightDiffuse[1] = 1.0f;
    gbLightDiffuse[2] = 1.0f;
    gbLightDiffuse[3] = 1.0f;
    
    gbLightSpecular[0] = 1.0f;
    gbLightSpecular[1] = 1.0f;
    gbLightSpecular[2] = 1.0f;
    gbLightSpecular[3] = 1.0f;

    materialShininess = 128.0f;
    
    bLight = NO;
    gbLightAngleZero = 0.0f;
    dcounter = 0;
    gbFullScreen = NO;
    
    count = 0;
    
    return(0);
}

- (void) resize : (int)width : (int)height
{
    // code
    if (height < 0)
        height = 1;
    
    WIN_WIDTH = width;
    WIN_HEIGHT = height;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

                glUniform3fv(laUniform, 1, gbLightAmbient);
                glUniform3fv(ldUniform, 1, gbLightDiffuse);
                glUniform3fv(lsUniform, 1, gbLightSpecular);

                if (dcounter == 1)
                    glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleZero) * 50, cos(gbLightAngleZero) * 50, 1.0f));
                else if (dcounter == 2)
                    glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZero) * 50, 0.0f, cos(gbLightAngleZero) * 50, 1.0f));
                else if (dcounter == 3)
                    glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZero) * 50, cos(gbLightAngleZero) * 50, 0.0f, 1.0f));
                else
                    glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    
    if (gbFullScreen == FALSE)
        {
            glViewport(0, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(0, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

    // From Left
        // 1 Sphere 1st Column
        // transformations
        mat4 translationMatrix = mat4::identity();
        mat4 modelMatrix = mat4::identity();
        mat4 viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(-0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ***** 1st sphere on 1st column, emerald *****
        // ambient material
        materialAmbient[0] = 0.0215f; // r
        materialAmbient[1] = 0.1745f; // g
        materialAmbient[2] = 0.0215f; // b
        materialAmbient[3] = 1.0f;   // a

        // diffuse material
        materialDiffuse[0] = 0.07568f; // r
        materialDiffuse[1] = 0.61424f; // g
        materialDiffuse[2] = 0.07568f; // b
        materialDiffuse[3] = 1.0f;    // a

        // specular material
        materialSpecular[0] = 0.633f;    // r
        materialSpecular[1] = 0.727811f; // g
        materialSpecular[2] = 0.633f;    // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.6 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(0, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(0, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 2nd Sphere 1st Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.135f;  // r
        materialAmbient[1] = 0.2225f; // g
        materialAmbient[2] = 0.1575f; // b
        materialAmbient[3] = 1.0f;   // a

        // diffuse material
        materialDiffuse[0] = 0.54f; // r
        materialDiffuse[1] = 0.89f; // g
        materialDiffuse[2] = 0.63f; // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.316228f; // r
        materialSpecular[1] = 0.316228f; // g
        materialSpecular[2] = 0.316228f; // b
        materialSpecular[3] = 1.0f;      // a

        // shininess
        materialShininess = float(0.1 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(0, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(0, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 3rd Sphere 1st Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        materialAmbient[0] = 0.05375f; // r
        materialAmbient[1] = 0.05f;    // g
        materialAmbient[2] = 0.06625f; // b
        materialAmbient[3] = 1.0f;     // a

        // diffuse material
        materialDiffuse[0] = 0.18275f; // r
        materialDiffuse[1] = 0.17f;    // g
        materialDiffuse[2] = 0.22525f; // b
        materialDiffuse[3] = 1.0f;     // a

        // specular material
        materialSpecular[0] = 0.332741f; // r
        materialSpecular[1] = 0.328634f; // g
        materialSpecular[2] = 0.346435f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.3 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);

        if (gbFullScreen == NO)
        {
            glViewport(0, WIN_HEIGHT / 3.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(0, FULL_WIN_HEIGHT / 3.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 4th Sphere 1st Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        materialAmbient[0] = 0.25f;    // r
        materialAmbient[1] = 0.20725f; // g
        materialAmbient[2] = 0.20725f; // b
        materialAmbient[3] = 1.0f;    // a

        // diffuse material
        materialDiffuse[0] = 1.0f;   // r
        materialDiffuse[1] = 0.829f; // g
        materialDiffuse[2] = 0.829f; // b
        materialDiffuse[3] = 1.0f;  // a

        // specular material
        materialSpecular[0] = 0.296648f; // r
        materialSpecular[1] = 0.296648f; // g
        materialSpecular[2] = 0.296648f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.088 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(0, WIN_HEIGHT / 6.5, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(0, FULL_WIN_HEIGHT / 6.5, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 5th Sphere 1st Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.1745f;  // r
        materialAmbient[1] = 0.01175f; // g
        materialAmbient[2] = 0.01175f; // b
        materialAmbient[3] = 1.0f;    // a

        // diffuse material
        materialDiffuse[0] = 0.61424f; // r
        materialDiffuse[1] = 0.04136f; // g
        materialDiffuse[2] = 0.04136f; // b
        materialDiffuse[3] = 1.0f;    // a

        // specular material
        materialSpecular[0] = 0.727811f; // r
        materialSpecular[1] = 0.626959f; // g
        materialSpecular[2] = 0.626959f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.6 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(0, WIN_HEIGHT / 220.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(0, FULL_WIN_HEIGHT / 220.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 6th Sphere 1st Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        materialAmbient[0] = 0.1f;     // r
        materialAmbient[1] = 0.18725f; // g
        materialAmbient[2] = 0.1745f;  // b
        materialAmbient[3] = 1.0f;    // a

        // diffuse material
        materialDiffuse[0] = 0.396f;   // r
        materialDiffuse[1] = 0.74151f; // g
        materialDiffuse[2] = 0.69102f; // b
        materialDiffuse[3] = 1.0f;    // a

        // specular material
        materialSpecular[0] = 0.297254f; // r
        materialSpecular[1] = 0.30829f;  // g
        materialSpecular[2] = 0.306678f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.1 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        // 1 Sphere 2nd Column
        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }
        // transformations
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.329412f; // r
        materialAmbient[1] = 0.223529f; // g
        materialAmbient[2] = 0.027451f; // b
        materialAmbient[3] = 1.0f;     // a

        // diffuse material
        materialDiffuse[0] = 0.780392f; // r
        materialDiffuse[1] = 0.568627f; // g
        materialDiffuse[2] = 0.113725f; // b
        materialDiffuse[3] = 1.0f;     // a

        // specular material
        materialSpecular[0] = 0.992157f; // r
        materialSpecular[1] = 0.941176f; // g
        materialSpecular[2] = 0.807843f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.21794872 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);

        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 2nd Sphere 2nd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.2125f; // r
        materialAmbient[1] = 0.1275f; // g
        materialAmbient[2] = 0.054f;  // b
        materialAmbient[3] = 1.0f;   // a

        // diffuse material
        materialDiffuse[0] = 0.714f;   // r
        materialDiffuse[1] = 0.4284f;  // g
        materialDiffuse[2] = 0.18144f; // b
        materialDiffuse[3] = 1.0f;    // a

        // specular material
        materialSpecular[0] = 0.393548f; // r
        materialSpecular[1] = 0.271906f; // g
        materialSpecular[2] = 0.166721f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.2 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 3rd Sphere 2nd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.25f; // r
        materialAmbient[1] = 0.25f; // g
        materialAmbient[2] = 0.25f; // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.4f;  // r
        materialDiffuse[1] = 0.4f;  // g
        materialDiffuse[2] = 0.4f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.774597f; // r
        materialSpecular[1] = 0.774597f; // g
        materialSpecular[2] = 0.774597f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.6 * 128);
        
        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 3.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 3.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 4th Sphere 2nd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.19125f; // r
        materialAmbient[1] = 0.0735f;  // g
        materialAmbient[2] = 0.0225f;  // b
        materialAmbient[3] = 1.0f;    // a

        // diffuse material
        materialDiffuse[0] = 0.7038f;  // r
        materialDiffuse[1] = 0.27048f; // g
        materialDiffuse[2] = 0.0828f;  // b
        materialDiffuse[3] = 1.0f;    // a

        // specular material
        materialSpecular[0] = 0.256777f; // r
        materialSpecular[1] = 0.137622f; // g
        materialSpecular[2] = 0.086014f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.1 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 6.5f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 6.5f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }


        // 5th Sphere 2nd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.24725f; // r
        materialAmbient[1] = 0.1995f;  // g
        materialAmbient[2] = 0.0745f;  // b
        materialAmbient[3] = 1.0f;    // a

        // diffuse material
        materialDiffuse[0] = 0.75164f; // r
        materialDiffuse[1] = 0.60648f; // g
        materialDiffuse[2] = 0.22648f; // b
        materialDiffuse[3] = 1.0f;    // a

        // specular material
        materialSpecular[0] = 0.628281f; // r;
        materialSpecular[1] = 0.555802f; // g
        materialSpecular[2] = 0.366065f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.4 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 220, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 220, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 6th Sphere 2nd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.19225f; // r
        materialAmbient[1] = 0.19225f; // g
        materialAmbient[2] = 0.19225f; // b
        materialAmbient[3] = 1.0f;    // a

        // diffuse material
        materialDiffuse[0] = 0.50754f; // r
        materialDiffuse[1] = 0.50754f; // g
        materialDiffuse[2] = 0.50754f; // b
        materialDiffuse[3] = 1.0f;    // a

        // specular material
        materialSpecular[0] = 0.508273f; // r
        materialSpecular[1] = 0.508273f; // g
        materialSpecular[2] = 0.508273f; // b
        materialSpecular[3] = 1.0f;     // a

        // shininess
        materialShininess = float(0.4 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 1.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 1.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 1 Sphere 3rd Column
        // transformations
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.0f;  // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.01f; // r
        materialDiffuse[1] = 0.01f; // g
        materialDiffuse[2] = 0.01f; // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.50f; // r
        materialSpecular[1] = 0.50f; // g
        materialSpecular[2] = 0.50f; // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.25 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 1.1, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 2nd Sphere 3rd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.1f;  // g
        materialAmbient[2] = 0.06f; // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.0f;        // r
        materialDiffuse[1] = 0.50980392f; // g
        materialDiffuse[2] = 0.50980392f; // b
        materialDiffuse[3] = 1.0f;       // a

        // specular material
        materialSpecular[0] = 0.50196078f; // r
        materialSpecular[1] = 0.50196078f; // g
        materialSpecular[2] = 0.50196078f; // b
        materialSpecular[3] = 1.0f;       // a

        // shininess
        materialShininess = float(0.25 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);

        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 3rd Sphere 3rd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.0f;  // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.1f;  // r
        materialDiffuse[1] = 0.35f; // g
        materialDiffuse[2] = 0.1f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.45f; // r
        materialSpecular[1] = 0.55f; // g
        materialSpecular[2] = 0.45f; // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.25 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 3.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 3.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 4th Sphere 3rd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.0f;  // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.5f;  // r
        materialDiffuse[1] = 0.0f;  // g
        materialDiffuse[2] = 0.0f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.7f;  // r
        materialSpecular[1] = 0.6f;  // g
        materialSpecular[2] = 0.6f;  // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.25 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 6.5f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 6.5f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 5th Sphere 3rd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.0f;  // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.55f; // r
        materialDiffuse[1] = 0.55f; // g
        materialDiffuse[2] = 0.55f; // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.70f; // r
        materialSpecular[1] = 0.70f; // g
        materialSpecular[2] = 0.70f; // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.25 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 220, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 220, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }

        // 6th Sphere 3rd Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.0f;  // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.5f;  // r
        materialDiffuse[1] = 0.5f;  // g
        materialDiffuse[2] = 0.0f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.60f; // r
        materialSpecular[1] = 0.60f; // g
        materialSpecular[2] = 0.50f; // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.25 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 1.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 1.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }
        // 1st Sphere 4th Column
        // transformations
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.02f; // r
        materialAmbient[1] = 0.02f; // g
        materialAmbient[2] = 0.02f; // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.01f; // r
        materialDiffuse[1] = 0.01f; // g
        materialDiffuse[2] = 0.01f; // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.4f;  // r
        materialSpecular[1] = 0.4f;  // g
        materialSpecular[2] = 0.4f;  // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.078125 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);

        glBindVertexArray(0);

        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }
        // 2nd Sphere 4th Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.05f; // g
        materialAmbient[2] = 0.05f; // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.4f;  // r
        materialDiffuse[1] = 0.5f;  // g
        materialDiffuse[2] = 0.5f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.04f; // r
        materialSpecular[1] = 0.7f;  // g
        materialSpecular[2] = 0.7f;  // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.078125 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }
        // 3rd Sphere 4th Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.0f;  // r
        materialAmbient[1] = 0.05f; // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.4f;  // r
        materialDiffuse[1] = 0.5f;  // g
        materialDiffuse[2] = 0.4f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.04f; // r
        materialSpecular[1] = 0.7f;  // g
        materialSpecular[2] = 0.04f; // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.078125 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 3.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 3.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }
        // 4th Sphere 4th Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.05f; // r
        materialAmbient[1] = 0.0f;  // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.5f;  // r
        materialDiffuse[1] = 0.4f;  // g
        materialDiffuse[2] = 0.4f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.7f;  // r
        materialSpecular[1] = 0.04f; // g
        materialSpecular[2] = 0.04f; // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.078125 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 6.5f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 6.5f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }
        // 5th Sphere 4th Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.05f; // r
        materialAmbient[1] = 0.05f; // g
        materialAmbient[2] = 0.05f; // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.5f;  // r
        materialDiffuse[1] = 0.5f;  // g
        materialDiffuse[2] = 0.5f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.7f;  // r
        materialSpecular[1] = 0.7f;  // g
        materialSpecular[2] = 0.7f;  // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.078125 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);


        if (gbFullScreen == NO)
        {
            glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 220, WIN_WIDTH / 4, WIN_HEIGHT / 4);
        }
        else
        {
            glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 220, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
        }
        // 6th Sphere 4th Column
        translationMatrix = mat4::identity();
        modelMatrix = mat4::identity();
        viewMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelMatrix = translationMatrix; // order is imp

        // ambient material
        materialAmbient[0] = 0.05f; // r
        materialAmbient[1] = 0.05f; // g
        materialAmbient[2] = 0.0f;  // b
        materialAmbient[3] = 1.0f; // a

        // diffuse material
        materialDiffuse[0] = 0.5f;  // r
        materialDiffuse[1] = 0.5f;  // g
        materialDiffuse[2] = 0.4f;  // b
        materialDiffuse[3] = 1.0f; // a

        // specular material
        materialSpecular[0] = 0.7f;  // r
        materialSpecular[1] = 0.7f;  // g
        materialSpecular[2] = 0.04f; // b
        materialSpecular[3] = 1.0f; // a

        // shininess
        materialShininess = float(0.078125 * 128);

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        if (bLight == TRUE)
        {
            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }

    glBindVertexArray(vao_sphere);

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    glBindVertexArray(0);
        
        glUseProgram(0);
}

- (void) myupdate
{
    // code
    gbLightAngleZero = gbLightAngleZero + 0.05f;

        if (gbLightAngleZero >= 360.0f)
            gbLightAngleZero = gbLightAngleZero - 360.0f;
}

- (void) uninitialize
{
    // code
    if (vbo_sphere_position)
        {
            glDeleteBuffers(1, &vbo_sphere_position);
            vbo_sphere_position = 0;
        }

        if (vbo_sphere_normal)
        {
            glDeleteBuffers(1, &vbo_sphere_normal);
            vbo_sphere_normal = 0;
        }

        if (vbo_sphere_elements)
        {
            glDeleteBuffers(1, &vbo_sphere_elements);
            vbo_sphere_elements = 0;
        }

        if (vao_sphere)
        {
            glDeleteVertexArrays(1, &vao_sphere);
            vao_sphere = 0;
        }

        free(sphereData.vertices);
        free(sphereData.normals);
        free(sphereData.textureCoordinates);
        free(sphereData.elements);

        sphereData.vertices = NULL;
        sphereData.normals = NULL;
        sphereData.textureCoordinates = NULL;
        sphereData.elements = NULL;


    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);
        
        GLsizei numAttachedShaders;
        
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
        
        GLuint* shaderObjects = NULL;
        
        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
        
        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);
        
        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }
        
        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);
        
        // stepF 6 : unuse the shader program object
        glUseProgram(0);
        
        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        
        if (depthRenderbuffer)
        {
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            depthRenderbuffer = 0;
        }
        
        if (coloRenderbuffer)
        {
            glDeleteRenderbuffers(1, &coloRenderbuffer);
            coloRenderbuffer = 0;
        }
        
        if (defaultFramebuffer)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            defaultFramebuffer = 0;
        }
        
        if ([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext : nil];
            [EAGLContext release];
            eaglContext = nil;
        }
    }
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
    if (dcounter <= 3)
    {
        dcounter++;
    }
    else{
        dcounter = 0;
    }
}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
            if (bLight == NO)
            {
                bLight = YES;
            }
            else
            {
                bLight = NO;
            }
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
}

- (void) dealloc
{
    [self uninitialize];
    
    if (displayLink)
    {
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

@end
