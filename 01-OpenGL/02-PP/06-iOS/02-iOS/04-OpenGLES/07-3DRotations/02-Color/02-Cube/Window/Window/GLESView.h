//
//  MyView.h
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

- (void) startAnimation;

- (void) stopAnimation;

@end
