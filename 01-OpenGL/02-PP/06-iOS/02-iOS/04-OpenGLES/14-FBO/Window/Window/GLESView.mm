//
//  MyView.m
//  Window
//
//  Created by user226054 on 12/25/22.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#import "Sphere.h"
using namespace vmath;

@implementation GLESView
{
    @private
    EAGLContext* eaglContext; 
    
    GLuint DefaultFramebuffer;
    GLuint ColoRenderbuffer;
    GLuint DepthRenderbuffer;
    
    CADisplayLink* displayLink; 
    GLint fps;
    BOOL isAnimating;
    
    // Programmable Pipeline related global variables
    GLuint shaderProgramObject;
    enum
    {
        AMC_ATTRIBUTE_POSITION = 0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    GLuint vao_cube;
    GLuint vbo_cube_position;
    GLuint vbo_cube_texcoords;

    // FBO related Variables
    GLuint fbo;
    GLuint rbo;
    GLuint fbo_texture;
    bool pFBOResult;
    GLuint FBO_WIDTH;
    GLuint FBO_HEIGHT;
    int winWidth;
    int winHeight;
    
    GLfloat angleCube;

    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;
    GLuint textureSamplerUniform;
    mat4 perspectiveProjectionMatrix;
    
    // Texture Data
    GLuint shaderProgramObject_pf;
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_elements;
            
    SphereData sphereData;
    int doubleTap;
    
        GLuint modelMatrixUniform_pf;
        GLuint viewMatrixUniform_pf;
        GLuint projectionMatrixUniform_pf;

        //Lights
        GLuint laUniform_pf[3]; //light ambient
        GLuint ldUniform_pf[3]; // light diffuse
        GLuint lsUniform_pf[3]; // light specular
        GLuint lightPositionUniform_pf[3];

        GLuint kaUniform_pf;
        GLuint kdUniform_pf;
        GLuint ksUniform_pf;
        GLuint materialShininessUniform_pf;

        GLuint lightingEnabledUniform_pf;

        BOOL bLight;
        GLfloat angleLight;
        

        struct Light
        {
            vec4 lightAmbient;
            vec4 lightDiffuse;
            vec4 lightSpecular;
            vec4 lightPosition;
        };
        Light lights[3];

        GLfloat materialAmbient[4];
        GLfloat materialDiffuse[4];
        GLfloat materialSpecular[4];
        GLfloat materialShininess;
        
        mat4 perspectiveProjectionMatrix_pf;
    
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("Failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        [EAGLContext setCurrentContext : eaglContext];
        
        printf("OpenGL Vendor: %s\n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
        printf("OpenGL Version: %s\n", glGetString(GL_VERSION));
        
        glGenFramebuffers(1, &DefaultFramebuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, DefaultFramebuffer);
        
        glGenRenderbuffers(1, &ColoRenderbuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, ColoRenderbuffer);
        
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, ColoRenderbuffer);
        
        GLint backingWidth;
        
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &DepthRenderbuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, DepthRenderbuffer);
        
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, DepthRenderbuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        int result = [self initialize];
        if(result==0)
        {
            printf("initialise Succcesfull\n");
        }
        
        fps = 60; 
        isAnimating = NO;
        
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGetsureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        
        [self addGestureRecognizer:swipeGetsureRecognizer];
        
        UILongPressGestureRecognizer *longPressGetsureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGetsureRecognizer];
    }
    return(self);
}

+ (Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, ColoRenderbuffer);
    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];

    GLint backingWidth;
    
    GLint backingHeight;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    glGenRenderbuffers(1, &DepthRenderbuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, DepthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, DepthRenderbuffer);
    
    [self resize : backingWidth : backingHeight];
    
    /*if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }*/
    
    [self drawView : nil];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, DefaultFramebuffer);
    
    [self display];
    [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, ColoRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        [displayLink setPreferredFramesPerSecond : fps];
        
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    GLint status;
        GLint infoLogLength;
        char* log = NULL;
    
	// Vertex Shader
        const GLchar* vertexShaderSourceCode =
            "#version 300 es" \
            "\n" \
            "in vec4 a_position;" \
            "in vec2 a_texcoord;" \
            "uniform mat4 u_modelMatrix;" \
            "uniform mat4 u_viewMatrix;" \
            "uniform mat4 u_projectionMatrix;" \
            "out vec2 a_texcoord_out;" \
            "void main(void)" \
            "{" \
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix*a_position;" \
            "a_texcoord_out=a_texcoord;" \
            "}";

        GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

        glCompileShader(vertexShaderObject);
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
                    printf("Vertex Shader Compilation log : %s\n", log);

                    free(log);
                  
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Fragment Shader
        const GLchar* fragmentShaderSourceCode =
            "#version 300 es" \
            "\n" \
            "precision highp float;"
            "in vec2 a_texcoord_out;" \
            "uniform sampler2D u_textureSampler;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
            "FragColor = texture(u_textureSampler,a_texcoord_out);" \
            "}";

        GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

        glCompileShader(fragmentShaderObject);

        status=0;
        infoLogLength=0;
        log = NULL;
      
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
                   
                    printf("Fragment Shader Compilation log : %s\n", log);
                    
                    free(log);
                 
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        shaderProgramObject = glCreateProgram();
        
        glAttachShader(shaderProgramObject, vertexShaderObject);
        glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre-link Binding
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

        glLinkProgram(shaderProgramObject);

        status = 0;
        infoLogLength = 0;
        log = NULL;

        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
                    
                    printf("Shader Program Link log : %s\n", log);
                    
                    free(log);
                 
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Post Linking
        modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
        viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
        projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
        textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_textureSampler");
        
	  // Declartion of Vertex data arrays
        const GLfloat cubePosition[] = {
            // top
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,

            // Bottom
            1.0f, -1.0f, -1.0f,
           -1.0f, -1.0f, -1.0f,
           -1.0f, -1.0f,  1.0f,
            1.0f, -1.0f,  1.0f,

            // front
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,

            // back
            1.0f, 1.0f, -1.0f,
           -1.0f, 1.0f, -1.0f,
           -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,

            // right
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,

            // left
            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f
        };

        const GLfloat cubeTexcoord[] = {
            0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 0.0f,

                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 0.0f,

                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 0.0f,

                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 0.0f,

                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 0.0f,

                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 0.0f
        };

        // Cube
        // vao related code
        glGenVertexArrays(1, &vao_cube);
        glBindVertexArray(vao_cube);
        
	  // vbo for Position
        glGenBuffers(1, &vbo_cube_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);

        glBufferData(GL_ARRAY_BUFFER, sizeof(cubePosition), cubePosition, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
   
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // Cube Texcoords

        // vbo for Texcoords
        glGenBuffers(1, &vbo_cube_texcoords);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texcoords);

        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoord), cubeTexcoord, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
      
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        // Depth related changes
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        // Clear the screen
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        
	  // Enabling Texture
        glEnable(GL_TEXTURE_2D);
        
    
        perspectiveProjectionMatrix = mat4::identity();
   
    angleCube=0.0f;
    FBO_WIDTH=512;
    FBO_HEIGHT=512;
    pFBOResult = FALSE;
    
        int iRetVal;
        // fbo code
        pFBOResult = [self createFBO:FBO_WIDTH : FBO_HEIGHT];
        if (pFBOResult == true)
        {
            iRetVal = [self initialize_Sphere:FBO_WIDTH :FBO_HEIGHT];
            // Here you should do error checking
        }
        else
        {
            printf("createFBO Failed\n");
        }
    
    return(0);
}
-(int) initialize_Sphere:(int)FBO_WIDTH :(int)FBO_HEIGHT
{
    GLint status;
    GLint infoLogLength;
    char* log = NULL;
   
            // Vertex Shader
            const GLchar* vertexShaderSourceCode_pf =
                "#version 300 es" \
                "\n" \
                "in vec4 a_position;" \
                "in vec3 a_normal;" \
                "uniform mat4 u_modelMatrix;" \
                "uniform mat4 u_viewMatrix;" \
                "uniform mat4 u_projectionMatrix;" \
                "uniform vec4 u_lightPosition[3];" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec3 transformedNormals;" \
                "out vec3 lightDirection[3];" \
                "out vec3 viewerVector;" \
                "void main(void)" \
                "{" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec4 eyeCoordinates = u_viewMatrix*u_modelMatrix*a_position;" \
                "mat3 normalMatrix=mat3(u_viewMatrix*u_modelMatrix);" \
                "transformedNormals=normalize(normalMatrix*a_normal);" \
                "viewerVector=normalize(-eyeCoordinates.xyz);" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "lightDirection[i]=normalize(vec3(u_lightPosition[i])-eyeCoordinates.xyz);" \
                "}" \
                "}" \
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
                "}";

            GLuint vertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

            glShaderSource(vertexShaderObject_pf, 1, (const GLchar**)&vertexShaderSourceCode_pf, NULL);

            glCompileShader(vertexShaderObject_pf);

            glGetShaderiv(vertexShaderObject_pf, GL_COMPILE_STATUS, &status);

            if (status == GL_FALSE)
            {
                glGetShaderiv(vertexShaderObject_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
                if (infoLogLength > 0)
                {
                    log = (char*)malloc(infoLogLength);
                    if (log != NULL)
                    {
                        GLsizei written;
                        glGetShaderInfoLog(vertexShaderObject_pf, infoLogLength, &written, log);

                        printf("Vertex Shader Compilation log :\n");

                        free(log);
                 
                        [self uninitialize_Sphere];
                        [self release];
                        exit(0);
                    }
                }
            }

            // Fragment Shader
            const GLchar* fragmentShaderSourceCode_pf =
                "#version 300 es" \
                "\n" \
                "precision highp float;" \
                "in vec3 transformedNormals;" \
                "in vec3 lightDirection[3];" \
                "in vec3 viewerVector;" \
                "uniform vec3 u_la[3];" \
                "uniform vec3 u_ld[3];" \
                "uniform vec3 u_ls[3];" \
                "uniform vec3 u_ka;" \
                "uniform vec3 u_kd;" \
                "uniform vec3 u_ks;" \
                "uniform float u_materialShininess;" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec4 FragColor;" \
                "void main(void)" \
                "{" \
                "vec3 phong_ads_light;" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec3 normalised_transformed_normals=normalize(transformedNormals);" \
                "vec3 normalised_viewerVector=normalize(viewerVector);" \
                "vec3 ambient[3];" \
                "vec3 diffuse[3];" \
                "vec3 reflectionVector[3];" \
                "vec3 specular[3];" \
                "vec3 total[3];" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "ambient[i] = u_la[i] * u_ka; " \
                "diffuse[i]=u_ld[i]*u_kd*max(dot(lightDirection[i],normalised_transformed_normals),0.0);" \
                "reflectionVector[i]=reflect(-lightDirection[i],normalised_transformed_normals);" \
                "specular[i]=u_ls[i]*u_ks*pow(max(dot(reflectionVector[i],normalised_viewerVector),0.0),u_materialShininess);" \
                "total[i] = ambient[i] + diffuse[i] + specular[i];" \
                "}" \
                "phong_ads_light = total[0] + total[1]+total[2];" \
                "}" \
                "else" \
                "{"
                "phong_ads_light=vec3(1.0,1.0,1.0);" \
                "}"
                "FragColor = vec4(phong_ads_light,1.0);" \
                "}";

            GLuint fragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

            glShaderSource(fragmentShaderObject_pf, 1, (const GLchar**)&fragmentShaderSourceCode_pf, NULL);

            glCompileShader(fragmentShaderObject_pf);

            status = 0;
            infoLogLength = 0;
            log = NULL;

            glGetShaderiv(fragmentShaderObject_pf, GL_COMPILE_STATUS, &status);

            if (status == GL_FALSE)
            {
                glGetShaderiv(fragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
                if (infoLogLength > 0)
                {
                    log = (char*)malloc(infoLogLength);
                    if (log != NULL)
                    {
                        GLsizei written;
                        glGetShaderInfoLog(fragmentShaderObject_pf, infoLogLength, &written, log);
                   
                        printf("Fragment Shader Compilation log : \n");
                  
                        free(log);
                      
                        [self uninitialize_Sphere];
                        [self release];
                        exit(0);
                    }
                }
            }

            shaderProgramObject_pf = glCreateProgram();

            glAttachShader(shaderProgramObject_pf, vertexShaderObject_pf);
            glAttachShader(shaderProgramObject_pf, fragmentShaderObject_pf);

            // Pre-link Binding
            glBindAttribLocation(shaderProgramObject_pf, AMC_ATTRIBUTE_POSITION, "a_position");
            glBindAttribLocation(shaderProgramObject_pf, AMC_ATTRIBUTE_NORMAL, "a_normal");

            glLinkProgram(shaderProgramObject_pf);

            status = 0;
            infoLogLength = 0;
            log = NULL;

            glGetProgramiv(shaderProgramObject_pf, GL_LINK_STATUS, &status);
            if (status == GL_FALSE)
            {
                glGetProgramiv(shaderProgramObject_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
                if (infoLogLength > 0)
                {
                    log = (char*)malloc(infoLogLength);
                    if (log != NULL)
                    {
                        GLsizei written;
                        glGetProgramInfoLog(shaderProgramObject_pf, infoLogLength, &written, log);

                        printf("Shader Program Link log :\n");

                        free(log);

                        [self uninitialize_Sphere];
                        [self release];
                        exit(0);
                    }
                }
            }

            // Post Linking
            modelMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_modelMatrix");
            viewMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_viewMatrix");
            projectionMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_projectionMatrix");

            laUniform_pf[0] = glGetUniformLocation(shaderProgramObject_pf, "u_la[0]");
            ldUniform_pf[0] = glGetUniformLocation(shaderProgramObject_pf, "u_ld[0]");
            lsUniform_pf[0] = glGetUniformLocation(shaderProgramObject_pf, "u_ls[0]");
            lightPositionUniform_pf[0] = glGetUniformLocation(shaderProgramObject_pf, "u_lightPosition[0]");

            laUniform_pf[1] = glGetUniformLocation(shaderProgramObject_pf, "u_la[1]");
            ldUniform_pf[1] = glGetUniformLocation(shaderProgramObject_pf, "u_ld[1]");
            lsUniform_pf[1] = glGetUniformLocation(shaderProgramObject_pf, "u_ls[1]");
            lightPositionUniform_pf[1] = glGetUniformLocation(shaderProgramObject_pf, "u_lightPosition[1]");

            laUniform_pf[2] = glGetUniformLocation(shaderProgramObject_pf, "u_la[2]");
            ldUniform_pf[2] = glGetUniformLocation(shaderProgramObject_pf, "u_ld[2]");
            lsUniform_pf[2] = glGetUniformLocation(shaderProgramObject_pf, "u_ls[2]");
            lightPositionUniform_pf[2] = glGetUniformLocation(shaderProgramObject_pf, "u_lightPosition[2]");

            kaUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_ka");
            kdUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_kd");
            ksUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_ks");
            materialShininessUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_materialShininess");

            lightingEnabledUniform_pf = glGetUniformLocation(shaderProgramObject_pf, "u_lightingEnabled");

        	   // Declaration of Vertex data arrays
               Sphere *sphere = new Sphere();
               sphere->getSphereVertexData(&sphereData);

               delete sphere;
               sphere = NULL;
           
               // vao related code
               glGenVertexArrays(1, &vao_sphere);
               glBindVertexArray(vao_sphere);
              
               glGenBuffers(1, &vbo_sphere_position);
               glBindBuffer(GL_ARRAY_BUFFER,vbo_sphere_position);

               glBufferData(GL_ARRAY_BUFFER, sizeof(float)*1146, sphereData.vertices, GL_STATIC_DRAW);
               glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
           
               glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

               glBindBuffer(GL_ARRAY_BUFFER, 0);
               
               // vbo related code
               glGenBuffers(1, &vbo_sphere_normal);
               glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
               glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.normals, GL_STATIC_DRAW);

               glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

               glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

               glBindBuffer(GL_ARRAY_BUFFER, 0);

               // vbo elements
               glGenBuffers(1, &vbo_sphere_elements);
               glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
               glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * 2280, sphereData.elements, GL_STATIC_DRAW);
               glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
           

               glBindVertexArray(0);
            
            // Depth related changes
            glClearDepthf(1.0f);
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LEQUAL);

            // Clear the screen
            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
 
        angleLight=0.0f;
            bLight=NO;
        doubleTap=0;

		// Light 1
            lights[0].lightAmbient[0]=0.0f;
            lights[0].lightAmbient[1]=0.0f;
            lights[0].lightAmbient[2]=0.0f;
            lights[0].lightAmbient[3]=1.0f;
            
            lights[0].lightDiffuse[0]=1.0f;
            lights[0].lightDiffuse[1]=0.0f;
            lights[0].lightDiffuse[2]=0.0f;
            lights[0].lightDiffuse[3]=1.0f;
            
            lights[0].lightSpecular[0]=1.0f;
            lights[0].lightSpecular[1]=0.0f;
            lights[0].lightSpecular[2]=0.0f;
            lights[0].lightSpecular[3]=1.0f;
            
            lights[0].lightPosition[0]=0.0f;
            lights[0].lightPosition[1]=0.0f;
            lights[0].lightPosition[2]=0.0f;
            lights[0].lightPosition[3]=1.0f;
            
            // Light 2
            lights[1].lightAmbient[0]=0.0f;
            lights[1].lightAmbient[1]=0.0f;
            lights[1].lightAmbient[2]=0.0f;
            lights[1].lightAmbient[3]=1.0f;
            
            lights[1].lightDiffuse[0]=0.0f;
            lights[1].lightDiffuse[1]=1.0f;
            lights[1].lightDiffuse[2]=0.0f;
            lights[1].lightDiffuse[3]=1.0f;
            
            lights[1].lightSpecular[0]=0.0f;
            lights[1].lightSpecular[1]=1.0f;
            lights[1].lightSpecular[2]=0.0f;
            lights[1].lightSpecular[3]=1.0f;
            
            lights[1].lightPosition[0]=0.0f;
            lights[1].lightPosition[1]=0.0f;
            lights[1].lightPosition[2]=0.0f;
            lights[1].lightPosition[3]=1.0f;
        
        // Light 3
        lights[2].lightAmbient[0]=0.0f;
        lights[2].lightAmbient[1]=0.0f;
        lights[2].lightAmbient[2]=0.0f;
        lights[2].lightAmbient[3]=1.0f;
        
        lights[2].lightDiffuse[0]=0.0f;
        lights[2].lightDiffuse[1]=0.0f;
        lights[2].lightDiffuse[2]=1.0f;
        lights[2].lightDiffuse[3]=1.0f;
        
        lights[2].lightSpecular[0]=0.0f;
        lights[2].lightSpecular[1]=0.0f;
        lights[2].lightSpecular[2]=1.0f;
        lights[2].lightSpecular[3]=1.0f;
        
        lights[2].lightPosition[0]=0.0f;
        lights[2].lightPosition[1]=0.0f;
        lights[2].lightPosition[2]=0.0f;
        lights[2].lightPosition[3]=1.0f;
        
        materialAmbient[0] =0.0f;
        materialAmbient[1] =0.0f;
        materialAmbient[2] =0.0f;
        materialAmbient[3] =1.0f;
        
        materialDiffuse[0] =1.0f;
        materialDiffuse[1] =1.0f;
        materialDiffuse[2] =1.0f;
        materialDiffuse[3] =1.0f;
        
        materialSpecular[0] =1.0f;
        materialSpecular[1] =1.0f;
        materialSpecular[2] =1.0f;
        materialSpecular[3] =1.0f;
        
        materialShininess = 50.0f;
        
            perspectiveProjectionMatrix_pf = mat4::identity();
       
    [self resize_Sphere:FBO_WIDTH :FBO_HEIGHT];
        
        return(0);
    
}
-(bool) createFBO: (GLint) textureWidth : (GLint) textureHeight
{
    // code
    int maxRenderbufferSize;
    glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE,&maxRenderbufferSize);
    if (maxRenderbufferSize < textureWidth || maxRenderbufferSize < textureHeight)
    {
        printf("Insufficient RenderBuffersize\n");
        return false;
    }
   
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);

    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, textureWidth, textureHeight);

    glGenTextures(1, &fbo_texture);
    glBindTexture(GL_TEXTURE_2D, fbo_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, NULL);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);

    GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (result != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Frame Buffer is not complete\n");
        return false;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return true;
}
- (void) resize : (int)width : (int)height
{
    winWidth = width;
        winHeight = height;
    if (height == 0) 
            height = 1;
        glViewport(0, 0, width, height);

        perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width /(GLfloat)height, 0.1f, 100.0f);
    
}
- (void) resize_Sphere : (int)width : (int)height
{
    if (height == 0) 
            height = 1;
        glViewport(0, 0, width, height);

        perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width /(GLfloat)height, 0.1f, 100.0f);
    
}
-(void) display_Sphere:(GLint)textureWidth :(GLint)textureHeight
{
    
            glBindFramebuffer(GL_FRAMEBUFFER, fbo);
            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            [self resize_Sphere:textureWidth :textureHeight];
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glUseProgram(shaderProgramObject_pf);
            
            // Transformations
            mat4 translationMatrix = mat4::identity();
            mat4 modelMatrix = mat4::identity();
            mat4 viewMatrix = mat4::identity();
            
            translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
            modelMatrix = translationMatrix;
            
            glUniformMatrix4fv(modelMatrixUniform_pf, 1, GL_FALSE, modelMatrix);
            glUniformMatrix4fv(viewMatrixUniform_pf, 1, GL_FALSE, viewMatrix);
            glUniformMatrix4fv(projectionMatrixUniform_pf, 1, GL_FALSE, perspectiveProjectionMatrix);
            
                    if(bLight==YES)
                    {
                        glUniform1i(lightingEnabledUniform_pf, 1); 
                        
                        glUniform3fv(laUniform_pf[0], 1, lights[0].lightAmbient);
                        glUniform3fv(ldUniform_pf[0], 1, lights[0].lightDiffuse);
                        glUniform3fv(lsUniform_pf[0], 1, lights[0].lightSpecular);
                        glUniform4fv(lightPositionUniform_pf[0], 1, vmath::vec4(0.0f, 80*sin(angleLight), 80*cos(angleLight), 1.0f));
                        
                        glUniform3fv(laUniform_pf[1], 1, lights[1].lightAmbient);
                        glUniform3fv(ldUniform_pf[1], 1, lights[1].lightDiffuse);
                        glUniform3fv(lsUniform_pf[1], 1, lights[1].lightSpecular);
                        glUniform4fv(lightPositionUniform_pf[1], 1, vmath::vec4(80*sin(angleLight),0.0f, 80*cos(angleLight), 1.0f));
                        
                        glUniform3fv(laUniform_pf[2], 1, lights[2].lightAmbient);
                        glUniform3fv(ldUniform_pf[2], 1, lights[2].lightDiffuse);
                        glUniform3fv(lsUniform_pf[2], 1, lights[2].lightSpecular);
                        glUniform4fv(lightPositionUniform_pf[2], 1, vmath::vec4(80*sin(angleLight), 80*cos(angleLight),0.0f, 1.0f));
                        
                        glUniform3fv(kaUniform_pf, 1, materialAmbient);
                        glUniform3fv(kdUniform_pf, 1, materialDiffuse);
                        glUniform3fv(ksUniform_pf, 1, materialSpecular);
                        
                        glUniform1f(materialShininessUniform_pf, materialShininess);
                    }
                    else
                    {
                        glUniform1i(lightingEnabledUniform_pf, 0);
                    }
           
            glBindVertexArray(vao_sphere);
            
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
            glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
            
            glBindVertexArray(0);
       
                glUseProgram(0);
    		   // glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
-(void) update_Sphere
{
    // code
    angleLight = angleLight + 0.05f;
    if (angleLight >= 360.0f)
    {
        angleLight = angleLight - 360.0f;
    }
}
- (void) display
{
    // code
    if (pFBOResult == true)
        {
            
            [self display_Sphere:FBO_WIDTH : FBO_HEIGHT];
            [self update_Sphere];
        }
        
        glBindFramebuffer(GL_FRAMEBUFFER, DefaultFramebuffer);
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        [self resize:winWidth :winHeight];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shaderProgramObject);

        // Cube
        // Transformations
         mat4 translationMatrix = mat4::identity();
         mat4 scaleMatrix= mat4::identity();
         mat4 rotationMatrix_x = mat4::identity();
         mat4 rotationMatrix_y = mat4::identity();
         mat4 rotationMatrix_z = mat4::identity();
         mat4 rotationMatrix = mat4::identity();
         mat4 modelMatrix = mat4::identity();
         mat4 viewMatrix = mat4::identity();

        translationMatrix = ::translate(0.0f, 0.0f, -5.0f);
        scaleMatrix = ::scale(0.75f, 0.75f, 0.75f);
        rotationMatrix_x = vmath::rotate(angleCube, 1.0f, 0.0f, 0.0f);
        rotationMatrix_y= vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
        rotationMatrix_z= vmath::rotate(angleCube, 0.0f, 0.0f, 1.0f);
        rotationMatrix = rotationMatrix_x * rotationMatrix_y * rotationMatrix_z;
        modelMatrix = translationMatrix*scaleMatrix*rotationMatrix; 

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fbo_texture);
        glUniform1i(textureSamplerUniform, 0);
        glBindVertexArray(vao_cube);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        glUseProgram(0);
}

- (void) myupdate
{
    // code
    
        angleCube = angleCube + 1.0f;
        if (angleCube >= 360)
        {
            angleCube = angleCube - 360.0f;
        }
}

- (void) uninitialize
{
       // code
       // Deletion and uninitialzation of vbo
        
        if (vbo_cube_position)
        {
            glDeleteBuffers(1, &vbo_cube_position);
            vbo_cube_position = 0;
        }
        if (vbo_cube_texcoords)
        {
            glDeleteBuffers(1, &vbo_cube_texcoords);
            vbo_cube_texcoords = 0;
        }

        // Deletion and unintialization of vao
        if (vao_cube)
        {
            glDeleteVertexArrays(1, &vao_cube);
            vao_cube = 0;
        }

        if (shaderProgramObject)
        {
            glUseProgram(shaderProgramObject);
            GLsizei numAttachedShaders;

            glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
            GLuint* shaderObjects = NULL;

            shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

            glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

            for (GLsizei i = 0; i < numAttachedShaders; i++)
            {
                glDetachShader(shaderProgramObject, shaderObjects[i]);
                glDeleteShader(shaderObjects[i]);
                shaderObjects[i] = 0;
            }
 
            free(shaderObjects);
            shaderObjects = NULL;
            glUseProgram(0);
            glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    if (DepthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &DepthRenderbuffer);
        DepthRenderbuffer = 0;
    }
    if (ColoRenderbuffer)
    {
        glDeleteRenderbuffers(1, &ColoRenderbuffer);
        ColoRenderbuffer = 0;
    }
    if (DefaultFramebuffer)
    {
        glDeleteFramebuffers(1, &DefaultFramebuffer);
        DefaultFramebuffer = 0;
    }
    
    if ([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext : nil];
        [EAGLContext release];
        eaglContext = nil;
    }
    
}
- (void) uninitialize_Sphere
{
    // code
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    
    if (vbo_sphere_elements)
    {
        glDeleteBuffers(1, &vbo_sphere_elements);
        vbo_sphere_elements = 0;
    }
    
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    
    free(sphereData.vertices);
    free(sphereData.normals);
    free(sphereData.textureCoordinates);
    free(sphereData.elements);
    
    sphereData.vertices = NULL;
    sphereData.normals = NULL;
    sphereData.textureCoordinates = NULL;
    sphereData.elements = NULL;

    if (shaderProgramObject_pf)
    {
        glUseProgram(shaderProgramObject_pf);
        GLsizei numAttachedShaders;

        glGetProgramiv(shaderProgramObject_pf, GL_ATTACHED_SHADERS, &numAttachedShaders);
        GLuint* shaderObjects = NULL;

        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

        glGetAttachedShaders(shaderProgramObject_pf, numAttachedShaders, &numAttachedShaders, shaderObjects);

        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject_pf, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }
  
        free(shaderObjects);
        shaderObjects = NULL;
        glUseProgram(0);
        glDeleteProgram(shaderProgramObject_pf);
        shaderProgramObject_pf = 0;
    }
}
- (BOOL) acceptsFirstResponder
{
    return YES;
}
- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    //code
}
- (void) onSingleTap:(UITapGestureRecognizer*)tgr
{
    //code
    
    
}
- (void) onDoubleTap:(UITapGestureRecognizer*)tgr
{
    //code
    doubleTap++;
            if(doubleTap > 1)
            {
                bLight = NO;
                doubleTap = 0;
            }
            else
            {
                bLight = YES;
            }
   
}
- (void) onSwipe:(UISwipeGestureRecognizer*)sgr
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}
- (void) onLongPress:(UILongPressGestureRecognizer*)lpgr
{
    // code
    
}
- (void) dealloc
{
    [self uninitialize];
    if(displayLink)
    {
        [displayLink release];
        displayLink=nil;
    }
    [super dealloc];
}
@end

