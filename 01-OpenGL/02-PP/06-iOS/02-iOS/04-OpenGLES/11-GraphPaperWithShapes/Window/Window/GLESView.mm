//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
using namespace vmath;

@implementation GLESView
{
    @private
    EAGLContext* eaglContext; // embedded apple graphics library
    
    GLuint defaultFramebuffer;
    GLuint coloRenderbuffer;
    GLuint depthRenderbuffer;
    
    CADisplayLink* displayLink; // CA for Core Animation
    GLint framesPerSecond;
    BOOL isAnimating;
    
    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    // variables for X Axis Line
    GLuint vao_horizontalLine;
    GLuint vbo_horizontalLine_position;
    GLuint vbo_horizontalLine_color;

    // variables for Y Axis Line
    GLuint vao_verticalLine;
    GLuint vbo_verticalLine_position;
    GLuint vbo_verticalLine_color;

    // variables for Horizontal Graph Lines
    GLuint vao_graphHLines;
    GLuint vbo_graphHLines_position;
    GLuint vbo_graphHLines_color;

    // variables for Triangle Lines
    GLuint vao_triangleLines;
    GLuint vbo_triangleLines_position;
    GLuint vbo_triangleLines_color;

    // variables for Square Lines
    GLuint vao_squareLines;
    GLuint vbo_squareLines_position;
    GLuint vbo_squareLines_color;

    // variables for Square Lines
    GLuint vao_circleLines;
    GLuint vbo_circleLines_position;
    GLuint vbo_circleLines_color;

    // variables for Vertical Graph Lines
    GLuint vao_graphVLines;
    GLuint vbo_graphVLines_position;
    
    GLuint mvpMatrixUniform;

    mat4 perspectiveProjectionMatrix;
    
    float base;
    float height1;
    float base1;

    BOOL gFlag;
    BOOL tFlag;
    BOOL sFlag;
    BOOL cFlag;

    GLfloat circleLine[101][3];
    
    int longPress;
    int doubleTap;
    int singleTap;
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // 1) Create drawable surface which is called as layer
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        // 2) Set EAGLLayer properties (this is similar to setting pixelformatdistribute properties)
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        // 3) Create eagl context
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // 4) Set this context as current context
        [EAGLContext setCurrentContext : eaglContext];
        
        // 5) Print OpenGLES info
        printf("OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
        printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
        printf("OpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // 6) Create frame buffer
        glGenFramebuffers(1, &defaultFramebuffer);
        
        // 7) Bind With Above frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // 8) Create color render buffer
        glGenRenderbuffers(1, &coloRenderbuffer);
        
        // 9) Bind with above color render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
        
        // 10) Provide storage to render buffer
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        // 11) Set frame buffer according render buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, coloRenderbuffer);
        
        // 12) Declare backing width
        GLint backingWidth;
        
        // 13) Declare backing height
        GLint backingHeight;
        
        // 14) Get Color render buffer width into backing width
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        // 15) Get Color render buffer height into backing height
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        // 16) Create depth buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        
        // 17) bind above depth render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        // 18) Provide storage to depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        // 19) Get
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // 20) check the status of frame buffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // call initialize
        int result = [self initialize];
        
        if(result!=0)
        {
            printf("Result Not Obtained \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        
        framesPerSecond = 60; // this is started from iOS 8.0
        isAnimating = NO;
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

+ (Class)layerClass
{
    // code
    
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);

    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];
    
    // 12) Declare backing width
    GLint backingWidth;
    
    // 13) Declare backing height
    GLint backingHeight;
    
    // 14) Get Color render buffer width into backing width
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    // 15) Get Color render buffer height into backing height
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    // 16) Create depth buffer
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    // 17) bind above depth render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    // 18) Provide storage to depth render buffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    // 19) Get
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    // call our resize here
    [self resize : backingWidth : backingHeight];
    
    // 20) check the status of frame buffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }
    
    // call drawView after resize
    [self drawView : nil]; // or [self drawView : self];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    // call our display and update here
    [self display];
    // [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        // create display link
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        // set frames per second for the display link
        [displayLink setPreferredFramesPerSecond : framesPerSecond];
        
        // add this display link to run loop
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        // remove display link from run loop
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    // code
    const GLchar* vertexShaderSourceCode =
            "#version 300 core" \
            "\n" \
            "in vec4 a_position;" \
            "in vec4 a_color;" \
            "uniform mat4 u_mvpMatrix;" \
            "out vec4 a_color_out;" \
            "void main(void)" \
            "{" \
                "gl_Position = u_mvpMatrix * a_position;" \
                "a_color_out = a_color;" \
            "}";

        // StepC 2 : creating shading object
        GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                    // stepC 5 e : display the log
                    printf("Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode =
            "#version 300 core" \
            "\n" \
            "precision highp float;" \
            "in vec4 a_color_out;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
                "FragColor = a_color_out;" \
            "}";

        // StepC 2 : creating shading object
        GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    printf("Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "a_color");

        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    printf("Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);

                }
            }
        }

        mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

        // vao & vbo related code
        //declarations of vertex data arrays
    const GLfloat horizontalLine[] =
        {
            1.0f,0.0f,0.0f,
            -1.0,0.0f,0.0f
        };

        const GLfloat horizontalLineColor[] =
        {
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0,
            1.0f, 0.0f, 0.0f
        };

        const GLfloat verticalLine[] =
        {
            0.0f,1.0f,0.0f,
            0.0,-1.0f,0.0f
        };

        const GLfloat verticalLineColor[] =
        {
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0,
            0.0f, 1.0f, 0.0f
        };

        const GLfloat graphHLines[] =
        {
            -1.0f, 1.0f, 0.0f, // 1
            1.0f, 1.0f, 0.0f,

            -1.0f, 0.95f, 0.0f, // 2
            1.0f, 0.95f, 0.0f,

            -1.0f, 0.90f, 0.0f, // 3
            1.0f, 0.90f, 0.0f,

            -1.0f, 0.85f, 0.0f, // 4
            1.0f, 0.85f, 0.0f,

            -1.0f, 0.80f, 0.0f, // 5
            1.0f, 0.80f, 0.0f,

            -1.0f, 0.75f, 0.0f, // 6
            1.0f, 0.75f, 0.0f,

            -1.0f, 0.70f, 0.0f, // 7
            1.0f, 0.70f, 0.0f,

            -1.0f, 0.65f, 0.0f, // 8
            1.0f, 0.65f, 0.0f,

            -1.0f, 0.60f, 0.0f, // 9
            1.0f, 0.60f, 0.0f,

            -1.0f, 0.55f, 0.0f, // 10
            1.0f, 0.55f, 0.0f,

            -1.0f, 0.50f, 0.0f, // 11
            1.0f, 0.50f, 0.0f,

            -1.0f, 0.45f, 0.0f, // 12
            1.0f, 0.45f, 0.0f,

            -1.0f, 0.40f, 0.0f, // 13
            1.0f, 0.40f, 0.0f,

            -1.0f, 0.35f, 0.0f, // 14
            1.0f, 0.35f, 0.0f,

            -1.0f, 0.30f, 0.0f, // 15
            1.0f, 0.30f, 0.0f,

            -1.0f, 0.25f, 0.0f, // 16
            1.0f, 0.25f, 0.0f,

            -1.0f, 0.20f, 0.0f, // 17
            1.0f, 0.20f, 0.0f,

            -1.0f, 0.15f, 0.0f, // 18
            1.0f, 0.15f, 0.0f,

            -1.0f, 0.10f, 0.0f, // 19
            1.0f, 0.10f, 0.0f,

            -1.0f, 0.05f, 0.0f, // 20
            1.0f, 0.05f, 0.0f,

            -1.0f, -1.0f, 0.0f, // 21
            1.0f, -1.0f, 0.0f,

            -1.0f, -0.95f, 0.0f, // 22
            1.0f, -0.95f, 0.0f,

            -1.0f, -0.90f, 0.0f, // 23
            1.0f, -0.90f, 0.0f,

            -1.0f, -0.85f, 0.0f, // 24
            1.0f, -0.85f, 0.0f,

            -1.0f, -0.80f, 0.0f, // 25
            1.0f, -0.80f, 0.0f,

            -1.0f, -0.75f, 0.0f, // 26
            1.0f, -0.75f, 0.0f,

            -1.0f, -0.70f, 0.0f, // 27
            1.0f, -0.70f, 0.0f,

            -1.0f, -0.65f, 0.0f, // 28
            1.0f, -0.65f, 0.0f,

            -1.0f, -0.60f, 0.0f, // 29
            1.0f, -0.60f, 0.0f,

            -1.0f, -0.55f, 0.0f, // 30
            1.0f, -0.55f, 0.0f,

            -1.0f, -0.50f, 0.0f, // 31
            1.0f, -0.50f, 0.0f,

            -1.0f, -0.45f, 0.0f, // 32
            1.0f, -0.45f, 0.0f,

            -1.0f, -0.40f, 0.0f, // 33
            1.0f, -0.40f, 0.0f,

            -1.0f, -0.35f, 0.0f, // 34
            1.0f, -0.35f, 0.0f,

            -1.0f, -0.30f, 0.0f, // 35
            1.0f, -0.30f, 0.0f,

            -1.0f, -0.25f, 0.0f, // 36
            1.0f, -0.25f, 0.0f,

            -1.0f, -0.20f, 0.0f, // 37
            1.0f, -0.20f, 0.0f,

            -1.0f, -0.15f, 0.0f, // 38
            1.0f, -0.15f, 0.0f,

            -1.0f, -0.10f, 0.0f, // 39
            1.0f, -0.10f, 0.0f,

            -1.0f, -0.05f, 0.0f, // 40
            1.0f, -0.05f, 0.0f,
        };

        const GLfloat graphVLines[] =
        {
            1.0f, 1.0f, 0.0f, // 1
            1.0f, -1.0f, 0.0f,

            0.95f, 1.0f, 0.0f, // 2
            0.95f, -1.0f, 0.0f,

            0.90f, 1.0f, 0.0f, // 3
            0.90f, -1.0f, 0.0f,

            0.85f, 1.0f, 0.0f, // 4
            0.85f, -1.0f, 0.0f,

            0.80f, 1.0f, 0.0f, // 5
            0.80f, -1.0f, 0.0f,

            0.75f, 1.0f, 0.0f, // 6
            0.75f, -1.0f, 0.0f,

            0.70f, 1.0f, 0.0f, // 7
            0.70f, -1.0f, 0.0f,

            0.65f, 1.0f,  0.0f, // 8
            0.65f, -1.0f, 0.0f,

            0.60f, 1.0f, 0.0f, // 9
            0.60f, -1.0f, 0.0f,

            0.55f, 1.0f, 0.0f, // 10
            0.55f, -1.0f, 0.0f,

            0.50f, 1.0f, 0.0f, // 11
            0.50f, -1.0f, 0.0f,

            0.45f, 1.0f, 0.0f, // 12
            0.45f, -1.0f, 0.0f,

            0.40f, 1.0f, 0.0f, // 13
            0.40f, -1.0f, 0.0f,

            0.35f, 1.0f, 0.0f, // 14
            0.35f, -1.0f, 0.0f,

            0.30f, 1.0f, 0.0f, // 15
            0.30f, -1.0f, 0.0f,

            0.25f, 1.0f, 0.0f, // 16
            0.25f, -1.0f, 0.0f,

            0.20f, 1.0f, 0.0f, // 17
            0.20f, -1.0f, 0.0f,

            0.15f, 1.0f, 0.0f, // 18
            0.15f, -1.0f, 0.0f,

            0.10f, 1.0f, 0.0f, // 19
            0.10f, -1.0f, 0.0f,

            0.05f, 1.0f, 0.0f, // 20
            0.05f, -1.0f, 0.0f,

            -1.0f, 1.0f, 0.0f, // 21
            -1.0f, -1.0f, 0.0f,

            -0.95f, 1.0f, 0.0f, // 22
            -0.95f, -1.0f, 0.0f,

            -0.90f, 1.0f, 0.0f, // 23
            -0.90f, -1.0f, 0.0f,

            -0.85f, 1.0f, 0.0f, // 24
            -0.85f, -1.0f, 0.0f,

            -0.80f, 1.0f, 0.0f, // 25
            -0.80f, -1.0f, 0.0f,

            -0.75f, 1.0f, 0.0f, // 26
            -0.75f, -1.0f, 0.0f,

            -0.70f, 1.0f, 0.0f, // 27
            -0.70f, -1.0f, 0.0f,

            -0.65f, 1.0f, 0.0f, // 28
            -0.65f, -1.0f, 0.0f,

            -0.60f, 1.0f, 0.0f, // 29
            -0.60f, -1.0f, 0.0f,

            -0.55f, 1.0f, 0.0f, // 30
            -0.55f, -1.0f, 0.0f,

            -0.50f, 1.0f, 0.0f, // 31
            -0.50f, -1.0f, 0.0f,

            -0.45f, 1.0f, 0.0f, // 32
            -0.45f, -1.0f, 0.0f,

            -0.40f, 1.0f, 0.0f, // 33
            -0.40f, -1.0f, 0.0f,

            -0.35f, 1.0f, 0.0f, // 34
            -0.35f, -1.0f, 0.0f,

            -0.30f, 1.0f, 0.0f, // 35
            -0.30f, -1.0f, 0.0f,

            -0.25f, 1.0f, 0.0f, // 36
            -0.25f, -1.0f, 0.0f,

            -0.20f, 1.0f, 0.0f, // 37
            -0.20f, -1.0f, 0.0f,

            -0.15f, 1.0f, 0.0f, // 38
            -0.15f, -1.0f, 0.0f,

            -0.10f, 1.0f, 0.0f, // 39
            -0.10f, -1.0f, 0.0f,

            -0.05f, 1.0f, 0.0f, // 40
            -0.05f, -1.0f, 0.0f
        };

        const GLfloat graphHVLinesColor[] =
        {
            0.0f, 0.0f, 1.0f, // 1
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 2
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 3
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 4
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 5
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 6
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 7
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 8
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 9
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 10
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 11
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 12
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 13
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 14
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 15
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 16
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 17
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 18
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 19
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 20
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 21
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 22
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 23
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 24
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 25
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 26
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 27
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 28
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 29
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 30
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 31
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 32
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 33
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 34
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 35
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 36
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 37
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 38
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 39
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 40
            0.0f, 0.0f, 1.0f,
        };

        // Horizontal Line
        // vao related code
        glGenVertexArrays(1, &vao_horizontalLine); //vao:- vertex array object
        glBindVertexArray(vao_horizontalLine);
        
        // vbo for position
        glGenBuffers(1, &vbo_horizontalLine_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_horizontalLine_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLine), horizontalLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_horizontalLine_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_horizontalLine_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLineColor), horizontalLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        // Vertical Line
        // vao related code
        glGenVertexArrays(1, &vao_verticalLine); //vao:- vertex array object
        glBindVertexArray(vao_verticalLine);

        // vbo for position
        glGenBuffers(1, &vbo_verticalLine_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_verticalLine_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLine), verticalLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_verticalLine_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_verticalLine_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLineColor), verticalLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);


        // Graph Horizontal Lines
        // vao related code
        glGenVertexArrays(1, &vao_graphHLines); //vao:- vertex array object
        glBindVertexArray(vao_graphHLines);

        // vbo for position
        glGenBuffers(1, &vbo_graphHLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(graphHLines), graphHLines, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_graphHLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(graphHVLinesColor), graphHVLinesColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        // Graph Vertical Lines
        // vao related code
        glGenVertexArrays(1, &vao_graphVLines); //vao:- vertex array object
        glBindVertexArray(vao_graphVLines);

        // vbo for position
        glGenBuffers(1, &vbo_graphVLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphVLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(graphVLines), graphVLines, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_graphHLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(graphHVLinesColor), graphHVLinesColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);
    
        base = 1.2f;
        height1 = 0.0f;
        base1 = base;

        gFlag = NO;
        tFlag = NO;
        sFlag = NO;
        cFlag = NO;
    
        height1 = ((sqrt(3)) * (base1 / 2));
        base = base / 2;
        height1 = height1 / 2;
        float median = (base1 / sqrt(3)) / -4;

        const GLfloat triangleLine[] =
        {
            0.0f, (height1 - median), 0.0f,
            -base, -height1 - median, 0.0f,
            -base, -height1 - median, 0.0f,
            base, -height1 - median, 0.0f,
            base, -height1 - median, 0.0f,
            0.0f, (height1 - median), 0.0f
        };

        const GLfloat triangleLineColor[] =
        {
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f
        };

        // Triangle Line
        // vao related code
        glGenVertexArrays(1, &vao_triangleLines); //vao:- vertex array object
        glBindVertexArray(vao_triangleLines);

        // vbo for position
        glGenBuffers(1, &vbo_triangleLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_triangleLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleLine), triangleLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_triangleLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_triangleLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleLineColor), triangleLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        const GLfloat squareLine[] =
        {
            float(((base1 / sqrt(3)) * 2) / 2), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
            float(-(((base1 / sqrt(3)) * 2) / 2)), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,

            float(-(((base1 / sqrt(3)) * 2) / 2)), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
            float(-(((base1 / sqrt(3)) * 2) / 2)), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,

            float(-(((base1 / sqrt(3)) * 2) / 2)), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,
            float(((base1 / sqrt(3)) * 2) / 2), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,

            float(((base1 / sqrt(3)) * 2) / 2), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,
            float(((base1 / sqrt(3)) * 2) / 2), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
        };

        const GLfloat squareLineColor[] =
        {
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
        };

        // Square Line
        // vao related code
        glGenVertexArrays(1, &vao_squareLines); //vao:- vertex array object
        glBindVertexArray(vao_squareLines);

        // vbo for position
        glGenBuffers(1, &vbo_squareLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_squareLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(squareLine), squareLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_squareLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_squareLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(squareLineColor), squareLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        [self circleArray : (base1 / sqrt(3)) : (base1 / sqrt(3)) : 0.0f : 0.0f];

        const GLfloat circleLineColor[] =
        {
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
        };

        // Square Line
        // vao related code
        glGenVertexArrays(1, &vao_circleLines); //vao:- vertex array object
        glBindVertexArray(vao_circleLines);

        // vbo for position
        glGenBuffers(1, &vbo_circleLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_circleLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(circleLine), circleLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_circleLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_circleLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(circleLineColor), circleLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);
    
    glClearDepthf(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    perspectiveProjectionMatrix = mat4::identity();
    
    singleTap = 0;
    doubleTap = 0;
    longPress = 0;
    
    return(0);
}

- (void) circleArray : (float) radius_x : (float) radius_y : (float) start : (float) end
{
    for (int i = 0; i <= 100; i++)
        {
            float angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
            circleLine[i][0] = cos(angle) * radius_x;
            circleLine[i][1] = sin(angle) * radius_y;
            circleLine[i][2] = 0.0f;
        }
}


- (void) resize : (int)width : (int)height
{
    // code
    if (height < 0)
        height = 1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);

    // "Triangle"
    // transformations
    mat4 translationMatrix = mat4::identity();
    mat4 modelViewMatrix = mat4::identity();
    mat4 modelViewProjectionMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -2.5f); // or only translate - glTranslate is replaced by this line
    modelViewMatrix = translationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    if (gFlag == YES)
    {
        glBindVertexArray(vao_graphHLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        glLineWidth(3);
        glDrawArrays(GL_LINES, 0, 80);

        glBindVertexArray(0);

        glBindVertexArray(vao_graphVLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        glLineWidth(3);
        glDrawArrays(GL_LINES, 0, 80);

        glBindVertexArray(0);

        glBindVertexArray(vao_horizontalLine);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        glLineWidth(3);
        glDrawArrays(GL_LINES, 0, 2);

        glBindVertexArray(0);

        glBindVertexArray(vao_verticalLine);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        glLineWidth(3);
        glDrawArrays(GL_LINES, 0, 2);

        glBindVertexArray(0);
    }
    
    if (tFlag == YES && gFlag == YES)
    {
        glBindVertexArray(vao_triangleLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        glLineWidth(3);
        glDrawArrays(GL_LINES, 0, 6);

        glBindVertexArray(0);
    }

    if (sFlag == YES && gFlag == YES)
    {
        glBindVertexArray(vao_squareLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        glLineWidth(3);
        glDrawArrays(GL_LINES, 0, 8);

        glBindVertexArray(0);
    }

    if (cFlag == YES && gFlag == YES)
    {
        glBindVertexArray(vao_circleLines);

        glDrawArrays(GL_LINE_LOOP, 0, 100);

        glBindVertexArray(0);
    }

        // stepE 3 : unuse the shader program object
        
        glUseProgram(0);
}

- (void) myupdate
{
    // code
}

- (void) uninitialize
{
    // code
    if (vbo_verticalLine_color)
        {
            glDeleteBuffers(1, &vbo_verticalLine_color);
            vbo_verticalLine_color = 0;
        }

        if (vbo_verticalLine_position)
        {
            glDeleteBuffers(1, &vbo_verticalLine_position);
            vbo_verticalLine_position = 0;
        }

        if (vao_verticalLine)
        {
            glDeleteVertexArrays(1, &vao_verticalLine);
            vao_verticalLine = 0;
        }

        if (vbo_horizontalLine_color)
        {
            glDeleteBuffers(1, &vbo_horizontalLine_color);
            vbo_horizontalLine_color = 0;
        }

        if (vbo_horizontalLine_position)
        {
            glDeleteBuffers(1, &vbo_horizontalLine_position);
            vbo_horizontalLine_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_horizontalLine)
        {
            glDeleteVertexArrays(1, &vao_horizontalLine);
            vao_horizontalLine = 0;
        }

        if (vbo_graphHLines_color)
        {
            glDeleteBuffers(1, &vbo_graphHLines_color);
            vbo_graphHLines_color = 0;
        }

        if (vbo_graphHLines_position)
        {
            glDeleteBuffers(1, &vbo_graphHLines_position);
            vbo_graphHLines_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_graphHLines)
        {
            glDeleteVertexArrays(1, &vao_graphHLines);
            vao_graphHLines = 0;
        }
        
        if (vbo_triangleLines_color)
        {
            glDeleteBuffers(1, &vbo_triangleLines_color);
            vbo_triangleLines_color = 0;
        }

        if (vbo_triangleLines_position)
        {
            glDeleteBuffers(1, &vbo_triangleLines_position);
            vbo_triangleLines_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_triangleLines)
        {
            glDeleteVertexArrays(1, &vao_triangleLines);
            vao_triangleLines = 0;
        }
        
        if (vbo_squareLines_color)
        {
            glDeleteBuffers(1, &vbo_squareLines_color);
            vbo_squareLines_color = 0;
        }

        if (vbo_squareLines_position)
        {
            glDeleteBuffers(1, &vbo_squareLines_position);
            vbo_squareLines_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_squareLines)
        {
            glDeleteVertexArrays(1, &vao_squareLines);
            vao_squareLines = 0;
        }
        

        if (vbo_circleLines_color)
        {
            glDeleteBuffers(1, &vbo_circleLines_color);
            vbo_circleLines_color = 0;
        }

        if (vbo_circleLines_position)
        {
            glDeleteBuffers(1, &vbo_circleLines_position);
            vbo_circleLines_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_circleLines)
        {
            glDeleteVertexArrays(1, &vao_circleLines);
            vao_circleLines = 0;
        }

        if (vbo_graphVLines_position)
        {
            glDeleteBuffers(1, &vbo_graphVLines_position);
            vbo_graphVLines_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_graphVLines)
        {
            glDeleteVertexArrays(1, &vao_graphVLines);
            vao_graphVLines = 0;
        }
    
    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);
        
        GLsizei numAttachedShaders;
        
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
        
        GLuint* shaderObjects = NULL;
        
        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
        
        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);
        
        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }
        
        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);
        
        // stepF 6 : unuse the shader program object
        glUseProgram(0);
        
        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        
        if (depthRenderbuffer)
        {
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            depthRenderbuffer = 0;
        }
        
        if (coloRenderbuffer)
        {
            glDeleteRenderbuffers(1, &coloRenderbuffer);
            coloRenderbuffer = 0;
        }
        
        if (defaultFramebuffer)
        {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            defaultFramebuffer = 0;
        }
        
        if ([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext : nil];
            [EAGLContext release];
            eaglContext = nil;
        }
    }
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
    if (longPress == 0)
            {
                if (singleTap == 1)
                    tFlag = YES;

                if (singleTap == 2)
                    sFlag = YES;

                if (singleTap == 3)
                     cFlag = YES;

                if(singleTap != 4)
                    singleTap++;
            }
            else if (longPress == 1)
            {
                if (singleTap == 1)
                    tFlag = NO;

                if (singleTap == 2)
                    sFlag = NO;

                if (singleTap == 3)
                     cFlag = NO;

                if(singleTap != 0)
                    singleTap--;
            }
}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
    doubleTap++;
            
            if (doubleTap == 1)
                gFlag = true;

            if(doubleTap > 1)
            {
                doubleTap = 0;
                gFlag = false;
            }
    
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
    longPress++;
            if(longPress > 1)
                longPress = 0;
}

- (void) dealloc
{
    [self uninitialize];
    
    if (displayLink)
    {
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

@end
