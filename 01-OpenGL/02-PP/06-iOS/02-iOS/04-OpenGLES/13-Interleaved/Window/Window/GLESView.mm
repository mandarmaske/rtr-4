//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
using namespace vmath;

@implementation GLESView
{
    @private
    EAGLContext* eaglContext; // embedded apple graphics library
    
    GLuint defaultFramebuffer;
    GLuint coloRenderbuffer;
    GLuint depthRenderbuffer;
    
    CADisplayLink* displayLink; // CA for Core Animation
    GLint framesPerSecond;
    BOOL isAnimating;
    
    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    GLuint vao_cube;
    GLuint vbo;

    GLuint texture_marble;
    GLuint textureSamplerUniform;

    mat4 perspectiveProjectionMatrix;

    BOOL bLight;

    // Uniform
    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;

    GLuint laUniform; // light ambient
    GLuint ldUniform; // light diffuse
    GLuint lsUniform; // light specular
    GLuint lightPositionUniform;

    GLuint kaUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint materialShininessUniform;

    GLuint lightingEnabledUniform;
    
    GLfloat lightAmbient[4];
    GLfloat lightDiffuse[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;

    GLfloat angleCube;
    
    int doubleTap;
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // 1) Create drawable surface which is called as layer
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        // 2) Set EAGLLayer properties (this is similar to setting pixelformatdistribute properties)
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        // 3) Create eagl context
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // 4) Set this context as current context
        [EAGLContext setCurrentContext : eaglContext];
        
        // 5) Print OpenGLES info
        printf("OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
        printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
        printf("OpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // 6) Create frame buffer
        glGenFramebuffers(1, &defaultFramebuffer);
        
        // 7) Bind With Above frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // 8) Create color render buffer
        glGenRenderbuffers(1, &coloRenderbuffer);
        
        // 9) Bind with above color render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
        
        // 10) Provide storage to render buffer
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        // 11) Set frame buffer according render buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, coloRenderbuffer);
        
        // 12) Declare backing width
        GLint backingWidth;
        
        // 13) Declare backing height
        GLint backingHeight;
        
        // 14) Get Color render buffer width into backing width
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        // 15) Get Color render buffer height into backing height
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        // 16) Create depth buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        
        // 17) bind above depth render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        // 18) Provide storage to depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        // 19) Get
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // 20) check the status of frame buffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // call initialize
        int result = [self initialize];
        
        if(result!=0)
        {
            printf("Result Not Obtained \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        
        framesPerSecond = 60; // this is started from iOS 8.0
        isAnimating = NO;
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

+ (Class)layerClass
{
    // code
    
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);

    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];
    
    // 12) Declare backing width
    GLint backingWidth;
    
    // 13) Declare backing height
    GLint backingHeight;
    
    // 14) Get Color render buffer width into backing width
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    // 15) Get Color render buffer height into backing height
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    // 16) Create depth buffer
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    // 17) bind above depth render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    // 18) Provide storage to depth render buffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    // 19) Get
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    // call our resize here
    [self resize : backingWidth : backingHeight];
    
    // 20) check the status of frame buffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }
    
    // call drawView after resize
    [self drawView : nil]; // or [self drawView : self];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    // call our display and update here
    [self display];
    [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        // create display link
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        // set frames per second for the display link
        [displayLink setPreferredFramesPerSecond : framesPerSecond];
        
        // add this display link to run loop
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        // remove display link from run loop
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    // code
    const GLchar* vertexShaderSourceCode =
            "#version 300 core" \
            "\n" \
    "in vec4 a_position;" \
            "in vec4 a_color;" \
            "in vec3 a_normal;" \
            "in vec2 a_texcoord;" \
            "uniform mat4 u_modelMatrix;" \
            "uniform mat4 u_viewMatrix;" \
            "uniform mat4 u_projectionMatrix;" \
            "uniform vec4 u_lightPosition;" \
            "uniform int u_lightingEnabled;" \
            "out vec3 transformedNormals;" \
            "out vec3 lightDirection;" \
            "out vec3 viewerVector;" \
            "out vec4 a_color_out;" \
            "out vec2 a_texcoord_out;" \
            "void main(void)" \
            "{" \
            "if(u_lightingEnabled==1)" \
            "{" \
            "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
            "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
            "transformedNormals = normalMatrix * a_normal;" \
            "lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" \
            "viewerVector = -eyeCoordinates.xyz;" \
            "}" \
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
            "a_color_out = a_color;" \
            "a_texcoord_out = a_texcoord;" \
            "}";

        // StepC 2 : creating shading object
        GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                    // stepC 5 e : display the log
                    printf("Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode =
            "#version 300 core" \
            "\n" \
    "precision highp float;" \
    "uniform highp sampler2D u_textureSampler;" \
            "in vec4 a_color_out;" \
            "in vec2 a_texcoord_out;" \
            "in vec3 transformedNormals;" \
            "in vec3 lightDirection;" \
            "in vec3 viewerVector;" \
            "uniform vec3 u_la;" \
            "uniform vec3 u_ld;" \
            "uniform vec3 u_ls;" \
            "uniform vec3 u_ka;" \
            "uniform vec3 u_kd;" \
            "uniform vec3 u_ks;" \
            "uniform float u_materialShininess;" \
            "uniform highp int u_lightingEnabled;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
            "vec3 phong_ads_light;" \
            "if(u_lightingEnabled==1)" \
            "{" \
                "vec3 ambient = u_la * u_ka;" \
                "vec3 normalized_transformedNormals = normalize(transformedNormals);" \
                "vec3 normalized_lightDirection = normalize(lightDirection);" \
                "vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" \
                "vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" \
                "vec3 normalized_viewerVector = normalize(viewerVector);" \
                "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" \
                "phong_ads_light = ambient + diffuse + specular;" \
            "}" \
            "else" \
            "{" \
            "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
            "}" \
                "FragColor = vec4(vec3(texture(u_textureSampler, a_texcoord_out)) * phong_ads_light * vec3(a_color_out), 1.0);" \
            "}";

        // StepC 2 : creating shading object
        GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    printf("Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");
        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    printf("Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    exit(0);

                }
            }
        }

    textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_textureSampler");

        modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
        viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
        projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

        // light uniform
        laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
        ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
        lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
        lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_lightPosition");

        // material uniform
        kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
        kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
        ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
        materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");

        // light enable uniform
        lightingEnabledUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");


    // vao & vbo related code
        //declarations of vertex data arrays
    const GLfloat cube_pcnt[] =
        {
            //front             //color-red         //normal-front      //texture-front
            1.0f, 1.0f, 1.0f,   1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   1.0f, 0.0f,
            -1.0f, 1.0f, 1.0f,  1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,
            -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 1.0f,
            1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   1.0f, 1.0f,

            //right             //color-green       //normal-right      //texture-right
            1.0f, 1.0f, -1.0f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f,
            1.0f, 1.0f, 1.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   0.0f, 0.0f,
            1.0f, -1.0f, 1.0f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   0.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,

            //back              //color-blue        //normal-back       //texture-back
            -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  1.0f, 0.0f,
            1.0f, 1.0f, -1.0f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  0.0f, 0.0f,
            1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  0.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  1.0f, 1.0f,

            //left              //color-cyan        //normal-left       //texture-back
            -1.0f, 1.0f, 1.0f,  0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  0.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
            -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  1.0f, 1.0f,

            //top               //color-magenta     //normal-top        //texture-top
            1.0f, 1.0f, -1.0f,  1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,
            -1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f,
            -1.0f, 1.0f, 1.0f,  1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1.0f,
            1.0f, 1.0f, 1.0f,   1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   1.0f, 1.0f,

            //bottom            //color-yellow      //normal-bottom     //texture-bottom
            1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  0.0f, 0.0f,
            -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  0.0f, 1.0f,
            1.0f, -1.0f, 1.0f,  1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  1.0f, 1.0f
        };

        // cube
        // vao
        glGenVertexArrays(1, &vao_cube); // vao:- vertex array object
        glBindVertexArray(vao_cube);

        // vbo for cubeposition
        glGenBuffers(1, &vbo);

        glBindBuffer(GL_ARRAY_BUFFER, vbo); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(cube_pcnt), cube_pcnt, GL_STATIC_DRAW);

        // sizeof(cube_pcnt) is nothing but 11 * 24 * sizeof(float) = 264 * sizeof(float) - 11 ani 24 he rows ani column nahi pan he better readability sath
        // aahe, actual te 1D array aahe

        // Position
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(0)); // 5th ani 6th para change aahe
        // 11 * sizeof(GLfloat) evdhi dhang taakli ki pudchi position milal - asa 24 vela hoil
        // 6th parameter - kuthun pudchi position milale tar array chya 0th position - 0th position ne dahanga takayala chalu karu

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        // Color
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        // Normal
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));

        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

        // Texture
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(GLfloat), (void*)(9 * sizeof(GLfloat)));

        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);

        glBindVertexArray(0);
    
    glClearDepthf(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    texture_marble = [self loadGLTexture : @"Marble" : @"bmp"];

    if (texture_marble == 0)
    {
        [self uninitialize];
        [self release];
        exit(0);
    }

    lightAmbient[0] = 0.1f;
    lightAmbient[1] = 0.1f;
    lightAmbient[2] = 0.1f;
    lightAmbient[3] = 1.0f;
     
     lightDiffuse[0] = 1.0f;
     lightDiffuse[1] = 1.0f;
     lightDiffuse[2] = 1.0f;
     lightDiffuse[3] = 1.0f;
     
     lightSpecular[0] = 1.0f;
     lightSpecular[1] = 1.0f;
     lightSpecular[2] = 1.0f;
     lightSpecular[3] = 1.0f;
     
     lightPosition[0] = 0.0f;
     lightPosition[1] = 0.0f;
     lightPosition[2] = 2.0f;
     lightPosition[3] = 1.0f;

     materialAmbient[0] = 0.0f;
     materialAmbient[1] = 0.0f;
     materialAmbient[2] = 0.0f;
     materialAmbient[3] = 1.0f;
     
     materialDiffuse[0] = 0.5f;
     materialDiffuse[1] = 0.2f;
     materialDiffuse[2] = 0.7f;
     materialDiffuse[3] = 1.0f;
     
     materialSpecular[0] = 0.7f;
     materialSpecular[1] = 0.7f;
     materialSpecular[2] = 0.7f;
     materialSpecular[3] = 1.0f;
     
     materialShininess = 128.0f;
     angleCube = 0.0f;
     bLight = NO;
    
    doubleTap = 0;
    
    return(0);
}

- (GLuint) loadGLTexture : (NSString*) textureFileName : (NSString*) extension
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
    NSString* textureFileNameWithExtension = [appBundle pathForResource : textureFileName ofType : extension];

    if (textureFileNameWithExtension == nil)
    {
        printf("texture file path name not obtained\n");
        [self uninitialize];
        [self release];
        
        return(0);
    }
    
    // Step 1 : get NSImage Representaion of texture file
    UIImage *uiImage = [[UIImage alloc]initWithContentsOfFile : textureFileNameWithExtension];
    // error checking for nsImage
    if (uiImage == nil)
    {
        printf("uiImage not obtained from texture file\n");
        [self uninitialize];
        [self release];
    }

    // Step 2 : from above NSImage Representaion obtain CGImage Representation of texture file
    CGImageRef cgImage = [uiImage CGImage];
    // context is not OpenGL context it is CG Graphics context

    // Step 3 : From this CGImage Representation get width and height of the image
    int width = (int)CGImageGetWidth(cgImage);
    int height = (int)CGImageGetHeight(cgImage);

    // Step 4 : From this CGImage Representation get CGData provider
    CGDataProviderRef cgDataProvider = CGImageGetDataProvider(cgImage);
    
    // Step 5 : Using CGData provider get Image Data in the form CFData (CF-Core Foundation)
    CFDataRef imageData = CGDataProviderCopyData(cgDataProvider);

    // Step 6 : Convert this CFData formatted image data into void*
    void* pixels = (void*)CFDataGetBytePtr(imageData); // returns const UInt8* which is type casted into void*

    // Proceed with usual texture creation code from this commment
    GLuint texture = 0;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture); // note the *
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    CFRelease(imageData);
    glBindTexture(GL_TEXTURE_2D, 0); // unbind

    return(texture);
}

- (void) resize : (int)width : (int)height
{
    // code
    if (height < 0)
        height = 1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);

    // transformations
        mat4 translationMatrix = mat4::identity();
        mat4 scaleMatrix = mat4::identity();
        mat4 rotationMatrix_x = mat4::identity();
        mat4 rotationMatrix_y = mat4::identity();
        mat4 rotationMatrix_z = mat4::identity();
        mat4 rotationMatrix = mat4::identity();
        mat4 modelMatrix = mat4::identity();
        mat4 viewMatrix = mat4::identity();
        mat4 modelViewProjectionMatrix = mat4::identity();
        rotationMatrix_x = vmath::rotate(angleCube, 1.0f, 0.0f, 0.0f);
        rotationMatrix_y = vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
        rotationMatrix_z = vmath::rotate(angleCube, 0.0f, 0.0f, 1.0f);
        rotationMatrix = rotationMatrix_x * rotationMatrix_y * rotationMatrix_x;
        translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); //glTranslatef() is replaced by this line
        scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
        modelMatrix = translationMatrix * rotationMatrix * scaleMatrix; // order is imp
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelMatrix; // order imp aahe

        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

        // sending light related uniforms
        if (doubleTap == 1)
        {
            glUniform1i(lightingEnabledUniform, 1);

            glUniform3fv(laUniform, 1, lightAmbient);
            glUniform3fv(ldUniform, 1, lightDiffuse);
            glUniform3fv(lsUniform, 1, lightSpecular);
            glUniform4fv(lightPositionUniform, 1, lightPosition);

            glUniform3fv(kaUniform, 1, materialAmbient);
            glUniform3fv(kdUniform, 1, materialDiffuse);
            glUniform3fv(ksUniform, 1, materialSpecular);
            glUniform1f(materialShininessUniform, materialShininess);
        }
        else
        {
            glUniform1i(lightingEnabledUniform, 0);
        }

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture_marble);
        glUniform1i(textureSamplerUniform, 0);

        glBindVertexArray(vao_cube);

        // stepE 2 : draw the desiered graphics/animation
        // here will be magic code

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //PP made glQuad nahi other option ne draw karayche
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // stepE 3 : unuse the shader program object
        
        glUseProgram(0);
    
}

- (void) myupdate
{
    // code

        angleCube = angleCube + 1.0f;

        if (angleCube >= 360.0f)
            angleCube = angleCube - 360.0f;
}

- (void) uninitialize
{
    // code
    if (vbo)
        {
            glDeleteBuffers(1, &vbo);
            vbo = 0;
        }

        if (vao_cube)
        {
            glDeleteVertexArrays(1, &vao_cube);
            vao_cube = 0;
        }

        if (texture_marble)
        {
            glDeleteTextures(1, &texture_marble);
            texture_marble = 0;
        }

        // shader uninitialization
        if (shaderProgramObject)
        {
            // stepF 0
            glUseProgram(shaderProgramObject);

            GLsizei numAttachedShaders;
            
            // stepF 1 : get the no. of attached shaders
            glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

            GLuint* shaderObjects = NULL;

            // stepF 2 : create empty buffer to hold array of  attached shader objects
            shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

            // stepF 3 : allocate enough memory to hold array of attached shader objects
            glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

            // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
            for (GLsizei i = 0; i < numAttachedShaders; i++)
            {
                glDetachShader(shaderProgramObject, shaderObjects[i]);
                glDeleteShader(shaderObjects[i]);
                shaderObjects[i] = 0;
            }

            // stepF 5 : free the memory allocated for the buffer
            free(shaderObjects);

            // stepF 6 : unuse the shader program object
            glUseProgram(0);

            // stepF 7 : delete the shader program object
            glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    
    
    if (depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if (coloRenderbuffer)
    {
        glDeleteRenderbuffers(1, &coloRenderbuffer);
        coloRenderbuffer = 0;
    }
    
    if (defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if ([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext : nil];
        [EAGLContext release];
        eaglContext = nil;
    }
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
    doubleTap++;
    
    if (doubleTap > 1)
        doubleTap = 0;
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
}

- (void) dealloc
{
    [self uninitialize];
    
    if (displayLink)
    {
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

@end

