//
//  AppDelegate.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "GLESView.h"

@implementation AppDelegate
{
    @private
    UIWindow *window;
    ViewController *viewController;
    GLESView *view;
}

- (BOOL) application : (UIApplication*)application didFinishLaunchingWithOptions : (NSDictionary*)launchOptions
{
    // code
    
    // Step 1 - creating window
    window = [UIWindow new]; // yaane window tayaar honar
    
    // Step 2 - set background
    [window setBackgroundColor : [UIColor blackColor]];
    
    // Step 3 - create view controll
    viewController = [[ViewController alloc]init];
    
    // Step 4 - setRootViewController
    [window setRootViewController : viewController];
    
    // CGRect rect = window.screen.bounds; // same line as per below line but called as . to set and get
    CGRect rect = [[window screen]bounds];
    view = [[GLESView alloc]initWithFrame : rect];
    
    [viewController setView : view];
    [view release]; // reference count kami karnysathi
    [window makeKeyAndVisible];
    [view startAnimation];
    
    return(YES);
}

- (void) applicationWillResignActive : (UIApplication*)application // jar dusra application var gela tar hi application resign hote
{
    // code
    [view stopAnimation];
}

- (void) applicationDidEnterBackground : (UIApplication*)application
{
    // code
}

- (void) applicationWillEnterForeground : (UIApplication*)application // foreground var application top la aali
{
    // code
}

- (void) applicationDidBecomeActive : (UIApplication*)application // jevha active hoil tevha
{
    // code
    [view startAnimation];
}

- (void) applicationWillTerminate : (UIApplication*)application
{
    // code
    [view stopAnimation];
}

- (void) dealloc
{
    // code
    if (view)
    {
        [view release];
        view = nil;
    }
    
    if (viewController)
    {
        [viewController release];
        viewController = nil;
    }
    
    if (window)
    {
        [window release];
        window = nil;
    }
    
    [super dealloc];
}

@end
