//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

@implementation GLESView
{
    @private
    EAGLContext* eaglContext; // embedded apple graphics library
    
    GLuint defaultFramebuffer;
    GLuint coloRenderbuffer;
    GLuint depthRenderbuffer;
    
    CADisplayLink* displayLink; // CA for Core Animation
    GLint framesPerSecond;
    BOOL isAnimating;
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // 1) Create drawable surface which is called as layer
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];
        
        // 2) Set EAGLLayer properties (this is similar to setting pixelformatdistribute properties)
        [eaglLayer setOpaque : YES];
        NSNumber* boolNumber = [NSNumber numberWithBool : NO];
        NSDictionary* dictionary = [NSDictionary dictionaryWithObjectsAndKeys : boolNumber, kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        [eaglLayer setDrawableProperties : dictionary];
        
        // 3) Create eagl context
        eaglContext = [[EAGLContext alloc] initWithAPI : kEAGLRenderingAPIOpenGLES3];
        
        if (eaglContext == nil)
        {
            // code
            printf("failed to create eagl context\n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // 4) Set this context as current context
        [EAGLContext setCurrentContext : eaglContext];
        
        // 5) Print OpenGLES info
        printf("OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
        printf("OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
        printf("OpenGL Version : %s \n", glGetString(GL_VERSION));
        printf("OpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // 6) Create frame buffer
        glGenFramebuffers(1, &defaultFramebuffer);
        
        // 7) Bind With Above frame buffer
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // 8) Create color render buffer
        glGenRenderbuffers(1, &coloRenderbuffer);
        
        // 9) Bind with above color render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
        
        // 10) Provide storage to render buffer
        [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : eaglLayer];
        
        // 11) Set frame buffer according render buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, coloRenderbuffer);
        
        // 12) Declare backing width
        GLint backingWidth;
        
        // 13) Declare backing height
        GLint backingHeight;
        
        // 14) Get Color render buffer width into backing width
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        // 15) Get Color render buffer height into backing height
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        // 16) Create depth buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        
        // 17) bind above depth render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        // 18) Provide storage to depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        // 19) Get
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // 20) check the status of frame buffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Incomplete Frame buffer \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        // call initialize
        int result = [self initialize];
        
        if(result!=0)
        {
            printf("Result Not Obtained \n");
            [self uninitialize];
            [self release];
            exit(0);
        }
        
        
        framesPerSecond = 60; // this is started from iOS 8.0
        isAnimating = NO;
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

+ (Class)layerClass
{
    // code
    
    return([CAEAGLLayer class]);
}

- (void) layoutSubviews
{
    // code
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);

    [eaglContext renderbufferStorage : GL_RENDERBUFFER fromDrawable : (CAEAGLLayer*)[self layer]];
    
    // 12) Declare backing width
    GLint backingWidth;
    
    // 13) Declare backing height
    GLint backingHeight;
    
    // 14) Get Color render buffer width into backing width
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    
    // 15) Get Color render buffer height into backing height
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    // 16) Create depth buffer
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    // 17) bind above depth render buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    // 18) Provide storage to depth render buffer
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    
    // 19) Get
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    // call our resize here
    [self resize : backingWidth : backingHeight];
    
    // 20) check the status of frame buffer
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Incomplete Frame buffer \n");
        [self uninitialize];
        [self release];
        exit(0);
    }
    
    // call drawView after resize
    [self drawView : nil]; // or [self drawView : self];
}

/*- (void) drawRect : (CGRect) dirtyRect
{
    // code
}*/

- (void) drawView : (id)sender
{
    // code
    [EAGLContext setCurrentContext : eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    // call our display and update here
    [self display];
    // [self myupdate];
    
    glBindRenderbuffer(GL_RENDERBUFFER, coloRenderbuffer);
    
    [eaglContext presentRenderbuffer : GL_RENDERBUFFER];
}

- (void) startAnimation
{
    // code
    if (isAnimating == NO)
    {
        // create display link
        displayLink = [CADisplayLink displayLinkWithTarget : self selector : @selector(drawView : )];
        
        // set frames per second for the display link
        [displayLink setPreferredFramesPerSecond : framesPerSecond];
        
        // add this display link to run loop
        [displayLink addToRunLoop : [NSRunLoop currentRunLoop]  forMode : NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

- (void) stopAnimation
{
    // code
    if (isAnimating == YES)
    {
        // remove display link from run loop
        [displayLink invalidate];
        
        isAnimating = NO;
    }
}

- (int) initialize
{
    // code
    glClearDepthf(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    
    return(0);
}

- (void) resize : (int)width : (int)height
{
    // code
    if (height < 0)
        height = 1;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
}

- (void) display
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
}

- (void) myupdate
{
    // code
}

- (void) uninitialize
{
    // code
    if (depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if (coloRenderbuffer)
    {
        glDeleteRenderbuffers(1, &coloRenderbuffer);
        coloRenderbuffer = 0;
    }
    
    if (defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if ([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext : nil];
        [EAGLContext release];
        eaglContext = nil;
    }
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    [self uninitialize];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
}

- (void) dealloc
{
    [self uninitialize];
    
    if (displayLink)
    {
        [displayLink release];
        displayLink = nil;
    }
    
    [super dealloc];
}

@end
