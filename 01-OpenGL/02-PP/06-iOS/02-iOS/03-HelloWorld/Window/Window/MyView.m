//
//  MyView.m
//  Window
//
//  Created by user226430 on 12/25/22.
//

#import "MyView.h"

@implementation MyView

{
    @private
    NSString* string;
}

- (id) initWithFrame : (CGRect)frame
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        string = @"Happy New Year Sabko ^-^";
        
        // gesture recognizer code
        
        // Single Tap
        
        UITapGestureRecognizer* singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onSingleTap : )];
        [singleTapGestureRecognizer setNumberOfTapsRequired : 1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired : 1]; // kiti fingers olkhayche
        [singleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : singleTapGestureRecognizer];
        
        
        // Double Tap
        UITapGestureRecognizer* doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget : self action : @selector(onDoubleTap : )];
        [doubleTapGestureRecognizer setNumberOfTapsRequired : 2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired : 1];
        [doubleTapGestureRecognizer setDelegate : self];
        [self addGestureRecognizer : doubleTapGestureRecognizer];
        
        // Differentiate between double tap and single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail : doubleTapGestureRecognizer];
        
        // Swipe
        UISwipeGestureRecognizer* swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget : self action : @selector(onSwipe : )];
        [self addGestureRecognizer : swipeGestureRecognizer];
        
        // Long Press
        UILongPressGestureRecognizer* longPressGestureRecgonizer = [[UILongPressGestureRecognizer alloc]initWithTarget: self action : @selector(onLongPress : )];
        [self addGestureRecognizer : longPressGestureRecgonizer];
    }
    
    return(self);
}

- (void) drawRect : (CGRect) dirtyRect // apple recommended the name dirtyRect
{
    // code
    // he 3 lines sangtaat ki background color set kara ani te rect fill zhali pahije
    UIColor* backgroundColor = [UIColor blackColor];
    [backgroundColor set];
    UIRectFill(dirtyRect);
    
    UIFont *font = [UIFont fontWithName : @"helvetica"
                                   size : 24];
    UIColor *textColor = [UIColor greenColor];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys : font, NSFontAttributeName,
    textColor, NSForegroundColorAttributeName, nil]; // array dila dictionaryWithObjectsAndKeys la
    
    CGSize textSize = [string sizeWithAttributes : dictionary]; // NSSize ~ CGSize
    CGPoint point;
    
    point.x = dirtyRect.size.width / 2 - textSize.width / 2;
    point.y = dirtyRect.size.height / 2 - textSize.height / 2 + 12;
    
    [string drawAtPoint : point withAttributes : dictionary];
}

- (BOOL) acceptsFirstResponder
{
    // code
    
    return(YES);
}

- (void) touchesBegan : (NSSet*)touches withEvent : (UIEvent*)event
{
    // code
    
}

- (void) onSingleTap : (UITapGestureRecognizer*) tgr // tgr - tap gesture recgnizer
{
    // code
    string = @"Single Tap";
    [self setNeedsDisplay];

}

- (void) onDoubleTap : (UITapGestureRecognizer*) tgr
{
    // code
    string = @"Double Tap";
    [self setNeedsDisplay];
}

- (void) onSwipe : (UISwipeGestureRecognizer*) sgr // sgr swipe gesture recognizer
{
    // code
    //string = @"Swipe"; // simulator var swipe karta yet nahi tyamule comment kela
    //[self setNeedsDisplay];
    [self release];
    exit(0);
}

- (void) onLongPress : (UILongPressGestureRecognizer*) lpgr // lpgr - long press geture recognizer
{
    // code
    string = @"Long Press";
    [self setNeedsDisplay];
}

- (void) dealloc
{
    [super dealloc];
}

@end
