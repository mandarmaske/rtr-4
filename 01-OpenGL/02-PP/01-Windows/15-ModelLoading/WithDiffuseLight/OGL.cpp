// header file
#include<windows.h>
#include"OGL.h"//icon
#include<stdio.h> // for file io functions
#include<stdlib.h> // for exit()

// OpenGL header file
// step A 1 : include file
#include<gl\glew.h> // this must be before including gl.h - extension enabling sathi
#include<GL\gl.h>
#include"vmath.h"
using namespace vmath;

// OpenGL library
// stepA 2 : lnking with import library
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"OpenGL32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable declarations
BOOL gbActiveWindow = FALSE;
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE* gpFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// programable pipeline related global veriable
GLuint shaderProgramObject;

enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

GLuint vao;
GLuint vbo;
GLuint vbo_normal;
GLuint elementBuffer;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint ldUniform;
GLuint kdUniform;
GLuint lightpositionUniform;

GLuint lightingEnableUniform;

BOOL bLight = FALSE;

GLfloat lightDiffuse[] = { 1.0f,1.0f ,1.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,0.5f ,0.5f,1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };

mat4 perspectiveProjectionMatrix;

struct vec_int
{
	int* p;
	int size;
};

struct vec_float
{
	float* pf;
	int size;
};

#define BUFFER_SIZE 1024
char buffer[BUFFER_SIZE];

FILE *gp_mesh_file = NULL;
struct vec_float *gp_vertex, *gp_texture, *gp_normal;
struct vec_float *gp_vertex_sorted, *gp_texture_sorted, *gp_normal_sorted;
struct vec_int *gp_vertex_indices, *gp_texture_indices, *gp_normal_indices;

GLfloat angleModel = 0.0f;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iRetval = 0;

	// code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation Of Log File Faild. Exitting"), TEXT("File IO ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created  \n");
	}

	// initialization of WNDCLASS structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// register above class or wndclass 
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,//yache kam aase aahe ki window satat taskbar varti rahil.
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE, // WS_CLIPCHILDREN : jr mala chlid window aastil tr tyana maja area cover karun deu nakomala overlap karat aastil tr tyana clip kr majya vr kon nako, clip mhanje kapa
		//|WS_CLIPSIBLINGS:SIBLINGS mhaje bhau bhau majya bhav jari mala overlap karat aastil tr tyana clip kr majya vr kon nako
		//|WS_VISIBLE:initially make me visibe even if there WM_PAINT
		(GetSystemMetrics(SM_CXSCREEN) / 2 - 400),
		(GetSystemMetrics(SM_CYSCREEN) / 2 - 300),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetval = initialize();
	
	if(iRetval== -1)
	{
		fprintf(gpFile, "Choose Pixel Format Faild \n");
		uninitialize();
	}
	if (iRetval == -2)
	{
		fprintf(gpFile, "Set Pixel Format Faild \n");
		uninitialize();
	}

	if (iRetval == -3)
	{
		fprintf(gpFile, "Create OpenGL Context Faild \n");
		uninitialize();
	}

	if (iRetval == -4)
	{
		fprintf(gpFile, "Making OpenGl Context As Current Context Faild \n");
		uninitialize();
	}

	if (iRetval == -5)
	{
		fprintf(gpFile, "glew Faild \n");
		uninitialize();
	}

	// show window
	ShowWindow(hwnd, iCmdShow);

	// foregrounding and focusing the window
	SetForegroundWindow(hwnd);//here we use ghwnd and hwnd,but here ghwnd used for global function so we use hwnd
	SetFocus(hwnd);

	// game loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}
	uninitialize();

	return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void resize(int, int);
	

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);
		//break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		case 'L':
		case 'l':
			if (bLight == FALSE)
			{
				bLight = TRUE;
			}
			else
			{
				bLight = FALSE;
			}
			break;

		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 27:
			DestroyWindow(hwnd);
			break;

		default:
			break;
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));//resize(width , hight);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//varible declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = FALSE;

	}

}

int initialize(void)//convetion follow karayche mhanun initialize small
{
	// function declarations
	void printGLInfo(void);
	void uninitialize(void);
	void resize(int width, int height);
	void load_mesh(void);

	// veriable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// initialization of pixelformatdescriptortable
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	// getdc
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	
	// set the choosen pixel format
	// code
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
		
	}
	// create OpenGl rendering context
	ghrc = wglCreateContext(ghdc);//briging API

	if (ghrc == NULL)
	{
		return(-3);
	}
	// make the rendering context to current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
		
	}

	// stepA 3 : initializaton of glew
	// glew initializaton
	if (glewInit() != GLEW_OK)
	{
		return(-5);
	}

	// print OpenGLInfo
	printGLInfo();

	// StepC 1 : writing shading code
	// vertex shader 
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec4 u_lightPosition;" \
		"uniform int u_lightingEnabled;" \
		"out vec3 diffuse_light_color;" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled==1)" \
		"{"
		"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
		"mat3 normalMatrix = mat3(transpose(inverse(u_viewMatrix * u_modelMatrix)));" \
		"vec3 transformedNormals = normalize(normalMatrix * a_normal);" \
		"vec3 lightDirection = normalize(vec3(u_lightPosition - eyeCoordinates));" \
		"diffuse_light_color = u_ld * u_kd * max(dot(lightDirection,transformedNormals),0.0);" \
		"}" \
		"gl_Position = u_projectionMatrix *u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti string cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);  
		
		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
				
				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_color_out;" \
		"in vec3 diffuse_light_color;" \
		"uniform int u_lightingEnabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled==1)"
		"{" \
		"FragColor = vec4(diffuse_light_color,1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
	
	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, fragmentShaderObject);
	
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "a_color");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize();

			}
		}
	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

	ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	lightpositionUniform = glGetUniformLocation(shaderProgramObject, "u_lightPosition");
	lightingEnableUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

	load_mesh();

	// vao & vbo related code
	//declarations of vertex data arrays

	// vao
	glGenVertexArrays(1, &vao); // vao:- vertex array object
	glBindVertexArray(vao);

	// vbo 
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo); //bind buffer
	glBufferData(GL_ARRAY_BUFFER, (gp_vertex_sorted -> size) * sizeof(GLfloat), gp_vertex_sorted -> pf, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
	glBufferData(GL_ARRAY_BUFFER, (gp_normal_sorted -> size) * sizeof(GLfloat), gp_normal_sorted -> pf, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &elementBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer); //bind buffer
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (gp_vertex_indices -> size) * sizeof(GLint), gp_vertex_indices -> p, GL_STATIC_DRAW); 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // unbind buffer
	
	glBindVertexArray(0);

	// here start OpenGL code (depth & clear color code)
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	//depth related changes
	glShadeModel(GL_SMOOTH); 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

//  stepB 
void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	// StepB 1 : printing OpenGL vendor,renderer,version,GLSL version 
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	// StepB 2 : printing no. of supproted extension & names of extension
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}


void resize(int width, int height)
{
	// code
	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject);

	mat4 translationMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	// square
	// transformations
	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); 
	rotationMatrix = vmath::rotate(angleModel, 0.0f, 1.0f, 0.0f);
	modelMatrix = translationMatrix * rotationMatrix; // order is imp
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	// sending light related uniforms
	if (bLight == TRUE)
	{
		glUniform1i(lightingEnableUniform, 1);

		glUniform3fv(ldUniform, 1, lightDiffuse);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform4fv(lightpositionUniform, 1, lightPosition);
	}
	else
	{
		glUniform1i(lightingEnableUniform, 0);
	}

	glBindVertexArray(vao);

	//glPointSize(2.0f);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
	glDrawElements(GL_TRIANGLES, (gp_vertex_indices->size), GL_UNSIGNED_INT, NULL);
	//glDrawElements(GL_TRIANGLE_FAN, (gp_vertex_indices->size), GL_UNSIGNED_INT, NULL);
	//glDrawElements(GL_POINTS, (gp_vertex_indices->size), GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0); 
	
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void load_mesh(void)
{
	struct vec_int *create_vec_int(void);
	struct vec_float *create_vec_float(void);
	int push_back_vec_int(struct vec_int *p_vec_int, int data);
	int push_back_vec_float(struct vec_float *p_vec_int, float data);

	char *space = " ";
	char *slash = "/";
	char *first_token = NULL;
	char *token = NULL;

	char *f_entries[3] = {NULL, NULL, NULL};
	int nr_pos_cords = 0;
	int nr_tex_cords = 0;
	int nr_normal_cords = 0;
	int nr_faces = 0;
	int i, vi;

	gp_mesh_file = fopen("MonkeyHead.obj", "r");

	if (gp_mesh_file == NULL)
	{
		fprintf(stderr, "error in opening file\n");
		exit(EXIT_FAILURE);
	}

	gp_vertex = create_vec_float();
	gp_texture = create_vec_float();
	gp_normal = create_vec_float();

	gp_vertex_indices = create_vec_int();
	gp_texture_indices = create_vec_int();
	gp_normal_indices = create_vec_int();

	while (fgets(buffer, BUFFER_SIZE, gp_mesh_file) != NULL)
	{
		first_token = strtok(buffer, space);

		if (strcmp(first_token, "v") == 0)
		{
			nr_pos_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_vertex, atof(token));
		}
		else if (strcmp(first_token, "vt") == 0)
		{
			nr_tex_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_texture, atof(token));
		}
		else if (strcmp(first_token, "vn") == 0)
		{
			nr_normal_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_normal, atof(token));
		}
		else if (strcmp(first_token, "f") == 0)
		{
			nr_faces++;
			for (i = 0; i < 3; i++)
			{
				f_entries[i] = strtok(NULL, space);
			}

			for (i = 0; i < 3; i++)
			{
				token = strtok(f_entries[i], slash);
				push_back_vec_int(gp_vertex_indices, atoi(token) - 1);

				token = strtok(NULL, slash);
				push_back_vec_int(gp_texture_indices, atoi(token) - 1);

				token = strtok(NULL, slash);
				push_back_vec_int(gp_normal_indices, atoi(token) - 1);
			}
		}
	}

	gp_vertex_sorted = create_vec_float();
	for (i = 0; i < gp_vertex_indices -> size; i++)
	{
		push_back_vec_float(gp_vertex_sorted, gp_vertex -> pf[i]);
	}

	gp_texture_sorted = create_vec_float();
	for (i = 0; i < gp_texture_indices -> size; i++)
	{
		push_back_vec_float(gp_texture_sorted, gp_texture -> pf[i]);
	}

	gp_normal_sorted = create_vec_float();
	for (i = 0; i < gp_normal_indices->size; i++)
	{
		push_back_vec_float(gp_normal_sorted, gp_normal -> pf[i]);
	}

	fclose(gp_mesh_file);
	gp_mesh_file = NULL;
}

void update(void)
{
	// code

	angleModel = angleModel + 1.0f;

	if (angleModel >= 360.0f)
		angleModel = angleModel - 360.0f;
}

struct vec_int *create_vec_int(void)
{
	struct vec_int *p = (struct vec_int*)malloc(sizeof(struct vec_int));
	memset(p, 0, sizeof(struct vec_int));
	return p;
}

struct vec_float *create_vec_float(void)
{
	struct vec_float *p = (struct vec_float*)malloc(sizeof(struct vec_float));
	memset(p, 0, sizeof(struct vec_float));
	return p;
}

int push_back_vec_int(struct vec_int* p_vec_int, int data)
{
	p_vec_int -> p = (int*)realloc(p_vec_int -> p, (p_vec_int -> size + 1) * sizeof(int));
	p_vec_int -> size = p_vec_int -> size + 1;
	p_vec_int -> p[p_vec_int -> size - 1] = data;
	return(0);
}

int push_back_vec_float(struct vec_float *p_vec_float, float data)
{
	p_vec_float -> pf = (float*)realloc(p_vec_float -> pf, (p_vec_float -> size + 1) * sizeof(float));
	p_vec_float -> size = p_vec_float -> size + 1;
	p_vec_float -> pf[p_vec_float -> size - 1] = data;
	return(0);
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);
	int destroy_vec_int(struct vec_int* p_vec_int);
	int destroy_vec_float(struct vec_float *p_vec_float);

	// code
	if (gbFullScreen==TRUE)
	{
		ToggleFullScreen();
	}

	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	if (vbo_normal)
	{
		glDeleteBuffers(1, &vbo_normal);
		vbo_normal = 0;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (elementBuffer)
	{
		glDeleteBuffers(1, &elementBuffer);
		elementBuffer = 0;
	}

	// shader uninitialization
	if (shaderProgramObject)
	{
		// stepF 0
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	destroy_vec_float(gp_vertex);
	gp_vertex = NULL;

	destroy_vec_float(gp_texture);
	gp_texture = NULL;

	destroy_vec_float(gp_normal);
	gp_normal = NULL;

	destroy_vec_float(gp_vertex_sorted);
	gp_vertex_sorted = NULL;

	destroy_vec_float(gp_texture_sorted);
	gp_texture_sorted = NULL;

	destroy_vec_float(gp_normal_sorted);
	gp_normal_sorted = NULL;

	destroy_vec_int(gp_vertex_indices);
	gp_vertex_indices = NULL;

	destroy_vec_int(gp_texture_indices);
	gp_texture_indices = NULL;

	destroy_vec_int(gp_normal_indices);
	gp_normal_indices = NULL;

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed");
		fclose(gpFile);
		gpFile = NULL;
	}
}

int destroy_vec_int(struct vec_int* p_vec_int)
{
	free(p_vec_int->p);
	free(p_vec_int);
	return(0);
}

int destroy_vec_float(struct vec_float* p_vec_float)
{
	free(p_vec_float->pf);
	free(p_vec_float);
	return(0);
}

