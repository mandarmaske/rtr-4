//copy above blue color code and make depth realated changes in initialize() and display()

in initialize() this two lines are removed
glShadeModel(GL_SMOOTH);
glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

// globally add some files
// OpenGL header file
#include<gl\glew.h> // this must be before including gl.h

// OpenGL library
#pragma comment(lib,"glew32.lib")

in initialize() 
//after error block of wglMakeCurrent and above glClearColor

// make some changes in build.bat

// after initialize() write new function
void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}

// in initialize() make some changes

// function declarations
void printGLInfo(void);
void uninitialize(void);
void resize(void);

// after glew initializaton error block call this printGLInfo() 
// print OpenGLInfo
printGLInfo();

// ha call zala ki lhali aani glClearColor() chya varti add below lines
// print OpenGLInfo
	printGLInfo();

	// Step3 1
	// vertex shader 
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"void main(void)" \
		"{" \
		"}";

	// Step3 2
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// step3 3
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti string cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// step3 4
	glCompileShader(vertexShaderObject); // inline complier ne compile hoto human understandable to gpu understandable

	// step3 5
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// step3 5 a
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// step3 5 b
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);  
		
		// step3 5 c
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// step3 5 d
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
				
				// step3 5 e
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// step3 5 f
				free(log);

				// step3 5 g
				uninitialize();
			}
		}
	}

// after above write same as it for fragment shader and make some changes in it
// Step3 1
	// fragment shader 
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"void main(void)" \
		"{" \
		"}";

	// Step3 2
	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// step3 3
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
	
	// step3 4
	glCompileShader(fragmentShaderObject); // inline coplier ne compile hoto human understandable to gpu understandable

	// step3 5
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// step3 5 a
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// step3 5 b
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// step3 5 c
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// step3 5 d
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// step3 5 e
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// step3 5 f
				free(log);

				// step3 5 g
				uninitialize();
			}
		}
	}

// after fragment shader
// go globally
// programable pipeline related global veriable
GLuint shaderProgramObject;

// Shader program object
	// stepD 1 
	shaderProgramObject = glCreateProgram();

	// stepD 2
	glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	// stepD 3
	glLinkProgram(shaderProgramObject);

	//stepD 4
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

				// stepD 4e
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f
				free(log);

				// stepD 4g
				uninitialize();

			}
		}
	}

//in display() add some line

// after glClear() and above SwapBuffer() add below code
// stepE 1
	// use the shader program object
	glUseProgram(shaderProgramObject);

	// stepE 2
	// here will be magic code

	// stepE 3
	// unuse the shader program object
	glUseProgram(0);


// in uninitialize()
// after gbFullScreen and above wglGetCurrentContext() blocks add below code

// shader uninitialization
	if (shaderProgramObject)
	{
		// stepF 0
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		
		// stepF 1
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5
		free(shaderObjects);

		// stepF 6
		glUseProgram(0);

		// stepF 7
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}



 

































