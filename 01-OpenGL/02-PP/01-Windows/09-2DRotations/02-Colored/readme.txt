changes
1) declare 2 variables globally
GLfloat angleTriangle = 0.0f;
GLfloat angleSquare = 0.0f;

2) in display(),
	for triangle
	1)mat4 rotationMatrix = mat4::identity(); after translationMatriz
	2)rotationMatrix = vamth::rotate(angleTriangle, 0.0f, 1.0f, 0.0f); after initialization of traslationMatrix
	for square
	1)mat4 rotationMatrix = mat4::identity(); after translationMatriz
	2)rotationMatrix = vamth::rotate(angleSquare, 1.0f, 0.0f, 0.0f); after initialization 

3) in update() same as FFP
	angleTriangle = angleTriangle + 2.5f;
	if (angleTriangle >= 360.0f)
	{
		angleTriangle = angleTriangle - 360.0f;
	}
		
	angleSquare = angleSquare + 2.5f;
	if (angleSquare >= 360.0f)
	{
		angleSquare = angleSquare - 360.0f;
	}