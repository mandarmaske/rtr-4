1) take build.bat, OGL.cpp and Clapper.ico and vmath.h from 3DRotation
2) take ogl.rc, ogl.h, Stone And Vijay_Kundali from ffp 3d Texture File
3) GLuint vbo_pyramid_color; change to vbo_pyramid_texcoord and vbo_cube_color to vbo_cube_texcoord everywhere
4) declare new variables like GLuint texture_kundali; and GLuint texture_stone; and GLuint textureSamplerUniform;
5) in initialize(), vec4 change to vec2, change where a_color or a_color_out and declare a_texture or a_texture_out
in vertex and fragment shader
6) add new line before "out vec4 FragColor;" \ -> "uniform sampler2D u_textureSampler;" \
7) in main of fragment add line - "FragColor = texture(u_textureSampler, a_texcoord_out);" \
8) at post linking add line after mvpMatrixUniform, textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_textureSampler");
9) instead of pyrmaid color and cube color add texture coordinate from group leaders files
10) in vbo code for texture add new changes,

In Pyramid code
glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexcoords), pyramidTexcoords, GL_STATIC_DRAW);
glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);

In Cube Code
glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoords), cubeTexcoords, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

11) copy if loadGLTExture prototype call and glEnable line from ffp texture code, handle all error handling
12) copy LoadGLTexture()
13) changes in LoadGLTexture(), glPixelStorei(GL_UNPACK_ALIGNMENT, 1); instead of 4 2nd Para
14) instead of gluBuild2DMipmaps(), use or write glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits); and glGenerateMipmap(GL_TEXTURE_2D);
15) in display(), after glUniformMatrix4fv() Add 3 lines of pyramid
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_stone);
	glUniform1i(textureSamplerUniform, 0);

after glUniformMatrix4fv() Add 3 lines of cube
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_kundali);
	glUniform1i(textureSamplerUniform, 0);
16) unbind line after glBindVertexArray(0) of pyramid and cube, glBindTexture(GL_TEXTURE_2D, 0);



