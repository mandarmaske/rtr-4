template texture smiley
1) declare texture_checkerboard and checkerboard array copy from FFP checkerboard
2) prototype LoadGLtexture as per FFP Code written type is void and call is LoadGLTexture(); change it
3) copy LoadGLTexture() and Make_Checkerboard() as it is no change
4) in display(), instead of glBindTexture(GL_TEXTURE_2D, texture_smiley); chnage to glBindTexture(GL_TEXTURE_2D, texture_Checkerboard);
5) in initialize(), remove position array, change in vao of alBufferData glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
6) in display(), declare position array after binding of vao and add lines
GLfloat position[12];

	position[0] = 0.0f;
	position[1] = 1.0f;
	position[2] = 0.0f;
	position[3] = -2.0f;
	position[4] = 1.0f;
	position[5] = 0.0f;
	position[6] = -2.0f;
	position[7] = -1.0f;
	position[8] = 0.0f;
	position[9] = 0.0f;
	position[10] = -1.0f;
	position[11] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	position[0] = 2.41421f;
	position[1] = 1.0f;
	position[2] = -1.41421f;
	position[3] = 1.0f;
	position[4] = 1.0f;
	position[5] = 0.0f;
	position[6] = 1.0f;
	position[7] = -1.0f;
	position[8] = 0.0f;
	position[9] = 2.41421f;
	position[10] = -1.0f;
	position[11] = -1.41421f;

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(position), position, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

upto unbinding of vertex Array