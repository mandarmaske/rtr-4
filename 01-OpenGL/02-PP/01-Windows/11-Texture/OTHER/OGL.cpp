// header file
#include<windows.h>
#include"OGL.h"//icon
#include"Wishes.h"//icon
#include<stdio.h> // for file io functions
#include<stdlib.h> // for exit()

// OpenGL header file
// step A 1 : include file
#include<gl\glew.h> // this must be before including gl.h
#include<GL\gl.h>
#include"vmath.h"
using namespace vmath;

// OpenGL library
// stepA 2 : lnking with import library
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"OpenGL32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable declarations
BOOL gbActiveWindow = FALSE;
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE* gpFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// function declarations
void initialize_wishes(void);
BOOL loadStbImage(GLuint*, char*);
void display_subhecha(void);
void display_Wishes(void);
void display_Leaf(void);
void update_wishes(void);
void uninitialize_wishes(void);

// programmable pipeline related global veriable
GLuint shaderProgramObject;

enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

// Global Variables Declarations
int count_wishes = 1;

GLuint vao_wishes;
GLuint vbo_position_wishes;
GLuint vbo_texcoord_wishes; // texture

GLuint vao_leafRight;
GLuint vbo_position_leafRight;
GLuint vbo_texcoord_leafRight;

GLuint vao_leafLeft;
GLuint vbo_position_leafLeft;
GLuint vbo_texcoord_leafLeft;

GLuint textureRightLeaf; // texture
GLuint textureLeftLeaf;
GLuint textureWishes;
unsigned char* data_wishes;

GLfloat angleRotate_wishes = 90.0f;

GLuint mvpMatrixUniform_wishes;
GLuint textureSamplerUniform_wishes; // texture

mat4 perspectiveProjectionMatrix_wishes;

// Programable Pipeline related Variables
GLuint shaderProgramObject_wishes;

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void);
	void uninitialize(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iRetval = 0;

	// code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation Of Log File Faild. Exitting"), TEXT("File IO ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created  \n");
	}

	// initialization of WNDCLASS structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// register above class or wndclass 
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,//yache kam aase aahe ki window satat taskbar varti rahil.
		szAppName,
		TEXT("OpenGL Window - Mandar Maske RTR2021-116"),
		WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE, // WS_CLIPCHILDREN : jr mala chlid window aastil tr tyana maja area cover karun deu nakomala overlap karat aastil tr tyana clip kr majya vr kon nako, clip mhanje kapa
		(GetSystemMetrics(SM_CXSCREEN) / 2 - 400),
		(GetSystemMetrics(SM_CYSCREEN) / 2 - 300),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetval = initialize();
	
	if(iRetval== -1)
	{
		fprintf(gpFile, "Choose Pixel Format Faild \n");
		uninitialize();
	}
	if (iRetval == -2)
	{
		fprintf(gpFile, "Set Pixel Format Faild \n");
		uninitialize();
	}

	if (iRetval == -3)
	{
		fprintf(gpFile, "Create OpenGL Context Faild \n");
		uninitialize();
	}

	if (iRetval == -4)
	{
		fprintf(gpFile, "Making OpenGl Context As Current Context Faild \n");
		uninitialize();
	}

	if (iRetval == -5)
	{
		fprintf(gpFile, "glew Faild \n");
		uninitialize();
	}

	if (iRetval == -6)
	{
		fprintf(gpFile, "Failed to load smiley texture \n");
		uninitialize();
	}

	// show window
	ShowWindow(hwnd, iCmdShow);

	// foregrounding and focusing the window
	SetForegroundWindow(hwnd);//here we use ghwnd and hwnd,but here ghwnd used for global function so we use hwnd
	SetFocus(hwnd);

	// game loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();
			}
		}
	}
	uninitialize();

	return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void resize(int, int);
	

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);
		//break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 27:
			DestroyWindow(hwnd);
			break;

		default:
			break;
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));//resize(width , hight);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//varible declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = FALSE;

	}

}

int initialize(void)//convetion follow karayche mhanun initialize small
{
	// function declarations
	void printGLInfo(void);
	void uninitialize(void);
	void resize(int width, int height);
	BOOL LoadGLTexture(GLuint*, TCHAR[]);

	// veriable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// initialization of pixelformatdescriptortable
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	// getdc
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	
	// set the choosen pixel format
	// code
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
		
	}
	// create OpenGl rendering context
	ghrc = wglCreateContext(ghdc);//briging API

	if (ghrc == NULL)
	{
		return(-3);
	}
	// make the rendering context to current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
		
	}

	// stepA 3 : initializaton of glew
	// glew initializaton
	if (glewInit() != GLEW_OK)
	{
		return(-5);
	}

	// print OpenGLInfo
	printGLInfo();

	// StepC 1 : writing shading code
	// Vertex Shader
	const GLchar* vertexShaderSourceCode_wishes =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec2 a_texcoord;" \
		"uniform mat4 u_mvpMatrix;" \
		"out vec2 a_texcoord_out;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * a_position;" \
		"a_texcoord_out = a_texcoord;" \
		"}";

	GLuint vertexShaderObject_wishes = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShaderObject_wishes, 1, (const GLchar**)&vertexShaderSourceCode_wishes, NULL);

	// compile to code for GPU understandable using inline compiler from GPU
	glCompileShader(vertexShaderObject_wishes);

	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// error checking
	//getting compilation Status
	glGetShaderiv(vertexShaderObject_wishes, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_wishes, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_wishes, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", log);
				free(log);
				uninitialize_wishes();
			}
		}
	}

	// Fragment Shader
	const GLchar* fragmentShaderSourceCode_wishes =
		"#version 460 core" \
		"\n" \
		"in vec2 a_texcoord_out;" \
		"uniform sampler2D u_texturesampler;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec4 texcolor = texture(u_texturesampler, a_texcoord_out);" \
		"if(texcolor.a < 0.1)" \
		"{" \
		"discard;" \
		"}" \
		"FragColor = texcolor;" \
		"}";

	/*Sampler: */

	GLuint fragmentShaderObject_wishes = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(fragmentShaderObject_wishes, 1, (const GLchar**)&fragmentShaderSourceCode_wishes, NULL);

	// compile  the source code
	glCompileShader(fragmentShaderObject_wishes);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetShaderiv(fragmentShaderObject_wishes, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_wishes, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_wishes, infoLogLength, &written, log);
				fprintf(gpFile, "Fragmentt Shader Compilation Log: %s\n", log);
				free(log);
				uninitialize_wishes();
			}
		}
	}

	// Shader program Object
	shaderProgramObject_wishes = glCreateProgram();

	glAttachShader(shaderProgramObject_wishes, vertexShaderObject_wishes);
	glAttachShader(shaderProgramObject_wishes, fragmentShaderObject_wishes);

	// Prelinked binding of shader program object
	glBindAttribLocation(shaderProgramObject_wishes, AMC_ATTRIBUTE_POSITION, "a_position"); // first reason for black screen
	glBindAttribLocation(shaderProgramObject_wishes, AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

	glLinkProgram(shaderProgramObject_wishes);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetProgramiv(shaderProgramObject_wishes, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_wishes, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject_wishes, infoLogLength, &written, log);
				fprintf(gpFile, "Shader Program Link Log: %s\n", log);
				free(log);
				uninitialize_wishes();
			}
		}
	}

	// Post Linking
	mvpMatrixUniform_wishes = glGetUniformLocation(shaderProgramObject_wishes, "u_mvpMatrix"); // this is done after linking
	textureSamplerUniform_wishes = glGetUniformLocation(shaderProgramObject_wishes, "u_texturesampler");

	// declaration of vertex data arrays
	const GLfloat position_wishes[] =
	{
		1.0,1.0,0.0,
		-1.0,1.0,0.0,
		-1.0,-1.0,0.0,
		1.0,-1.0,0.0
	};

	const GLfloat texcoord_wishes[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
	};

	// vao and vbo related code
	// vao
	// quad
	glGenVertexArrays(1, &vao_wishes);
	glBindVertexArray(vao_wishes);

	// vbo for position
	glGenBuffers(1, &vbo_position_wishes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_wishes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(position_wishes), position_wishes, GL_STATIC_DRAW);

	// imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glGenBuffers(1, &vbo_texcoord_wishes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texcoord_wishes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoord_wishes), texcoord_wishes, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // Stop Recording


	const GLfloat wishesQuadPositionRight[] =
	{
		1.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, -1.0, 0.0,
		1.0, -1.0, 0.0,
	};

	const GLfloat wishesQuadPositionLeft[] =
	{
		0.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
	};

	// vao
	// quad
	glGenVertexArrays(1, &vao_leafRight);
	glBindVertexArray(vao_leafRight);

	// vbo for position
	glGenBuffers(1, &vbo_position_leafRight);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_leafRight);
	glBufferData(GL_ARRAY_BUFFER, sizeof(wishesQuadPositionRight), wishesQuadPositionRight, GL_STATIC_DRAW);

	// imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// vbo for Texture
	glGenBuffers(1, &vbo_texcoord_leafRight);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texcoord_leafRight);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoord_wishes), texcoord_wishes, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // Stop Recording

	// vao
	// quad
	glGenVertexArrays(1, &vao_leafLeft);
	glBindVertexArray(vao_leafLeft);

	// vbo for position
	glGenBuffers(1, &vbo_position_leafLeft);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_leafLeft);
	glBufferData(GL_ARRAY_BUFFER, sizeof(wishesQuadPositionLeft), wishesQuadPositionLeft, GL_STATIC_DRAW);

	// imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// vbo for Texture
	glGenBuffers(1, &vbo_texcoord_leafLeft);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texcoord_leafLeft);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoord_wishes), texcoord_wishes, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // Stop Recording

	//glBindVertexArray(0); // Stop Recording

	if (loadStbImage(&textureLeftLeaf, "leftLeaf.png") == FALSE)
	{
		fprintf(gpFile, "loadStdImage failed for Left Leaf.\n");
		uninitialize_wishes();
	}

	if (loadStbImage(&textureRightLeaf, "rightLeaf.png") == FALSE)
	{
		fprintf(gpFile, "loadStdImage failed for Right Leaf.\n");
		uninitialize_wishes();
	}

	if (loadStbImage(&textureWishes, "birthday_wishes.png") == FALSE)
	{
		fprintf(gpFile, "loadStdImage failed for Texture Wishes.\n");
		uninitialize_wishes();
	}

	glEnable(GL_TEXTURE_2D);

	perspectiveProjectionMatrix_wishes = mat4::identity();


	if (LoadGLTexture(&texture_smiley, MAKEINTRESOURCE(ID_BITMAP_SMILEY)) == FALSE)
		return(-6);

	// enabling the texture
	glEnable(GL_TEXTURE_2D);

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

BOOL loadStbImage(GLuint* texture, char* filename)
{
	int width, height;
	int components;

	unsigned char* imageData = stbi_load(filename, &width, &height, &components, STBI_rgb_alpha);

	BOOL bResult = FALSE;

	// Code
	if (imageData)
	{
		bResult = TRUE;
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);

		glBindTexture(GL_TEXTURE_2D, 0);

		stbi_image_free(imageData);
	}

	return bResult;
}

//  stepB 
void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	// StepB 1 : printing OpenGL vendor,renderer,version,GLSL version 
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	// StepB 2 : printing no. of supproted extension & names of extension
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}


void resize(int width, int height)
{
	// code
	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject);

	// Square
	// transformations
	mat4 translationMatrix= mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); //glTranslatef() is replaced by this line
	modelViewMatrix = translationMatrix; // order is imp
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // order imp aahe
	
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  

	display_subhecha();
	display_Leaf();

	// stepE 3 : unuse the shader program object
	
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen==TRUE)
	{
		ToggleFullScreen();
	}

	if (texture_smiley)
	{
		glDeleteTextures(1, &texture_smiley);
		texture_smiley = 0;
	}

	if (vbo_texcoord)
	{
		glDeleteBuffers(1, &vbo_texcoord);
		vbo_texcoord = 0;
	}

	// deletion/ uninitialization of vbo
	if (vbo_position)
	{
		glDeleteBuffers(1, &vbo_position);
		vbo_position = 0;
	}

	// deletion/ uninitialization of vao
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	// shader uninitialization
	if (shaderProgramObject)
	{
		// stepF 0
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed");
		fclose(gpFile);
		gpFile = NULL;
	}
}

