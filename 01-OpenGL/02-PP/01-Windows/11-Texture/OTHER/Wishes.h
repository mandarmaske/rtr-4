#pragma once
#include<gl\glew.h> 
#include<GL\gl.h>
#include"vmath.h"

// function declarations
void initialize_wishes(void);
BOOL loadStbImage(GLuint*, char*);
void display_subhecha(void);
void display_Wishes(void);
void display_Leaf(void);
void update_wishes(void);
void uninitialize_wishes(void);

// Global Variables Declarations
int count_wishes = 1;

GLuint vao_wishes;
GLuint vbo_position_wishes;
GLuint vbo_texcoord_wishes; // texture

GLuint vao_leafRight;
GLuint vbo_position_leafRight;
GLuint vbo_texcoord_leafRight;

GLuint vao_leafLeft;
GLuint vbo_position_leafLeft;
GLuint vbo_texcoord_leafLeft;

GLuint textureRightLeaf; // texture
GLuint textureLeftLeaf;
GLuint textureWishes;
unsigned char* data_wishes;

GLfloat angleRotate_wishes = 90.0f;

GLuint mvpMatrixUniform_wishes;
GLuint textureSamplerUniform_wishes; // texture

mat4 perspectiveProjectionMatrix_wishes;

// Programable Pipeline related Variables
GLuint shaderProgramObject_wishes;

void initialize_wishes(void)
{
	// Vertex Shader
	const GLchar* vertexShaderSourceCode_wishes =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec2 a_texcoord;" \
		"uniform mat4 u_mvpMatrix;" \
		"out vec2 a_texcoord_out;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * a_position;" \
		"a_texcoord_out = a_texcoord;" \
		"}";

	GLuint vertexShaderObject_wishes = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShaderObject_wishes, 1, (const GLchar**)&vertexShaderSourceCode_wishes, NULL);

	// compile to code for GPU understandable using inline compiler from GPU
	glCompileShader(vertexShaderObject_wishes);

	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// error checking
	//getting compilation Status
	glGetShaderiv(vertexShaderObject_wishes, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_wishes, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_wishes, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", log);
				free(log);
				uninitialize_wishes();
			}
		}
	}

	// Fragment Shader
	const GLchar* fragmentShaderSourceCode_wishes =
		"#version 460 core" \
		"\n" \
		"in vec2 a_texcoord_out;" \
		"uniform sampler2D u_texturesampler;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec4 texcolor = texture(u_texturesampler, a_texcoord_out);" \
		"if(texcolor.a < 0.1)" \
		"{" \
		"discard;" \
		"}" \
		"FragColor = texcolor;" \
		"}";

	/*Sampler: */

	GLuint fragmentShaderObject_wishes = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(fragmentShaderObject_wishes, 1, (const GLchar**)&fragmentShaderSourceCode_wishes, NULL);

	// compile  the source code
	glCompileShader(fragmentShaderObject_wishes);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetShaderiv(fragmentShaderObject_wishes, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_wishes, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_wishes, infoLogLength, &written, log);
				fprintf(gpFile, "Fragmentt Shader Compilation Log: %s\n", log);
				free(log);
				uninitialize_wishes();
			}
		}
	}

	// Shader program Object
	shaderProgramObject_wishes = glCreateProgram();

	glAttachShader(shaderProgramObject_wishes, vertexShaderObject_wishes);
	glAttachShader(shaderProgramObject_wishes, fragmentShaderObject_wishes);

	// Prelinked binding of shader program object
	glBindAttribLocation(shaderProgramObject_wishes, AMC_ATTRIBUTE_POSITION, "a_position"); // first reason for black screen
	glBindAttribLocation(shaderProgramObject_wishes, AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

	glLinkProgram(shaderProgramObject_wishes);

	status = 0;
	infoLogLength = 0;
	log = NULL;

	glGetProgramiv(shaderProgramObject_wishes, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_wishes, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject_wishes, infoLogLength, &written, log);
				fprintf(gpFile, "Shader Program Link Log: %s\n", log);
				free(log);
				uninitialize_wishes();
			}
		}
	}

	// Post Linking
	mvpMatrixUniform_wishes = glGetUniformLocation(shaderProgramObject_wishes, "u_mvpMatrix"); // this is done after linking
	textureSamplerUniform_wishes = glGetUniformLocation(shaderProgramObject_wishes, "u_texturesampler");

	// declaration of vertex data arrays
	const GLfloat position_wishes[] =
	{
		1.0,1.0,0.0,
		-1.0,1.0,0.0,
		-1.0,-1.0,0.0,
		1.0,-1.0,0.0
	};

	const GLfloat texcoord_wishes[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
	};

	// vao and vbo related code
	// vao
	// quad
	glGenVertexArrays(1, &vao_wishes);
	glBindVertexArray(vao_wishes);

	// vbo for position
	glGenBuffers(1, &vbo_position_wishes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_wishes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(position_wishes), position_wishes, GL_STATIC_DRAW);

	// imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glGenBuffers(1, &vbo_texcoord_wishes);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texcoord_wishes);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoord_wishes), texcoord_wishes, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // Stop Recording


	const GLfloat wishesQuadPositionRight[] =
	{
		1.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, -1.0, 0.0,
		1.0, -1.0, 0.0,
	};

	const GLfloat wishesQuadPositionLeft[] =
	{
		0.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
	};

	// vao
	// quad
	glGenVertexArrays(1, &vao_leafRight);
	glBindVertexArray(vao_leafRight);

	// vbo for position
	glGenBuffers(1, &vbo_position_leafRight);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_leafRight);
	glBufferData(GL_ARRAY_BUFFER, sizeof(wishesQuadPositionRight), wishesQuadPositionRight, GL_STATIC_DRAW);

	// imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// vbo for Texture
	glGenBuffers(1, &vbo_texcoord_leafRight);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texcoord_leafRight);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoord_wishes), texcoord_wishes, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // Stop Recording

	// vao
	// quad
	glGenVertexArrays(1, &vao_leafLeft);
	glBindVertexArray(vao_leafLeft);

	// vbo for position
	glGenBuffers(1, &vbo_position_leafLeft);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_leafLeft);
	glBufferData(GL_ARRAY_BUFFER, sizeof(wishesQuadPositionLeft), wishesQuadPositionLeft, GL_STATIC_DRAW);

	// imp
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// vbo for Texture
	glGenBuffers(1, &vbo_texcoord_leafLeft);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_texcoord_leafLeft);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoord_wishes), texcoord_wishes, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // Stop Recording

	//glBindVertexArray(0); // Stop Recording

	if (loadStbImage(&textureLeftLeaf, "leftLeaf.png") == FALSE)
	{
		fprintf(gpFile, "loadStdImage failed for Left Leaf.\n");
		uninitialize_wishes();
	}

	if (loadStbImage(&textureRightLeaf, "rightLeaf.png") == FALSE)
	{
		fprintf(gpFile, "loadStdImage failed for Right Leaf.\n");
		uninitialize_wishes();
	}

	if (loadStbImage(&textureWishes, "birthday_wishes.png") == FALSE)
	{
		fprintf(gpFile, "loadStdImage failed for Texture Wishes.\n");
		uninitialize_wishes();
	}

	glEnable(GL_TEXTURE_2D);

	perspectiveProjectionMatrix_wishes = mat4::identity();
}

BOOL loadStbImage(GLuint* texture, char* filename)
{
	int width, height;
	int components;

	unsigned char* imageData = stbi_load(filename, &width, &height, &components, STBI_rgb_alpha);

	BOOL bResult = FALSE;

	// Code
	if (imageData)
	{
		bResult = TRUE;
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);

		glBindTexture(GL_TEXTURE_2D, 0);

		stbi_image_free(imageData);
	}

	return bResult;
}

void display_subhecha(void)
{
	// function declarations
	void display_Leaf(void);
	void display_Wishes(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// USE THE SHADER PROGRAM OBJECT
	glUseProgram(shaderProgramObject_wishes);

	// here there should be the drawing code
	display_Leaf();
	if (count_wishes == 2)
	{
		display_Wishes();
	}

	// UNUSE THE SHADER PROGRAM OBJECT
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void display_Wishes(void)
{
	mat4 translationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f); // glTranslatef() is replaced by this line
	scaleMatrix = vmath::scale(0.75f, 0.75f, 0.0f);
	modelViewMatrix = translationMatrix * scaleMatrix; // order is very important

	// Steps for transformations
	modelViewProjectionMatrix = perspectiveProjectionMatrix_wishes * modelViewMatrix;

	glUniformMatrix4fv(mvpMatrixUniform_wishes, 1, GL_FALSE, modelViewProjectionMatrix);

	// for texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureWishes);
	glUniform1i(textureSamplerUniform_wishes, 0);
	glBindVertexArray(vao_wishes);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void display_Leaf(void)
{
	// "QUAD" 
	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	mat4 rotateMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f); // glTranslatef() is replaced by this line
	rotateMatrix = vmath::rotate(-angleRotate_wishes, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = translationMatrix * scaleMatrix * rotateMatrix; // order is very important

	// Steps for transformations
	modelViewProjectionMatrix = perspectiveProjectionMatrix_wishes * modelViewMatrix;

	glUniformMatrix4fv(mvpMatrixUniform_wishes, 1, GL_FALSE, modelViewProjectionMatrix);

	// for texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureLeftLeaf);
	glUniform1i(textureSamplerUniform_wishes, 0);
	glBindVertexArray(vao_leafRight);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	rotateMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f); // glTranslatef() is replaced by this line
	rotateMatrix = vmath::rotate(angleRotate_wishes, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = translationMatrix * scaleMatrix * rotateMatrix; // order is very important

	// Steps for transformations
	modelViewProjectionMatrix = perspectiveProjectionMatrix_wishes * modelViewMatrix;

	glUniformMatrix4fv(mvpMatrixUniform_wishes, 1, GL_FALSE, modelViewProjectionMatrix);

	// for texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureRightLeaf);
	glUniform1i(textureSamplerUniform_wishes, 0);
	glBindVertexArray(vao_leafLeft);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

}

void update_wishes(void)
{
	// code
	if (count_wishes == 1)
	{
		angleRotate_wishes = angleRotate_wishes - 0.5f;
		if (angleRotate_wishes <= -3.0f)
			count_wishes = 2;
	}
}

void uninitialize_wishes(void)
{
	
	// texture
	if (textureRightLeaf)
	{
		glDeleteTextures(1, &textureRightLeaf);
		textureRightLeaf = 0;
	}

	if (textureLeftLeaf)
	{
		glDeleteTextures(1, &textureLeftLeaf);
		textureLeftLeaf = 0;
	}

	if (textureWishes)
	{
		glDeleteTextures(1, &textureWishes);
		textureWishes = 0;
	}


	// deleting and uninitialization of vbo
	if (vbo_texcoord_wishes)
	{
		glDeleteBuffers(1, &vbo_texcoord_wishes);
		vbo_texcoord_wishes = 0;
	}

	if (vbo_texcoord_leafRight)
	{
		glDeleteBuffers(1, &vbo_texcoord_leafRight);
		vbo_texcoord_leafRight = 0;
	}

	if (vbo_texcoord_leafLeft)
	{
		glDeleteBuffers(1, &vbo_texcoord_leafLeft);
		vbo_texcoord_leafLeft = 0;
	}

	if (vbo_position_wishes)
	{
		glDeleteBuffers(1, &vbo_position_wishes);
		vbo_position_wishes = 0;
	}

	// deleting and unitiallization of vao
	if (vao_wishes)
	{
		glDeleteVertexArrays(1, &vao_wishes);
		vao_wishes = 0;
	}

	// Shader Uninitialization
	if (shaderProgramObject_wishes)
	{
		glUseProgram(shaderProgramObject_wishes);

		GLsizei numAttachedShaders;

		glGetProgramiv(shaderProgramObject_wishes, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjcets = NULL;
		shaderObjcets = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// filling this empty buffer by shader object
		glGetAttachedShaders(shaderProgramObject_wishes, numAttachedShaders, &numAttachedShaders, shaderObjcets);

		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject_wishes, shaderObjcets[i]);
			glDeleteShader(shaderObjcets[i]);

			shaderObjcets[i] = 0;
		}

		free(shaderObjcets);
		shaderObjcets = NULL;

		glUseProgram(0);

		glDeleteProgram(shaderProgramObject_wishes);

		shaderProgramObject_wishes = 0;
	}
}