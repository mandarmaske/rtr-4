// copy above bluecolor background color 

* assuming we wrote respective shader code
A: in initialize()
1. perlinked binding of shader progrm object with vertex attributes
2. postlinked retriving/getting uniform location from the shader program object
3. declair vertex data array
4. create vertex array object
5. bind respective vertex array object & start recording buffer & bufferdata related steps
a. create vertex data buffer & start recording 
b. bind with vertex data buffer
c. create storage for buffer data for a perticular target
d. specify where, how much ,which buffer data to be considered as vertex data arrays
e. enable the respectve buffer binding vertex array target
f. unbind with the respective vertex data buffer
6. unbind with the respective vertex object & so stop recording 
7. initialize needed matrices using vmath

B: in resize use vmath for initialization of perticular projection matrix(in this eg. it is orthographic projection matrix)

C: in display()
1. use the shader program object(this step is used in prevoius program)
2. transformation related steps
a. declair & initialize needed matrices
b. do translation,rotation,scale transformations using vmath
c. do necessary matrix multiplications
3. send above transformation matrix/matrices to the shader in respective matrix uniforms
4. bind with vertex array object
5. draw necessary scene/drawing (this steps mentation in prevoius program)
6. unbind with vertex array object
7. unuse shader program object (this steps mentation in prevoius program)

D: in uninitialize()
1. delete/uninitialize cerated vertex buffer objects
2. delete/uninitialize cerated vertex array objects

// globally add one header file
#include"vmath.h"
using namespace vmath;

// programable pipeline related global veriable
enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};
GLuint vao;
GLuint vbo;
GLuint mvpMatrixUniform;
mat4 orthographicProjectionMatrix;

// in initialize () vertex shader code
//after "\n" and above "void main(void)" \ add some line
"in vec4 a_position;" \
"uniform mat4 u_mvpMatrix;" \

// in void main chya curly bress chya aat
"gl_Position = u_mvpMatrix * a_position;" \


// fragment shader 
//after "\n" and above "void main(void)" \ add some line
"out vec4 FragColor;" \

// in void main chya curly bress chya aat
"FragColor = vec4(1.0,1.0,1.0,1.0);" \

// after glAttachShader 2 lines
glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");

// after linking error chacking block add below lines
mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

// above depth related chages add below lines
// vao & vbo related code
	//declarations of vertex data arrays
	const GLfloat triangleVertices[] =
	{
		0.0f,50.0f,0.0f,
		-50.0,-50.f,0.0f,
		50.0f,-50.0f,0.0f
	};

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

// above warmup resize call add below line
mat4::identity();

in resize()
// after glViewport() add below line

if (width <= height)
	{
		orthographicProjectionMatrix=vmath::ortho (- 100.0f,
			100.0f,
			(-100.0f * ((GLfloat)height / (GLfloat)width)),
			(100.0f * ((GLfloat)height / (GLfloat)width)),
			-100.0f,
			100.0f);
	}
	else
	{
		orthographicProjectionMatrix = vmath::ortho((-100.0f * ((GLfloat)width / (GLfloat)height)),
			(100.0f * ((GLfloat)width / (GLfloat)height)),
			-100.0f,
			100.0f,
			-100.0f,
			100.0f);
	}

in display()
// stepE 1 :  use the shader program object

	// transformations
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	
	modelViewProjectionMatrix = orthographicProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glBindVertexArray(0);

in uninitialize()

// fullscreen chya khali aani shader program chya varti
// deletion/ uninitialization of vbo
	if (vbo)
	{
		glDelteBuffers(1, &vbo);
		vbo = 0;
	}

	// deletion/ uninitialization of vao
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}



























