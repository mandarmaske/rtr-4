// header file
#include<windows.h>
#include"OGL.h"//icon
#include<stdio.h> // for file io functions
#include<stdlib.h> // for exit()

// OpenGL header file
// step A 1 : include file
#include<gl\glew.h> // this must be before including gl.h
#include<GL\gl.h>
#include"vmath.h"
using namespace vmath;

// OpenGL library
// stepA 2 : lnking with import library
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"OpenGL32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable declarations
BOOL gbActiveWindow = FALSE;
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE* gpFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// programable pipeline related global veriable
GLuint shaderProgramObject;
enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

GLuint vao_pyramid;
GLuint vbo_pyramid_position;
GLuint vbo_pyramid_normal;
GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix;

GLfloat anglePyramid = 0.0f;

BOOL bLight = FALSE;

// Uniform
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint laUniform[2]; // light ambient
GLuint ldUniform[2]; // light diffuse
GLuint lsUniform[2]; // light specular
GLuint lightPositionUniform[2];

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShininessUniform;

GLuint lightingEnabledUniform;

struct Light
{
	vec4 lightAmbient;
	vec4 lightDiffuse;
	vec4 lightSpecular;
	vec4 lightPosition;
};

Light lights[2];

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iRetval = 0;

	// code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation Of Log File Faild. Exitting"), TEXT("File IO ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created  \n");
	}

	// initialization of WNDCLASS structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// register above class or wndclass 
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,//yache kam aase aahe ki window satat taskbar varti rahil.
		szAppName,
		TEXT("OGL Window"),
		WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE, // WS_CLIPCHILDREN : jr mala chlid window aastil tr tyana maja area cover karun deu nakomala overlap karat aastil tr tyana clip kr majya vr kon nako, clip mhanje kapa
		//|WS_CLIPSIBLINGS:SIBLINGS mhaje bhau bhau majya bhav jari mala overlap karat aastil tr tyana clip kr majya vr kon nako
		//|WS_VISIBLE:initially make me visibe even if there WM_PAINT
		(GetSystemMetrics(SM_CXSCREEN) / 2 - 400),
		(GetSystemMetrics(SM_CYSCREEN) / 2 - 300),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetval = initialize();
	
	if(iRetval== -1)
	{
		fprintf(gpFile, "Choose Pixel Format Faild \n");
		uninitialize();
	}
	if (iRetval == -2)
	{
		fprintf(gpFile, "Set Pixel Format Faild \n");
		uninitialize();
	}

	if (iRetval == -3)
	{
		fprintf(gpFile, "Create OpenGL Context Faild \n");
		uninitialize();
	}

	if (iRetval == -4)
	{
		fprintf(gpFile, "Making OpenGl Context As Current Context Faild \n");
		uninitialize();
	}

	if (iRetval == -5)
	{
		fprintf(gpFile, "glew Faild \n");
		uninitialize();
	}

	// show window
	ShowWindow(hwnd, iCmdShow);

	// foregrounding and focusing the window
	SetForegroundWindow(hwnd);//here we use ghwnd and hwnd,but here ghwnd used for global function so we use hwnd
	SetFocus(hwnd);

	// game loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}
	uninitialize();

	return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void resize(int, int);
	

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);
		//break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		case 'L':
		case 'l':
			if (bLight == FALSE)
			{
				bLight = TRUE;
			}
			else
			{
				bLight = FALSE;
			}
			break;

		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 27:
			DestroyWindow(hwnd);
			break;

		default:
			break;
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));//resize(width , hight);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//varible declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = FALSE;

	}

}

int initialize(void)//convetion follow karayche mhanun initialize small
{
	// function declarations
	void printGLInfo(void);
	void uninitialize(void);
	void resize(int width, int height);

	// veriable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// initialization of pixelformatdescriptortable
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	// getdc
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	// set the choosen pixel format
	// code
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);

	}
	// create OpenGl rendering context
	ghrc = wglCreateContext(ghdc);//briging API

	if (ghrc == NULL)
	{
		return(-3);
	}
	// make the rendering context to current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);

	}

	// stepA 3 : initializaton of glew
	// glew initializaton
	if (glewInit() != GLEW_OK)
	{
		return(-5);
	}

	// print OpenGLInfo
	printGLInfo();

	// StepC 1 : writing shading code
	// vertex shader 
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec4 u_lightPosition[2];" \
		"uniform int u_lightingEnabled;" \
		"out vec3 transformedNormals;" \
		"out vec3 lightDirection[2];" \
		"out vec3 viewerVector;" \
		"void main(void)" \
		"{" \
			"if(u_lightingEnabled == 1)" \
			"{" \
				"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
				"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
				"transformedNormals = normalMatrix * a_normal;" \
				"viewerVector = -eyeCoordinates.xyz;" \
				"for(int i = 0; i < 2; i++)" \
				"{" \
					"lightDirection[i] = vec3(u_lightPosition[i]) - eyeCoordinates.xyz;" \
				"}" \
			"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);  
		
		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
				
				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec3 transformedNormals;" \
		"in vec3 lightDirection[2];" \
		"in vec3 viewerVector;" \
		"uniform vec3 u_la[2];" \
		"uniform vec3 u_ld[2];" \
		"uniform vec3 u_ls[2];" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShininess;" \
		"uniform int u_lightingEnabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_light;" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec3 ambient[2];" \
		"vec3 diffuse[2];" \
		"vec3 reflectionVector[2];" \
		"vec3 specular[2];" \
		"vec3 normalized_transformed_normal;" \
		"vec3 normalized_lightDirection[2];" \
		"vec3 normalized_viewerVector;" \
		"normalized_transformed_normal = normalize(transformedNormals); " \
		"normalized_viewerVector = normalize(viewerVector);" \
		"for(int i = 0; i < 2; i++)" \
		"{" \
		"normalized_lightDirection[i] = normalize(lightDirection[i]);" \
		"ambient[i] = u_la[i] * u_ka;" \
		"diffuse[i] = u_ld[i] * u_kd * max(dot(normalized_lightDirection[i], normalized_transformed_normal), 0.0);" \
		"reflectionVector[i] = reflect(-normalized_lightDirection[i], normalized_transformed_normal);" \
		"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalized_viewerVector), 0.0), u_materialShininess);" \
		"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" \
		"}" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_light,1.0);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
	
	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, fragmentShaderObject);
	
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize();

			}
		}
	}

	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

	// light uniform
	laUniform[0] = glGetUniformLocation(shaderProgramObject, "u_la[0]");
	ldUniform[0] = glGetUniformLocation(shaderProgramObject, "u_ld[0]");
	lsUniform[0] = glGetUniformLocation(shaderProgramObject, "u_ls[0]");
	lightPositionUniform[0] = glGetUniformLocation(shaderProgramObject, "u_lightPosition[0]");

	laUniform[1] = glGetUniformLocation(shaderProgramObject, "u_la[1]");
	ldUniform[1] = glGetUniformLocation(shaderProgramObject, "u_ld[1]");
	lsUniform[1] = glGetUniformLocation(shaderProgramObject, "u_ls[1]");
	lightPositionUniform[1] = glGetUniformLocation(shaderProgramObject, "u_lightPosition[1]");

	// material uniform
	kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");

	// light enable uniform
	lightingEnabledUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

	// vao & vbo related code
	//declarations of vertex data arrays
	const GLfloat pyramidPosition[] =
	{ 
		// front
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		// right
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		// back
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		// left
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	const GLfloat pyramidNormal[] =
	{
		0.0f, 0.447214f, 0.894427f,// front-top
		0.0f, 0.447214f, 0.894427f,// front-left
		0.0f, 0.447214f, 0.894427f,// front-right

		0.894427f, 0.447214f, 0.0f, // right-top
		0.894427f, 0.447214f, 0.0f, // right-left
		0.894427f, 0.447214f, 0.0f, // right-right

		0.0f, 0.447214f, -0.894427f, // back-top
		0.0f, 0.447214f, -0.894427f, // back-left
		0.0f, 0.447214f, -0.894427f, // back-right

		-0.894427f, 0.447214f, 0.0f, // left-top
		-0.894427f, 0.447214f, 0.0f, // left-left
		-0.894427f, 0.447214f, 0.0f // left-right
	};

	// pyramid
	// vao
	glGenVertexArrays(1, &vao_pyramid); // vao:- vertex array object
	glBindVertexArray(vao_pyramid);

	// vbo for position
	glGenBuffers(1, &vbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position); //bind buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidPosition), pyramidPosition, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// vbo for normals
	glGenBuffers(1, &vbo_pyramid_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// here start OpenGL code (depth & clear color code)
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	lights[0].lightAmbient = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	lights[0].lightDiffuse = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f); // Red Light
	lights[0].lightSpecular = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	lights[0].lightPosition = vmath::vec4(-2.0f, 0.0f, 0.0f, 1.0f);

	lights[1].lightAmbient = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	lights[1].lightDiffuse = vmath::vec4(0.0f, 0.0f, 1.0f, 1.0f); // Blue Light
	lights[1].lightSpecular = vmath::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	lights[1].lightPosition = vmath::vec4(2.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

//  stepB 
void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	// StepB 1 : printing OpenGL vendor,renderer,version,GLSL version 
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	// StepB 2 : printing no. of supproted extension & names of extension
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}


void resize(int width, int height)
{
	// code
	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject);

	// triangle
	// transformations
	mat4 translationMatrix= mat4::identity();
	mat4 rotationMatrix= mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); //glTranslatef() is replaced by this line
	rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
	modelMatrix = translationMatrix * rotationMatrix; // order is imp
	
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE)
	{
		glUniform1i(lightingEnabledUniform, 1);

		for (int i = 0; i < 2; i++)
		{
			glUniform3fv(laUniform[i], 1, lights[i].lightAmbient);
			glUniform3fv(ldUniform[i], 1, lights[i].lightDiffuse);
			glUniform3fv(lsUniform[i], 1, lights[i].lightSpecular);
			glUniform4fv(lightPositionUniform[i], 1, lights[i].lightPosition);
		}

		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}
	else
	{
		glUniform1i(lightingEnabledUniform, 0);
	}

	glBindVertexArray(vao_pyramid);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code
	//glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position);
	glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0);

	// stepE 3 : unuse the shader program object
	
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	// code
	anglePyramid = anglePyramid + 0.5f;

	if (anglePyramid >= 360.0f)
		anglePyramid = anglePyramid - 360.0f;
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen==TRUE)
	{
		ToggleFullScreen();
	}

	// deletion/ uninitialization of vbo
	if (vbo_pyramid_normal)
	{
		glDeleteBuffers(1, &vbo_pyramid_normal);
		vbo_pyramid_normal = 0;
	}

	if (vbo_pyramid_position)
	{
		glDeleteBuffers(1, &vbo_pyramid_position);
		vbo_pyramid_position = 0;
	}

	// deletion/ uninitialization of vao
	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}

	// shader uninitialization
	if (shaderProgramObject)
	{
		// stepF 0
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed");
		fclose(gpFile);
		gpFile = NULL;
	}
}

