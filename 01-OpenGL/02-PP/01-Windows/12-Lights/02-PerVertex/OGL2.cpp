// Header Files
#include<windows.h>
#include<stdio.h> // for file io functions
#include<stdlib.h> // std library for exit()
#include "OGL.h"

//OpenGL Header files
#include<GL/glew.h> //THIS MUST BE BEFORE gl.h
#include<GL/gl.h>
#include "vmath.h"

using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
//OpenGL Libraies
#pragma comment(lib,"glew32.lib")  //Not Compulsary
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"Sphere.lib")

#include "Sphere.h"
// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Gloabal Variable declarations
HWND ghwnd = NULL;
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL; //For file I/O
BOOL gbActiveWindow = FALSE;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
BOOL bLight = FALSE;
//Programable Pipeline related global variable
GLuint shaderProgramObject;
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};
GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint laUniform; //light amibiet
GLuint ldUniform; //Diffuse
GLuint lsUniform; //Specular
GLuint lightPositionUniform;

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShininessUniform;

GLuint lightingEnabledUniform;


mat4 perspectiveProjectionMatrix;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

/*
GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialShininess = 50.0f;
*/
GLfloat LightAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat MaterialDiffuse[] = { 0.5f,0.2f,0.7f,1.0f };
GLfloat MaterialSpecular[] = { 0.7f,0.7f,0.7f,1.0f };
GLfloat MaterialShininess = 128.0f;

unsigned short gNumElements;
GLuint gNumVertices;

// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Function Declaration
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("VirendraWindow");
	BOOL bDone = FALSE;
	int iRetVal = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation of log file failed. Exiting..."), TEXT("File I/O Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is Successfully Created\n");
	}

	// Initialization of WNDCLASSEX structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register  WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create The window
	hwnd = CreateWindowExA(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Virendra OGL Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(GetSystemMetrics(SM_CXSCREEN) / 2) - (WIN_WIDTH / 2),
		(GetSystemMetrics(SM_CYSCREEN) / 2) - (WIN_HEIGHT / 2),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	iRetVal = initialize();
	if (iRetVal == -1)
	{
		fprintf(gpFile, "Choose pixel format failed\n");
		uninitialize();
	}
	else if (iRetVal == -2)
	{
		fprintf(gpFile, "Set pixel format failed\n");
		uninitialize();
	}
	else if (iRetVal == -3)
	{
		fprintf(gpFile, "Create OpenGL context failed\n");
		uninitialize();
	}
	else if (iRetVal == -4)
	{
		fprintf(gpFile, " Making OpenGl context as failed\n");
		uninitialize();
	}
	else if (iRetVal == -5)
	{
		fprintf(gpFile, " glew Initialization Failed\n");
		uninitialize();
	}
	else
	{
		fprintf(gpFile, "Initialisez Succesfull\n");
	}
	// Show Window
	ShowWindow(hwnd, iCmdShow);
	
	//foregrounding and Focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game Loop 
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// Render the Seen
				display();
				//Upadte the seen
				update();
			}
		}
	}
	uninitialize();
	return((int)msg.wParam);
}

// CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Function Declartion
	void ToggleFullScreen(void);
	void resize(int, int);

	// Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;
	case WM_ERASEBKGND:
		 return 0;//break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 27:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;
		case 'L':
		case 'l':
			if (bLight == FALSE)
			{
				bLight = TRUE;
			}
			else
			{
				bLight = FALSE;
			}
			break;
		default:
			break;
		}
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullScreen(void)
{
	//varibale declaraions
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;


	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullscreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullscreen = FALSE;
	}
}

int initialize(void)
{
	// Function declarations
	void uninitialize(void);
	void printGLInfo(void);
	void resize(int, int);
	// Variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	GLint status;
	GLint infoLogLength;
	char* log = NULL;
	// code
	//Initailization of pixel format desc structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));//memset((void *)&pfd,NULL,sizeof(PIXELFORMATDESCRIPTOR))
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; //24 also can be done

	//GetDC
	ghdc = GetDC(ghwnd);

	//Choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return -1;
	}
		//Set The chosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return -2;

	//Create OpenGL rendering Context
	ghrc=wglCreateContext(ghdc);
	if (ghrc == NULL)
		return -3;

	//Make rendering Context as current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return -4;

	//Glew Initialization
	if (glewInit() != GLEW_OK)
	{
		return -5;
	}
	//Print OpenGL info
	printGLInfo();

	//Vertex Shader
	//C1.Writing Shader Code
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec4 u_lightPosition;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShininess;" \
		"uniform int u_lightingEnabled;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled==1)" \
		"{" \
		"vec3 ambient=u_la*u_ka;" \
		"vec4 eyeCoordinates = u_viewMatrix*u_modelMatrix*a_position;" \
		"mat3 normalMatrix=mat3(u_viewMatrix*u_modelMatrix);" \
		"vec3 transformedNormals=normalize(normalMatrix*a_normal);" \
		"vec3 lightDirection=normalize(vec3(u_lightPosition)-eyeCoordinates.xyz);" \
		"vec3 diffuse=u_ld*u_kd*max(dot(lightDirection,transformedNormals),0.0);" \
		"vec3 reflectionVector=reflect(-lightDirection,transformedNormals);" \
		"vec3 viewerVector=normalize(-eyeCoordinates.xyz);" \
		"vec3 specular=u_ls*u_ks*pow(max(dot(reflectionVector,viewerVector),0.0),u_materialShininess);" \
		"phong_ads_light=ambient+diffuse+specular;" \
		"}" \
		"else" \
		"{" 
		"phong_ads_light=vec3(1.0,1.0,1.0);" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	//C2.Create shader objaect
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//C3.Give shader src code to shader Object
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	//C4.Compile Shader
	glCompileShader(vertexShaderObject);

	//C5.Error Checking of Shader Compilation
	

	//C5.a.Getting Compilation Status
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		//C5.b.Getting Length of message/logs of compilation status
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			//C5.c.Allocate Enough Memory to buffer to hold the compilation log
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				//C5.d Get the compilation log into this allocated buffer
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
				//C5.e Diplay Log or display contents of buffer
				fprintf(gpFile, "Vertex Shader Compilation log : %s\n", log);
				//C5.f.Free the allocated Buffer
				free(log);
				//C5.g.Exit the application due to error.
				uninitialize();
			}
		}
	}

	//Fragment Shader
	//C1.Writing fragement Shader Code
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_light,1.0);" \
		"}";

	//C2.Create Fragment shader objaect
	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//C3.Give shader src code to shader Object
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);

	//C4.Compile Shader
	glCompileShader(fragmentShaderObject);

	//C5.Error Checking of Shader Compilation
	status=0;
	infoLogLength=0;
	log = NULL;
	//C5.a.Getting Compilation Status
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		//C5.b.Getting Length of message/logs of compilation status
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			//C5.c.Allocate Enough Memory to buffer to hold the compilation log
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				//C5.d Get the compilation log into this allocated buffer
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
				//C5.e Diplay Log or display contents of buffer
				fprintf(gpFile, "Fragment Shader Compilation log : %s\n", log);
				//C5.f.Free the allocated Buffer
				free(log);
				//C5.g.Exit the application due to error.
				uninitialize();
			}
		}
	}
	//Creating,Linking Error of shader Program
	//D1.Create Shader Program Object
	shaderProgramObject = glCreateProgram();
    
	//D2.Attached Desired Shader to this Shader program Object
	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	//Prelink Binding
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");//Andhar
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");//Andhar

	//D3.Link Link Shader program Object
	glLinkProgram(shaderProgramObject);

	//D4.Do linking Error Checking 
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//D5.a.Error Checking of Shader Compilation
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		//D5.b.Getting Length of message/logs of compilation status
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			//D5.c.Allocate Enough Memory to buffer to hold the compilation log
			log = (char*)malloc(infoLogLength);
			if (log != NULL)
			{
				//D5.d Get the compilation log into this allocated buffer
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
				//D5.e Diplay Log or display contents of buffer
				fprintf(gpFile, "Shader Program Link log : %s\n", log);
				//D5.f.Free the allocated Buffer
				free(log);
				//D5.g.Exit the application due to error.
				uninitialize();
			}
		}
	}
	//Post Linking 
	modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");//Andhar
	viewMatrixUniform= glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform= glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");
	laUniform= glGetUniformLocation(shaderProgramObject, "u_la");
	ldUniform= glGetUniformLocation(shaderProgramObject, "u_ld");
	lsUniform= glGetUniformLocation(shaderProgramObject, "u_ls");//Specular
	lightPositionUniform= glGetUniformLocation(shaderProgramObject, "u_lightPosition");

	kaUniform= glGetUniformLocation(shaderProgramObject, "u_ka");
	kdUniform= glGetUniformLocation(shaderProgramObject, "u_kd");
	ksUniform= glGetUniformLocation(shaderProgramObject, "u_ks");
	materialShininessUniform= glGetUniformLocation(shaderProgramObject, "u_materialShininess");

	lightingEnabledUniform= glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

	//Declartion of Vertex data array
	
	
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();
	//VAO and VBO related code
	 // vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
	
	//Depth Realted Changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//glShadeModel(GL_SMOOTH); pp madhe kaam nahi
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  pp Kahi kaam Nahi
	//Clear the scrern using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();
	//Warm Up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	
	return 0;
}

void printGLInfo(void)
{
	//Variable Declarations
	GLint numExtensions=0;

	//code
	fprintf(gpFile, "OpenGL Vendor: %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	fprintf(gpFile, "Number Of Supported Extensions : %d\n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}
}
void resize(int width, int height)
{
	// Code
	if (height == 0) // to avoid divied by 0 in future call
		height = 1;
	glViewport(0, 0, width, height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width /(GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//E1.Use Shader Program Object
	glUseProgram(shaderProgramObject);

	//E2.Here There Should be drawing code 
	//Transformations
	mat4 translationMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	

	translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
	modelMatrix = translationMatrix;
	

	//b.
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelMatrix;

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (bLight == TRUE)
	{
		glUniform1i(lightingEnabledUniform, 1);

		glUniform3fv(laUniform,1, LightAmbient);
		glUniform3fv(ldUniform,1, LightDiffuse);
		glUniform3fv(lsUniform, 1, LightSpecular);
		glUniform4fv(lightPositionUniform, 1, LightPosition);

		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);

		glUniform1f(materialShininessUniform, MaterialShininess);
	}
	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);


	//E3.Unuse the shader program object
	glUseProgram(0);
	SwapBuffers(ghdc);
}

void update(void)
{
	// code
}

void uninitialize(void)
{
	// Function Declarations
	void ToggleFullScreen(void);
	// Code
	if (gbFullscreen)
	{
		ToggleFullScreen();
	}
	//Deletion and Uninitialzation of VBO
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}
	//Deletion and Unintialization of VAO
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}
	//F.Shader Uninitialization
	if (shaderProgramObject)
	{
		glUseProgram(shaderProgramObject);
		GLsizei numAttachedShaders;
		//F1.Get Number Of attached shaders
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
		GLuint* shaderObjects = NULL;
		//F2.Create Empty Buffer to hold array of attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));
		//F3.Allocate Enough Memory to this buffers according to the attached shaders fill it with attached shader objects
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);
		//F4.As The Number of attached shaders can be more than 1 start a loop and inside that loop dettached shader one by one,
		//delete shader 1 by 1 and finish the loop. 
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}
		//F5.Free The memmory allocated for the buffer.
		free(shaderObjects);
		shaderObjects = NULL;
		//F6.Unused shader program Object
		glUseProgram(0);
		//F7.Delete Shader Program Object
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}





