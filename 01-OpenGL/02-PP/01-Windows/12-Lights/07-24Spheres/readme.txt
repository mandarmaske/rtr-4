if (gbFullScreen == FALSE)
	{
		glViewport(0, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

if (gbFullScreen == FALSE)
	{
		glViewport(0, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

if (gbFullScreen == FALSE)
	{
		glViewport(0, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

if (gbFullScreen == FALSE)
	{
		glViewport(0, WIN_HEIGHT / 3.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 3.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

if (gbFullScreen == FALSE)
	{
		glViewport(0, WIN_HEIGHT / 6.5, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 6.5, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

if (gbFullScreen == FALSE)
	{
		glViewport(0, WIN_HEIGHT / 100.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 100.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}