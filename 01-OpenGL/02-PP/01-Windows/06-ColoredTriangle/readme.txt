Changes
1) GLuint vbo; change to GLuint vbo_position; (change everywhere it is written - renaming)
2) below global declarations of vbo_position declare new variable GLuint vbo_color;
3) in vertex shader after "in vec4 a_position;" \ press enter and add new line "in vec4 a_color;" \
4) after uniform line add new line "out vec4 a_color_out;" \
5) in main of vertexShader after gl_Position line add new line "a_color_out = a_color;" \
6) in fragment shader after "\n" \ add new line "in vec4 a_color_out;" \
7) in main of fragmentShader initialize "FragColor = a_color_out;" \
8) change triangleVertices to trianglePosition, and where it used (renaming)
9) declare new array const GLfloat triangleColor[] =
	{
		1.0f, 0.0f, 0.0f, // Red
		0.0f, 1.0f, 0.0, // Green
		0.0f, 0.0f, 1.0f // Blue
	};
10) after unbinding of vbo position add some lines for colors 
	glGenBuffers(1, &vbo_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
11) before that go to above code where we bind postion code (glBindAttribLocation())
add a new line - glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "a_color");
12) glClearColor() set to black all 0's
13) in unintialize add 3 lines before vbo_position code
if (vbo_color)
	{
		glDeleteBuffers(1, &vbo_color);
		vbo_color = 0;
	}


