// header file

#include<windows.h>
#include"OGL.h"//icon
#include<stdio.h> // for file io functions
#include<stdlib.h> // for exit()

// OpenGL header file
// step A 1 : include file
#include<GL\glew.h> // this must be before including gl.h
#include<GL\gl.h>
#include"vmath.h"
using namespace vmath;

// OpenGL library
// stepA 2 : lnking with import library
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"OpenGL32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable declarations
BOOL gbActiveWindow = FALSE;
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE* gpFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// programable pipeline related global veriable
GLuint shaderProgramObject;

enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

GLuint vao;
GLuint vbo;
GLuint mvpMatrixUniform;

// tessellation shader related variables (uniform)
GLuint noOfSegmentsUniform;
GLuint noOfStripsUniform;
GLuint lineColorUniform;

unsigned int uiNoOfSegments; // not opengl related uniform so unsigned int type

mat4 perspectiveProjectionMatrix; // change 1

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iRetval = 0;

	// code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation Of Log File Faild. Exitting"), TEXT("File IO ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created  \n");
	}

	// initialization of WNDCLASS structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// register above class or wndclass 
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,//yache kam aase aahe ki window satat taskbar varti rahil.
		szAppName,
		TEXT("Mandar Maske RTR2021-116 OpenGL Window"),
		WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE, // WS_CLIPCHILDREN : jr mala chlid window aastil tr tyana maja area cover karun deu nakomala overlap karat aastil tr tyana clip kr majya vr kon nako, clip mhanje kapa
		//|WS_CLIPSIBLINGS:SIBLINGS mhaje bhau bhau majya bhav jari mala overlap karat aastil tr tyana clip kr majya vr kon nako
		//|WS_VISIBLE:initially make me visibe even if there WM_PAINT
		(GetSystemMetrics(SM_CXSCREEN) / 2 - 400),
		(GetSystemMetrics(SM_CYSCREEN) / 2 - 300),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	iRetval = initialize();
	
	if(iRetval== -1)
	{
		fprintf(gpFile, "Choose Pixel Format Faild \n");
		uninitialize();
	}
	if (iRetval == -2)
	{
		fprintf(gpFile, "Set Pixel Format Faild \n");
		uninitialize();
	}

	if (iRetval == -3)
	{
		fprintf(gpFile, "Create OpenGL Context Faild \n");
		uninitialize();
	}

	if (iRetval == -4)
	{
		fprintf(gpFile, "Making OpenGl Context As Current Context Faild \n");
		uninitialize();
	}

	if (iRetval == -5)
	{
		fprintf(gpFile, "glew Faild \n");
		uninitialize();
	}

	// show window
	ShowWindow(hwnd, iCmdShow);

	// foregrounding and focusing the window
	SetForegroundWindow(hwnd);//here we use ghwnd and hwnd,but here ghwnd used for global function so we use hwnd
	SetFocus(hwnd);

	// game loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}
	uninitialize();

	return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void resize(int, int);
	

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);
		//break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 27:
			DestroyWindow(hwnd);
			break;

		case VK_UP:
			if (uiNoOfSegments >= 30)
				uiNoOfSegments = 30;
			else
				uiNoOfSegments++;
			break;

		case VK_DOWN:
			if (uiNoOfSegments <= 1)
				uiNoOfSegments = 1;
			else
				uiNoOfSegments--;
			break;

		default:
			break;
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));//resize(width , hight);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//varible declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = FALSE;

	}

}

int initialize(void)//convetion follow karayche mhanun initialize small
{
	// function declarations
	void printGLInfo(void);
	void uninitialize(void);
	void resize(int width, int height);

	// veriable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	// initialization of pixelformatdescriptortable
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	// getdc
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	
	// set the choosen pixel format
	// code
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
		
	}
	// create OpenGl rendering context
	ghrc = wglCreateContext(ghdc);//briging API

	if (ghrc == NULL)
	{
		return(-3);
	}
	// make the rendering context to current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
		
	}

	// stepA 3 : initializaton of glew
	// glew initializaton
	if (glewInit() != GLEW_OK)
	{
		return(-5);
	}

	// print OpenGLInfo
	printGLInfo();

	// StepC 1 : writing shader code
	// vertex shader 
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec2 a_position;"  \
		"void main(void)"  \
		"{" \
			"gl_Position = vec4(a_position, 0.0, 1.0);" \
		"}";

	// change - instead of vec4 - vec2 is written, "uniform mat4 u_mvpMatrix;" \ hi line kadhali
	// a poaaition consist x, y so it covered vec2 of vec4 and z, w are 0.0 and 1.0

	// StepC 2 : creating shader object
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);  
		
		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
				
				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// tessellation control shader
	const GLchar* tesellationControlShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"layout(vertices=4)out;" \
		"uniform int u_noOfSegments;" \
		"uniform int u_noOfStrips;" \
		"void main(void)" \
		"{" \
		"gl_out[gl_InvocationID].gl_Position=gl_in[gl_InvocationID].gl_Position;" \
		"gl_TessLevelOuter[0]=float(u_noOfStrips);" \
		"gl_TessLevelOuter[1]=float(u_noOfSegments);" \
		"}";

	// laout() shader macros, layout(vertices = 4) ha ek data type zhala ani pudhe 'out' he asach pahije swtacha name kai tar nahi chalat
	// "gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" \ -  struct array
	// "gl_TessLevelOuter[0] = float(u_noOfStrips);" \ typecasting as constructor or macro
	//"gl_TessLevelOuter[1] = float(u_noOfSegments);" \ typecasting as constructor or macro

	// StepC 2 : creating shader object
	GLuint tessellationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(tessellationControlShaderObject, 1, (const GLchar**)&tesellationControlShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(tessellationControlShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(tessellationControlShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(tessellationControlShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(tessellationControlShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Tessellation Control Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// tessellation control shader
	const GLchar* tesellationEvaluationShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"layout(isolines)in;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
		"vec3 p0 = gl_in[0].gl_Position.xyz;" \
		"vec3 p1 = gl_in[1].gl_Position.xyz;" \
		"vec3 p2 = gl_in[2].gl_Position.xyz;" \
		"vec3 p3 = gl_in[3].gl_Position.xyz;" \
		"float u = gl_TessCoord.x;" \
		"vec3 p= (float)p0*(1-u)*(1-u)*(1-u)+p1*3*u*(1-u)*(1-u)+p2*3*u*u*(1-u)+p3*u*u*u;" \
		"gl_Position = u_mvpMatrix * vec4(p, 1.0);" \
		"}";

	/*
	"#version 460 core" \
		"\n" \
		"layout(isoliles) in;" \ // laout() shader macros, layout(isoliles) ha ek data type zhala ani pudhe 'in' he asach pahije swtacha name kai tar nahi chalat
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
			"vec3 p0 = gl_in[0].gl_Position.xyz;" \ // swizzling - 4 madhle 3 ghetle 
			"vec3 p1 = gl_in[1].gl_Position.xyz;" \ // swizzling - 4 madhle 3 ghetle 
			"vec3 p2 = gl_in[2].gl_Position.xyz;" \ // swizzling - 4 madhle 3 ghetle 
			"vec3 p3 = gl_in[3].gl_Position.xyz;" \ // swizzling - 4 madhle 3 ghetle 
			"float u = gl_TessCoord.x;" \
			"vec3 p = p0 * (1 - u) * (1 - u) * (1 - u) + p1 * 3 * u * (1 - u) * (1 - u) + p2 * 3 * u * u * (1 - u) + p3 * u * u * u;" \
			"gl_Position = u_mvpMatrix * vec4(p, 1.0);" \ // w = 1.0, p ne 3 cover kele
		"}";
	*/

	// StepC 2 : creating shader object
	GLuint tessellationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(tessellationEvaluationShaderObject, 1, (const GLchar**)&tesellationEvaluationShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(tessellationEvaluationShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(tessellationEvaluationShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(tessellationEvaluationShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(tessellationEvaluationShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Tessellation Evaluation Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"uniform vec4 u_lineColor;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"FragColor = vec4(u_lineColor);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
	
	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, tessellationControlShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, tessellationEvaluationShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, fragmentShaderObject);
	
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position"); // Be Aware 

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize();

			}
		}
	}

	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
	noOfSegmentsUniform = glGetUniformLocation(shaderProgramObject, "u_noOfSegments");
	noOfStripsUniform = glGetUniformLocation(shaderProgramObject, "u_noOfStrips");
	lineColorUniform = glGetUniformLocation(shaderProgramObject, "u_lineColor");

	// vao & vbo related code
	//declarations of vertex data arrays
	const GLfloat vertices[] =
	{
		-1.0f, -1.0f,
		-0.5f, 1.0f,
		0.5f, -1.0f,
		1.0f, 1.0f
	};

	glGenVertexArrays(1, &vao); //vao:- vertex array object
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	glBindVertexArray(0);

	// here start OpenGL code (depth & clear color code)
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	uiNoOfSegments = 1;

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

//  stepB 
void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	// StepB 1 : printing OpenGL vendor,renderer,version,GLSL version 
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	// StepB 2 : printing no. of supproted extension & names of extension
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}


void resize(int width, int height)
{
	// code
	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject);

	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glUniform1i(noOfSegmentsUniform, uiNoOfSegments);
	TCHAR str[255];
	wsprintf(str, TEXT("Mandar Maske RTR2021-116 OpenGL Window || No. Of Segments = %d"), uiNoOfSegments);
	SetWindowText(ghwnd, str);
	glUniform1i(noOfStripsUniform, 1);

	if (uiNoOfSegments == 1)
		glUniform4fv(lineColorUniform, 1, vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	else if (uiNoOfSegments >= 1 && uiNoOfSegments <= 29)
		glUniform4fv(lineColorUniform, 1, vmath::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	else if (uiNoOfSegments == 30)
		glUniform4fv(lineColorUniform, 1, vmath::vec4(0.0f, 1.0f, 0.0f, 1.0f));

	glBindVertexArray(vao);

	glPatchParameteri(GL_PATCH_VERTICES, 4);

	glDrawArrays(GL_PATCHES, 0, 4);

	glBindVertexArray(0);

	// stepE 3 : unuse the shader program object
	
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	// code

}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gbFullScreen==TRUE)
	{
		ToggleFullScreen();
	}

	// deletion/ uninitialization of vbo
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	// deletion/ uninitialization of vao
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	// shader uninitialization
	if (shaderProgramObject)
	{
		// stepF 0
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed");
		fclose(gpFile);
		gpFile = NULL;
	}
}

