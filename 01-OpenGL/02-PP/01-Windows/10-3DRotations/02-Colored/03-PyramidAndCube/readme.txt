// copy above color2D rotation code and replace tringle->pyramid and square->cube

// globally add 1 veriable
GLuint vbo_cube_color;

// in initialize() // file madle copy past(color position)

// after glVertexAttrib3f() and above glBindBuffer()
// cube color
	glGenBuffers(1, &vbo_cube_color);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_color); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColor), cubeColor, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr


//in display()
// add line
mat4 rotationMatrix_x = mat4::identity();
	mat4 rotationMatrix_y = mat4::identity();
	mat4 rotationMatrix_z = mat4::identity();
	rotationMatrix = mat4::identity();
scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);

modelViewMatrix = translationMatrix * scaleMatrix * rotationMatrix ;
rotationMatrix = rotationMatrix_x * rotationMatrix_y * rotationMatrix_x;


in pyramid 
// after glBindVertexArray() and above glBindVertexArray(0);
// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glDrawArrays(GL_TRIANGLES, 0, 12);

in cube
// after glBindVertexArray() and above glBindVertexArray(0);
// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //PP made glQuad nahi other option ne draw karayche
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);


in uninitialize()

if (vbo_cube_color)
	{
		glDeleteBuffers(1, &vbo_cube_color);
		vbo_cube_color = 0;
	}



















