// standard headers
#include<stdio.h> // for standard io
#include<stdlib.h> // for exit()
#include "Sphere.h"
#include<memory.h> // for memset()

// X11 headers
#include<X11/Xlib.h> // Similar like windows.h	
#include<X11/Xutil.h> // XVisualInfo
#include<X11/XKBlib.h> // for Keyboard

// OpenGL header files
#include<GL/glew.h>
#include<GL/gl.h> // for OpenGL functionality
#include<GL/glx.h> // fro briging API
#include"vmath.h"
using namespace vmath;

// OpenGL library
#pragma comment(lib,"OpenGL32.lib")//programatically sangtoy majilib file 
//ekadun ghe mi cmd la denar nahi
#pragma comment(lib,"GLU32.lib")

// Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable
Display *display = NULL;
XVisualInfo *visualInfo = NULL; // 10 member che struct aahe aat made visual struct aahe jyache swatat 8 member aahet
Colormap colorMap;
Window window;
Bool gbFullScreen = False;
Bool bActiveWindow = False;
FILE* gpFile = NULL;

// OpenGL realeated veriable
/*typedef GLXContext (*glXCreateContexAttribsARBProc)(Display* ,GLXFBConfig,GLXContext,Bool,const int *);
glXCreateContexAttribsARBProc glXCreateContexAttribsARB = NULL;*/
GLXFBConfig glxFbConfig;

typedef GLXContext (*glXCreateContextAttribsARPProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARPProc glXCreateContextAttribsARB=NULL;

GLXContext glxContext;

// programable pipeline related global veriable
GLuint shaderProgramObject_Sphere;
GLuint shaderProgramObject_PF;

enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};


GLuint vao_sphere;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_elements;
GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix; // chnage 1

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices, gNumElements;

// Uniform
GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint laUniform; // light ambient
GLuint ldUniform; // light diffuse
GLuint lsUniform; // light specular
GLuint lightPositionUniform;

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShininessUniform;

GLuint lightingEnabledUniform;

GLfloat materialAmbient[4];
GLfloat materialDiffuse[4];
GLfloat materialSpecular[4];
GLfloat materialShininess;

Bool gbLight = False;

GLfloat gbLightAngleZero = 0.0f;

GLfloat gbLightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat gbLightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat gbLightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat gbLightPositionX[] = { 0.0f, 0.0f, 0.0f, 1.0f }; // Positional Light because of 1.0f

int counter = 1;
int dcounter = 0;

GLfloat FULL_WIN_WIDTH = 0;
GLfloat FULL_WIN_HEIGHT = 0;

// entry point()
int main(void)
{
	// function declarations
	int initialize(void);
	void resize(int,int);
	void draw(void);
	void toggleFullscreen(void);
	void update(void);
	void uninitialize(void);
	
	// local veriables
	int defaultScreen;
	int defaultDepth;
	GLXFBConfig * glxFbConfigs = NULL;
	GLXFBConfig bestGlxFbConfigs;
	XVisualInfo *tempXVisualinfo = NULL;
	int numFBConfig;
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom wm_delete_window_atom;
	XEvent event;
	KeySym keysym;
	int screenWidth;
	int screenHeight;
	char keys[26];

	static int frameBufferAttributes[]= {   // static nahi kela tari chalto pn saglikade he static aahe
											GLX_X_RENDERABLE, True,
											GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
											GLX_RENDER_TYPE, GLX_RGBA_BIT,
											GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
											GLX_RED_SIZE, 8,
											GLX_GREEN_SIZE, 8,
											GLX_BLUE_SIZE, 8,
											GLX_ALPHA_SIZE, 8,
											GLX_STENCIL_SIZE,8,
											GLX_DEPTH_SIZE, 24,
											None // ete 0 dila tari cahlto pn xlib style sathi None
										}; // it is same like pixel format attribute
	Bool bDone = False;
	static int winWidth;
	static int winHeight;


	// code

	gpFile = fopen("Log.txt", "w"); 
	
	if (gpFile == 0)
	{
		printf("fopen() Failed");
		exit(0);
	}
	else
	{
		printf("Log File Successfully Created.\n\n");
	}

	// step 1 : Open the Display()

	display=XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		printf("ERROR:XOpenDisplay() Failed \n");
		uninitialize();
		exit(1);
	}
	
	// step 2 : Get default screen from display
	defaultScreen=XDefaultScreen(display);

	Screen* screen = DefaultScreenOfDisplay(display);
	
	FULL_WIN_WIDTH = screen -> width;
	FULL_WIN_HEIGHT = screen -> height;
	
	// step 3 : Get default screen from display & default screen
	defaultDepth=XDefaultDepth(display,defaultScreen);

	glxFbConfigs = glXChooseFBConfig(display , defaultScreen , frameBufferAttributes , &numFBConfig );
	
	if(glxFbConfigs == NULL)
	{
		fprintf(gpFile,"Error : glxFbConfigs() Faild \n");
		uninitialize();
		exit(1);
	}
	fprintf(gpFile,"Found no of frame buffer configs = %d \n",numFBConfig);

	// find best frame buffer config from array
	int bestFrameBufferConfig = -1;
	int wrostFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int wrostNumberOfSamples = 999;	

	for(int i = 0; i < numFBConfig ; i++)
	{
		tempXVisualinfo = glXGetVisualFromFBConfig(display , glxFbConfigs[i]);
		if(tempXVisualinfo != NULL)
		{
			int samples , sampleBuffers;
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLES, &samples);
			fprintf(gpFile, "Visual info = 0x%lu Found sampleBuffers = %d, smaples = %d \n", tempXVisualinfo->visualid, sampleBuffers, samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}

			if(wrostFrameBufferConfig < 0 || !sampleBuffers || samples < wrostNumberOfSamples)
			{
				wrostFrameBufferConfig = i ;
				wrostNumberOfSamples = samples; 
			}
		}
		XFree(tempXVisualinfo);
		tempXVisualinfo = NULL;
	}

	bestGlxFbConfigs = glxFbConfigs[bestFrameBufferConfig];

	glxFbConfig = bestGlxFbConfigs;

	XFree(glxFbConfigs);
	glxFbConfigs = NULL;

	visualInfo = glXGetVisualFromFBConfig(display, bestGlxFbConfigs);
	fprintf(gpFile," Visual id of best visual info is 0X%lu \n",visualInfo -> visualid);


	// step 4 : Get XVisualInfo by using glXChooseVisual & do error checking
	//visualInfo=glXChooseVisual(display,defaultScreen,frameBufferAttributes);
	
	/*if (visualInfo==NULL)
	{
		printf("ERROR:glXChooseVisual() Failed \n");
		uninitialize();
		exit(1);
	}*/
	
	// step 5 : Fill/initialize structure XSetWindowAttributes & along with that also state Colormap & EventMask
	memset(&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel=0;
	windowAttributes.background_pixel=XBlackPixel(display,defaultScreen);
	windowAttributes.background_pixmap=0;
	windowAttributes.colormap=XCreateColormap(display,
						   RootWindow(display,visualInfo->screen),
						   visualInfo->visual,
						   AllocNone);
	windowAttributes.event_mask=ExposureMask|KeyPressMask|StructureNotifyMask|FocusChangeMask;
	
	// step 6 : initialize grobal color map by using color map from window attributes
	colorMap=windowAttributes.colormap;
	
	// step 7 : Initialize window style called as styleMask (Style cha mukhvata lavne)
	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;
	
	// step 8 : Create window using XCreateWindow using XlibAPI & do error checking
	window=XCreateWindow(display,
			     RootWindow(display,visualInfo->screen),
			     100,
			     100,
			     WIN_WIDTH,
			     WIN_HEIGHT,
			     0,
			     visualInfo->depth,
			     InputOutput,
			     visualInfo->visual,
			     styleMask,
			     &windowAttributes);
	if(!window)
	{
		printf("ERROR:XCreateWindow() Failed \n");
		uninitialize();
		exit(1);
	}
			     
	// step 9 : Give name to your window in it's title / caption bar
	XStoreName(display,window,"OpenGL Window - Mandar Maske RTR2021-116");
	
	// step 10 : Prepare our window to respond to :
	// a : Closing window by clicking  by close button
	// b : Closing window by close option  in system menu by creating & setting WindowManager protocol atom  

	wm_delete_window_atom=XInternAtom(display,"WM_DELETE_WINDOW",True);   // here WM:WindowManager
	
	XSetWMProtocols(display,window,&wm_delete_window_atom,1);
	
	// step 11 : Show the window by XMapWindow()
	XMapWindow(display,window);

	// Centering of window
	screenWidth=XWidthOfScreen(XScreenOfDisplay(display,defaultScreen));
	screenHeight=XHeightOfScreen(XScreenOfDisplay(display,defaultScreen));
	XMoveWindow(display,window,screenWidth/2-WIN_WIDTH/2,screenHeight/2-WIN_HEIGHT/2);
	
	initialize();

	// step 12 : Create a message loop by :
	// a : Getting next event XNextEvent()
	// b : Handling  keypress by Escape key
	// c : By handling message 33
	// message loop
	while (bDone==False)
	{
		while(XPending(display)) // Xpendind mhanje windows madle peek msg
		{
			// a : Getting next event XNextEvent()
			XNextEvent(display,&event);
		
			switch(event.type)
			{
				case MapNotify: // it is similar like WM_CREATE
					break;

				case FocusIn: // set focus
					bActiveWindow = True;
					break;

				case FocusOut: // kill focus
					bActiveWindow = False;
					break;
				
				case KeyPress:
					keysym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);
				
					switch(keysym)
					{
						// b : Handling  keypress by Escape key
						case XK_Escape:
							bDone = True;
							break;
					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					
					switch(keys[0])
					{
						case 'F':
						case 'f':
							if(gbFullScreen==False)
							{
								toggleFullscreen();
								gbFullScreen=True;
							}
							else
							{
								toggleFullscreen();
								gbFullScreen=False;
							}
						break;

						case 'L':
						case 'l':
							if (gbLight == False)
							{
								gbLight = True;
								counter = 1;
							}
							else
							{
								gbLight = False;
							}
							break;

						case 'X':
						case 'x':
							dcounter = 1;
						break;

						case 'Y':
						case 'y':
							dcounter = 2;
						break;

						case 'Z':
						case 'z':
							dcounter = 3;
						break;

						default:
							dcounter = 0;
						break;
					}
					break;

				case ConfigureNotify: // it is similar like WM_Size:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
						
				// c : By handling message 33
				case 33: // all close function working here // it is analogus to wm_delete_window_atom
					bDone = True;
					break;	
			}				
		}
		if (bActiveWindow == True)
		{
			update();

			draw();
		}
	}
			
	// step 13 : After closing message loop call uninitialize() & return()	     
	uninitialize();
	
	return(0);
}

void toggleFullscreen(void)
{
	// local veriables
	Atom wm_current_state_atom;
	Atom wm_fullscreen_state_atom;
	XEvent event;

	// code
	wm_current_state_atom=XInternAtom(display,"_NET_WM_STATE",False);
	wm_fullscreen_state_atom=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);

	memset(&event,0,sizeof(XEvent));
	event.type=ClientMessage;
	event.xclient.window=window;
	event.xclient.message_type=wm_current_state_atom;
	event.xclient.format=32;
	event.xclient.data.l[0]=gbFullScreen?0:1;
	event.xclient.data.l[1]=wm_fullscreen_state_atom;

	XSendEvent(display,
			   RootWindow(display,visualInfo->screen),
			   False,
			   SubstructureNotifyMask,
			   &event);	//similar like sendmessage & Postmessage 
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	void uninitialize(void);
	void printGLInfo(void);
	
	//code
	//glxContext = glXCreateContext(display,visualInfo,NULL,True);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARPProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL)
	{
		fprintf(gpFile, "glXCreateContextAttribesARP Failed\n\n");
		uninitialize();
		exit(1);
	}

	GLint contextAttributes[]=
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,6,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	//glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	if(!glxContext)
	{
		GLint contextAttributes[]=
		{
		GLX_CONTEXT_MAJOR_VERSION_ARB,1,
		GLX_CONTEXT_MINOR_VERSION_ARB,0,
		None
		};
		glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
		fprintf(gpFile, "Can not Support 4.6 hence falling back to default version\n\n");
	}
	else
	{
		fprintf(gpFile, "Found the support to 4.6 version \n\n");
	}

	//Cheking S/w or H/w 
	if(!glXIsDirect(display,glxContext))
	{
		fprintf(gpFile, "Direct(h/w) Rendering is not Supported \n\n");
	}
	else
	{
		fprintf(gpFile, "Direct(h/w) Rendering is Supported \n\n");
	}

	// make current context
	glXMakeCurrent(display, window, glxContext);
	//Glew Initialization
	if (glewInit() != GLEW_OK)
	{
		return -5;
	}

	// print OpenGLInfo
	printGLInfo();

	// StepC 1 : writing shading code
	// vertex shader for per fragment
	const GLchar* vertexShaderSourceCode_PF =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec4 u_lightPosition;" \
		"uniform int u_lightingEnabled;" \
		"out vec3 transformedNormals;" \
		"out vec3 lightDirection;" \
		"out vec3 viewerVector;" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
		"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
		"transformedNormals = normalMatrix * a_normal;" \
		"viewerVector = -eyeCoordinates.xyz;" \
		"lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject_PF = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject_PF, 1, (const GLchar**)&vertexShaderSourceCode_PF, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject_PF); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject_PF, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_PF, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex PerFragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// fragment shader for per vertex
	const GLchar* fragmentShaderSourceCode_PF =
		"#version 460 core" \
		"\n" \
		"in vec3 transformedNormals;" \
		"in vec3 lightDirection;" \
		"in vec3 viewerVector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShininess;" \
		"uniform int u_lightingEnabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_light;" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec3 ambient;" \
		"vec3 diffuse;" \
		"vec3 reflectionVector;" \
		"vec3 specular;" \
		"vec3 normalized_transformed_normal;" \
		"vec3 normalized_lightDirection;" \
		"vec3 normalized_viewerVector;" \
		"normalized_transformed_normal = normalize(transformedNormals); " \
		"normalized_viewerVector = normalize(viewerVector);" \
		"normalized_lightDirection = normalize(lightDirection);" \
		"ambient = u_la * u_ka;" \
		"diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformed_normal), 0.0);" \
		"reflectionVector = reflect(-normalized_lightDirection, normalized_transformed_normal);" \
		"specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" \
		"phong_ads_light = phong_ads_light + ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_light,1.0);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject_PF = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject_PF, 1, (const GLchar**)&fragmentShaderSourceCode_PF, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject_PF); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject_PF, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_PF, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// Shader program object perfragment
	// stepD 1 : create shader program object
	shaderProgramObject_PF = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject_PF, vertexShaderObject_PF); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject_PF, fragmentShaderObject_PF);

	glBindAttribLocation(shaderProgramObject_PF, AMC_ATTRIBUTE_POSITION, "a_position");
	glBindAttribLocation(shaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "a_normal");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject_PF);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject_PF, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject_PF, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program PerFragment link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize();

			}
		}
	}

	// perfragment
	modelMatrixUniform = glGetUniformLocation(shaderProgramObject_PF, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
	viewMatrixUniform = glGetUniformLocation(shaderProgramObject_PF, "u_viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(shaderProgramObject_PF, "u_projectionMatrix");

	// light uniform
	laUniform = glGetUniformLocation(shaderProgramObject_PF, "u_la");
	ldUniform = glGetUniformLocation(shaderProgramObject_PF, "u_ld");
	lsUniform = glGetUniformLocation(shaderProgramObject_PF, "u_ls");
	lightPositionUniform = glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition");

	// material uniform
	kaUniform = glGetUniformLocation(shaderProgramObject_PF, "u_ka");
	kdUniform = glGetUniformLocation(shaderProgramObject_PF, "u_kd");
	ksUniform = glGetUniformLocation(shaderProgramObject_PF, "u_ks");
	materialShininessUniform = glGetUniformLocation(shaderProgramObject_PF, "u_materialShininess");

	// light enable uniform
	lightingEnabledUniform = glGetUniformLocation(shaderProgramObject_PF, "u_lightingEnabled");

	// StepC 1 : writing shading code
	// vertex shader 
	const GLchar* vertexShaderSourceCode_Sphere =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvpMatrix * a_position;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject_Sphere = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject_Sphere, 1, (const GLchar**)&vertexShaderSourceCode_Sphere, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject_Sphere); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject_Sphere, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_Sphere, GL_INFO_LOG_LENGTH, &infoLogLength);  
		
		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_Sphere, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
				
				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode_Sphere =
		"#version 460 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject_Sphere = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject_Sphere, 1, (const GLchar**)&fragmentShaderSourceCode_Sphere, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
	
	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject_Sphere); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject_Sphere, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_Sphere, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_Sphere, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject_Sphere = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject_Sphere, vertexShaderObject_Sphere); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject_Sphere, fragmentShaderObject_Sphere);
	
	glBindAttribLocation(shaderProgramObject_Sphere, AMC_ATTRIBUTE_POSITION, "a_position");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject_Sphere);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject_Sphere, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_Sphere, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject_Sphere, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize();

			}
		}
	}

	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject_Sphere, "u_mvpMatrix");

	// vao & vbo related code
	//declarations of vertex data arrays
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	// position vbo
	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &vbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	
	// here start OpenGL code 
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}

void resize(int width,int height)
{
	// code
	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void draw(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject_Sphere);

	glUseProgram(shaderProgramObject_PF);

	if (gbLight == True)
	{
		glUniform1i(lightingEnabledUniform, 1);

		glUniform3fv(laUniform, 1, gbLightAmbient);
		glUniform3fv(ldUniform, 1, gbLightDiffuse);
		glUniform3fv(lsUniform, 1, gbLightSpecular);

		if (dcounter == 1)
			glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleZero) * 50, cos(gbLightAngleZero) * 50, 1.0f));
		else if (dcounter == 2)
			glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZero) * 50, 0.0f, cos(gbLightAngleZero) * 50, 1.0f));
		else if (dcounter == 3)
			glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZero) * 50, cos(gbLightAngleZero) * 50, 0.0f, 1.0f));
		else
			glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	}
	else
	{
		glUniform1i(lightingEnabledUniform, 0);
	}

	if (gbFullScreen == False)
	{
		glViewport(0, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// From Left
	// 1 Sphere 1st Column 
	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(-0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ***** 1st sphere on 1st column, emerald *****
	// ambient material
	materialAmbient[0] = 0.0215f; // r
	materialAmbient[1] = 0.1745f; // g
	materialAmbient[2] = 0.0215f; // b
	materialAmbient[3] = 1.0f;   // a

	// diffuse material
	materialDiffuse[0] = 0.07568f; // r
	materialDiffuse[1] = 0.61424f; // g
	materialDiffuse[2] = 0.07568f; // b
	materialDiffuse[3] = 1.0f;    // a

	// specular material
	materialSpecular[0] = 0.633f;    // r
	materialSpecular[1] = 0.727811f; // g
	materialSpecular[2] = 0.633f;    // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.6 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(0, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 2nd Sphere 1st Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.135f;  // r
	materialAmbient[1] = 0.2225f; // g
	materialAmbient[2] = 0.1575f; // b
	materialAmbient[3] = 1.0f;   // a

	// diffuse material
	materialDiffuse[0] = 0.54f; // r
	materialDiffuse[1] = 0.89f; // g
	materialDiffuse[2] = 0.63f; // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.316228f; // r
	materialSpecular[1] = 0.316228f; // g
	materialSpecular[2] = 0.316228f; // b
	materialSpecular[3] = 1.0f;      // a

	// shininess
	materialShininess = float(0.1 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(0, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 3rd Sphere 1st Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	materialAmbient[0] = 0.05375f; // r
	materialAmbient[1] = 0.05f;    // g
	materialAmbient[2] = 0.06625f; // b
	materialAmbient[3] = 1.0f;     // a

	// diffuse material
	materialDiffuse[0] = 0.18275f; // r
	materialDiffuse[1] = 0.17f;    // g
	materialDiffuse[2] = 0.22525f; // b
	materialDiffuse[3] = 1.0f;     // a

	// specular material
	materialSpecular[0] = 0.332741f; // r
	materialSpecular[1] = 0.328634f; // g
	materialSpecular[2] = 0.346435f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.3 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(0, WIN_HEIGHT / 3.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 3.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 4th Sphere 1st Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	materialAmbient[0] = 0.25f;    // r
	materialAmbient[1] = 0.20725f; // g
	materialAmbient[2] = 0.20725f; // b
	materialAmbient[3] = 1.0f;    // a

	// diffuse material
	materialDiffuse[0] = 1.0f;   // r
	materialDiffuse[1] = 0.829f; // g
	materialDiffuse[2] = 0.829f; // b
	materialDiffuse[3] = 1.0f;  // a

	// specular material
	materialSpecular[0] = 0.296648f; // r
	materialSpecular[1] = 0.296648f; // g
	materialSpecular[2] = 0.296648f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.088 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(0, WIN_HEIGHT / 6.5, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 6.5, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 5th Sphere 1st Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.1745f;  // r
	materialAmbient[1] = 0.01175f; // g
	materialAmbient[2] = 0.01175f; // b
	materialAmbient[3] = 1.0f;    // a

	// diffuse material
	materialDiffuse[0] = 0.61424f; // r
	materialDiffuse[1] = 0.04136f; // g
	materialDiffuse[2] = 0.04136f; // b
	materialDiffuse[3] = 1.0f;    // a

	// specular material
	materialSpecular[0] = 0.727811f; // r
	materialSpecular[1] = 0.626959f; // g
	materialSpecular[2] = 0.626959f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.6 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(0, WIN_HEIGHT / 220.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(0, FULL_WIN_HEIGHT / 220.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 6th Sphere 1st Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	materialAmbient[0] = 0.1f;     // r
	materialAmbient[1] = 0.18725f; // g
	materialAmbient[2] = 0.1745f;  // b
	materialAmbient[3] = 1.0f;    // a

	// diffuse material
	materialDiffuse[0] = 0.396f;   // r
	materialDiffuse[1] = 0.74151f; // g
	materialDiffuse[2] = 0.69102f; // b
	materialDiffuse[3] = 1.0f;    // a

	// specular material
	materialSpecular[0] = 0.297254f; // r
	materialSpecular[1] = 0.30829f;  // g
	materialSpecular[2] = 0.306678f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.1 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	// 1 Sphere 2nd Column 
	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}
	// transformations
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.329412f; // r
	materialAmbient[1] = 0.223529f; // g
	materialAmbient[2] = 0.027451f; // b
	materialAmbient[3] = 1.0f;     // a

	// diffuse material
	materialDiffuse[0] = 0.780392f; // r
	materialDiffuse[1] = 0.568627f; // g
	materialDiffuse[2] = 0.113725f; // b
	materialDiffuse[3] = 1.0f;     // a

	// specular material
	materialSpecular[0] = 0.992157f; // r
	materialSpecular[1] = 0.941176f; // g
	materialSpecular[2] = 0.807843f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.21794872 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 2nd Sphere 2nd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.2125f; // r
	materialAmbient[1] = 0.1275f; // g
	materialAmbient[2] = 0.054f;  // b
	materialAmbient[3] = 1.0f;   // a

	// diffuse material
	materialDiffuse[0] = 0.714f;   // r
	materialDiffuse[1] = 0.4284f;  // g
	materialDiffuse[2] = 0.18144f; // b
	materialDiffuse[3] = 1.0f;    // a

	// specular material
	materialSpecular[0] = 0.393548f; // r
	materialSpecular[1] = 0.271906f; // g
	materialSpecular[2] = 0.166721f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.2 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 3rd Sphere 2nd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.25f; // r
	materialAmbient[1] = 0.25f; // g
	materialAmbient[2] = 0.25f; // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.4f;  // r
	materialDiffuse[1] = 0.4f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.774597f; // r
	materialSpecular[1] = 0.774597f; // g
	materialSpecular[2] = 0.774597f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.6 * 128);
	
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 3.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 3.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 4th Sphere 2nd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.19125f; // r
	materialAmbient[1] = 0.0735f;  // g
	materialAmbient[2] = 0.0225f;  // b
	materialAmbient[3] = 1.0f;    // a

	// diffuse material
	materialDiffuse[0] = 0.7038f;  // r
	materialDiffuse[1] = 0.27048f; // g
	materialDiffuse[2] = 0.0828f;  // b
	materialDiffuse[3] = 1.0f;    // a

	// specular material
	materialSpecular[0] = 0.256777f; // r
	materialSpecular[1] = 0.137622f; // g
	materialSpecular[2] = 0.086014f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.1 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 6.5f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 6.5f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}


	// 5th Sphere 2nd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.24725f; // r
	materialAmbient[1] = 0.1995f;  // g
	materialAmbient[2] = 0.0745f;  // b
	materialAmbient[3] = 1.0f;    // a

	// diffuse material
	materialDiffuse[0] = 0.75164f; // r
	materialDiffuse[1] = 0.60648f; // g
	materialDiffuse[2] = 0.22648f; // b
	materialDiffuse[3] = 1.0f;    // a

	// specular material
	materialSpecular[0] = 0.628281f; // r;
	materialSpecular[1] = 0.555802f; // g
	materialSpecular[2] = 0.366065f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.4 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 220, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 2.3f, FULL_WIN_HEIGHT / 220, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 6th Sphere 2nd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.19225f; // r
	materialAmbient[1] = 0.19225f; // g
	materialAmbient[2] = 0.19225f; // b
	materialAmbient[3] = 1.0f;    // a

	// diffuse material
	materialDiffuse[0] = 0.50754f; // r
	materialDiffuse[1] = 0.50754f; // g
	materialDiffuse[2] = 0.50754f; // b
	materialDiffuse[3] = 1.0f;    // a

	// specular material
	materialSpecular[0] = 0.508273f; // r
	materialSpecular[1] = 0.508273f; // g
	materialSpecular[2] = 0.508273f; // b
	materialSpecular[3] = 1.0f;     // a

	// shininess
	materialShininess = float(0.4 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 1.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 1.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 1 Sphere 3rd Column 
	// transformations
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.01f; // r
	materialDiffuse[1] = 0.01f; // g
	materialDiffuse[2] = 0.01f; // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.50f; // r
	materialSpecular[1] = 0.50f; // g
	materialSpecular[2] = 0.50f; // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.25 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 1.1, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 2nd Sphere 3rd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.1f;  // g
	materialAmbient[2] = 0.06f; // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.0f;        // r
	materialDiffuse[1] = 0.50980392f; // g
	materialDiffuse[2] = 0.50980392f; // b
	materialDiffuse[3] = 1.0f;       // a

	// specular material
	materialSpecular[0] = 0.50196078f; // r
	materialSpecular[1] = 0.50196078f; // g
	materialSpecular[2] = 0.50196078f; // b
	materialSpecular[3] = 1.0f;       // a

	// shininess
	materialShininess = float(0.25 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 3rd Sphere 3rd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.1f;  // r
	materialDiffuse[1] = 0.35f; // g
	materialDiffuse[2] = 0.1f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.45f; // r
	materialSpecular[1] = 0.55f; // g
	materialSpecular[2] = 0.45f; // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.25 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 3.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 3.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 4th Sphere 3rd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.0f;  // g
	materialDiffuse[2] = 0.0f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.6f;  // g
	materialSpecular[2] = 0.6f;  // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.25 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 6.5f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 6.5f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 5th Sphere 3rd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.55f; // r
	materialDiffuse[1] = 0.55f; // g
	materialDiffuse[2] = 0.55f; // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.70f; // r
	materialSpecular[1] = 0.70f; // g
	materialSpecular[2] = 0.70f; // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.25 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 220, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 1.1f, FULL_WIN_HEIGHT / 220, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}

	// 6th Sphere 3rd Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.0f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.60f; // r
	materialSpecular[1] = 0.60f; // g
	materialSpecular[2] = 0.50f; // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.25 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 1.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 1.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}
	// 1st Sphere 4th Column 
	// transformations
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.02f; // r
	materialAmbient[1] = 0.02f; // g
	materialAmbient[2] = 0.02f; // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.01f; // r
	materialDiffuse[1] = 0.01f; // g
	materialDiffuse[2] = 0.01f; // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.4f;  // r
	materialSpecular[1] = 0.4f;  // g
	materialSpecular[2] = 0.4f;  // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.078125 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 1.65f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 1.65f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}
	// 2nd Sphere 4th Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.05f; // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.4f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.5f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.04f; // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.7f;  // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.078125 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 2.2f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 2.2f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}
	// 3rd Sphere 4th Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.4f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.04f; // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.04f; // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.078125 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 3.3f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 3.3f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}
	// 4th Sphere 4th Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.05f; // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.4f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.04f; // g
	materialSpecular[2] = 0.04f; // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.078125 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 6.5f, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 6.5f, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}
	// 5th Sphere 4th Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.05f; // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.05f; // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.5f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.7f;  // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.078125 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	if (gbFullScreen == False)
	{
		glViewport(WIN_WIDTH / 1.3f, WIN_HEIGHT / 220, WIN_WIDTH / 4, WIN_HEIGHT / 4);
	}
	else
	{
		glViewport(WIN_WIDTH / 0.75f, FULL_WIN_HEIGHT / 220, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
	}
	// 6th Sphere 4th Column
	translationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelMatrix = translationMatrix; // order is imp

	// ambient material
	materialAmbient[0] = 0.05f; // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.04f; // b
	materialSpecular[3] = 1.0f; // a

	// shininess
	materialShininess = float(0.078125 * 128);

	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == True)
	{
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);

		glUniform1f(materialShininessUniform, materialShininess);
	}

	glBindVertexArray(vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	// stepE 3 : unuse the shader program object
	
	glUseProgram(0);
	glUseProgram(0);

	
	glXSwapBuffers(display,window);
}

void update(void)
{
	// code
	gbLightAngleZero = gbLightAngleZero + 0.05f;

	if (gbLightAngleZero >= 360.0f)
		gbLightAngleZero = gbLightAngleZero - 360.0f;
}

void uninitialize(void)
{
	// function declarations
	void toggleFullScreen(void);
		
	// code
	GLXContext currentContext;
	currentContext=glXGetCurrentContext();

	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}

	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}

	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}

	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	// shader uninitialization
	if (shaderProgramObject_Sphere)
	{
		// stepF 0
		glUseProgram(shaderProgramObject_Sphere);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject_Sphere, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject_Sphere, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject_Sphere, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject_Sphere);
		shaderProgramObject_Sphere = 0;
	}

	if (shaderProgramObject_PF)
	{
		// stepF 0
		glUseProgram(shaderProgramObject_PF);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject_PF, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject_PF, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject_PF, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject_PF);
		shaderProgramObject_PF = 0;
	}

	if(currentContext && currentContext == glxContext)
	{
		glXMakeCurrent(display,0,0);
	}
	
	if(glxContext)
	{
		glXDestroyContext(display,glxContext);
		glxContext = NULL;
	}

	if(visualInfo)
	{
		free(visualInfo);
		visualInfo = NULL;
	}

	if (gbFullScreen)
	{
		toggleFullscreen();
		gbFullScreen = False;
	}

	if(window)
	{
		XDestroyWindow(display,window);
	}
	
	if(colorMap)
	{
		XFreeColormap(display,colorMap);
	}
	
	//thos we uninitialize display & colormap we don't uninitialize visualinfo because we cant create visual info we just matched/obtained visual info
	
	if(display)
	{
		XCloseDisplay(display);
		display=NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); 
		gpFile = NULL; 
	}
	
}

 

// Compile Command :- gcc -o OGL OGL.c -lX11 -lGL -lGLU -lGLEW
// Run Command :- ./OGL


