// standard headers
#include<stdio.h> // for standard io
#include<stdlib.h> // for exit()
#include "Sphere.h"
#include<memory.h> // for memset()

// X11 headers
#include<X11/Xlib.h> // Similar like windows.h	
#include<X11/Xutil.h> // XVisualInfo
#include<X11/XKBlib.h> // for Keyboard

// OpenGL header files
#include<GL/glew.h>
#include<GL/gl.h> // for OpenGL functionality
#include<GL/glx.h> // fro briging API
#include"vmath.h"
using namespace vmath;

// texture library header
#include <SOIL/SOIL.h>

// OpenGL library
#pragma comment(lib,"OpenGL32.lib")//programatically sangtoy majilib file 
//ekadun ghe mi cmd la denar nahi
#pragma comment(lib,"GLU32.lib")

// Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define FBO_WIDTH 512
#define FBO_HEIGHT 512

// global veriable
Display *display = NULL;
XVisualInfo *visualInfo = NULL; // 10 member che struct aahe aat made visual struct aahe jyache swatat 8 member aahet
Colormap colorMap;
Window window;
Bool fullscreen = False;
Bool bActiveWindow = False;
FILE* gpFile = NULL;

// OpenGL realeated veriable
/*typedef GLXContext (*glXCreateContexAttribsARBProc)(Display* ,GLXFBConfig,GLXContext,Bool,const int *);
glXCreateContexAttribsARBProc glXCreateContexAttribsARB = NULL;*/
GLXFBConfig glxFbConfig;

typedef GLXContext (*glXCreateContextAttribsARPProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARPProc glXCreateContextAttribsARB=NULL;

GLXContext glxContext;

int winWidth;
int winHeight;

// programable pipeline related global veriable
GLuint shaderProgramObject;

enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

GLuint vao_cube;
GLuint vbo_cube_position;
GLuint vbo_cube_texcoord;

//GLuint texture_marble;
GLuint textureSamplerUniform;

// cube mvp uniform
GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix;

GLfloat angleCube = 0.0f;

// fbo (frame buffer object) related global variables
GLuint fbo; // frame buffer object
GLuint rbo; // render buffer object
GLuint fbo_texture;
bool bFBO_Result = false;

// texture scene global variables
GLuint shaderProgramObject_Sphere;
GLuint shaderProgramObject_Sphere_PF;
GLuint shaderProgramObject_Sphere_PV;

GLuint vao_sphere;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal;
GLuint vbo_sphere_elements;

mat4 perspectiveProjectionMatrix_sphere;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int sphere_gNumVertices, sphere_gNumElements;

// Uniform
GLuint modelMatrixUniform_sphere;
GLuint viewMatrixUniform_sphere;
GLuint projectionMatrixUniform_sphere;

GLuint laUniform_sphere[3]; // light ambient
GLuint ldUniform_sphere[3]; // light diffuse
GLuint lsUniform_sphere[3]; // light specular
GLuint lightPositionUniform_sphere[3];

GLuint kaUniform_sphere;
GLuint kdUniform_sphere;
GLuint ksUniform_sphere;
GLuint materialShininessUniform_sphere;

GLuint lightingEnabledUniform_sphere;

struct Light
{
	vec4 lightAmbient_sphere;
	vec4 lightDiffuse_sphere;
	vec4 lightSpecular_sphere;
	vec4 lightPosition_sphere;
};

Light lights_sphere[3];

Bool gbLight = False;

GLfloat gbMaterialAmbient_sphere[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat gbMaterialDiffuse_sphere[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // White 
GLfloat gbMaterialSpecular_sphere[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat gbMaterialShininess_sphere = 128.0f; // 50.0f or 128.0f

GLfloat gbLightAngleZero_sphere = 0.0f;
GLfloat gbLightAngleOne_sphere = 0.0f;
GLfloat gbLightAngleTwo_sphere = 0.0f;

int counter = 1;

// entry point()
int main(void)
{
	// function declarations
	int initialize(void);
	void resize(int,int);
	void draw(void);
	void toggleFullscreen(void);
	void update(void);
	void uninitialize(void);
	
	// local veriables
	int defaultScreen;
	int defaultDepth;
	GLXFBConfig * glxFbConfigs = NULL;
	GLXFBConfig bestGlxFbConfigs;
	XVisualInfo *tempXVisualinfo = NULL;
	int numFBConfig;
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom wm_delete_window_atom;
	XEvent event;
	KeySym keysym;
	int screenWidth;
	int screenHeight;
	char keys[26];

	int iRetVal = 0;

	static int frameBufferAttributes[]= {   // static nahi kela tari chalto pn saglikade he static aahe
											GLX_X_RENDERABLE, True,
											GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
											GLX_RENDER_TYPE, GLX_RGBA_BIT,
											GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
											GLX_RED_SIZE, 8,
											GLX_GREEN_SIZE, 8,
											GLX_BLUE_SIZE, 8,
											GLX_ALPHA_SIZE, 8,
											GLX_STENCIL_SIZE,8,
											GLX_DEPTH_SIZE, 24,
											None // ete 0 dila tari cahlto pn xlib style sathi None
										}; // it is same like pixel format attribute
	Bool bDone = False;
	static int winWidth;
	static int winHeight;

	// code

	gpFile = fopen("Log.txt", "w"); 
	
	if (gpFile == 0)
	{
		printf("fopen() Failed");
		exit(0);
	}
	else
	{
		printf("Log File Successfully Created.\n\n");
	}

	// step 1 : Open the Display()

	display=XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		printf("ERROR:XOpenDisplay() Failed \n");
		uninitialize();
		exit(1);
	}
	
	// step 2 : Get default screen from display
	defaultScreen=XDefaultScreen(display);
	
	// step 3 : Get default screen from display & default screen
	defaultDepth=XDefaultDepth(display,defaultScreen);

	glxFbConfigs = glXChooseFBConfig(display , defaultScreen , frameBufferAttributes , &numFBConfig );
	
	if(glxFbConfigs == NULL)
	{
		fprintf(gpFile,"Error : glxFbConfigs() Faild \n");
		uninitialize();
		exit(1);
	}
	fprintf(gpFile,"Found no of frame buffer configs = %d \n",numFBConfig);

	// find best frame buffer config from array
	int bestFrameBufferConfig = -1;
	int wrostFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int wrostNumberOfSamples = 999;	

	for(int i = 0; i < numFBConfig ; i++)
	{
		tempXVisualinfo = glXGetVisualFromFBConfig(display , glxFbConfigs[i]);
		if(tempXVisualinfo != NULL)
		{
			int samples , sampleBuffers;
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLES, &samples);
			fprintf(gpFile, "Visual info = 0x%lu Found sampleBuffers = %d, smaples = %d \n", tempXVisualinfo->visualid, sampleBuffers, samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}

			if(wrostFrameBufferConfig < 0 || !sampleBuffers || samples < wrostNumberOfSamples)
			{
				wrostFrameBufferConfig = i ;
				wrostNumberOfSamples = samples; 
			}
		}
		XFree(tempXVisualinfo);
		tempXVisualinfo = NULL;
	}

	bestGlxFbConfigs = glxFbConfigs[bestFrameBufferConfig];

	glxFbConfig = bestGlxFbConfigs;

	XFree(glxFbConfigs);
	glxFbConfigs = NULL;

	visualInfo = glXGetVisualFromFBConfig(display, bestGlxFbConfigs);
	fprintf(gpFile," Visual id of best visual info is 0X%lu \n",visualInfo -> visualid);

	// step 4 : Get XVisualInfo by using glXChooseVisual & do error checking
	//visualInfo=glXChooseVisual(display,defaultScreen,frameBufferAttributes);
	
	/*if (visualInfo==NULL)
	{
		printf("ERROR:glXChooseVisual() Failed \n");
		uninitialize();
		exit(1);
	}*/
	
	// step 5 : Fill/initialize structure XSetWindowAttributes & along with that also state Colormap & EventMask
	memset(&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel=0;
	windowAttributes.background_pixel=XBlackPixel(display,defaultScreen);
	windowAttributes.background_pixmap=0;
	windowAttributes.colormap=XCreateColormap(display,
						   RootWindow(display,visualInfo->screen),
						   visualInfo->visual,
						   AllocNone);
	windowAttributes.event_mask=ExposureMask|KeyPressMask|StructureNotifyMask|FocusChangeMask;
	
	// step 6 : initialize grobal color map by using color map from window attributes
	colorMap=windowAttributes.colormap;
	
	// step 7 : Initialize window style called as styleMask (Style cha mukhvata lavne)
	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;
	
	// step 8 : Create window using XCreateWindow using XlibAPI & do error checking
	window=XCreateWindow(display,
			     RootWindow(display,visualInfo->screen),
			     100,
			     100,
			     WIN_WIDTH,
			     WIN_HEIGHT,
			     0,
			     visualInfo->depth,
			     InputOutput,
			     visualInfo->visual,
			     styleMask,
			     &windowAttributes);
	if(!window)
	{
		printf("ERROR:XCreateWindow() Failed \n");
		uninitialize();
		exit(1);
	}
			     
	// step 9 : Give name to your window in it's title / caption bar
	XStoreName(display,window,"OpenGL Window - Mandar Maske RTR2021-116");
	
	// step 10 : Prepare our window to respond to :
	// a : Closing window by clicking  by close button
	// b : Closing window by close option  in system menu by creating & setting WindowManager protocol atom  

	wm_delete_window_atom=XInternAtom(display,"WM_DELETE_WINDOW",True);   // here WM:WindowManager
	
	XSetWMProtocols(display,window,&wm_delete_window_atom,1);
	
	// step 11 : Show the window by XMapWindow()
	XMapWindow(display,window);

	// Centering of window
	screenWidth=XWidthOfScreen(XScreenOfDisplay(display,defaultScreen));
	screenHeight=XHeightOfScreen(XScreenOfDisplay(display,defaultScreen));
	XMoveWindow(display,window,screenWidth/2-WIN_WIDTH/2,screenHeight/2-WIN_HEIGHT/2);
	

	
	iRetVal = initialize();

	if (iRetVal == -1)
	{
		fprintf(gpFile, "GLEW initialization Failed\n\n");
		uninitialize();
	}

	else if (iRetVal == -2)
	{
		fprintf(gpFile, "createFBO Failed\n\n");
		uninitialize();
	}

	// step 12 : Create a message loop by :
	// a : Getting next event XNextEvent()
	// b : Handling  keypress by Escape key
	// c : By handling message 33
	// message loop
	while (bDone==False)
	{
		while(XPending(display)) // Xpendind mhanje windows madle peek msg
		{
			// a : Getting next event XNextEvent()
			XNextEvent(display,&event);
		
			switch(event.type)
			{
				case MapNotify: // it is similar like WM_CREATE
					break;

				case FocusIn: // set focus
					bActiveWindow = True;
					break;

				case FocusOut: // kill focus
					bActiveWindow = False;
					break;
				
				case KeyPress:
					keysym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);
				
					switch(keysym)
					{
						// b : Handling  keypress by Escape key
						case XK_Escape:
							if(fullscreen==False)
							{
								toggleFullscreen();
								fullscreen=True;
							}
							else
							{
								toggleFullscreen();
								fullscreen=False;
							}
							break;
					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					
					switch(keys[0])
					{
						case 'Q':
						case 'q':
							bDone = True;
							break;

						case 'L':
						case 'l':
							if (gbLight == False)
							{
								counter = 1;
								gbLight = True;
							}
							else
							{
								gbLight = False;
							}
							break;
					
						case 'F':
						case 'f':
							counter = 2;
						break;

						case 'V':
						case 'v':
							counter = 1;
							break;
					}
					break;

				case ConfigureNotify: // it is similar like WM_Size:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
						
				// c : By handling message 33
				case 33: // all close function working here // it is analogus to wm_delete_window_atom
					bDone = True;
					break;	
			}				
		}
		if (bActiveWindow == True)
		{
			update();

			draw();
		}
	}
			
	// step 13 : After closing message loop call uninitialize() & return()	     
	uninitialize();
	
	return(0);
}

void toggleFullscreen(void)
{
	// local veriables
	Atom wm_current_state_atom;
	Atom wm_fullscreen_state_atom;
	XEvent event;

	// code
	wm_current_state_atom=XInternAtom(display,"_NET_WM_STATE",False);
	wm_fullscreen_state_atom=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);

	memset(&event,0,sizeof(XEvent));
	event.type=ClientMessage;
	event.xclient.window=window;
	event.xclient.message_type=wm_current_state_atom;
	event.xclient.format=32;
	event.xclient.data.l[0]=fullscreen?0:1;
	event.xclient.data.l[1]=wm_fullscreen_state_atom;

	XSendEvent(display,
			   RootWindow(display,visualInfo->screen),
			   False,
			   SubstructureNotifyMask,
			   &event);	//similar like sendmessage & Postmessage 
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	void uninitialize(void);
	void printGLInfo(void);
	Bool createFBO(GLint, GLint);
	int initialize_sphere(int, int);
	
	//code
	//glxContext = glXCreateContext(display,visualInfo,NULL,True);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARPProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL)
	{
		fprintf(gpFile, "glXCreateContextAttribesARP Failed\n\n");
		uninitialize();
		exit(1);
	}

	GLint contextAttributes[]=
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,6,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	//glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	if(!glxContext)
	{
		GLint contextAttributes[]=
		{
		GLX_CONTEXT_MAJOR_VERSION_ARB,1,
		GLX_CONTEXT_MINOR_VERSION_ARB,0,
		None
		};
		glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
		fprintf(gpFile, "Can not Support 4.6 hence falling back to default version\n\n");
	}
	else
	{
		fprintf(gpFile, "Found the support to 4.6 version \n\n");
	}

	//Cheking S/w or H/w 
	if(!glXIsDirect(display,glxContext))
	{
		fprintf(gpFile, "Direct(h/w) Rendering is not Supported \n\n");
	}
	else
	{
		fprintf(gpFile, "Direct(h/w) Rendering is Supported \n\n");
	}

	// make current context
	glXMakeCurrent(display, window, glxContext);
	//Glew Initialization
	if (glewInit() != GLEW_OK)
	{
		return -1;
	}

	// print OpenGLInfo
	printGLInfo();

	// StepC 1 : writing shading code
	// vertex shader 
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec2 a_texcoord;" \
		"uniform mat4 u_mvpMatrix;" \
		"out vec2 a_texcoord_out;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvpMatrix * a_position;" \
			"a_texcoord_out = a_texcoord;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);  
		
		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
				
				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec2 a_texcoord_out;" \
		"uniform sampler2D u_textureSampler;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"FragColor = texture(u_textureSampler, a_texcoord_out);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
	
	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
	glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize();

			}
		}
	}

	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
	textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_textureSampler");

	// vao & vbo related code
	//declarations of vertex data arrays
	const GLfloat cubePosition[] =
	{
	
	// front
	1.0f, 1.0f, 1.0f,
   -1.0f, 1.0f, 1.0f,
   -1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,

	// right
	1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,

	// back
	1.0f, 1.0f, -1.0f,
   -1.0f, 1.0f, -1.0f,
   -1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	// left
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,

	// top
	1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,

	// bottom
	1.0f, -1.0f, -1.0f,
   -1.0f, -1.0f, -1.0f,
   -1.0f, -1.0f,  1.0f,
	1.0f, -1.0f,  1.0f,

	};

	const GLfloat cubeTexcoords[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
	};

	// cube
	// vao
	glGenVertexArrays(1, &vao_cube); // vao:- vertex array object
	glBindVertexArray(vao_cube);

	// vbo for cubeposition
	glGenBuffers(1, &vbo_cube_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubePosition), cubePosition, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);

	// cube color
	glGenBuffers(1, &vbo_cube_texcoord);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texcoord); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoords), cubeTexcoords, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	glBindVertexArray(0);

	// here start OpenGL code 
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	perspectiveProjectionMatrix = mat4::identity();

	// fbo code
	bFBO_Result = createFBO(FBO_WIDTH, FBO_HEIGHT);

	if (bFBO_Result == true)
	{
		initialize_sphere(FBO_WIDTH, FBO_HEIGHT);

		// here there should be error checking
	}
	else
	{
		//fprintf(gpFile, "CreateFBO Failed Checked\n");
		return(-2);
	}	

	// enabling the texture
	glEnable(GL_TEXTURE_2D);

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

int initialize_sphere(int width, int height)
{
	// function declarations
	void uninitialize_sphere(void);
	void resize_sphere(int width, int height);

	// as all variables and checking is done in main initialize

	// Lights Shader
	const GLchar* vertexShaderSourceCode_PV =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec3 u_la[3];" \
		"uniform vec3 u_ld[3];" \
		"uniform vec3 u_ls[3];" \
		"uniform vec4 u_lightPosition[3];" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShininess;" \
		"uniform int u_lightingEnabled;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec3 ambient[3];" \
		"vec3 lightDirection[3];" \
		"vec3 diffuse[3];" \
		"vec3 reflectionVector[3];" \
		"vec3 specular[3];" \
		"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
		"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
		"vec3 transformedNormals = normalize(normalMatrix * a_normal);" \
		"vec3 viewerVector = normalize(-eyeCoordinates.xyz);" \
		"for(int i = 0; i < 3; i++)" \
		"{"\
		"ambient[i] = u_la[i] * u_ka;"
		"lightDirection[i] = normalize(vec3(u_lightPosition[i]) - eyeCoordinates.xyz);" \
		"diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformedNormals), 0.0);" \
		"reflectionVector[i] = reflect(-lightDirection[i], transformedNormals);" \
		"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewerVector), 0.0), u_materialShininess);" \
		"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" \
		"}"\
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject_PV = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject_PV, 1, (const GLchar**)&vertexShaderSourceCode_PV, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject_PV); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject_PV, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_PV, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_PV, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader Sphere PV compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize_sphere();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode_PV =
		"#version 460 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_light,1.0);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject_PV = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject_PV, 1, (const GLchar**)&fragmentShaderSourceCode_PV, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject_PV); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject_PV, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_PV, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_PV, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader Sphere PV compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize_sphere();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject_Sphere_PV = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject_Sphere_PV, vertexShaderObject_PV); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject_Sphere_PV, fragmentShaderObject_PV);

	glBindAttribLocation(shaderProgramObject_Sphere_PV, AMC_ATTRIBUTE_POSITION, "a_position");
	glBindAttribLocation(shaderProgramObject_Sphere_PV, AMC_ATTRIBUTE_NORMAL, "a_normal");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject_Sphere_PV);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject_Sphere_PV, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_Sphere_PV, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject_Sphere_PV, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program PV Sphere link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize_sphere();

			}
		}
	}

	modelMatrixUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
	viewMatrixUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_viewMatrix");
	projectionMatrixUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_projectionMatrix");

	// light uniform
	laUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[0]");
	ldUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[0]");
	lsUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[0]");
	lightPositionUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[0]");

	laUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[1]");
	ldUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[1]");
	lsUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[1]");
	lightPositionUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[1]");

	laUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[2]");
	ldUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[2]");
	lsUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[2]");
	lightPositionUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[2]");

	// material uniform
	kaUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ka");
	kdUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_kd");
	ksUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ks");
	materialShininessUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_materialShininess");

	// light enable uniform
	lightingEnabledUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightingEnabled");

	// vertex shader for per fragment
	const GLchar* vertexShaderSourceCode_PF =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec3 a_normal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform vec4 u_lightPosition[3];" \
		"uniform int u_lightingEnabled;" \
		"out vec3 transformedNormals;" \
		"out vec3 lightDirection[3];" \
		"out vec3 viewerVector;" \
		"void main(void)" \
		"{" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
		"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
		"transformedNormals = normalMatrix * a_normal;" \
		"viewerVector = -eyeCoordinates.xyz;" \
		"for(int i = 0; i < 3; i++)" \
		"{" \
		"lightDirection[i] = vec3(u_lightPosition[i]) - eyeCoordinates.xyz;" \
		"}" \
		"}" \
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject_PF = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject_PF, 1, (const GLchar**)&vertexShaderSourceCode_PF, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject_PF); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject_PF, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_PF, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex PerFragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize_sphere();
			}
		}
	}

	// fragment shader for per vertex
	const GLchar* fragmentShaderSourceCode_PF =
		"#version 460 core" \
		"\n" \
		"in vec3 transformedNormals;" \
		"in vec3 lightDirection[3];" \
		"in vec3 viewerVector;" \
		"uniform vec3 u_la[3];" \
		"uniform vec3 u_ld[3];" \
		"uniform vec3 u_ls[3];" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShininess;" \
		"uniform int u_lightingEnabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_light;" \
		"if(u_lightingEnabled == 1)" \
		"{" \
		"vec3 ambient[3];" \
		"vec3 diffuse[3];" \
		"vec3 reflectionVector[3];" \
		"vec3 specular[3];" \
		"vec3 normalized_transformed_normal;" \
		"vec3 normalized_lightDirection[3];" \
		"vec3 normalized_viewerVector;" \
		"normalized_transformed_normal = normalize(transformedNormals); " \
		"normalized_viewerVector = normalize(viewerVector);" \
		"for(int i = 0; i < 3; i++)" \
		"{" \
		"normalized_lightDirection[i] = normalize(lightDirection[i]);" \
		"ambient[i] = u_la[i] * u_ka;" \
		"diffuse[i] = u_ld[i] * u_kd * max(dot(normalized_lightDirection[i], normalized_transformed_normal), 0.0);" \
		"reflectionVector[i] = reflect(-normalized_lightDirection[i], normalized_transformed_normal);" \
		"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalized_viewerVector), 0.0), u_materialShininess);" \
		"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" \
		"}" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_light,1.0);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject_PF = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject_PF, 1, (const GLchar**)&fragmentShaderSourceCode_PF, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject_PF); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject_PF, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_PF, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize_sphere();
			}
		}
	}

	// Shader program object perfragment
	// stepD 1 : create shader program object
	shaderProgramObject_Sphere_PF = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject_Sphere_PF, vertexShaderObject_PF); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject_Sphere_PF, fragmentShaderObject_PF);

	glBindAttribLocation(shaderProgramObject_Sphere_PF, AMC_ATTRIBUTE_POSITION, "a_position");
	glBindAttribLocation(shaderProgramObject_Sphere_PF, AMC_ATTRIBUTE_NORMAL, "a_normal");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject_Sphere_PF);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject_Sphere_PF, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_Sphere_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject_Sphere_PF, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program PerFragment link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize_sphere();

			}
		}
	}

	// perfragment
	modelMatrixUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
	viewMatrixUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_viewMatrix");
	projectionMatrixUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_projectionMatrix");

	// light uniform
	laUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[0]");
	ldUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[0]");
	lsUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[0]");
	lightPositionUniform_sphere[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[0]");

	laUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[1]");
	ldUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[1]");
	lsUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[1]");
	lightPositionUniform_sphere[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[1]");

	laUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[2]");
	ldUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[2]");
	lsUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[2]");
	lightPositionUniform_sphere[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[2]");

	// material uniform
	kaUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ka");
	kdUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_kd");
	ksUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ks");
	materialShininessUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_materialShininess");

	// light enable uniform
	lightingEnabledUniform_sphere = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightingEnabled");

	// StepC 1 : writing shading code
	// vertex shader 
	const GLchar* vertexShaderSourceCode_Sphere =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * a_position;" \
		"}";

	// StepC 2 : creating shading object
	GLuint vertexShaderObject_Sphere = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject_Sphere, 1, (const GLchar**)&vertexShaderSourceCode_Sphere, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject_Sphere); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject_Sphere, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject_Sphere, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject_Sphere, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize_sphere();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode_Sphere =
		"#version 460 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject_Sphere = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject_Sphere, 1, (const GLchar**)&fragmentShaderSourceCode_Sphere, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject_Sphere); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject_Sphere, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject_Sphere, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject_Sphere, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize_sphere();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject_Sphere = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject_Sphere, vertexShaderObject_Sphere); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject_Sphere, fragmentShaderObject_Sphere);

	glBindAttribLocation(shaderProgramObject_Sphere, AMC_ATTRIBUTE_POSITION, "a_position");

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject_Sphere);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject_Sphere, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject_Sphere, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject_Sphere, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize_sphere();

			}
		}
	}

	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject_Sphere, "u_mvpMatrix");

	// vao & vbo related code
	//declarations of vertex data arrays
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	sphere_gNumVertices = getNumberOfSphereVertices();
	sphere_gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	// position vbo
	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &vbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// here start OpenGL code (depth & clear color code)
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix_sphere = mat4::identity();

	lights_sphere[0].lightAmbient_sphere = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	lights_sphere[0].lightDiffuse_sphere = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f); // Red Light
	lights_sphere[0].lightSpecular_sphere = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	lights_sphere[0].lightPosition_sphere = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	lights_sphere[1].lightAmbient_sphere = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	lights_sphere[1].lightDiffuse_sphere = vmath::vec4(0.0f, 1.0f, 0.0f, 1.0f); // Green Light
	lights_sphere[1].lightSpecular_sphere = vmath::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	lights_sphere[1].lightPosition_sphere = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	lights_sphere[2].lightAmbient_sphere = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	lights_sphere[2].lightDiffuse_sphere = vmath::vec4(0.0f, 0.0f, 1.0f, 1.0f); // Blue Light
	lights_sphere[2].lightSpecular_sphere = vmath::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	lights_sphere[2].lightPosition_sphere = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	//warmup resize call
	resize_sphere(FBO_WIDTH, FBO_HEIGHT);

	return(0);
}

bool createFBO(GLint textureWidth, GLint textureHeight)
{
	// code
	// step 1 - Check available render buffer size
	int maxRenderbufferSize;

	glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &maxRenderbufferSize);

	if (maxRenderbufferSize < textureWidth || maxRenderbufferSize < textureHeight)
	{
		fprintf(gpFile, "Insufficient Render Buffer Size\n");
		return(false);
	}
	
	// Step 2 - Create Frame Buffer Object
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// Step 3 - Create Render Buffer Object
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);

	// Step 4 - Where to keep this render buffer (Storage and format of render buffer)
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, textureWidth, textureHeight); // 1 - target, 2 - format, 3, 4 - size kiti laagel storage la

	// Step 5 - Create empty texture for upcoming target scene
	glGenTextures(1, &fbo_texture);
	glBindTexture(GL_TEXTURE_2D, fbo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // horizontall
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // vertical
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, NULL); // GL_UNSIGNED_SHORT_5_6_5 matches with GL_DEPTH_COMPONENT16
	// 5_6_5 - Red, Green, Blue, why 6 - human eye is more adjustable to green color spectrum, (GL_DEPTH_COMPONENT16) 16 / 3 hot nahi so 5, 6, 5 kela
	// green la ektra 1 bit dila
	// last parameter is null ka - karan empty full window texture

	// Step 6 - Give Above texture to fbo
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture, 0); 
	// 1 - target kuthe aahe buffer, 2 - kuthe chitkavu texture, 3 - binding point, 4 - binding point la konta texture, 5 - mipmap level

	// Step 7 - Give rbo to fbo (empty texture kasa bharaycha tar rbo ne)
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
	// 1 - target kuthe aahe buffer, 2 - frame buffer na render buffer kuthun baghaycha aahe tar depth buffer, 3 - konala taaku, 4 - kuth jo data ahe to kon aahe

	// Step 8 - Check whether the frame buffer created successfull or not
	GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	// 1 - target la aani frame buffer ahe ka nahi check kar

	if (result != GL_FRAMEBUFFER_COMPLETE)
	{
		fprintf(gpFile, "Frame Buffer is not complete\n");
		return(false);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);// framebuffer unbind zhala ki render buffer ani texture automatically unbind hotat

	return(true);
}



void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}



void resize(int width,int height)
{
	// code
	winWidth = width;
	winHeight = height;

	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void resize_sphere(int width, int height)
{
	// code
	if (height == 0)
		height = 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix_sphere = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void draw(void)
{
	void display_sphere(GLint, GLint);
	void update_sphere(void);

	// code
	if (bFBO_Result == true)
	{
		display_sphere(FBO_WIDTH, FBO_HEIGHT);

		update_sphere();
	}

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	resize(winWidth, winHeight);

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject);

	// square
	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();
	mat4 rotationMatrix_x = mat4::identity();
	mat4 rotationMatrix_y = mat4::identity();
	mat4 rotationMatrix_z = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	rotationMatrix_x = vmath::rotate(angleCube, 1.0f, 0.0f, 0.0f);
	rotationMatrix_y = vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
	rotationMatrix_z = vmath::rotate(angleCube, 0.0f, 0.0f, 1.0f);
	rotationMatrix = rotationMatrix_x * rotationMatrix_y * rotationMatrix_x;
	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); //glTranslatef() is replaced by this line
	scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
	modelViewMatrix = translationMatrix * rotationMatrix * scaleMatrix; // order is imp
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // order imp aahe

	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fbo);
	glUniform1i(textureSamplerUniform, 0);

	glBindVertexArray(vao_cube);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //PP made glQuad nahi other option ne draw karayche
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);


	// stepE 3 : unuse the shader program object
	
	glUseProgram(0);
	
	glXSwapBuffers(display,window);
}

void update(void)
{
	// code

	angleCube = angleCube + 0.003f;

	if (angleCube >= 360.0f)
		angleCube = angleCube - 360.0f;
	
}

void display_sphere(GLint textureWidth, GLint textureHeight)
{
	// code
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	resize_sphere(textureWidth, textureHeight);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject_Sphere);

	if (counter == 1)
	{
		glUseProgram(shaderProgramObject_Sphere_PV);
	}
	else if (counter == 2)
	{
		glUseProgram(shaderProgramObject_Sphere_PF);
	}

	// Red Light
	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	//sprintf(str, "LightAngleZero = %f, LightAngleOne = %f, LightAngleTwo = %f", gbLightAngleZero_sphere, gbLightAngleOne_sphere, gbLightAngleTwo_sphere);
	//SetWindowTextA(ghwnd, str);

	modelMatrix = translationMatrix * rotationMatrix; // order is imp

	glUniformMatrix4fv(modelMatrixUniform_sphere, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
	glUniformMatrix4fv(viewMatrixUniform_sphere, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform_sphere, 1, GL_FALSE, perspectiveProjectionMatrix_sphere);

	if (gbLight == True)
	{
		glUniform1i(lightingEnabledUniform_sphere, 1);

		glUniform3fv(laUniform_sphere[0], 1, lights_sphere[0].lightAmbient_sphere);
		glUniform3fv(ldUniform_sphere[0], 1, lights_sphere[0].lightDiffuse_sphere);
		glUniform3fv(lsUniform_sphere[0], 1, lights_sphere[0].lightSpecular_sphere);
		glUniform4fv(lightPositionUniform_sphere[0], 1, vmath::vec4(0.0f, sin(gbLightAngleZero_sphere) * 2, cos(gbLightAngleZero_sphere) * 2, 1.0f));

		glUniform3fv(laUniform_sphere[1], 1, lights_sphere[1].lightAmbient_sphere);
		glUniform3fv(ldUniform_sphere[1], 1, lights_sphere[1].lightDiffuse_sphere);
		glUniform3fv(lsUniform_sphere[1], 1, lights_sphere[1].lightSpecular_sphere);
		glUniform4fv(lightPositionUniform_sphere[1], 1, vmath::vec4(sin(gbLightAngleZero_sphere) * 2, 0.0f, cos(gbLightAngleZero_sphere) * 2, 1.0f));

		glUniform3fv(laUniform_sphere[2], 1, lights_sphere[2].lightAmbient_sphere);
		glUniform3fv(ldUniform_sphere[2], 1, lights_sphere[2].lightDiffuse_sphere);
		glUniform3fv(lsUniform_sphere[2], 1, lights_sphere[2].lightSpecular_sphere);
		glUniform4fv(lightPositionUniform_sphere[2], 1, vmath::vec4(sin(gbLightAngleZero_sphere) * 2, cos(gbLightAngleZero_sphere) * 2, 0.0f, 1.0f));


		glUniform3fv(kaUniform_sphere, 1, gbMaterialAmbient_sphere);
		glUniform3fv(kdUniform_sphere, 1, gbMaterialDiffuse_sphere);
		glUniform3fv(ksUniform_sphere, 1, gbMaterialSpecular_sphere);

		glUniform1f(materialShininessUniform_sphere, gbMaterialShininess_sphere);
	}
	else
	{
		glUniform1i(lightingEnabledUniform_sphere, 0);
	}

	glBindVertexArray(vao_sphere);

	// stepE 2 : draw the desiered graphics/animation
	// here will be magic code

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, sphere_gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	// stepE 3 : unuse the shader program object

	glUseProgram(0);
	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//SwapBuffers(ghdc);
}

void update_sphere(void)
{
	// code
	gbLightAngleZero_sphere = gbLightAngleZero_sphere + 0.0005f;

	if (gbLightAngleZero_sphere >= 360.0f)
		gbLightAngleZero_sphere = gbLightAngleZero_sphere - 360.0f;
}


void uninitialize(void)
{
	// function declarations
	void toggleFullScreen(void);
	void uninitialize_sphere(void);	
	
	// code
	GLXContext currentContext;
	currentContext=glXGetCurrentContext();

	uninitialize_sphere();

	if (vbo_cube_position)
	{
		glDeleteBuffers(1, &vbo_cube_position);
		vbo_cube_position = 0;
	}

	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	if (vbo_cube_texcoord)
	{
		glDeleteBuffers(1, &vbo_cube_texcoord);
		vbo_cube_texcoord = 0;
	}

	// shader uninitialization
	if (shaderProgramObject)
	{
		// stepF 0
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	if(currentContext && currentContext == glxContext)
	{
		glXMakeCurrent(display,0,0);
	}
	
	if(glxContext)
	{
		glXDestroyContext(display,glxContext);
		glxContext = NULL;
	}

	if(visualInfo)
	{
		free(visualInfo);
		visualInfo = NULL;
	}

	if (fullscreen)
	{
		toggleFullscreen();
		fullscreen = False;
	}

	if(window)
	{
		XDestroyWindow(display,window);
	}
	
	if(colorMap)
	{
		XFreeColormap(display,colorMap);
	}
	
	//thos we uninitialize display & colormap we don't uninitialize visualinfo because we cant create visual info we just matched/obtained visual info
	
	if(display)
	{
		XCloseDisplay(display);
		display=NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); 
		gpFile = NULL; 
	}
	
}

void uninitialize_sphere(void)
{
	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}

	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}

	if (vbo_sphere_elements)
	{
		glDeleteBuffers(1, &vbo_sphere_elements);
		vbo_sphere_elements = 0;
	}

	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	// shader uninitialization
	if (shaderProgramObject_Sphere)
	{
		// stepF 0
		glUseProgram(shaderProgramObject_Sphere);

		GLsizei numAttachedShaders;

		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject_Sphere, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject_Sphere, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject_Sphere, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject_Sphere);
		shaderProgramObject_Sphere = 0;
	}
}

 

// Compile Command :- gcc -o OGL OGL.c -lX11 -lGL -lGLU -lGLEW
// Run Command :- ./OGL


