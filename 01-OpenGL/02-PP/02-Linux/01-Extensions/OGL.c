// standard headers
#include<stdio.h> // for standard io
#include<stdlib.h> // for exit()
#include<memory.h> // for memset()

// X11 headers
#include<X11/Xlib.h> // Similar like windows.h	
#include<X11/Xutil.h> // XVisualInfo
#include<X11/XKBlib.h> // for Keyboard

// OpenGL header files
#include<GL/glew.h>
#include<GL/gl.h> // for OpenGL functionality
#include<GL/glx.h> // fro briging API

// OpenGL library
#pragma comment(lib,"OpenGL32.lib")//programatically sangtoy majilib file 
//ekadun ghe mi cmd la denar nahi
#pragma comment(lib,"GLU32.lib")


// Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable
Display *display = NULL;
XVisualInfo *visualInfo = NULL; // 10 member che struct aahe aat made visual struct aahe jyache swatat 8 member aahet
Colormap colorMap;
Window window;
Bool fullscreen = False;
Bool bActiveWindow = False;
FILE* gpFile = NULL;


// OpenGL realeated veriable
/*typedef GLXContext (*glXCreateContexAttribsARBProc)(Display* ,GLXFBConfig,GLXContext,Bool,const int *);
glXCreateContexAttribsARBProc glXCreateContexAttribsARB = NULL;*/
GLXFBConfig glxFbConfig;

typedef GLXContext (*glXCreateContextAttribsARPProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARPProc glXCreateContextAttribsARB=NULL;

GLXContext glxContext;

// entry point()
int main(void)
{
	// function declarations
	int initialize(void);
	void resize(int,int);
	void draw(void);
	void toggleFullscreen(void);
	void update(void);
	void uninitialize(void);
	
	// local veriables
	int defaultScreen;
	int defaultDepth;
	GLXFBConfig * glxFbConfigs = NULL;
	GLXFBConfig bestGlxFbConfigs;
	XVisualInfo *tempXVisualinfo = NULL;
	int numFBConfig;
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom wm_delete_window_atom;
	XEvent event;
	KeySym keysym;
	int screenWidth;
	int screenHeight;
	char keys[26];

	static int frameBufferAttributes[]= {   // static nahi kela tari chalto pn saglikade he static aahe
											GLX_X_RENDERABLE, True,
											GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
											GLX_RENDER_TYPE, GLX_RGBA_BIT,
											GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
											GLX_RED_SIZE, 8,
											GLX_GREEN_SIZE, 8,
											GLX_BLUE_SIZE, 8,
											GLX_ALPHA_SIZE, 8,
											GLX_STENCIL_SIZE,8,
											GLX_DEPTH_SIZE, 24,
											None // ete 0 dila tari cahlto pn xlib style sathi None
										}; // it is same like pixel format attribute
	Bool bDone = False;
	static int winWidth;
	static int winHeight;


	// code

	gpFile = fopen("Log.txt", "w"); 
	
	if (gpFile == 0)
	{
		printf("fopen() Failed");
		exit(0);
	}
	else
	{
		printf("Log File Successfully Created.\n\n");
	}

	// step 1 : Open the Display()

	display=XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		printf("ERROR:XOpenDisplay() Failed \n");
		uninitialize();
		exit(1);
	}
	
	// step 2 : Get default screen from display
	defaultScreen=XDefaultScreen(display);
	
	// step 3 : Get default screen from display & default screen
	defaultDepth=XDefaultDepth(display,defaultScreen);

	glxFbConfigs = glXChooseFBConfig(display , defaultScreen , frameBufferAttributes , &numFBConfig );
	
	if(glxFbConfigs == NULL)
	{
		fprintf(gpFile,"Error : glxFbConfigs() Faild \n");
		uninitialize();
		exit(1);
	}
	fprintf(gpFile,"Found no of frame buffer configs = %d \n",numFBConfig);

	// find best frame buffer config from array
	int bestFrameBufferConfig = -1;
	int wrostFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int wrostNumberOfSamples = 999;	

	for(int i = 0; i < numFBConfig ; i++)
	{
		tempXVisualinfo = glXGetVisualFromFBConfig(display , glxFbConfigs[i]);
		if(tempXVisualinfo != NULL)
		{
			int samples , sampleBuffers;
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLES, &samples);
			fprintf(gpFile, "Visual info = 0x%lu Found sampleBuffers = %d, smaples = %d \n", tempXVisualinfo->visualid, sampleBuffers, samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}

			if(wrostFrameBufferConfig < 0 || !sampleBuffers || samples < wrostNumberOfSamples)
			{
				wrostFrameBufferConfig = i ;
				wrostNumberOfSamples = samples; 
			}
		}
		XFree(tempXVisualinfo);
		tempXVisualinfo = NULL;
	}

	bestGlxFbConfigs = glxFbConfigs[bestFrameBufferConfig];

	glxFbConfig = bestGlxFbConfigs;

	XFree(glxFbConfigs);
	glxFbConfigs = NULL;

	visualInfo = glXGetVisualFromFBConfig(display, bestGlxFbConfigs);
	fprintf(gpFile," Visual id of best visual info is 0X%lu \n",visualInfo -> visualid);


	// step 4 : Get XVisualInfo by using glXChooseVisual & do error checking
	//visualInfo=glXChooseVisual(display,defaultScreen,frameBufferAttributes);
	
	/*if (visualInfo==NULL)
	{
		printf("ERROR:glXChooseVisual() Failed \n");
		uninitialize();
		exit(1);
	}*/
	
	// step 5 : Fill/initialize structure XSetWindowAttributes & along with that also state Colormap & EventMask
	memset(&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel=0;
	windowAttributes.background_pixel=XBlackPixel(display,defaultScreen);
	windowAttributes.background_pixmap=0;
	windowAttributes.colormap=XCreateColormap(display,
						   RootWindow(display,visualInfo->screen),
						   visualInfo->visual,
						   AllocNone);
	windowAttributes.event_mask=ExposureMask|KeyPressMask|StructureNotifyMask|FocusChangeMask;
	
	// step 6 : initialize grobal color map by using color map from window attributes
	colorMap=windowAttributes.colormap;
	
	// step 7 : Initialize window style called as styleMask (Style cha mukhvata lavne)
	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;
	
	// step 8 : Create window using XCreateWindow using XlibAPI & do error checking
	window=XCreateWindow(display,
			     RootWindow(display,visualInfo->screen),
			     100,
			     100,
			     WIN_WIDTH,
			     WIN_HEIGHT,
			     0,
			     visualInfo->depth,
			     InputOutput,
			     visualInfo->visual,
			     styleMask,
			     &windowAttributes);
	if(!window)
	{
		printf("ERROR:XCreateWindow() Failed \n");
		uninitialize();
		exit(1);
	}
			     
	// step 9 : Give name to your window in it's title / caption bar
	XStoreName(display,window,"OpenGL Window");
	
	// step 10 : Prepare our window to respond to :
	// a : Closing window by clicking  by close button
	// b : Closing window by close option  in system menu by creating & setting WindowManager protocol atom  

	wm_delete_window_atom=XInternAtom(display,"WM_DELETE_WINDOW",True);   // here WM:WindowManager
	
	XSetWMProtocols(display,window,&wm_delete_window_atom,1);
	
	// step 11 : Show the window by XMapWindow()
	XMapWindow(display,window);

	// Centering of window
	screenWidth=XWidthOfScreen(XScreenOfDisplay(display,defaultScreen));
	screenHeight=XHeightOfScreen(XScreenOfDisplay(display,defaultScreen));
	XMoveWindow(display,window,screenWidth/2-WIN_WIDTH/2,screenHeight/2-WIN_HEIGHT/2);
	
	initialize();

	// step 12 : Create a message loop by :
	// a : Getting next event XNextEvent()
	// b : Handling  keypress by Escape key
	// c : By handling message 33
	// message loop
	while (bDone==False)
	{
		while(XPending(display)) // Xpendind mhanje windows madle peek msg
		{
			// a : Getting next event XNextEvent()
			XNextEvent(display,&event);
		
			switch(event.type)
			{
				case MapNotify: // it is similar like WM_CREATE
					break;

				case FocusIn: // set focus
					bActiveWindow = True;
					break;

				case FocusOut: // kill focus
					bActiveWindow = False;
					break;
				
				case KeyPress:
					keysym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);
				
					switch(keysym)
					{
						// b : Handling  keypress by Escape key
						case XK_Escape:
							bDone = True;
							break;
					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					
					switch(keys[0])
					{
						case 'F':
						case 'f':
							if(fullscreen==False)
							{
								toggleFullscreen();
								fullscreen=True;
							}
							else
							{
								toggleFullscreen();
								fullscreen=False;
							}
						break;
					}
					break;

				case ConfigureNotify: // it is similar like WM_Size:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
						
				// c : By handling message 33
				case 33: // all close function working here // it is analogus to wm_delete_window_atom
					bDone = True;
					break;	
			}				
		}
		if (bActiveWindow == True)
		{
			update();

			draw();
		}
	}
			
	// step 13 : After closing message loop call uninitialize() & return()	     
	uninitialize();
	
	return(0);
}

void toggleFullscreen(void)
{
	// local veriables
	Atom wm_current_state_atom;
	Atom wm_fullscreen_state_atom;
	XEvent event;

	// code
	wm_current_state_atom=XInternAtom(display,"_NET_WM_STATE",False);
	wm_fullscreen_state_atom=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);

	memset(&event,0,sizeof(XEvent));
	event.type=ClientMessage;
	event.xclient.window=window;
	event.xclient.message_type=wm_current_state_atom;
	event.xclient.format=32;
	event.xclient.data.l[0]=fullscreen?0:1;
	event.xclient.data.l[1]=wm_fullscreen_state_atom;

	XSendEvent(display,
			   RootWindow(display,visualInfo->screen),
			   False,
			   SubstructureNotifyMask,
			   &event);	//similar like sendmessage & Postmessage 
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	void uninitialize(void);
	void printGLInfo(void);
	
	//code
	//glxContext = glXCreateContext(display,visualInfo,NULL,True);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARPProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL)
	{
		fprintf(gpFile, "glXCreateContextAttribesARP Failed\n\n");
		uninitialize();
		exit(1);
	}

	GLint contextAttributes[]=
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,6,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	//glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	if(!glxContext)
	{
		GLint contextAttributes[]=
		{
		GLX_CONTEXT_MAJOR_VERSION_ARB,1,
		GLX_CONTEXT_MINOR_VERSION_ARB,0,
		None
		};
		glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
		fprintf(gpFile, "Can not Support 4.6 hence falling back to default version\n\n");
	}
	else
	{
		fprintf(gpFile, "Found the support to 4.6 version \n\n");
	}

	//Cheking S/w or H/w 
	if(!glXIsDirect(display,glxContext))
	{
		fprintf(gpFile, "Direct(h/w) Rendering is not Supported \n\n");
	}
	else
	{
		fprintf(gpFile, "Direct(h/w) Rendering is Supported \n\n");
	}

	// make current context
	glXMakeCurrent(display, window, glxContext);
	//Glew Initialization
	if (glewInit() != GLEW_OK)
	{
		return -5;
	}

	// print OpenGLInfo
	printGLInfo();

	// here start OpenGL code 
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	//depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}

void resize(int width,int height)
{
	// code
	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

}

void draw(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	
	
	glXSwapBuffers(display,window);
}

void update(void)
{
	// code
	
}

void uninitialize(void)
{
	// function declarations
	void toggleFullScreen(void);
		
	// code
	GLXContext currentContext;
	currentContext=glXGetCurrentContext();

	if(currentContext && currentContext == glxContext)
	{
		glXMakeCurrent(display,0,0);
	}
	
	if(glxContext)
	{
		glXDestroyContext(display,glxContext);
		glxContext = NULL;
	}

	if(visualInfo)
	{
		free(visualInfo);
		visualInfo = NULL;
	}

	if (fullscreen)
	{
		toggleFullscreen();
		fullscreen = False;
	}

	if(window)
	{
		XDestroyWindow(display,window);
	}
	
	if(colorMap)
	{
		XFreeColormap(display,colorMap);
	}
	
	//thos we uninitialize display & colormap we don't uninitialize visualinfo because we cant create visual info we just matched/obtained visual info
	
	if(display)
	{
		XCloseDisplay(display);
		display=NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); 
		gpFile = NULL; 
	}
	
}

 

// Compile Command :- gcc -o OGL OGL.c -lX11 -lGL -lGLU -lGLEW
// Run Command :- ./OGL


