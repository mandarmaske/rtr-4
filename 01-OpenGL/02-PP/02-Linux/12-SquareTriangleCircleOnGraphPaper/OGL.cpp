// standard headers
#include<stdio.h> // for standard io
#include<stdlib.h> // for exit()
#include<memory.h> // for memset()

// X11 headers
#include<X11/Xlib.h> // Similar like windows.h	
#include<X11/Xutil.h> // XVisualInfo
#include<X11/XKBlib.h> // for Keyboard

// OpenGL header files
#include<GL/glew.h>
#include<GL/gl.h> // for OpenGL functionality
#include<GL/glx.h> // fro briging API
#include"vmath.h"
using namespace vmath;

// OpenGL library
#pragma comment(lib,"OpenGL32.lib")//programatically sangtoy majilib file 
//ekadun ghe mi cmd la denar nahi
#pragma comment(lib,"GLU32.lib")

// Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable
Display *display = NULL;
XVisualInfo *visualInfo = NULL; // 10 member che struct aahe aat made visual struct aahe jyache swatat 8 member aahet
Colormap colorMap;
Window window;
Bool fullscreen = False;
Bool bActiveWindow = False;
FILE* gpFile = NULL;

// OpenGL realeated veriable
/*typedef GLXContext (*glXCreateContexAttribsARBProc)(Display* ,GLXFBConfig,GLXContext,Bool,const int *);
glXCreateContexAttribsARBProc glXCreateContexAttribsARB = NULL;*/
GLXFBConfig glxFbConfig;

typedef GLXContext (*glXCreateContextAttribsARPProc)(Display*,GLXFBConfig,GLXContext,Bool,const int*);
glXCreateContextAttribsARPProc glXCreateContextAttribsARB=NULL;

GLXContext glxContext;

// programable pipeline related global veriable
GLuint shaderProgramObject;

enum 
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

// variables for X Axis Line
GLuint vao_horizontalLine;
GLuint vbo_horizontalLine_position;
GLuint vbo_horizontalLine_color;

// variables for Y Axis Line
GLuint vao_verticalLine;
GLuint vbo_verticalLine_position;
GLuint vbo_verticalLine_color;

// variables for Horizontal Graph Lines
GLuint vao_graphHLines;
GLuint vbo_graphHLines_position;
GLuint vbo_graphHLines_color;

// variables for Triangle Lines
GLuint vao_triangleLines;
GLuint vbo_triangleLines_position;
GLuint vbo_triangleLines_color;

// variables for Square Lines
GLuint vao_squareLines;
GLuint vbo_squareLines_position;
GLuint vbo_squareLines_color;

// variables for Square Lines
GLuint vao_circleLines;
GLuint vbo_circleLines_position;
GLuint vbo_circleLines_color;

// variables for Vertical Graph Lines
GLuint vao_graphVLines;
GLuint vbo_graphVLines_position;

GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix; 

float base = 1.2f;
float height = 0.0f;
float base1 = base;

Bool gFlag = False;
Bool tFlag = False;
Bool sFlag = False;
Bool cFlag = False;

GLfloat circleLine[101][3] = { 0 };

// entry point()
int main(void)
{
	// function declarations
	int initialize(void);
	void resize(int,int);
	void draw(void);
	void toggleFullscreen(void);
	void update(void);
	void uninitialize(void);
	
	// local veriables
	int defaultScreen;
	int defaultDepth;
	GLXFBConfig * glxFbConfigs = NULL;
	GLXFBConfig bestGlxFbConfigs;
	XVisualInfo *tempXVisualinfo = NULL;
	int numFBConfig;
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom wm_delete_window_atom;
	XEvent event;
	KeySym keysym;
	int screenWidth;
	int screenHeight;
	char keys[26];

	static int frameBufferAttributes[]= {   // static nahi kela tari chalto pn saglikade he static aahe
											GLX_X_RENDERABLE, True,
											GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
											GLX_RENDER_TYPE, GLX_RGBA_BIT,
											GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
											GLX_RED_SIZE, 8,
											GLX_GREEN_SIZE, 8,
											GLX_BLUE_SIZE, 8,
											GLX_ALPHA_SIZE, 8,
											GLX_STENCIL_SIZE,8,
											GLX_DEPTH_SIZE, 24,
											None // ete 0 dila tari cahlto pn xlib style sathi None
										}; // it is same like pixel format attribute
	Bool bDone = False;
	static int winWidth;
	static int winHeight;


	// code

	gpFile = fopen("Log.txt", "w"); 
	
	if (gpFile == 0)
	{
		printf("fopen() Failed");
		exit(0);
	}
	else
	{
		printf("Log File Successfully Created.\n\n");
	}

	// step 1 : Open the Display()

	display=XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		printf("ERROR:XOpenDisplay() Failed \n");
		uninitialize();
		exit(1);
	}
	
	// step 2 : Get default screen from display
	defaultScreen=XDefaultScreen(display);
	
	// step 3 : Get default screen from display & default screen
	defaultDepth=XDefaultDepth(display,defaultScreen);

	glxFbConfigs = glXChooseFBConfig(display , defaultScreen , frameBufferAttributes , &numFBConfig );
	
	if(glxFbConfigs == NULL)
	{
		fprintf(gpFile,"Error : glxFbConfigs() Faild \n");
		uninitialize();
		exit(1);
	}
	fprintf(gpFile,"Found no of frame buffer configs = %d \n",numFBConfig);

	// find best frame buffer config from array
	int bestFrameBufferConfig = -1;
	int wrostFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int wrostNumberOfSamples = 999;	

	for(int i = 0; i < numFBConfig ; i++)
	{
		tempXVisualinfo = glXGetVisualFromFBConfig(display , glxFbConfigs[i]);
		if(tempXVisualinfo != NULL)
		{
			int samples , sampleBuffers;
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
			glXGetFBConfigAttrib(display, glxFbConfigs[i], GLX_SAMPLES, &samples);
			fprintf(gpFile, "Visual info = 0x%lu Found sampleBuffers = %d, smaples = %d \n", tempXVisualinfo->visualid, sampleBuffers, samples);

			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}

			if(wrostFrameBufferConfig < 0 || !sampleBuffers || samples < wrostNumberOfSamples)
			{
				wrostFrameBufferConfig = i ;
				wrostNumberOfSamples = samples; 
			}
		}
		XFree(tempXVisualinfo);
		tempXVisualinfo = NULL;
	}

	bestGlxFbConfigs = glxFbConfigs[bestFrameBufferConfig];

	glxFbConfig = bestGlxFbConfigs;

	XFree(glxFbConfigs);
	glxFbConfigs = NULL;

	visualInfo = glXGetVisualFromFBConfig(display, bestGlxFbConfigs);
	fprintf(gpFile," Visual id of best visual info is 0X%lu \n",visualInfo -> visualid);


	// step 4 : Get XVisualInfo by using glXChooseVisual & do error checking
	//visualInfo=glXChooseVisual(display,defaultScreen,frameBufferAttributes);
	
	/*if (visualInfo==NULL)
	{
		printf("ERROR:glXChooseVisual() Failed \n");
		uninitialize();
		exit(1);
	}*/
	
	// step 5 : Fill/initialize structure XSetWindowAttributes & along with that also state Colormap & EventMask
	memset(&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel=0;
	windowAttributes.background_pixel=XBlackPixel(display,defaultScreen);
	windowAttributes.background_pixmap=0;
	windowAttributes.colormap=XCreateColormap(display,
						   RootWindow(display,visualInfo->screen),
						   visualInfo->visual,
						   AllocNone);
	windowAttributes.event_mask=ExposureMask|KeyPressMask|StructureNotifyMask|FocusChangeMask;
	
	// step 6 : initialize grobal color map by using color map from window attributes
	colorMap=windowAttributes.colormap;
	
	// step 7 : Initialize window style called as styleMask (Style cha mukhvata lavne)
	styleMask=CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;
	
	// step 8 : Create window using XCreateWindow using XlibAPI & do error checking
	window=XCreateWindow(display,
			     RootWindow(display,visualInfo->screen),
			     100,
			     100,
			     WIN_WIDTH,
			     WIN_HEIGHT,
			     0,
			     visualInfo->depth,
			     InputOutput,
			     visualInfo->visual,
			     styleMask,
			     &windowAttributes);
	if(!window)
	{
		printf("ERROR:XCreateWindow() Failed \n");
		uninitialize();
		exit(1);
	}
			     
	// step 9 : Give name to your window in it's title / caption bar
	XStoreName(display,window,"OpenGL Window - Mandar Maske RTR2021-116");
	
	// step 10 : Prepare our window to respond to :
	// a : Closing window by clicking  by close button
	// b : Closing window by close option  in system menu by creating & setting WindowManager protocol atom  

	wm_delete_window_atom=XInternAtom(display,"WM_DELETE_WINDOW",True);   // here WM:WindowManager
	
	XSetWMProtocols(display,window,&wm_delete_window_atom,1);
	
	// step 11 : Show the window by XMapWindow()
	XMapWindow(display,window);

	// Centering of window
	screenWidth=XWidthOfScreen(XScreenOfDisplay(display,defaultScreen));
	screenHeight=XHeightOfScreen(XScreenOfDisplay(display,defaultScreen));
	XMoveWindow(display,window,screenWidth/2-WIN_WIDTH/2,screenHeight/2-WIN_HEIGHT/2);
	
	initialize();

	// step 12 : Create a message loop by :
	// a : Getting next event XNextEvent()
	// b : Handling  keypress by Escape key
	// c : By handling message 33
	// message loop
	while (bDone==False)
	{
		while(XPending(display)) // Xpendind mhanje windows madle peek msg
		{
			// a : Getting next event XNextEvent()
			XNextEvent(display,&event);
		
			switch(event.type)
			{
				case MapNotify: // it is similar like WM_CREATE
					break;

				case FocusIn: // set focus
					bActiveWindow = True;
					break;

				case FocusOut: // kill focus
					bActiveWindow = False;
					break;
				
				case KeyPress:
					keysym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);
				
					switch(keysym)
					{
						// b : Handling  keypress by Escape key
						case XK_Escape:
							bDone = True;
							break;
					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					
					switch(keys[0])
					{
						case 'F':
						case 'f':
							if(fullscreen==False)
							{
								toggleFullscreen();
								fullscreen=True;
							}
							else
							{
								toggleFullscreen();
								fullscreen=False;
							}
						break;

						case 'G':
							gFlag = False;
						break;

						case 'g':
							gFlag = True;
						break;

						case 'T':
							tFlag = False;
						break;

						case 't':
							tFlag = True;
						break;

						case 'S':
							sFlag = False;
						break;

						case 's':
							sFlag = True;
						break;

						case 'C':
							cFlag = False;
						break;
		
						case 'c':
							cFlag = True;
						break;
					}
					break;

				case ConfigureNotify: // it is similar like WM_Size:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
						
				// c : By handling message 33
				case 33: // all close function working here // it is analogus to wm_delete_window_atom
					bDone = True;
					break;	
			}				
		}
		if (bActiveWindow == True)
		{
			update();

			draw();
		}
	}
			
	// step 13 : After closing message loop call uninitialize() & return()	     
	uninitialize();
	
	return(0);
}

void toggleFullscreen(void)
{
	// local veriables
	Atom wm_current_state_atom;
	Atom wm_fullscreen_state_atom;
	XEvent event;

	// code
	wm_current_state_atom=XInternAtom(display,"_NET_WM_STATE",False);
	wm_fullscreen_state_atom=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);

	memset(&event,0,sizeof(XEvent));
	event.type=ClientMessage;
	event.xclient.window=window;
	event.xclient.message_type=wm_current_state_atom;
	event.xclient.format=32;
	event.xclient.data.l[0]=fullscreen?0:1;
	event.xclient.data.l[1]=wm_fullscreen_state_atom;

	XSendEvent(display,
			   RootWindow(display,visualInfo->screen),
			   False,
			   SubstructureNotifyMask,
			   &event);	//similar like sendmessage & Postmessage 
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	void uninitialize(void);
	void printGLInfo(void);
	void circleArray(float radius_x, float radius_y, float start, float end);
	
	//code
	//glxContext = glXCreateContext(display,visualInfo,NULL,True);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARPProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL)
	{
		fprintf(gpFile, "glXCreateContextAttribesARP Failed\n\n");
		uninitialize();
		exit(1);
	}

	GLint contextAttributes[]=
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,6,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	//glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
	if(!glxContext)
	{
		GLint contextAttributes[]=
		{
		GLX_CONTEXT_MAJOR_VERSION_ARB,1,
		GLX_CONTEXT_MINOR_VERSION_ARB,0,
		None
		};
		glxContext=glXCreateContextAttribsARB(display,glxFbConfig,NULL,True,contextAttributes);
		fprintf(gpFile, "Can not Support 4.6 hence falling back to default version\n\n");
	}
	else
	{
		fprintf(gpFile, "Found the support to 4.6 version \n\n");
	}

	//Cheking S/w or H/w 
	if(!glXIsDirect(display,glxContext))
	{
		fprintf(gpFile, "Direct(h/w) Rendering is not Supported \n\n");
	}
	else
	{
		fprintf(gpFile, "Direct(h/w) Rendering is Supported \n\n");
	}

	// make current context
	glXMakeCurrent(display, window, glxContext);
	//Glew Initialization
	if (glewInit() != GLEW_OK)
	{
		return -5;
	}

	// print OpenGLInfo
	printGLInfo();

	// StepC 1 : writing shading code
	// vertex shader 
	// vertex shader 
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_position;" \
		"in vec4 a_color;" \
		"uniform mat4 u_mvpMatrix;" \
		"out vec4 a_color_out;" \
		"void main(void)" \
		"{" \
			"gl_Position = u_mvpMatrix * a_position;" \
			"a_color_out = a_color;" \
		"}";


	// StepC 2 : creating shading object
	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

	// stepC 4 : compile the shader
	glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	GLint status;
	GLint infoLogLength;
	char* log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);  
		
		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
				
				// stepC 5 e : display the log
				fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepc 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// StepC 1 : writing shading code
	// fragment shader 
	const GLchar* fragmentShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 a_color_out;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
			"FragColor = a_color_out;" \
		"}";

	// StepC 2 : creating shading object
	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// stepC 3 : giving shader code to shader object 
	glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
	
	// stepC 4 : compile the shader
	glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

	// stepC 5 : error checking of shader compilation
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	// stepC 5 a : getting compilation status
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

	// stepC 5 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepC 5 d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

				// stepC 5 e : display the log
				fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

				// stepC 5 f : free the allocated buffer
				free(log);

				// stepC 5 g : exit the application due to error
				uninitialize();
			}
		}
	}

	// Shader program object
	// stepD 1 : create shader program object
	shaderProgramObject = glCreateProgram();

	// stepD 2 : attach desired shaders to this shader program  object
	glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	// stepD 3 : link shader program object
	glLinkProgram(shaderProgramObject);

	//stepD 4 : do link error checking with similar to a to g steps like above 
	// reinitialization of this 3 veriables
	status = 0;
	infoLogLength = 0;
	log = NULL;

	//stepD 4 a : getting compilation status
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

	//stepD 4 b : getting length of log of compilation status
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

		// stepD 4c : allocate enough memory to the buffer to hold the compiletation log
		if (infoLogLength > 0)
		{
			log = (char*)malloc(infoLogLength);

			// stepD 4d : get the compilation log into this allocated buffer
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

				// stepD 4e  : display the log
				fprintf(gpFile, "Shader Program link log : %s \n", log);

				// stepD 4f : free the allocated buffer
				free(log);

				// stepD 4g : exit the application due to error
				uninitialize();

			}
		}
	}

	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

	// vao & vbo related code
	//declarations of vertex data arrays
	const GLfloat horizontalLine[] =
	{
		1.0f,0.0f,0.0f,
		-1.0,0.0f,0.0f
	};

	const GLfloat horizontalLineColor[] =
	{
		1.0f, 0.0f, 0.0f, 
		1.0f, 0.0f, 0.0, 
		1.0f, 0.0f, 0.0f 
	};

	const GLfloat verticalLine[] =
	{
		0.0f,1.0f,0.0f,
		0.0,-1.0f,0.0f
	};

	const GLfloat verticalLineColor[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0,
		0.0f, 1.0f, 0.0f
	};

	const GLfloat graphHLines[] =
	{
		-1.0f, 1.0f, 0.0f, // 1
		1.0f, 1.0f, 0.0f,

		-1.0f, 0.95f, 0.0f, // 2
		1.0f, 0.95f, 0.0f,

		-1.0f, 0.90f, 0.0f, // 3
		1.0f, 0.90f, 0.0f,

		-1.0f, 0.85f, 0.0f, // 4
		1.0f, 0.85f, 0.0f,

		-1.0f, 0.80f, 0.0f, // 5
		1.0f, 0.80f, 0.0f,

		-1.0f, 0.75f, 0.0f, // 6
		1.0f, 0.75f, 0.0f,

		-1.0f, 0.70f, 0.0f, // 7
		1.0f, 0.70f, 0.0f,

		-1.0f, 0.65f, 0.0f, // 8
		1.0f, 0.65f, 0.0f,

		-1.0f, 0.60f, 0.0f, // 9
		1.0f, 0.60f, 0.0f,

		-1.0f, 0.55f, 0.0f, // 10
		1.0f, 0.55f, 0.0f,

		-1.0f, 0.50f, 0.0f, // 11
		1.0f, 0.50f, 0.0f,

		-1.0f, 0.45f, 0.0f, // 12
		1.0f, 0.45f, 0.0f,

		-1.0f, 0.40f, 0.0f, // 13
		1.0f, 0.40f, 0.0f,

		-1.0f, 0.35f, 0.0f, // 14
		1.0f, 0.35f, 0.0f,

		-1.0f, 0.30f, 0.0f, // 15
		1.0f, 0.30f, 0.0f,

		-1.0f, 0.25f, 0.0f, // 16
		1.0f, 0.25f, 0.0f,

		-1.0f, 0.20f, 0.0f, // 17
		1.0f, 0.20f, 0.0f,

		-1.0f, 0.15f, 0.0f, // 18
		1.0f, 0.15f, 0.0f,

		-1.0f, 0.10f, 0.0f, // 19
		1.0f, 0.10f, 0.0f,

		-1.0f, 0.05f, 0.0f, // 20
		1.0f, 0.05f, 0.0f,

		-1.0f, -1.0f, 0.0f, // 21
		1.0f, -1.0f, 0.0f,

		-1.0f, -0.95f, 0.0f, // 22
		1.0f, -0.95f, 0.0f,

		-1.0f, -0.90f, 0.0f, // 23
		1.0f, -0.90f, 0.0f,

		-1.0f, -0.85f, 0.0f, // 24
		1.0f, -0.85f, 0.0f,

		-1.0f, -0.80f, 0.0f, // 25
		1.0f, -0.80f, 0.0f,

		-1.0f, -0.75f, 0.0f, // 26
		1.0f, -0.75f, 0.0f,

		-1.0f, -0.70f, 0.0f, // 27
		1.0f, -0.70f, 0.0f,

		-1.0f, -0.65f, 0.0f, // 28
		1.0f, -0.65f, 0.0f,

		-1.0f, -0.60f, 0.0f, // 29
		1.0f, -0.60f, 0.0f,

		-1.0f, -0.55f, 0.0f, // 30
		1.0f, -0.55f, 0.0f,

		-1.0f, -0.50f, 0.0f, // 31
		1.0f, -0.50f, 0.0f,

		-1.0f, -0.45f, 0.0f, // 32
		1.0f, -0.45f, 0.0f,

		-1.0f, -0.40f, 0.0f, // 33
		1.0f, -0.40f, 0.0f,

		-1.0f, -0.35f, 0.0f, // 34
		1.0f, -0.35f, 0.0f,

		-1.0f, -0.30f, 0.0f, // 35
		1.0f, -0.30f, 0.0f,

		-1.0f, -0.25f, 0.0f, // 36
		1.0f, -0.25f, 0.0f,

		-1.0f, -0.20f, 0.0f, // 37
		1.0f, -0.20f, 0.0f,

		-1.0f, -0.15f, 0.0f, // 38
		1.0f, -0.15f, 0.0f,

		-1.0f, -0.10f, 0.0f, // 39
		1.0f, -0.10f, 0.0f,

		-1.0f, -0.05f, 0.0f, // 40
		1.0f, -0.05f, 0.0f,
	};

	const GLfloat graphVLines[] =
	{
		1.0f, 1.0f, 0.0f, // 1
		1.0f, -1.0f, 0.0f,

		0.95f, 1.0f, 0.0f, // 2
		0.95f, -1.0f, 0.0f,

		0.90f, 1.0f, 0.0f, // 3
		0.90f, -1.0f, 0.0f,

		0.85f, 1.0f, 0.0f, // 4
		0.85f, -1.0f, 0.0f,

		0.80f, 1.0f, 0.0f, // 5
		0.80f, -1.0f, 0.0f,

		0.75f, 1.0f, 0.0f, // 6
		0.75f, -1.0f, 0.0f,

		0.70f, 1.0f, 0.0f, // 7
		0.70f, -1.0f, 0.0f,

		0.65f, 1.0f,  0.0f, // 8
		0.65f, -1.0f, 0.0f,

		0.60f, 1.0f, 0.0f, // 9
		0.60f, -1.0f, 0.0f,

		0.55f, 1.0f, 0.0f, // 10
		0.55f, -1.0f, 0.0f,

		0.50f, 1.0f, 0.0f, // 11
		0.50f, -1.0f, 0.0f,

		0.45f, 1.0f, 0.0f, // 12
		0.45f, -1.0f, 0.0f,

		0.40f, 1.0f, 0.0f, // 13
		0.40f, -1.0f, 0.0f,

		0.35f, 1.0f, 0.0f, // 14
		0.35f, -1.0f, 0.0f,

		0.30f, 1.0f, 0.0f, // 15
		0.30f, -1.0f, 0.0f,

		0.25f, 1.0f, 0.0f, // 16
		0.25f, -1.0f, 0.0f,

		0.20f, 1.0f, 0.0f, // 17
		0.20f, -1.0f, 0.0f,

		0.15f, 1.0f, 0.0f, // 18
		0.15f, -1.0f, 0.0f,

		0.10f, 1.0f, 0.0f, // 19
		0.10f, -1.0f, 0.0f,

		0.05f, 1.0f, 0.0f, // 20
		0.05f, -1.0f, 0.0f,

		-1.0f, 1.0f, 0.0f, // 21
		-1.0f, -1.0f, 0.0f,

		-0.95f, 1.0f, 0.0f, // 22
		-0.95f, -1.0f, 0.0f,

		-0.90f, 1.0f, 0.0f, // 23
		-0.90f, -1.0f, 0.0f,

		-0.85f, 1.0f, 0.0f, // 24
		-0.85f, -1.0f, 0.0f,

		-0.80f, 1.0f, 0.0f, // 25
		-0.80f, -1.0f, 0.0f,

		-0.75f, 1.0f, 0.0f, // 26
		-0.75f, -1.0f, 0.0f,

		-0.70f, 1.0f, 0.0f, // 27
		-0.70f, -1.0f, 0.0f,

		-0.65f, 1.0f, 0.0f, // 28
		-0.65f, -1.0f, 0.0f,

		-0.60f, 1.0f, 0.0f, // 29
		-0.60f, -1.0f, 0.0f,

		-0.55f, 1.0f, 0.0f, // 30
		-0.55f, -1.0f, 0.0f,

		-0.50f, 1.0f, 0.0f, // 31
		-0.50f, -1.0f, 0.0f,

		-0.45f, 1.0f, 0.0f, // 32
		-0.45f, -1.0f, 0.0f,

		-0.40f, 1.0f, 0.0f, // 33
		-0.40f, -1.0f, 0.0f,

		-0.35f, 1.0f, 0.0f, // 34
		-0.35f, -1.0f, 0.0f,

		-0.30f, 1.0f, 0.0f, // 35
		-0.30f, -1.0f, 0.0f,

		-0.25f, 1.0f, 0.0f, // 36
		-0.25f, -1.0f, 0.0f,

		-0.20f, 1.0f, 0.0f, // 37
		-0.20f, -1.0f, 0.0f,

		-0.15f, 1.0f, 0.0f, // 38
		-0.15f, -1.0f, 0.0f,

		-0.10f, 1.0f, 0.0f, // 39
		-0.10f, -1.0f, 0.0f,

		-0.05f, 1.0f, 0.0f, // 40
		-0.05f, -1.0f, 0.0f
	};

	const GLfloat graphHVLinesColor[] =
	{
		0.0f, 0.0f, 1.0f, // 1
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 2
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 3
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 4
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 5
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 6
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 7
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 8
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 9
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 10
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 11
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 12
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 13
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 14
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 15
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 16
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 17
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 18
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 19
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 20
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 21
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 22
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 23
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 24
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 25
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 26
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 27
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 28
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 29
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 30
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 31
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 32
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 33
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 34
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 35
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 36
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 37
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 38
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 39
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, 1.0f, // 40
		0.0f, 0.0f, 1.0f,
	};

	// Horizontal Line
	// vao related code
	glGenVertexArrays(1, &vao_horizontalLine); //vao:- vertex array object
	glBindVertexArray(vao_horizontalLine);
	
	// vbo for position
	glGenBuffers(1, &vbo_horizontalLine_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_horizontalLine_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLine), horizontalLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	// vbo for color
	glGenBuffers(1, &vbo_horizontalLine_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_horizontalLine_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLineColor), horizontalLineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

	glBindVertexArray(0);

	// Vertical Line
	// vao related code
	glGenVertexArrays(1, &vao_verticalLine); //vao:- vertex array object
	glBindVertexArray(vao_verticalLine);

	// vbo for position
	glGenBuffers(1, &vbo_verticalLine_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_verticalLine_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLine), verticalLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	// vbo for color
	glGenBuffers(1, &vbo_verticalLine_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_verticalLine_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLineColor), verticalLineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

	glBindVertexArray(0);


	// Graph Horizontal Lines
	// vao related code
	glGenVertexArrays(1, &vao_graphHLines); //vao:- vertex array object
	glBindVertexArray(vao_graphHLines);

	// vbo for position
	glGenBuffers(1, &vbo_graphHLines_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(graphHLines), graphHLines, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	// vbo for color
	glGenBuffers(1, &vbo_graphHLines_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphHVLinesColor), graphHVLinesColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

	glBindVertexArray(0);

	// Graph Vertical Lines
	// vao related code
	glGenVertexArrays(1, &vao_graphVLines); //vao:- vertex array object
	glBindVertexArray(vao_graphVLines);

	// vbo for position
	glGenBuffers(1, &vbo_graphVLines_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_graphVLines_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(graphVLines), graphVLines, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	// vbo for color
	glGenBuffers(1, &vbo_graphHLines_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(graphHVLinesColor), graphHVLinesColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

	glBindVertexArray(0);

	height = ((sqrt(3)) * (base1 / 2));
	base = base / 2;
	height = height / 2;
	float median = (base1 / sqrt(3)) / -4;

	const GLfloat triangleLine[] =
	{
		0.0f, (height - median), 0.0f,
		-base, -height - median, 0.0f,
		-base, -height - median, 0.0f,
		base, -height - median, 0.0f,
		base, -height - median, 0.0f,
		0.0f, (height - median), 0.0f
	};

	const GLfloat triangleLineColor[] =
	{
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f
	};

	// Triangle Line
	// vao related code
	glGenVertexArrays(1, &vao_triangleLines); //vao:- vertex array object
	glBindVertexArray(vao_triangleLines);

	// vbo for position
	glGenBuffers(1, &vbo_triangleLines_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_triangleLines_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleLine), triangleLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	// vbo for color
	glGenBuffers(1, &vbo_triangleLines_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_triangleLines_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleLineColor), triangleLineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

	glBindVertexArray(0);

	const GLfloat squareLine[] =
	{
		float(((base1 / sqrt(3)) * 2) / 2), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
		float(-(((base1 / sqrt(3)) * 2) / 2)), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,

		float(-(((base1 / sqrt(3)) * 2) / 2)), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
		float(-(((base1 / sqrt(3)) * 2) / 2)), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,

		float(-(((base1 / sqrt(3)) * 2) / 2)), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,
		float(((base1 / sqrt(3)) * 2) / 2), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,

		float(((base1 / sqrt(3)) * 2) / 2), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,
		float(((base1 / sqrt(3)) * 2) / 2), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
	};

	const GLfloat squareLineColor[] =
	{
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
	};

	// Square Line
	// vao related code
	glGenVertexArrays(1, &vao_squareLines); //vao:- vertex array object
	glBindVertexArray(vao_squareLines);

	// vbo for position
	glGenBuffers(1, &vbo_squareLines_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_squareLines_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(squareLine), squareLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	// vbo for color
	glGenBuffers(1, &vbo_squareLines_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_squareLines_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(squareLineColor), squareLineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

	glBindVertexArray(0);

	circleArray((base1 / sqrt(3)), (base1 / sqrt(3)), 0.0f, 0.0f);

	const GLfloat circleLineColor[] =
	{
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
	};

	// Square Line
	// vao related code
	glGenVertexArrays(1, &vao_circleLines); //vao:- vertex array object
	glBindVertexArray(vao_circleLines);

	// vbo for position
	glGenBuffers(1, &vbo_circleLines_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_circleLines_position); //bind buffer

	glBufferData(GL_ARRAY_BUFFER, sizeof(circleLine), circleLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data) 

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
	//glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

	// vbo for color
	glGenBuffers(1, &vbo_circleLines_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_circleLines_color); //bind buffer for color
	glBufferData(GL_ARRAY_BUFFER, sizeof(circleLineColor), circleLineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

	glBindVertexArray(0);

	// here start OpenGL code 
	//clear the screen using blue color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//depth related changes
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void printGLInfo(void)
{
	// local veriable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(gpFile, "Number Of Supported Extensions : %d \n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, " %s \n", glGetStringi(GL_EXTENSIONS,i));
	}
}

void circleArray(float radius_x, float radius_y, float start, float end)
{
	for (int i = 0; i <= 100; i++)
	{
		float angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		circleLine[i][0] = cos(angle) * radius_x;
		circleLine[i][1] = sin(angle) * radius_y;
		circleLine[i][2] = 0.0f;
	}
}

void resize(int width,int height)
{
	// code
	if (height == 0)
		height= 1; // to avoid divided by 0 illegal instruction for feature code

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void draw(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// stepE 1 :  use the shader program object
	glUseProgram(shaderProgramObject);

	// "Triangle"
	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -2.5f); // or only translate - glTranslate is replaced by this line
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	if (gFlag == True)
	{
		glBindVertexArray(vao_graphHLines);

		// stepE 2 : draw the desired graphics/animation
		// here will be magic code
		glLineWidth(3);
		glDrawArrays(GL_LINES, 0, 80); 

		glBindVertexArray(0);

		glBindVertexArray(vao_graphVLines);

		// stepE 2 : draw the desired graphics/animation
		// here will be magic code
		glLineWidth(3);
		glDrawArrays(GL_LINES, 0, 80);

		glBindVertexArray(0);

		glBindVertexArray(vao_horizontalLine);

		// stepE 2 : draw the desired graphics/animation
		// here will be magic code
		glLineWidth(3);
		glDrawArrays(GL_LINES, 0, 2);

		glBindVertexArray(0);

		glBindVertexArray(vao_verticalLine);

		// stepE 2 : draw the desired graphics/animation
		// here will be magic code
		glLineWidth(3);
		glDrawArrays(GL_LINES, 0, 2);

		glBindVertexArray(0);
	}
	
	if (tFlag == True && gFlag == True)
	{
		glBindVertexArray(vao_triangleLines);

		// stepE 2 : draw the desired graphics/animation
		// here will be magic code
		glLineWidth(3);
		glDrawArrays(GL_LINES, 0, 6);

		glBindVertexArray(0);
	}

	if (sFlag == True && gFlag == True)
	{
		glBindVertexArray(vao_squareLines);

		// stepE 2 : draw the desired graphics/animation
		// here will be magic code
		glLineWidth(3);
		glDrawArrays(GL_LINES, 0, 8);

		glBindVertexArray(0);
	}

	if (cFlag == True && gFlag == True)
	{
		glBindVertexArray(vao_circleLines);

		glDrawArrays(GL_LINE_LOOP, 0, 100);

		glBindVertexArray(0);
	}

	// stepE 3 : unuse the shader program object
	
	glUseProgram(0);

	
	glXSwapBuffers(display,window);
}

void update(void)
{
	// code
	
}

void uninitialize(void)
{
	// function declarations
	void toggleFullScreen(void);
		
	// code
	GLXContext currentContext;
	currentContext=glXGetCurrentContext();

	// deletion/ uninitialization of vbo
	if (vbo_verticalLine_position)
	{
		glDeleteBuffers(1, &vbo_verticalLine_position);
		vbo_verticalLine_position = 0;
	}

	// deletion/ uninitialization of vao
	if (vao_verticalLine)
	{
		glDeleteVertexArrays(1, &vao_verticalLine);
		vao_verticalLine = 0;
	}

	// deletion/ uninitialization of vbo triangle
	if (vbo_horizontalLine_position)
	{
		glDeleteBuffers(1, &vbo_horizontalLine_position);
		vbo_horizontalLine_position = 0;
	}

	// deletion/ uninitialization of vao
	if (vao_horizontalLine)
	{
		glDeleteVertexArrays(1, &vao_horizontalLine);
		vao_horizontalLine = 0;
	}

	// shader uninitialization
	if (shaderProgramObject)
	{
		// stepF 0
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		
		// stepF 1 : get the no. of attached shaders
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

		GLuint* shaderObjects = NULL;

		// stepF 2 : create empty buffer to hold array of  attached shader objects
		shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

		// stepF 3 : allocate enough memory to hold array of attached shader objects
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

		// stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}

		// stepF 5 : free the memory allocated for the buffer
		free(shaderObjects);

		// stepF 6 : unuse the shader program object
		glUseProgram(0);

		// stepF 7 : delete the shader program object
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	if(currentContext && currentContext == glxContext)
	{
		glXMakeCurrent(display,0,0);
	}
	
	if(glxContext)
	{
		glXDestroyContext(display,glxContext);
		glxContext = NULL;
	}

	if(visualInfo)
	{
		free(visualInfo);
		visualInfo = NULL;
	}

	if (fullscreen)
	{
		toggleFullscreen();
		fullscreen = False;
	}

	if(window)
	{
		XDestroyWindow(display,window);
	}
	
	if(colorMap)
	{
		XFreeColormap(display,colorMap);
	}
	
	//thos we uninitialize display & colormap we don't uninitialize visualinfo because we cant create visual info we just matched/obtained visual info
	
	if(display)
	{
		XCloseDisplay(display);
		display=NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); 
		gpFile = NULL; 
	}
	
}

 

// Compile Command :- gcc -o OGL OGL.c -lX11 -lGL -lGLU -lGLEW
// Run Command :- ./OGL


