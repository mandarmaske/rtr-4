//
//  Window.m
//
//
//  Created by user226430 on 12/11/22.
//

// hrader files
#import<Foundation/Foundation.h> // same like<stdio.h>
#import<Cocoa/Cocoa.h> // same like<window.h>

#import<QuartzCore/CVDisplayLink.h> // ya varti synk/ vertexrect avalabun aahe
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>

#import "vmath.h"
using namespace vmath;

#import"Sphere.h"
// global function declaration
CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*);//CVDisplayLinkRef* pn mhanu shakto

// global vderiable declaration
FILE* gpFile = NULL;

//interface declaration/class declaration
@interface AppDelegate : NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// entry point function
int main(int argc, char*argv[])
{
    // code
    // creation of object
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init]; // object tayar honar aahe

    // create application object for this application
    NSApp = [NSApplication sharedApplication];

    // create app deligate object
    AppDelegate *appDelegate = [[AppDelegate alloc]init];

    // give our AppDeligate to NsApp
    [NSApp setDelegate : appDelegate];

    // start gameloop or(runloop)
    [NSApp run];

    // tail outorelease pool to releaseall objects created by this application
    [pool release];

    return(0);
}

// implementation of AppDelegate
@implementation AppDelegate
{
    @private
    NSWindow *window;
    GLView *view;
}

- (void) applicationDidFinishLaunching : (NSNotification *)notification //sililar like WM_Create
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
    NSString *appDirPath = [appBundle bundlePath]; // Window dir cha path deto
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // 04-Log file cha path
    NSString *logFileNameWithPath = [NSString stringWithFormat : @"%@/log.txt", parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath UTF8String];
    // const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if (gpFile == NULL)
    {
        [self release];
        [NSApp terminate : self];
    }
    
    fprintf(gpFile, "Program Is Started Successfully\n");
    

    NSRect rect = NSMakeRect(0.0, 0.0, 800.0, 600.0); //NSRect :- CGRect (CG:CoreGraphics)

    window = [[NSWindow alloc]initWithContentRect : rect
                                        styleMask : NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                          backing : NSBackingStoreBuffered
                                            defer : NO
             ];

    [window setTitle : @"macOS Window : MDM"];
    NSColor *backgroundColor = [NSColor blackColor];
    [window setBackgroundColor : backgroundColor];

    [window center];

    view = [[GLView alloc] initWithFrame : rect];
    [window setContentView : view];
    
    // set window delegate to this object
    [window setDelegate : self];

    [window makeKeyAndOrderFront : self]; // similar like overlapped window
}

- (void) applicationWillTerminate : (NSNotification *)notification //sililar like WM_Destroy
{
    // code
   /* if (gpFile)
    {
        fprintf(gpFile, "Program Is Terminated Successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }*/

}

- (void) windowWillClose : (NSNotification *)notification // sililar like WM_Close
{
    // code
    [NSApp terminate : self];
}

- (void) dealloc
{
    // code

    if(view)
    {
        [view release];
        view = nil;
    }

    if(window)
    {
        [window release];
        window = nil; // null : nil
    }
   
    [super dealloc];
}

@end

// implementing GLView
@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;

    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_elements;
    
    SphereData sphereData;

    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionMatrixUniform;

    GLuint laUniform; // light ambient
    GLuint ldUniform; // light diffuse
    GLuint lsUniform; // light specular
    GLuint lightPositionUniform;

    GLuint kaUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint materialShininessUniform;

    GLuint lightingEnabledUniform;

    BOOL bLight;
    
    GLfloat lightAmbient[4];
    GLfloat lightDiffuse[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;

    GLfloat gbLightAngleX;
    GLfloat gbLightAngleY;
    GLfloat gbLightAngleZ;

    GLint keyPressed;

    int WIN_WIDTH;
    int WIN_HEIGHT;
    int FULL_WIN_WIDTH;
    int FULL_WIN_HEIGHT;
    
    BOOL gbFullScreen;
    int counter;

    mat4 perspectiveProjectionMatrix;

}

- (id) initWithFrame : (NSRect) frame // frame or rect
{
    // code
    self = [super initWithFrame : frame];
    
    if(self)
    {
        // code
        // step1 : initialize the array of opengl pixel format attributes
        NSOpenGLPixelFormatAttribute openGLPixelFormatAttributes[] =
                                                                    {
                                                                        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core, // OpenGL 1st version je lock kele aahe
                                                                        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                                                                        NSOpenGLPFAColorSize, 24,
                                                                        NSOpenGLPFADepthSize, 24,
                                                                        NSOpenGLPFAAlphaSize, 8,
                                                                        NSOpenGLPFANoRecovery,
                                                                        NSOpenGLPFAAccelerated, // mala h/w rendering pahije (Accelerated :mhanje h/w rendering)
                                                                        NSOpenGLPFADoubleBuffer, // don't fall back(h/w rendering bhetat nasel tr s/w rendering la jau nako fail ho) no recovery
                                                                        0
                                                                    };
       
        // step2 : create opengl pixel format using above attributes
        NSOpenGLPixelFormat *glPixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes : openGLPixelFormatAttributes]autorelease];
       
        if(glPixelFormat == nil)
        {
            fprintf(gpFile,"faild to create glPixelFormat\n");
            [self uninitialize];
            [self release];
            [NSApp terminate : self];
        }
        // step3 : create opengl context using above pixel format
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat : glPixelFormat shareContext : nil]autorelease];

        if(glContext == nil)
        {
            fprintf(gpFile,"faild to create glContext\n");
            [self uninitialize];
            [self release];
            [NSApp terminate : self];
        }
        // step4 : set this view's pixel format by using above pixel format
        [self setPixelFormat : glPixelFormat];

        // step5 : set view's opengl context by using above opengl context
        [self setOpenGLContext : glContext];

    }
    return(self);
}

// define getframe for time custom method with predefined signature
- (CVReturn) getFrameForTime : (const CVTimeStamp*) outputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];

    [pool release]; // he perthread aaste

    return(kCVReturnSuccess); // kCVReturnSuccess : core vedio cha horizontal blank aahe
    
}

- (void) prepareOpenGL // overridable given by NSOpenGLView
{
    // code
    [super prepareOpenGL]; // tyanchyach method la call karne

    // make OpenGL context current
    [[self openGLContext]makeCurrentContext];

    // set doublebuffer swapping interval into one
    GLint swapInterval = 1;
    [[self openGLContext]setValues : &swapInterval forParameter : NSOpenGLCPSwapInterval]; // cp : context parameter

    //log OpenGLLog;

    fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    // call our initialize
    [self initialize];

    // create,configar,start display link

    // step 1 : create display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    
    // step 2 : set the call back with display link
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallBack, self);
   
    // step 3 : convert NSOpenGLPixel format to CGLPixel format
    CGLPixelFormatObj cglpixelformat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

    // step 4 : convert NSOpenGLcontext format to CGL context
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];

    // step 5 : using above info set current cgdisplay to cglpixelformat and context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglpixelformat);

    // step 6 : start display link
    CVDisplayLinkStart(displayLink);
}

// implementation of overridden reshape

- (void) reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect = [self bounds];
    
    int width = rect.size.width;
    int height = rect.size.height;
    
    //call out resize
    [self resize : width : height];
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}


- (void) drawRect : (NSRect) dirtyRect // apple recommended the name dirtyRect
{
    // code

    // to prevent flickring
    [self drawView]; // hi adjustment miorix nintr keli sirani
    
}

-(void) drawView
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
   
    // call our display here
    [self display];

    [self myupdate];

    // do double buffering
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(int) initialize
{
    // code
    // vertex shader
    const GLchar* vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 a_position;" \
    "in vec3 a_normal;" \
    "uniform mat4 u_modelMatrix;" \
    "uniform mat4 u_viewMatrix;" \
    "uniform mat4 u_projectionMatrix;" \
    "uniform vec4 u_lightPosition;" \
    "uniform int u_lightingEnabled;" \
    "out vec3 transformedNormals;" \
    "out vec3 lightDirection;" \
    "out vec3 viewerVector;" \
    "void main(void)" \
    "{" \
        "if(u_lightingEnabled==1)" \
        "{" \
            "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
            "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
            "transformedNormals = normalMatrix * a_normal;" \
            "lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" \
            "viewerVector = -eyeCoordinates.xyz;" \
        "}" \
    "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
    "}";

    // StepC 2 : creating shading object
    GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // stepC 3 : giving shader code to shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

    // stepC 4 : compile the shader
    glCompileShader(vertexShaderObject);

    // stepC 5 : error checking of shader compilation
    GLint status;
    GLint infoLogLength;
    char* log = NULL;

    // stepC 5 a : getting compilation status
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

    // stepC 5 b : getting length of log of compilation status
    if (status == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
        // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
        if (infoLogLength > 0)
        {
            log = (char*)malloc(infoLogLength);

            // stepC 5 d : get the compilation log into this allocated buffer
            if (log != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                // stepC 5 e : display the log
                fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

                // stepC 5 f : free the allocated buffer
                free(log);

                // stepc 5 g : exit the application due to error
                [self uninitialize];
                [self release];
                [NSApp terminate : self];
            }
        }
    }

    // StepC 1 : writing shading code
    // fragment shader
    const GLchar* fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec3 transformedNormals;" \
    "in vec3 lightDirection;" \
    "in vec3 viewerVector;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_materialShininess;" \
    "uniform int u_lightingEnabled;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
        "vec3 phong_ads_light;" \
        "if(u_lightingEnabled==1)" \
        "{" \
            "vec3 ambient = u_la * u_ka;" \
            "vec3 normalized_transformedNormals = normalize(transformedNormals);" \
            "vec3 normalized_lightDirection = normalize(lightDirection);" \
            "vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" \
            "vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" \
            "vec3 normalized_viewerVector = normalize(viewerVector);" \
            "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" \
            "phong_ads_light = ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
            "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_light,1.0);" \
    "}";

    // StepC 2 : creating shading object
    GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // stepC 3 : giving shader code to shader object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
    // stepC 4 : compile the shader
    glCompileShader(fragmentShaderObject);

    // stepC 5 : error checking of shader compilation
    // reinitialization of this 3 veriables
    status = 0;
    infoLogLength = 0;
    log = NULL;

    // stepC 5 a : getting compilation status
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

    // stepC 5 b : getting length of log of compilation status
    if (status == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

        // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
        if (infoLogLength > 0)
        {
            log = (char*)malloc(infoLogLength);

            // stepC 5 d : get the compilation log into this allocated buffer
            if (log != NULL)
           {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                // stepC 5 e : display the log
                fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

                // stepC 5 f : free the allocated buffer
                free(log);

                // stepC 5 g : exit the application due to error
                [self uninitialize];
                [self release];
                [NSApp terminate : self];
            }
        }
    }

    // Shader program object
    // stepD 1 : create shader program object
    shaderProgramObject = glCreateProgram();

    // stepD 2 : attach desired shaders to this shader program  object
    glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
    glAttachShader(shaderProgramObject, fragmentShaderObject);
        
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");

    // stepD 3 : link shader program object
    glLinkProgram(shaderProgramObject);

    //stepD 4 : do link error checking with similar to a to g steps like above
    status = 0;
    infoLogLength = 0;
    log = NULL;

    //stepD 4 a : getting compilation status
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

    //stepD 4 b : getting length of log of compilation status
    if (status == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

        // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
        if (infoLogLength > 0)
        {
            log = (char*)malloc(infoLogLength);

            // stepD 4d : get the compilation log into this allocated buffer
            if (log != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                // stepD 4e  : display the log
                fprintf(gpFile, "Shader Program link log : %s \n", log);

                // stepD 4f : free the allocated buffer
                free(log);

                // stepD 4g : exit the application due to error
                [self uninitialize];
                [self release];
                [NSApp terminate : self];

            }
        }
    }

    modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
    viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
    projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

    // light uniform
    laUniform = glGetUniformLocation(shaderProgramObject, "u_la");
    ldUniform = glGetUniformLocation(shaderProgramObject, "u_ld");
    lsUniform = glGetUniformLocation(shaderProgramObject, "u_ls");
    lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_lightPosition");

    // material uniform
    kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
    kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
    ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
    materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");
    
    lightingEnabledUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

    // vao & vbo related code
    //declarations of vertex data arrays
    Sphere *sphere = new Sphere();
    sphere->getSphereVertexData(&sphereData);

    delete sphere;
    sphere = NULL;

    // vao & vbo related code
    //declarations of vertex data arrays

    // vao
    // vao
    glGenVertexArrays(1, &vao_sphere);
    glBindVertexArray(vao_sphere);

    // position vbo
    glGenBuffers(1, &vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // normal vbo
    glGenBuffers(1, &vbo_sphere_normal);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.normals, GL_STATIC_DRAW);

    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // element vbo
    glGenBuffers(1, &vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * 2280, sphereData.elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    
    // depth Realted Changes
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    perspectiveProjectionMatrix = mat4::identity();

    lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 1.0f;

    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    
    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;
    
    
    keyPressed = 0;

    gbLightAngleX = 0.0f;
    gbLightAngleY = 0.0f;
    gbLightAngleZ = 0.0f;
    
    bLight = NO;
   // gbFullScreen = NO;
    counter = 0;
    
    return(0);
}

-(void) resize : (int)width : (int) height
{
    // code
    if (height == 0)
        height= 1;

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

-(void) display
{
    // code
    if (counter == 1)
    {
        WIN_WIDTH = 1920;
        WIN_HEIGHT = 1080;
        FULL_WIN_WIDTH = 800;
        FULL_WIN_HEIGHT = 600;
    }
    else
    {
        WIN_WIDTH = 800;
        WIN_HEIGHT = 600;
        FULL_WIN_WIDTH = 1920;
        FULL_WIN_HEIGHT = 1080;
    }
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // stepE 1 :  use the shader program object
    glUseProgram(shaderProgramObject);

    // transformations
    mat4 translationMatrix = mat4::identity();
    mat4 modelMatrix = mat4::identity();
    mat4 viewMatrix = mat4::identity();
    mat4 projectionViewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //projectionViewMatrix = perspectiveProjectionMatrix * modelMatrix;
    
    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        glUniform4fv(lightPositionUniform, 1, lightPosition);

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);

        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }

    glBindVertexArray(vao_sphere);

    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        //glUniform4fv(lightPositionUniform, 1, lightPosition);

        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX), cos(gbLightAngleX), 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY),  0.0f, cos(gbLightAngleY), 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ), cos(gbLightAngleZ), 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    [self drawTwentyFourSpheres];

    glBindVertexArray(0);


    glBindVertexArray(0);
    
    // stepE 3 : unuse the shader program object
    
    glUseProgram(0);

}

-(void) myupdate
{
    // code
    if (keyPressed == 1)
    {
        gbLightAngleX = gbLightAngleX + 0.05f;
        if (gbLightAngleX >= 360.0f)
            gbLightAngleX = gbLightAngleX - 360.0f;
    }

    else if (keyPressed == 2)
    {
        gbLightAngleY = gbLightAngleY + 0.05f;
        if (gbLightAngleY >= 360.0f)
            gbLightAngleY = gbLightAngleY - (360.0f);
    }

    else if (keyPressed == 3)
    {
        gbLightAngleZ = gbLightAngleZ + 0.05f;
        if (gbLightAngleZ >= 360.0f)
            gbLightAngleZ = gbLightAngleZ - 360.0f;
    }
    
}

- (void) drawTwentyFourSpheres
{
    // code

    // ***** 1st sphere on 1st column, emerald *****
    // ambient material
    materialAmbient[0] = 0.0215f; // r
    materialAmbient[1] = 0.1745f; // g
    materialAmbient[2] = 0.0215f; // b
    materialAmbient[3] = 1.0f;   // a

    // diffuse material
    materialDiffuse[0] = 0.07568f; // r
    materialDiffuse[1] = 0.61424f; // g
    materialDiffuse[2] = 0.07568f; // b
    materialDiffuse[3] = 1.0f;    // a

    // specular material
    materialSpecular[0] = 0.633f;    // r
    materialSpecular[1] = 0.727811f; // g
    materialSpecular[2] = 0.633f;    // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.6 * 128);

    // transformations
    mat4 translationMatrix = mat4::identity();
    mat4 modelMatrix = mat4::identity();
    mat4 viewMatrix = mat4::identity();

    //translationMatrix = vmath::translate(-5.5f, 4.8f, -15.0f); // or only translate() - glTranslate is replaced by this line
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(0, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(0, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);

        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX)*50, cos(gbLightAngleX)*50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY)*50, 0.0f, cos(gbLightAngleY)*50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ)*50, cos(gbLightAngleZ)*50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }

    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
    

    // ***** 2nd sphere on 1st column, jade *****
    // ambient material
    materialAmbient[0] = 0.135f;  // r
    materialAmbient[1] = 0.2225f; // g
    materialAmbient[2] = 0.1575f; // b
    materialAmbient[3] = 1.0f;   // a

    // diffuse material
    materialDiffuse[0] = 0.54f; // r
    materialDiffuse[1] = 0.89f; // g
    materialDiffuse[2] = 0.63f; // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.316228f; // r
    materialSpecular[1] = 0.316228f; // g
    materialSpecular[2] = 0.316228f; // b
    materialSpecular[3] = 1.0f;      // a

    // shininess
    materialShininess = float(0.1 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    //translationMatrix = vmath::translate(-5.5f, 3.0f, -15.0f); // or only translate() - glTranslate is replaced by this line
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(0, WIN_HEIGHT / 1.6, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(0, FULL_WIN_HEIGHT / 1.6, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 3rd sphere on 1st column, obsidian *****
    // ambient material
    materialAmbient[0] = 0.05375f; // r
    materialAmbient[1] = 0.05f;    // g
    materialAmbient[2] = 0.06625f; // b
    materialAmbient[3] = 1.0f;     // a

    // diffuse material
    materialDiffuse[0] = 0.18275f; // r
    materialDiffuse[1] = 0.17f;    // g
    materialDiffuse[2] = 0.22525f; // b
    materialDiffuse[3] = 1.0f;     // a

    // specular material
    materialSpecular[0] = 0.332741f; // r
    materialSpecular[1] = 0.328634f; // g
    materialSpecular[2] = 0.346435f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.3 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(0, WIN_HEIGHT / 2.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(0, FULL_WIN_HEIGHT / 2.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
    // ***** 4th sphere on 1st column, pearl *****
    // ambient material
    materialAmbient[0] = 0.25f;    // r
    materialAmbient[1] = 0.20725f; // g
    materialAmbient[2] = 0.20725f; // b
    materialAmbient[3] = 1.0f;    // a

    // diffuse material
    materialDiffuse[0] = 1.0f;   // r
    materialDiffuse[1] = 0.829f; // g
    materialDiffuse[2] = 0.829f; // b
    materialDiffuse[3] = 1.0f;  // a

    // specular material
    materialSpecular[0] = 0.296648f; // r
    materialSpecular[1] = 0.296648f; // g
    materialSpecular[2] = 0.296648f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.088 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    //translationMatrix = vmath::translate(-5.5f, -1.0f, -15.0f); // or only translate() - glTranslate is replaced by this line
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(0, WIN_HEIGHT / 3.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(0, FULL_WIN_HEIGHT / 3.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }


    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    // ***** 5th sphere on 1st column, ruby *****
    // ambient material
    materialAmbient[0] = 0.1745f;  // r
    materialAmbient[1] = 0.01175f; // g
    materialAmbient[2] = 0.01175f; // b
    materialAmbient[3] = 1.0f;    // a

    // diffuse material
    materialDiffuse[0] = 0.61424f; // r
    materialDiffuse[1] = 0.04136f; // g
    materialDiffuse[2] = 0.04136f; // b
    materialDiffuse[3] = 1.0f;    // a

    // specular material
    materialSpecular[0] = 0.727811f; // r
    materialSpecular[1] = 0.626959f; // g
    materialSpecular[2] = 0.626959f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.6 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    //translationMatrix = vmath::translate(-5.5f, -3.0f, -15.0f); // or only translate() - glTranslate is replaced by this line
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(0, WIN_HEIGHT / 6.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(0, FULL_WIN_HEIGHT / 6.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);



    // ***** 6th sphere on 1st column, turquoise *****
    // ambient material
    materialAmbient[0] = 0.1f;     // r
    materialAmbient[1] = 0.18725f; // g
    materialAmbient[2] = 0.1745f;  // b
    materialAmbient[3] = 1.0f;    // a

    // diffuse material
    materialDiffuse[0] = 0.396f;   // r
    materialDiffuse[1] = 0.74151f; // g
    materialDiffuse[2] = 0.69102f; // b
    materialDiffuse[3] = 1.0f;    // a

    // specular material
    materialSpecular[0] = 0.297254f; // r
    materialSpecular[1] = 0.30829f;  // g
    materialSpecular[2] = 0.306678f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.1 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    //translationMatrix = vmath::translate(-5.5f, -5.0f, -15.0f); // or only translate() - glTranslate is replaced by this line
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(0, WIN_HEIGHT / 90.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(0, FULL_WIN_HEIGHT / 90.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 1st sphere on 2nd column, brass *****
    // ambient material
    materialAmbient[0] = 0.329412f; // r
    materialAmbient[1] = 0.223529f; // g
    materialAmbient[2] = 0.027451f; // b
    materialAmbient[3] = 1.0f;     // a

    // diffuse material
    materialDiffuse[0] = 0.780392f; // r
    materialDiffuse[1] = 0.568627f; // g
    materialDiffuse[2] = 0.113725f; // b
    materialDiffuse[3] = 1.0f;     // a

    // specular material
    materialSpecular[0] = 0.992157f; // r
    materialSpecular[1] = 0.941176f; // g
    materialSpecular[2] = 0.807843f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.21794872 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    //translationMatrix = vmath::translate(-1.8f, 4.8f, -15.0f); // or only translate() - glTranslate is replaced by this line
    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH/4, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH/4, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 2nd sphere on 2nd column, bronze *****
    // ambient material
    materialAmbient[0] = 0.2125f; // r
    materialAmbient[1] = 0.1275f; // g
    materialAmbient[2] = 0.054f;  // b
    materialAmbient[3] = 1.0f;   // a

    // diffuse material
    materialDiffuse[0] = 0.714f;   // r
    materialDiffuse[1] = 0.4284f;  // g
    materialDiffuse[2] = 0.18144f; // b
    materialDiffuse[3] = 1.0f;    // a

    // specular material
    materialSpecular[0] = 0.393548f; // r
    materialSpecular[1] = 0.271906f; // g
    materialSpecular[2] = 0.166721f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.2 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 1.6, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 1.6, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 3rd sphere on 2nd column, chrome *****
    // ambient material
    materialAmbient[0] = 0.25f; // r
    materialAmbient[1] = 0.25f; // g
    materialAmbient[2] = 0.25f; // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.4f;  // r
    materialDiffuse[1] = 0.4f;  // g
    materialDiffuse[2] = 0.4f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.774597f; // r
    materialSpecular[1] = 0.774597f; // g
    materialSpecular[2] = 0.774597f; // b
    materialSpecular[3] = 1.0f;     // a
    
    // shininess
    materialShininess = float(0.6 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 2.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 2.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 4th sphere on 2nd column, copper *****
    // ambient material
    materialAmbient[0] = 0.19125f; // r
    materialAmbient[1] = 0.0735f;  // g
    materialAmbient[2] = 0.0225f;  // b
    materialAmbient[3] = 1.0f;    // a

    // diffuse material
    materialDiffuse[0] = 0.7038f;  // r
    materialDiffuse[1] = 0.27048f; // g
    materialDiffuse[2] = 0.0828f;  // b
    materialDiffuse[3] = 1.0f;    // a

    // specular material
    materialSpecular[0] = 0.256777f; // r
    materialSpecular[1] = 0.137622f; // g
    materialSpecular[2] = 0.086014f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.1 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(-0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 3.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 3.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 5th sphere on 2nd column, gold *****
    // ambient material
    materialAmbient[0] = 0.24725f; // r
    materialAmbient[1] = 0.1995f;  // g
    materialAmbient[2] = 0.0745f;  // b
    materialAmbient[3] = 1.0f;    // a

    // diffuse material
    materialDiffuse[0] = 0.75164f; // r
    materialDiffuse[1] = 0.60648f; // g
    materialDiffuse[2] = 0.22648f; // b
    materialDiffuse[3] = 1.0f;    // a

    // specular material
    materialSpecular[0] = 0.628281f; // r;
    materialSpecular[1] = 0.555802f; // g
    materialSpecular[2] = 0.366065f; // b
    materialSpecular[3] = 1.0f;     // a
    
    // shininess
    materialShininess = float(0.4 * 128);
    
    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 6.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 6.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 6th sphere on 2nd column, silver *****
    // ambient material
    materialAmbient[0] = 0.19225f; // r
    materialAmbient[1] = 0.19225f; // g
    materialAmbient[2] = 0.19225f; // b
    materialAmbient[3] = 1.0f;    // a

    // diffuse material
    materialDiffuse[0] = 0.50754f; // r
    materialDiffuse[1] = 0.50754f; // g
    materialDiffuse[2] = 0.50754f; // b
    materialDiffuse[3] = 1.0f;    // a

    // specular material
    materialSpecular[0] = 0.508273f; // r
    materialSpecular[1] = 0.508273f; // g
    materialSpecular[2] = 0.508273f; // b
    materialSpecular[3] = 1.0f;     // a

    // shininess
    materialShininess = float(0.4 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 4, WIN_HEIGHT / 90.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 90.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    // ***** 1st sphere on 3rd column, black *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.0f;  // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.01f; // r
    materialDiffuse[1] = 0.01f; // g
    materialDiffuse[2] = 0.01f; // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.50f; // r
    materialSpecular[1] = 0.50f; // g
    materialSpecular[2] = 0.50f; // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.25 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 2, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    // ***** 2nd sphere on 3rd column, cyan *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.1f;  // g
    materialAmbient[2] = 0.06f; // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.0f;        // r
    materialDiffuse[1] = 0.50980392f; // g
    materialDiffuse[2] = 0.50980392f; // b
    materialDiffuse[3] = 1.0f;       // a

    // specular material
    materialSpecular[0] = 0.50196078f; // r
    materialSpecular[1] = 0.50196078f; // g
    materialSpecular[2] = 0.50196078f; // b
    materialSpecular[3] = 1.0f;       // a

    // shininess
    materialShininess = float(0.25 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 1.6, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 2, FULL_WIN_HEIGHT / 1.6, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    // ***** 3rd sphere on 2nd column, green *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.0f;  // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.1f;  // r
    materialDiffuse[1] = 0.35f; // g
    materialDiffuse[2] = 0.1f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.45f; // r
    materialSpecular[1] = 0.55f; // g
    materialSpecular[2] = 0.45f; // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.25 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 2.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 2, FULL_WIN_HEIGHT / 2.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);



    // ***** 4th sphere on 3rd column, red *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.0f;  // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.5f;  // r
    materialDiffuse[1] = 0.0f;  // g
    materialDiffuse[2] = 0.0f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.7f;  // r
    materialSpecular[1] = 0.6f;  // g
    materialSpecular[2] = 0.6f;  // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.25 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 3.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 2, FULL_WIN_HEIGHT / 3.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    // ***** 5th sphere on 3rd column, white *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.0f;  // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.55f; // r
    materialDiffuse[1] = 0.55f; // g
    materialDiffuse[2] = 0.55f; // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.70f; // r
    materialSpecular[1] = 0.70f; // g
    materialSpecular[2] = 0.70f; // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.25 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 6.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 2, FULL_WIN_HEIGHT / 6.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    // ***** 6th sphere on 3rd column, yellow plastic *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.0f;  // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a


    // diffuse material
    materialDiffuse[0] = 0.5f;  // r
    materialDiffuse[1] = 0.5f;  // g
    materialDiffuse[2] = 0.0f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.60f; // r
    materialSpecular[1] = 0.60f; // g
    materialSpecular[2] = 0.50f; // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.25 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 2, WIN_HEIGHT / 90.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 2, FULL_WIN_HEIGHT / 90.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


    // ***** 1st sphere on 4th column, black *****
    // ambient material
    materialAmbient[0] = 0.02f; // r
    materialAmbient[1] = 0.02f; // g
    materialAmbient[2] = 0.02f; // b
    materialAmbient[3] = 1.0f; // a
    
    // diffuse material
    materialDiffuse[0] = 0.01f; // r
    materialDiffuse[1] = 0.01f; // g
    materialDiffuse[2] = 0.01f; // b
    materialDiffuse[3] = 1.0f; // a
    
    // specular material
    materialSpecular[0] = 0.4f;  // r
    materialSpecular[1] = 0.4f;  // g
    materialSpecular[2] = 0.4f;  // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.078125 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 1.4, WIN_HEIGHT / 1.3, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 1.4, FULL_WIN_HEIGHT / 1.3, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 2nd sphere on 4th column, cyan *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.05f; // g
    materialAmbient[2] = 0.05f; // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.4f;  // r
    materialDiffuse[1] = 0.5f;  // g
    materialDiffuse[2] = 0.5f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.04f; // r
    materialSpecular[1] = 0.7f;  // g
    materialSpecular[2] = 0.7f;  // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.078125 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 1.4, WIN_HEIGHT / 1.6, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 1.4, FULL_WIN_HEIGHT / 1.6, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 3rd sphere on 4th column, green *****
    // ambient material
    materialAmbient[0] = 0.0f;  // r
    materialAmbient[1] = 0.05f; // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.4f;  // r
    materialDiffuse[1] = 0.5f;  // g
    materialDiffuse[2] = 0.4f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.04f; // r
    materialSpecular[1] = 0.7f;  // g
    materialSpecular[2] = 0.04f; // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.078125 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 1.4, WIN_HEIGHT / 2.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 1.4, FULL_WIN_HEIGHT / 2.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 4th sphere on 4th column, red *****
    // ambient material
    materialAmbient[0] = 0.05f; // r
    materialAmbient[1] = 0.0f;  // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a
    
    // diffuse material
    materialDiffuse[0] = 0.5f;  // r
    materialDiffuse[1] = 0.4f;  // g
    materialDiffuse[2] = 0.4f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.7f;  // r
    materialSpecular[1] = 0.04f; // g
    materialSpecular[2] = 0.04f; // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.078125 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 1.4, WIN_HEIGHT / 3.1, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 1.4, FULL_WIN_HEIGHT / 3.1, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 5th sphere on 4th column, white *****
    // ambient material
    materialAmbient[0] = 0.05f; // r
    materialAmbient[1] = 0.05f; // g
    materialAmbient[2] = 0.05f; // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.5f;  // r
    materialDiffuse[1] = 0.5f;  // g
    materialDiffuse[2] = 0.5f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.7f;  // r
    materialSpecular[1] = 0.7f;  // g
    materialSpecular[2] = 0.7f;  // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.078125 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 1.4, WIN_HEIGHT / 6.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 1.4, FULL_WIN_HEIGHT / 6.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);

    // ***** 6th sphere on 4th column, yellow rubber *****
    // ambient material
    materialAmbient[0] = 0.05f; // r
    materialAmbient[1] = 0.05f; // g
    materialAmbient[2] = 0.0f;  // b
    materialAmbient[3] = 1.0f; // a

    // diffuse material
    materialDiffuse[0] = 0.5f;  // r
    materialDiffuse[1] = 0.5f;  // g
    materialDiffuse[2] = 0.4f;  // b
    materialDiffuse[3] = 1.0f; // a

    // specular material
    materialSpecular[0] = 0.7f;  // r
    materialSpecular[1] = 0.7f;  // g
    materialSpecular[2] = 0.04f; // b
    materialSpecular[3] = 1.0f; // a

    // shininess
    materialShininess = float(0.078125 * 128);

    // transformations
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
    modelMatrix = translationMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

    if (gbFullScreen == NO)
    {
        glViewport(WIN_WIDTH / 1.4, WIN_HEIGHT / 90.0, WIN_WIDTH / 4, WIN_HEIGHT / 4);
    }
    else
    {
        glViewport(FULL_WIN_WIDTH / 1.4, FULL_WIN_HEIGHT / 90.0, FULL_WIN_WIDTH / 4, FULL_WIN_HEIGHT / 4);
    }

    glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

    glBindVertexArray(vao_sphere);

    // sending light related uniforms
    if (bLight == YES)
    {
        glUniform1i(lightingEnabledUniform, 1);

        glUniform3fv(laUniform, 1, lightAmbient);
        glUniform3fv(ldUniform, 1, lightDiffuse);
        glUniform3fv(lsUniform, 1, lightSpecular);
        if (keyPressed == 1) // xRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(0.0f, sin(gbLightAngleX) * 50, cos(gbLightAngleX) * 50, 1.0f));
        }
        else if (keyPressed == 2) // yRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleY) * 50, 0.0f, cos(gbLightAngleY) * 50, 1.0));
        }
        else if (keyPressed == 3) // zRotation
        {
            glUniform4fv(lightPositionUniform, 1, vmath::vec4(sin(gbLightAngleZ) * 50, cos(gbLightAngleZ) * 50, 0.0f, 1.0));
        }
        else
        {
            glUniform4fv(lightPositionUniform, 1, lightPosition);
        }

        glUniform3fv(kaUniform, 1, materialAmbient);
        glUniform3fv(kdUniform, 1, materialDiffuse);
        glUniform3fv(ksUniform, 1, materialSpecular);
        glUniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        glUniform1i(lightingEnabledUniform, 0);
    }
    // stepE 2 : draw the desiered graphics/animation
    // here will be magic code

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
}

- (void) uninitialize
{
    // code
    // deletion/ uninitialization of vbo
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }

    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    if (vbo_sphere_elements)
    {
        glDeleteBuffers(1, &vbo_sphere_elements);
        vbo_sphere_elements = 0;
    }

    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    free(sphereData.vertices);
    free(sphereData.normals);
    free(sphereData.textureCoordinates);
    free(sphereData.elements);

    sphereData.vertices = NULL;
    sphereData.normals = NULL;
    sphereData.textureCoordinates = NULL;
    sphereData.elements = NULL;

    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);

        GLsizei numAttachedShaders;
            
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

        GLuint* shaderObjects = NULL;

        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }

        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);

        // stepF 6 : unuse the shader program object
        glUseProgram(0);

        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
    
    if (gpFile)
    {
        fprintf(gpFile, "Log File Successfully Closed");
        fclose(gpFile);
        gpFile = NULL;
    }

}

- (BOOL) acceptsFirstResponder // onTouchEvent is same to this
{
    // code
    [[self window]makeFirstResponder : self]; // window is method which is not related to window veriable in AppDelegate

    return(YES);
}

- (void) keyDown : (NSEvent*) event
{
    //code
    int key = (int)[[event characters]characterAtIndex : 0];

    switch(key)
    {
        case 27:
            [self uninitialize];
            [self release];
            [NSApp terminate : self];
        break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen : self];
            if (counter == 0)
            {
                counter++;
            }
            else if (counter == 1)
            {
                counter--;
            }
        break;

        case 'L':
        case 'l':
            if (bLight == NO)
            {
                bLight = YES;
            }
            else
            {
                bLight = NO;
            }
        break;

        case 'X':
        case 'x':
            keyPressed = 1;
            gbLightAngleX = 0.0f; // Reset
        break;

        case 'Y':
        case 'y':
            keyPressed = 2;
            gbLightAngleY = 0.0f; // Reset
        break;

        case 'Z':
        case 'z':
            keyPressed = 3;
            gbLightAngleZ = 0.0f; // Reset
        break;
        
        default:
            keyPressed = 0;
/*            {
                glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);
            }
            else
            {
                glViewport(0, 0, FULL_WIN_WIDTH, FULL_WIN_HEIGHT);
            }
*/
        break;
    }
}

- (void) mouseDown : (NSEvent*)event
{
    // code

}

- (void) dealloc
{
    // code
    [super dealloc];

    if(displayLink)
    {
        CVDisplayLinkStop(displayLink);
        CVDisplayLinkRelease(displayLink);

        displayLink = nil;
    }
}

@end

// implement the display link call back function
CVReturn MyDisplayLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp* currentTime, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* view)
{
    // code
    CVReturn result = [(GLView*)view getFrameForTime : outputTime];
    
    return(result);
}

// compile : sh build.sh











