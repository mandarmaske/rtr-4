//
//  OGL.m
//  build.sh madhye pan changes karne
//
//  Created by user226430 on 12/11/22.
//
// display -> drawView->getFrameForTime->MyDisplayLinkCallback->Core Video cha Driver

// header files
#import <Foundation/Foundation.h> // like stdio.h
#import <Cocoa/Cocoa.h>
#import "vmath.h"
using namespace vmath;

#import <QuartzCore/CVDisplayLink.h> // Core Video Display Link - sync refresh rate yaavar depend aahe
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*); // vegla thread create karto

// global variable declarations
FILE* gpFile = NULL;

// interface declarations / class declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// entry-point function
int main(int argc, char* argv[])
{
   // code
   
   // creation of object
   NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

   // create application object for this application
   NSApp = [NSApplication sharedApplication];
   
   // create app delegate object
   AppDelegate *appDelegate = [[AppDelegate alloc]init];
   
   // give our app delegate to NSApp
   [NSApp setDelegate : appDelegate];
   
   // start gameloop (run loop)
   [NSApp run];
   
   // tell autorelase pool to delete its all object
   [pool release];
   
   return(0);
}

// implementation of AppDelegate
@implementation AppDelegate
{
@private
   NSWindow *window;
   GLView *view;
}

- (void) applicationDidFinishLaunching : (NSNotification*)notification // similar like WM_CREATE
{
   // code
   NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
   NSString *appDirPath = [appBundle bundlePath]; // Window dir cha path
   NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // 04-Log file cha path
   NSString *logFileNameWithPath = [NSString stringWithFormat : @"%@/log.txt", parentDirPath];
   
   if (logFileNameWithPath == nil)
   {
       printf("Path cannot be obtained\n");
       [self release];
       [NSApp terminate : self];
   }
   
   const char *pszLogFileNameWithPath = [logFileNameWithPath UTF8String];
   // const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
   
   gpFile = fopen(pszLogFileNameWithPath, "w");
   
   if (gpFile == NULL)
   {
       [self release];
       [NSApp terminate : self];
   }
   
   fprintf(gpFile, "Log File Created successfully\n");
   
   NSRect rect = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSRect ~ CGRect (CoreGraphics)
   
   window = [[NSWindow alloc] initWithContentRect : rect
                                       styleMask : NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing : NSBackingStoreBuffered
                                           defer : NO];
   
   [window setTitle : @"macOS Window : MDM"];
   NSColor *backgroundColor = [NSColor blackColor];
   [window setBackgroundColor : backgroundColor];
   [window center];
   
   view = [[GLView alloc] initWithFrame : rect];
   [window setContentView : view];
   
   // set windows delegate to this object
   [window setDelegate : self];
   
   [window makeKeyAndOrderFront : self];
}

- (void) applicationWillTerminate : (NSNotification*)notification // similar like WM_DESTROY
{
   // code
   if (gpFile)
   {
       fprintf(gpFile, "Log File Closed Successfully\n");
       fclose(gpFile);
       gpFile = NULL;
   }
}

- (void) windowWillClose : (NSNotification*)notification // similar like WM_CLOSE
{
   // code
   [NSApp terminate : self]; // self is AppDelegate
}

- (void) dealloc
{
   // code
   if (view)
   {
       [view release];
       view = nil;
   }
   
   if (window)
   {
       [window release];
       window = nil; // null ~ nil
   }
   
   [super dealloc];
}
@end

// implementing GLView
@implementation GLView
{
   @private
   CVDisplayLinkRef displayLink;
   
   GLuint shaderProgramObject;

   enum
   {
       AMC_ATTRIBUTE_POSITION=0,
       AMC_ATTRIBUTE_COLOR,
       AMC_ATTRIBUTE_NORMAL,
       AMC_ATTRIBUTE_TEXTURE0
   };
   
   // variables for pyramid
   GLuint vao_Pyramid;
GLuint vbo_Pyramid_position;
GLuint vbo_Pyramid_normal;

GLfloat anglePyramid;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionMatrixUniform;

mat4 perspectiveProjectionMatrix;

GLuint laUniform[2];            // light ambient
GLuint ldUniform[2];            // light Diffuse
GLuint lsUniform[2];            // light specular
GLuint lightPositionUniform[2];

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialEnableUniform;

GLuint lightingEnableUniform;

struct Light
{
   vec4 lightAmbient;
   vec4 lightDiffuse;
   vec4 lightSpecular;
   vec4 lightPosition;
};

Light lights[2];

GLfloat materialAmbient[4];
GLfloat materialDiffuse[4];
GLfloat materialSpecular[4];
GLfloat materialShininess;
   
   BOOL bLight;

}

- (id) initWithFrame : (NSRect) frame // frame or rect aapla naav aahe
{
   // code
   self = [super initWithFrame : frame];
   
   if (self)
   {
       // code
     
     // Step 1 : initialize the array of OpenGL Pixel Format Attributes (PFA)
       NSOpenGLPixelFormatAttribute openGLPixelFormarAttributes[] =
     {
       NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
       NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
           NSOpenGLPFAColorSize, 24,
         NSOpenGLPFADepthSize, 24,
         NSOpenGLPFAAlphaSize, 8,
           NSOpenGLPFANoRecovery,
       NSOpenGLPFAAccelerated,
       NSOpenGLPFADoubleBuffer,
           0
     };

     // Step 2 : Create OpenGL pixel format using above attributes
     NSOpenGLPixelFormat *glPixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes : openGLPixelFormarAttributes]autorelease];
     
     if (glPixelFormat == nil)
     {
       fprintf(gpFile, "failed to create glPixelFormat\n");
       [self uninitialize];
       [self release];
       [NSApp terminate : self];
     }

       // Step 3 : Create OpenGL context using above pixel format
     NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat : glPixelFormat shareContext : nil]autorelease];
   
     if (glContext == nil)
     {
       fprintf(gpFile, "failed to create glContext\n");
       [self uninitialize];
       [self release];
       [NSApp terminate : self];
     }

       // Step 4 : Set this view's pixel format by using above pixel format
     [self setPixelFormat : glPixelFormat];

       // step 5 : Set view's context using above OpenGL context
     [self setOpenGLContext : glContext];
   }
   
   return(self);
}

// define getFrameForTime custom method with predefined signature
- (CVReturn) getFrameForTime : (const CVTimeStamp*) outputTime
{
   // code
   NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
   [self drawView];

   [pool release];
   return(kCVReturnSuccess);
}

- (void) prepareOpenGL // given by NSOpenGLView which overridable
{
   // code
   [super prepareOpenGL];

   // make the OpenGL context current
   [[self openGLContext]makeCurrentContext];

   // set double buffer swapping interval to 1
   GLint swapInterval = 1;

   [[self openGLContext]setValues : &swapInterval forParameter : NSOpenGLCPSwapInterval]; // CP - Context Parameter
   
   // OpenGL Log
   fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
   fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
   fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
   fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

   // call our initialize
   [self initialize];

   // create, configure, start display link
   
   // Step 1- Create the display link
   CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);

   // Step 2 - Set the callback display link
   CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);

   // Step 3 - Create OpenGL pixel format to CGL pixel format
   CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat]CGLPixelFormatObj]; // Obj means pointer

   // Step 4 - Convert NSOpenGLContext to CGLContext
   CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj];

   // Step 5 - Using above info set current CGDisplay to CGLPixelFormat And Context
   CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);

   // Step 6 - Start the display link
   CVDisplayLinkStart(displayLink);
}

// implementation of reshape which overridable
- (void) reshape
{
   // code
   [super reshape];
   
   CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

   NSRect rect = [self bounds]; // mala boundaries
   int width = rect.size.width;
   int height = rect.size.height;

   // call our resize
   [self resize : width : height];

   CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) drawRect : (NSRect) dirtyRect // drawRect ha view chya thread madhye kaam karto
{
   // code
   
   // To prevent flickering
   [self drawView];
}

- (void) drawView // navin thread sathi aahe - similar to drawRect
{
   // code
   [[self openGLContext]makeCurrentContext];

   CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

   // call our display here
   [self display];
   [self myupdate];

   // do double buffering
   CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

   CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (int) initialize
{
   // code
   // vertex shader
       const GLchar* vertexShaderSourceCode =
       "#version 410 core" \
       "\n" \
       "in vec4 a_position;" \
       "in vec3 a_normal;" \
       "uniform mat4 u_modelMatrix;" \
       "uniform mat4 u_viewMatrix;" \
       "uniform mat4 u_projectionMatrix;" \
       "uniform vec3 u_la[2];" \
       "uniform vec3 u_ld[2];" \
       "uniform vec3 u_ls[2];" \
       "uniform vec4 u_lightPosition[2];" \
       "uniform vec3 u_ka;" \
       "uniform vec3 u_kd;" \
       "uniform vec3 u_ks;" \
       "uniform float u_materialShininess;" \
       "uniform int u_lightingEnable;" \
       "out vec3 phong_ads_light;" \
       "void main(void)" \
       "{" \
       "if(u_lightingEnable == 1)" \
       "{" \
       "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
       "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
       "vec3 transformedNormals = normalize(normalMatrix * a_normal);" \
       "vec3 viewerVector = normalize(-eyeCoordinates.xyz);" \
       "vec3 ambient[2];" \
       "vec3 lightDirection[2];" \
       "vec3 diffuse[2];" \
       "vec3 reflectionVector[2];" \
       "vec3 specular[2];" \
       "vec3 total[2];"
       "for(int i = 0; i < 2; i++)" \
       "{" \
       "ambient[i] = u_la[i] * u_ka;" \
       "lightDirection[i] = normalize(vec3(u_lightPosition[i]) - eyeCoordinates.xyz);" \
       "diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformedNormals), 0.0);" \
       "reflectionVector[i] = reflect(-lightDirection[i], transformedNormals);" \
       "specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewerVector), 0.0), u_materialShininess);" \
       "total[i] = ambient[i] + diffuse[i] + specular[i];" \
       "}" \
       "phong_ads_light = total[0] + total[1];" \
       "}" \
       "else" \
       "{" \
       "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
       "}" \
       "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
       "}";

   GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
   glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
   glCompileShader(vertexShaderObject);

   GLint status;
   GLint infoLogLength;
   char* log = NULL;
   glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);

   if (status == GL_FALSE)
   {
       glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
       if (infoLogLength > 0)
       {
           log = (char*)malloc(infoLogLength);
           if (log != NULL)
           {
               GLsizei written;
               glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
               fprintf(gpFile, "Vertex Shader Compilation Log : %s \n", log);
               free(log);
               [self uninitialize];
               [self release];
               [NSApp terminate: self];
           }
       }
   }

   // Fragment Shader
   const GLchar* fragmentShaderSourceCode=
       "#version 410 core" \
       "\n" \
       "in vec3 phong_ads_light;" \
       "out vec4 FragColor;" \
       "void main(void)" \
       "{" \
       "FragColor = vec4(phong_ads_light, 1.0);" \
       "}";

   GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

   glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
   glCompileShader(fragmentShaderObject);

   status = 0;
   infoLogLength = 0;
   log = NULL;

   glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);

   if (status == GL_FALSE)
   {
       glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
       if (infoLogLength > 0)
       {
           log = (char*)malloc(infoLogLength);
           if(log != NULL)
           {
               GLsizei written;
               glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
               fprintf(gpFile, "Fragment Shader Compilation Log : %s \n", log);
               free(log);
               [self uninitialize];
               [self release];
               [NSApp terminate: self];
           }
       }
   }

   // Shader program object
   shaderProgramObject = glCreateProgram();
   glAttachShader(shaderProgramObject, vertexShaderObject);
   glAttachShader(shaderProgramObject, fragmentShaderObject);

   glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
   glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "a_normal");

   glLinkProgram(shaderProgramObject);
   
   
   status = 0;
   infoLogLength = 0;
   log = NULL;
   glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);
   if (status == GL_FALSE)
   {
       glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
       if (infoLogLength > 0)
       {
           log = (char*)malloc(infoLogLength);
           if (log != NULL)
           {
               GLsizei written;
               glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
               fprintf(gpFile, "Shader Program Link Log : %s \n", log);
               free(log);
               [self uninitialize];
               [self release];
               [NSApp terminate: self];
           }
       }
   }

   modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
   viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
   projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

   laUniform[0] = glGetUniformLocation(shaderProgramObject, "u_la[0]");
   ldUniform[0] = glGetUniformLocation(shaderProgramObject, "u_ld[0]");
   lsUniform[0] = glGetUniformLocation(shaderProgramObject, "u_ls[0]");
   lightPositionUniform[0] = glGetUniformLocation(shaderProgramObject, "u_lightPosition[0]");

   laUniform[1] = glGetUniformLocation(shaderProgramObject, "u_la[1]");
   ldUniform[1] = glGetUniformLocation(shaderProgramObject, "u_ld[1]");
   lsUniform[1] = glGetUniformLocation(shaderProgramObject, "u_ls[1]");
   lightPositionUniform[1] = glGetUniformLocation(shaderProgramObject, "u_lightPosition[1]");

   kaUniform = glGetUniformLocation(shaderProgramObject, "u_ka");
   kdUniform = glGetUniformLocation(shaderProgramObject, "u_kd");
   ksUniform = glGetUniformLocation(shaderProgramObject, "u_ks");
   materialEnableUniform = glGetUniformLocation(shaderProgramObject, "u_materialShininess");

   lightingEnableUniform = glGetUniformLocation(shaderProgramObject, "u_lightingEnable");

   // declaration vertex data arrays
   const GLfloat pyramidPosition[] =
   {
       // front
       0.0f, 1.0f, 0.0f,
       -1.0f, -1.0f, 1.0f,
       1.0f, -1.0f, 1.0f,

       // right
       0.0f, 1.0f, 0.0f,
       1.0f, -1.0f, 1.0f,
       1.0f, -1.0f, -1.0f,

       // back
       0.0f, 1.0f, 0.0f,
       1.0f, -1.0f, -1.0f,
       -1.0f, -1.0f, -1.0f,

       // left
       0.0f, 1.0f, 0.0f,
       -1.0f, -1.0f, -1.0f,
       -1.0f, -1.0f, 1.0f

   };

   GLfloat pyramidNormals[] =
   {
       0.0f, 0.447214f, 0.894427f,// front-top
       0.0f, 0.447214f, 0.894427f,// front-left
       0.0f, 0.447214f, 0.894427f,// front-right

       0.894427f, 0.447214f, 0.0f, // right-top
       0.894427f, 0.447214f, 0.0f, // right-left
       0.894427f, 0.447214f, 0.0f, // right-right

       0.0f, 0.447214f, -0.894427f, // back-top
       0.0f, 0.447214f, -0.894427f, // back-left
       0.0f, 0.447214f, -0.894427f, // back-right

       -0.894427f, 0.447214f, 0.0f, // left-top
       -0.894427f, 0.447214f, 0.0f, // left-left
       -0.894427f, 0.447214f, 0.0f // left-right

   };

   // vao and vbo related code
   
   // Pyramid
   // vao
   glGenVertexArrays(1, &vao_Pyramid);
   glBindVertexArray(vao_Pyramid);                                    // Bind Vertex Array Object(vao)

   // vbo for position
   glGenBuffers(1, &vbo_Pyramid_position);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_Pyramid_position);                        // Bind Buffer
   glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidPosition), pyramidPosition, GL_STATIC_DRAW);
   glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);                    // GL_FALSE means not Normalise
   glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

   glBindBuffer(GL_ARRAY_BUFFER, 0);                        // Unbind Buffer

   // vbo for normal
   glGenBuffers(1, &vbo_Pyramid_normal);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_Pyramid_normal);                        // Bind Buffer
   glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
   glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);                    // GL_FALSE means not Normalise
   glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

   glBindVertexArray(0);                                    // Unbind Vertex Array Object(vao) Pyramid

   // depth
   glClearDepth(1.0f);
   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LEQUAL);
   
   glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
   
   lights[0].lightAmbient = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
   lights[0].lightDiffuse = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f);
   lights[0].lightSpecular = vmath::vec4(1.0f, 0.0f, 0.0f, 1.0f);
   lights[0].lightPosition = vmath::vec4(-2.0f, 0.0f, 0.0f, 1.0f);

   lights[1].lightAmbient = vmath::vec4(0.0f, 0.0f, 0.0f, 1.0f);
   lights[1].lightDiffuse = vmath::vec4(0.0f, 0.0f, 1.0f, 1.0f);
   lights[1].lightSpecular = vmath::vec4(0.0f, 1.0f, 0.0f, 1.0f);
   lights[1].lightPosition = vmath::vec4(2.0f, 0.0f, 0.0f, 1.0f);

    materialAmbient[0]=0.0f;
    materialAmbient[1]=0.0f;
    materialAmbient[2]=0.0f;
    materialAmbient[3]=1.0f;
    
    materialDiffuse[0]=1.0f;
    materialDiffuse[1]=1.0f;
    materialDiffuse[2]=1.0f;
    materialDiffuse[3]=1.0f;
    
    materialSpecular[0]=1.0f;
    materialSpecular[1]=1.0f;
    materialSpecular[2]=1.0f;
    materialSpecular[3]=1.0f;
    materialShininess=50.0f;
   
   perspectiveProjectionMatrix = mat4::identity();

   anglePyramid = 0.0f;
   bLight = NO;

   return(0);
}

- (void) resize : (int) width : (int) height
{
   // code
   if (height < 0)
   {
       height = 1;
   }

   glViewport(0, 0, (GLsizei)width, (GLsizei)height);
   
   perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
   // code
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   
   // use the shader program object
       glUseProgram(shaderProgramObject);

   // Cube
   // Transformations
   mat4 translationMatrix = mat4::identity();
   mat4 rotationMatrix = mat4::identity();
   mat4 modelMatrix = mat4::identity();
   mat4 viewMatrix = mat4::identity();

   translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);   //glTranslatef() is replaced by this line
   rotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);

   modelMatrix = translationMatrix * rotationMatrix;        // Order is very important
   
   glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
   glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
   glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

   if (bLight == TRUE)
   {
       glUniform1i(lightingEnableUniform, 1);
       
       for (int i = 0; i < 2; i++)
       {
           glUniform3fv(laUniform[i], 1, lights[i].lightAmbient);
           glUniform3fv(ldUniform[i], 1, lights[i].lightDiffuse);
           glUniform3fv(lsUniform[i], 1, lights[i].lightSpecular);
           glUniform4fv(lightPositionUniform[i], 1, lights[i].lightPosition);
           
           //fprintf(gpFile,"light is enabled. \n");
       }

       glUniform3fv(kaUniform, 1, materialAmbient);
       glUniform3fv(kdUniform, 1, materialDiffuse);
       glUniform3fv(ksUniform, 1, materialSpecular);
       glUniform1f(materialEnableUniform, materialShininess);
   }
   else
   {
       glUniform1i(lightingEnableUniform, 0);
   }

   glBindVertexArray(vao_Pyramid);

   glDrawArrays(GL_TRIANGLES, 0, 12);

   glBindVertexArray(0);

   // unuse the shader program object
   glUseProgram(0);
}

- (void) myupdate // NSOpenGLView madhye tyachi swatchi update aahe, jar update lihla aaplya function cha ki te override kelya sarkha hoil ani je update aahe NSOpenGLView Madhye te update animation saathi nahi karat, NSWindow La move or resize sathi hi update aahe
{
   // code
   anglePyramid = anglePyramid + 0.5f;

       if (anglePyramid >= 360.0f)
           anglePyramid = anglePyramid - 360.0f;
}

- (void) uninitialize
{
   // code
   if (vbo_Pyramid_normal)
       {
           glDeleteBuffers(1, &vbo_Pyramid_normal);
           vbo_Pyramid_normal = 0;
       }
   
   if (vbo_Pyramid_position)
   {
       glDeleteBuffers(1, &vbo_Pyramid_position);
       vbo_Pyramid_position = 0;
   }

       // uninitialization of vertex buffer object(vao)
       if (vao_Pyramid)
       {
           glDeleteVertexArrays(1, &vao_Pyramid);
           vao_Pyramid = 0;
       }

   
   // shader uninitialization
   if (shaderProgramObject)
   {
       // stepF 0
       glUseProgram(shaderProgramObject);

       GLsizei numAttachedShaders;
           
       // stepF 1 : get the no. of attached shaders
       glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

       GLuint* shaderObjects = NULL;

       // stepF 2 : create empty buffer to hold array of  attached shader objects
       shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

       // stepF 3 : allocate enough memory to hold array of attached shader objects
       glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

       // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
       for (GLsizei i = 0; i < numAttachedShaders; i++)
       {
           glDetachShader(shaderProgramObject, shaderObjects[i]);
           glDeleteShader(shaderObjects[i]);
           shaderObjects[i] = 0;
       }

       // stepF 5 : free the memory allocated for the buffer
       free(shaderObjects);

       // stepF 6 : unuse the shader program object
       glUseProgram(0);

       // stepF 7 : delete the shader program object
       glDeleteProgram(shaderProgramObject);
       shaderProgramObject = 0;
   }
}

- (BOOL) acceptsFirstResponder // onTouchEvent is anologous to acceptsFirstResponder
{
   // code
   [[self window]makeFirstResponder : self]; // window is a method which is not related to window variable in AppDelegate
   
   return(YES);
}

- (void) keyDown : (NSEvent*) event
{
   // code
   int key = (int) [[event characters] characterAtIndex : 0]; // character is method to get index
   
   switch(key)
   {
       case 27:
           [self uninitialize];
           [self release];
           [NSApp terminate : self]; // self is MyView
           break;
           
       case 'F':
       case 'f':
           [[self window]toggleFullScreen : self];
           break;
           
       case 'L':
               case 'l':
                   if (bLight == NO)
                   {
                       bLight = YES;
                   }
                   else
                   {
                       bLight = NO;
                   }
                   break;
           
       default:
           break;
   }
}

- (void) mouseDown : (NSEvent*) event
{
   // code
   
}

- (void) dealloc
{
   // code
   [super dealloc];

   if (displayLink)
   {
   CVDisplayLinkStop(displayLink);
   CVDisplayLinkRelease(displayLink);
   displayLink = nil;
   }
}
@end

// implement the display link callback function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* currentTime, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* view)
{
   // code
   CVReturn result = [(GLView*)view getFrameForTime : outputTime];

   return(result);
}

// compile -
// clang -o Window Window.m -framework Cocoa

// execute -
// ./Window
