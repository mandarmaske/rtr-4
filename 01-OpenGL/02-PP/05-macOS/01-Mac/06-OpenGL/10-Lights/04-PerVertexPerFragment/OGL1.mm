//
//  OGL.m
//  build.sh madhye pan changes karne
//
//  Created by user226430 on 12/11/22.
//
// display -> drawView->getFrameForTime->MyDisplayLinkCallback->Core Video cha Driver

// header files
#import <Foundation/Foundation.h> // like stdio.h
#import <Cocoa/Cocoa.h>
#import "vmath.h"
using namespace vmath;

#import <QuartzCore/CVDisplayLink.h> // Core Video Display Link - sync refresh rate yaavar depend aahe
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "Sphere.h"

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*); // vegla thread create karto

// global variable declarations
FILE* gpFile = NULL;

// interface declarations / class declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// entry-point function
int main(int argc, char* argv[])
{
    // code
    
    // creation of object
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    // create application object for this application
    NSApp = [NSApplication sharedApplication];
    
    // create app delegate object
    AppDelegate *appDelegate = [[AppDelegate alloc]init];
    
    // give our app delegate to NSApp
    [NSApp setDelegate : appDelegate];
    
    // start gameloop (run loop)
    [NSApp run];
    
    // tell autorelase pool to delete its all object
    [pool release];
    
    return(0);
}

// implementation of AppDelegate
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *view;
}

- (void) applicationDidFinishLaunching : (NSNotification*)notification // similar like WM_CREATE
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
    NSString *appDirPath = [appBundle bundlePath]; // Window dir cha path
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // 04-Log file cha path
    NSString *logFileNameWithPath = [NSString stringWithFormat : @"%@/log.txt", parentDirPath];
    
    if (logFileNameWithPath == nil)
    {
        printf("Path cannot be obtained\n");
        [self release];
        [NSApp terminate : self];
    }
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath UTF8String];
    // const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if (gpFile == NULL)
    {
        [self release];
        [NSApp terminate : self];
    }
    
    fprintf(gpFile, "Log File Created successfully\n");
    
    NSRect rect = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSRect ~ CGRect (CoreGraphics)
    
    window = [[NSWindow alloc] initWithContentRect : rect
                                        styleMask : NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                          backing : NSBackingStoreBuffered
                                            defer : NO];
    
    [window setTitle : @"macOS Window : MDM"];
    NSColor *backgroundColor = [NSColor blackColor];
    [window setBackgroundColor : backgroundColor];
    [window center];
    
    view = [[GLView alloc] initWithFrame : rect];
    [window setContentView : view];
    
    // set windows delegate to this object
    [window setDelegate : self];
    
    [window makeKeyAndOrderFront : self];
}

- (void) applicationWillTerminate : (NSNotification*)notification // similar like WM_DESTROY
{
    // code
    if (gpFile)
    {
        fprintf(gpFile, "Log File Closed Successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void) windowWillClose : (NSNotification*)notification // similar like WM_CLOSE
{
    // code
    [NSApp terminate : self]; // self is AppDelegate
}

- (void) dealloc
{
    // code
    if (view)
    {
        [view release];
        view = nil;
    }
    
    if (window)
    {
        [window release];
        window = nil; // null ~ nil
    }
    
    [super dealloc];
}
@end

// implementing GLView
@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint shaderProgramObject_PV;
    GLuint shaderProgramObject_PF;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    // variables for cube
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_elements;
    
    SphereData sphereData;
    
    GLuint modelMatrixUniform_pv;
    GLuint viewMatrixUniform_pv;
    GLuint projectionMatrixUniform_pv;

    GLuint modelMatrixUniform_pf;
    GLuint viewMatrixUniform_pf;
    GLuint projectionMatrixUniform_pf;

    GLuint laUniform_pv; // light ambient
    GLuint ldUniform_pv; // light diffuse
    GLuint lsUniform_pv; // light specular
    GLuint lightPositionUniform_pv;

    GLuint kaUniform_pv;
    GLuint kdUniform_pv;
    GLuint ksUniform_pv;
    GLuint materialShininessUniform_pv;
    GLuint lightingEnabledUniform_pv;

    GLuint laUniform_pf; // light ambient
    GLuint ldUniform_pf; // light diffuse
    GLuint lsUniform_pf; // light specular
    GLuint lightPositionUniform_pf;

    GLuint kaUniform_pf;
    GLuint kdUniform_pf;
    GLuint ksUniform_pf;
    GLuint materialShininessUniform_pf;

    GLuint lightingEnabledUniform_pf;

    BOOL bLight;
    
    GLfloat lightAmbient[4];
    GLfloat lightDiffuse[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;

    mat4 perspectiveProjectionMatrix;
    
    int counter;
}

- (id) initWithFrame : (NSRect) frame // frame or rect aapla naav aahe
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // code
      
      // Step 1 : initialize the array of OpenGL Pixel Format Attributes (PFA)
        NSOpenGLPixelFormatAttribute openGLPixelFormarAttributes[] =
      {
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFAColorSize, 24,
          NSOpenGLPFADepthSize, 24,
          NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFANoRecovery,
        NSOpenGLPFAAccelerated,
        NSOpenGLPFADoubleBuffer,
            0
      };

      // Step 2 : Create OpenGL pixel format using above attributes
      NSOpenGLPixelFormat *glPixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes : openGLPixelFormarAttributes]autorelease];
      
      if (glPixelFormat == nil)
      {
        fprintf(gpFile, "failed to create glPixelFormat\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 3 : Create OpenGL context using above pixel format
      NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat : glPixelFormat shareContext : nil]autorelease];
    
      if (glContext == nil)
      {
        fprintf(gpFile, "failed to create glContext\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 4 : Set this view's pixel format by using above pixel format
      [self setPixelFormat : glPixelFormat];

        // step 5 : Set view's context using above OpenGL context
      [self setOpenGLContext : glContext];
    }
    
    return(self);
}

// define getFrameForTime custom method with predefined signature
- (CVReturn) getFrameForTime : (const CVTimeStamp*) outputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

- (void) prepareOpenGL // given by NSOpenGLView which overridable
{
    // code
    [super prepareOpenGL];

    // make the OpenGL context current
    [[self openGLContext]makeCurrentContext];

    // set double buffer swapping interval to 1
    GLint swapInterval = 1;

    [[self openGLContext]setValues : &swapInterval forParameter : NSOpenGLCPSwapInterval]; // CP - Context Parameter
    
    // OpenGL Log
    fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    // call our initialize
    [self initialize];

    // create, configure, start display link
    
    // Step 1- Create the display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);

    // Step 2 - Set the callback display link
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);

    // Step 3 - Create OpenGL pixel format to CGL pixel format
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat]CGLPixelFormatObj]; // Obj means pointer

    // Step 4 - Convert NSOpenGLContext to CGLContext
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj];

    // Step 5 - Using above info set current CGDisplay to CGLPixelFormat And Context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);

    // Step 6 - Start the display link
    CVDisplayLinkStart(displayLink);
}

// implementation of reshape which overridable
- (void) reshape
{
    // code
    [super reshape];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds]; // mala boundaries
    int width = rect.size.width;
    int height = rect.size.height;

    // call our resize
    [self resize : width : height];

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) drawRect : (NSRect) dirtyRect // drawRect ha view chya thread madhye kaam karto
{
    // code
    
    // To prevent flickering
    [self drawView];
}

- (void) drawView // navin thread sathi aahe - similar to drawRect
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // call our display here
    [self display];
    [self myupdate];

    // do double buffering
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (int) initialize
{
    // code
    // vertex shader
    const GLchar* vertexShaderSourceCode_PV =
            "#version 410 core" \
            "\n" \
            "in vec4 a_position;" \
            "in vec3 a_normal;" \
            "uniform mat4 u_modelMatrix;" \
            "uniform mat4 u_viewMatrix;" \
            "uniform mat4 u_projectionMatrix;" \
            "uniform vec3 u_la;" \
            "uniform vec3 u_ld;" \
            "uniform vec3 u_ls;" \
            "uniform vec4 u_lightPosition;" \
            "uniform vec3 u_ka;" \
            "uniform vec3 u_kd;" \
            "uniform vec3 u_ks;" \
            "uniform float u_materialShininess;" \
            "uniform int u_lightingEnabled;" \
            "out vec3 phong_ads_light;" \
            "void main(void)" \
            "{" \
                "if(u_lightingEnabled==1)" \
                "{" \
                    "vec3 ambient = u_la * u_ka;" \
                    "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
                    "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
                    "vec3 transformedNormals = normalize(normalMatrix * a_normal);" \
                    "vec3 lightDirection = normalize(vec3(u_lightPosition) - eyeCoordinates.xyz);" \
                    "vec3 diffuse = u_ld * u_kd * max(dot(lightDirection, transformedNormals), 0.0);" \
                    "vec3 reflectionVector = reflect(-lightDirection, transformedNormals);" \
                    "vec3 viewerVector = normalize(-eyeCoordinates.xyz);" \
                    "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, viewerVector), 0.0), u_materialShininess);" \
                    "phong_ads_light = ambient + diffuse + specular;" \
                "}" \
                "else" \
                "{" \
                    "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
                "}" \
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
            "}";

        // StepC 2 : creating shading object
        GLuint vertexShaderObject_PV = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject_PV, 1, (const GLchar**)&vertexShaderSourceCode_PV, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject_PV); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject_PV, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_PV, GL_INFO_LOG_LENGTH, &infoLogLength);
            
            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_PV, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                    // stepC 5 e : display the log
                    fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode_PV =
            "#version 410 core" \
            "\n" \
            "in vec3 phong_ads_light;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
                "FragColor = vec4(phong_ads_light,1.0);" \
            "}";

        // StepC 2 : creating shading object
        GLuint fragmentShaderObject_PV = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject_PV, 1, (const GLchar**)&fragmentShaderSourceCode_PV, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject_PV); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject_PV, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_PV, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_PV, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject_PV = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject_PV, vertexShaderObject_PV); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject_PV, fragmentShaderObject_PV);
        
        glBindAttribLocation(shaderProgramObject_PV, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject_PV, AMC_ATTRIBUTE_NORMAL, "a_normal");

        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject_PV);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject_PV, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_PV, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_PV, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    fprintf(gpFile, "Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];

                }
            }
        }

        modelMatrixUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
        viewMatrixUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_viewMatrix");
        projectionMatrixUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_projectionMatrix");

        // light uniform
        laUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_la");
        ldUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_ld");
        lsUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_ls");
        lightPositionUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_lightPosition");

        // material uniform
        kaUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_ka");
        kdUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_kd");
        ksUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_ks");
        materialShininessUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_materialShininess");

        // light enable uniform
        lightingEnabledUniform_pv = glGetUniformLocation(shaderProgramObject_PV, "u_lightingEnabled");

        // Per Fragment
        // vertex shader
        const GLchar* vertexShaderSourceCode_PF =
            "#version 410 core" \
            "\n" \
            "in vec4 a_position;" \
            "in vec3 a_normal;" \
            "uniform mat4 u_modelMatrix;" \
            "uniform mat4 u_viewMatrix;" \
            "uniform mat4 u_projectionMatrix;" \
            "uniform vec4 u_lightPosition;" \
            "uniform int u_lightingEnabled;" \
            "out vec3 transformedNormals;" \
            "out vec3 lightDirection;" \
            "out vec3 viewerVector;" \
            "void main(void)" \
            "{" \
            "if(u_lightingEnabled==1)" \
            "{" \
            "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" \
            "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" \
            "transformedNormals = normalMatrix * a_normal;" \
            "lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" \
            "viewerVector = -eyeCoordinates.xyz;" \
            "}" \
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
            "}";

        // StepC 2 : creating shading object
        GLuint vertexShaderObject_PF = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject_PF, 1, (const GLchar**)&vertexShaderSourceCode_PF, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject_PF); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject_PF, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_PF, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode_PF =
            "#version 410 core" \
            "\n" \
            "in vec3 transformedNormals;" \
            "in vec3 lightDirection;" \
            "in vec3 viewerVector;" \
            "uniform vec3 u_la;" \
            "uniform vec3 u_ld;" \
            "uniform vec3 u_ls;" \
            "uniform vec3 u_ka;" \
            "uniform vec3 u_kd;" \
            "uniform vec3 u_ks;" \
            "uniform float u_materialShininess;" \
            "uniform int u_lightingEnabled;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
            "vec3 phong_ads_light;" \
            "if(u_lightingEnabled==1)" \
            "{" \
            "vec3 ambient = u_la * u_ka;" \
            "vec3 normalized_transformedNormals = normalize(transformedNormals);" \
            "vec3 normalized_lightDirection = normalize(lightDirection);" \
            "vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" \
            "vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" \
            "vec3 normalized_viewerVector = normalize(viewerVector);" \
            "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" \
            "phong_ads_light = ambient + diffuse + specular;" \
            "}" \
            "else" \
            "{" \
            "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
            "}" \
            "FragColor = vec4(phong_ads_light,1.0);" \
            "}";

        // StepC 2 : creating shading object
        GLuint fragmentShaderObject_PF = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject_PF, 1, (const GLchar**)&fragmentShaderSourceCode_PF, NULL); // kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject_PF); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject_PF, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_PF, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject_PF = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject_PF, vertexShaderObject_PF); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject_PF, fragmentShaderObject_PF);

        glBindAttribLocation(shaderProgramObject_PF, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject_PF, AMC_ATTRIBUTE_NORMAL, "a_normal");

        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject_PF);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject_PF, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_PF, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_PF, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    fprintf(gpFile, "Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];

                }
            }
        }

        modelMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
        viewMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_viewMatrix");
        projectionMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_projectionMatrix");

        // light uniform
        laUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_la");
        ldUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_ld");
        lsUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_ls");
        lightPositionUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition");

        // material uniform
        kaUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_ka");
        kdUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_kd");
        ksUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_ks");
        materialShininessUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_materialShininess");

        // light enable uniform
        lightingEnabledUniform_pf = glGetUniformLocation(shaderProgramObject_PF, "u_lightingEnabled");

        Sphere *sphere = new Sphere();
        sphere->getSphereVertexData(&sphereData);

        delete sphere;
        sphere = NULL;

    // vao & vbo related code
        //declarations of vertex data arrays

    // vao
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);

        // position vbo
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.vertices, GL_STATIC_DRAW);

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // normal vbo
        glGenBuffers(1, &vbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.normals, GL_STATIC_DRAW);

        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // element vbo
        glGenBuffers(1, &vbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * 2280, sphereData.elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glBindVertexArray(0);

        // here start OpenGL code (depth & clear color code)
        //clear the screen using blue color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //depth related changes
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix = mat4::identity();
    
    lightAmbient[0] = 0.1f;
    lightAmbient[1] = 0.1f;
    lightAmbient[2] = 0.1f;
    lightAmbient[3] = 1.0f;

    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    
    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;
    
    materialAmbient[0] = 0.0f;
    materialAmbient[1] = 0.0f;
    materialAmbient[2] = 0.0f;
    materialAmbient[3] = 1.0f;
    
    materialDiffuse[0] = 0.5f;
    materialDiffuse[1] = 0.2f;
    materialDiffuse[2] = 0.7f;
    materialDiffuse[3] = 1.0f;
    
    materialSpecular[0] = 0.7f;
    materialSpecular[1] = 0.7f;
    materialSpecular[2] = 0.7f;
    materialSpecular[3] = 1.0f;

    materialShininess = 128.0f;
    
    lightPosition[0] = 100.0f;
    lightPosition[1] = 100.0f;
    lightPosition[2] = 100.0f;
    lightPosition[3] = 1.0f;
    
    bLight = NO;
    counter = 1;
    
    return(0);
}

- (void) resize : (int) width : (int) height
{
    // code
    if (height < 0)
    {
        height = 1;
    }

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if (counter == 1)
        {
            glUseProgram(shaderProgramObject_PV);
        }
        else if (counter == 2)
        {
            glUseProgram(shaderProgramObject_PF);
        }

    // transformations
    mat4 modelMatrix = mat4::identity();
    mat4 viewMatrix = mat4::identity();
    mat4 projectionViewMatrix = mat4::identity();

    mat4 translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f); // or only translate() - glTranslate is replaced by this line
        modelMatrix = translationMatrix;
        
    glUniformMatrix4fv(modelMatrixUniform_pv, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform_pv, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform_pv, 1, GL_FALSE, perspectiveProjectionMatrix);


     glUniformMatrix4fv(modelMatrixUniform_pf, 1, GL_FALSE, modelMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)
    glUniformMatrix4fv(viewMatrixUniform_pf, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projectionMatrixUniform_pf, 1, GL_FALSE, perspectiveProjectionMatrix);

    // sending light related uniforms
    if (bLight == YES)
    {
	  if (counter == 1)
        {
            glUniform1i(lightingEnabledUniform_pv, 1);

                glUniform3fv(laUniform_pv, 1, lightAmbient);
                glUniform3fv(ldUniform_pv, 1, lightDiffuse);
                glUniform3fv(lsUniform_pv, 1, lightSpecular);
                glUniform4fv(lightPositionUniform_pv, 1, lightPosition);

                glUniform3fv(kaUniform_pv, 1, materialAmbient);
                glUniform3fv(kdUniform_pv, 1, materialDiffuse);
                glUniform3fv(ksUniform_pv, 1, materialSpecular);

                glUniform1f(materialShininessUniform_pv, materialShininess);
        }
        else if (counter == 2)
        {
            glUniform1i(lightingEnabledUniform_pf, 1);

                glUniform3fv(laUniform_pf, 1, lightAmbient);
                glUniform3fv(ldUniform_pf, 1, lightDiffuse);
                glUniform3fv(lsUniform_pf, 1, lightSpecular);
                glUniform4fv(lightPositionUniform_pf, 1, lightPosition);

                glUniform3fv(kaUniform_pf, 1, materialAmbient);
                glUniform3fv(kdUniform_pf, 1, materialDiffuse);
                glUniform3fv(ksUniform_pf, 1, materialSpecular);

                glUniform1f(materialShininessUniform_pf, materialShininess);
        }
    }
    else
    {
        if (counter == 1)
        {
		glUniform1i(lightingEnabledUniform_pv, 0);

	  }
	  else if (counter == 2)
	  {
		glUniform1i(lightingEnabledUniform_pf, 0);
	  }	
    }

        glBindVertexArray(vao_sphere);

        // stepE 2 : draw the desiered graphics/animation
        // here will be magic code

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);


        glBindVertexArray(0);

        // stepE 3 : unuse the shader program object
        
        glUseProgram(0);
}

- (void) myupdate // NSOpenGLView madhye tyachi swatchi update aahe, jar update lihla aaplya function cha ki te override kelya sarkha hoil ani je update aahe NSOpenGLView Madhye te update animation saathi nahi karat, NSWindow La move or resize sathi hi update aahe
{
    // code
}

- (void) uninitialize
{
    // code
    if (vbo_sphere_position)
        {
            glDeleteBuffers(1, &vbo_sphere_position);
            vbo_sphere_position = 0;
        }

        if (vbo_sphere_normal)
        {
            glDeleteBuffers(1, &vbo_sphere_normal);
            vbo_sphere_normal = 0;
        }

        if (vbo_sphere_elements)
        {
            glDeleteBuffers(1, &vbo_sphere_elements);
            vbo_sphere_elements = 0;
        }

        if (vao_sphere)
        {
            glDeleteVertexArrays(1, &vao_sphere);
            vao_sphere = 0;
        }

        free(sphereData.vertices);
        free(sphereData.normals);
        free(sphereData.textureCoordinates);
        free(sphereData.elements);

        sphereData.vertices = NULL;
        sphereData.normals = NULL;
        sphereData.textureCoordinates = NULL;
        sphereData.elements = NULL;

    
    // shader uninitialization
    if (shaderProgramObject_PV)
        {
            // stepF 0
            glUseProgram(shaderProgramObject_PV);

            GLsizei numAttachedShaders;
            
            // stepF 1 : get the no. of attached shaders
            glGetProgramiv(shaderProgramObject_PV, GL_ATTACHED_SHADERS, &numAttachedShaders);

            GLuint* shaderObjects = NULL;

            // stepF 2 : create empty buffer to hold array of  attached shader objects
            shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

            // stepF 3 : allocate enough memory to hold array of attached shader objects
            glGetAttachedShaders(shaderProgramObject_PV, numAttachedShaders, &numAttachedShaders, shaderObjects);

            // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
            for (GLsizei i = 0; i < numAttachedShaders; i++)
            {
                glDetachShader(shaderProgramObject_PV, shaderObjects[i]);
                glDeleteShader(shaderObjects[i]);
                shaderObjects[i] = 0;
            }

            // stepF 5 : free the memory allocated for the buffer
            free(shaderObjects);

            // stepF 6 : unuse the shader program object
            glUseProgram(0);

            // stepF 7 : delete the shader program object
            glDeleteProgram(shaderProgramObject_PV);
            shaderProgramObject_PV = 0;
        }

        if (shaderProgramObject_PF)
        {
            // stepF 0
            glUseProgram(shaderProgramObject_PF);

            GLsizei numAttachedShaders;

            // stepF 1 : get the no. of attached shaders
            glGetProgramiv(shaderProgramObject_PF, GL_ATTACHED_SHADERS, &numAttachedShaders);

            GLuint* shaderObjects = NULL;

            // stepF 2 : create empty buffer to hold array of  attached shader objects
            shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

            // stepF 3 : allocate enough memory to hold array of attached shader objects
            glGetAttachedShaders(shaderProgramObject_PF, numAttachedShaders, &numAttachedShaders, shaderObjects);

            // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
            for (GLsizei i = 0; i < numAttachedShaders; i++)
            {
                glDetachShader(shaderProgramObject_PF, shaderObjects[i]);
                glDeleteShader(shaderObjects[i]);
                shaderObjects[i] = 0;
            }

            // stepF 5 : free the memory allocated for the buffer
            free(shaderObjects);

            // stepF 6 : unuse the shader program object
            glUseProgram(0);

            // stepF 7 : delete the shader program object
            glDeleteProgram(shaderProgramObject_PF);
            shaderProgramObject_PF = 0;
        }
}

- (BOOL) acceptsFirstResponder // onTouchEvent is anologous to acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder : self]; // window is a method which is not related to window variable in AppDelegate
    
    return(YES);
}

- (void) keyDown : (NSEvent*) event
{
    // code
    int key = (int) [[event characters] characterAtIndex : 0]; // character is method to get index
    
    switch(key)
    {
        case 27:
            [[self window]toggleFullScreen : self];
            break;
            
        case 'Q':
        case 'q':
            [self uninitialize];
            [self release];
            [NSApp terminate : self]; // self is MyView
            break;
            
       
            
        case 'L':
                case 'l':
                    counter = 1;
                    if (bLight == NO)
                    {
                        bLight = YES;
                    }
                    else
                    {
                        bLight = NO;
                    }
                    break;
            
        case 'V':
                case 'v':
                    counter = 1;
                    break;
            
        case 'F':
        case 'f':
            counter = 2;
            break;
            
        default:
            break;
    }
}

- (void) mouseDown : (NSEvent*) event
{
    // code
    
}

- (void) dealloc
{
    // code
    [super dealloc];

    if (displayLink)
    {
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    displayLink = nil;
    }
}
@end

// implement the display link callback function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* currentTime, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* view)
{
    // code
    CVReturn result = [(GLView*)view getFrameForTime : outputTime];

    return(result);
}

// compile -
// clang -o Window Window.m -framework Cocoa

// execute -
// ./Window
