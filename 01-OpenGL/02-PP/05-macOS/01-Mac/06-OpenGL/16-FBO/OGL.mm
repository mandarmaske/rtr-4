//
//  OGL.m
//  build.sh madhye pan changes karne
//
//  Created by user226430 on 12/11/22.
//
// display -> drawView->getFrameForTime->MyDisplayLinkCallback->Core Video cha Driver

// header files
#import <Foundation/Foundation.h> // like stdio.h
#import <Cocoa/Cocoa.h>
#import "Sphere.h"
#import "vmath.h"
using namespace vmath;

#import <QuartzCore/CVDisplayLink.h> // Core Video Display Link - sync refresh rate yaavar depend aahe
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*); // vegla thread create karto

// global variable declarations
FILE* gpFile = NULL;

// interface declarations / class declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// entry-point function
int main(int argc, char* argv[])
{
    // code
    
    // creation of object
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    // create application object for this application
    NSApp = [NSApplication sharedApplication];
    
    // create app delegate object
    AppDelegate *appDelegate = [[AppDelegate alloc]init];
    
    // give our app delegate to NSApp
    [NSApp setDelegate : appDelegate];
    
    // start gameloop (run loop)
    [NSApp run];
    
    // tell autorelase pool to delete its all object
    [pool release];
    
    return(0);
}

// implementation of AppDelegate
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *view;
}

- (void) applicationDidFinishLaunching : (NSNotification*)notification // similar like WM_CREATE
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
    NSString *appDirPath = [appBundle bundlePath]; // Window dir cha path
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // 04-Log file cha path
    NSString *logFileNameWithPath = [NSString stringWithFormat : @"%@/log.txt", parentDirPath];
    
    if (logFileNameWithPath == nil)
    {
        printf("Path cannot be obtained\n");
        [self release];
        [NSApp terminate : self];
    }
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath UTF8String];
    // const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if (gpFile == NULL)
    {
        [self release];
        [NSApp terminate : self];
    }
    
    fprintf(gpFile, "Log File Created successfully\n");
    
    NSRect rect = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSRect ~ CGRect (CoreGraphics)
    
    window = [[NSWindow alloc] initWithContentRect : rect
                                        styleMask : NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                          backing : NSBackingStoreBuffered
                                            defer : NO];
    
    [window setTitle : @"macOS Window : MDM"];
    NSColor *backgroundColor = [NSColor blackColor];
    [window setBackgroundColor : backgroundColor];
    [window center];
    
    view = [[GLView alloc] initWithFrame : rect];
    [window setContentView : view];
    
    // set windows delegate to this object
    [window setDelegate : self];
    
    [window makeKeyAndOrderFront : self];
}

- (void) applicationWillTerminate : (NSNotification*)notification // similar like WM_DESTROY
{
    // code
    if (gpFile)
    {
        fprintf(gpFile, "Log File Closed Successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void) windowWillClose : (NSNotification*)notification // similar like WM_CLOSE
{
    // code
    [NSApp terminate : self]; // self is AppDelegate
}

- (void) dealloc
{
    // code
    if (view)
    {
        [view release];
        view = nil;
    }
    
    if (window)
    {
        [window release];
        window = nil; // null ~ nil
    }
    
    [super dealloc];
}
@end

// implementing GLView
@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    GLuint shaderProgramObject;
    
    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    GLuint vao_cube;
    GLuint vbo_cube_position;
    GLuint vbo_cube_texcoord;
    
    //GLuint texture_marble;
    GLuint textureSamplerUniform;
    
    // cube mvp uniform
    GLuint mvpMatrixUniform;
    
    mat4 perspectiveProjectionMatrix;
    
    GLfloat angleCube;
    
    // fbo (frame buffer object) related global variables
    GLuint fbo; // frame buffer object
    GLuint rbo; // render buffer object
    GLuint fbo_texture;
    BOOL bFBO_Result;
    
    // texture scene global variables
    GLuint shaderProgramObject_Sphere_PF;
    GLuint shaderProgramObject_Sphere_PV;
    
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_elements;
    
    mat4 perspectiveProjectionMatrix_sphere;
    
    // Uniform
   GLuint modelMatrixUniform_pv;
   GLuint viewMatrixUniform_pv;
   GLuint projectionMatrixUniform_pv;

    //Lights

    // per vertex
    GLuint laUniform_pv[3];
    GLuint ldUniform_pv[3];
    GLuint lsUniform_pv[3];
    GLuint lightPositionUniform_pv[3];

    GLuint kaUniform_pv;
    GLuint kdUniform_pv;
    GLuint ksUniform_pv;
    GLuint materialShininessUniform_pv;

    GLuint lightingEnabledUniform_pv;

    // per fragment
    GLuint modelMatrixUniform_pf;
    GLuint viewMatrixUniform_pf;
    GLuint projectionMatrixUniform_pf;

    //Lights
    GLuint laUniform_pf[3];
    GLuint ldUniform_pf[3];
    GLuint lsUniform_pf[3];
    GLuint lightPositionUniform_pf[3];

    GLuint kaUniform_pf;
    GLuint kdUniform_pf;
    GLuint ksUniform_pf;
    GLuint materialShininessUniform_pf;

    GLuint lightingEnabledUniform_pf;

    
    struct Light
    {
        vec4 lightAmbient;
        vec4 lightDiffuse;
        vec4 lightSpecular;
        vec4 lightPosition;
    };
    
   Light lights[3];
    
    BOOL bLight;
    SphereData sphereData;

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;
    
    char choosenKey;

    GLfloat gbLightAngleZero;
    
    int FBO_WIDTH;
    int FBO_HEIGHT;
    
    int counter;
    int winWidth;
    int winHeight;
}

- (id) initWithFrame : (NSRect) frame // frame or rect aapla naav aahe
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // code
      
      // Step 1 : initialize the array of OpenGL Pixel Format Attributes (PFA)
        NSOpenGLPixelFormatAttribute openGLPixelFormarAttributes[] =
        {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
                NSOpenGLPFAColorSize, 24,
              NSOpenGLPFADepthSize, 24,
              NSOpenGLPFAAlphaSize, 8,
                NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFADoubleBuffer,
                0
      };

      // Step 2 : Create OpenGL pixel format using above attributes
      NSOpenGLPixelFormat *glPixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes : openGLPixelFormarAttributes]autorelease];
      
      if (glPixelFormat == nil)
      {
        fprintf(gpFile, "failed to create glPixelFormat\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 3 : Create OpenGL context using above pixel format
      NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat : glPixelFormat shareContext : nil]autorelease];
    
      if (glContext == nil)
      {
        fprintf(gpFile, "failed to create glContext\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 4 : Set this view's pixel format by using above pixel format
      [self setPixelFormat : glPixelFormat];

        // step 5 : Set view's context using above OpenGL context
      [self setOpenGLContext : glContext];
    }
    
    return(self);
}

// define getFrameForTime custom method with predefined signature
- (CVReturn) getFrameForTime : (const CVTimeStamp*) outputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

- (void) prepareOpenGL // given by NSOpenGLView which overridable
{
    // code
    [super prepareOpenGL];

    // make the OpenGL context current
    [[self openGLContext]makeCurrentContext];

    // set double buffer swapping interval to 1
    GLint swapInterval = 1;

    [[self openGLContext]setValues : &swapInterval forParameter : NSOpenGLCPSwapInterval]; // CP - Context Parameter
    
    // OpenGL Log
    fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    // call our initialize
    [self initialize];

    // create, configure, start display link
    
    // Step 1- Create the display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);

    // Step 2 - Set the callback display link
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);

    // Step 3 - Create OpenGL pixel format to CGL pixel format
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat]CGLPixelFormatObj]; // Obj means pointer

    // Step 4 - Convert NSOpenGLContext to CGLContext
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj];

    // Step 5 - Using above info set current CGDisplay to CGLPixelFormat And Context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);

    // Step 6 - Start the display link
    CVDisplayLinkStart(displayLink);
}

// implementation of reshape which overridable
- (void) reshape
{
    // code
    [super reshape];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds]; // mala boundaries
    int width = rect.size.width;
    int height = rect.size.height;

    // call our resize
    [self resize : width : height];

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) drawRect : (NSRect) dirtyRect // drawRect ha view chya thread madhye kaam karto
{
    // code
    
    // To prevent flickering
    [self drawView];
}

- (void) drawView // navin thread sathi aahe - similar to drawRect
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // call our display here
    [self display];
    [self myupdate];

    // do double buffering
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (int) initialize
{
    // code
    // vertex shader
    const GLchar* vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 a_position;" \
    "in vec2 a_texcoord;" \
    "uniform mat4 u_mvpMatrix;" \
    "out vec2 a_texcoord_out;" \
    "void main(void)" \
    "{" \
        "gl_Position = u_mvpMatrix * a_position;" \
        "a_texcoord_out = a_texcoord;" \
    "}";

    // StepC 2 : creating shading object
    GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    // stepC 3 : giving shader code to shader object
    glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);
    
    // stepC 4 : compile the shader
    glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable
    
    // stepC 5 : error checking of shader compilation
    GLint status;
    GLint infoLogLength;
    char* log = NULL;

    // stepC 5 a : getting compilation status
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

    // stepC 5 b : getting length of log of compilation status
    if (status == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
        // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
        if (infoLogLength > 0)
        {
            log = (char*)malloc(infoLogLength);

            // stepC 5 d : get the compilation log into this allocated buffer
            if (log != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
                    
                // stepC 5 e : display the log
                fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

                // stepC 5 f : free the allocated buffer
                free(log);

                // stepc 5 g : exit the application due to error
                [self uninitialize];
                [self release];
                [NSApp terminate : self];
            }
        }
    }

    // StepC 1 : writing shading code
    // fragment shader
    const GLchar* fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec2 a_texcoord_out;" \
    "uniform sampler2D u_textureSampler;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
        "FragColor = texture(u_textureSampler, a_texcoord_out);" \
    "}";

    // StepC 2 : creating shading object
    GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    // stepC 3 : giving shader code to shader object
    glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);
    
    // stepC 4 : compile the shader
    glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

    // stepC 5 : error checking of shader compilation
    // reinitialization of this 3 veriables
    status = 0;
    infoLogLength = 0;
    log = NULL;

    // stepC 5 a : getting compilation status
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

    // stepC 5 b : getting length of log of compilation status
    if (status == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

        // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
        if (infoLogLength > 0)
        {
            log = (char*)malloc(infoLogLength);

            // stepC 5 d : get the compilation log into this allocated buffer
            if (log != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
                
                // stepC 5 e : display the log
                fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

                // stepC 5 f : free the allocated buffer
                free(log);

                // stepC 5 g : exit the application due to error
                [self uninitialize];
                [self release];
                [NSApp terminate : self];
            }
        }
    }

    // Shader program object
    // stepD 1 : create shader program object
    shaderProgramObject = glCreateProgram();
    
    // stepD 2 : attach desired shaders to this shader program  object
    glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
    glAttachShader(shaderProgramObject, fragmentShaderObject);
        
    glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    fprintf(gpFile, "Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];

                }
            }
        }

        mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix"); // uniform che location aahe (he non-zero aste)
        textureSamplerUniform = glGetUniformLocation(shaderProgramObject, "u_textureSampler");


    // vao & vbo related code
        //declarations of vertex data arrays
    const GLfloat cubePosition[] =
        {
            // top
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,

        // bottom
        1.0f, -1.0f, -1.0f,
       -1.0f, -1.0f, -1.0f,
       -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,

        // front
        1.0f, 1.0f, 1.0f,
       -1.0f, 1.0f, 1.0f,
       -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        // back
        1.0f, 1.0f, -1.0f,
       -1.0f, 1.0f, -1.0f,
       -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        // right
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        // left
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,

        };

        const GLfloat cubeTexcoords[] =
        {
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f,
        };

        // cube
        // vao
        glGenVertexArrays(1, &vao_cube); // vao:- vertex array object
        glBindVertexArray(vao_cube);

        // vbo for cubeposition
        glGenBuffers(1, &vbo_cube_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(cubePosition), cubePosition, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);

        // cube color
        glGenBuffers(1, &vbo_cube_texcoord);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_texcoord); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexcoords), cubeTexcoords, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        glBindVertexArray(0);
    
        // here start OpenGL code (depth & clear color code)
        //clear the screen using blue color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //depth related changes
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix = mat4::identity();
    
       materialAmbient[0] = 0.0f;
       materialAmbient[1] = 0.0f;
       materialAmbient[2] = 0.0f;
       materialAmbient[3] = 1.0f;
       
       materialDiffuse[0] = 1.0f;
       materialDiffuse[1] = 1.0f;
       materialDiffuse[2] = 1.0f;
       materialDiffuse[3] = 1.0f;
       
       materialSpecular[0] = 1.0f;
       materialSpecular[1] = 1.0f;
       materialSpecular[2] = 1.0f;
       materialSpecular[3] = 1.0f;

       materialShininess = 128.0f;
   
    choosenKey = 'n';
    
    bLight = NO;
    
    gbLightAngleZero = 0.0f;
    
        bFBO_Result = NO;
        angleCube = 0.0f;
    
        FBO_WIDTH = 512;
        FBO_HEIGHT = 512;
    
        [self createFBO : FBO_WIDTH : FBO_HEIGHT];
        [self initializeSphere : FBO_WIDTH : FBO_HEIGHT];


    return(0);
}

- (void) createFBO : (GLint)textureWidth : (GLint)textureHeight
{
    // code
        // step 1 - Check available render buffer size
        int maxRenderbufferSize;

        glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &maxRenderbufferSize);

        if (maxRenderbufferSize < textureWidth || maxRenderbufferSize < textureHeight)
        {
            fprintf(gpFile, "Insufficient Render Buffer Size\n");
            [self uninitialize];
            [self release];
            [NSApp terminate : self];
        }
        
        // Step 2 - Create Frame Buffer Object
        glGenFramebuffers(1, &fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);

        // Step 3 - Create Render Buffer Object
        glGenRenderbuffers(1, &rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, rbo);

        // Step 4 - Where to keep this render buffer (Storage and format of render buffer)
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, textureWidth, textureHeight); // 1 - target, 2 - format, 3, 4 - size kiti laagel storage la

        // Step 5 - Create empty texture for upcoming target scene
        glGenTextures(1, &fbo_texture);
        glBindTexture(GL_TEXTURE_2D, fbo_texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // horizontall
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // vertical
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, NULL); // GL_UNSIGNED_SHORT_5_6_5 matches with GL_DEPTH_COMPONENT16
        // 5_6_5 - Red, Green, Blue, why 6 - human eye is more adjustable to green color spectrum, (GL_DEPTH_COMPONENT16) 16 / 3 hot nahi so 5, 6, 5 kela
        // green la ektra 1 bit dila
        // last parameter is null ka - karan empty full window texture

        // Step 6 - Give Above texture to fbo
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture, 0);
        // 1 - target kuthe aahe buffer, 2 - kuthe chitkavu texture, 3 - binding point, 4 - binding point la konta texture, 5 - mipmap level

        // Step 7 - Give rbo to fbo (empty texture kasa bharaycha tar rbo ne)
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
        // 1 - target kuthe aahe buffer, 2 - frame buffer na render buffer kuthun baghaycha aahe tar depth buffer, 3 - konala taaku, 4 - kuth jo data ahe to kon aahe

        // Step 8 - Check whether the frame buffer created successfull or not
        GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        // 1 - target la aani frame buffer ahe ka nahi check kar

        if (result != GL_FRAMEBUFFER_COMPLETE)
        {
            fprintf(gpFile, "Frame Buffer is not complete\n");
            [self uninitialize];
            [self release];
            [NSApp terminate : self];
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

- (void) initializeSphere : (int) width : (int) height
{
        // as all variables and checking is done in main initialize

        // Lights Shader
      const GLchar* vertexShaderSourceCode_pv =
            "#version 410 core" \
            "\n" \
                "in vec4 a_position;" \
                "in vec3 a_normal;" \
                "uniform mat4 u_modelMatrix;" \
                "uniform mat4 u_viewMatrix;" \
                "uniform mat4 u_projectionMatrix;" \
                "uniform vec3 u_la[3];" \
                "uniform vec3 u_ld[3];" \
                "uniform vec3 u_ls[3];" \
                "uniform vec4 u_lightPosition[3];" \
                "uniform vec3 u_ka;" \
                "uniform vec3 u_kd;" \
                "uniform vec3 u_ks;" \
                "uniform float u_materialShininess;" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec3 phong_ads_light;" \
                "void main(void)" \
                "{" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec4 eyeCoordinates = u_viewMatrix*u_modelMatrix*a_position;" \
                "mat3 normalMatrix=mat3(u_viewMatrix*u_modelMatrix);" \
                "vec3 transformedNormals=normalize(normalMatrix*a_normal);" \
                "vec3 viewerVector=normalize(-eyeCoordinates.xyz);" \
                "vec3 ambient[3];" \
                "vec3 lightDirection[3];" \
                "vec3 diffuse[3];" \
                "vec3 reflectionVector[3];" \
                "vec3 specular[3];" \
                "vec3 total[3];" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "ambient[i] = u_la[i] * u_ka; " \
                "lightDirection[i]=normalize(vec3(u_lightPosition[i])-eyeCoordinates.xyz);" \
                "diffuse[i]=u_ld[i]*u_kd*max(dot(lightDirection[i],transformedNormals),0.0);" \
                "reflectionVector[i]=reflect(-lightDirection[i],transformedNormals);" \
                "specular[i]=u_ls[i]*u_ks*pow(max(dot(reflectionVector[i],viewerVector),0.0),u_materialShininess);" \
                "total[i] = ambient[i] + diffuse[i] + specular[i];" \
                "}" \
                "phong_ads_light = total[0] + total[1]+total[2];" \
                "}" \
                "else" \
                "{"
                "phong_ads_light=vec3(1.0,1.0,1.0);" \
                "}" \
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
                "}";

    
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        GLuint vertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);

        glShaderSource(vertexShaderObject_pv, 1, (const GLchar**)&vertexShaderSourceCode_pv, NULL);

        glCompileShader(vertexShaderObject_pv);

        glGetShaderiv(vertexShaderObject_pv, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_pv, infoLogLength, &written, log);
 
                    fprintf(gpFile, "Vertex Shader Compilation log : %s\n", log);
                    
                    free(log);
                     
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // Fragment Shader
       
        const GLchar* fragmentShaderSourceCode_pv =
            "#version 410 core" \
            "\n" \
            "in vec3 phong_ads_light;" \
                "out vec4 FragColor;" \
                "void main(void)" \
                "{" \
                "FragColor = vec4(phong_ads_light,1.0);" \
                "}";

        GLuint fragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(fragmentShaderObject_pv, 1, (const GLchar**)&fragmentShaderSourceCode_pv, NULL);

        glCompileShader(fragmentShaderObject_pv);

        status=0;
        infoLogLength=0;
        log = NULL;
       
        glGetShaderiv(fragmentShaderObject_pv, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_pv, infoLogLength, &written, log);
                    
                    fprintf(gpFile, "Fragment Shader Compilation log : %s\n", log);
                   
                    free(log);
                   
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }
      
        shaderProgramObject_Sphere_PV = glCreateProgram();
        
        glAttachShader(shaderProgramObject_Sphere_PV, vertexShaderObject_pv);
        glAttachShader(shaderProgramObject_Sphere_PV, fragmentShaderObject_pv);

        glBindAttribLocation(shaderProgramObject_Sphere_PV, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject_Sphere_PV, AMC_ATTRIBUTE_NORMAL, "a_normal");

        glLinkProgram(shaderProgramObject_Sphere_PV);

        status = 0;
        infoLogLength = 0;
        log = NULL;

        glGetProgramiv(shaderProgramObject_Sphere_PV, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_Sphere_PV, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_Sphere_PV, infoLogLength, &written, log);
                    
                    fprintf(gpFile, "Shader Program Link log : %s\n", log);
                   
                    free(log);
                    
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }
        
           modelMatrixUniform_pv = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_modelMatrix");
           viewMatrixUniform_pv= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_viewMatrix");
           projectionMatrixUniform_pv= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_projectionMatrix");
           laUniform_pv[0]= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[0]");
           ldUniform_pv[0]= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[0]");
           lsUniform_pv[0]= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[0]");
           lightPositionUniform_pv[0]= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[0]");

           laUniform_pv[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[1]");
           ldUniform_pv[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[1]");
           lsUniform_pv[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[1]");
           lightPositionUniform_pv[1] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[1]");

           laUniform_pv[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[2]");
           ldUniform_pv[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[2]");
           lsUniform_pv[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[2]");
           lightPositionUniform_pv[2] = glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[2]");

           kaUniform_pv= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ka");
           kdUniform_pv= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_kd");
           ksUniform_pv= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ks");
           materialShininessUniform_pv= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_materialShininess");

           lightingEnabledUniform_pv= glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightingEnabled");

        // Per Fragment
        // Vertex Shader
       
        const GLchar* vertexShaderSourceCode_pf =
            "#version 410 core" \
            "\n" \
                "in vec4 a_position;" \
                "in vec3 a_normal;" \
                "uniform mat4 u_modelMatrix;" \
                "uniform mat4 u_viewMatrix;" \
                "uniform mat4 u_projectionMatrix;" \
                "uniform vec4 u_lightPosition[3];" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec3 transformedNormals;" \
                "out vec3 lightDirection[3];" \
                "out vec3 viewerVector;" \
                "void main(void)" \
                "{" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec4 eyeCoordinates = u_viewMatrix*u_modelMatrix*a_position;" \
                "mat3 normalMatrix=mat3(u_viewMatrix*u_modelMatrix);" \
                "transformedNormals=normalize(normalMatrix*a_normal);" \
                "viewerVector=normalize(-eyeCoordinates.xyz);" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "lightDirection[i]=normalize(vec3(u_lightPosition[i])-eyeCoordinates.xyz);" \
                "}" \
                "}" \
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" \
                "}";

        GLuint vertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

        glShaderSource(vertexShaderObject_pf, 1, (const GLchar**)&vertexShaderSourceCode_pf, NULL);

        glCompileShader(vertexShaderObject_pf);

        glGetShaderiv(vertexShaderObject_pf, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject_pf, infoLogLength, &written, log);
                    
                    fprintf(gpFile, "Vertex Shader Compilation log : %s\n", log);
                    
                    free(log);
                    
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // Fragment Shader
        
        const GLchar* fragmentShaderSourceCode_pf =
            "#version 410 core" \
            "\n" \
    "in vec3 transformedNormals;" \
                "in vec3 lightDirection[3];" \
                "in vec3 viewerVector;" \
                "uniform vec3 u_la[3];" \
                "uniform vec3 u_ld[3];" \
                "uniform vec3 u_ls[3];" \
                "uniform vec3 u_ka;" \
                "uniform vec3 u_kd;" \
                "uniform vec3 u_ks;" \
                "uniform float u_materialShininess;" \
                "uniform mediump int u_lightingEnabled;" \
                "out vec4 FragColor;" \
                "void main(void)" \
                "{" \
                "vec3 phong_ads_light;" \
                "if(u_lightingEnabled==1)" \
                "{" \
                "vec3 normalised_transformed_normals=normalize(transformedNormals);" \
                "vec3 normalised_viewerVector=normalize(viewerVector);" \
                "vec3 ambient[3];" \
                "vec3 diffuse[3];" \
                "vec3 reflectionVector[3];" \
                "vec3 specular[3];" \
                "vec3 total[3];" \
                "for(int i=0; i<3;i++)" \
                "{" \
                "ambient[i] = u_la[i] * u_ka; " \
                "diffuse[i]=u_ld[i]*u_kd*max(dot(lightDirection[i],normalised_transformed_normals),0.0);" \
                "reflectionVector[i]=reflect(-lightDirection[i],normalised_transformed_normals);" \
                "specular[i]=u_ls[i]*u_ks*pow(max(dot(reflectionVector[i],normalised_viewerVector),0.0),u_materialShininess);" \
                "total[i] = ambient[i] + diffuse[i] + specular[i];" \
                "}" \
                "phong_ads_light = total[0] + total[1]+total[2];" \
                "}" \
                "else" \
                "{"
                "phong_ads_light=vec3(1.0,1.0,1.0);" \
                "}"
                "FragColor = vec4(phong_ads_light,1.0);" \
                "}";

        GLuint fragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(fragmentShaderObject_pf, 1, (const GLchar**)&fragmentShaderSourceCode_pf, NULL);

        glCompileShader(fragmentShaderObject_pf);

        status = 0;
        infoLogLength = 0;
        log = NULL;
        
        glGetShaderiv(fragmentShaderObject_pf, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject_pf, infoLogLength, &written, log);

                    fprintf(gpFile, "Fragment Shader Compilation log : %s\n", log);

                    free(log);
                   
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }
        
        shaderProgramObject_Sphere_PF = glCreateProgram();

        glAttachShader(shaderProgramObject_Sphere_PF, vertexShaderObject_pf);
        glAttachShader(shaderProgramObject_Sphere_PF, fragmentShaderObject_pf);

        glBindAttribLocation(shaderProgramObject_Sphere_PF, AMC_ATTRIBUTE_POSITION, "a_position");
        glBindAttribLocation(shaderProgramObject_Sphere_PF, AMC_ATTRIBUTE_NORMAL, "a_normal");

        glLinkProgram(shaderProgramObject_Sphere_PF);

        status = 0;
        infoLogLength = 0;
        log = NULL;

        glGetProgramiv(shaderProgramObject_Sphere_PF, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject_Sphere_PF, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject_Sphere_PF, infoLogLength, &written, log);

                    fprintf(gpFile, "Shader Program Link log : %s\n", log);

                    free(log);

                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

            modelMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_modelMatrix");
            viewMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_viewMatrix");
            projectionMatrixUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_projectionMatrix");
            laUniform_pf[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[0]");
            ldUniform_pf[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[0]");
            lsUniform_pf[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[0]");//Specular
            lightPositionUniform_pf[0] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[0]");

            laUniform_pf[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[1]");
            ldUniform_pf[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[1]");
            lsUniform_pf[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[1]");//Specular
            lightPositionUniform_pf[1] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[1]");

            laUniform_pf[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[2]");
            ldUniform_pf[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[2]");
            lsUniform_pf[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[2]");//Specular
            lightPositionUniform_pf[2] = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[2]");

            kaUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ka");
            kdUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_kd");
            ksUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ks");
            materialShininessUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_materialShininess");

            lightingEnabledUniform_pf = glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightingEnabled");

        // Declaration of Vertex data array
        Sphere *sphere = new Sphere();
        sphere->getSphereVertexData(&sphereData);

        delete sphere;
        sphere = NULL;
    
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);
       
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER,vbo_sphere_position);//Array hold karnara

        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*1146, sphereData.vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
      
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1, &vbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 1146, sphereData.normals, GL_STATIC_DRAW);

        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenBuffers(1, &vbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(short) * 2280, sphereData.elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
        glBindVertexArray(0);

        // here start OpenGL code (depth & clear color code)
        //clear the screen using blue color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //depth related changes
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix_sphere = mat4::identity();

       lights[0].lightAmbient[0]=0.0f;
            lights[0].lightAmbient[1]=0.0f;
            lights[0].lightAmbient[2]=0.0f;
            lights[0].lightAmbient[3]=1.0f;
            
            lights[0].lightDiffuse[0]=1.0f;
            lights[0].lightDiffuse[1]=0.0f;
            lights[0].lightDiffuse[2]=0.0f;
            lights[0].lightDiffuse[3]=1.0f;
            
            lights[0].lightSpecular[0]=1.0f;
            lights[0].lightSpecular[1]=0.0f;
            lights[0].lightSpecular[2]=0.0f;
            lights[0].lightSpecular[3]=1.0f;
            
            lights[0].lightPosition[0]=0.0f;
            lights[0].lightPosition[1]=0.0f;
            lights[0].lightPosition[2]=0.0f;
            lights[0].lightPosition[3]=1.0f;
            
            //Light 1
            lights[1].lightAmbient[0]=0.0f;
            lights[1].lightAmbient[1]=0.0f;
            lights[1].lightAmbient[2]=0.0f;
            lights[1].lightAmbient[3]=1.0f;
            
            lights[1].lightDiffuse[0]=0.0f;
            lights[1].lightDiffuse[1]=1.0f;
            lights[1].lightDiffuse[2]=0.0f;
            lights[1].lightDiffuse[3]=1.0f;
            
            lights[1].lightSpecular[0]=0.0f;
            lights[1].lightSpecular[1]=1.0f;
            lights[1].lightSpecular[2]=0.0f;
            lights[1].lightSpecular[3]=1.0f;
            
            lights[1].lightPosition[0]=0.0f;
            lights[1].lightPosition[1]=0.0f;
            lights[1].lightPosition[2]=0.0f;
            lights[1].lightPosition[3]=1.0f;
        
        //Light2
        lights[2].lightAmbient[0]=0.0f;
        lights[2].lightAmbient[1]=0.0f;
        lights[2].lightAmbient[2]=0.0f;
        lights[2].lightAmbient[3]=1.0f;
        
        lights[2].lightDiffuse[0]=0.0f;
        lights[2].lightDiffuse[1]=0.0f;
        lights[2].lightDiffuse[2]=1.0f;
        lights[2].lightDiffuse[3]=1.0f;
        
        lights[2].lightSpecular[0]=0.0f;
        lights[2].lightSpecular[1]=0.0f;
        lights[2].lightSpecular[2]=1.0f;
        lights[2].lightSpecular[3]=1.0f;
        
        lights[2].lightPosition[0]=0.0f;
        lights[2].lightPosition[1]=0.0f;
        lights[2].lightPosition[2]=0.0f;
        lights[2].lightPosition[3]=1.0f;

        //warmup resize call
        [self resize_sphere : FBO_WIDTH : FBO_HEIGHT];
}

- (void) resize_sphere : (int)width : (int)height
{
    if (height == 0)
            height = 1; // to avoid divided by 0 illegal instruction for feature code

        glViewport(0, 0, (GLsizei)width, (GLsizei)height);

        perspectiveProjectionMatrix_sphere = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (GLuint) loadGLTexture : (const char*) textureFileName
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
      NSString *appDirPath = [appBundle bundlePath]; // Window dir cha path
      NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // 04-Log file cha path
      NSString *textureFileNameWithPath = [NSString stringWithFormat : @"%@/%s", parentDirPath, textureFileName];

    if (textureFileNameWithPath == nil)
    {
        fprintf(gpFile, "texture file path name not obtained\n");
        [self uninitialize];
           [self release];
           [NSApp terminate : self];
        return(0);
    }
    
    // Step 1 : get NSImage Representaion of texture file
    NSImage *nsImage = [[NSImage alloc]initWithContentsOfFile : textureFileNameWithPath];
    // error checking for nsImage
    if (nsImage == nil)
    {
        fprintf(gpFile, "nsImage not obtained from texture file\n");
        [self uninitialize];
            [self release];
            [NSApp terminate : self];
    }

    // Step 2 : from above NSImage Representaion obtain CGImage Representation of texture file
    CGImageRef    cgImage = [nsImage CGImageForProposedRect : nil context : nil hints : nil];
    // context is not OpenGL context it is CG Graphics context

    // Step 3 : From this CGImage Representation get width and height of the image
    int width = (int)CGImageGetWidth(cgImage);
    int height = (int)CGImageGetHeight(cgImage);

    // Step 4 : From this CGImage Representation get CGData provider
    CGDataProviderRef cgDataProvider = CGImageGetDataProvider(cgImage);
    
    // Step 5 : Using CGData provider get Image Data in the form CFData (CF-Core Foundation)
    CFDataRef imageData = CGDataProviderCopyData(cgDataProvider);

    // Step 6 : Convert this CFData formatted image data into void*
    void* pixels = (void*)CFDataGetBytePtr(imageData); // returns const UInt8* which is type casted into void*

    // Proceed with usual texture creation code from this commment
    GLuint texture = 0;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture); // note the *
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    CFRelease(imageData);
    glBindTexture(GL_TEXTURE_2D, 0); // unbind

    return(texture);
}

- (void) resize : (int) width : (int) height
{
    // code
    if (height < 0)
    {
        height = 1;
    }

    winWidth = width;
    winHeight = height;
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    [self display_sphere : FBO_WIDTH : FBO_HEIGHT];

    [self update_sphere];
    
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
       [self resize : winWidth : winHeight];

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // stepE 1 :  use the shader program object
        glUseProgram(shaderProgramObject);

        // square
        // transformations
        mat4 translationMatrix = mat4::identity();
        mat4 scaleMatrix = mat4::identity();
        mat4 rotationMatrix_x = mat4::identity();
        mat4 rotationMatrix_y = mat4::identity();
        mat4 rotationMatrix_z = mat4::identity();
        mat4 rotationMatrix = mat4::identity();
        mat4 modelViewMatrix = mat4::identity();
        mat4 modelViewProjectionMatrix = mat4::identity();
        rotationMatrix_x = vmath::rotate(angleCube, 1.0f, 0.0f, 0.0f);
        rotationMatrix_y = vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
        rotationMatrix_z = vmath::rotate(angleCube, 0.0f, 0.0f, 1.0f);
        rotationMatrix = rotationMatrix_x * rotationMatrix_y * rotationMatrix_x;
        translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); //glTranslatef() is replaced by this line
        scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
        modelViewMatrix = translationMatrix * rotationMatrix * scaleMatrix; // order is imp
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix; // order imp aahe

        glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fbo);
        glUniform1i(textureSamplerUniform, 0);

        glBindVertexArray(vao_cube);

        // stepE 2 : draw the desiered graphics/animation
        // here will be magic code

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4); //PP made glQuad nahi other option ne draw karayche
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // stepE 3 : unuse the shader program object
        
        glUseProgram(0);
}

- (void) display_sphere : (GLint)textureWidth : (GLint)textureHeight
{
    // code
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        [self resize_sphere : textureWidth : textureHeight];

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
        if (bLight == YES)
        {
            if (choosenKey == 'v' || choosenKey == 'V')
           {
               glUseProgram(shaderProgramObject_Sphere_PV);
                       
                       // transformations
                       mat4 translationMatrix = mat4::identity();
                       mat4 modelMatrix = mat4::identity();
                       mat4 viewMatrix = mat4::identity();
                       
                       
                       translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
                       modelMatrix = translationMatrix;
                       
                       glUniformMatrix4fv(modelMatrixUniform_pv, 1, GL_FALSE, modelMatrix);
                       glUniformMatrix4fv(viewMatrixUniform_pv, 1, GL_FALSE, viewMatrix);
                       glUniformMatrix4fv(projectionMatrixUniform_pv, 1, GL_FALSE, perspectiveProjectionMatrix_sphere);
                       
                      
                       glUniform1i(lightingEnabledUniform_pv, 1);
                               
                               glUniform3fv(laUniform_pv[0], 1, lights[0].lightAmbient);
                               glUniform3fv(ldUniform_pv[0], 1, lights[0].lightDiffuse);
                               glUniform3fv(lsUniform_pv[0], 1, lights[0].lightSpecular);
                               glUniform4fv(lightPositionUniform_pv[0], 1, vmath::vec4(0.0f, 80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero), 1.0f));

                               glUniform3fv(laUniform_pv[1], 1, lights[1].lightAmbient);
                               glUniform3fv(ldUniform_pv[1], 1, lights[1].lightDiffuse);
                               glUniform3fv(lsUniform_pv[1], 1, lights[1].lightSpecular);
                               glUniform4fv(lightPositionUniform_pv[1], 1, vmath::vec4(80*sin(gbLightAngleZero),0.0f, 80*cos(gbLightAngleZero), 1.0f));

                               glUniform3fv(laUniform_pv[2], 1, lights[2].lightAmbient);
                               glUniform3fv(ldUniform_pv[2], 1, lights[2].lightDiffuse);
                               glUniform3fv(lsUniform_pv[2], 1, lights[2].lightSpecular);
                               glUniform4fv(lightPositionUniform_pv[2], 1, vmath::vec4(80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero),0.0f, 1.0f));

                               glUniform3fv(kaUniform_pv, 1, materialAmbient);
                               glUniform3fv(kdUniform_pv, 1, materialDiffuse);
                               glUniform3fv(ksUniform_pv, 1, materialSpecular);

                               glUniform1f(materialShininessUniform_pv, materialShininess);
                      
                       glBindVertexArray(vao_sphere);
                       
                       glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);

                       glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
                       
                       glBindVertexArray(0);
           }
           else if (choosenKey == 'f' || choosenKey == 'F')
           {
               glUseProgram(shaderProgramObject_Sphere_PF);
                 
                       // transformations
                       mat4 translationMatrix = mat4::identity();
                       mat4 modelMatrix = mat4::identity();
                       mat4 viewMatrix = mat4::identity();
                       
                       
                       translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
                       modelMatrix = translationMatrix;
                       
                       glUniformMatrix4fv(modelMatrixUniform_pf, 1, GL_FALSE, modelMatrix);
                       glUniformMatrix4fv(viewMatrixUniform_pf, 1, GL_FALSE, viewMatrix);
                       glUniformMatrix4fv(projectionMatrixUniform_pf, 1, GL_FALSE, perspectiveProjectionMatrix_sphere);
                       
                       
                       glUniform1i(lightingEnabledUniform_pf, 1);

                               glUniform3fv(laUniform_pf[0], 1, lights[0].lightAmbient);
                               glUniform3fv(ldUniform_pf[0], 1, lights[0].lightDiffuse);
                               glUniform3fv(lsUniform_pf[0], 1, lights[0].lightSpecular);
                               glUniform4fv(lightPositionUniform_pf[0], 1, vmath::vec4(0.0f, 80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero), 1.0f));

                               glUniform3fv(laUniform_pf[1], 1, lights[1].lightAmbient);
                               glUniform3fv(ldUniform_pf[1], 1, lights[1].lightDiffuse);
                               glUniform3fv(lsUniform_pf[1], 1, lights[1].lightSpecular);
                               glUniform4fv(lightPositionUniform_pf[1], 1, vmath::vec4(80*sin(gbLightAngleZero),0.0f, 80*cos(gbLightAngleZero), 1.0f));

                               glUniform3fv(laUniform_pf[2], 1, lights[2].lightAmbient);
                               glUniform3fv(ldUniform_pf[2], 1, lights[2].lightDiffuse);
                               glUniform3fv(lsUniform_pf[2], 1, lights[2].lightSpecular);
                               glUniform4fv(lightPositionUniform_pf[2], 1, vmath::vec4(80*sin(gbLightAngleZero), 80*cos(gbLightAngleZero),0.0f, 1.0f));

                               glUniform3fv(kaUniform_pf, 1, materialAmbient);
                               glUniform3fv(kdUniform_pf, 1, materialDiffuse);
                               glUniform3fv(ksUniform_pf, 1, materialSpecular);

                               glUniform1f(materialShininessUniform_pf, materialShininess);
                           
                      
                       glBindVertexArray(vao_sphere);
                       
                       glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);

                       glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
                       
                       glBindVertexArray(0);
               
               
           }
        }
        else
        {
             glUseProgram(shaderProgramObject_Sphere_PF);
              
                    // transformations
                    mat4 translationMatrix = mat4::identity();
                    mat4 modelMatrix = mat4::identity();
                    mat4 viewMatrix = mat4::identity();
                    
                    
                    translationMatrix = ::translate(0.0f, 0.0f, -2.0f);
                    modelMatrix = translationMatrix;
                    
                    glUniformMatrix4fv(modelMatrixUniform_pf, 1, GL_FALSE, modelMatrix);
                    glUniformMatrix4fv(viewMatrixUniform_pf, 1, GL_FALSE, viewMatrix);
                    glUniformMatrix4fv(projectionMatrixUniform_pf, 1, GL_FALSE, perspectiveProjectionMatrix_sphere);
                   
                    glUniform1i(lightingEnabledUniform_pf, 0);
                   
                    glBindVertexArray(vao_sphere);
                    
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);

                    glDrawElements(GL_TRIANGLES, sphereData.numberOfElements, GL_UNSIGNED_SHORT, 0);
                    
                    glBindVertexArray(0);
            
        }
        // stepE 3 : unuse the shader program object

        glUseProgram(0);
    
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

- (void) myupdate // NSOpenGLView madhye tyachi swatchi update aahe, jar update lihla aaplya function cha ki te override kelya sarkha hoil ani je update aahe NSOpenGLView Madhye te update animation saathi nahi karat, NSWindow La move or resize sathi hi update aahe
{
    // code
    angleCube = angleCube + 1.0f;

        if (angleCube >= 360.0f)
            angleCube = angleCube - 360.0f;
}

- (void) update_sphere
{
    gbLightAngleZero = gbLightAngleZero + 0.05f;
    if (gbLightAngleZero >= 360.0f)
    {
        gbLightAngleZero = gbLightAngleZero - 360.0f;
    }
}

- (void) uninitialize
{
    // code
    [self uninitialize_sphere];
    
    if (vbo_cube_position)
        {
            glDeleteBuffers(1, &vbo_cube_position);
            vbo_cube_position = 0;
        }

        if (vao_cube)
        {
            glDeleteVertexArrays(1, &vao_cube);
            vao_cube = 0;
        }

        if (vbo_cube_texcoord)
        {
            glDeleteBuffers(1, &vbo_cube_texcoord);
            vbo_cube_texcoord = 0;
        }
    
    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);

        GLsizei numAttachedShaders;
            
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

        GLuint* shaderObjects = NULL;

        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }

        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);

        // stepF 6 : unuse the shader program object
        glUseProgram(0);

        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
}

- (void) uninitialize_sphere
{
    if (vbo_sphere_position)
        {
            glDeleteBuffers(1, &vbo_sphere_position);
            vbo_sphere_position = 0;
        }

        if (vbo_sphere_normal)
        {
            glDeleteBuffers(1, &vbo_sphere_normal);
            vbo_sphere_normal = 0;
        }

        if (vbo_sphere_elements)
        {
            glDeleteBuffers(1, &vbo_sphere_elements);
            vbo_sphere_elements = 0;
        }

        if (vao_sphere)
        {
            glDeleteVertexArrays(1, &vao_sphere);
            vao_sphere = 0;
        }
    
    free(sphereData.vertices);
    free(sphereData.normals);
    free(sphereData.textureCoordinates);
    free(sphereData.elements);

    sphereData.vertices = NULL;
    sphereData.normals = NULL;
    sphereData.textureCoordinates = NULL;
    sphereData.elements = NULL;
}

- (BOOL) acceptsFirstResponder // onTouchEvent is anologous to acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder : self]; // window is a method which is not related to window variable in AppDelegate
    
    return(YES);
}

- (void) keyDown : (NSEvent*) event
{
    // code
    int key = (int) [[event characters] characterAtIndex : 0]; // character is method to get index
    
    switch(key)
    {
        case 27:
            [[self window]toggleFullScreen : self];
            break;
            
        case 'Q':
        case 'q':
            [self uninitialize];
            [self release];
            [NSApp terminate : self]; // self is MyView
                    break;

                case 'L':
                case 'l':
                    if (bLight == NO)
                    {
                        choosenKey = 'v';
                        bLight = YES;
                    }
                    else
                    {
                        choosenKey = 'n';
                        bLight = NO;
                    }
                    break;
            
        case 'v':
            case 'V':
                    choosenKey = 'v';
                break;
            case 'f':
            case 'F':
                    choosenKey = 'f';
                break;
            
        default:
            break;
    }
}

- (void) mouseDown : (NSEvent*) event
{
    // code
    
}

- (void) dealloc
{
    // code
    [super dealloc];

    if (displayLink)
    {
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    displayLink = nil;
    }
}
@end

// implement the display link callback function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* currentTime, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* view)
{
    // code
    CVReturn result = [(GLView*)view getFrameForTime : outputTime];

    return(result);
}

// compile -
// clang -o Window Window.m -framework Cocoa

// execute -
// ./Window

