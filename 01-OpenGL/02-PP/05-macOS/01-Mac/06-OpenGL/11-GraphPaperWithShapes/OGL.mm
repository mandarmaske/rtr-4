//
//  OGL.m
//  build.sh madhye pan changes karne
//
//  Created by user226430 on 12/11/22.
//
// display -> drawView->getFrameForTime->MyDisplayLinkCallback->Core Video cha Driver

// header files
#import <Foundation/Foundation.h> // like stdio.h
#import <Cocoa/Cocoa.h>
#import "vmath.h"
using namespace vmath;

#import <QuartzCore/CVDisplayLink.h> // Core Video Display Link - sync refresh rate yaavar depend aahe
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*); // vegla thread create karto

// global variable declarations
FILE* gpFile = NULL;

// interface declarations / class declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// entry-point function
int main(int argc, char* argv[])
{
    // code
    
    // creation of object
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    // create application object for this application
    NSApp = [NSApplication sharedApplication];
    
    // create app delegate object
    AppDelegate *appDelegate = [[AppDelegate alloc]init];
    
    // give our app delegate to NSApp
    [NSApp setDelegate : appDelegate];
    
    // start gameloop (run loop)
    [NSApp run];
    
    // tell autorelase pool to delete its all object
    [pool release];
    
    return(0);
}

// implementation of AppDelegate
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *view;
}

- (void) applicationDidFinishLaunching : (NSNotification*)notification // similar like WM_CREATE
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
    NSString *appDirPath = [appBundle bundlePath]; // Window dir cha path
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // 04-Log file cha path
    NSString *logFileNameWithPath = [NSString stringWithFormat : @"%@/log.txt", parentDirPath];
    
    if (logFileNameWithPath == nil)
    {
        printf("Path cannot be obtained\n");
        [self release];
        [NSApp terminate : self];
    }
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath UTF8String];
    // const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if (gpFile == NULL)
    {
        [self release];
        [NSApp terminate : self];
    }
    
    fprintf(gpFile, "Log File Created successfully\n");
    
    NSRect rect = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSRect ~ CGRect (CoreGraphics)
    
    window = [[NSWindow alloc] initWithContentRect : rect
                                        styleMask : NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                          backing : NSBackingStoreBuffered
                                            defer : NO];
    
    [window setTitle : @"macOS Window : MDM"];
    NSColor *backgroundColor = [NSColor blackColor];
    [window setBackgroundColor : backgroundColor];
    [window center];
    
    view = [[GLView alloc] initWithFrame : rect];
    [window setContentView : view];
    
    // set windows delegate to this object
    [window setDelegate : self];
    
    [window makeKeyAndOrderFront : self];
}

- (void) applicationWillTerminate : (NSNotification*)notification // similar like WM_DESTROY
{
    // code
    if (gpFile)
    {
        fprintf(gpFile, "Log File Closed Successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void) windowWillClose : (NSNotification*)notification // similar like WM_CLOSE
{
    // code
    [NSApp terminate : self]; // self is AppDelegate
}

- (void) dealloc
{
    // code
    if (view)
    {
        [view release];
        view = nil;
    }
    
    if (window)
    {
        [window release];
        window = nil; // null ~ nil
    }
    
    [super dealloc];
}
@end

// implementing GLView
@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    // variables for X Axis Line
    GLuint vao_horizontalLine;
    GLuint vbo_horizontalLine_position;
    GLuint vbo_horizontalLine_color;

    // variables for Y Axis Line
    GLuint vao_verticalLine;
    GLuint vbo_verticalLine_position;
    GLuint vbo_verticalLine_color;

    // variables for Horizontal Graph Lines
    GLuint vao_graphHLines;
    GLuint vbo_graphHLines_position;
    GLuint vbo_graphHLines_color;

    // variables for Triangle Lines
    GLuint vao_triangleLines;
    GLuint vbo_triangleLines_position;
    GLuint vbo_triangleLines_color;

    // variables for Square Lines
    GLuint vao_squareLines;
    GLuint vbo_squareLines_position;
    GLuint vbo_squareLines_color;

    // variables for Square Lines
    GLuint vao_circleLines;
    GLuint vbo_circleLines_position;
    GLuint vbo_circleLines_color;

    // variables for Vertical Graph Lines
    GLuint vao_graphVLines;
    GLuint vbo_graphVLines_position;
    
    GLuint mvpMatrixUniform;

    mat4 perspectiveProjectionMatrix;
    
    float base;
    float height1;
    float base1;

    BOOL gFlag;
    BOOL tFlag;
    BOOL sFlag;
    BOOL cFlag;

    GLfloat circleLine[101][3];
}

- (id) initWithFrame : (NSRect) frame // frame or rect aapla naav aahe
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // code
      
      // Step 1 : initialize the array of OpenGL Pixel Format Attributes (PFA)
        NSOpenGLPixelFormatAttribute openGLPixelFormarAttributes[] =
      {
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFAColorSize, 24,
          NSOpenGLPFADepthSize, 24,
          NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFANoRecovery,
        NSOpenGLPFAAccelerated,
        NSOpenGLPFADoubleBuffer,
            0
      };

      // Step 2 : Create OpenGL pixel format using above attributes
      NSOpenGLPixelFormat *glPixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes : openGLPixelFormarAttributes]autorelease];
      
      if (glPixelFormat == nil)
      {
        fprintf(gpFile, "failed to create glPixelFormat\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 3 : Create OpenGL context using above pixel format
      NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat : glPixelFormat shareContext : nil]autorelease];
    
      if (glContext == nil)
      {
        fprintf(gpFile, "failed to create glContext\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 4 : Set this view's pixel format by using above pixel format
      [self setPixelFormat : glPixelFormat];

        // step 5 : Set view's context using above OpenGL context
      [self setOpenGLContext : glContext];
    }
    
    return(self);
}

// define getFrameForTime custom method with predefined signature
- (CVReturn) getFrameForTime : (const CVTimeStamp*) outputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

- (void) prepareOpenGL // given by NSOpenGLView which overridable
{
    // code
    [super prepareOpenGL];

    // make the OpenGL context current
    [[self openGLContext]makeCurrentContext];

    // set double buffer swapping interval to 1
    GLint swapInterval = 1;

    [[self openGLContext]setValues : &swapInterval forParameter : NSOpenGLCPSwapInterval]; // CP - Context Parameter
    
    // OpenGL Log
    fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    // call our initialize
    [self initialize];

    // create, configure, start display link
    
    // Step 1- Create the display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);

    // Step 2 - Set the callback display link
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);

    // Step 3 - Create OpenGL pixel format to CGL pixel format
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat]CGLPixelFormatObj]; // Obj means pointer

    // Step 4 - Convert NSOpenGLContext to CGLContext
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj];

    // Step 5 - Using above info set current CGDisplay to CGLPixelFormat And Context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);

    // Step 6 - Start the display link
    CVDisplayLinkStart(displayLink);
}

// implementation of reshape which overridable
- (void) reshape
{
    // code
    [super reshape];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds]; // mala boundaries
    int width = rect.size.width;
    int height = rect.size.height;

    // call our resize
    [self resize : width : height];

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) drawRect : (NSRect) dirtyRect // drawRect ha view chya thread madhye kaam karto
{
    // code
    
    // To prevent flickering
    [self drawView];
}

- (void) drawView // navin thread sathi aahe - similar to drawRect
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // call our display here
    [self display];
    [self myupdate];

    // do double buffering
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (int) initialize
{
    // code
    // vertex shader
        const GLchar* vertexShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "in vec4 a_position;" \
            "in vec4 a_color;" \
            "uniform mat4 u_mvpMatrix;" \
            "out vec4 a_color_out;" \
            "void main(void)" \
            "{" \
                "gl_Position = u_mvpMatrix * a_position;" \
                "a_color_out = a_color;" \
            "}";

        // StepC 2 : creating shading object
        GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                    // stepC 5 e : display the log
                    fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "in vec4 a_color_out;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
                "FragColor = a_color_out;" \
            "}";

        // StepC 2 : creating shading object
        GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
        // ----------
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "a_color");

        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    fprintf(gpFile, "Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

    // vao & vbo related code
        //declarations of vertex data arrays

    const GLfloat horizontalLine[] =
        {
            1.0f,0.0f,0.0f,
            -1.0,0.0f,0.0f
        };

        const GLfloat horizontalLineColor[] =
        {
            1.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0,
            1.0f, 0.0f, 0.0f
        };

        const GLfloat verticalLine[] =
        {
            0.0f,1.0f,0.0f,
            0.0,-1.0f,0.0f
        };

        const GLfloat verticalLineColor[] =
        {
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0,
            0.0f, 1.0f, 0.0f
        };

        const GLfloat graphHLines[] =
        {
            -1.0f, 1.0f, 0.0f, // 1
            1.0f, 1.0f, 0.0f,

            -1.0f, 0.95f, 0.0f, // 2
            1.0f, 0.95f, 0.0f,

            -1.0f, 0.90f, 0.0f, // 3
            1.0f, 0.90f, 0.0f,

            -1.0f, 0.85f, 0.0f, // 4
            1.0f, 0.85f, 0.0f,

            -1.0f, 0.80f, 0.0f, // 5
            1.0f, 0.80f, 0.0f,

            -1.0f, 0.75f, 0.0f, // 6
            1.0f, 0.75f, 0.0f,

            -1.0f, 0.70f, 0.0f, // 7
            1.0f, 0.70f, 0.0f,

            -1.0f, 0.65f, 0.0f, // 8
            1.0f, 0.65f, 0.0f,

            -1.0f, 0.60f, 0.0f, // 9
            1.0f, 0.60f, 0.0f,

            -1.0f, 0.55f, 0.0f, // 10
            1.0f, 0.55f, 0.0f,

            -1.0f, 0.50f, 0.0f, // 11
            1.0f, 0.50f, 0.0f,

            -1.0f, 0.45f, 0.0f, // 12
            1.0f, 0.45f, 0.0f,

            -1.0f, 0.40f, 0.0f, // 13
            1.0f, 0.40f, 0.0f,

            -1.0f, 0.35f, 0.0f, // 14
            1.0f, 0.35f, 0.0f,

            -1.0f, 0.30f, 0.0f, // 15
            1.0f, 0.30f, 0.0f,

            -1.0f, 0.25f, 0.0f, // 16
            1.0f, 0.25f, 0.0f,

            -1.0f, 0.20f, 0.0f, // 17
            1.0f, 0.20f, 0.0f,

            -1.0f, 0.15f, 0.0f, // 18
            1.0f, 0.15f, 0.0f,

            -1.0f, 0.10f, 0.0f, // 19
            1.0f, 0.10f, 0.0f,

            -1.0f, 0.05f, 0.0f, // 20
            1.0f, 0.05f, 0.0f,

            -1.0f, -1.0f, 0.0f, // 21
            1.0f, -1.0f, 0.0f,

            -1.0f, -0.95f, 0.0f, // 22
            1.0f, -0.95f, 0.0f,

            -1.0f, -0.90f, 0.0f, // 23
            1.0f, -0.90f, 0.0f,

            -1.0f, -0.85f, 0.0f, // 24
            1.0f, -0.85f, 0.0f,

            -1.0f, -0.80f, 0.0f, // 25
            1.0f, -0.80f, 0.0f,

            -1.0f, -0.75f, 0.0f, // 26
            1.0f, -0.75f, 0.0f,

            -1.0f, -0.70f, 0.0f, // 27
            1.0f, -0.70f, 0.0f,

            -1.0f, -0.65f, 0.0f, // 28
            1.0f, -0.65f, 0.0f,

            -1.0f, -0.60f, 0.0f, // 29
            1.0f, -0.60f, 0.0f,

            -1.0f, -0.55f, 0.0f, // 30
            1.0f, -0.55f, 0.0f,

            -1.0f, -0.50f, 0.0f, // 31
            1.0f, -0.50f, 0.0f,

            -1.0f, -0.45f, 0.0f, // 32
            1.0f, -0.45f, 0.0f,

            -1.0f, -0.40f, 0.0f, // 33
            1.0f, -0.40f, 0.0f,

            -1.0f, -0.35f, 0.0f, // 34
            1.0f, -0.35f, 0.0f,

            -1.0f, -0.30f, 0.0f, // 35
            1.0f, -0.30f, 0.0f,

            -1.0f, -0.25f, 0.0f, // 36
            1.0f, -0.25f, 0.0f,

            -1.0f, -0.20f, 0.0f, // 37
            1.0f, -0.20f, 0.0f,

            -1.0f, -0.15f, 0.0f, // 38
            1.0f, -0.15f, 0.0f,

            -1.0f, -0.10f, 0.0f, // 39
            1.0f, -0.10f, 0.0f,

            -1.0f, -0.05f, 0.0f, // 40
            1.0f, -0.05f, 0.0f,
        };

        const GLfloat graphVLines[] =
        {
            1.0f, 1.0f, 0.0f, // 1
            1.0f, -1.0f, 0.0f,

            0.95f, 1.0f, 0.0f, // 2
            0.95f, -1.0f, 0.0f,

            0.90f, 1.0f, 0.0f, // 3
            0.90f, -1.0f, 0.0f,

            0.85f, 1.0f, 0.0f, // 4
            0.85f, -1.0f, 0.0f,

            0.80f, 1.0f, 0.0f, // 5
            0.80f, -1.0f, 0.0f,

            0.75f, 1.0f, 0.0f, // 6
            0.75f, -1.0f, 0.0f,

            0.70f, 1.0f, 0.0f, // 7
            0.70f, -1.0f, 0.0f,

            0.65f, 1.0f,  0.0f, // 8
            0.65f, -1.0f, 0.0f,

            0.60f, 1.0f, 0.0f, // 9
            0.60f, -1.0f, 0.0f,

            0.55f, 1.0f, 0.0f, // 10
            0.55f, -1.0f, 0.0f,

            0.50f, 1.0f, 0.0f, // 11
            0.50f, -1.0f, 0.0f,

            0.45f, 1.0f, 0.0f, // 12
            0.45f, -1.0f, 0.0f,

            0.40f, 1.0f, 0.0f, // 13
            0.40f, -1.0f, 0.0f,

            0.35f, 1.0f, 0.0f, // 14
            0.35f, -1.0f, 0.0f,

            0.30f, 1.0f, 0.0f, // 15
            0.30f, -1.0f, 0.0f,

            0.25f, 1.0f, 0.0f, // 16
            0.25f, -1.0f, 0.0f,

            0.20f, 1.0f, 0.0f, // 17
            0.20f, -1.0f, 0.0f,

            0.15f, 1.0f, 0.0f, // 18
            0.15f, -1.0f, 0.0f,

            0.10f, 1.0f, 0.0f, // 19
            0.10f, -1.0f, 0.0f,

            0.05f, 1.0f, 0.0f, // 20
            0.05f, -1.0f, 0.0f,

            -1.0f, 1.0f, 0.0f, // 21
            -1.0f, -1.0f, 0.0f,

            -0.95f, 1.0f, 0.0f, // 22
            -0.95f, -1.0f, 0.0f,

            -0.90f, 1.0f, 0.0f, // 23
            -0.90f, -1.0f, 0.0f,

            -0.85f, 1.0f, 0.0f, // 24
            -0.85f, -1.0f, 0.0f,

            -0.80f, 1.0f, 0.0f, // 25
            -0.80f, -1.0f, 0.0f,

            -0.75f, 1.0f, 0.0f, // 26
            -0.75f, -1.0f, 0.0f,

            -0.70f, 1.0f, 0.0f, // 27
            -0.70f, -1.0f, 0.0f,

            -0.65f, 1.0f, 0.0f, // 28
            -0.65f, -1.0f, 0.0f,

            -0.60f, 1.0f, 0.0f, // 29
            -0.60f, -1.0f, 0.0f,

            -0.55f, 1.0f, 0.0f, // 30
            -0.55f, -1.0f, 0.0f,

            -0.50f, 1.0f, 0.0f, // 31
            -0.50f, -1.0f, 0.0f,

            -0.45f, 1.0f, 0.0f, // 32
            -0.45f, -1.0f, 0.0f,

            -0.40f, 1.0f, 0.0f, // 33
            -0.40f, -1.0f, 0.0f,

            -0.35f, 1.0f, 0.0f, // 34
            -0.35f, -1.0f, 0.0f,

            -0.30f, 1.0f, 0.0f, // 35
            -0.30f, -1.0f, 0.0f,

            -0.25f, 1.0f, 0.0f, // 36
            -0.25f, -1.0f, 0.0f,

            -0.20f, 1.0f, 0.0f, // 37
            -0.20f, -1.0f, 0.0f,

            -0.15f, 1.0f, 0.0f, // 38
            -0.15f, -1.0f, 0.0f,

            -0.10f, 1.0f, 0.0f, // 39
            -0.10f, -1.0f, 0.0f,

            -0.05f, 1.0f, 0.0f, // 40
            -0.05f, -1.0f, 0.0f
        };

        const GLfloat graphHVLinesColor[] =
        {
            0.0f, 0.0f, 1.0f, // 1
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 2
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 3
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 4
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 5
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 6
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 7
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 8
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 9
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 10
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 11
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 12
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 13
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 14
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 15
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 16
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 17
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 18
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 19
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 20
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 21
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 22
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 23
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 24
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 25
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 26
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 27
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 28
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 29
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 30
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 31
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 32
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 33
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 34
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 35
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 36
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 37
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 38
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 39
            0.0f, 0.0f, 1.0f,

            0.0f, 0.0f, 1.0f, // 40
            0.0f, 0.0f, 1.0f,
        };

        // Horizontal Line
        // vao related code
        glGenVertexArrays(1, &vao_horizontalLine); //vao:- vertex array object
        glBindVertexArray(vao_horizontalLine);
        
        // vbo for position
        glGenBuffers(1, &vbo_horizontalLine_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_horizontalLine_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLine), horizontalLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_horizontalLine_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_horizontalLine_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(horizontalLineColor), horizontalLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        // Vertical Line
        // vao related code
        glGenVertexArrays(1, &vao_verticalLine); //vao:- vertex array object
        glBindVertexArray(vao_verticalLine);

        // vbo for position
        glGenBuffers(1, &vbo_verticalLine_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_verticalLine_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLine), verticalLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_verticalLine_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_verticalLine_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(verticalLineColor), verticalLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);


        // Graph Horizontal Lines
        // vao related code
        glGenVertexArrays(1, &vao_graphHLines); //vao:- vertex array object
        glBindVertexArray(vao_graphHLines);

        // vbo for position
        glGenBuffers(1, &vbo_graphHLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(graphHLines), graphHLines, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_graphHLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(graphHVLinesColor), graphHVLinesColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        // Graph Vertical Lines
        // vao related code
        glGenVertexArrays(1, &vao_graphVLines); //vao:- vertex array object
        glBindVertexArray(vao_graphVLines);

        // vbo for position
        glGenBuffers(1, &vbo_graphVLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphVLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(graphVLines), graphVLines, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_graphHLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_graphHLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(graphHVLinesColor), graphHVLinesColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);
    
        base = 1.2f;
        height1 = 0.0f;
        base1 = base;

        gFlag = NO;
        tFlag = NO;
        sFlag = NO;
        cFlag = NO;
    
        height1 = ((sqrt(3)) * (base1 / 2));
        base = base / 2;
        height1 = height1 / 2;
        float median = (base1 / sqrt(3)) / -4;

        const GLfloat triangleLine[] =
        {
            0.0f, (height1 - median), 0.0f,
            -base, -height1 - median, 0.0f,
            -base, -height1 - median, 0.0f,
            base, -height1 - median, 0.0f,
            base, -height1 - median, 0.0f,
            0.0f, (height1 - median), 0.0f
        };

        const GLfloat triangleLineColor[] =
        {
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f
        };

        // Triangle Line
        // vao related code
        glGenVertexArrays(1, &vao_triangleLines); //vao:- vertex array object
        glBindVertexArray(vao_triangleLines);

        // vbo for position
        glGenBuffers(1, &vbo_triangleLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_triangleLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleLine), triangleLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_triangleLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_triangleLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleLineColor), triangleLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        const GLfloat squareLine[] =
        {
            float(((base1 / sqrt(3)) * 2) / 2), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
            float(-(((base1 / sqrt(3)) * 2) / 2)), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,

            float(-(((base1 / sqrt(3)) * 2) / 2)), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
            float(-(((base1 / sqrt(3)) * 2) / 2)), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,

            float(-(((base1 / sqrt(3)) * 2) / 2)), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,
            float(((base1 / sqrt(3)) * 2) / 2), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,

            float(((base1 / sqrt(3)) * 2) / 2), float(-(((base1 / sqrt(3)) * 2) / 2)), 0.0f,
            float(((base1 / sqrt(3)) * 2) / 2), float(((base1 / sqrt(3)) * 2) / 2), 0.0f,
        };

        const GLfloat squareLineColor[] =
        {
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
        };

        // Square Line
        // vao related code
        glGenVertexArrays(1, &vao_squareLines); //vao:- vertex array object
        glBindVertexArray(vao_squareLines);

        // vbo for position
        glGenBuffers(1, &vbo_squareLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_squareLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(squareLine), squareLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_squareLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_squareLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(squareLineColor), squareLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        [self circleArray : (base1 / sqrt(3)) : (base1 / sqrt(3)) : 0.0f : 0.0f];

        const GLfloat circleLineColor[] =
        {
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
        };

        // Square Line
        // vao related code
        glGenVertexArrays(1, &vao_circleLines); //vao:- vertex array object
        glBindVertexArray(vao_circleLines);

        // vbo for position
        glGenBuffers(1, &vbo_circleLines_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_circleLines_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(circleLine), circleLine, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_circleLines_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_circleLines_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(circleLineColor), circleLineColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        // here start OpenGL code (depth & clear color code)
        //clear the screen using blue color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //depth related changes
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix = mat4::identity();

    return(0);
}

- (void) circleArray : (float) radius_x : (float) radius_y : (float) start : (float) end
{
    for (int i = 0; i <= 100; i++)
        {
            float angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
            circleLine[i][0] = cos(angle) * radius_x;
            circleLine[i][1] = sin(angle) * radius_y;
            circleLine[i][2] = 0.0f;
        }
}

- (void) resize : (int) width : (int) height
{
    // code
    if (height < 0)
    {
        height = 1;
    }

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);

        // "Triangle"
        // transformations
        mat4 translationMatrix = mat4::identity();
        mat4 modelViewMatrix = mat4::identity();
        mat4 modelViewProjectionMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -2.5f); // or only translate - glTranslate is replaced by this line
        modelViewMatrix = translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        
        glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        if (gFlag == YES)
        {
            glBindVertexArray(vao_graphHLines);

            // stepE 2 : draw the desired graphics/animation
            // here will be magic code
            glLineWidth(3);
            glDrawArrays(GL_LINES, 0, 80);

            glBindVertexArray(0);

            glBindVertexArray(vao_graphVLines);

            // stepE 2 : draw the desired graphics/animation
            // here will be magic code
            glLineWidth(3);
            glDrawArrays(GL_LINES, 0, 80);

            glBindVertexArray(0);

            glBindVertexArray(vao_horizontalLine);

            // stepE 2 : draw the desired graphics/animation
            // here will be magic code
            glLineWidth(3);
            glDrawArrays(GL_LINES, 0, 2);

            glBindVertexArray(0);

            glBindVertexArray(vao_verticalLine);

            // stepE 2 : draw the desired graphics/animation
            // here will be magic code
            glLineWidth(3);
            glDrawArrays(GL_LINES, 0, 2);

            glBindVertexArray(0);
        }
        
        if (tFlag == YES && gFlag == YES)
        {
            glBindVertexArray(vao_triangleLines);

            // stepE 2 : draw the desired graphics/animation
            // here will be magic code
            glLineWidth(3);
            glDrawArrays(GL_LINES, 0, 6);

            glBindVertexArray(0);
        }

        if (sFlag == YES && gFlag == YES)
        {
            glBindVertexArray(vao_squareLines);

            // stepE 2 : draw the desired graphics/animation
            // here will be magic code
            glLineWidth(3);
            glDrawArrays(GL_LINES, 0, 8);

            glBindVertexArray(0);
        }

        if (cFlag == YES && gFlag == YES)
        {
            glBindVertexArray(vao_circleLines);

            glDrawArrays(GL_LINE_LOOP, 0, 100);

            glBindVertexArray(0);
        }
        
        glUseProgram(0);
}

- (void) myupdate // NSOpenGLView madhye tyachi swatchi update aahe, jar update lihla aaplya function cha ki te override kelya sarkha hoil ani je update aahe NSOpenGLView Madhye te update animation saathi nahi karat, NSWindow La move or resize sathi hi update aahe
{
    // code
    
}

- (void) uninitialize
{
    // code
    if (vbo_verticalLine_position)
        {
            glDeleteBuffers(1, &vbo_verticalLine_position);
            vbo_verticalLine_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_verticalLine)
        {
            glDeleteVertexArrays(1, &vao_verticalLine);
            vao_verticalLine = 0;
        }

        // deletion/ uninitialization of vbo triangle
        if (vbo_horizontalLine_position)
        {
            glDeleteBuffers(1, &vbo_horizontalLine_position);
            vbo_horizontalLine_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao_horizontalLine)
        {
            glDeleteVertexArrays(1, &vao_horizontalLine);
            vao_horizontalLine = 0;
        }    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);

        GLsizei numAttachedShaders;
            
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

        GLuint* shaderObjects = NULL;

        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }

        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);

        // stepF 6 : unuse the shader program object
        glUseProgram(0);

        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
}

- (BOOL) acceptsFirstResponder // onTouchEvent is anologous to acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder : self]; // window is a method which is not related to window variable in AppDelegate
    
    return(YES);
}

- (void) keyDown : (NSEvent*) event
{
    // code
    int key = (int) [[event characters] characterAtIndex : 0]; // character is method to get index
    
    switch(key)
    {
        case 27:
            [self uninitialize];
            [self release];
            [NSApp terminate : self]; // self is MyView
            break;
            
        case 'F':
        case 'f':
            [[self window]toggleFullScreen : self];
            break;
            
        case 'G':
                gFlag = NO;
                break;

            case 'g':
                gFlag = YES;
                break;

            case 'T':
                tFlag = NO;
                break;

            case 't':
                    tFlag = YES;
                    break;

                case 'S':
                    sFlag = NO;
                    break;

                case 's':
                    sFlag = YES;
                    break;

                case 'C':
                    cFlag = NO;
                    break;
                
                case 'c':
                    cFlag = YES;
                    break;
            
        default:
            break;
    }
}

- (void) mouseDown : (NSEvent*) event
{
    // code
    
}

- (void) dealloc
{
    // code
    [super dealloc];

    if (displayLink)
    {
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    displayLink = nil;
    }
}
@end

// implement the display link callback function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* currentTime, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* view)
{
    // code
    CVReturn result = [(GLView*)view getFrameForTime : outputTime];

    return(result);
}

// compile -
// clang -o Window Window.m -framework Cocoa

// execute -
// ./Window
