//
//  OGL.m
//  build.sh madhye pan changes karne
//
//  Created by user226430 on 12/11/22.
//
// display -> drawView->getFrameForTime->MyDisplayLinkCallback->Core Video cha Driver

// header files
#import <Foundation/Foundation.h> // like stdio.h
#import <Cocoa/Cocoa.h>
#import "vmath.h"
using namespace vmath;

#import <QuartzCore/CVDisplayLink.h> // Core Video Display Link - sync refresh rate yaavar depend aahe
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

// global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp*, const CVTimeStamp*, CVOptionFlags, CVOptionFlags*, void*); // vegla thread create karto

// global variable declarations
FILE* gpFile = NULL;

// interface declarations / class declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// entry-point function
int main(int argc, char* argv[])
{
    // code
    
    // creation of object
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    // create application object for this application
    NSApp = [NSApplication sharedApplication];
    
    // create app delegate object
    AppDelegate *appDelegate = [[AppDelegate alloc]init];
    
    // give our app delegate to NSApp
    [NSApp setDelegate : appDelegate];
    
    // start gameloop (run loop)
    [NSApp run];
    
    // tell autorelase pool to delete its all object
    [pool release];
    
    return(0);
}

// implementation of AppDelegate
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *view;
}

- (void) applicationDidFinishLaunching : (NSNotification*)notification // similar like WM_CREATE
{
    // code
    NSBundle *appBundle = [NSBundle mainBundle]; // sandbox la cocoa internally bundle mhanto
    NSString *appDirPath = [appBundle bundlePath]; // Window dir cha path
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent]; // 04-Log file cha path
    NSString *logFileNameWithPath = [NSString stringWithFormat : @"%@/log.txt", parentDirPath];
    
    if (logFileNameWithPath == nil)
    {
        printf("Path cannot be obtained\n");
        [self release];
        [NSApp terminate : self];
    }
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath UTF8String];
    // const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding : NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogFileNameWithPath, "w");
    
    if (gpFile == NULL)
    {
        [self release];
        [NSApp terminate : self];
    }
    
    fprintf(gpFile, "Log File Created successfully\n");
    
    NSRect rect = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSRect ~ CGRect (CoreGraphics)
    
    window = [[NSWindow alloc] initWithContentRect : rect
                                        styleMask : NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                          backing : NSBackingStoreBuffered
                                            defer : NO];
    
    [window setTitle : @"macOS Window : MDM"];
    NSColor *backgroundColor = [NSColor blackColor];
    [window setBackgroundColor : backgroundColor];
    [window center];
    
    view = [[GLView alloc] initWithFrame : rect];
    [window setContentView : view];
    
    // set windows delegate to this object
    [window setDelegate : self];
    
    [window makeKeyAndOrderFront : self];
}

- (void) applicationWillTerminate : (NSNotification*)notification // similar like WM_DESTROY
{
    // code
    if (gpFile)
    {
        fprintf(gpFile, "Log File Closed Successfully\n");
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void) windowWillClose : (NSNotification*)notification // similar like WM_CLOSE
{
    // code
    [NSApp terminate : self]; // self is AppDelegate
}

- (void) dealloc
{
    // code
    if (view)
    {
        [view release];
        view = nil;
    }
    
    if (window)
    {
        [window release];
        window = nil; // null ~ nil
    }
    
    [super dealloc];
}
@end

// implementing GLView
@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint shaderProgramObject;

    enum
    {
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0
    };
    
    GLuint vao;
    GLuint vbo_position;
    GLuint vbo_color;
    GLuint mvpMatrixUniform;

    mat4 perspectiveProjectionMatrix;
}

- (id) initWithFrame : (NSRect) frame // frame or rect aapla naav aahe
{
    // code
    self = [super initWithFrame : frame];
    
    if (self)
    {
        // code
      
      // Step 1 : initialize the array of OpenGL Pixel Format Attributes (PFA)
        NSOpenGLPixelFormatAttribute openGLPixelFormarAttributes[] =
      {
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
        NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFAColorSize, 24,
          NSOpenGLPFADepthSize, 24,
          NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFANoRecovery,
        NSOpenGLPFAAccelerated,
        NSOpenGLPFADoubleBuffer,
            0
      };

      // Step 2 : Create OpenGL pixel format using above attributes
      NSOpenGLPixelFormat *glPixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes : openGLPixelFormarAttributes]autorelease];
      
      if (glPixelFormat == nil)
      {
        fprintf(gpFile, "failed to create glPixelFormat\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 3 : Create OpenGL context using above pixel format
      NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat : glPixelFormat shareContext : nil]autorelease];
    
      if (glContext == nil)
      {
        fprintf(gpFile, "failed to create glContext\n");
        [self uninitialize];
        [self release];
        [NSApp terminate : self];
      }

        // Step 4 : Set this view's pixel format by using above pixel format
      [self setPixelFormat : glPixelFormat];

        // step 5 : Set view's context using above OpenGL context
      [self setOpenGLContext : glContext];
    }
    
    return(self);
}

// define getFrameForTime custom method with predefined signature
- (CVReturn) getFrameForTime : (const CVTimeStamp*) outputTime
{
    // code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];

    [pool release];
    return(kCVReturnSuccess);
}

- (void) prepareOpenGL // given by NSOpenGLView which overridable
{
    // code
    [super prepareOpenGL];

    // make the OpenGL context current
    [[self openGLContext]makeCurrentContext];

    // set double buffer swapping interval to 1
    GLint swapInterval = 1;

    [[self openGLContext]setValues : &swapInterval forParameter : NSOpenGLCPSwapInterval]; // CP - Context Parameter
    
    // OpenGL Log
    fprintf(gpFile, "OpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(gpFile, "OpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(gpFile, "OpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    // call our initialize
    [self initialize];

    // create, configure, start display link
    
    // Step 1- Create the display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);

    // Step 2 - Set the callback display link
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);

    // Step 3 - Create OpenGL pixel format to CGL pixel format
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj) [[self pixelFormat]CGLPixelFormatObj]; // Obj means pointer

    // Step 4 - Convert NSOpenGLContext to CGLContext
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj];

    // Step 5 - Using above info set current CGDisplay to CGLPixelFormat And Context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);

    // Step 6 - Start the display link
    CVDisplayLinkStart(displayLink);
}

// implementation of reshape which overridable
- (void) reshape
{
    // code
    [super reshape];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds]; // mala boundaries
    int width = rect.size.width;
    int height = rect.size.height;

    // call our resize
    [self resize : width : height];

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) drawRect : (NSRect) dirtyRect // drawRect ha view chya thread madhye kaam karto
{
    // code
    
    // To prevent flickering
    [self drawView];
}

- (void) drawView // navin thread sathi aahe - similar to drawRect
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // call our display here
    [self display];

    // do double buffering
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (int) initialize
{
    // code
    // vertex shader
        const GLchar* vertexShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "in vec4 a_position;" \
            "in vec4 a_color;" \
            "uniform mat4 u_mvpMatrix;" \
            "out vec4 a_color_out;" \
            "void main(void)" \
            "{" \
                "gl_Position = u_mvpMatrix * a_position;" \
                "a_color_out = a_color;" \
            "}";

        // StepC 2 : creating shading object
        GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(vertexShaderObject, 1, (const GLchar**)&vertexShaderSourceCode , NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe

        // stepC 4 : compile the shader
        glCompileShader(vertexShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        GLint status;
        GLint infoLogLength;
        char* log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
            
            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu
                    
                    // stepC 5 e : display the log
                    fprintf(gpFile, "Vertex Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepc 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // StepC 1 : writing shading code
        // fragment shader
        const GLchar* fragmentShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "in vec4 a_color_out;" \
            "out vec4 FragColor;" \
            "void main(void)" \
            "{" \
                "FragColor = a_color_out;" \
            "}";

        // StepC 2 : creating shading object
        GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        // stepC 3 : giving shader code to shader object
        glShaderSource(fragmentShaderObject, 1, (const GLchar**)&fragmentShaderSourceCode, NULL);// kontya shader object dyaycha aahe, kiti sting cha dyachaaahe shader cha array aahe(pn aapla 1 aahe),aapan tyala string dili, 3rd paramerter multiple array aasel tr NULL dene mhanje dili aahe ti string purna ghe
        
        // stepC 4 : compile the shader
        glCompileShader(fragmentShaderObject); // inline coplier ne comple hoto human understandable to gpu unserstandable

        // stepC 5 : error checking of shader compilation
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        // stepC 5 a : getting compilation status
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status); // konachya shader che,kashacha status pahije,Kasamade bharu

        // stepC 5 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepC 5 c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepC 5 d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log); // kontya shader chya complilation chi pishvi bharachi aahe,kiti lambi aahe tyachi,actully kiti bharli,kashamade bharu

                    // stepC 5 e : display the log
                    fprintf(gpFile, "Fragment Shader compilation log : %s \n", log);

                    // stepC 5 f : free the allocated buffer
                    free(log);

                    // stepC 5 g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        // Shader program object
        // stepD 1 : create shader program object
        shaderProgramObject = glCreateProgram();

        // stepD 2 : attach desired shaders to this shader program  object
        glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "a_position");
        // ----------
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "a_color");

        // stepD 3 : link shader program object
        glLinkProgram(shaderProgramObject);

        //stepD 4 : do link error checking with similar to a to g steps like above
        // reinitialization of this 3 veriables
        status = 0;
        infoLogLength = 0;
        log = NULL;

        //stepD 4 a : getting compilation status
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status); // konache linking status ghyache aahe,  , konamade bharu

        //stepD 4 b : getting length of log of compilation status
        if (status == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);

            // stepD 4c : allocate enough memory to the buffer to hold the compiletation log
            if (infoLogLength > 0)
            {
                log = (char*)malloc(infoLogLength);

                // stepD 4d : get the compilation log into this allocated buffer
                if (log != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);

                    // stepD 4e  : display the log
                    fprintf(gpFile, "Shader Program link log : %s \n", log);

                    // stepD 4f : free the allocated buffer
                    free(log);

                    // stepD 4g : exit the application due to error
                    [self uninitialize];
                    [self release];
                    [NSApp terminate : self];
                }
            }
        }

        mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

        // vao & vbo related code
        //declarations of vertex data arrays
        const GLfloat trianglePosition[] =
        {
            0.0f,1.0f,0.0f,
            -1.0,-1.f,0.0f,
            1.0f,-1.0f,0.0f
        };

        const GLfloat triangleColor[] =
        {
            1.0f, 0.0f, 0.0f, // Red
            0.0f, 1.0f, 0.0, // Green
            0.0f, 0.0f, 1.0f // Blue
        };

        // vao related code
        glGenVertexArrays(1, &vao); //vao:- vertex array object
        glBindVertexArray(vao);
        
        // vbo for position
        glGenBuffers(1, &vbo_position);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_position); //bind buffer

        glBufferData(GL_ARRAY_BUFFER, sizeof(trianglePosition), trianglePosition, GL_STATIC_DRAW); //glBufferData:-( aata maja data ya buffer made bhar target chya tonda ne bhar,kiti data bharaycha, data dya, GL_STATIC_DRAW:- aatach bhar data)

        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); // glVertexAttribPointer:-tu jo data buffer made bharlas target point cha vapar karun ya data kade openGL che engine kase pahu
        //glVertexAttribPointer(shader made mi 0th position la map karat aahe, yache tu bgtana 3-3 che set kr,kontya type ne bagu tr float ne patav(ekade floatch lagte),data bgtana kiti udya marun baghu, data patavnar aahe to normalized aahe ka :- nahi mahanun false,udya marachya nahit(data saral detoy),udya aastil tr position ky aasel(pn udya nahit tr NULL))

        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //glEnableVertexAttribArray:-vertex chya attribute cha array enable kr

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer

        // vbo for color
        glGenBuffers(1, &vbo_color);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color); //bind buffer for color
        glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind buffer for color

        glBindVertexArray(0);

        // here start OpenGL code (depth & clear color code)
        //clear the screen using blue color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        //depth related changes
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix = mat4::identity();

    return(0);
}

- (void) resize : (int) width : (int) height
{
    // code
    if (height < 0)
    {
        height = 1;
    }

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

- (void) display
{
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);

        // transformations
        mat4 translationMatrix = mat4::identity();
        mat4 modelViewMatrix = mat4::identity();
        mat4 modelViewProjectionMatrix = mat4::identity();

        translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f); // or only translate() - glTranslate is replaced by this line
        modelViewMatrix = translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        
        glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        glBindVertexArray(vao);

        // stepE 2 : draw the desiered graphics/animation
        // here will be magic code

        glDrawArrays(GL_TRIANGLES, 0, 3);

        glBindVertexArray(0);

        // stepE 3 : unuse the shader program object
        
        glUseProgram(0);
}

- (void) myupdate // NSOpenGLView madhye tyachi swatchi update aahe, jar update lihla aaplya function cha ki te override kelya sarkha hoil ani je update aahe NSOpenGLView Madhye te update animation saathi nahi karat, NSWindow La move or resize sathi hi update aahe
{
    // code
}

- (void) uninitialize
{
    // code
    if (vbo_color)
        {
            glDeleteBuffers(1, &vbo_color);
            vbo_color = 0;
        }

        // deletion/ uninitialization of vbo
        if (vbo_position)
        {
            glDeleteBuffers(1, &vbo_position);
            vbo_position = 0;
        }

        // deletion/ uninitialization of vao
        if (vao)
        {
            glDeleteVertexArrays(1, &vao);
            vao = 0;
        }

    // shader uninitialization
    if (shaderProgramObject)
    {
        // stepF 0
        glUseProgram(shaderProgramObject);

        GLsizei numAttachedShaders;
            
        // stepF 1 : get the no. of attached shaders
        glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);

        GLuint* shaderObjects = NULL;

        // stepF 2 : create empty buffer to hold array of  attached shader objects
        shaderObjects = (GLuint*)malloc(numAttachedShaders * sizeof(GLuint));

        // stepF 3 : allocate enough memory to hold array of attached shader objects
        glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);

        // stepF 4 : as no. of attached shaders can be more than one start a loop & inside that loop detach shader & delete the shader one by one & finish the loop
        for (GLsizei i = 0; i < numAttachedShaders; i++)
        {
            glDetachShader(shaderProgramObject, shaderObjects[i]);
            glDeleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }

        // stepF 5 : free the memory allocated for the buffer
        free(shaderObjects);

        // stepF 6 : unuse the shader program object
        glUseProgram(0);

        // stepF 7 : delete the shader program object
        glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
    }
}

- (BOOL) acceptsFirstResponder // onTouchEvent is anologous to acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder : self]; // window is a method which is not related to window variable in AppDelegate
    
    return(YES);
}

- (void) keyDown : (NSEvent*) event
{
    // code
    int key = (int) [[event characters] characterAtIndex : 0]; // character is method to get index
    
    switch(key)
    {
        case 27:
            [self uninitialize];
            [self release];
            [NSApp terminate : self]; // self is MyView
            break;
            
        case 'F':
        case 'f':
            [[self window]toggleFullScreen : self];
            break;
            
        default:
            break;
    }
}

- (void) mouseDown : (NSEvent*) event
{
    // code
    
}

- (void) dealloc
{
    // code
    [super dealloc];

    if (displayLink)
    {
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    displayLink = nil;
    }
}
@end

// implement the display link callback function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* currentTime, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* view)
{
    // code
    CVReturn result = [(GLView*)view getFrameForTime : outputTime];

    return(result);
}

// compile -
// clang -o Window Window.m -framework Cocoa

// execute -
// ./Window
