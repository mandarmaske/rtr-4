package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

// matrix package
import android.opengl.Matrix;

// texture related
import android.graphics.BitmapFactory;

import android.graphics.Bitmap;

import android.opengl.GLUtils;


public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	private int vao_pyramid[]=new int[1];
	private int vbo_pyramid[]=new int[1];
	private int vbo_pyramidtexcoord[]=new int[1];

	private int vao_cube[]=new int[1];
	private int vbo_cube[]=new int[1];
	private int vbo_cubetexcoord[]=new int[1];

	private int texture_stone[]=new int[1];
	private int texture_kundali[]=new int[1];

	private int textureSamplerUniform;

	private int mvpMatrixUniform; // hyacha address pass karaycha naahi mhanun array bracket nahi

	private float anglePyramid;
	private float angleCube;

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	private int loadGLTexture(int imageResourceId)
	{
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inScaled = false;

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageResourceId, options);

		int texture [] = new int[1];
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		return texture[0];
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 a_position;" +
		"in vec2 a_texcoord;" +
		"uniform mat4 u_mvpMatrix;" +
		"out vec2 a_texcoord_out;" +
		"void main(void)" +
		"{" +
			"gl_Position = u_mvpMatrix * a_position;" +
			"a_texcoord_out = a_texcoord;" +
		"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 a_texcoord_out;" +
		"uniform highp sampler2D u_textureSampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = texture(u_textureSampler, a_texcoord_out);" +
		"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		mvpMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_textureSampler");

		// vao & vbo related code
		//declarations of vertex data arrays
		final float pyramidVertices[] = new float[]
		{
			// front
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// right
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// back
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			// left
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		final float pyramidTexcoord[] = new float[]
		{
			0.5f, 1.0f, // front-top
			0.0f, 0.0f, // front-left
			1.0f, 0.0f, // front-right

			0.5f, 1.0f, // right-top
			1.0f, 0.0f, // right-left
			0.0f, 0.0f, // right-right

			0.5f, 1.0f, // back-top
			1.0f, 0.0f, // back-left
			0.0f, 0.0f, // back-right

			0.5f, 1.0f, // left-top
			0.0f, 0.0f, // left-left
			1.0f, 0.0f, // left-right
		};

		final float cubeVertices[] = new float[]
		{
			// top
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			// bottom
			1.0f, -1.0f, -1.0f,
		   -1.0f, -1.0f, -1.0f,
		   -1.0f, -1.0f,  1.0f,
			1.0f, -1.0f,  1.0f,

			// front
			1.0f, 1.0f, 1.0f,
		   -1.0f, 1.0f, 1.0f,
		   -1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// back
			1.0f, 1.0f, -1.0f,
		   -1.0f, 1.0f, -1.0f,
		   -1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			// right
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// left
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
		};

		final float cubeTexcoords[] = new float[]
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
		};

		// vao and vbo related code
		
		// pyramid
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);	
		GLES32.glBindVertexArray(vao_pyramid[0]);


		// position
		GLES32.glGenBuffers(1, vbo_pyramid, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4); // byte madhil size
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer = byteBuffer.asFloatBuffer(); 
		positionBuffer.put(pyramidVertices);
		positionBuffer.position(0); 

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		// texcoord

		GLES32.glGenBuffers(1, vbo_pyramidtexcoord, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramidtexcoord[0]);
		ByteBuffer pbyteBuffer = ByteBuffer.allocateDirect(pyramidTexcoord.length * 4); // byte madhil size
		pbyteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer pPositionBuffer = pbyteBuffer.asFloatBuffer(); 
		pPositionBuffer.put(pyramidTexcoord);
		pPositionBuffer.position(0); 

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidTexcoord.length * 4, pPositionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		// cube
		GLES32.glGenVertexArrays(1, vao_cube,0);
		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glGenBuffers(1, vbo_cube,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_cube[0]);

		GLES32.glGenBuffers(1, vbo_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube[0]);

		ByteBuffer cubeByteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4); // byte madhil size
		cubeByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cubePositionBuffer = cubeByteBuffer.asFloatBuffer(); 
		cubePositionBuffer.put(cubeVertices);
		cubePositionBuffer.position(0); 
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, cubePositionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// texcoord

		GLES32.glGenBuffers(1, vbo_cubetexcoord, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cubetexcoord[0]);

		ByteBuffer cbyteBuffer = ByteBuffer.allocateDirect(cubeTexcoords.length * 4); // byte madhil size
		cbyteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cpositionBuffer = cbyteBuffer.asFloatBuffer(); 
		cpositionBuffer.put(cubeTexcoords);
		cpositionBuffer.position(0); 
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexcoords.length * 4, cpositionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glEnable(GLES32.GL_TEXTURE_2D);

		texture_stone[0]=loadGLTexture(R.raw.stone);
		texture_kundali[0]=loadGLTexture(R.raw.vijay_kundali);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		// pyramid
		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float rotationMatrix[]=new float[16];
		Matrix.setIdentityM(rotationMatrix,0);

		float modelViewMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewMatrix,0);

		float modelViewProjectionMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(translationMatrix, 0, -2.0f, 0.0f, -5.2f);
		Matrix.setRotateM(rotationMatrix, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
		
		//modelViewMatrix = translationMatrix;
		Matrix.multiplyMM(modelViewMatrix,0,translationMatrix,0,rotationMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix,0);

		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_stone[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glBindVertexArray(vao_pyramid[0]);

		// stepE 2 : draw the desiered graphics/animation
		// here will be magic code

		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		// cube
		Matrix.setIdentityM(translationMatrix,0);

		float rotationMatrix_X[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_X,0);

		float rotationMatrix_Y[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Y,0);

		float rotationMatrix_Z[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Z,0);

		float scaleMatrix[]=new float[16];
		Matrix.setIdentityM(scaleMatrix,0);

		float tempMatrix[]=new float[16];
		Matrix.setIdentityM(tempMatrix,0);

		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(translationMatrix, 0, 2.0f, 0.0f, -5.2f);

		Matrix.setRotateM(rotationMatrix_X, 0, angleCube, 1.0f, 0.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Y, 0, angleCube, 0.0f, 1.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Z, 0, angleCube, 0.0f, 0.0f, 1.0f);

		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);

		Matrix.multiplyMM(rotationMatrix_X,0,rotationMatrix_Y,0,rotationMatrix_Z,0);

		Matrix.multiplyMM(tempMatrix,0,translationMatrix,0,rotationMatrix_X,0);

		Matrix.multiplyMM(modelViewMatrix,0,tempMatrix,0,scaleMatrix,0);

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix,0);
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glBindVertexArray(vao_cube[0]);

		// stepE 2 : draw the desiered graphics/animation
		// here will be magic code
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		// Here Will Be Drawing Code

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void update()
	{
		anglePyramid = anglePyramid + 1.0f;
		if (anglePyramid >= 360)
		{
			anglePyramid = anglePyramid - 360.0f;
		}

		angleCube = angleCube + 1.0f;
		if (angleCube >= 360)
		{
			angleCube = angleCube - 360.0f;
		}
	}

	private void uninitialize()
	{
		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		if(texture_stone[0]>0)
		{
			GLES32.glDeleteTextures(1, texture_stone, 0);
			texture_stone[0] = 0;
		}

		if(texture_kundali[0]>0)
		{
			GLES32.glDeleteTextures(1, texture_kundali, 0);
			texture_kundali[0] = 0;
		}

		// deletion / uninitialization of vbo
		
		if (vbo_pyramid[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid,0);
			vbo_pyramid[0] = 0;
		}

		if (vbo_pyramidtexcoord[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramidtexcoord,0);
			vbo_pyramidtexcoord[0] = 0;
		}

		if (vbo_cube[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_cube,0);
			vbo_cube[0] = 0;
		}

		if (vbo_cubetexcoord[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_cubetexcoord,0);
			vbo_cubetexcoord[0] = 0;
		}

		// deletion / uninitialization of vao

		if (vao_cube[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube,0);
			vao_cube[0] = 0;
		}

		if (vao_pyramid[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid,0);
			vao_pyramid[0] = 0;
		}
	}
}
