package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

// matrix package
import android.opengl.Matrix;

// texture related
// import android.graphics.BitmapFactory; - ha package resource la laagto image (checkerboard) banvtana nahi 

import android.graphics.Bitmap;

import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	private int vao_square[] = new int[1];
	private int vbo_square_position[] = new int[1];
	private int vbo_square_texcoord[] = new int[1];

	private final int checkerboard_width = 64;
	private final int checkerboard_height = 64;
	byte checkerBoard[] = new byte[checkerboard_width * checkerboard_height * 4];
	private int texture_Checkerboard[] = new int[1];

	private int textureSamplerUniform;

	private int mvpMatrixUniform; // hyacha address pass karaycha naahi mhanun array bracket nahi

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 a_position;" +
		"in vec2 a_texcoord;" +
		"uniform mat4 u_mvpMatrix;" +
		"out vec2 a_texcoord_out;" +
		"void main(void)" +
		"{" +
			"gl_Position = u_mvpMatrix * a_position;" +
			"a_texcoord_out = a_texcoord;" +
		"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 a_texcoord_out;" +
		"uniform highp sampler2D u_textureSampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = texture(u_textureSampler, a_texcoord_out);" +
		"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		mvpMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_textureSampler");

		// vao & vbo related code
		//declarations of vertex data arrays

		final float texcoords[] = new float[]
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
		};

		// vao realted code
		GLES32.glGenVertexArrays(1, vao_square, 0);

		GLES32.glBindVertexArray(vao_square[0]);

		GLES32.glGenBuffers(1, vbo_square_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square_position[0]); //bind buffer

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 4 * 3 * 4, null, GLES32.GL_DYNAMIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// texcoord buffer
		GLES32.glGenBuffers(1, vbo_square_texcoord, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square_texcoord[0]); //bind buffer

		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(texcoords.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer1.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer texBuffer = byteBuffer1.asFloatBuffer(); // Byte Buffer to float buffer
		texBuffer.put(texcoords); // float buffer dila tyaat kai bharu tar triangleVertices
		texBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, texcoords.length * 4, texBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, 2, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glEnable(GLES32.GL_TEXTURE_2D);
		texture_Checkerboard[0] = loadGLTexture();

		GLES32.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void make_chekerboard()
	{
		// variable declarations
		int c;

		// code
		for (int i = 0; i < checkerboard_height; i++)
		{
			for (int j = 0; j < checkerboard_width; j++)
			{
				c = ((i & 8) ^ (j & 8)) * 255;

				checkerBoard[(i * 64 + j) * 4 + 0] = (byte)c;
				checkerBoard[(i * 64 + j) * 4 + 1] = (byte)c;
				checkerBoard[(i * 64 + j) * 4 + 2] = (byte)c;
				checkerBoard[(i * 64 + j) * 4 + 3] = (byte)255;
			}
		}
	}

	private int loadGLTexture()
	{
		make_chekerboard();

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(checkerboard_width * checkerboard_height * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		byteBuffer.put(checkerBoard);
		byteBuffer.position(0);

		Bitmap bitmap = Bitmap.createBitmap(checkerboard_width, checkerboard_height, Bitmap.Config.ARGB_8888);
		bitmap.copyPixelsFromBuffer(byteBuffer);

		int texture[] = new int[1];

		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_REPEAT);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_REPEAT);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_NEAREST);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_NEAREST);

		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		//GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		return texture[0];
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		float translationMatrix[] = new float[16];
		Matrix.setIdentityM(translationMatrix, 0);

		float modelViewMatrix[] = new float[16];
		Matrix.setIdentityM(modelViewMatrix, 0);

		float modelViewProjectionMatrix[] = new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		// transformations
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -5.2f);
		modelViewMatrix = translationMatrix;

		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glActiveTexture(GLES32.GL_TEXTURE0); 
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_Checkerboard[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glBindVertexArray(vao_square[0]);

		float position[] = new float[12];

		position[0] = 0.0f;
		position[1] = 1.0f;
		position[2] = 0.0f;
		position[3] = -2.0f;
		position[4] = 1.0f;
		position[5] = 0.0f;
		position[6] = -2.0f;
		position[7] = -1.0f;
		position[8] = 0.0f;
		position[9] = 0.0f;
		position[10] = -1.0f;
		position[11] = 0.0f;

		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(position.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer1.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer = byteBuffer1.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer.put(position); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, position.length * 4, positionBuffer, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);

		position[0] = 2.41421f;
		position[1] = 1.0f;
		position[2] = -1.41421f;
		position[3] = 1.0f;
		position[4] = 1.0f;
		position[5] = 0.0f;
		position[6] = 1.0f;
		position[7] = -1.0f;
		position[8] = 0.0f;
		position[9] = 2.41421f;
		position[10] = -1.0f;
		position[11] = -1.41421f;

		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(position.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer2.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer1 = byteBuffer2.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer1.put(position); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer1.position(0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square_position[0]);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, position.length * 4, positionBuffer1, GLES32.GL_DYNAMIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);

		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		// Here Will Be Drawing Code

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void uninitialize()
	{
		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		if (texture_Checkerboard[0] > 1)
		{
			GLES32.glDeleteTextures(1, texture_Checkerboard, 0);
			texture_Checkerboard[0] = 0;
		}

		// deletion/ uninitialization of vbo
		if (vbo_square_texcoord[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_square_texcoord, 0);
			vbo_square_texcoord[0] = 0;
		}

		if (vbo_square_position[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_square_position, 0);
			vbo_square_position[0] = 0;
		}

		// deletion/ uninitialization of vao
		if (vao_square[0] > 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_square, 0);
			vao_square[0] = 0;
		}
	}
}
