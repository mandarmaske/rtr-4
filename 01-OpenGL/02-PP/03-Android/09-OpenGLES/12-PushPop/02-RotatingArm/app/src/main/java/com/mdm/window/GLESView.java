package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import java.nio.ShortBuffer;

// matrix package
import android.opengl.Matrix;
import java.util.*;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	// sphere variables
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

	private int numVertices;
	private int numElements;

	private int modelMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;

	//Lights
	private int laUniform; //light amibiet
	private int ldUniform; //Diffuse
	private int lsUniform; //Specular
	private int lightPositionUniform;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShininessUniform;

	private int lightingEnabledUniform;

	private float lightAmbient[]={0.1f,0.1f,0.1f,1.0f};
	private float lightDiffuse[]={1.0f,1.0f,1.0f,1.0f};
	private float lightSpecular[]={ 1.0f,1.0f,1.0f,1.0f };
	private float lightPosition[]={ 0.0f,0.0f,100.0f,1.0f };

	private float materialAmbient[]={ 0.0f,0.0f,0.0f,1.0f };
	private float materialDiffuse[]={ 1.0f,1.0f,0.0f,1.0f };
	private float materialSpecular[]={ 0.7f,0.7f,0.7f,1.0f};
	private float materialShininess=128.0f;

	private int doubleTap = 0;
	private int singleTap = 0;

	private int elbow = 0;
	private int shoulder = 0;
	private int palm = 0;

	int gnumVertices;
	int gnumElements;

	private float perspectiveProjectionMatrix[] = new float[16]; 

	private class Stack
	{
		private float[] arr[];
		private int top;
		private int limit;
		
		Stack()
		{
			limit = 32;
			arr = new float[32][];
			top = -1;
		}

		Stack(int size)
		{
			limit = size;
			arr = new float[size][];
			top = -1;
		}

		void push(float[] value)
		{
			if(isFull())
			{
				System.out.println("MDM- Stack limit exceeded");
				uninitialize();
				System.exit(0);
			}
			else
			{
				top++;
				arr[top] = value;
			}
		}

		float[] pop()
		{
				if(isEmpty())
				{
					System.out.println("MDM- Stack is empty");
					uninitialize();
					System.exit(0);
				}
				top--;
				return arr[top + 1];

		}

		boolean isEmpty()
		{
			return top == limit -1; 
		}
		boolean isFull ()
		{
			return top == limit - 1;

		}
	}

	Stack stack = new Stack();

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap>1)
		{
			doubleTap=0;
		}
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap>3)
		{
			singleTap=0;
		}
		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 a_position;" +
			"in vec3 a_normal;" +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform vec4 u_lightPosition;" +
			"uniform mediump int u_lightingEnabled;" +
			"out vec3 transformedNormals;" +
			"out vec3 lightDirection;" +
			"out vec3 viewerVector;" +
			"void main(void)" +
			"{" +
			"if(u_lightingEnabled==1)" +
			"{" +
			"vec4 eyeCoordinates = u_viewMatrix*u_modelMatrix*a_position;" +
			"mat3 normalMatrix=mat3(u_viewMatrix*u_modelMatrix);" +
			"transformedNormals=normalMatrix*a_normal;" +
			"lightDirection=vec3(u_lightPosition)-eyeCoordinates.xyz;" +
			"viewerVector=-eyeCoordinates.xyz;" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
			"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"in vec3 transformedNormals;" +
			"in vec3 lightDirection;" +
			"in vec3 viewerVector;" +
			"uniform vec3 u_la;" +
			"uniform vec3 u_ld;" +
			"uniform vec3 u_ls;" +
			"uniform vec3 u_ka;" +
			"uniform vec3 u_kd;" +
			"uniform vec3 u_ks;" +
			"uniform float u_materialShininess;" +
			"uniform int u_lightingEnabled;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"vec3 phong_ads_light;" +
			"if(u_lightingEnabled==1)" +
			"{" +
			"vec3 ambient=u_la*u_ka;" +
			"vec3 normalised_transformed_normals=normalize(transformedNormals);" +
			"vec3 normalised_light_direction=normalize(lightDirection);" +
			"vec3 diffuse=u_ld*u_kd*max(dot(normalised_light_direction,normalised_transformed_normals),0.0);" +
			"vec3 reflectionVector=reflect(-normalised_light_direction,normalised_transformed_normals);" +
			"vec3 normalised_viewerVector=normalize(viewerVector);" +
			"vec3 specular=u_ls*u_ks*pow(max(dot(reflectionVector,normalised_viewerVector),0.0),u_materialShininess);" +
			"phong_ads_light=ambient+diffuse+specular;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_light=vec3(1.0,1.0,1.0);" +
			"}" +
			"FragColor = vec4(phong_ads_light,1.0);" +
			"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

		laUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		ldUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		lsUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");

		lightPositionUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition");
		kaUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShininessUniform=GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");

		lightingEnabledUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		gnumVertices = sphere.getNumberOfSphereVertices();
        gnumElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		doubleTap=0;
        singleTap=0;

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);
		//Here there drawing code
		// Shoulder
		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float scaleMatrix[]=new float[16];
		Matrix.setIdentityM(scaleMatrix,0);

		float rotationMatrix[]=new float[16];
		Matrix.setIdentityM(rotationMatrix,0);

		float previousMatrix[]=new float[16];
		Matrix.setIdentityM(previousMatrix,0);

		float modelMatrix[]=new float[16];
		Matrix.setIdentityM(modelMatrix,0);

		float viewMatrix[]=new float[16];
		Matrix.setIdentityM(viewMatrix,0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -6.0f);
		modelMatrix = translationMatrix.clone();
        stack.push(modelMatrix.clone());
		Matrix.setRotateM(rotationMatrix, 0,(float)shoulder, 0.0f, 0.0f, 1.0f);
		Matrix.translateM(translationMatrix, 0, 1.0f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrix,0);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translationMatrix,0);
		
		stack.push(modelMatrix.clone());
		Matrix.scaleM(scaleMatrix, 0, 2.0f, 0.5f, 1.0f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,scaleMatrix,0);

		//Matrix.multiplyMM(modelMatrix,0,translationMatrix,0,rotationMatrix,0);
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

		GLES32.glUniform1i(lightingEnabledUniform,1);
		GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
		GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
		GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);
		GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition,0);
		GLES32.glUniform3fv(kaUniform, 1, materialAmbient,0);
		GLES32.glUniform3fv(kdUniform, 1, materialDiffuse,0);
		GLES32.glUniform3fv(ksUniform, 1, materialSpecular,0);
		GLES32.glUniform1f(materialShininessUniform, materialShininess);

		GLES32.glBindVertexArray(vao_sphere[0]);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, gnumElements, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);

		// for forearm
		modelMatrix = stack.pop();
		Matrix.setIdentityM(translationMatrix,0);
		
		Matrix.translateM(translationMatrix, 0, 0.1f, 0.0f, 0.0f);
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setRotateM(rotationMatrix, 0,(float)elbow, 0.0f, 0.0f, 1.0f);
		
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translationMatrix,0);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrix,0);
		
		Matrix.setIdentityM(translationMatrix,0);
		Matrix.translateM(translationMatrix,0,1.0f,0.0f,0.0f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translationMatrix,0);
		
		stack.push(modelMatrix.clone());

		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.scaleM(scaleMatrix,0,scaleMatrix,0,2.0f,0.5f,0.5f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,scaleMatrix,0);
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

		GLES32.glUniform1i(lightingEnabledUniform,1);
		GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
		GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
		GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);
		GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition,0);
		GLES32.glUniform3fv(kaUniform, 1, materialAmbient,0);
		GLES32.glUniform3fv(kdUniform, 1, materialDiffuse,0);
		GLES32.glUniform3fv(ksUniform, 1, materialSpecular,0);
		GLES32.glUniform1f(materialShininessUniform, materialShininess);

		GLES32.glBindVertexArray(vao_sphere[0]);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, gnumElements, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);

		// for palm
		modelMatrix = stack.pop();

		Matrix.setIdentityM(translationMatrix,0);
		Matrix.translateM(translationMatrix,0,1.0f,0.0f,0.0f);
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setRotateM(rotationMatrix,0,(float)palm , 0.0f,0.0f,1.0f);
		
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translationMatrix,0);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrix,0);
		
		Matrix.setIdentityM(translationMatrix,0);
		Matrix.translateM(translationMatrix,0,0.5f,0.0f,0.0f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translationMatrix,0);

		stack.push(modelMatrix.clone());

		Matrix.setIdentityM(scaleMatrix ,0);
		Matrix.scaleM(scaleMatrix,0,scaleMatrix,0,1.0f,0.5f,0.5f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,scaleMatrix,0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

		GLES32.glUniform1i(lightingEnabledUniform,1);
		GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
		GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
		GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);
		GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition,0);
		GLES32.glUniform3fv(kaUniform, 1, materialAmbient,0);
		GLES32.glUniform3fv(kdUniform, 1, materialDiffuse,0);
		GLES32.glUniform3fv(ksUniform, 1, materialSpecular,0);
		GLES32.glUniform1f(materialShininessUniform, materialShininess);

		GLES32.glBindVertexArray(vao_sphere[0]);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, gnumElements, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);
	

		modelMatrix= stack.pop();
		modelMatrix= stack.pop();

		GLES32.glUseProgram(0);
		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void update()
	{
		if(singleTap==1)
		{
			shoulder = (shoulder + 3) % 360;
		}
		else if(singleTap==2)
		{
			elbow = (elbow + 3) % 360;
		}
		else if(singleTap==3)
		{
			palm = (palm + 3) % 360;
		}
		else
		{
			shoulder=0;
			elbow=0;
			palm=0;
		}	
	}

	private void uninitialize()
	{
		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		// deletion/ uninitialization of vao & vbo
		if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
	}
}
