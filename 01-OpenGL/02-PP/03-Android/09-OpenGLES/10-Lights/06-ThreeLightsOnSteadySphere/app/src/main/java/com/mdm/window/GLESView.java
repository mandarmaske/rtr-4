package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import java.nio.ShortBuffer;

// matrix package
import android.opengl.Matrix;

// math 
import java.lang.Math;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject_PV;
	private int shaderProgramObject_PF;

	// sphere variables
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

	private int numVertices;
	private int numElements;

	private int modelMatrixUniform_PV;
	private int viewMatrixUniform_PV;
	private int projectionMatrixUniform_PV;

	private int modelMatrixUniform_PF;
	private int viewMatrixUniform_PF;
	private int projectionMatrixUniform_PF;

	//Lights
	private int laUniform_PV[] = new int[3];
	private int ldUniform_PV[] = new int[3];
	private int lsUniform_PV[] = new int[3];
	private int lightPositionUniform_PV[] = new int[3];
	private int lightingUniform_PV;

	private int kaUniform_PV;
	private int kdUniform_PV;
	private int ksUniform_PV;
	private int materialShininessUniform_PV;

	private int laUniform_PF[] = new int[3];
	private int ldUniform_PF[] = new int[3];
	private int lsUniform_PF[] = new int[3];
	private int lightPositionUniform_PF[] = new int[3];
	private int lightingUniform_PF;

	private int kaUniform_PF;
	private int kdUniform_PF;
	private int ksUniform_PF;
	private int materialShininessUniform_PF;

	private int lightingEnabledUniform_PV;
	private int lightingEnabledUniform_PF;

	private class Light
	{
		float lightAmbient[]=new float[4];
		float lightDiffuse[]=new float[4];
		float lightSpecular[]=new float[4];
		float lightPosition[]=new float[4];
	}

	private Light lights[] = new Light[3];

	private float materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float materialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float materialShininess = 128.0f;

	private int doubleTap = 0;
	private int singleTap = 0;

	private float lightAngleZero = 0.0f;

	boolean bLight = false;

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		// code
		doubleTap++;
		if(doubleTap>1)
		{
			bLight=true;
			doubleTap=0;
		}
		else
		{
			bLight=false;
		}

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;

		if(singleTap>1)
		{
			singleTap=0;
		}

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader pervertex
		final String vertexShaderSourceCode_PV=String.format
		(
			"#version 320 es" +
            "\n" +
            "precision highp float;" +
            "in vec4 a_position;" +
			"in vec3 a_normal;" +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform vec3 u_la[3];" +
			"uniform vec3 u_ld[3];" +
			"uniform vec3 u_ls[3];" +
			"uniform vec4 u_lightPosition[3];" +
			"uniform vec3 u_ka;" +
			"uniform vec3 u_kd;" +
			"uniform vec3 u_ks;" +
			"uniform float u_materialShininess;" +
			"uniform mediump int u_lightingEnabled;" +
			"out vec3 phong_ads_light;" +
			"void main(void)" +
			"{" +
			"if(u_lightingEnabled == 1)" +
			"{" +
			"vec3 ambient[3];" +
			"vec3 lightDirection[3];" +
			"vec3 diffuse[3];" +
			"vec3 reflectionVector[3];" +
			"vec3 specular[3];" +
			"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
			"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
			"vec3 transformedNormals = normalize(normalMatrix * a_normal);" +
			"vec3 viewerVector = normalize(-eyeCoordinates.xyz);" +
			"for(int i = 0; i < 3; i++)" +
			"{"+
			"ambient[i] = u_la[i] * u_ka;" +
			"lightDirection[i] = normalize(vec3(u_lightPosition[i]) - eyeCoordinates.xyz);" +
			"diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformedNormals), 0.0);" +
			"reflectionVector[i] = reflect(-lightDirection[i], transformedNormals);" +
			"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewerVector), 0.0), u_materialShininess);" +
			"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
			"}" +
			"}" +
			"else" +
			"{" +
			"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
			"}"
			);

		int vertexShaderObject_PV=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject_PV, vertexShaderSourceCode_PV);
		GLES32.glCompileShader(vertexShaderObject_PV);

		int status[]=new int[1];
		int infoLogLength[]=new int[1];
		String log=null;

		GLES32.glGetShaderiv(vertexShaderObject_PV, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject_PV, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(vertexShaderObject_PV);
				System.out.println("vab: Vertex Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment shader pervertex
		final String fragmentShaderSourceCode_PV=String.format
		(
			"#version 320 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 phong_ads_light;" +
            "out vec4 FragColor;" +
			"void main(void)" +
			"{" +
				"FragColor = vec4(phong_ads_light,1.0);" +
			"}"
		);

		int fragmentShaderObject_PV=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject_PV, fragmentShaderSourceCode_PV);
		GLES32.glCompileShader(fragmentShaderObject_PV);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(fragmentShaderObject_PV, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject_PV, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(fragmentShaderObject_PV);
				System.out.println("vab: Fragment Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		// shader Program
		shaderProgramObject_PV=GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject_PV, vertexShaderObject_PV); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject_PV, fragmentShaderObject_PV);
		
		// prelinkg
		GLES32.glBindAttribLocation(shaderProgramObject_PV, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject_PV, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		GLES32.glLinkProgram(shaderProgramObject_PV);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetProgramInfoLog(shaderProgramObject_PV);
				System.out.println("MDM- PV Shader Program Link log: "+log);
				uninitialize();
				System.exit(0);
			}
		}
		//Post Linking
		modelMatrixUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
		viewMatrixUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_viewMatrix");
		projectionMatrixUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_projectionMatrix");

		laUniform_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_la[0]");
		ldUniform_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ld[0]");
		lsUniform_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ls[0]");
		lightPositionUniform_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_lightPosition[0]");

		laUniform_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_la[1]");
		ldUniform_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ld[1]");
		lsUniform_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ls[1]");
		lightPositionUniform_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_lightPosition[1]");

		laUniform_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_la[2]");
		ldUniform_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ld[2]");
		lsUniform_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ls[2]");
		lightPositionUniform_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_lightPosition[2]");

		kaUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ka");
		kdUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_kd");
		ksUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_ks");
		materialShininessUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_materialShininess");

		lightingEnabledUniform_PV = GLES32.glGetUniformLocation(shaderProgramObject_PV, "u_lightingEnabled");


		// vertex shader perfragment
		final String vertexShaderSourceCode_PF=String.format
		(
				"#version 320 es" +
				"\n" +
				"precision highp float;" +
				"in vec4 a_position;" +
				"in vec3 a_normal;" +
				"uniform mat4 u_modelMatrix;" +
				"uniform mat4 u_viewMatrix;" +
				"uniform mat4 u_projectionMatrix;" +
				"uniform vec4 u_lightPosition[3];" +
				"uniform mediump int u_lightingEnabled;" +
				"out vec3 transformedNormals;" +
				"out vec3 lightDirection[3];" +
				"out vec3 viewerVector;" +
				"void main(void)" +
				"{" +
				"if(u_lightingEnabled == 1)" +
				"{" +
				"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
				"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
				"transformedNormals = normalMatrix * a_normal;" +
				"viewerVector = -eyeCoordinates.xyz;" +
				"for(int i = 0; i < 3; i++)" +
				"{" +
				"lightDirection[i] = vec3(u_lightPosition[i]) - eyeCoordinates.xyz;" +
				"}" +
				"}" +
				"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
				"}"
		);

		int vertexShaderObject_PF=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject_PF, vertexShaderSourceCode_PF);
		GLES32.glCompileShader(vertexShaderObject_PF);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(vertexShaderObject_PF, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject_PF, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(vertexShaderObject_PF);
				System.out.println("MDM- PV Vertex Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment shader perfragment
		final String fragmentShaderSourceCode_PF=String.format
		(
				"#version 320 es" +
				"\n" +
				"precision highp float;" +
				"in vec3 transformedNormals;" +
				"in vec3 lightDirection[3];" +
				"in vec3 viewerVector;" +
				"uniform vec3 u_la[3];" +
				"uniform vec3 u_ld[3];" +
				"uniform vec3 u_ls[3];" +
				"uniform vec3 u_ka;" +
				"uniform vec3 u_kd;" +
				"uniform vec3 u_ks;" +
				"uniform float u_materialShininess;" +
				"uniform mediump int u_lightingEnabled;" +
				"out vec4 FragColor;" +
				"void main(void)" +
				"{" +
				"vec3 phong_ads_light;" +
				"if(u_lightingEnabled == 1)" +
				"{" +
				"vec3 ambient[3];" +
				"vec3 diffuse[3];" +
				"vec3 reflectionVector[3];" +
				"vec3 specular[3];" +
				"vec3 normalized_transformed_normal;" +
				"vec3 normalized_lightDirection[3];" +
				"vec3 normalized_viewerVector;" +
				"normalized_transformed_normal = normalize(transformedNormals); " +
				"normalized_viewerVector = normalize(viewerVector);" +
				"for(int i = 0; i < 3; i++)" +
				"{" +
				"normalized_lightDirection[i] = normalize(lightDirection[i]);" +
				"ambient[i] = u_la[i] * u_ka;" +
				"diffuse[i] = u_ld[i] * u_kd * max(dot(normalized_lightDirection[i], normalized_transformed_normal), 0.0);" +
				"reflectionVector[i] = reflect(-normalized_lightDirection[i], normalized_transformed_normal);" +
				"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalized_viewerVector), 0.0), u_materialShininess);" +
				"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
				"}" +
				"}" +
				"else" +
				"{" +
				"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
				"}" +
				"FragColor = vec4(phong_ads_light,1.0);" +
				"}"
		);

		int fragmentShaderObject_PF=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject_PF, fragmentShaderSourceCode_PF);
		GLES32.glCompileShader(fragmentShaderObject_PF);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(fragmentShaderObject_PF, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject_PF, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(fragmentShaderObject_PF);
				System.out.println("MDM- PF Fragment Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		// shader Program
		shaderProgramObject_PF=GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject_PF, vertexShaderObject_PF); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject_PF, fragmentShaderObject_PF);
		
		// prelinkg
		GLES32.glBindAttribLocation(shaderProgramObject_PF, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject_PF, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		GLES32.glLinkProgram(shaderProgramObject_PF);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetProgramiv(shaderProgramObject_PF, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject_PF, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetProgramInfoLog(shaderProgramObject_PF);
				System.out.println("MDM- PF Shader Program Link log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Post Linking
		modelMatrixUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
		viewMatrixUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_viewMatrix");
		projectionMatrixUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_projectionMatrix");

		laUniform_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_la[0]");
		ldUniform_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ld[0]");
		lsUniform_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ls[0]");
		lightPositionUniform_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition[0]");

		laUniform_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_la[1]");
		ldUniform_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ld[1]");
		lsUniform_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ls[1]");
		lightPositionUniform_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition[1]");

		laUniform_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_la[2]");
		ldUniform_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ld[2]");
		lsUniform_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ls[2]");
		lightPositionUniform_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_lightPosition[2]");

		kaUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ka");
		kdUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_kd");
		ksUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_ks");
		materialShininessUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_materialShininess");

		lightingEnabledUniform_PF = GLES32.glGetUniformLocation(shaderProgramObject_PF, "u_lightingEnabled");

		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

		lights[0] = new Light();
        lights[1] = new Light();
        lights[2] = new Light();

		// light 1
		lights[0].lightAmbient[0] = 0.0f; 
		lights[0].lightAmbient[1] = 0.0f; 
		lights[0].lightAmbient[2] = 0.0f; 
		lights[0].lightAmbient[3] = 1.0f; 

		lights[0].lightDiffuse[0] = 1.0f; // Red Light
		lights[0].lightDiffuse[1] = 0.0f; 
		lights[0].lightDiffuse[2] = 0.0f; 
		lights[0].lightDiffuse[3] = 1.0f; 

		lights[0].lightSpecular[0] = 1.0f;
		lights[0].lightSpecular[1] = 0.0f;
		lights[0].lightSpecular[2] = 0.0f;
		lights[0].lightSpecular[3] = 1.0f;

		// light 2
		lights[1].lightAmbient[0] = 0.0f; 
		lights[1].lightAmbient[1] = 0.0f; 
		lights[1].lightAmbient[2] = 0.0f; 
		lights[1].lightAmbient[3] = 1.0f; 

		lights[1].lightDiffuse[0] = 0.0f; 
		lights[1].lightDiffuse[1] = 1.0f; // Green Light
		lights[1].lightDiffuse[2] = 0.0f; 
		lights[1].lightDiffuse[3] = 1.0f; 

		lights[1].lightSpecular[0] = 0.0f;
		lights[1].lightSpecular[1] = 0.0f;
		lights[1].lightSpecular[2] = 1.0f;
		lights[1].lightSpecular[3] = 1.0f;

		

		// light 3
		lights[2].lightAmbient[0] = 0.0f; 
		lights[2].lightAmbient[1] = 0.0f; 
		lights[2].lightAmbient[2] = 0.0f; 
		lights[2].lightAmbient[3] = 1.0f; 

		lights[2].lightDiffuse[0] = 0.0f; 
		lights[2].lightDiffuse[1] = 0.0f; 
		lights[2].lightDiffuse[2] = 1.0f; // Blue Light
		lights[2].lightDiffuse[3] = 1.0f; 

		lights[2].lightSpecular[0] = 0.0f;
		lights[2].lightSpecular[1] = 0.0f;
		lights[2].lightSpecular[2] = 1.0f;
		lights[2].lightSpecular[3] = 1.0f;

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		// lights directions
		// light 1 - x direction
		lights[0].lightPosition[0] = 0.0f; 
		lights[0].lightPosition[1] = 2 * (float) Math.sin(lightAngleZero);
		lights[0].lightPosition[2] = 2 * (float) Math.cos(lightAngleZero); 
		lights[0].lightPosition[3] = 1.0f;
		
		// light 2 - y direction
		lights[1].lightPosition[0] = 2 * (float) Math.sin(lightAngleZero); 
		lights[1].lightPosition[1] = 0.0f; 
		lights[1].lightPosition[2] = 2 *(float) Math.cos(lightAngleZero); 
		lights[1].lightPosition[3] = 1.0f; 

		// light 2 - z direction
		lights[2].lightPosition[0] = 2 *(float) Math.sin(lightAngleZero); 
		lights[2].lightPosition[1] = 2 *(float) Math.cos(lightAngleZero); 
		lights[2].lightPosition[2] = 0.0f; 
		lights[2].lightPosition[3] = 1.0f; 

		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		if (singleTap == 0)
		{
			GLES32.glUseProgram(shaderProgramObject_PV);
		}
		else if (singleTap == 1)
		{
			GLES32.glUseProgram(shaderProgramObject_PF);
			//GLES32.glUseProgram(shaderProgramObject_PF);
		}
		//Here there drawing code

		// cube

		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float modelMatrix[]=new float[16];
		Matrix.setIdentityM(modelMatrix,0);

		float viewMatrix[]=new float[16];
		Matrix.setIdentityM(viewMatrix,0);

		float projectionMatrix[]=new float[16];
		Matrix.setIdentityM(projectionMatrix,0);

		float modelViewProjectionMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.0f);
		modelMatrix = translationMatrix;

		//Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelMatrix,0);

		GLES32.glBindVertexArray(vao_sphere[0]);

		if (singleTap == 0)
		{
			GLES32.glUniformMatrix4fv(modelMatrixUniform_PV, 1, false, modelMatrix,0); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
			GLES32.glUniformMatrix4fv(viewMatrixUniform_PV, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform_PV, 1, false, perspectiveProjectionMatrix,0);
		
			for (int i = 0; i < 3; i++)
			{
				GLES32.glUniform3fv(laUniform_PV[i], 1, lights[i].lightAmbient, 0);
				GLES32.glUniform3fv(ldUniform_PV[i], 1, lights[i].lightDiffuse, 0);
				GLES32.glUniform3fv(lsUniform_PV[i], 1, lights[i].lightSpecular, 0);
				GLES32.glUniform4fv(lightPositionUniform_PV[i], 1, lights[i].lightPosition, 0);
			}

			GLES32.glUniform3fv(kaUniform_PV, 1, materialAmbient,0);
			GLES32.glUniform3fv(kdUniform_PV, 1, materialDiffuse,0);
			GLES32.glUniform3fv(ksUniform_PV, 1, materialSpecular,0);
			GLES32.glUniform1f(materialShininessUniform_PV, materialShininess);

			if (bLight == true)
			{
				GLES32.glUniform1i(lightingEnabledUniform_PV, 1);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform_PV, 0);
			}
		}
		else if (singleTap == 1)
		{
			GLES32.glUniformMatrix4fv(modelMatrixUniform_PF, 1, false, modelMatrix,0); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
			GLES32.glUniformMatrix4fv(viewMatrixUniform_PF, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform_PF, 1, false, perspectiveProjectionMatrix,0);

			for (int i = 0; i < 3; i++)
			{
				GLES32.glUniform3fv(laUniform_PF[i], 1, lights[i].lightAmbient, 0);
				GLES32.glUniform3fv(ldUniform_PF[i], 1, lights[i].lightDiffuse, 0);
				GLES32.glUniform3fv(lsUniform_PF[i], 1, lights[i].lightSpecular, 0);
				GLES32.glUniform4fv(lightPositionUniform_PF[i], 1, lights[i].lightPosition, 0);
			}

			GLES32.glUniform3fv(kaUniform_PF, 1, materialAmbient,0);
			GLES32.glUniform3fv(kdUniform_PF, 1, materialDiffuse,0);
			GLES32.glUniform3fv(ksUniform_PF, 1, materialSpecular,0);
			GLES32.glUniform1f(materialShininessUniform_PF, materialShininess);

			if (bLight == true)
			{
				GLES32.glUniform1i(lightingEnabledUniform_PF, 1);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform_PF, 0);
			}
		}

		 // bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void update()
	{
		lightAngleZero = lightAngleZero + 0.05f;

		if (lightAngleZero >= 360.0f)
			lightAngleZero = lightAngleZero - 360.0f;
	}

	private void uninitialize()
	{
		// code
		// pervertex
		if(shaderProgramObject_PV > 0)
		{
			GLES32.glUseProgram(shaderProgramObject_PV);
			int retVal[]=new int[1];
			GLES32.glGetProgramiv(shaderProgramObject_PV, GLES32.GL_ATTACHED_SHADERS, retVal,0);
			if(retVal[0] > 0)
			{
				int numAttachedShaders=retVal[0];
				int shaderObjects[]=new int[numAttachedShaders];
				GLES32.glGetAttachedShaders(shaderProgramObject_PV, numAttachedShaders, retVal,0, shaderObjects,0);

				for(int i=0; i<numAttachedShaders;i++)
				{
					GLES32.glDetachShader(shaderProgramObject_PV, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}
			GLES32.glUseProgram(0);
			GLES32.glDeleteProgram(shaderProgramObject_PV);
			shaderProgramObject_PV = 0;
		}

		// perfragment
		if(shaderProgramObject_PF > 0)
		{
			GLES32.glUseProgram(shaderProgramObject_PF);
			int retVal[]=new int[1];
			GLES32.glGetProgramiv(shaderProgramObject_PF, GLES32.GL_ATTACHED_SHADERS, retVal,0);
			if(retVal[0] > 0)
			{
				int numAttachedShaders=retVal[0];
				int shaderObjects[]=new int[numAttachedShaders];
				GLES32.glGetAttachedShaders(shaderProgramObject_PF, numAttachedShaders, retVal,0, shaderObjects,0);

				for(int i=0; i<numAttachedShaders;i++)
				{
					GLES32.glDetachShader(shaderProgramObject_PF, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}
			GLES32.glUseProgram(0);
			GLES32.glDeleteProgram(shaderProgramObject_PF);
			shaderProgramObject_PF = 0;
		}

		// deletion/ uninitialization of vao & vbo
		if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
	}
}
