package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import java.nio.ShortBuffer;

// matrix package
import android.opengl.Matrix;

// math 
import java.lang.Math;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	// sphere variables
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

	private int modelMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;

	private float angleForXRotation = 0.0f;
	private float angleForYRotation = 0.0f;
	private float angleForZRotation = 0.0f;

	private int globalWidth;
	private int globalHeight;

	private int numVertices;
	private int numElements;

	//Lights
	private int laUniform;
	private int ldUniform;
	private int lsUniform;
	private int lightPositionUniform;

	private int lightingEnabledUniform;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShininessUniform;

	// lighting related arrays
	private float lightAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
	private float lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
	private float lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
	private float lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

	private float materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float materialDiffuse[] = { 0.5f,0.2f,0.7f,1.0f };
	private float materialSpecular[] = { 0.7f,0.7f,0.7f,1.0f };
	private float materialShininess = 128.0f;

	private int doubleTap = 0;
	private int singleTap = 0;

	boolean bLight = false;

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
		globalWidth = width;
		globalHeight = height;
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		// code
		doubleTap++;
		if(doubleTap>1)
		{
			doubleTap = 0;
			singleTap = 0;
		}

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;

		if(singleTap > 3)
		{
			singleTap = 0;
		}

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	// custom private functions
	private void initialize()
	{
		// code
		// vertex shader
		final String vertexShaderSourceCode=String.format
		(
				"#version 320 es" +
				"\n" +
				"in vec4 a_position;" +
				"in vec3 a_normal;" +
				"uniform mat4 u_modelMatrix;" +
				"uniform mat4 u_viewMatrix;" +
				"uniform mat4 u_projectionMatrix;" +
				"uniform vec4 u_lightPosition;" +
				"uniform mediump int u_lightingEnable;" +
				"out vec3 transformedNormals;" +
				"out vec3 lightDirection;" +
				"out vec3 viewerVector;" +
				"void main(void)" +
				"{" +
					"if(u_lightingEnable==1)" +
					"{" +
						"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
						"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
						"transformedNormals = normalMatrix * a_normal;" +
						"lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" +
						"viewerVector = -eyeCoordinates.xyz;" +
					"}" +
					"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
				"}"
		);

		int vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[]=new int[1];
		int infoLogLength[]=new int[1];
		String log=null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("MDM- Vertex Shader Compilation log: " + log);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment shader
		final String fragmentShaderSourceCode=String.format
		(
				"#version 320 es" +
				"\n" +
				"precision highp float;" +
				"in vec3 transformedNormals;" +
				"in vec3 lightDirection;" +
				"in vec3 viewerVector;" +
				"uniform vec3 u_la;" +
				"uniform vec3 u_ld;" +
				"uniform vec3 u_ls;" +
				"uniform vec3 u_ka;" +
				"uniform vec3 u_kd;" +
				"uniform vec3 u_ks;" +
				"uniform float u_materialShininess;" +
				"uniform mediump int u_lightingEnable;" +
				"out vec4 FragColor;" +
				"void main(void)" +
				"{" +
					"vec3 phong_ads_light;" +
					"if(u_lightingEnable==1)" +
					"{" +
						"vec3 ambient = u_la * u_ka;" +
						"vec3 normalized_transformedNormals = normalize(transformedNormals);" +
						"vec3 normalized_lightDirection = normalize(lightDirection);" +
						"vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" +
						"vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" +
						"vec3 normalized_viewerVector = normalize(viewerVector);" +
						"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" +
						"phong_ads_light = ambient + diffuse + specular;" +
					"}" +
					"else" +
					"{" +
						"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
					"}" +
					"FragColor = vec4(phong_ads_light,1.0);" +
				"}"
		);

		int fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("MDM- Fragment Shader Compilation log: " + log);
				uninitialize();
				System.exit(0);
			}
		}

		// shader Program
		shaderProgramObject=GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// prelinkg
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		GLES32.glLinkProgram(shaderProgramObject);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("MDM- Shader Program Link log: " + log);
				uninitialize();
				System.exit(0);
			}
		}
		//Post Linking
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition");

		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");

		lightingEnabledUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightingEnable");


		Sphere sphere=new Sphere();

        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];

		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		// vao and vbo related code
		
		// sphere
		GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);

		 GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);

		// genral OpenGL
		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		// enable culling
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE);

		GLES32.glClearColor(0.25f,0.25f,0.25f,1.0f);

		// initialization of projection matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float modelMatrix[]=new float[16];
		Matrix.setIdentityM(modelMatrix,0);

		float viewMatrix[]=new float[16];
		Matrix.setIdentityM(viewMatrix,0);

		float projectionMatrix[]=new float[16];
		Matrix.setIdentityM(projectionMatrix,0);

		float modelViewProjectionMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.0f);
		modelMatrix = translationMatrix;

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelMatrix,0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

		GLES32.glBindVertexArray(vao_sphere[0]);

		drawTwentyFourSpheres();
        
        // unbind vao
        GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void update()
	{
		if (singleTap == 1)
		{
			angleForXRotation = angleForXRotation + 0.05f;
			if (angleForXRotation >= 360.0f)
				angleForXRotation = angleForXRotation - 360.0f;
		}

		else if (singleTap == 2)
		{
			angleForYRotation = angleForYRotation + 0.05f;
			if (angleForYRotation >= 360.0f)
				angleForYRotation = angleForYRotation - (360.0f);
		}

		else if (singleTap == 3)
		{
			angleForZRotation = angleForZRotation + 0.05f;
			if (angleForZRotation >= 360.0f)
				angleForZRotation = angleForZRotation - 360.0f;
		}	
	}

	private void drawTwentyFourSpheres()
	{
			// ***** 1st sphere on 1st column, emerald *****
			float materialAmbient1[]={ 0.0215f, 0.1745f, 0.0215f, 1.0f };
			float materialDiffuse1[]={ 0.07568f, 0.61424f, 0.07568f, 1.0f };
			float materialSpecular1[]={ 0.633f, 0.727811f, 0.633f, 1.0f };
			float materialShininess1=76.8f;

			float lightPositionX[]={0.0f, 80*(float) Math.sin(angleForXRotation),80*(float) Math.cos(angleForXRotation),0.0f};
			float lightPositionY[]={80*(float) Math.sin(angleForYRotation), 0.0f,80*(float) Math.cos(angleForYRotation),0.0f};
			float lightPositionZ[]={80*(float) Math.sin(angleForZRotation), 80*(float) Math.cos(angleForZRotation),0.0f,0.0f};

			float translationMatrix[]=new float[16];

			Matrix.setIdentityM(translationMatrix,0);

			float modelMatrix[]=new float[16];
			Matrix.setIdentityM(modelMatrix,0);

			float viewMatrix[]=new float[16];
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 1.0f, -6.0f);

			modelMatrix = translationMatrix;
			//Matrix.multiplyMM(modelMatrix,0,translationMatrix,0,rotationMatrix,0);

			GLES32.glViewport(0, globalHeight/2, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap == 1)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}
				GLES32.glUniform3fv(kaUniform, 1, materialAmbient1,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse1,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular1,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess1);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 2nd sphere on 1st column, jade *****
			float materialAmbient2[]={ 0.135f, 0.2225f, 0.1575f, 1.0f };
			float materialDiffuse2[]={ 0.54f, 0.89f, 0.63f, 1.0f };
			float materialSpecular2[]={ 0.316228f, 0.316228f, 0.316228f, 1.0f };
			float materialShininess2=38.4f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;
		
			GLES32.glViewport(0, globalHeight/3, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient2,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse2,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular2,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess2);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 3rd sphere on 1st column, obsidian *****
			float materialAmbient3[]={ 0.05375f, 0.05f, 0.06625f, 1.0f };
			float materialDiffuse3[]={ 0.18275f, 0.17f, 0.22525f, 1.0f };
			float materialSpecular3[]={0.332741f, 0.328634f, 0.346435f, 1.0f };
			float materialShininess3=38.4f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 1.4f, -6.0f);

			modelMatrix = translationMatrix;
		
			GLES32.glViewport(0, globalHeight/6, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient3,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse3,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular3,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess3);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 4th sphere on 1st column, pearl *****
			float materialAmbient4[]={ 0.25f, 0.20725f, 0.20725f, 1.0f };
			float materialDiffuse4[]={ 1.0f, 0.829f, 0.829f, 1.0f };
			float materialSpecular4[]={0.296648f, 0.296648f, 0.296648f, 1.0f };
			float materialShininess4=11.264f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(0, globalHeight/26, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient4,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse4,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular4,0);
				GLES32.glUniform1f(materialShininessUniform, materialShininess4);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 5th sphere on 1st column, ruby *****
			float materialAmbient5[]={ 0.1745f, 0.01175f, 0.01175f, 1.0f };
			float materialDiffuse5[]={ 0.61424f, 0.04136f, 0.04136f, 1.0f };
			float materialSpecular5[]={0.727811f, 0.626959f, 0.626959f, 1.0f };
			float materialShininess5=76.6f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 0.0f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(0, globalHeight/50, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient5,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse5,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular5,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess5);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 6th sphere on 1st column, turquoise *****
			float materialAmbient6[]={ 0.1f, 0.18725f, 0.1745f, 1.0f };
			float materialDiffuse6[]={ 0.396f, 0.74151f, 0.69102f, 1.0f };
			float materialSpecular6[]={0.297254f, 0.30829f, 0.306678f, 1.0f };
			float materialShininess6=12.8f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);
			Matrix.translateM(translationMatrix, 0, -1.0f, -1.3f, -6.0f);

			modelMatrix = translationMatrix;

			GLES32.glViewport(0, globalHeight/70, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient6,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse6,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular6,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess6);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);


			// ***** 1st sphere on 2nd column, brass *****
			float materialAmbient7[]={ 0.329412f, 0.223529f, 0.027451f, 1.0f };
			float materialDiffuse7[]={ 0.780392f, 0.568627f, 0.113725f, 1.0f };
			float materialSpecular7[]={ 0.992157f, 0.941176f, 0.807843f, 1.0f };
			float materialShininess7=76.8f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);
			Matrix.translateM(translationMatrix, 0, -1.0f, 1.0f, -6.0f);

			modelMatrix = translationMatrix;
		
			GLES32.glViewport(globalWidth/5, globalHeight/2, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap == 1)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient7,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse7,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular7,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess7);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 2nd sphere on 2nd column, bronze *****
			float materialAmbient8[]={ 0.2125f, 0.1275f, 0.054f, 1.0f };
			float materialDiffuse8[]={ 0.714f, 0.4284f, 0.18144f, 1.0f };
			float materialSpecular8[]={ 0.393548f, 0.271906f, 0.166721f, 1.0f };
			float materialShininess8=25.6f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;

			GLES32.glViewport(globalWidth/5, globalHeight/3, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient8,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse8,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular8,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess8);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 3rd sphere on 2nd column, chrome *****
			float materialAmbient9[]={ 0.25f, 0.25f, 0.25f, 1.0f };
			float materialDiffuse9[]={ 0.4f, 0.4f, 0.4f, 1.0f };
			float materialSpecular9[]={0.774597f, 0.774597f, 0.774597f, 1.0f };
			float materialShininess9=76.8f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 1.4f, -6.0f);

			modelMatrix = translationMatrix;
		
			GLES32.glViewport(globalWidth/5, globalHeight/6, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient9,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse9,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular9,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess9);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 4th sphere on 2nd column, copper *****
			float materialAmbient10[]={ 0.19125f, 0.0735f, 0.0225f, 1.0f };
			float materialDiffuse10[]={ 0.7038f, 0.27048f, 0.0828f, 1.0f };
			float materialSpecular10[]={0.256777f, 0.137622f, 0.086014f, 1.0f };
			float materialShininess10=12.8f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/5, globalHeight/26, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient10,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse10,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular10,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess10);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 5th sphere on 2nd column, gold *****
			float materialAmbient11[]={ 0.24725f, 0.1995f, 0.0745f, 1.0f };
			float materialDiffuse11[]={ 0.75164f, 0.60648f, 0.22648f, 1.0f };
			float materialSpecular11[]={0.628281f, 0.555802f, 0.366065f, 1.0f };
			float materialShininess11=51.2f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, 0.0f, -6.0f);

			modelMatrix = translationMatrix;
	
			GLES32.glViewport(globalWidth/5, globalHeight/50, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient11,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse11,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular11,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess11);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 6th sphere on 2nd column, silver *****
			float materialAmbient12[]={ 0.19225f, 0.19225f, 0.19225f, 1.0f };
			float materialDiffuse12[]={ 0.50754f, 0.50754f, 0.50754f, 1.0f };
			float materialSpecular12[]={0.508273f, 0.508273f, 0.508273f, 1.0f };
			float materialShininess12=51.2f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, -1.0f, -1.3f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/5, globalHeight/70, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient12,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse12,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular12,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess12);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 1st sphere on 3rd column, black *****
			float materialAmbient13[]={ 0.0f, 0.0f, 0.0f, 1.0f };
			float materialDiffuse13[]={ 0.01f, 0.01f, 0.01f, 1.0f };
			float materialSpecular13[]={ 0.50f, 0.50f, 0.50f, 1.0f };
			float materialShininess13=32f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.0f, 1.0f, -6.0f);

			modelMatrix = translationMatrix;
	
			GLES32.glViewport(globalWidth/3, globalHeight/2, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient13,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse13,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular13,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess13);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 2nd sphere on 3rd column, cyan *****
			float materialAmbient14[]={ 0.0f, 0.1f, 0.06f, 1.0f };
			float materialDiffuse14[]={ 0.0f, 0.50980392f, 0.50980392f, 1.0f };
			float materialSpecular14[]={0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
			float materialShininess14=32.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.0f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/3, globalHeight/3, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient14,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse14,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular14,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess14);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 3rd sphere on 3nd column, green *****
			float materialAmbient15[]={ 0.0f, 0.0f, 0.0f, 1.0f };
			float materialDiffuse15[]={ 0.1f, 0.35f, 0.1f, 1.0f };
			float materialSpecular15[]={0.45f, 0.55f, 0.45f, 1.0f };
			float materialShininess15=32.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.0f, 1.4f, -6.0f);

			modelMatrix = translationMatrix;

			GLES32.glViewport(globalWidth/3, globalHeight/6, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient15,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse15,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular15,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess15);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 4th sphere on 3rd column, red *****
			float materialAmbient16[]={ 0.0f, 0.0f, 0.0f, 1.0f };
			float materialDiffuse16[]={ 0.5f, 0.0f, 0.0f, 1.0f };
			float materialSpecular16[]={0.7f, 0.6f, 0.6f, 1.0f };
			float materialShininess16=32f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.0f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;
		
			GLES32.glViewport(globalWidth/3, globalHeight/26, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient16,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse16,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular16,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess16);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 5th sphere on 3rd column, white *****
			float materialAmbient17[]={ 0.0f, 0.0f, 0.0f, 1.0f };
			float materialDiffuse17[]={ 0.55f, 0.55f, 0.55f, 1.0f };
			float materialSpecular17[]={0.70f, 0.70f, 0.70f, 1.0f };
			float materialShininess17=32.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/3, globalHeight/50, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient17,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse17,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular17,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess17);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 6th sphere on 3rd column, yellow plastic *****
			float materialAmbient18[]={0.0f, 0.0f, 0.0f, 1.0f };
			float materialDiffuse18[]={ 0.5f, 0.5f, 0.0f, 1.0f };
			float materialSpecular18[]={0.60f, 0.60f, 0.50f, 1.0f };
			float materialShininess18=32.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.0f, -1.3f, -6.0f);

			modelMatrix = translationMatrix;

			GLES32.glViewport(globalWidth/3, globalHeight/70, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient18,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse18,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular18,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess18);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 1st sphere on 4th column, black *****
			float materialAmbient19[]={ 0.02f, 0.02f, 0.02f, 1.0f };
			float materialDiffuse19[]={ 0.01f, 0.01f, 0.01f, 1.0f };
			float materialSpecular19[]={ 0.4f, 0.4f, 0.4f, 1.0f };
			float materialShininess19=10f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.2f, 1.0f, -6.0f);

			modelMatrix = translationMatrix;

			GLES32.glViewport(globalWidth/2, globalHeight/2, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);
				
				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient19,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse19,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular19,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess19);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 2nd sphere on 4th column, cyan *****
			float materialAmbient20[]={ 0.0f, 0.05f, 0.05f, 1.0f };
			float materialDiffuse20[]={ 0.4f, 0.5f, 0.5f, 1.0f };
			float materialSpecular20[]={0.04f, 0.7f, 0.7f, 1.0f };
			float materialShininess20=10.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.2f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;
		
			GLES32.glViewport(globalWidth/2, globalHeight/3, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient20,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse20,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular20,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess20);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 3rd sphere on 4th column, green *****
			float materialAmbient21[]={ 0.0f, 0.05f, 0.0f, 1.0f };
			float materialDiffuse21[]={ 0.4f, 0.5f, 0.4f, 1.0f };
			float materialSpecular21[]={0.04f, 0.7f, 0.04f, 1.0f };
			float materialShininess21=10.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.2f, 1.4f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/2, globalHeight/6, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient21,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse21,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular21,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess21);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 4th sphere on 4th column, red *****
			float materialAmbient22[]={ 0.05f, 0.0f, 0.0f, 1.0f };
			float materialDiffuse22[]={ 0.5f, 0.4f, 0.4f, 1.0f };
			float materialSpecular22[]={0.7f, 0.04f, 0.04f, 1.0f };
			float materialShininess22=10f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.2f, 1.2f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/2, globalHeight/26, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}
				GLES32.glUniform3fv(kaUniform, 1, materialAmbient22,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse22,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular22,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess22);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 5th sphere on 4th column, white *****
			float materialAmbient23[]={ 0.05f, 0.05f, 0.05f, 1.0f };
			float materialDiffuse23[]={ 0.5f, 0.5f, 0.5f, 1.0f };
			float materialSpecular23[]={0.7f, 0.7f, 0.7f, 1.0f };
			float materialShininess23=10.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.2f, 0.0f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/2, globalHeight/50, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient23,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse23,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular23,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess23);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// ***** 6th sphere on 4th column, yellow rubber *****
			float materialAmbient24[]={0.05f, 0.05f, 0.0f, 1.0f };
			float materialDiffuse24[]={ 0.5f, 0.5f, 0.4f, 1.0f };
			float materialSpecular24[]={0.7f, 0.7f, 0.7f, 0.04f };
			float materialShininess24=10.0f;

			Matrix.setIdentityM(translationMatrix,0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(viewMatrix,0);

			Matrix.translateM(translationMatrix, 0, 0.2f, -1.3f, -6.0f);

			modelMatrix = translationMatrix;
			
			GLES32.glViewport(globalWidth/2, globalHeight/70, globalWidth /2, globalHeight / 2);
			GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
			GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

				GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
				GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);

				if (singleTap==1) 
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionX, 0);
				}
				else if(singleTap==2)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionY, 0);
				}
				else if(singleTap==3)
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPositionZ, 0);
				}
				else
				{
					GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
				}

				GLES32.glUniform3fv(kaUniform, 1, materialAmbient24,0);
				GLES32.glUniform3fv(kdUniform, 1, materialDiffuse24,0);
				GLES32.glUniform3fv(ksUniform, 1, materialSpecular24,0);

				GLES32.glUniform1f(materialShininessUniform, materialShininess24);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform, 0);
			}

			// stepE 2 : draw the desiered graphics/animation
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
	}

	private void uninitialize()
	{
		//Code

		if(shaderProgramObject > 0)
		{
			GLES32.glUseProgram(shaderProgramObject);
			int retVal[]=new int[1];
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal,0);
			if(retVal[0] > 0)
			{
				int numAttachedShaders=retVal[0];
				int shaderObjects[]=new int[numAttachedShaders];
				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal,0, shaderObjects,0);

				for(int i=0; i<numAttachedShaders;i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}
			GLES32.glUseProgram(0);
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		// deletion / uninitialization of vbo
		if (vbo_sphere_position[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_position,0);
			vbo_sphere_position[0] = 0;
		}

		 if(vbo_sphere_normal[0]>0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }

		if(vbo_sphere_element[0]>0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

		// deletion / uninitialization of vao

		if (vao_sphere[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, vao_sphere,0);
			vao_sphere[0] = 0;
		}
	}
}
