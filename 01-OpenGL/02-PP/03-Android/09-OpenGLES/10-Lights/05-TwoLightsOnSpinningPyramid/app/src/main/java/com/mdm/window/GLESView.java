package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

// matrix package
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	private int vao_pyramid[] = new int[1];
	private int vbo_pyramid_position[] = new int[1];
	private int vbo_pyramid_normals[]=new int[1];
    private int vbo_pyramid_color[]=new int[1];

	private int modelMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;
	
	private float angle = 0.0f;

	//Lights
	private int laUniform[]=new int[2];
	private int ldUniform[]=new int[2];
	private int lsUniform[]=new int[2];
	private int lightPositionUniform[]=new int[2];
	private int lightingEnabledUniform;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShininessUniform;

	private class Light
	{
		float lightAmbient[]=new float[4];
		float lightDiffuse[]=new float[4];
		float lightSpecular[]=new float[4];
		float lightPosition[]=new float[4];
	}

	private Light lights[] = new Light[2];

	private float materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
	private float materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
	private float materialShininess = 50.0f;

	private int doubleTap = 0;

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		// code
		doubleTap = doubleTap + 1;
		if(doubleTap > 1)
		{
			doubleTap = 0;
		}

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		// code

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 a_position;" +
			"in vec3 a_normal;" +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform vec4 u_lightPosition[2];" +
			"uniform mediump int u_lightingEnabled;" +
			"out vec3 transformedNormals;" +
			"out vec3 lightDirection[2];" +
			"out vec3 viewerVector;" +
			"void main(void)" +
			"{" +
				"if(u_lightingEnabled == 1)" +
				"{" +
					"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
					"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
					"transformedNormals = normalMatrix * a_normal;" +
					"viewerVector = -eyeCoordinates.xyz;" +
					"for(int i = 0; i < 2; i++)" +
					"{" +
						"lightDirection[i] = vec3(u_lightPosition[i]) - eyeCoordinates.xyz;" +
					"}" +
				"}" +
				"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
			"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec3 transformedNormals;" +
		"in vec3 lightDirection[2];" +
		"in vec3 viewerVector;" +
		"uniform vec3 u_la[2];" +
		"uniform vec3 u_ld[2];" +
		"uniform vec3 u_ls[2];" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShininess;" +
		"uniform mediump int u_lightingEnabled;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"vec3 phong_ads_light;" +
			"if(u_lightingEnabled == 1)" +
			"{" +
				"vec3 ambient[2];" +
				"vec3 diffuse[2];" +
				"vec3 reflectionVector[2];" +
				"vec3 specular[2];" +
				"vec3 normalized_transformed_normal;" +
				"vec3 normalized_lightDirection[2];" +
				"vec3 normalized_viewerVector;" +
				"normalized_transformed_normal = normalize(transformedNormals); " +
				"normalized_viewerVector = normalize(viewerVector);" +
				"for(int i = 0; i < 2; i++)" +
				"{" +
					"normalized_lightDirection[i] = normalize(lightDirection[i]);" +
					"ambient[i] = u_la[i] * u_ka;" +
					"diffuse[i] = u_ld[i] * u_kd * max(dot(normalized_lightDirection[i], normalized_transformed_normal), 0.0);" +
					"reflectionVector[i] = reflect(-normalized_lightDirection[i], normalized_transformed_normal);" +
					"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalized_viewerVector), 0.0), u_materialShininess);" +
					"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
				"}" +
			"}" +
			"else" +
			"{" +
				"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"FragColor = vec4(phong_ads_light,1.0);" +
		"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_COLOR, "a_color");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix");//Andhar
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

		// light uniform
		laUniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_la[0]");
		ldUniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld[0]");
		lsUniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls[0]");
		lightPositionUniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition[0]");

		laUniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_la[1]");
		ldUniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld[1]");
		lsUniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls[1]");
		lightPositionUniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition[1]");

		// material uniform
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");

		// light enable uniform
		lightingEnabledUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

		// vao & vbo related code
		//declarations of vertex data arrays
		final float pyramidPosition[] = new float[]
		{
			// front
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// right
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// back
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			// left
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		final float pyramidColor[] = new float[]
		{
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
		};

		final float pyramidNormals[] = new float[]
		{
			0.0f, 0.447214f, 0.894427f,// front-top
			0.0f, 0.447214f, 0.894427f,// front-left
			0.0f, 0.447214f, 0.894427f,// front-right

			0.894427f, 0.447214f, 0.0f, // right-top
			0.894427f, 0.447214f, 0.0f, // right-left
			0.894427f, 0.447214f, 0.0f, // right-right

			0.0f, 0.447214f, -0.894427f, // back-top
			0.0f, 0.447214f, -0.894427f, // back-left
			0.0f, 0.447214f, -0.894427f, // back-right

			-0.894427f, 0.447214f, 0.0f, // left-top
			-0.894427f, 0.447214f, 0.0f, // left-left
			-0.894427f, 0.447214f, 0.0f // left-right
		};

		// Cube
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);

		GLES32.glBindVertexArray(vao_pyramid[0]);

		GLES32.glGenBuffers(1, vbo_pyramid_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_position[0]); //bind buffer

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidPosition.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer pyramidPositionBuffer = byteBuffer.asFloatBuffer(); // Byte Buffer to float buffer
		pyramidPositionBuffer.put(pyramidPosition); // float buffer dila tyaat kai bharu tar triangleVertices
		pyramidPositionBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidPosition.length * 4, pyramidPositionBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// color
		GLES32.glGenBuffers(1, vbo_pyramid_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_pyramid_color[0]);

		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(pyramidColor.length * 4);
		byteBuffer1.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBuffer1.asFloatBuffer();
		colorBuffer.put(pyramidColor);
		colorBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidColor.length*4, colorBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//Normals

		GLES32.glGenBuffers(1, vbo_pyramid_normals,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_pyramid_normals[0]);

		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(pyramidNormals.length*4);
		byteBuffer2.order(ByteOrder.nativeOrder());
		FloatBuffer normalBuffer = byteBuffer2.asFloatBuffer();
		normalBuffer.put(pyramidNormals);
		normalBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidNormals.length*4, normalBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		lights[0]=new Light();

		lights[0].lightAmbient[0]=0.0f; 
		lights[0].lightAmbient[1]=0.0f; 
		lights[0].lightAmbient[2]=0.0f; 
		lights[0].lightAmbient[3]=1.0f;
		
		lights[0].lightDiffuse[0]=1.0f;
		lights[0].lightDiffuse[1]=0.0f;
		lights[0].lightDiffuse[2]=0.0f;
		lights[0].lightDiffuse[3]=1.0f;
		
		lights[0].lightSpecular[0]=1.0f;
		lights[0].lightSpecular[1]=0.0f;
		lights[0].lightSpecular[2]=0.0f;
		lights[0].lightSpecular[3]=1.0f;
		
		lights[0].lightPosition[0]=-2.0f;
		lights[0].lightPosition[1]=0.0f;
		lights[0].lightPosition[2]=0.0f;
		lights[0].lightPosition[3]=1.0f;

		lights[1]=new Light();

		lights[1].lightAmbient[0]=0.0f; 
		lights[1].lightAmbient[1]=0.0f; 
		lights[1].lightAmbient[2]=0.0f; 
		lights[1].lightAmbient[3]=1.0f;
		
		lights[1].lightDiffuse[0]=0.0f;
		lights[1].lightDiffuse[1]=0.0f;
		lights[1].lightDiffuse[2]=1.0f;
		lights[1].lightDiffuse[3]=1.0f;
		
		lights[1].lightSpecular[0]=0.0f;
		lights[1].lightSpecular[1]=0.0f;
		lights[1].lightSpecular[2]=1.0f;
		lights[1].lightSpecular[3]=1.0f;
		
		lights[1].lightPosition[0]=2.0f;
		lights[1].lightPosition[1]=0.0f;
		lights[1].lightPosition[2]=0.0f;
		lights[1].lightPosition[3]=1.0f;

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix, 0);

		float rotationMatrix[]=new float[16];
		Matrix.setIdentityM(rotationMatrix, 0);

		float modelMatrix[]=new float[16];
		Matrix.setIdentityM(modelMatrix, 0);

		float viewMatrix[]=new float[16];
		Matrix.setIdentityM(viewMatrix, 0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -4.0f);
		Matrix.setRotateM(rotationMatrix, 0, angle, 0.0f, 1.0f, 0.0f);

		Matrix.multiplyMM(modelMatrix, 0, translationMatrix, 0, rotationMatrix, 0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);

		//Sending Light related Uniforms

		if (doubleTap == 1)
		{
			GLES32.glUniform1i(lightingEnabledUniform, 1);
			for (int i = 0; i < 2; i++)
			{
				GLES32.glUniform3fv(laUniform[i], 1, lights[i].lightAmbient,0);
				GLES32.glUniform3fv(ldUniform[i], 1, lights[i].lightDiffuse,0);
				GLES32.glUniform3fv(lsUniform[i], 1, lights[i].lightSpecular,0);
				GLES32.glUniform4fv(lightPositionUniform[i], 1, lights[i].lightPosition,0);
			}

			GLES32.glUniform3fv(kaUniform, 1, materialAmbient,0);
			GLES32.glUniform3fv(kdUniform, 1, materialDiffuse,0);
			GLES32.glUniform3fv(ksUniform, 1, materialSpecular,0);

			GLES32.glUniform1f(materialShininessUniform, materialShininess);
		}
		else
		{
			GLES32.glUniform1i(lightingEnabledUniform, 0);
		}

		GLES32.glBindVertexArray(vao_pyramid[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		
		GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void update()
	{
		angle = angle + 0.5f;

		if (angle >= 360.0f)
		{
			angle = angle - 360.0f;
		}
	}

	private void uninitialize()
	{
		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		// deletion/ uninitialization of vbo

		if (vbo_pyramid_position[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_position, 0);
			vbo_pyramid_position[0] = 0;
		}

		if (vbo_pyramid_normals[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_normals,0);
			vbo_pyramid_normals[0] = 0;
		}
		if (vbo_pyramid_color[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_color,0);
			vbo_pyramid_color[0] = 0;
		}

		// deletion/ uninitialization of vao
		if (vao_pyramid[0] > 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
			vao_pyramid[0] = 0;
		}
	}
}
