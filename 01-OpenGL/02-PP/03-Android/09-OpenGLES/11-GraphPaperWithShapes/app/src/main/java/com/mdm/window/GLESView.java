package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import java.lang.Math;

// matrix package
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	// variables for X Axis Line
	private int vao_horizontalLine[] = new int[1];
	private int vbo_horizontalLine_position[] = new int[1];
	private int vbo_horizontalLine_color[] = new int[1]; 

	// variables for Y Axis Line
	private int vao_verticalLine[] = new int[1];
	private int vbo_verticalLine_position[] = new int[1];
	private int vbo_verticalLine_color[] = new int[1]; 

	// variables for Horizontal Graph Lines
	private int vao_graphHLines[] = new int[1];
	private int vbo_graphHLines_position[] = new int[1];
	private int vbo_graphHLines_color[] = new int[1]; 

	// variables for Triangle Lines
	private int vao_triangleLines[] = new int[1];
	private int vbo_triangleLines_position[] = new int[1];
	private int vbo_triangleLines_color[] = new int[1]; 

	// variables for Square Lines
	private int vao_squareLines[] = new int[1];
	private int vbo_squareLines_position[] = new int[1];
	private int vbo_squareLines_color[] = new int[1]; 

	// variables for Vertical Graph Lines
	private int vao_circleLines[] = new int[1];
	private int vbo_circleLines_position[] = new int[1];
	private int vbo_circleLines_color[] = new int[1]; 

	// variables for Vertical Graph Lines
	private int vao_graphVLines[] = new int[1];
	private int vbo_graphVLines_position[] = new int[1];

	private float base = 1.2f;
	private float height = 0.0f;
	private float base1 = base;

	private int singleTap = 0;
	private int doubleTap = 0;
	private int longPress = 0;

	boolean gFlag = false;
	boolean tFlag = false;
	boolean sFlag = false;
	boolean cFlag = false;

	private int mvpMatrixUniform; // hyacha address pass karaycha naahi mhanun array bracket nahi

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		
		if (doubleTap == 1)
			gFlag = true;

		if(doubleTap > 1)
		{
			doubleTap = 0;
			gFlag = false;
		}

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		if (longPress == 0)
		{
			if (singleTap == 1)
				tFlag = true;

			if (singleTap == 2)
				sFlag = true;

			if (singleTap == 3)
				 cFlag = true;

			if(singleTap != 4)
				singleTap++;
		}
		else if (longPress == 1)
		{
			if (singleTap == 1)
				tFlag = false;

			if (singleTap == 2)
				sFlag = false;

			if (singleTap == 3)
				 cFlag = false;

			if(singleTap != 0)
				singleTap--;
		}

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		longPress++;
		if(longPress > 1)
			longPress = 0;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 a_position;" +
		"in vec4 a_color;" +
		"uniform mat4 u_mvpMatrix;" +
		"out vec4 a_color_out;" +
		"void main(void)" +
		"{" +
			"gl_Position = u_mvpMatrix * a_position;" +
			"a_color_out = a_color;" +
		"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 a_color_out;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = a_color_out;" +
		"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_COLOR, "a_color");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		mvpMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

		// vao & vbo related code
		//declarations of vertex data arrays

		final float horizontalLine[] = new float[]
		{
			1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f
		};

		final float horizontalLineColor[] = new float[]
		{
			1.0f, 0.0f, 0.0f, 
			1.0f, 0.0f, 0.0f, 
			1.0f, 0.0f, 0.0f 
		};

		final float verticalLine[] = new float[]
		{
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};

		final float verticalLineColor[] = new float[]
		{
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f
		};

		final float graphHLines[] = new float[]
		{
			-1.0f, 1.0f, 0.0f, // 1
			1.0f, 1.0f, 0.0f,

			-1.0f, 0.95f, 0.0f, // 2
			1.0f, 0.95f, 0.0f,

			-1.0f, 0.90f, 0.0f, // 3
			1.0f, 0.90f, 0.0f,

			-1.0f, 0.85f, 0.0f, // 4
			1.0f, 0.85f, 0.0f,

			-1.0f, 0.80f, 0.0f, // 5
			1.0f, 0.80f, 0.0f,

			-1.0f, 0.75f, 0.0f, // 6
			1.0f, 0.75f, 0.0f,

			-1.0f, 0.70f, 0.0f, // 7
			1.0f, 0.70f, 0.0f,

			-1.0f, 0.65f, 0.0f, // 8
			1.0f, 0.65f, 0.0f,

			-1.0f, 0.60f, 0.0f, // 9
			1.0f, 0.60f, 0.0f,

			-1.0f, 0.55f, 0.0f, // 10
			1.0f, 0.55f, 0.0f,

			-1.0f, 0.50f, 0.0f, // 11
			1.0f, 0.50f, 0.0f,

			-1.0f, 0.45f, 0.0f, // 12
			1.0f, 0.45f, 0.0f,

			-1.0f, 0.40f, 0.0f, // 13
			1.0f, 0.40f, 0.0f,

			-1.0f, 0.35f, 0.0f, // 14
			1.0f, 0.35f, 0.0f,

			-1.0f, 0.30f, 0.0f, // 15
			1.0f, 0.30f, 0.0f,

			-1.0f, 0.25f, 0.0f, // 16
			1.0f, 0.25f, 0.0f,

			-1.0f, 0.20f, 0.0f, // 17
			1.0f, 0.20f, 0.0f,

			-1.0f, 0.15f, 0.0f, // 18
			1.0f, 0.15f, 0.0f,

			-1.0f, 0.10f, 0.0f, // 19
			1.0f, 0.10f, 0.0f,

			-1.0f, 0.05f, 0.0f, // 20
			1.0f, 0.05f, 0.0f,

			-1.0f, -1.0f, 0.0f, // 21
			1.0f, -1.0f, 0.0f,

			-1.0f, -0.95f, 0.0f, // 22
			1.0f, -0.95f, 0.0f,

			-1.0f, -0.90f, 0.0f, // 23
			1.0f, -0.90f, 0.0f,

			-1.0f, -0.85f, 0.0f, // 24
			1.0f, -0.85f, 0.0f,

			-1.0f, -0.80f, 0.0f, // 25
			1.0f, -0.80f, 0.0f,

			-1.0f, -0.75f, 0.0f, // 26
			1.0f, -0.75f, 0.0f,

			-1.0f, -0.70f, 0.0f, // 27
			1.0f, -0.70f, 0.0f,

			-1.0f, -0.65f, 0.0f, // 28
			1.0f, -0.65f, 0.0f,

			-1.0f, -0.60f, 0.0f, // 29
			1.0f, -0.60f, 0.0f,

			-1.0f, -0.55f, 0.0f, // 30
			1.0f, -0.55f, 0.0f,

			-1.0f, -0.50f, 0.0f, // 31
			1.0f, -0.50f, 0.0f,

			-1.0f, -0.45f, 0.0f, // 32
			1.0f, -0.45f, 0.0f,

			-1.0f, -0.40f, 0.0f, // 33
			1.0f, -0.40f, 0.0f,

			-1.0f, -0.35f, 0.0f, // 34
			1.0f, -0.35f, 0.0f,

			-1.0f, -0.30f, 0.0f, // 35
			1.0f, -0.30f, 0.0f,

			-1.0f, -0.25f, 0.0f, // 36
			1.0f, -0.25f, 0.0f,

			-1.0f, -0.20f, 0.0f, // 37
			1.0f, -0.20f, 0.0f,

			-1.0f, -0.15f, 0.0f, // 38
			1.0f, -0.15f, 0.0f,

			-1.0f, -0.10f, 0.0f, // 39
			1.0f, -0.10f, 0.0f,

			-1.0f, -0.05f, 0.0f, // 40
			1.0f, -0.05f, 0.0f,
		};

		final float graphVLines[] = new float[]
		{
			1.0f, 1.0f, 0.0f, // 1
			1.0f, -1.0f, 0.0f,

			0.95f, 1.0f, 0.0f, // 2
			0.95f, -1.0f, 0.0f,

			0.90f, 1.0f, 0.0f, // 3
			0.90f, -1.0f, 0.0f,

			0.85f, 1.0f, 0.0f, // 4
			0.85f, -1.0f, 0.0f,

			0.80f, 1.0f, 0.0f, // 5
			0.80f, -1.0f, 0.0f,

			0.75f, 1.0f, 0.0f, // 6
			0.75f, -1.0f, 0.0f,

			0.70f, 1.0f, 0.0f, // 7
			0.70f, -1.0f, 0.0f,

			0.65f, 1.0f,  0.0f, // 8
			0.65f, -1.0f, 0.0f,

			0.60f, 1.0f, 0.0f, // 9
			0.60f, -1.0f, 0.0f,

			0.55f, 1.0f, 0.0f, // 10
			0.55f, -1.0f, 0.0f,

			0.50f, 1.0f, 0.0f, // 11
			0.50f, -1.0f, 0.0f,

			0.45f, 1.0f, 0.0f, // 12
			0.45f, -1.0f, 0.0f,

			0.40f, 1.0f, 0.0f, // 13
			0.40f, -1.0f, 0.0f,

			0.35f, 1.0f, 0.0f, // 14
			0.35f, -1.0f, 0.0f,

			0.30f, 1.0f, 0.0f, // 15
			0.30f, -1.0f, 0.0f,

			0.25f, 1.0f, 0.0f, // 16
			0.25f, -1.0f, 0.0f,

			0.20f, 1.0f, 0.0f, // 17
			0.20f, -1.0f, 0.0f,

			0.15f, 1.0f, 0.0f, // 18
			0.15f, -1.0f, 0.0f,

			0.10f, 1.0f, 0.0f, // 19
			0.10f, -1.0f, 0.0f,

			0.05f, 1.0f, 0.0f, // 20
			0.05f, -1.0f, 0.0f,

			-1.0f, 1.0f, 0.0f, // 21
			-1.0f, -1.0f, 0.0f,

			-0.95f, 1.0f, 0.0f, // 22
			-0.95f, -1.0f, 0.0f,

			-0.90f, 1.0f, 0.0f, // 23
			-0.90f, -1.0f, 0.0f,

			-0.85f, 1.0f, 0.0f, // 24
			-0.85f, -1.0f, 0.0f,

			-0.80f, 1.0f, 0.0f, // 25
			-0.80f, -1.0f, 0.0f,

			-0.75f, 1.0f, 0.0f, // 26
			-0.75f, -1.0f, 0.0f,

			-0.70f, 1.0f, 0.0f, // 27
			-0.70f, -1.0f, 0.0f,

			-0.65f, 1.0f, 0.0f, // 28
			-0.65f, -1.0f, 0.0f,

			-0.60f, 1.0f, 0.0f, // 29
			-0.60f, -1.0f, 0.0f,

			-0.55f, 1.0f, 0.0f, // 30
			-0.55f, -1.0f, 0.0f,

			-0.50f, 1.0f, 0.0f, // 31
			-0.50f, -1.0f, 0.0f,

			-0.45f, 1.0f, 0.0f, // 32
			-0.45f, -1.0f, 0.0f,

			-0.40f, 1.0f, 0.0f, // 33
			-0.40f, -1.0f, 0.0f,

			-0.35f, 1.0f, 0.0f, // 34
			-0.35f, -1.0f, 0.0f,

			-0.30f, 1.0f, 0.0f, // 35
			-0.30f, -1.0f, 0.0f,

			-0.25f, 1.0f, 0.0f, // 36
			-0.25f, -1.0f, 0.0f,

			-0.20f, 1.0f, 0.0f, // 37
			-0.20f, -1.0f, 0.0f,

			-0.15f, 1.0f, 0.0f, // 38
			-0.15f, -1.0f, 0.0f,

			-0.10f, 1.0f, 0.0f, // 39
			-0.10f, -1.0f, 0.0f,

			-0.05f, 1.0f, 0.0f, // 40
			-0.05f, -1.0f, 0.0f
		};

		final float graphHVLinesColor[] = new float[]
		{
			0.0f, 0.0f, 1.0f, // 1
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 2
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 3
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 4
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 5
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 6
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 7
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 8
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 9
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 10
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 11
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 12
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 13
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 14
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 15
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 16
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 17
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 18
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 19
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 20
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 21
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 22
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 23
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 24
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 25
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 26
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 27
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 28
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 29
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 30
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 31
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 32
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 33
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 34
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 35
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 36
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 37
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 38
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 39
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f, // 40
			0.0f, 0.0f, 1.0f,
		};

		// Horizontal Line
		// vao realted code
		GLES32.glGenVertexArrays(1, vao_horizontalLine, 0);

		GLES32.glBindVertexArray(vao_horizontalLine[0]);

		GLES32.glGenBuffers(1, vbo_horizontalLine_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_horizontalLine_position[0]); //bind buffer

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(horizontalLine.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer1 = byteBuffer.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer1.put(horizontalLine); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer1.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, horizontalLine.length * 4, positionBuffer1, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_horizontalLine_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_horizontalLine_color[0]); //bind buffer

		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(horizontalLineColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer1.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer colorBuffer1 = byteBuffer1.asFloatBuffer(); // Byte Buffer to float buffer
		colorBuffer1.put(horizontalLineColor); // float buffer dila tyaat kai bharu tar triangleVertices
		colorBuffer1.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, horizontalLineColor.length * 4, colorBuffer1, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		// Vertical Line
		// vao realted code
		GLES32.glGenVertexArrays(1, vao_verticalLine, 0);

		GLES32.glBindVertexArray(vao_verticalLine[0]);

		GLES32.glGenBuffers(1, vbo_verticalLine_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_verticalLine_position[0]); //bind buffer

		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(verticalLine.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer2.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer2 = byteBuffer2.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer2.put(verticalLine); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer2.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, verticalLine.length * 4, positionBuffer2, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_verticalLine_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_verticalLine_color[0]); //bind buffer

		ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(verticalLineColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer3.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer colorBuffer2 = byteBuffer3.asFloatBuffer(); // Byte Buffer to float buffer
		colorBuffer2.put(verticalLineColor); // float buffer dila tyaat kai bharu tar triangleVertices
		colorBuffer2.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, verticalLineColor.length * 4, colorBuffer2, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		// Graph Horizontal Lines
		// vao realted code
		GLES32.glGenVertexArrays(1, vao_graphHLines, 0);

		GLES32.glBindVertexArray(vao_graphHLines[0]);

		GLES32.glGenBuffers(1, vbo_graphHLines_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graphHLines_position[0]); //bind buffer

		ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(graphHLines.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer4.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer3 = byteBuffer4.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer3.put(graphHLines); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer3.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphHLines.length * 4, positionBuffer3, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_graphHLines_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graphHLines_color[0]); //bind buffer

		ByteBuffer byteBuffer5 = ByteBuffer.allocateDirect(graphHVLinesColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer5.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer colorBuffer3 = byteBuffer5.asFloatBuffer(); // Byte Buffer to float buffer
		colorBuffer3.put(graphHVLinesColor); // float buffer dila tyaat kai bharu tar triangleVertices
		colorBuffer3.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphHVLinesColor.length * 4, colorBuffer3, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		// Graph Vertical Lines
		// vao realted code
		GLES32.glGenVertexArrays(1, vao_graphVLines, 0);

		GLES32.glBindVertexArray(vao_graphVLines[0]);

		GLES32.glGenBuffers(1, vbo_graphVLines_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graphVLines_position[0]); //bind buffer

		ByteBuffer byteBuffer6 = ByteBuffer.allocateDirect(graphVLines.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer6.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer4 = byteBuffer6.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer4.put(graphVLines); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer4.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphVLines.length * 4, positionBuffer4, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_graphHLines_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_graphHLines_color[0]); //bind buffer

		ByteBuffer byteBuffer7 = ByteBuffer.allocateDirect(graphHVLinesColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer7.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer colorBuffer4 = byteBuffer7.asFloatBuffer(); // Byte Buffer to float buffer
		colorBuffer4.put(graphHVLinesColor); // float buffer dila tyaat kai bharu tar triangleVertices
		colorBuffer4.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, graphHVLinesColor.length * 4, colorBuffer4, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		height = (float)((Math.sqrt(3)) * (base1 / 2));
		base = base / 2;
		height = height / 2;
		float median = (float)(base1 / Math.sqrt(3)) / -4;

		final float triangleLine[] = new float[]
		{
			0.0f, (height - median), 0.0f,
			-base, -height - median, 0.0f,
			-base, -height - median, 0.0f,
			base, -height - median, 0.0f,
			base, -height - median, 0.0f,
			0.0f, (height - median), 0.0f
		};

		final float triangleLineColor[] = new float[]
		{
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f
		};

		// Triangle Line
		// vao realted code
		GLES32.glGenVertexArrays(1, vao_triangleLines, 0);

		GLES32.glBindVertexArray(vao_triangleLines[0]);

		GLES32.glGenBuffers(1, vbo_triangleLines_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_triangleLines_position[0]); //bind buffer

		ByteBuffer byteBuffer8 = ByteBuffer.allocateDirect(triangleLine.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer8.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer5 = byteBuffer8.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer5.put(triangleLine); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer5.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleLine.length * 4, positionBuffer5, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_triangleLines_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_triangleLines_color[0]); //bind buffer

		ByteBuffer byteBuffer9 = ByteBuffer.allocateDirect(triangleLineColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer9.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer colorBuffer5 = byteBuffer9.asFloatBuffer(); // Byte Buffer to float buffer
		colorBuffer5.put(triangleLineColor); // float buffer dila tyaat kai bharu tar triangleVertices
		colorBuffer5.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleLineColor.length * 4, colorBuffer5, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		final float squareLine[] = new float[]
		{
			(float)(((base1 / Math.sqrt(3)) * 2) / 2), (float)(((base1 / Math.sqrt(3)) * 2) / 2), 0.0f,
			(float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), (float)(((base1 / Math.sqrt(3)) * 2) / 2), 0.0f,

			(float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), (float)(((base1 / Math.sqrt(3)) * 2) / 2), 0.0f,
			(float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), (float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0f,

			(float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), (float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0f,
			(float)(((base1 / Math.sqrt(3)) * 2) / 2), (float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0f,

			(float)(((base1 / Math.sqrt(3)) * 2) / 2), (float)(-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0f,
			(float)(((base1 / Math.sqrt(3)) * 2) / 2), (float)(((base1 / Math.sqrt(3)) * 2) / 2), 0.0f,
		};

		final float squareLineColor[] = new float[]
		{
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
		};

		// Square Line
		// vao realted code
		GLES32.glGenVertexArrays(1, vao_squareLines, 0);

		GLES32.glBindVertexArray(vao_squareLines[0]);

		GLES32.glGenBuffers(1, vbo_squareLines_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_squareLines_position[0]); //bind buffer

		ByteBuffer byteBuffer10 = ByteBuffer.allocateDirect(squareLine.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer10.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer6 = byteBuffer10.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer6.put(squareLine); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer6.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squareLine.length * 4, positionBuffer6, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_squareLines_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_squareLines_color[0]); //bind buffer

		ByteBuffer byteBuffer11 = ByteBuffer.allocateDirect(squareLineColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer11.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer colorBuffer6 = byteBuffer11.asFloatBuffer(); // Byte Buffer to float buffer
		colorBuffer6.put(squareLineColor); // float buffer dila tyaat kai bharu tar triangleVertices
		colorBuffer6.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squareLineColor.length * 4, colorBuffer6, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		//circleArray((float)(base1 / Math.sqrt(3)), (float)(base1 / Math.sqrt(3)), 0.0f, 0.0f);

		final float circleLine[] = new float[]
		{
			0.692820f, 0.000000f, 0.000000f,
			0.691455f, 0.043481f, 0.000000f,
			0.687363f, 0.086790f, 0.000000f,
			0.680561f, 0.129757f, 0.000000f,
			0.671076f, 0.172212f, 0.000000f,
			0.658945f, 0.213988f, 0.000000f,
			0.644217f, 0.254921f, 0.000000f,
			0.626948f, 0.294849f, 0.000000f,
			0.607208f, 0.333614f, 0.000000f,
			0.585074f, 0.371064f, 0.000000f,
			0.560633f, 0.407051f, 0.000000f,
			0.533982f, 0.441433f, 0.000000f,
			0.505226f, 0.474075f, 0.000000f,
			0.474477f, 0.504848f, 0.000000f,
			0.441858f, 0.533630f, 0.000000f,
			0.407497f, 0.560309f, 0.000000f,
			0.371530f, 0.584778f, 0.000000f,
			0.334097f, 0.606942f, 0.000000f,
			0.295348f, 0.626713f, 0.000000f,
			0.255434f, 0.644014f, 0.000000f,
			0.214513f, 0.658775f, 0.000000f,
			0.172746f, 0.670939f, 0.000000f,
			0.130298f, 0.680457f, 0.000000f,
			0.087337f, 0.687293f, 0.000000f,
			0.044031f, 0.691420f, 0.000000f,
			0.000552f, 0.692820f, 0.000000f,
			-0.042930f, 0.691489f, 0.000000f,
			-0.086242f, 0.687432f, 0.000000f,
			-0.129215f, 0.680664f, 0.000000f,
			-0.171677f, 0.671213f, 0.000000f,
			-0.213463f, 0.659116f, 0.000000f,
			-0.254408f, 0.644420f, 0.000000f,
			-0.294349f, 0.627183f, 0.000000f,
			-0.333130f, 0.607474f, 0.000000f,
			-0.370598f, 0.585369f, 0.000000f,
			-0.406604f, 0.560957f, 0.000000f,
			-0.441008f, 0.534333f, 0.000000f,
			-0.473673f, 0.505603f, 0.000000f,
			-0.504470f, 0.474879f, 0.000000f,
			-0.533278f, 0.442283f, 0.000000f,
			-0.559984f, 0.407943f, 0.000000f,
			-0.584482f, 0.371995f, 0.000000f,
			-0.606676f, 0.334581f, 0.000000f,
			-0.626478f, 0.295847f, 0.000000f,
			-0.643810f, 0.255947f, 0.000000f,
			-0.658604f, 0.215038f, 0.000000f,
			-0.670801f, 0.173281f, 0.000000f,
			-0.680354f, 0.130840f, 0.000000f,
			-0.687224f, 0.087884f, 0.000000f,
			-0.691384f, 0.044582f, 0.000000f,
			-0.692819f, 0.001103f, 0.000000f,
			-0.691523f, -0.042379f, 0.000000f,
			-0.687500f, -0.085695f, 0.000000f,
			-0.680767f, -0.128672f, 0.000000f,
			-0.671350f, -0.171143f, 0.000000f,
			-0.659285f, -0.212939f, 0.000000f,
			-0.644622f, -0.253895f, 0.000000f,
			-0.627417f, -0.293850f, 0.000000f,
			-0.607739f, -0.332647f, 0.000000f,
			-0.585664f, -0.370132f, 0.000000f,
			-0.561281f, -0.406158f, 0.000000f,
			-0.534684f, -0.440582f, 0.000000f,
			-0.505980f, -0.473270f, 0.000000f,
			-0.475281f, -0.504092f, 0.000000f,
			-0.442708f, -0.532926f, 0.000000f,
			-0.408389f, -0.559659f, 0.000000f,
			-0.372461f, -0.584186f, 0.000000f,
			-0.335064f, -0.606409f, 0.000000f,
			-0.296346f, -0.626242f, 0.000000f,
			-0.256459f, -0.643606f, 0.000000f,
			-0.215562f, -0.658432f, 0.000000f,
			-0.173815f, -0.670663f, 0.000000f,
			-0.131382f, -0.680249f, 0.000000f,
			-0.088431f, -0.687154f, 0.000000f,
			-0.045132f, -0.691349f, 0.000000f,
			-0.001655f, -0.692818f, 0.000000f,
			0.041829f, -0.691557f, 0.000000f,
			0.085147f, -0.687568f, 0.000000f,
			0.128130f, -0.680869f, 0.000000f,
			0.170608f, -0.671486f, 0.000000f,
			0.212414f, -0.659455f, 0.000000f,
			0.253381f, -0.644824f, 0.000000f,
			0.293350f, -0.627651f, 0.000000f,
			0.332162f, -0.608003f, 0.000000f,
			0.369665f, -0.585959f, 0.000000f,
			0.405710f, -0.561604f, 0.000000f,
			0.440156f, -0.535035f, 0.000000f,
			0.472867f, -0.506357f, 0.000000f,
			0.503713f, -0.475682f, 0.000000f,
			0.532573f, -0.443132f, 0.000000f,
			0.559334f, -0.408835f, 0.000000f,
			0.583889f, -0.372926f, 0.000000f,
			0.606142f, -0.335547f, 0.000000f,
			0.626006f, -0.296844f, 0.000000f,
			0.643402f, -0.256972f, 0.000000f,
			0.658260f, -0.216086f, 0.000000f,
			0.670524f, -0.174349f, 0.000000f,
			0.680144f, -0.131924f, 0.000000f,
			0.687083f, -0.088979f, 0.000000f,
			0.691313f, -0.045683f, 0.000000f,
			0.692817f, -0.002207f, 0.000000f,
		};
		 
		final float circleLineColor[] = new float[]
		{
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
		};

		// Circle Line
		// vao realted code
		GLES32.glGenVertexArrays(1, vao_circleLines, 0);

		GLES32.glBindVertexArray(vao_circleLines[0]);

		GLES32.glGenBuffers(1, vbo_circleLines_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_circleLines_position[0]); //bind buffer

		ByteBuffer byteBuffer12 = ByteBuffer.allocateDirect(circleLine.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer12.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer positionBuffer7 = byteBuffer12.asFloatBuffer(); // Byte Buffer to float buffer
		positionBuffer7.put(circleLine); // float buffer dila tyaat kai bharu tar triangleVertices
		positionBuffer7.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleLine.length * 4, positionBuffer7, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_circleLines_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_circleLines_color[0]); //bind buffer

		ByteBuffer byteBuffer13 = ByteBuffer.allocateDirect(circleLineColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer13.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer colorBuffer7 = byteBuffer13.asFloatBuffer(); // Byte Buffer to float buffer
		colorBuffer7.put(circleLineColor); // float buffer dila tyaat kai bharu tar triangleVertices
		colorBuffer7.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, circleLineColor.length * 4, colorBuffer7, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE); // culling mode enable hot yaane

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		// function declarations
		
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		float translationMatrix[] = new float[16];
		Matrix.setIdentityM(translationMatrix, 0);

		float modelViewMatrix[] = new float[16];
		Matrix.setIdentityM(modelViewMatrix, 0);

		float modelViewProjectionMatrix[] = new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		// transformations
		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.5f);
		modelViewMatrix = translationMatrix;

		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		if (gFlag == true)
		{
			GLES32.glBindVertexArray(vao_graphHLines[0]);

			// stepE 2 : draw the desired graphics/animation
			// here will be magic code
			GLES32.glLineWidth(3);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 80); 

			GLES32.glBindVertexArray(0);

			GLES32.glBindVertexArray(vao_graphVLines[0]);

			// stepE 2 : draw the desired graphics/animation
			// here will be magic code
			GLES32.glLineWidth(3);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 80);

			GLES32.glBindVertexArray(0);

			GLES32.glBindVertexArray(vao_horizontalLine[0]);

			// stepE 2 : draw the desired graphics/animation
			// here will be magic code
			GLES32.glLineWidth(3);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

			GLES32.glBindVertexArray(0);

			GLES32.glBindVertexArray(vao_verticalLine[0]);

			// stepE 2 : draw the desired graphics/animation
			// here will be magic code
			GLES32.glLineWidth(3);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

			GLES32.glBindVertexArray(0);
		}
	
		if (tFlag == true && gFlag == true)
		{
			GLES32.glBindVertexArray(vao_triangleLines[0]);

			// stepE 2 : draw the desired graphics/animation
			// here will be magic code
			GLES32.glLineWidth(3);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 6);

			GLES32.glBindVertexArray(0);
		}

		if (sFlag == true && gFlag == true)
		{
			GLES32.glBindVertexArray(vao_squareLines[0]);

			// stepE 2 : draw the desired graphics/animation
			// here will be magic code
			GLES32.glLineWidth(3);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 8);

			GLES32.glBindVertexArray(0);
		}

		if (cFlag == true && gFlag == true)
		{
				GLES32.glBindVertexArray(vao_circleLines[0]);
				GLES32.glLineWidth(3);
				GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 100);
				GLES32.glBindVertexArray(0);
		}

		// Here Will Be Drawing Code

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void circleArray(float radius_x, float radius_y, float start, float end)
	{
		
	}

	private void uninitialize()
	{
		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		if (vbo_verticalLine_position[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_verticalLine_position, 0);
			vbo_verticalLine_position[0] = 0;
		}

		// deletion/ uninitialization of vao
		if (vao_verticalLine[0] > 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_verticalLine, 0);
			vao_verticalLine[0] = 0;
		}

		// deletion/ uninitialization of vbo triangle
		if (vbo_horizontalLine_position[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_horizontalLine_position, 0);
			vbo_horizontalLine_position[0] = 0;
		}

		// deletion/ uninitialization of vao
		if (vao_horizontalLine[0] > 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_horizontalLine, 0);
			vao_horizontalLine[0] = 0;
		}
	}
}
