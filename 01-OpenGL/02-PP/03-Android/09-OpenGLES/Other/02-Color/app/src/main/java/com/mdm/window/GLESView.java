package com.mdm.window;

// textview related packages
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.content.Context;

// event related packages
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

// matrix package
import android.opengl.Matrix;

// texture related packages 
import android.graphics.Bitmap; 
import android.graphics.BitmapFactory;

import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements OnDoubleTapListener,OnGestureListener,GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private int shaderProgramObject;

	private int vao_pyramid[]=new int[1];
	private int vbo_pyramid[]=new int[1];
	private int vbo_pyramidtexcoord[]=new int[1];

	private int vao_cube[]=new int[1];
	private int vbo_cube[]=new int[1];
	private int vbo_cubetexcoord[]=new int[1];

	private int texture_stone[]=new int[1];
	private int texture_kundali[]=new int[1];

	private int mvpMatrixUniform;
	private int textureSamplerUniform;

	private float anglePyramid;
	private float angleCube;
	private float perspectiveProjectionMatrix[]=new float[16];

	private Context context;
	GLESView(Context _context)
	{
		super(_context);

		context=_context;

		setEGLContextClientVersion(3);//Opengl ES 3.2 Opengl based upon Android based ndk
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		// gesture Related Code
		gestureDetector=new GestureDetector(context, this, null, false);
		// acitivity de,mala kon istner karnar aahe,any seperate handler to handle me : NULL,false : future use
		gestureDetector.setOnDoubleTapListener(this);
	}

	// three methods of GLSurfaceView of Renderer 
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VAB: " + glesVersion);

		String vender = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("VAB: " + vender);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("VAB: " + renderer);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VAB: " + glslVersion);

		String extensions = gl.glGetString(GL10.GL_EXTENSIONS);
		//System.out.print("VAB: " + extensions +"\n");
		int count = 1;
		for(int i = 0; i < extensions.length(); i++)
		{
			if(i == 0)
			{
				System.out.println("");
				System.out.print("VAB: Extensions ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extensions.charAt(i));
				count++;
			}
			else if(Character.isWhitespace(extensions.charAt(i)))
			{
				System.out.println("");
				System.out.print("VAB: Extensions ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extensions.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		// onDrawframe should be considered gameLoop update()
		update();
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//Code
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);

			return true;
	}

	// three methos of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//Code
		return true;
	}
	@Override
	public boolean onDoubleTapEvent(MotionEvent e) // ha super la jaeil ka yachi garenty naste 
	{
		//Code
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//Code
		return true;
	}

	// six methods of OnGestureListner

	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		// code
	}

	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		// code
		uninitialize();
		System.exit(0);
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e)
	{
				
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;	
	}

	// custom private funtions
	private void initialize()
	{
		// code
		// vertex shader
		final String vertexShaderSourceCode=String.format
		(
				"#version 320 es" +
				"\n" +
				"in vec4 a_position;" +
				"in vec2 a_texcooard;" +
				"uniform mat4 u_mvpMatrix;" +
				"out vec2 a_texcooard_out;" +
				"void main(void)" +
				"{" +
					"gl_Position = u_mvpMatrix * a_position;" +
					"a_texcooard_out = a_texcooard;" +
				"}"
		);

		int vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[]=new int[1];
		int infoLogLength[]=new int[1];
		String log=null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("vgn: Vertex Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment shader
		final String fragmentShaderSourceCode=String.format
		(
				"#version 320 es" +
				"\n" +
				"precision highp float;" +
				"uniform highp sampler2D u_textureSampler;" +
				"in vec2 a_texcooard_out;" +
				"out vec4 FragColor;" +
				"void main(void)" +
				"{" +
				"FragColor = texture(u_textureSampler , a_texcooard_out);" +
				"}"
		);

		int fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("vgn: Fragment Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		// shader Program
		shaderProgramObject=GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		// prelinkg
		//GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, "a_texcooard");

		GLES32.glLinkProgram(shaderProgramObject);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("vgn: Shader Program Link log: "+log);
				uninitialize();
				System.exit(0);
			}
		}
		//Post Linking
		mvpMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_textureSampler");
		
		final float pyramidVertices[] = new float[]
		{
			// front
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// right
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// back
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			// left
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		final float pyramidTexcoord[] = new float[]
		{
			0.5f, 1.0f, // front-top
			0.0f, 0.0f, // front-left
			1.0f, 0.0f, // front-right

			0.5f, 1.0f, // right-top
			1.0f, 0.0f, // right-left
			0.0f, 0.0f, // right-right

			0.5f, 1.0f, // back-top
			1.0f, 0.0f, // back-left
			0.0f, 0.0f, // back-right

			0.5f, 1.0f, // left-top
			0.0f, 0.0f, // left-left
			1.0f, 0.0f, // left-right
		};

		final float cubeVertices[] = new float[]
		{
			// top
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			// bottom
			1.0f, -1.0f, -1.0f,
		   -1.0f, -1.0f, -1.0f,
		   -1.0f, -1.0f,  1.0f,
			1.0f, -1.0f,  1.0f,

			// front
			1.0f, 1.0f, 1.0f,
		   -1.0f, 1.0f, 1.0f,
		   -1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// back
			1.0f, 1.0f, -1.0f,
		   -1.0f, 1.0f, -1.0f,
		   -1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			// right
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// left
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		final float cubeTexcoords[] = new float[]
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
		
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f
		};

		// vao and vbo related code
		
		// pyramid
		GLES32.glGenVertexArrays(1, vao_pyramid,0); //vao:- vertex array object
		GLES32.glBindVertexArray(vao_pyramid[0]);

		GLES32.glGenBuffers(1, vbo_pyramid,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_pyramid[0]);

		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(pyramidVertices.length*4); // aasa buffer pass kr jo natively pass kr // *4 :- size of nahi java made mhanun aapan manully krto
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer=byteBuffer.asFloatBuffer(); // position navacha buffer tayar kr majya byte buffer pasun .tya veli byte buffer la float buffer mhanun consider kr
		positionBuffer.put(pyramidVertices);
		positionBuffer.position(0); // FloatBuffer position buffer made 0th position pasun start kr

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// for texcoord
		GLES32.glGenBuffers(1, vbo_pyramidtexcoord,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_pyramidtexcoord[0]);

		byteBuffer=ByteBuffer.allocateDirect(pyramidTexcoord.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer texcoordBuffer=byteBuffer.asFloatBuffer();
		texcoordBuffer.put(pyramidTexcoord);
		texcoordBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidTexcoord.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, 2, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		// cube
		GLES32.glGenVertexArrays(1, vao_cube,0);
		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glGenBuffers(1, vbo_cube,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_cube[0]);

		byteBuffer=ByteBuffer.allocateDirect(cubeVertices.length*4); // aasa buffer pass kr jo natively pass kr // *4 :- size of nahi java made mhanun aapan manully krto
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer=byteBuffer.asFloatBuffer(); // position navacha buffer tayar kr majya byte buffer pasun .tya veli byte buffer la float buffer mhanun consider kr
		positionBuffer.put(cubeVertices);
		positionBuffer.position(0); // FloatBuffer position buffer made 0th position pasun start kr

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// for texcoord
		GLES32.glGenBuffers(1, vbo_cubetexcoord,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_cubetexcoord[0]);

		byteBuffer=ByteBuffer.allocateDirect(cubeTexcoords.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		texcoordBuffer=byteBuffer.asFloatBuffer();
		texcoordBuffer.put(cubeTexcoords);
		texcoordBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexcoords.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, 2, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);
		
		// genral OpenGL
		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		// enable culling
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		GLES32.glEnable(GLES32.GL_TEXTURE_2D);

		texture_stone[0]=loadGLTexture(R.raw.stone);
		texture_kundali[0]=loadGLTexture(R.raw.vijay_kundali);

		anglePyramid = 0.0f;
		angleCube = 0.0f;

		// initialization of projection matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private int loadGLTexture(int imageResourceid)
	{
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),imageResourceid,options);
		int texture[] = new int[1];
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
		
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		return(texture[0]);
	}

	private void resize(int width,int height)
	{
		//Code
		if (height <= 0) // to avoid divied by 0 in future call
		height = 1;
		GLES32.glViewport(0,0,width,height);

		Matrix.perspectiveM(perspectiveProjectionMatrix ,0,45.0f, (float)width /(float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		//Code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);
		//Here there drawing code
		// pyramid
		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float rotationMatrix[]=new float[16];
		Matrix.setIdentityM(rotationMatrix,0);

		float modelViewMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewMatrix,0);

		float modelViewProjectionMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(translationMatrix, 0, -2.0f, 0.0f, -6.0f);
		Matrix.setRotateM(rotationMatrix, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
		
		//modelViewMatrix = translationMatrix;
		Matrix.multiplyMM(modelViewMatrix,0,translationMatrix,0,rotationMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix,0);

		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_stone[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glBindVertexArray(vao_pyramid[0]);

		// stepE 2 : draw the desiered graphics/animation
		// here will be magic code

		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		// cube
		Matrix.setIdentityM(translationMatrix,0);

		float rotationMatrix_X[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_X,0);

		float rotationMatrix_Y[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Y,0);

		float rotationMatrix_Z[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Z,0);

		float scaleMatrix[]=new float[16];
		Matrix.setIdentityM(scaleMatrix,0);

		float tempMatrix[]=new float[16];
		Matrix.setIdentityM(tempMatrix,0);

		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(translationMatrix, 0, 2.0f, 0.0f, -6.0f);
		//modelViewMatrix = translationMatrix;
		Matrix.setRotateM(rotationMatrix_X, 0, angleCube, 1.0f, 0.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Y, 0, angleCube, 0.0f, 1.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Z, 0, angleCube, 0.0f, 0.0f, 1.0f);

		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);

		Matrix.multiplyMM(rotationMatrix_X,0,rotationMatrix_Y,0,rotationMatrix_Z,0);

		Matrix.multiplyMM(tempMatrix,0,translationMatrix,0,rotationMatrix_X,0);

		Matrix.multiplyMM(modelViewMatrix,0,tempMatrix,0,scaleMatrix,0);

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix,0);
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_kundali[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glBindVertexArray(vao_cube[0]);

		// stepE 2 : draw the desiered graphics/animation
		// here will be magic code
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		GLES32.glUseProgram(0);

		requestRender(); // swapBuffer or glxswapBuffer sarkhe  
	}
	private void update()
	{
		
			anglePyramid = anglePyramid + 1.0f;
			if (anglePyramid >= 360)
			{
				anglePyramid = anglePyramid - 360.0f;
			}

			angleCube = angleCube + 1.0f;
			if (angleCube >= 360)
			{
				angleCube = angleCube - 360.0f;
			}
	}
	private void uninitialize()
	{
		//Code

		if(shaderProgramObject > 0)
		{
			GLES32.glUseProgram(shaderProgramObject);
			int retVal[]=new int[1];
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal,0);
			if(retVal[0] > 0)
			{
				int numAttachedShaders=retVal[0];
				int shaderObjects[]=new int[numAttachedShaders];
				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal,0, shaderObjects,0);

				for(int i=0; i<numAttachedShaders;i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}
			GLES32.glUseProgram(0);
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		if(texture_stone[0]>0)
		{
			GLES32.glDeleteTextures(1, texture_stone, 0);
			texture_stone[0] = 0;
		}

		if(texture_kundali[0]>0)
		{
			GLES32.glDeleteTextures(1, texture_kundali, 0);
			texture_kundali[0] = 0;
		}

		// deletion / uninitialization of vbo
		
		if (vbo_pyramid[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid,0);
			vbo_pyramid[0] = 0;
		}

		if (vbo_pyramidtexcoord[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramidtexcoord,0);
			vbo_pyramidtexcoord[0] = 0;
		}

		if (vbo_cube[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_cube,0);
			vbo_cube[0] = 0;
		}

		if (vbo_cubetexcoord[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_cubetexcoord,0);
			vbo_cubetexcoord[0] = 0;
		}

		// deletion / uninitialization of vao

		if (vao_cube[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube,0);
			vao_cube[0] = 0;
		}

		if (vao_pyramid[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid,0);
			vao_pyramid[0] = 0;
		}
	}

}
