package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

// matrix package
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	private int vao_pyramid[] = new int[1];
	private int vbo_pyramid_position[] = new int[1];
	private int vbo_pyramid_color[] = new int[1];

	private int vao_cube[] = new int[1];
	private int vbo_cube_position[] = new int[1];
	private int vbo_cube_color[] = new int[1];
	
	private float angle = 0.0f;

	private int mvpMatrixUniform; // hyacha address pass karaycha naahi mhanun array bracket nahi

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 a_position;" +
		"in vec4 a_color;" +
		"uniform mat4 u_mvpMatrix;" +
		"out vec4 a_color_out;" +
		"void main(void)" +
		"{" +
			"gl_Position = u_mvpMatrix * a_position;" +
			"a_color_out = a_color;" +
		"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 a_color_out;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = a_color_out;" +
		"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_COLOR, "a_color");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		mvpMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");

		// vao & vbo related code
		//declarations of vertex data arrays
		final float pyramidPosition[] = new float[]
		{
			// front
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// right
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// back
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			// left
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		final float pyramidColor[] = new float[]
		{
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f
		};

		final float cubePosition[] = new float[]
		{
			// top
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			// bottom
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// front
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
		    -1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// back
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			// right
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// left
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
		};

		final float cubeColor[] = new float[]
		{
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			1.0f, 0.5f, 0.0f,
			1.0f, 0.5f, 0.0f,
			1.0f, 0.5f, 0.0f,
			1.0f, 0.5f, 0.0f,

			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,

			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,

			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f
		};

		// Pyramid
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);

		GLES32.glBindVertexArray(vao_pyramid[0]);

		GLES32.glGenBuffers(1, vbo_pyramid_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_position[0]); //bind buffer

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidPosition.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer pyramidPositionBuffer = byteBuffer.asFloatBuffer(); // Byte Buffer to float buffer
		pyramidPositionBuffer.put(pyramidPosition); // float buffer dila tyaat kai bharu tar triangleVertices
		pyramidPositionBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidPosition.length * 4, pyramidPositionBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_pyramid_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_color[0]); //bind buffer

		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(pyramidColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer1.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer pyramidColorBuffer = byteBuffer1.asFloatBuffer(); // Byte Buffer to float buffer
		pyramidColorBuffer.put(pyramidColor); // float buffer dila tyaat kai bharu tar triangleVertices
		pyramidColorBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidColor.length * 4, pyramidColorBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		// Cube
		GLES32.glGenVertexArrays(1, vao_cube, 0);

		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glGenBuffers(1, vbo_cube_position, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_position[0]); //bind buffer

		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(cubePosition.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer2.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer cubePositionBuffer = byteBuffer2.asFloatBuffer(); // Byte Buffer to float buffer
		cubePositionBuffer.put(cubePosition); // float buffer dila tyaat kai bharu tar triangleVertices
		cubePositionBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubePosition.length * 4, cubePositionBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// vbo for color
		GLES32.glGenBuffers(1, vbo_cube_color, 0); 

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_color[0]); //bind buffer

		ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(cubeColor.length * 4); // asa buffer tayaar kar jo NDK la pass karel
		byteBuffer3.order(ByteOrder.nativeOrder()); // native order ghe tuza jasa aahe tasa - right to left or left to right
		FloatBuffer cubeColorBuffer = byteBuffer3.asFloatBuffer(); // Byte Buffer to float buffer
		cubeColorBuffer.put(cubeColor); // float buffer dila tyaat kai bharu tar triangleVertices
		cubeColorBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeColor.length * 4, cubeColorBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0); // java tla null ha 0 nahi window 0 asto NULL

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		float translationMatrix[] = new float[16];
		Matrix.setIdentityM(translationMatrix, 0);

		float rotationMatrix[] = new float[16];
		Matrix.setIdentityM(rotationMatrix, 0);

		float scaleMatrix[] = new float[16];
		Matrix.setIdentityM(scaleMatrix, 0);

		float resultMatrix[] = new float[16];
		Matrix.setIdentityM(resultMatrix, 0);

		float modelViewMatrix[] = new float[16];
		Matrix.setIdentityM(modelViewMatrix, 0);

		float modelViewProjectionMatrix[] = new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		// transformations
		// triangle
		Matrix.translateM(translationMatrix, 0, -2.0f, 0.0f, -5.2f);
		Matrix.setRotateM(rotationMatrix, 0, angle, 0.0f, 1.0f, 0.0f);

		Matrix.multiplyMM(modelViewMatrix, 0, translationMatrix, 0, rotationMatrix, 0);

		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindVertexArray(vao_pyramid[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);

		GLES32.glBindVertexArray(0);

		// cube
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		Matrix.translateM(translationMatrix, 0, 2.0f, 0.0f, -5.2f);
		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);
		Matrix.setRotateM(rotationMatrix, 0, angle, 1.0f, 1.0f, 1.0f);

		Matrix.multiplyMM(resultMatrix, 0, scaleMatrix, 0, rotationMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix, 0, translationMatrix, 0, resultMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

		GLES32.glBindVertexArray(0);

		// Here Will Be Drawing Code

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void update()
	{
		angle = angle + 0.5f;

		if (angle >= 360.0f)
		{
			angle = angle - 360.0f;
		}
	}

	private void uninitialize()
	{
		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		// deletion/ uninitialization of vbo
		if (vbo_pyramid_color[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_color, 0);
			vbo_pyramid_color[0] = 0;
		}

		if (vbo_pyramid_position[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_position, 0);
			vbo_pyramid_position[0] = 0;
		}

		// deletion/ uninitialization of vao
		if (vao_pyramid[0] > 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
			vao_pyramid[0] = 0;
		}

		if (vbo_cube_position[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_cube_position, 0);
			vbo_cube_position[0] = 0;
		}

		// deletion/ uninitialization of vao
		if (vao_cube[0] > 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0] = 0;
		}
	}
}
