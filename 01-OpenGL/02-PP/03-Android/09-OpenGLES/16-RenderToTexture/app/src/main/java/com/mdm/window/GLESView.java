package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import java.nio.ShortBuffer;

// matrix package
import android.opengl.Matrix;

// texture related
import android.graphics.BitmapFactory;

import android.graphics.Bitmap;

import android.opengl.GLUtils;


public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	private int vao_cube[]=new int[1];
	private int vbo_cube[]=new int[1];
	private int vbo_cubetexcoord[]=new int[1];

	private final int fboWidth = 512;
	private final int fboHeight = 512;

	private int winWidth;
	private int winHeight;

	private int textureSamplerUniform;

	private int mvpMatrixUniform; // hyacha address pass karaycha naahi mhanun array bracket nahi

	private float angleCube;

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	// fbo related variables
	private int fbo[] = new int[1]; // frame buffer object
	private int rbo[] = new int[1]; // render buffer object
	private int fbo_texture[] = new int[1];
	boolean bFBO_Result = false;

	// texture scene global variables
	// int shaderProgramObject_Sphere;
	private int shaderProgramObject_Sphere_PF;
	private int shaderProgramObject_Sphere_PV;

	private int vao_sphere[] = new int[1];
	private int vbo_sphere_position[] = new int[1];
	private int vbo_sphere_normal[] = new int[1];
	private int vbo_sphere_elements[] = new int[1];

	private float perspectiveProjectionMatrix_sphere[] = new float[16];

	private int numVertices;
	private int numElements;

	// per vertex related uniforms
	private int modelMatrixUniform_sphere_PV;
	private int viewMatrixUniform_sphere_PV;
	private int projectionMatrixUniform_sphere_PV;

	private int laUniform_sphere_PV[] = new int[3]; 
	private int ldUniform_sphere_PV[] = new int[3]; 
	private int lsUniform_sphere_PV[] = new int[3]; 
	private int lightPositionUniform_sphere_PV[] = new int[3]; 

	private int kaUniform_sphere_PV;
	private int kdUniform_sphere_PV;
	private int ksUniform_sphere_PV;
	private int materialShininessUniform_sphere_PV;

	private int lightingEnabledUniform_sphere_PV;

	// per fragment related uniforms
	private int modelMatrixUniform_sphere_PF;
	private int viewMatrixUniform_sphere_PF;
	private int projectionMatrixUniform_sphere_PF;

	private int laUniform_sphere_PF[] = new int[3]; 
	private int ldUniform_sphere_PF[] = new int[3]; 
	private int lsUniform_sphere_PF[] = new int[3]; 
	private int lightPositionUniform_sphere_PF[] = new int[3]; 

	private int kaUniform_sphere_PF;
	private int kdUniform_sphere_PF;
	private int ksUniform_sphere_PF;
	private int materialShininessUniform_sphere_PF;

	private int lightingEnabledUniform_sphere_PF;

	private class Light
	{
		float lightAmbient_sphere[]=new float[4];
		float lightDiffuse_sphere[]=new float[4];
		float lightSpecular_sphere[]=new float[4];
		float lightPosition_sphere[]=new float[4];
	}

	private Light lights_sphere[] = new Light[3];

	private int singleTap = 0;
	private int doubleTap = 0;

	private float materialAmbient_sphere[] = { 0.0f,0.0f,0.0f,1.0f };
	private float materialDiffuse_sphere[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float materialSpecular_sphere[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	private float materialShininess_sphere = 128.0f;

	private float lightAngleZero = 0.0f;

	private int counter = 1;

	boolean bLight = false;

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		// code
		doubleTap++;
		if(doubleTap > 1)
		{
			bLight = true;
			doubleTap = 0;
			singleTap = 0;
		}
		else
		{
			bLight = false;
		}

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		// Code
		singleTap++;

		if(singleTap > 1)
		{
			singleTap = 0;
		}

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	boolean createFBO(int textureWidth, int textureHeight)
	{
		String log = null;

		// code
		// step 1 - Check available render buffer size
		int maxRenderbufferSize;

		//GLES32.glGetIntegerv(GLES32.GL_MAX_RENDERBUFFER_SIZE, maxRenderbufferSize);

		//if (maxRenderbufferSize < textureWidth || maxRenderbufferSize < textureHeight)
		//{
			//System.out.println("MDM- Insufficient Render Buffer Size\n" + log);
		//	return(false);
		//}
	
		// Step 2 - Create Frame Buffer Object
		GLES32.glGenFramebuffers(1, fbo, 0);
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, fbo[0]);

		// Step 3 - Create Render Buffer Object
		GLES32.glGenRenderbuffers(1, rbo, 0);
		GLES32.glBindRenderbuffer(GLES32.GL_RENDERBUFFER, rbo[0]);

		// Step 4 - Where to keep this render buffer (Storage and format of render buffer)
		GLES32.glRenderbufferStorage(GLES32.GL_RENDERBUFFER, GLES32.GL_DEPTH_COMPONENT16, textureWidth, textureHeight); // 1 - target, 2 - format, 3, 4 - size kiti laagel storage la

		// Step 5 - Create empty texture for upcoming target scene
		GLES32.glGenTextures(1, fbo_texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, fbo_texture[0]);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_CLAMP_TO_EDGE); // horizontall
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_CLAMP_TO_EDGE); // vertical
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR); 
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR);

		GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D, 0, GLES32.GL_RGB, textureWidth, textureHeight, 0, GLES32.GL_RGB, GLES32.GL_UNSIGNED_SHORT_5_6_5, null); // GL_UNSIGNED_SHORT_5_6_5 matches with GL_DEPTH_COMPONENT16
		// 5_6_5 - Red, Green, Blue, why 6 - human eye is more adjustable to green color spectrum, (GL_DEPTH_COMPONENT16) 16 / 3 hot nahi so 5, 6, 5 kela
		// green la ektra 1 bit dila
		// last parameter is 0 ka - karan empty full window texture

		// Step 6 - Give Above texture to fbo
		GLES32.glFramebufferTexture2D(GLES32.GL_FRAMEBUFFER, GLES32.GL_COLOR_ATTACHMENT0, GLES32.GL_TEXTURE_2D, fbo_texture[0], 0); 
		// 1 - target kuthe aahe buffer, 2 - kuthe chitkavu texture, 3 - binding point, 4 - binding point la konta texture, 5 - mipmap level

		// Step 7 - Give rbo to fbo (empty texture kasa bharaycha tar rbo ne)
		GLES32.glFramebufferRenderbuffer(GLES32.GL_FRAMEBUFFER, GLES32.GL_DEPTH_ATTACHMENT, GLES32.GL_RENDERBUFFER, rbo[0]);
		// 1 - target kuthe aahe buffer, 2 - frame buffer na render buffer kuthun baghaycha aahe tar depth buffer, 3 - konala taaku, 4 - kuth jo data ahe to kon aahe

		// Step 8 - Check whether the frame buffer created successfull or not
		//enum result = GLES32.glCheckFramebufferStatus(GLES32.GL_FRAMEBUFFER);
		// 1 - target la aani frame buffer ahe ka nahi check kar

		//if (result != GLES32.GL_FRAMEBUFFER_COMPLETE)
		//{
			//System.out.println("MDM- Insufficient Render Buffer Size\n" + log);
			//return(false);
		//}

		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);// framebuffer unbind zhala ki render buffer ani texture automatically unbind hotat

		return(true);
	}

	private void resize_sphere(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private int initialize_sphere(int width, int height)
	{
		// vertex shader pervertex
		final String vertexShaderSourceCode_PV=String.format
		(
			"#version 320 es" +
            "\n" +
            "precision highp float;" +
            "in vec4 a_position;" +
			"in vec3 a_normal;" +
			"uniform mat4 u_modelMatrix;" +
			"uniform mat4 u_viewMatrix;" +
			"uniform mat4 u_projectionMatrix;" +
			"uniform vec3 u_la[3];" +
			"uniform vec3 u_ld[3];" +
			"uniform vec3 u_ls[3];" +
			"uniform vec4 u_lightPosition[3];" +
			"uniform vec3 u_ka;" +
			"uniform vec3 u_kd;" +
			"uniform vec3 u_ks;" +
			"uniform float u_materialShininess;" +
			"uniform mediump int u_lightingEnabled;" +
			"out vec3 phong_ads_light;" +
			"void main(void)" +
			"{" +
			"if(u_lightingEnabled == 1)" +
			"{" +
			"vec3 ambient[3];" +
			"vec3 lightDirection[3];" +
			"vec3 diffuse[3];" +
			"vec3 reflectionVector[3];" +
			"vec3 specular[3];" +
			"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
			"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
			"vec3 transformedNormals = normalize(normalMatrix * a_normal);" +
			"vec3 viewerVector = normalize(-eyeCoordinates.xyz);" +
			"for(int i = 0; i < 3; i++)" +
			"{"+
			"ambient[i] = u_la[i] * u_ka;" +
			"lightDirection[i] = normalize(vec3(u_lightPosition[i]) - eyeCoordinates.xyz);" +
			"diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformedNormals), 0.0);" +
			"reflectionVector[i] = reflect(-lightDirection[i], transformedNormals);" +
			"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewerVector), 0.0), u_materialShininess);" +
			"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
			"}" +
			"}" +
			"else" +
			"{" +
			"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
			"}"
			);

		int vertexShaderObject_PV=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject_PV, vertexShaderSourceCode_PV);
		GLES32.glCompileShader(vertexShaderObject_PV);

		int status[]=new int[1];
		int infoLogLength[]=new int[1];
		String log=null;

		GLES32.glGetShaderiv(vertexShaderObject_PV, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject_PV, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(vertexShaderObject_PV);
				System.out.println("vab: Vertex Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment shader pervertex
		final String fragmentShaderSourceCode_PV=String.format
		(
			"#version 320 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 phong_ads_light;" +
            "out vec4 FragColor;" +
			"void main(void)" +
			"{" +
				"FragColor = vec4(phong_ads_light,1.0);" +
			"}"
		);

		int fragmentShaderObject_PV=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject_PV, fragmentShaderSourceCode_PV);
		GLES32.glCompileShader(fragmentShaderObject_PV);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(fragmentShaderObject_PV, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject_PV, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(fragmentShaderObject_PV);
				System.out.println("vab: Fragment Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		// shader Program
		shaderProgramObject_Sphere_PV=GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject_Sphere_PV, vertexShaderObject_PV); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject_Sphere_PV, fragmentShaderObject_PV);
		
		// prelinkg
		GLES32.glBindAttribLocation(shaderProgramObject_Sphere_PV, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject_Sphere_PV, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		GLES32.glLinkProgram(shaderProgramObject_Sphere_PV);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetProgramiv(shaderProgramObject_Sphere_PV, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject_Sphere_PV, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetProgramInfoLog(shaderProgramObject_Sphere_PV);
				System.out.println("MDM- PV Sphere Shader Program Link log: "+log);
				uninitialize();
				System.exit(0);
			}
		}
		//Post Linking
		modelMatrixUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
		viewMatrixUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_viewMatrix");
		projectionMatrixUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_projectionMatrix");

		laUniform_sphere_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[0]");
		ldUniform_sphere_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[0]");
		lsUniform_sphere_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[0]");
		lightPositionUniform_sphere_PV[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[0]");

		laUniform_sphere_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[1]");
		ldUniform_sphere_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[1]");
		lsUniform_sphere_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[1]");
		lightPositionUniform_sphere_PV[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[1]");

		laUniform_sphere_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_la[2]");
		ldUniform_sphere_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ld[2]");
		lsUniform_sphere_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ls[2]");
		lightPositionUniform_sphere_PV[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightPosition[2]");

		kaUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ka");
		kdUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_kd");
		ksUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_ks");
		materialShininessUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_materialShininess");

		lightingEnabledUniform_sphere_PV = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PV, "u_lightingEnabled");


		// vertex shader perfragment
		final String vertexShaderSourceCode_PF=String.format
		(
				"#version 320 es" +
				"\n" +
				"precision highp float;" +
				"in vec4 a_position;" +
				"in vec3 a_normal;" +
				"uniform mat4 u_modelMatrix;" +
				"uniform mat4 u_viewMatrix;" +
				"uniform mat4 u_projectionMatrix;" +
				"uniform vec4 u_lightPosition[3];" +
				"uniform mediump int u_lightingEnabled;" +
				"out vec3 transformedNormals;" +
				"out vec3 lightDirection[3];" +
				"out vec3 viewerVector;" +
				"void main(void)" +
				"{" +
				"if(u_lightingEnabled == 1)" +
				"{" +
				"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
				"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
				"transformedNormals = normalMatrix * a_normal;" +
				"viewerVector = -eyeCoordinates.xyz;" +
				"for(int i = 0; i < 3; i++)" +
				"{" +
				"lightDirection[i] = vec3(u_lightPosition[i]) - eyeCoordinates.xyz;" +
				"}" +
				"}" +
				"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
				"}"
		);

		int vertexShaderObject_PF=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject_PF, vertexShaderSourceCode_PF);
		GLES32.glCompileShader(vertexShaderObject_PF);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(vertexShaderObject_PF, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject_PF, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(vertexShaderObject_PF);
				System.out.println("MDM- PV Vertex Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment shader perfragment
		final String fragmentShaderSourceCode_PF=String.format
		(
				"#version 320 es" +
				"\n" +
				"precision highp float;" +
				"in vec3 transformedNormals;" +
				"in vec3 lightDirection[3];" +
				"in vec3 viewerVector;" +
				"uniform vec3 u_la[3];" +
				"uniform vec3 u_ld[3];" +
				"uniform vec3 u_ls[3];" +
				"uniform vec3 u_ka;" +
				"uniform vec3 u_kd;" +
				"uniform vec3 u_ks;" +
				"uniform float u_materialShininess;" +
				"uniform mediump int u_lightingEnabled;" +
				"out vec4 FragColor;" +
				"void main(void)" +
				"{" +
				"vec3 phong_ads_light;" +
				"if(u_lightingEnabled == 1)" +
				"{" +
				"vec3 ambient[3];" +
				"vec3 diffuse[3];" +
				"vec3 reflectionVector[3];" +
				"vec3 specular[3];" +
				"vec3 normalized_transformed_normal;" +
				"vec3 normalized_lightDirection[3];" +
				"vec3 normalized_viewerVector;" +
				"normalized_transformed_normal = normalize(transformedNormals); " +
				"normalized_viewerVector = normalize(viewerVector);" +
				"for(int i = 0; i < 3; i++)" +
				"{" +
				"normalized_lightDirection[i] = normalize(lightDirection[i]);" +
				"ambient[i] = u_la[i] * u_ka;" +
				"diffuse[i] = u_ld[i] * u_kd * max(dot(normalized_lightDirection[i], normalized_transformed_normal), 0.0);" +
				"reflectionVector[i] = reflect(-normalized_lightDirection[i], normalized_transformed_normal);" +
				"specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalized_viewerVector), 0.0), u_materialShininess);" +
				"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
				"}" +
				"}" +
				"else" +
				"{" +
				"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
				"}" +
				"FragColor = vec4(phong_ads_light,1.0);" +
				"}"
		);

		int fragmentShaderObject_PF=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject_PF, fragmentShaderSourceCode_PF);
		GLES32.glCompileShader(fragmentShaderObject_PF);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(fragmentShaderObject_PF, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject_PF, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(fragmentShaderObject_PF);
				System.out.println("MDM- PF Fragment Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		// shader Program
		shaderProgramObject_Sphere_PF=GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject_Sphere_PF, vertexShaderObject_PF); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject_Sphere_PF, fragmentShaderObject_PF);
		
		// prelinkg
		GLES32.glBindAttribLocation(shaderProgramObject_Sphere_PF, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject_Sphere_PF, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		GLES32.glLinkProgram(shaderProgramObject_Sphere_PF);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetProgramiv(shaderProgramObject_Sphere_PF, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject_Sphere_PF, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetProgramInfoLog(shaderProgramObject_Sphere_PF);
				System.out.println("MDM- PF Shader Program Link log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Post Linking
		modelMatrixUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
		viewMatrixUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_viewMatrix");
		projectionMatrixUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_projectionMatrix");

		laUniform_sphere_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[0]");
		ldUniform_sphere_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[0]");
		lsUniform_sphere_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[0]");
		lightPositionUniform_sphere_PF[0] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[0]");

		laUniform_sphere_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[1]");
		ldUniform_sphere_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[1]");
		lsUniform_sphere_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[1]");
		lightPositionUniform_sphere_PF[1] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[1]");

		laUniform_sphere_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_la[2]");
		ldUniform_sphere_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ld[2]");
		lsUniform_sphere_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ls[2]");
		lightPositionUniform_sphere_PF[2] = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightPosition[2]");

		kaUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ka");
		kdUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_kd");
		ksUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_ks");
		materialShininessUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_materialShininess");

		lightingEnabledUniform_sphere_PF = GLES32.glGetUniformLocation(shaderProgramObject_Sphere_PF, "u_lightingEnabled");

		Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_elements,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_elements[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

		lights_sphere[0] = new Light();
        lights_sphere[1] = new Light();
        lights_sphere[2] = new Light();

		// light 1
		lights_sphere[0].lightAmbient_sphere[0] = 0.0f; 
		lights_sphere[0].lightAmbient_sphere[1] = 0.0f; 
		lights_sphere[0].lightAmbient_sphere[2] = 0.0f; 
		lights_sphere[0].lightAmbient_sphere[3] = 1.0f; 

		lights_sphere[0].lightDiffuse_sphere[0] = 1.0f; // Red Light
		lights_sphere[0].lightDiffuse_sphere[1] = 0.0f; 
		lights_sphere[0].lightDiffuse_sphere[2] = 0.0f; 
		lights_sphere[0].lightDiffuse_sphere[3] = 1.0f; 

		lights_sphere[0].lightSpecular_sphere[0] = 1.0f;
		lights_sphere[0].lightSpecular_sphere[1] = 0.0f;
		lights_sphere[0].lightSpecular_sphere[2] = 0.0f;
		lights_sphere[0].lightSpecular_sphere[3] = 1.0f;

		// light 2
		lights_sphere[1].lightAmbient_sphere[0] = 0.0f; 
		lights_sphere[1].lightAmbient_sphere[1] = 0.0f; 
		lights_sphere[1].lightAmbient_sphere[2] = 0.0f; 
		lights_sphere[1].lightAmbient_sphere[3] = 1.0f; 

		lights_sphere[1].lightDiffuse_sphere[0] = 0.0f; 
		lights_sphere[1].lightDiffuse_sphere[1] = 1.0f; // Green Light
		lights_sphere[1].lightDiffuse_sphere[2] = 0.0f; 
		lights_sphere[1].lightDiffuse_sphere[3] = 1.0f; 

		lights_sphere[1].lightSpecular_sphere[0] = 0.0f;
		lights_sphere[1].lightSpecular_sphere[1] = 0.0f;
		lights_sphere[1].lightSpecular_sphere[2] = 1.0f;
		lights_sphere[1].lightSpecular_sphere[3] = 1.0f;

		// light 3
		lights_sphere[2].lightAmbient_sphere[0] = 0.0f; 
		lights_sphere[2].lightAmbient_sphere[1] = 0.0f; 
		lights_sphere[2].lightAmbient_sphere[2] = 0.0f; 
		lights_sphere[2].lightAmbient_sphere[3] = 1.0f; 

		lights_sphere[2].lightDiffuse_sphere[0] = 0.0f; 
		lights_sphere[2].lightDiffuse_sphere[1] = 0.0f; 
		lights_sphere[2].lightDiffuse_sphere[2] = 1.0f; // Blue Light
		lights_sphere[2].lightDiffuse_sphere[3] = 1.0f; 

		lights_sphere[2].lightSpecular_sphere[0] = 0.0f;
		lights_sphere[2].lightSpecular_sphere[1] = 0.0f;
		lights_sphere[2].lightSpecular_sphere[2] = 1.0f;
		lights_sphere[2].lightSpecular_sphere[3] = 1.0f;

		resize_sphere(fboWidth, fboHeight);

		return(0);
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 a_position;" +
		"in vec2 a_texcoord;" +
		"uniform mat4 u_mvpMatrix;" +
		"out vec2 a_texcoord_out;" +
		"void main(void)" +
		"{" +
			"gl_Position = u_mvpMatrix * a_position;" +
			"a_texcoord_out = a_texcoord;" +
		"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 a_texcoord_out;" +
		"uniform highp sampler2D u_textureSampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = texture(u_textureSampler, a_texcoord_out);" +
		"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		mvpMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_textureSampler");

		// vao & vbo related code
		//declarations of vertex data arrays

		final float cubeVertices[] = new float[]
		{
			// top
			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			// bottom
			1.0f, -1.0f, -1.0f,
		   -1.0f, -1.0f, -1.0f,
		   -1.0f, -1.0f,  1.0f,
			1.0f, -1.0f,  1.0f,

			// front
			1.0f, 1.0f, 1.0f,
		   -1.0f, 1.0f, 1.0f,
		   -1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			// back
			1.0f, 1.0f, -1.0f,
		   -1.0f, 1.0f, -1.0f,
		   -1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,

			// right
			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			// left
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
		};

		final float cubeTexcoords[] = new float[]
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
		};

		// vao and vbo related code

		// cube
		GLES32.glGenVertexArrays(1, vao_cube,0);
		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glGenBuffers(1, vbo_cube,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_cube[0]);

		GLES32.glGenBuffers(1, vbo_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube[0]);

		ByteBuffer cubeByteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4); // byte madhil size
		cubeByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cubePositionBuffer = cubeByteBuffer.asFloatBuffer(); 
		cubePositionBuffer.put(cubeVertices);
		cubePositionBuffer.position(0); 
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeVertices.length * 4, cubePositionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// texcoord

		GLES32.glGenBuffers(1, vbo_cubetexcoord, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cubetexcoord[0]);

		ByteBuffer cbyteBuffer = ByteBuffer.allocateDirect(cubeTexcoords.length * 4); // byte madhil size
		cbyteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cpositionBuffer = cbyteBuffer.asFloatBuffer(); 
		cpositionBuffer.put(cubeTexcoords);
		cpositionBuffer.position(0); 
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeTexcoords.length * 4, cpositionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, 2, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glEnable(GLES32.GL_TEXTURE_2D);

		bFBO_Result = createFBO(fboWidth, fboHeight);

		int iRetVal;

		if (bFBO_Result == true)
		{
			iRetVal = initialize_sphere(fboWidth, fboWidth);
		}
		else
		{
			System.out.println("MDM- CreateFBO Failed " + log);
		}

		/*
		if (iRetVal != 0)
		{
			System.out.println("MDM- initialize_sphere Failed " + log);
		}
		*/

		GLES32.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		// code
		winWidth = width;
		winHeight = height;

		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display_sphere(int textureWidth, int textureHeight)
	{
		// lights directions
		// light 1 - x direction
		lights_sphere[0].lightPosition_sphere[0] = 0.0f; 
		lights_sphere[0].lightPosition_sphere[1] = 2 * (float) Math.sin(lightAngleZero);
		lights_sphere[0].lightPosition_sphere[2] = 2 * (float) Math.cos(lightAngleZero); 
		lights_sphere[0].lightPosition_sphere[3] = 1.0f;
		
		// light 2 - y direction
		lights_sphere[1].lightPosition_sphere[0] = 2 * (float) Math.sin(lightAngleZero); 
		lights_sphere[1].lightPosition_sphere[1] = 0.0f; 
		lights_sphere[1].lightPosition_sphere[2] = 2 *(float) Math.cos(lightAngleZero); 
		lights_sphere[1].lightPosition_sphere[3] = 1.0f; 

		// light 2 - z direction
		lights_sphere[2].lightPosition_sphere[0] = 2 *(float) Math.sin(lightAngleZero); 
		lights_sphere[2].lightPosition_sphere[1] = 2 *(float) Math.cos(lightAngleZero); 
		lights_sphere[2].lightPosition_sphere[2] = 0.0f; 
		lights_sphere[2].lightPosition_sphere[3] = 1.0f; 

		// code
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, fbo[0]);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		resize_sphere(textureWidth, textureHeight);

		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		if (singleTap == 0)
		{
			GLES32.glUseProgram(shaderProgramObject_Sphere_PV);
		}
		else if (singleTap == 1)
		{
			GLES32.glUseProgram(shaderProgramObject_Sphere_PF);
			//GLES32.glUseProgram(shaderProgramObject_PF);
		}
		//Here there drawing code

		// cube

		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float modelMatrix[]=new float[16];
		Matrix.setIdentityM(modelMatrix,0);

		float viewMatrix[]=new float[16];
		Matrix.setIdentityM(viewMatrix,0);

		float projectionMatrix[]=new float[16];
		Matrix.setIdentityM(projectionMatrix,0);

		float modelViewProjectionMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -2.0f);
		modelMatrix = translationMatrix;

		//Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelMatrix,0);

		GLES32.glBindVertexArray(vao_sphere[0]);

		if (singleTap == 0)
		{
			GLES32.glUniformMatrix4fv(modelMatrixUniform_sphere_PV, 1, false, modelMatrix,0); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
			GLES32.glUniformMatrix4fv(viewMatrixUniform_sphere_PV, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform_sphere_PV, 1, false, perspectiveProjectionMatrix,0);
		
			for (int i = 0; i < 3; i++)
			{
				GLES32.glUniform3fv(laUniform_sphere_PV[i], 1, lights_sphere[i].lightAmbient_sphere, 0);
				GLES32.glUniform3fv(ldUniform_sphere_PV[i], 1, lights_sphere[i].lightDiffuse_sphere, 0);
				GLES32.glUniform3fv(lsUniform_sphere_PV[i], 1, lights_sphere[i].lightSpecular_sphere, 0);
				GLES32.glUniform4fv(lightPositionUniform_sphere_PV[i], 1, lights_sphere[i].lightPosition_sphere, 0);
			}

			GLES32.glUniform3fv(kaUniform_sphere_PV, 1, materialAmbient_sphere,0);
			GLES32.glUniform3fv(kdUniform_sphere_PV, 1, materialDiffuse_sphere,0);
			GLES32.glUniform3fv(ksUniform_sphere_PV, 1, materialSpecular_sphere,0);
			GLES32.glUniform1f(materialShininessUniform_sphere_PV, materialShininess_sphere);

			if (bLight == true)
			{
				GLES32.glUniform1i(lightingEnabledUniform_sphere_PV, 1);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform_sphere_PV, 0);
			}
		}
		else if (singleTap == 1)
		{
			GLES32.glUniformMatrix4fv(modelMatrixUniform_sphere_PF, 1, false, modelMatrix,0); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
			GLES32.glUniformMatrix4fv(viewMatrixUniform_sphere_PF, 1, false, viewMatrix,0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform_sphere_PF, 1, false, perspectiveProjectionMatrix,0);
		
			for (int i = 0; i < 3; i++)
			{
				GLES32.glUniform3fv(laUniform_sphere_PF[i], 1, lights_sphere[i].lightAmbient_sphere, 0);
				GLES32.glUniform3fv(ldUniform_sphere_PF[i], 1, lights_sphere[i].lightDiffuse_sphere, 0);
				GLES32.glUniform3fv(lsUniform_sphere_PF[i], 1, lights_sphere[i].lightSpecular_sphere, 0);
				GLES32.glUniform4fv(lightPositionUniform_sphere_PF[i], 1, lights_sphere[i].lightPosition_sphere, 0);
			}

			GLES32.glUniform3fv(kaUniform_sphere_PF, 1, materialAmbient_sphere,0);
			GLES32.glUniform3fv(kdUniform_sphere_PF, 1, materialDiffuse_sphere,0);
			GLES32.glUniform3fv(ksUniform_sphere_PF, 1, materialSpecular_sphere,0);
			GLES32.glUniform1f(materialShininessUniform_sphere_PF, materialShininess_sphere);

			if (bLight == true)
			{
				GLES32.glUniform1i(lightingEnabledUniform_sphere_PF, 1);
			}
			else
			{
				GLES32.glUniform1i(lightingEnabledUniform_sphere_PF, 0);
			}
		}

		 // bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
	}

	private void update_sphere()
	{
		lightAngleZero = lightAngleZero + 0.03f;

		if (lightAngleZero >= 360.0f)
			lightAngleZero = lightAngleZero - 360.0f;
	}

	private void display()
	{
		if (bFBO_Result == true)
		{
			display_sphere(fboWidth, fboHeight);

			update_sphere();
		}

		GLES32.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		resize(winWidth, winHeight);

		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		// cube
		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float rotationMatrix[]=new float[16];
		Matrix.setIdentityM(rotationMatrix,0);

		float modelViewMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewMatrix,0);

		float modelViewProjectionMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);

		float rotationMatrix_X[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_X,0);

		float rotationMatrix_Y[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Y,0);

		float rotationMatrix_Z[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Z,0);

		float scaleMatrix[]=new float[16];
		Matrix.setIdentityM(scaleMatrix,0);

		float tempMatrix[]=new float[16];
		Matrix.setIdentityM(tempMatrix,0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -3.5f);

		Matrix.setRotateM(rotationMatrix_X, 0, angleCube, 1.0f, 0.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Y, 0, angleCube, 0.0f, 1.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Z, 0, angleCube, 0.0f, 0.0f, 1.0f);

		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);

		Matrix.multiplyMM(rotationMatrix_X,0,rotationMatrix_Y,0,rotationMatrix_Z,0);

		Matrix.multiplyMM(tempMatrix,0,translationMatrix,0,rotationMatrix_X,0);

		Matrix.multiplyMM(modelViewMatrix,0,tempMatrix,0,scaleMatrix,0);

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpMatrixUniform, 1, false, modelViewProjectionMatrix,0);
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, fbo[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		GLES32.glBindVertexArray(vao_cube[0]);

		// stepE 2 : draw the desiered graphics/animation
		// here will be magic code
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);

		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		// Here Will Be Drawing Code

		GLES32.glUseProgram(0);

		requestRender(); // swapbuffers or glxswapbuffers
	}

	private void update()
	{
		angleCube = angleCube + 0.5f;
		if (angleCube >= 360)
		{
			angleCube = angleCube - 360.0f;
		}
	}

	private void uninitialize_sphere()
	{
		if (vbo_sphere_position[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
			vbo_sphere_position[0] = 0;
		}

		if (vbo_sphere_normal[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
			vbo_sphere_normal[0] = 0;
		}

		if (vbo_sphere_elements[0] > 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_elements, 0);
			vbo_sphere_elements[0] = 0;
		}

		if (vao_sphere[0] > 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0] = 0;
		}

		if (shaderProgramObject_Sphere_PV > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject_Sphere_PV);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject_Sphere_PV, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject_Sphere_PV, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject_Sphere_PV, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject_Sphere_PV);
			shaderProgramObject_Sphere_PV = 0;
		}

		if (shaderProgramObject_Sphere_PF > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject_Sphere_PF);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject_Sphere_PF, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject_Sphere_PF, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject_Sphere_PF, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject_Sphere_PF);
			shaderProgramObject_Sphere_PF = 0;
		}
	}

	private void uninitialize()
	{
		uninitialize_sphere();

		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		if(fbo_texture[0]>0)
		{
			GLES32.glDeleteTextures(1, fbo_texture, 0);
			fbo_texture[0] = 0;
		}

		// deletion / uninitialization of vbo

		if (vbo_cube[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_cube,0);
			vbo_cube[0] = 0;
		}

		if (vbo_cubetexcoord[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_cubetexcoord,0);
			vbo_cubetexcoord[0] = 0;
		}

		// deletion / uninitialization of vao

		if (vao_cube[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube,0);
			vao_cube[0] = 0;
		}
	}
}
