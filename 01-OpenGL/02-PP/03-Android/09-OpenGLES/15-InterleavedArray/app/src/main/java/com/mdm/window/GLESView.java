package com.mdm.window;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.content.Context;

import android.graphics.Color;

import android.view.Gravity;

// event related packages
import android.view.GestureDetector;

import android.view.GestureDetector.OnDoubleTapListener;

import android.view.GestureDetector.OnGestureListener;

import android.view.MotionEvent;

// buffer related packages
import java.nio.ByteBuffer; // nio - native io or non blocking io (asynchronous)

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

// matrix package
import android.opengl.Matrix;

// texture related
import android.graphics.BitmapFactory;

import android.graphics.Bitmap;

import android.opengl.GLUtils;


public class GLESView extends GLSurfaceView implements OnGestureListener, OnDoubleTapListener, GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private Context context;

	// programable pipeline related veriable (ha variable purn class sathi global aahe)
	private int shaderProgramObject;

	private int vao_cube[]=new int[1];
	private int vbo_cube[]=new int[1];

	private int texture_marble[]=new int[1];

	private int textureSamplerUniform;
	
	// uniforms
	private int modelMatrixUniform; // hyacha address pass karaycha naahi mhanun array bracket nahi
	private int viewMatrixUniform;
	private int projectionMatrixUniform;

	private int laUniform; // light ambient
	private int ldUniform; // light diffuse
	private int lsUniform; // light specular
	private int lightPositionUniform;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShininessUniform;

	private int lightingEnabledUniform;

	private float lightAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
	private float lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
	private float lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
	private float lightPosition[] = { 0.0f,0.0f,2.0f,1.0f }; 

	private float materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
	private float materialDiffuse[] = { 0.5f,0.2f,0.7f,1.0f };
	private float materialSpecular[] = { 0.7f,0.7f,0.7f,1.0f };
	private float materialShininess = 128.0f;

	private float angleCube = 0.0f;

	private int doubleTap = 0;

	private float perspectiveProjectionMatrix[] = new float[16]; // matrices 16 cha linear array

	GLESView(Context _context)
	{
		super(_context);

		context = _context;

		setEGLContextClientVersion(3); // OpenGLES he internally EGL(Embbedded Graphics Library) use karto - ha 3 mhanje OpenGLES madhla aahe - OpenGL chi context set keli - windows madhye context convert karaychi garaj nahi karan ha karto toh internally
		setRenderer(this); // render kon class karnar aahe tar GLSurfaceView
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // dirty zhala view tar repaint karnyasathi - invalidaterect

		// gesture related code
		gestureDetector = new GestureDetector(context, this, null, false);
		// 1 - mala tujhi activity de,2 - mala kon aeiknar aahe(9 method implement), 3 - vegla class aahe ka implement karnar handling, 4 - false - reserved 
		gestureDetector.setOnDoubleTapListener(this);
	}

	// 3 methods of GLSurfaceView.Renderer
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String vendor = gl.glGetString(GL10.GL_VENDOR);
		System.out.println("MDM- OpenGL Vendor: " + vendor);

		String renderer = gl.glGetString(GL10.GL_RENDERER);
		System.out.println("MDM- OpenGL Renderer: " + renderer);

		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("MDM- OpenGL Version: " + glesVersion);

		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("MDM- OpenGL Shading Language Version: " + glslVersion);

		String extension = gl.glGetString(GL10.GL_EXTENSIONS);

		int count = 1;

		for(int i = 0; i < extension.length(); i++)
		{
			if (i == 0)
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				System.out.print(extension.charAt(i));
				count++;
			}
			else if (Character.isWhitespace(extension.charAt(i)))
			{
				System.out.println("");
				System.out.print("MDM- OpenGL Extension ");
				System.out.print(count);
				System.out.print(" ");
				count++;
			}
			else
			{
				System.out.print(extension.charAt(i));
			}
		}

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) // this should be considered as game loop
	{
		display();

		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//code
		if(!gestureDetector.onTouchEvent(e)) // nn touch event nahi zhala tar super madhun onTouchEvent la call jata
			super.onTouchEvent(e);

		return true;
	}

	// 3 methods of OnDoubleTapListner interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;

		if (doubleTap > 1)
			doubleTap = 0;

		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		

		return true;
	}

	// 6 methods of OnGestureListner
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) // fast swipe
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) // long press
	{
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//setText("Scroll/Swipe");
		uninitialize();
		System.exit(0);

		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) // show press
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}

	private int loadGLTexture(int imageResourceId)
	{
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inScaled = false;

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageResourceId, options);

		int texture [] = new int[1];
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		return texture[0];
	}

	// custom private functions
	private void initialize()
	{
		// vertex shader 
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 a_position;" +
		"in vec4 a_color;" +
		"in vec3 a_normal;" +
		"in vec2 a_texcoord;" +
		"uniform mat4 u_modelMatrix;" +
		"uniform mat4 u_viewMatrix;" +
		"uniform mat4 u_projectionMatrix;" +
		"uniform vec4 u_lightPosition;" +
		"uniform mediump int u_lightingEnabled;" +
		"out vec3 transformedNormals;" +
		"out vec3 lightDirection;" +
		"out vec3 viewerVector;" +
		"out vec4 a_color_out;" +
		"out vec2 a_texcoord_out;" +
		"void main(void)" +
		"{" +
		"if(u_lightingEnabled==1)" +
		"{" +
		"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
		"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
		"transformedNormals = normalMatrix * a_normal;" +
		"lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" +
		"viewerVector = -eyeCoordinates.xyz;" +
		"}" +
		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
		"a_color_out = a_color;" +
		"a_texcoord_out = a_texcoord;" +
		"}"
		);

		int vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[] = new int[1];
		int infoLogLength[] = new int[1];
		String log = null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(vertexShaderObject);

				System.out.println("MDM- Vertex Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// fragment shader 
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"uniform sampler2D u_textureSampler;" +
		"in vec4 a_color_out;" +
		"in vec2 a_texcoord_out;" +
		"in vec3 transformedNormals;" +
		"in vec3 lightDirection;" +
		"in vec3 viewerVector;" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShininess;" +
		"uniform mediump int u_lightingEnabled;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"vec3 phong_ads_light;" +
		"if(u_lightingEnabled==1)" +
		"{" +
			"vec3 ambient = u_la * u_ka;" +
			"vec3 normalized_transformedNormals = normalize(transformedNormals);" +
			"vec3 normalized_lightDirection = normalize(lightDirection);" +
			"vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" +
			"vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" +
			"vec3 normalized_viewerVector = normalize(viewerVector);" +
			"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" +
			"phong_ads_light = ambient + diffuse + specular;" +
		"}" +
		"else" +
		"{" +
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
		"}" +
			"FragColor = vec4(vec3(texture(u_textureSampler, a_texcoord_out)) * phong_ads_light * vec3(a_color_out), 1.0);" +
		"}"
		);

		int fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetShaderInfoLog(fragmentShaderObject);

				System.out.println("MDM- Fragment Shader compilation log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// Shader program
		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject); // konala chitkvayche ,konchitkvayche aahe
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_COLOR, "a_color");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

		// Link program - if not prelink then link program object
		GLES32.glLinkProgram(shaderProgramObject);

		// assignment of variables
		status[0] = 0;
		infoLogLength[0] = 0;
		log = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, status, 0);

		if(status[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);

			if(infoLogLength[0] > 0)
			{
				log = GLES32.glGetProgramInfoLog(shaderProgramObject);

				System.out.println("MDM- Shader Program link log : " + log);

				uninitialize();
				System.exit(0);
			}
		}

		// post linking uniform location
		textureSamplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_textureSampler");

		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

		// light uniform
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition");

		// material uniform
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");

		// light enable uniform
		lightingEnabledUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

		// vao & vbo related code
		//declarations of vertex data arrays

		final float cube_pcnt[] = new float[]
		{
			//front             //color-red         //normal-front      //texture-front
		1.0f, 1.0f, 1.0f,   1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   1.0f, 0.0f,
		-1.0f, 1.0f, 1.0f,  1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 1.0f,
		1.0f, -1.0f, 1.0f,  1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f,   1.0f, 1.0f,

		//right             //color-green       //normal-right      //texture-right
		1.0f, 1.0f, -1.0f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f,
		1.0f, 1.0f, 1.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   0.0f, 0.0f,
		1.0f, -1.0f, 1.0f,  0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   0.0f, 1.0f,
		1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,

		//back              //color-blue        //normal-back       //texture-back
		-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  1.0f, 0.0f,
		1.0f, 1.0f, -1.0f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,0.0f, 0.0f, 1.0f,   0.0f, 0.0f, -1.0f,  1.0f, 1.0f,

		//left              //color-cyan        //normal-left       //texture-back
		-1.0f, 1.0f, 1.0f,  0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
		-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  0.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
		-1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f,   -1.0f, 0.0f, 0.0f,  1.0f, 1.0f,

		//top               //color-magenta     //normal-top        //texture-top
		1.0f, 1.0f, -1.0f,  1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,
		-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f,
		-1.0f, 1.0f, 1.0f,  1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1.0f,
		1.0f, 1.0f, 1.0f,   1.0f, 0.0f, 1.0f,   0.0f, 1.0f, 0.0f,   1.0f, 1.0f,

		//bottom            //color-yellow      //normal-bottom     //texture-bottom
		1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  0.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  0.0f, 1.0f,
		1.0f, -1.0f, 1.0f,  1.0f, 1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  1.0f, 1.0f
		};

		System.out.println("MDM- before vao and vbo : " + log);

		// vao and vbo related code

		// cube
		GLES32.glGenVertexArrays(1, vao_cube,0);
		GLES32.glBindVertexArray(vao_cube[0]);

		GLES32.glGenBuffers(1, vbo_cube, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube[0]);

		ByteBuffer cubeByteBuffer = ByteBuffer.allocateDirect(cube_pcnt.length * 4); // byte madhil size
		cubeByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer cubePositionBuffer = cubeByteBuffer.asFloatBuffer(); 
		cubePositionBuffer.put(cube_pcnt);
		cubePositionBuffer.position(0); 
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cube_pcnt.length * 4, cubePositionBuffer, GLES32.GL_STATIC_DRAW);
		
		// position
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 11 * 4, 0);
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_POSITION);

		// Color
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 11 * 4, 3 * 4); 
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_COLOR);

		// Normal
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 11 * 4, 24);
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_NORMAL);

		// Texture
		GLES32.glVertexAttribPointer(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0, 2, GLES32.GL_FLOAT, false, 11 * 4, 36);
		GLES32.glEnableVertexAttribArray(MyGLESMacros.AMC_ATTRIBUTE_TEXTURE0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		System.out.println("MDM- after vao and vbo : " + log);

		//depth related changes
		GLES32.glClearDepthf(1.0f); // change glClearDepthf
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE); // culling mode enable hot yaane

		GLES32.glEnable(GLES32.GL_TEXTURE_2D);

		texture_marble[0]=loadGLTexture(R.raw.marble);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// initialization of perspectiveProjectionMatrix projection to identity
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
	}

	private void resize(int width, int height)
	{
		if (height <= 0)
			height = 1;

		GLES32.glViewport(0, 0, width, height);

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private void display()
	{
		//Code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);
		//Here there drawing code
		// pyramid
		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float modelMatrix[]=new float[16];
		Matrix.setIdentityM(modelMatrix,0);

		float viewMatrix[]=new float[16];
		Matrix.setIdentityM(viewMatrix,0);

		float modelViewProjectionMatrix[]=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		
		// cube

		float rotationMatrix_X[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_X,0);

		float rotationMatrix_Y[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Y,0);

		float rotationMatrix_Z[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_Z,0);

		float scaleMatrix[]=new float[16];
		Matrix.setIdentityM(scaleMatrix,0);

		float tempMatrix[]=new float[16];
		Matrix.setIdentityM(tempMatrix,0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -4.0f);
		//modelViewMatrix = translationMatrix;
		Matrix.setRotateM(rotationMatrix_X, 0, angleCube, 1.0f, 0.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Y, 0, angleCube, 0.0f, 1.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix_Z, 0, angleCube, 0.0f, 0.0f, 1.0f);

		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);

		Matrix.multiplyMM(rotationMatrix_X,0,rotationMatrix_Y,0,rotationMatrix_Z,0);

		Matrix.multiplyMM(tempMatrix,0,translationMatrix,0,rotationMatrix_X,0);

		Matrix.multiplyMM(modelMatrix,0,tempMatrix,0,scaleMatrix,0);

		//Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelMatrix,0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0); //glUniformMatrix4fv(kontya uniform made dhakalu, kiti matrix dhaklayche aahet ,matrix transpose karu ka, dhya to matix dhaklaycha aahe to)  
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_marble[0]);
		GLES32.glUniform1i(textureSamplerUniform, 0);

		if (doubleTap == 1)
		{
			GLES32.glUniform1i(lightingEnabledUniform, 1);

			GLES32.glUniform3fv(laUniform, 1, lightAmbient,0);
			GLES32.glUniform3fv(ldUniform, 1, lightDiffuse,0);
			GLES32.glUniform3fv(lsUniform, 1, lightSpecular,0);
			GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition,0);

			GLES32.glUniform3fv(kaUniform, 1, materialAmbient,0);
			GLES32.glUniform3fv(kdUniform, 1, materialDiffuse,0);
			GLES32.glUniform3fv(ksUniform, 1, materialSpecular,0);
			GLES32.glUniform1f(materialShininessUniform, materialShininess);
		}
		else
		{
			GLES32.glUniform1i(lightingEnabledUniform, 0);
		}

		GLES32.glBindVertexArray(vao_cube[0]);

		// stepE 2 : draw the desiered graphics/animation
		// here will be magic code
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

		GLES32.glUseProgram(0);

		requestRender(); // swapBuffer or glxswapBuffer sarkhe  
	}

	private void update()
	{
		angleCube = angleCube + 1.0f;
		if (angleCube >= 360)
		{
			angleCube = angleCube - 360.0f;
		}
	}

	private void uninitialize()
	{
		// code
		if (shaderProgramObject > 0) // java madhye arrays/classes/integers/floats boolean nahi karun shakat control flow madhye
		{
			GLES32.glUseProgram(shaderProgramObject);

			int retVal[] = new int[1];

			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal, 0);

			if (retVal[0] > 0)
			{
				int numAttachedShaders = retVal[0];

				int shaderObjects[] = new int[numAttachedShaders]; // jitki attached shader astil tevdhi memory de

				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal, 0, shaderObjects, 0); // 2nd para - kiti shader attached aahet, 3rd - actual kiti shaders dilet

				for (int i = 0; i < numAttachedShaders; i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}

			GLES32.glUseProgram(0);

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		if(texture_marble[0]>0)
		{
			GLES32.glDeleteTextures(1, texture_marble, 0);
			texture_marble[0] = 0;
		}

		if (vbo_cube[0]>0)
		{
			GLES32.glDeleteBuffers(1, vbo_cube,0);
			vbo_cube[0] = 0;
		}

		// deletion / uninitialization of vao

		if (vao_cube[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube,0);
			vao_cube[0] = 0;
		}
	}
}
