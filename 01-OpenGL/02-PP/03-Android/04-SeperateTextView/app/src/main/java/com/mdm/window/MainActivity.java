package com.mdm.window;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.graphics.Color;

public class MainActivity extends AppCompatActivity {

    private MyView myView; // class or field variable / member

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));

        myView = new MyView(this); // this means toh class swtah (this - to represent object of the class within itself)
        setContentView(myView);
    }
}