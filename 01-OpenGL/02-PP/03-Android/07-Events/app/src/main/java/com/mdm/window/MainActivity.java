package com.mdm.window;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.graphics.Color;

import android.view.Window;

import android.content.pm.ActivityInfo; // pm - package manager

// For Fullscreen package
import androidx.core.view.WindowCompat;

import androidx.core.view.WindowInsetsControllerCompat;

import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private MyView myView; // class or field variable / member

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // just like WM_CREATE
        
        // Hide Action Bar Or Title Bar
        getSupportActionBar().hide(); // android.app.actionbar
        
        // Full Screen Related Code
        WindowCompat.setDecorFitsSystemWindows(getWindow(), false); // fullscreen shi related kontahi shabd nahi ahe - android help

        // Hiding System Bars And IME (Input Method Editor)
        // WindowInsetsControllerCompat windowInsetsControllerCompat = ViewCompat.getWindowInsetsController(getWindow().getDecorView()); // getWindowInsetsController is deprecated api

        WindowInsetsControllerCompat windowInsetsControllerCompat = WindowCompat.getInsetsController(getWindow(), getWindow().getDecorView());
        // mala window insets controller Compat de kona kadun ViewCompat kadun kai call karun getWindowInsetsController tyala DecoreView Pass karun
        windowInsetsControllerCompat.hide(WindowInsetsCompat.Type.systemBars() | WindowInsetsCompat.Type.ime()); // je bar  milale te remove/hide karnyasathi
        
        // Forced Landscape Orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Setting Background Color
        getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));

        myView = new MyView(this); // this means toh class swtah (this - to represent object of the class within itself)
        setContentView(myView);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }
}
