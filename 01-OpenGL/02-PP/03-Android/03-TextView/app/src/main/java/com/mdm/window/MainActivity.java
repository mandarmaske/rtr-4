package com.mdm.window;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

// TextView Related Packages

import androidx.appcompat.widget.AppCompatTextView; // AppCompatTextView class sathi

import android.graphics.Color; // color sathi

import android.view.Gravity; // text align karnysathi

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0)); // getWindow().getDecorView().setBackgroundColor(Color.BLACK);
       // setContentView(R.layout.activity_main); - resources madhun xml file madhun aapan layout karu shakto
       AppCompatTextView myView = new AppCompatTextView(this);
       myView.setTextSize(64);
       myView.setTextColor(Color.GREEN); // or myview.setTextColor(Color.rgb(0, 255, 0));
       myView.setGravity(Gravity.CENTER);
       myView.setText("Hello, World!!!");

       setContentView(myView);
    }
}