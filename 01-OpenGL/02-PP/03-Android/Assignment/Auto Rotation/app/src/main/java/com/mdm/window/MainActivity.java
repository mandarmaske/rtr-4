package com.mdm.window;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.graphics.Color;

import android.view.View;

import android.view.Window;

import android.content.pm.ActivityInfo; // pm - package manager

public class MainActivity extends AppCompatActivity {

    private MyView myView; // class or field variable / member

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // just like WM_CREATE
        // FullScreen related Code
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | //  
                                                         View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | 
                                                         View.SYSTEM_UI_FLAG_IMMERSIVE); // maagchi window transparent hote jar chalu asel or immersion karayacha assel
        
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        
        // Auto Orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

        // Setting Background Color
        getWindow().getDecorView().setBackgroundColor(Color.rgb(0, 0, 0));

        myView = new MyView(this); // this means toh class swtah (this - to represent object of the class within itself)
        setContentView(myView);
    }
}