package com.vgn.window;


import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;
import android.content.Context;

//Event related packages
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

//Buffer related packages
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//Matrix Package
import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements OnDoubleTapListener,OnGestureListener,GLSurfaceView.Renderer
{
	private GestureDetector gestureDetector;
	private int shaderProgramObject;

	private int VAO_Cube[]=new int[1];
	private int VBO_Cube_Position[]=new int[1];
    private int VBO_Cube_Normals[]=new int[1];
    private int VBO_Cube_Color[]=new int[1];

	private int modelMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;

	private float angleCube;

	//Lights
	private int ldUniform;
	private int kdUniform;
	private int lightPositionUniform;

	private int lightingEnabledUniform;

	private float LightDiffuse[]=new float[4];
	private float LightPosition[]=new float[4];
	private float MaterialDiffuse[]=new float[4];

	private int singleTap;
	private int doubleTap;
	private float perspectiveProjectionMatrix[]=new float[16];
	private Context context;

	GLESView(Context _context)
	{
		super(_context);

		context=_context;

		setEGLContextClientVersion(3);//Opengl ES 3.2 Opengl based upon Android based ndk
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		//Gesture Related Code
		gestureDetector=new GestureDetector(context,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	//3 methods of GL surface view Renderer
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String glesVersion=gl.glGetString(GL10.GL_VERSION);
		System.out.println("vgn: "+glesVersion);

		String renderer=gl.glGetString(GL10.GL_RENDERER);
		System.out.println("vgn:renderer: "+renderer);

		String glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("vgn:glslVersion: "+glslVersion);

		initialize();
	}

	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		//On Drawframe should be considered gameLoop update
		update();
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		//Code
		if(!gestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);

			return true;
	}
	//3 methods of OnDoubleTapListener interface
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//Code
		doubleTap=doubleTap+1;
		if(doubleTap>1)
		{
			doubleTap=0;
		}
		return true;
	}
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//Code

		return true;
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		//Code
		singleTap=singleTap+1;
		if(singleTap>1)
		{
			singleTap=0;
		}
		return true;
	}

	//6 methods of OnGestureListener
	@Override
	public boolean onDown(MotionEvent e)
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
	{
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		//Code
	}

	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		//Code
		uninitialize();
		System.exit(0);
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e)
	{
				
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;	
	}

	//Custom private funtions
	private void initialize()
	{
		//Code
		//Vertex shader
		final String vertexShaderSourceCode=String.format
		(
				"#version 320 es" +
				"\n" +
				"in vec4 a_position;" +
				"in vec3 a_normal;" +
				"uniform mat4 u_modelMatrix;" +
				"uniform mat4 u_viewMatrix;" +
				"uniform mat4 u_projectionMatrix;" +
				"uniform vec3 u_ld;" +
				"uniform vec3 u_kd;" +
				"uniform vec4 u_lightPosition;" +
				"uniform mediump int u_lightingEnabled;" +
				"out vec3 diffuse_light_color;" +
				"void main(void)" +
				"{" +
				"if(u_lightingEnabled==1)" +
				"{" +
				"vec4 eyeCoordinates= u_viewMatrix*u_modelMatrix*a_position;" +
				"mat3 normalMatrix=mat3(transpose(inverse(u_viewMatrix*u_modelMatrix)));" +
				"vec3 transformedNormals=normalize(normalMatrix*a_normal);" +
				"vec3 lightDirection= normalize(vec3(u_lightPosition- eyeCoordinates));" +
				"diffuse_light_color=u_ld*u_kd*max(dot(lightDirection,transformedNormals),0.0);" +
				"}" +
				"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
				"}"
		);

		int vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);

		int status[]=new int[1];
		int infoLogLength[]=new int[1];
		String log=null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("vgn: Vertex Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment shader
		final String fragmentShaderSourceCode=String.format
		(
				"#version 320 es" +
				"\n" +
				"precision highp float;" +
				"in vec3 diffuse_light_color;" +
				"uniform mediump int u_lightingEnabled;" +
				"out vec4 FragColor;" +
				"void main(void)" +
				"{" +
				"if(u_lightingEnabled==1)" +
				"{" +
				"FragColor=vec4(diffuse_light_color,1.0);" +
				"}" +
				"else" +
				"{" +
				"FragColor = vec4(1.0,1.0,1.0,1.0);" +
				"}" +
				"}"
		);

		int fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("vgn: Fragment Shader Compilation log: "+log);
				uninitialize();
				System.exit(0);
			}
		}
		// Shader Program
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		//Pre linkg
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_COLOR, "a_color");
		GLES32.glBindAttribLocation(shaderProgramObject, MyGLESMacros.AMC_ATTRIBUTE_POSITION, "a_position");//Andhar
		GLES32.glLinkProgram(shaderProgramObject);

		status[0]=0;
		infoLogLength[0]=0;
		log=null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_COMPILE_STATUS, status,0);
		if(status[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
			if (infoLogLength[0] > 0)
			{
				log=GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("vgn: Shader Program Link log: "+log);
				uninitialize();
				System.exit(0);
			}
		}
		//Post Linking
		modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix");//Andhar
		viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

		ldUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		kdUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		lightPositionUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition");
		lightingEnabledUniform= GLES32.glGetUniformLocation(shaderProgramObject, "u_lightingEnabled");

		final float cubePosition[] = new float[]
				{
						// top
						1.0f, 1.0f, -1.0f,
						-1.0f, 1.0f, -1.0f,
						-1.0f, 1.0f, 1.0f,
						1.0f, 1.0f, 1.0f,

						//Bottom
						1.0f, -1.0f, -1.0f,
						-1.0f, -1.0f, -1.0f,
						-1.0f, -1.0f,  1.0f,
						1.0f, -1.0f,  1.0f,

						// front
						1.0f, 1.0f, 1.0f,
						-1.0f, 1.0f, 1.0f,
						-1.0f, -1.0f, 1.0f,
						1.0f, -1.0f, 1.0f,

						// back
						1.0f, 1.0f, -1.0f,
						-1.0f, 1.0f, -1.0f,
						-1.0f, -1.0f, -1.0f,
						1.0f, -1.0f, -1.0f,

						// right
						1.0f, 1.0f, -1.0f,
						1.0f, 1.0f, 1.0f,
						1.0f, -1.0f, 1.0f,
						1.0f, -1.0f, -1.0f,

						// left
						-1.0f, 1.0f, 1.0f,
						-1.0f, 1.0f, -1.0f,
						-1.0f, -1.0f, -1.0f,
						-1.0f, -1.0f, 1.0f
				};

		final float cubeColor[] = new float[]
		{
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
		};

		final float cubeNormals[] = new float[]
				{
						0.0f, 1.0f, 0.0f,  // top-right of top
						0.0f, 1.0f, 0.0f, // top-left of top
						0.0f, 1.0f, 0.0f, // bottom-left of top
						0.0f, 1.0f, 0.0f,  // bottom-right of top

						// bottom surface
						0.0f, -1.0f, 0.0f,  // top-right of bottom
						0.0f, -1.0f, 0.0f,  // top-left of bottom
						0.0f, -1.0f, 0.0f,  // bottom-left of bottom
						0.0f, -1.0f, 0.0f,   // bottom-right of bottom

						// front surface
						0.0f, 0.0f, 1.0f,  // top-right of front
						0.0f, 0.0f, 1.0f, // top-left of front
						0.0f, 0.0f, 1.0f, // bottom-left of front
						0.0f, 0.0f, 1.0f,  // bottom-right of front

						// back surface
						0.0f, 0.0f, -1.0f,  // top-right of back
						0.0f, 0.0f, -1.0f, // top-left of back
						0.0f, 0.0f, -1.0f, // bottom-left of back
						0.0f, 0.0f, -1.0f,  // bottom-right of back

						// right surface
						1.0f, 0.0f, 0.0f,  // top-right of right
						1.0f, 0.0f, 0.0f,  // top-left of right
						1.0f, 0.0f, 0.0f,  // bottom-left of right
						1.0f, 0.0f, 0.0f  // bottom-right of right

						// left surface
						-1.0f, 0.0f, 0.0f, // top-right of left
						-1.0f, 0.0f, 0.0f, // top-left of left
						-1.0f, 0.0f, 0.0f, // bottom-left of left
						-1.0f, 0.0f, 0.0f, // bottom-right of left
				};

		//VAO and VBO related code
		//Triangle
		GLES32.glGenVertexArrays(1, VAO_Cube,0);
		GLES32.glBindVertexArray(VAO_Cube[0]);

		GLES32.glGenBuffers(1, VBO_Cube_Position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,VBO_Cube_Position[0]);

		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(cubePosition.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer=byteBuffer.asFloatBuffer();
		positionBuffer.put(cubePosition);
		positionBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubePosition.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacro.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacro.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// color
		GLES32.glGenBuffers(1, VBO_Cube_Color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,VBO_Cube_Color[0]);

		byteBuffer=ByteBuffer.allocateDirect(cubeColor.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer=byteBuffer.asFloatBuffer();
		positionBuffer.put(cubeColor);
		positionBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeColor.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacro.AMC_ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacro.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		//Normals

		GLES32.glGenBuffers(1, VBO_Cube_Normals,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,VBO_Cube_Normals[0]);

		byteBuffer=ByteBuffer.allocateDirect(cubeNormals.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		positionBuffer=byteBuffer.asFloatBuffer();
		positionBuffer.put(cubeNormals);
		positionBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubeNormals.length*4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(MyGLESMacro.AMC_ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(MyGLESMacro.AMC_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		//Genral OPenGL
		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//Enable culling
		GLES32.glEnable(GLES32.GL_CULL_FACE_MODE);
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
		angleCube=0.0f;
		singleTap=0;
		doubleTap=0;
		LightDiffuse[0]=1.0f;
		LightDiffuse[1]=1.0f;
		LightDiffuse[2]=1.0f;
		LightDiffuse[3]=1.0f;

		MaterialDiffuse[0]=0.5f;
		MaterialDiffuse[1]=0.5f;
		MaterialDiffuse[2]=0.5f;
		MaterialDiffuse[3]=1.0f;

		LightPosition[0]=0.0f;
		LightPosition[1]=0.0f;
		LightPosition[2]=2.0f;
		LightPosition[3]=1.0f;


		//Initialization of projection matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}
	private void resize(int width,int height)
	{
		//Code
		if (height <= 0) // to avoid divied by 0 in future call
		height = 1;
		GLES32.glViewport(0,0,width,height);

		Matrix.perspectiveM(perspectiveProjectionMatrix ,0,45.0f, (float)width /(float)height, 0.1f, 100.0f);
	}
	private void display()
	{
		//Code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);
		//Here there drawing code
		//Triangle
		float translationMatrix[]=new float[16];
		Matrix.setIdentityM(translationMatrix,0);

		float scaleMatrix[]=new float[16];
		Matrix.setIdentityM(scaleMatrix,0);

		float rotationMatrix_x[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_x,0);

		float rotationMatrix_y[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_y,0);

		float rotationMatrix_z[]=new float[16];
		Matrix.setIdentityM(rotationMatrix_z,0);

		float rotationMatrix[]=new float[16];
		Matrix.setIdentityM(rotationMatrix,0);

		float modelMatrix[]=new float[16];
		Matrix.setIdentityM(modelMatrix,0);

		float viewMatrix[]=new float[16];
		Matrix.setIdentityM(viewMatrix,0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -4.0f);
		Matrix.scaleM(scaleMatrix, 0, 0.75f, 0.75f, 0.75f);
		//Matrix.setRotateM(rotationMatrix_x,0,angleCube, 1.0f, 0.0f, 0.0f);
		//Matrix.setRotateM(rotationMatrix_y,0,angleCube, 0.0f, 1.0f, 0.0f);
		Matrix.setRotateM(rotationMatrix,0,angleCube, 1.0f, 1.0f, 1.0f);

		//modelViewMatrix = translationMatrix;
		Matrix.multiplyMM(modelMatrix,0,translationMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrix,0);

		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix,0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix,0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix,0);

		//Sending Light related Uniforms

		if (doubleTap == 1)
		{
			GLES32.glUniform1i(lightingEnabledUniform,doubleTap);

			GLES32.glUniform3fv(ldUniform, 1, LightDiffuse,0);
			GLES32.glUniform3fv(kdUniform, 1, MaterialDiffuse,0);
			GLES32.glUniform4fv(lightPositionUniform, 1, LightPosition,0);
		}
		else
		{
			GLES32.glUniform1i(lightingEnabledUniform, 0);
		}


		GLES32.glBindVertexArray(VAO_Cube[0]);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);


		GLES32.glUseProgram(0);
		requestRender(); //Like swapbuffer
	}
	private void update()
	{
		if(singleTap==1) {
			angleCube = angleCube + 1.0f;
			if (angleCube >= 360) {
				angleCube = angleCube - 360.0f;
			}
		}
		else
		{
			angleCube=0;
		}
	}
	private void uninitialize()
	{
		//Code

		if(shaderProgramObject > 0)
		{
			GLES32.glUseProgram(shaderProgramObject);
			int retVal[]=new int[1];
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, retVal,0);
			if(retVal[0] > 0)
			{
				int numAttachedShaders=retVal[0];
				int shaderObjects[]=new int[numAttachedShaders];
				GLES32.glGetAttachedShaders(shaderProgramObject, numAttachedShaders, retVal,0, shaderObjects,0);

				for(int i=0; i<numAttachedShaders;i++)
				{
					GLES32.glDetachShader(shaderProgramObject, shaderObjects[i]);
					GLES32.glDeleteShader(shaderObjects[i]);
					shaderObjects[i] = 0;
				}
			}
			GLES32.glUseProgram(0);
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}

		if (VBO_Cube_Position[0]>0)
		{
			GLES32.glDeleteBuffers(1, VBO_Cube_Position,0);
			VBO_Cube_Position[0] = 0;
		}
		if (VBO_Cube_Normals[0]>0)
		{
			GLES32.glDeleteBuffers(1, VBO_Cube_Normals,0);
			VBO_Cube_Normals[0] = 0;
		}
		if (VBO_Cube_Color[0]>0)
		{
			GLES32.glDeleteBuffers(1, VBO_Cube_Color,0);
			VBO_Cube_Color[0] = 0;
		}
		if (VAO_Cube[0]>0)
		{
			GLES32.glDeleteVertexArrays(1, VAO_Cube,0);
			VAO_Cube[0] = 0;
		}
	}

}



