// JavaScript source code
function main() // function is keyword - onload la dilelya function la dilela case ani name he same pahije
{
    // code

    // 1) Get canvas
    var canvas = document.getElementById("MDM");
    // canvas is variable
    // document navchya object varti call kelelya function HTML madhla canvas id detoy
    // html file madhye jaaun ha canvas id gheun deto

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // 2) Get 2D context from canvas
    var context = canvas.getContext("2d"); // convention pramane 2d cha d small

    if (!context) {
        console.log("obtaining context failed\n");
    }
    else {
        console.log("context obtained\n");
    }

    // 3) Paint background of canvas by black color
    context.fillStyle = "black"; // in place of black we may write its hexadecimal code as - "#000000"
    // rang bharnyachi style fill style black aahe

    context.fillRect(0, 0, canvas.width, canvas.height);
}