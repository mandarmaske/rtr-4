// JavaScript source code

var canvas = null;
var bFullScreen = false;

function main() // function is keyword - onload la dilelya function la dilela case ani name he same pahije
{
    // code

    // 1) Get canvas
    canvas = document.getElementById("MDM");
    // canvas is variable
    // document navchya object varti call kelelya function HTML madhla canvas id detoy
    // html file madhye jaaun ha canvas id gheun deto

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // 2) Get 2D context from canvas
    var context = canvas.getContext("2d"); // convention pramane 2d cha d small

    if (!context) {
        console.log("obtaining context failed\n");
    }
    else {
        console.log("context obtained\n");
    }

    // 3) Paint background of canvas by black color
    context.fillStyle = "#000000";
    // in place of black we may write its hexadecimal code as - "#000000"
    // rang bharnyachi style fill style black aahe

    context.fillRect(0, 0, canvas.width, canvas.height);

    // String Display Steps
    // 1 - declare the string
    var str = "Hello World !!!";

    // 2 - Decide the font
    context.font = "48px sans-serif"; // 48 pixel size cha font de mhanane

    // 3 - Decide text color
    context.fillStyle = "green"; // #ffffff, varchi fillStyle ne keleli style black hoti te

    // 4 - Put the text in middle of the canvas horizontally and vertically
    context.textAlign = "center"; // this horizontally center
    context.textBaseline = "middle"; // this is vertically center

    // 5 - display the text
    context.fillText(str, canvas.width / 2, canvas.height / 2);
    // Para 1 - ji string display karaychi te

    // 4)  Adding keyboard, mouse event listners - callback function
    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    // window is inbuilt, canvas la represent karto window
    // 2nd parameter aapan kahihi lihu shakto, je double quote madhye lihle aahe te inbuilt aahet
    // 3rd parameter is captcher parameter - means, jya saglya class la inherited ahhe mi tyanna saglynna kaly de
    // true kela ki majhyapurta thev asa mhanane
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen) {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen) {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen) {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen) {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen) {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen) {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

// keyboard event listner
// 2nd parameter jo aahe te function chya pudhe naav lihne of addEventListener()
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 70:
            toggleFullScreen();
            drawText("Hello World!!!");
            break;
    }
}

function mouseDown()
{
    // code
}
