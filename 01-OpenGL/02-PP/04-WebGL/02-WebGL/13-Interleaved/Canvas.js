// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject; // var shaderProgramObject = null;

var vao_cube;
var vbo_cube_position;

var texture_marble = null;
var textureSamplerUniform;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var laUniform;
var ldUniform;
var lsUniform;
var lightPositionUniform;

var kaUniform;
var kdUniform;
var ksUniform;
var materialShininessUniform;
var lightingEnabledUniform;

var perspectiveProjectionMatrix;

var lightAmbient = [0.1, 0.1, 0.1, 1.0];
var lightDiffuse = [1.0, 1.0, 1.0, 1.0];
var lightSpecular = [1.0, 1.0, 1.0, 1.0];
var lightPosition = [0.0, 0.0, 2.0, 1.0];

var materialAmbient = [0.0, 0.0, 0.0, 1.0];
var materialDiffuse = [0.5, 0.2, 0.7, 1.0];
var materialSpecular = [0.7, 0.7, 0.7, 1.0];
var materialShininess = 128.0;

var angleCube = 0.0;

var bLight = false;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Vertex Shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec4 a_color;" +
        "in vec3 a_normal;" +
        "in vec2 a_texcoord;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec4 u_lightPosition;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 transformedNormals;" +
        "out vec3 lightDirection;" +
        "out vec3 viewerVector;" +
        "out vec4 a_color_out;" +
        "out vec2 a_texcoord_out;" +
        "void main(void)" +
        "{" +
            "if(u_lightingEnabled==1)" +
            "{" +
                "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
                "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
                "transformedNormals = normalMatrix * a_normal;" +
                "lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" +
                "viewerVector = -eyeCoordinates.xyz;" +
            "}" +
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
            "a_color_out = a_color;" +
            "a_texcoord_out = a_texcoord;" +
        "}";

    var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0)
        {
            alert("Vertex Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "uniform sampler2D u_textureSampler;" +
        "in vec4 a_color_out;" +
        "in vec2 a_texcoord_out;" +
        "in vec3 transformedNormals;" +
        "in vec3 lightDirection;" +
        "in vec3 viewerVector;" +
        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_materialShininess;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
            "vec3 phong_ads_light;" +
            "if(u_lightingEnabled==1)" +
            "{" +
                "vec3 ambient = u_la * u_ka;" +
                "vec3 normalized_transformedNormals = normalize(transformedNormals);" +
                "vec3 normalized_lightDirection = normalize(lightDirection);" +
                "vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" +
                "vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" +
                "vec3 normalized_viewerVector = normalize(viewerVector);" +
                "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" +
                "phong_ads_light = ambient + diffuse + specular;" +
            "}" +
            "else" +
            "{" +
                "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
            "}" +
            "FragColor = vec4(vec3(texture(u_textureSampler, a_texcoord_out)) * phong_ads_light * vec3(a_color_out), 1.0);" +
        "}";

    var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert("Fragment Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject = gl.createProgram();
    
    gl.attachShader(shaderProgramObject, vertexShaderObject); 
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_COLOR, "a_color");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

    // Shader program linking
    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0)
        {
            alert("Shader Program link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    // post linking uniform location
    textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "u_textureSampler");

    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projectionMatrix");

    // light uniform
    laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
    ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_lightPosition");

    // material uniform
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_materialShininess");

    // light enable uniform
    lightingEnabledUniform = gl.getUniformLocation(shaderProgramObject, "u_lightingEnabled");

    // vao & vbo related code
    var cube_pcnt = new Float32Array
    (
            [
            //front             //color-red         //normal-front      //texture-front
            1.0, 1.0, 1.0,      1.0, 0.0, 0.0,      0.0, 0.0, 1.0,       1.0, 0.0,
            -1.0, 1.0, 1.0,     1.0, 0.0, 0.0,      0.0, 0.0, 1.0,       0.0, 0.0,
            -1.0, -1.0, 1.0,    1.0, 0.0, 0.0,      0.0, 0.0, 1.0,       0.0, 1.0,
            1.0, -1.0, 1.0,     1.0, 0.0, 0.0,      0.0, 0.0, 1.0,       1.0, 1.0,

            //right             //color-green       //normal-right      //texture-right
            1.0, 1.0, -1.0,      0.0, 1.0, 0.0,     1.0, 0.0, 0.0,      1.0, 0.0,
            1.0, 1.0, 1.0,       0.0, 1.0, 0.0,     1.0, 0.0, 0.0,      0.0, 0.0,
            1.0, -1.0, 1.0,      0.0, 1.0, 0.0,     1.0, 0.0, 0.0,      0.0, 1.0,
            1.0, -1.0, -1.0,     0.0, 1.0, 0.0,     1.0, 0.0, 0.0,      1.0, 1.0,

            //back              //color-blue        //normal-back       //texture-back
            -1.0, 1.0, -1.0,     0.0, 0.0, 1.0,     0.0, 0.0, -1.0,     1.0, 0.0,
            1.0, 1.0, -1.0,      0.0, 0.0, 1.0,     0.0, 0.0, -1.0,     0.0, 0.0,
            1.0, -1.0, -1.0,     0.0, 0.0, 1.0,     0.0, 0.0, -1.0,     0.0, 1.0,
            -1.0, -1.0, -1.0,    0.0, 0.0, 1.0,     0.0, 0.0, -1.0,     1.0, 1.0,

            //left              //color-cyan        //normal-left       //texture-back
            -1.0, 1.0, 1.0,      0.0, 1.0, 1.0,     -1.0, 0.0, 0.0,      1.0, 0.0,
            -1.0, 1.0, -1.0,     0.0, 1.0, 1.0,     -1.0, 0.0, 0.0,      0.0, 0.0,
            -1.0, -1.0, -1.0,    0.0, 1.0, 1.0,     -1.0, 0.0, 0.0,      0.0, 1.0,
            -1.0, -1.0, 1.0,     0.0, 1.0, 1.0,     -1.0, 0.0, 0.0,      1.0, 1.0,

            //top               //color-magenta     //normal-top        //texture-top
            1.0, 1.0, -1.0,     1.0, 0.0, 1.0,      0.0, 1.0, 0.0,       1.0, 0.0,
            -1.0, 1.0, -1.0,    1.0, 0.0, 1.0,      0.0, 1.0, 0.0,       0.0, 0.0,
            -1.0, 1.0, 1.0,     1.0, 0.0, 1.0,      0.0, 1.0, 0.0,       0.0, 1.0,
            1.0, 1.0, 1.0,      1.0, 0.0, 1.0,      0.0, 1.0, 0.0,       1.0, 1.0,

            //bottom            //color-yellow      //normal-bottom     //texture-bottom
            1.0, -1.0, -1.0,    1.0, 1.0, 0.0,      0.0, -1.0, 0.0,      1.0, 0.0,
            -1.0, -1.0, -1.0,   1.0, 1.0, 0.0,      0.0, -1.0, 0.0,      0.0, 0.0,
            -1.0, -1.0, 1.0,    1.0, 1.0, 0.0,      0.0, -1.0, 0.0,      0.0, 1.0,
            1.0, -1.0, 1.0,     1.0, 1.0, 0.0,      0.0, -1.0, 0.0,      1.0, 1.0
            ]
    );

    // vao cube
    vao_cube = gl.createVertexArray();
    gl.bindVertexArray(vao_cube);

    // vbo cube
    vbo_cube_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_position);
    gl.bufferData(gl.ARRAY_BUFFER, cube_pcnt, gl.STATIC_DRAW);

    // position
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 11 * 4, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);

    // Color
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 11 * 4, 3 * 4);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);

    // Normal
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 11 * 4, 24);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_NORMAL);

    // Texture
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 11 * 4, 36);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_TEXTURE0);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // Clear the screen by blue color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    texture_marble = gl.createTexture();
    texture_marble.image = new Image();
    texture_marble.image.src = "marble.png";
    texture_marble.image.onload = function () {

        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.bindTexture(gl.TEXTURE_2D, texture_marble);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_marble.image);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if (canvas.height <= 0)
        canvas.height = 1;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Use the shader program object
    gl.useProgram(shaderProgramObject);

    // transformations cube
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var translationMatrix = mat4.create();
    var rotationMatrix = mat4.create();
    var rotationMatrix_X = mat4.create();
    var rotationMatrix_Y = mat4.create();
    var rotationMatrix_Z = mat4.create();
    var scaleMatrix = mat4.create();
    var temp = mat4.create();
    var temp1 = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -3.0]);
    mat4.scale(scaleMatrix, scaleMatrix, [0.75, 0.75, 0.75]);
    mat4.rotateX(rotationMatrix_X, rotationMatrix_X, degToRad(angleCube));
    mat4.rotateY(rotationMatrix_Y, rotationMatrix_Y, degToRad(angleCube));
    mat4.rotateZ(rotationMatrix_Z, rotationMatrix_Z, degToRad(angleCube));

    mat4.multiply(temp, rotationMatrix_Y, rotationMatrix_Z);
    mat4.multiply(rotationMatrix, temp, rotationMatrix_X);
    mat4.multiply(temp1, scaleMatrix, rotationMatrix);
    mat4.multiply(modelMatrix, translationMatrix, temp1);

    gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture_marble);
    gl.uniform1i(textureSamplerUniform, 0);

    if (bLight == true)
    {
        gl.uniform1i(lightingEnabledUniform, 1);

        gl.uniform3f(laUniform, lightAmbient[0], lightAmbient[1], lightAmbient[2]);
        gl.uniform3f(ldUniform, lightDiffuse[0], lightDiffuse[1], lightDiffuse[2]);
        gl.uniform3f(lsUniform, lightSpecular[0], lightSpecular[1], lightSpecular[2]);
        gl.uniform4f(lightPositionUniform, lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

        gl.uniform3f(kaUniform, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
        gl.uniform3f(kdUniform, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
        gl.uniform3f(ksUniform, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

        gl.uniform1f(materialShininessUniform, materialShininess);
    }
    else
    {
        gl.uniform1i(lightingEnabledUniform, 0);
    }

    gl.bindVertexArray(vao_cube);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

    gl.bindVertexArray(null);

    gl.useProgram(null);

    update();

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code

    angleCube = angleCube + 0.5;

    if (angleCube >= 360.0)
        angleCube = angleCube - 360.0;

}

function degToRad(degree)
{
    return (degree * Math.PI / 180.0);
}

// keyboard event listner
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 81: // case 27 escape he browser cha inbuilt aahe key
            uninitialize();
            window.close(); // not all browsers will follow this
            break;

        case 76:
            if (bLight == false)
            {
                bLight = true;
            }
            else
            {
                bLight = false;
            }
            break;

        case 70:
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo

    if (texture_marble)
    {
        gl.deleteTexture(texture_marble);
        texture_marble = null;
    }

    if (vbo_cube_position)
    {
        gl.deleteBuffer(vbo_cube_position);
        vbo_cube_position = null;
    }

    // deletion/ uninitialization of vao
    if (vao_cube)
    {
        gl.deleteVertexArray(vao_cube);
        vao_cube = null;
    }

    // shader uninitialization
    if (shaderProgramObject)
    {
        gl.useProgram(shaderProgramObject);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject);

        for (let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}
