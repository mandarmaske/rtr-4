// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject; // var shaderProgramObject = null

var vao_cube;
var vbo_cube;
var vbo_cube_texcoord;

var textureSamplerUniform;

var mvpMatrixUniform;
var perspectiveProjectionMatrix;

var angleCube = 0.0;

// sphere related code
var shaderProgramObjectPF;

var vao_sphere = null;

// Per Vertex
var modelMatrixUniformPV;
var viewMatrixUniformPV;
var projectionMatrixUniformPV;

var laUniformPV = new Array(3);
var ldUniformPV = new Array(3);
var lsUniformPV = new Array(3);
var lightPositionUniformPV = new Array(3);

var kaUniformPV;
var kdUniformPV;
var ksUniformPV;

var materialShininessUniformPV;
var lightingEnabledUniformPV;

// Per Fragment
var modelMatrixUniformPF;
var viewMatrixUniformPF;
var projectionMatrixUniformPF;

var laUniformPF = new Array(3);
var ldUniformPF = new Array(3);
var lsUniformPF = new Array(3);
var lightPositionUniformPF = new Array(3);

var kaUniformPF;
var kdUniformPF;
var ksUniformPF;

var materialShininessUniformPF;
var lightingEnabledUniformPF;

// light 1 - Red
var lightAmbient1 = [0.0, 0.0, 0.0, 1.0];
var lightDiffuse1 = [1.0, 0.0, 0.0, 1.0];
var lightSpecular1 = [1.0, 0.0, 0.0, 1.0];
var lightPosition1 = new Array(4);

// light 1 - Green
var lightAmbient2 = [0.0, 0.0, 0.0, 1.0];
var lightDiffuse2 = [0.0, 1.0, 0.0, 1.0];
var lightSpecular2 = [0.0, 1.0, 0.0, 1.0];
var lightPosition2 = new Array(4);

// light 1 - Blue
var lightAmbient3 = [0.0, 0.0, 0.0, 1.0];
var lightDiffuse3 = [0.0, 0.0, 1.0, 1.0];
var lightSpecular3 = [0.0, 0.0, 1.0, 1.0];
var lightPosition3 = new Array(4);

// material white
var materialAmbient = [0.0, 0.0, 0.0, 1.0];
var materialDiffuse = [1.0, 1.0, 1.0, 1.0];
var materialSpecular = [1.0, 1.0, 1.0, 1.0];
var materialShininess = 128.0;

var perspectiveProjectionMatrix_Sphere;

var lightAngleZero = 0.0;
var bLight = false;

var fbo_width = 512;
var fbo_height = 512;

var WINWIDTH = 800;
var WINHEIGHT = 600;

var winWidth;
var winHeight;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Vertex Shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec2 a_texcoord;" +
        "uniform mat4 u_mvpMatrix;" +
        "out vec2 a_texcoord_out;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvpMatrix * a_position;" +
        "a_texcoord_out=a_texcoord;" +
        "}";

    var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0) {
            alert("Vertex Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec2 a_texcoord_out;" +
        "uniform sampler2D u_textureSampler;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = texture(u_textureSampler,a_texcoord_out);" +
        "}";

    var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert("Fragment Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject = gl.createProgram();

    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_TEXTURE0, "a_texcoord");

    // Shader program linking
    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0) {
            alert("Shader Program link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_mvpMatrix");
    textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "u_textureSampler");

    // vao_cube & vbo_cube related code

    var cubePosition = new Float32Array
        (
            [
                // top
                1.0, 1.0, -1.0,
                -1.0, 1.0, -1.0,
                -1.0, 1.0, 1.0,
                1.0, 1.0, 1.0,

                // bottom
                1.0, -1.0, -1.0,
                -1.0, -1.0, -1.0,
                -1.0, -1.0, 1.0,
                1.0, -1.0, 1.0,

                // front
                1.0, 1.0, 1.0,
                -1.0, 1.0, 1.0,
                -1.0, -1.0, 1.0,
                1.0, -1.0, 1.0,

                // back
                1.0, 1.0, -1.0,
                -1.0, 1.0, -1.0,
                -1.0, -1.0, -1.0,
                1.0, -1.0, -1.0,

                // right
                1.0, 1.0, -1.0,
                1.0, 1.0, 1.0,
                1.0, -1.0, 1.0,
                1.0, -1.0, -1.0,

                // left
                -1.0, 1.0, 1.0,
                -1.0, 1.0, -1.0,
                -1.0, -1.0, -1.0,
                -1.0, -1.0, 1.0,
            ]
        );

    var cubeTexcoord = new Float32Array
        (
            [
                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,

                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,

                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,

                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,

                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,

                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0
            ]
        );

    // vao_cube realted code
    vao_cube = gl.createVertexArray();
    gl.bindVertexArray(vao_cube);

    // vbo_cube
    vbo_cube = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube);
    gl.bufferData(gl.ARRAY_BUFFER, cubePosition, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // vbo for color
    vbo_cube_texcoord = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_texcoord);
    gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // clear the screen by blue color
    gl.clearColor(1.0, 1.0, 1.0, 1.0); //blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix = mat4.create();

    // warmup resize call
    resize(WINWIDTH, WINHEIGHT);

    // fbo code
    bFBOResult = createFBO(fbo_width, fbo_height);
    var iRetVal;
    if (bFBOResult == true) {
        iRetVal = initialize_Sphere(fbo_width, fbo_height);
    }
    else {
        return (-6);
    }

    return (0);
}

function createFBO(textureWidth, textureHeight) {
    // code
    // create frame buffer object
    fbo = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);

    // create empty texture for upcoming target scene
    fbo_texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, fbo_texture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

    gl.texImage2D(gl.TEXTURE_2D,
        0,
        gl.RGB,
        textureWidth,
        textureHeight,
        0,
        gl.RGB,
        gl.UNSIGNED_SHORT_5_6_5,
        null);

    // give above texture to fbo
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, fbo_texture, 0);

    // unbind fbo (texture & rbo aslo unbind with this)
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    return (true);
}

function initialize_Sphere() {
    // code
    // Shader Code
    // Vertex Shader perfragment
    var vertexShaderSourceCodePF =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec3 a_normal;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec4 u_lightPosition[3];" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 transformedNormals;" +
        "out vec3 lightDirection[3];" +
        "out vec3 viewerVector;" +
        "void main(void)" +
        "{" +
        "if(u_lightingEnabled == 1)" +
        "{" +
        "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
        "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
        "transformedNormals = normalMatrix * a_normal;" +
        "viewerVector = -eyeCoordinates.xyz;" +
        "for(int i = 0; i < 3; i++)" +
        "{" +
        "lightDirection[i] = vec3(u_lightPosition[i]) - eyeCoordinates.xyz;" +
        "}" +
        "}" +
        "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
        "}";

    var vertexShaderObjectPF = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObjectPF, vertexShaderSourceCodePF);
    gl.compileShader(vertexShaderObjectPF);

    if (gl.getShaderParameter(vertexShaderObjectPF, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObjectPF);

        if (error.length > 0) {
            alert("Vertex Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCodePF =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 transformedNormals;" +
        "in vec3 lightDirection[3];" +
        "in vec3 viewerVector;" +
        "uniform vec3 u_la[3];" +
        "uniform vec3 u_ld[3];" +
        "uniform vec3 u_ls[3];" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_materialShininess;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "vec3 phong_ads_light;" +
        "if(u_lightingEnabled == 1)" +
        "{" +
        "vec3 ambient[3];" +
        "vec3 diffuse[3];" +
        "vec3 reflectionVector[3];" +
        "vec3 specular[3];" +
        "vec3 normalized_transformed_normal;" +
        "vec3 normalized_lightDirection[3];" +
        "vec3 normalized_viewerVector;" +
        "normalized_transformed_normal = normalize(transformedNormals); " +
        "normalized_viewerVector = normalize(viewerVector);" +
        "for(int i = 0; i < 3; i++)" +
        "{" +
        "normalized_lightDirection[i] = normalize(lightDirection[i]);" +
        "ambient[i] = u_la[i] * u_ka;" +
        "diffuse[i] = u_ld[i] * u_kd * max(dot(normalized_lightDirection[i], normalized_transformed_normal), 0.0);" +
        "reflectionVector[i] = reflect(-normalized_lightDirection[i], normalized_transformed_normal);" +
        "specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalized_viewerVector), 0.0), u_materialShininess);" +
        "phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
        "}" +
        "}" +
        "else" +
        "{" +
        "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
        "}" +
        "FragColor = vec4(phong_ads_light,1.0);" +
        "}";

    var fragmentShaderObjectPF = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObjectPF, fragmentShaderSourceCodePF);
    gl.compileShader(fragmentShaderObjectPF);

    if (gl.getShaderParameter(fragmentShaderObjectPF, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObjectPF);

        if (error.length > 0) {
            alert("Fragment Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObjectPF = gl.createProgram();

    gl.attachShader(shaderProgramObjectPF, vertexShaderObjectPF);
    gl.attachShader(shaderProgramObjectPF, fragmentShaderObjectPF);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObjectPF, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObjectPF, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObjectPF);

    if (gl.getProgramParameter(shaderProgramObjectPF, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgramObjectPF);

        if (error.length > 0) {
            alert("Shader Program link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    modelMatrixUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_modelMatrix");
    viewMatrixUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_viewMatrix");
    projectionMatrixUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_projectionMatrix");

    // light uniform
    laUniformPF[0] = gl.getUniformLocation(shaderProgramObjectPF, "u_la[0]");
    ldUniformPF[0] = gl.getUniformLocation(shaderProgramObjectPF, "u_ld[0]");
    lsUniformPF[0] = gl.getUniformLocation(shaderProgramObjectPF, "u_ls[0]");
    lightPositionUniformPF[0] = gl.getUniformLocation(shaderProgramObjectPF, "u_lightPosition[0]");

    // light uniform1
    laUniformPF[1] = gl.getUniformLocation(shaderProgramObjectPF, "u_la[1]");
    ldUniformPF[1] = gl.getUniformLocation(shaderProgramObjectPF, "u_ld[1]");
    lsUniformPF[1] = gl.getUniformLocation(shaderProgramObjectPF, "u_ls[1]");
    lightPositionUniformPF[1] = gl.getUniformLocation(shaderProgramObjectPF, "u_lightPosition[1]");

    // light uniform2
    laUniformPF[2] = gl.getUniformLocation(shaderProgramObjectPF, "u_la[2]");
    ldUniformPF[2] = gl.getUniformLocation(shaderProgramObjectPF, "u_ld[2]");
    lsUniformPF[2] = gl.getUniformLocation(shaderProgramObjectPF, "u_ls[2]");
    lightPositionUniformPF[2] = gl.getUniformLocation(shaderProgramObjectPF, "u_lightPosition[2]");

    // material uniform
    kaUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_ka");
    kdUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_kd");
    ksUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_ks");
    materialShininessUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_materialShininess");

    // light enable uniform
    lightingEnabledUniformPF = gl.getUniformLocation(shaderProgramObjectPF, "u_lightingEnabled");

    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // clear the screen by blue color
    gl.clearColor(1.0, 1.0, 1.0, 1.0); //blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix_Sphere = mat4.create();

    // warmup resize call
    resize_Sphere(fbo_width, fbo_height);
}

function resize()
{
    // code
    if (bFullScreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    winWidth = canvas.width;
    winHeight = canvas.height;

    if (canvas.height <= 0)
        canvas.height = 1;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function resize_Sphere(width, height) {
    //code

    if (height == 0) {
        height = 1;
    }

    gl.viewport(0, 0, width, height);

    mat4.perspective(perspectiveProjectionMatrix_Sphere,
        45.0,
        parseFloat(width) / parseFloat(height),
        0.1, 100.0);
}

function display()
{
    // code
    if (bFBOResult == true) {
        display_Sphere(fbo_width, fbo_height);
        update_Sphere();
    }

    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    resize(winWidth, winHeight);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // use the shader program object
    gl.useProgram(shaderProgramObject);

    // transformations
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var translationMatrix = mat4.create();
    var rotationMatrix = mat4.create();
    var rotationMatrix_X = mat4.create();
    var rotationMatrix_Y = mat4.create();
    var rotationMatrix_Z = mat4.create();
    var scaleMatrix = mat4.create();
    var tempMatrix = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -3.0]);
    mat4.rotateX(rotationMatrix_X, rotationMatrix_X, degToRad(angleCube));
    mat4.rotateY(rotationMatrix_Y, rotationMatrix_Y, degToRad(angleCube));
    mat4.rotateZ(rotationMatrix_Z, rotationMatrix_Z, degToRad(angleCube));
    mat4.scale(scaleMatrix, scaleMatrix, [0.75, 0.75, 0.75]);

    mat4.multiply(rotationMatrix_X, rotationMatrix_Y, rotationMatrix_Z);
    mat4.multiply(tempMatrix, translationMatrix, rotationMatrix_X);
    mat4.multiply(modelViewMatrix, tempMatrix, scaleMatrix);

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

    gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix);

    gl.activeTexture(gl.TEXTURE0);
    //gl.bindTexture(gl.TEXTURE_2D, texture_kundali);
    gl.uniform1i(textureSamplerUniform, 0);

    gl.bindVertexArray(vao_cube);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

    gl.bindVertexArray(null);

    gl.useProgram(null);

    // update
    update();

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code
    angleCube = angleCube + 1.0;

    if (angleCube >= 360.0)
        angleCube = angleCube - 360.0;
}

function display_Sphere(texture_Width, texture_Height) {
    // code

    // bind frame buffer
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    resize_Sphere(texture_Width, texture_Height);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // lights directions
    // light 1 - Xdirection
    var lightPosition = ([0.0, 100 * Math.sin(lightAngleZero), 100 * Math.cos(lightAngleZero), 1.0]);

    lightPosition = []
    lightPosition1[0] = 0.0;
    lightPosition1[1] = 80 * Math.sin(lightAngleZero);
    lightPosition1[2] = 80 * Math.cos(lightAngleZero);
    lightPosition1[3] = 1.0;

    // light 2 - y direction
    lightPosition2[0] = 80 * Math.sin(lightAngleZero);
    lightPosition2[1] = 0.0;
    lightPosition2[2] = 80 * Math.cos(lightAngleZero);
    lightPosition2[3] = 1.0;

    // light 2 - z direction
    lightPosition3[0] = 80 * Math.sin(lightAngleZero);
    lightPosition3[1] = 80 * Math.cos(lightAngleZero);
    lightPosition3[2] = 0.0;
    lightPosition3[3] = 1.0;

    // use the shader program object

    gl.useProgram(shaderProgramObjectPF);

    // transformations
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var translationMatrix = mat4.create();
    var scaleMatrix = mat4.create();
    var tempMatrix = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -10.0]);

    //mat4.scale(scaleMatrix,scaleMatrix,[0.75,0.75,0.75]);
    //modelMatrix = translationMatrix;

    mat4.multiply(modelMatrix, translationMatrix, tempMatrix);


    gl.uniformMatrix4fv(modelMatrixUniformPF, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniformPF, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniformPF, false, perspectiveProjectionMatrix_Sphere);

    gl.uniform3f(laUniformPF[0], lightAmbient1[0], lightAmbient1[1], lightAmbient1[2]);
    gl.uniform3f(ldUniformPF[0], lightDiffuse1[0], lightDiffuse1[1], lightDiffuse1[2]);
    gl.uniform3f(lsUniformPF[0], lightSpecular1[0], lightSpecular1[1], lightSpecular1[2]);
    gl.uniform4f(lightPositionUniformPF[0], lightPosition1[0], lightPosition1[1], lightPosition1[2], lightPosition1[3]);

    gl.uniform3f(laUniformPF[1], lightAmbient2[0], lightAmbient2[1], lightAmbient2[2]);
    gl.uniform3f(ldUniformPF[1], lightDiffuse2[0], lightDiffuse2[1], lightDiffuse2[2]);
    gl.uniform3f(lsUniformPF[1], lightSpecular2[0], lightSpecular2[1], lightSpecular2[2]);
    gl.uniform4f(lightPositionUniformPF[1], lightPosition2[0], lightPosition2[1], lightPosition2[2], lightPosition2[3]);

    gl.uniform3f(laUniformPF[2], lightAmbient3[0], lightAmbient3[1], lightAmbient3[2]);
    gl.uniform3f(ldUniformPF[2], lightDiffuse3[0], lightDiffuse3[1], lightDiffuse3[2]);
    gl.uniform3f(lsUniformPF[2], lightSpecular3[0], lightSpecular3[1], lightSpecular3[2]);
    gl.uniform4f(lightPositionUniformPF[2], lightPosition3[0], lightPosition3[1], lightPosition3[2], lightPosition3[3]);

    gl.uniform3f(kaUniformPF, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
    gl.uniform3f(kdUniformPF, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
    gl.uniform3f(ksUniformPF, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

    gl.uniform1f(materialShininessUniformPF, materialShininess);

    if (bLight == true) {
        gl.uniform1i(lightingEnabledUniformPF, 1);
    }
    else {
        gl.uniform1i(lightingEnabledUniformPF, 0);
    }

    sphere.draw();

    gl.useProgram(null);

    // update
    // update_Sphere();

    // unbind frame buffer
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    // double buffering emulation
    //requestAnimationFrame(display,canvas); // display la call honar, konavarti tr canvas. ya call ne repetition mention kele jate
}

function update_Sphere() {
    // code
    lightAngleZero = lightAngleZero + 0.05;

    if (lightAngleZero >= 360.0)
        lightAngleZero = lightAngleZero - 360.0;

}

function display_Sphere(texture_Width, texture_Height) {
    // code

    // bind frame buffer
    gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    resize_Sphere(texture_Width, texture_Height);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // lights directions
    // light 1 - Xdirection
    var lightPosition = ([0.0, 100 * Math.sin(lightAngleZero), 100 * Math.cos(lightAngleZero), 1.0]);

    lightPosition = []
    lightPosition1[0] = 0.0;
    lightPosition1[1] = 80 * Math.sin(lightAngleZero);
    lightPosition1[2] = 80 * Math.cos(lightAngleZero);
    lightPosition1[3] = 1.0;

    // light 2 - y direction
    lightPosition2[0] = 80 * Math.sin(lightAngleZero);
    lightPosition2[1] = 0.0;
    lightPosition2[2] = 80 * Math.cos(lightAngleZero);
    lightPosition2[3] = 1.0;

    // light 2 - z direction
    lightPosition3[0] = 80 * Math.sin(lightAngleZero);
    lightPosition3[1] = 80 * Math.cos(lightAngleZero);
    lightPosition3[2] = 0.0;
    lightPosition3[3] = 1.0;

    // use the shader program object

    gl.useProgram(shaderProgramObjectPF);

    // transformations
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var translationMatrix = mat4.create();
    var scaleMatrix = mat4.create();
    var tempMatrix = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -10.0]);

    //mat4.scale(scaleMatrix,scaleMatrix,[0.75,0.75,0.75]);
    //modelMatrix = translationMatrix;

    mat4.multiply(modelMatrix, translationMatrix, tempMatrix);


    gl.uniformMatrix4fv(modelMatrixUniformPF, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniformPF, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniformPF, false, perspectiveProjectionMatrix_Sphere);

    gl.uniform3f(laUniformPF[0], lightAmbient1[0], lightAmbient1[1], lightAmbient1[2]);
    gl.uniform3f(ldUniformPF[0], lightDiffuse1[0], lightDiffuse1[1], lightDiffuse1[2]);
    gl.uniform3f(lsUniformPF[0], lightSpecular1[0], lightSpecular1[1], lightSpecular1[2]);
    gl.uniform4f(lightPositionUniformPF[0], lightPosition1[0], lightPosition1[1], lightPosition1[2], lightPosition1[3]);

    gl.uniform3f(laUniformPF[1], lightAmbient2[0], lightAmbient2[1], lightAmbient2[2]);
    gl.uniform3f(ldUniformPF[1], lightDiffuse2[0], lightDiffuse2[1], lightDiffuse2[2]);
    gl.uniform3f(lsUniformPF[1], lightSpecular2[0], lightSpecular2[1], lightSpecular2[2]);
    gl.uniform4f(lightPositionUniformPF[1], lightPosition2[0], lightPosition2[1], lightPosition2[2], lightPosition2[3]);

    gl.uniform3f(laUniformPF[2], lightAmbient3[0], lightAmbient3[1], lightAmbient3[2]);
    gl.uniform3f(ldUniformPF[2], lightDiffuse3[0], lightDiffuse3[1], lightDiffuse3[2]);
    gl.uniform3f(lsUniformPF[2], lightSpecular3[0], lightSpecular3[1], lightSpecular3[2]);
    gl.uniform4f(lightPositionUniformPF[2], lightPosition3[0], lightPosition3[1], lightPosition3[2], lightPosition3[3]);

    gl.uniform3f(kaUniformPF, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
    gl.uniform3f(kdUniformPF, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
    gl.uniform3f(ksUniformPF, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

    gl.uniform1f(materialShininessUniformPF, materialShininess);

    if (bLight == true) {
        gl.uniform1i(lightingEnabledUniformPF, 1);
    }
    else {
        gl.uniform1i(lightingEnabledUniformPF, 0);
    }

    sphere.draw();

    gl.useProgram(null);

    // unbind frame buffer
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
}

function update_Sphere() {
    // code
    lightAngleZero = lightAngleZero + 0.05;

    if (lightAngleZero >= 360.0)
        lightAngleZero = lightAngleZero - 360.0;

}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

// keyboard event listner
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 70:
            toggleFullScreen();
            break;

        case 76:
            if (bLight == false)
            {
                bLight = true;
            }
            else
            {
                bLight = false;
            }
            break;

        case 81: 
            uninitialize();
            window.close(); 
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo
    if (vbo_cube_texcoord) {
        gl.deleteBuffer(vbo_cube_texcoord);
        vbo_cube_texcoord = null;
    }

    if (vbo_cube) {
        gl.deleteBuffer(vbo_cube);
        vbo_cube = null;
    }

    // deletion/ uninitialization of vao_cube
    if (vao_cube) {
        gl.deleteVertexArray(vao_cube);
        vao_cube = null;
    }

    // shader uninitialization
    if (shaderProgramObject) {
        gl.useProgram(shaderProgramObject);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject);

        for (let i = 0; i < shaderObjects.length; i++) {
            gl.detachShader(shaderProgramObject, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = 0;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }

    // shader uninitialization
    if (shaderProgramObjectPF) {
        gl.useProgram(shaderProgramObjectPF);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObjectPF);

        for (let i = 0; i < shaderProgramObjectPF.length; i++) {
            gl.detachShader(shaderProgramObjectPF, shaderObjects[i]);
            gl.deleteShader(shaderProgramObjectPF[i]);
            shaderProgramObjectPF[i] = 0;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObjectPF);
        shaderProgramObjectPF = null;
    }
}
