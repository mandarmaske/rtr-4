// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject_PV; 
var shaderProgramObject_PF; 

var sphere = null;

// Per Vertex
var modelMatrixUniform_PV;
var viewMatrixUniform_PV;
var projectionMatrixUniform_PV;

var laUniform_PV = new Array(3);
var ldUniform_PV = new Array(3);
var lsUniform_PV = new Array(3);
var lightPositionUniform_PV = new Array(3);

var kaUniform_PV;
var kdUniform_PV;
var ksUniform_PV;

var materialShininessUniform_PV;
var lightingEnabledUniform_PV;

// Per Fragment
var modelMatrixUniform_PF;
var viewMatrixUniform_PF;
var projectionMatrixUniform_PF;

var laUniform_PF = new Array(3);
var ldUniform_PF = new Array(3);
var lsUniform_PF = new Array(3);
var lightPositionUniform_PF = new Array(3);

var kaUniform_PF;
var kdUniform_PF;
var ksUniform_PF;

var materialShininessUniform_PF;
var lightingEnabledUniform_PF;

var bLight = false;

// light 1 - Red
var lightAmbient1 = [0.0, 0.0, 0.0, 1.0];
var lightDiffuse1 = [1.0, 0.0, 0.0, 1.0];
var lightSpecular1 = [1.0, 0.0, 0.0, 1.0];
var lightPosition1 = new Array(4);

// light 1 - Green
var lightAmbient2 = [0.0, 0.0, 0.0, 1.0];
var lightDiffuse2 = [0.0, 1.0, 0.0, 1.0];
var lightSpecular2 = [0.0, 0.0, 1.0, 1.0];
var lightPosition2 = new Array(4);

// light 1 - Blue
var lightAmbient3 = [0.0, 0.0, 0.0, 1.0];
var lightDiffuse3 = [0.0, 0.0, 1.0, 1.0];
var lightSpecular3 = [0.0, 0.0, 1.0, 1.0];
var lightPosition3 = new Array(4);

// material white
var materialAmbient = [0.0, 0.0, 0.0, 1.0];
var materialDiffuse = [1.0, 1.0, 1.0, 1.0];
var materialSpecular = [1.0, 1.0, 1.0, 1.0];

var materialShininess = 128.0;

var lightAngle = 0.0;

var perspectiveProjectionMatrix;

var counter = 1;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Per Vertex
    // Vertex Shader
    var vertexShaderSourceCode_PV =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 a_position;" +
        "in vec3 a_normal;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec3 u_la[3];" +
        "uniform vec3 u_ld[3];" +
        "uniform vec3 u_ls[3];" +
        "uniform vec4 u_lightPosition[3];" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_materialShininess;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 phong_ads_light;" +
        "void main(void)" +
        "{" +
            "if(u_lightingEnabled == 1)" +
            "{" +
                "vec3 ambient[3];" +
                "vec3 lightDirection[3];" +
                "vec3 diffuse[3];" +
                "vec3 reflectionVector[3];" +
                "vec3 specular[3];" +
                "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
                "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
                "vec3 transformedNormals = normalize(normalMatrix * a_normal);" +
                "vec3 viewerVector = normalize(-eyeCoordinates.xyz);" +
                "for(int i = 0; i < 3; i++)" +
                "{" +
                "ambient[i] = u_la[i] * u_ka;" +
                "lightDirection[i] = normalize(vec3(u_lightPosition[i]) - eyeCoordinates.xyz);" +
                "diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i], transformedNormals), 0.0);" +
                "reflectionVector[i] = reflect(-lightDirection[i], transformedNormals);" +
                "specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], viewerVector), 0.0), u_materialShininess);" +
                "phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
                "}" +
            "}" +
            "else" +
            "{" +
                "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
            "}" +
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
        "}";

    var vertexShaderObject_PV = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject_PV, vertexShaderSourceCode_PV);
    gl.compileShader(vertexShaderObject_PV);

    if (gl.getShaderParameter(vertexShaderObject_PV, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject_PV);

        if (error.length > 0)
        {
            alert("Vertex Shader Per Vertex compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode_PV =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 phong_ads_light;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
            "FragColor = vec4(phong_ads_light,1.0);" +
        "}";

    var fragmentShaderObject_PV = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject_PV, fragmentShaderSourceCode_PV);
    gl.compileShader(fragmentShaderObject_PV);

    if (gl.getShaderParameter(fragmentShaderObject_PV, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject_PV);

        if (error.length > 0) {
            alert("Fragment Shader Per Vertex compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject_PV = gl.createProgram();
    
    gl.attachShader(shaderProgramObject_PV, vertexShaderObject_PV);
    gl.attachShader(shaderProgramObject_PV, fragmentShaderObject_PV);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject_PV, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject_PV, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObject_PV);

    if (gl.getProgramParameter(shaderProgramObject_PV, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject_PV);

        if (error.length > 0)
        {
            alert("Shader Program Per Vertex link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    modelMatrixUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_modelMatrix");
    viewMatrixUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_viewMatrix");
    projectionMatrixUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_projectionMatrix");

    laUniform_PV[0] = gl.getUniformLocation(shaderProgramObject_PV, "u_la[0]");
    ldUniform_PV[0] = gl.getUniformLocation(shaderProgramObject_PV, "u_ld[0]");
    lsUniform_PV[0] = gl.getUniformLocation(shaderProgramObject_PV, "u_ls[0]");
    lightPositionUniform_PV[0] = gl.getUniformLocation(shaderProgramObject_PV, "u_lightPosition[0]");

    laUniform_PV[1] = gl.getUniformLocation(shaderProgramObject_PV, "u_la[1]");
    ldUniform_PV[1] = gl.getUniformLocation(shaderProgramObject_PV, "u_ld[1]");
    lsUniform_PV[1] = gl.getUniformLocation(shaderProgramObject_PV, "u_ls[1]");
    lightPositionUniform_PV[1] = gl.getUniformLocation(shaderProgramObject_PV, "u_lightPosition[1]");

    laUniform_PV[2] = gl.getUniformLocation(shaderProgramObject_PV, "u_la[2]");
    ldUniform_PV[2] = gl.getUniformLocation(shaderProgramObject_PV, "u_ld[2]");
    lsUniform_PV[2] = gl.getUniformLocation(shaderProgramObject_PV, "u_ls[2]");
    lightPositionUniform_PV[2] = gl.getUniformLocation(shaderProgramObject_PV, "u_lightPosition[2]");

    kaUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ka");
    kdUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_kd");
    ksUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ks");

    materialShininessUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_materialShininess");

    lightingEnabledUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_lightingEnabled");

    // Per Fragment
    // Vertex Shader
    var vertexShaderSourceCode_PF =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 a_position;" +
        "in vec3 a_normal;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec4 u_lightPosition[3];" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 transformedNormals;" +
        "out vec3 lightDirection[3];" +
        "out vec3 viewerVector;" +
        "void main(void)" +
        "{" +
            "if(u_lightingEnabled == 1)" +
            "{" +
                "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
                "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
                "transformedNormals = normalMatrix * a_normal;" +
                "viewerVector = -eyeCoordinates.xyz;" +
                "for(int i = 0; i < 3; i++)" +
                "{" +
                    "lightDirection[i] = vec3(u_lightPosition[i]) - eyeCoordinates.xyz;" +
                "}" +
            "}" +
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
        "}";

    var vertexShaderObject_PF = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject_PF, vertexShaderSourceCode_PF);
    gl.compileShader(vertexShaderObject_PF);

    if (gl.getShaderParameter(vertexShaderObject_PF, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject_PF);

        if (error.length > 0) {
            alert("Vertex Shader Per Fragment compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode_PF =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 transformedNormals;" +
        "in vec3 lightDirection[3];" +
        "in vec3 viewerVector;" +
        "uniform vec3 u_la[3];" +
        "uniform vec3 u_ld[3];" +
        "uniform vec3 u_ls[3];" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_materialShininess;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
            "vec3 phong_ads_light;" +
            "if(u_lightingEnabled == 1)" +
            "{" +
                "vec3 ambient[3];" +
                "vec3 diffuse[3];" +
                "vec3 reflectionVector[3];" +
                "vec3 specular[3];" +
                "vec3 normalized_transformed_normal;" +
                "vec3 normalized_lightDirection[3];" +
                "vec3 normalized_viewerVector;" +
                "normalized_transformed_normal = normalize(transformedNormals); " +
                "normalized_viewerVector = normalize(viewerVector);" +
                "for(int i = 0; i < 3; i++)" +
                "{" +
                    "normalized_lightDirection[i] = normalize(lightDirection[i]);" +
                    "ambient[i] = u_la[i] * u_ka;" +
                    "diffuse[i] = u_ld[i] * u_kd * max(dot(normalized_lightDirection[i], normalized_transformed_normal), 0.0);" +
                    "reflectionVector[i] = reflect(-normalized_lightDirection[i], normalized_transformed_normal);" +
                    "specular[i] = u_ls[i] * u_ks * pow(max(dot(reflectionVector[i], normalized_viewerVector), 0.0), u_materialShininess);" +
                    "phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
                "}" +
            "}" +
            "else" +
            "{" +
                "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
            "}" +
            "FragColor = vec4(phong_ads_light,1.0);" +
        "}";

    var fragmentShaderObject_PF = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject_PF, fragmentShaderSourceCode_PF);
    gl.compileShader(fragmentShaderObject_PF);

    if (gl.getShaderParameter(fragmentShaderObject_PF, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject_PF);

        if (error.length > 0) {
            alert("Fragment Shader Per Fragment compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject_PF = gl.createProgram();

    gl.attachShader(shaderProgramObject_PF, vertexShaderObject_PF);
    gl.attachShader(shaderProgramObject_PF, fragmentShaderObject_PF);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject_PF, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject_PF, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObject_PF);

    if (gl.getProgramParameter(shaderProgramObject_PF, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgramObject_PF);

        if (error.length > 0) {
            alert("Shader Program Per Fragment link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    modelMatrixUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_modelMatrix");
    viewMatrixUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_viewMatrix");
    projectionMatrixUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_projectionMatrix");

    laUniform_PF[0] = gl.getUniformLocation(shaderProgramObject_PF, "u_la[0]");
    ldUniform_PF[0] = gl.getUniformLocation(shaderProgramObject_PF, "u_ld[0]");
    lsUniform_PF[0] = gl.getUniformLocation(shaderProgramObject_PF, "u_ls[0]");
    lightPositionUniform_PF[0] = gl.getUniformLocation(shaderProgramObject_PF, "u_lightPosition[0]");

    laUniform_PF[1] = gl.getUniformLocation(shaderProgramObject_PF, "u_la[1]");
    ldUniform_PF[1] = gl.getUniformLocation(shaderProgramObject_PF, "u_ld[1]");
    lsUniform_PF[1] = gl.getUniformLocation(shaderProgramObject_PF, "u_ls[1]");
    lightPositionUniform_PF[1] = gl.getUniformLocation(shaderProgramObject_PF, "u_lightPosition[1]");

    laUniform_PF[2] = gl.getUniformLocation(shaderProgramObject_PF, "u_la[2]");
    ldUniform_PF[2] = gl.getUniformLocation(shaderProgramObject_PF, "u_ld[2]");
    lsUniform_PF[2] = gl.getUniformLocation(shaderProgramObject_PF, "u_ls[2]");
    lightPositionUniform_PF[2] = gl.getUniformLocation(shaderProgramObject_PF, "u_lightPosition[2]");

    kaUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ka");
    kdUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_kd");
    ksUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ks");

    materialShininessUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_materialShininess");

    lightingEnabledUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_lightingEnabled");

    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // Clear the screen by blue color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if (canvas.height <= 0)
        canvas.height = 1;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function display()
{
    // code
    // lights directions
    // light 1 - x direction
    lightPosition = []
    lightPosition1[0] = 0.0;
    lightPosition1[1] = 4 * Math.sin(lightAngle);
    lightPosition1[2] = 4 * Math.cos(lightAngle);
    lightPosition1[3] = 1.0;

    // light 2 - y direction
    lightPosition2[0] = 4 * Math.sin(lightAngle);
    lightPosition2[1] = 0.0;
    lightPosition2[2] = 4 * Math.cos(lightAngle);
    lightPosition2[3] = 1.0;

    // light 2 - z direction
    lightPosition3[0] = 4 * Math.sin(lightAngle);
    lightPosition3[1] = 4 * Math.cos(lightAngle);
    lightPosition3[2] = 0.0;
    lightPosition3[3] = 1.0;

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (counter == 1)
    {
        gl.useProgram(shaderProgramObject_PV);
    }
    else if (counter == 2)
    {
        gl.useProgram(shaderProgramObject_PF);
    }

    // transformations
    var translationMatrix = mat4.create();
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -5.0]);

    modelMatrix = translationMatrix;

    if (counter == 1)
    {
        if (bLight == true)
        {
            gl.uniform1i(lightingEnabledUniform_PV, 1);
        }
        else
        {
            gl.uniform1i(lightingEnabledUniform_PV, 0);
        }

        gl.uniformMatrix4fv(modelMatrixUniform_PV, false, modelMatrix);
        gl.uniformMatrix4fv(viewMatrixUniform_PV, false, viewMatrix);
        gl.uniformMatrix4fv(projectionMatrixUniform_PV, false, perspectiveProjectionMatrix);

        gl.uniform3f(laUniform_PV[0], lightAmbient1[0], lightAmbient1[1], lightAmbient1[2]);
        gl.uniform3f(ldUniform_PV[0], lightDiffuse1[0], lightDiffuse1[1], lightDiffuse1[2]);
        gl.uniform3f(lsUniform_PV[0], lightSpecular1[0], lightSpecular1[1], lightSpecular1[2]);
        gl.uniform4f(lightPositionUniform_PV[0], lightPosition1[0], lightPosition1[1], lightPosition1[2], lightPosition1[3]);

        gl.uniform3f(laUniform_PV[1], lightAmbient2[0], lightAmbient2[1], lightAmbient2[2]);
        gl.uniform3f(ldUniform_PV[1], lightDiffuse2[0], lightDiffuse2[1], lightDiffuse2[2]);
        gl.uniform3f(lsUniform_PV[1], lightSpecular2[0], lightSpecular2[1], lightSpecular2[2]);
        gl.uniform4f(lightPositionUniform_PV[1], lightPosition2[0], lightPosition2[1], lightPosition2[2], lightPosition2[3]);

        gl.uniform3f(laUniform_PV[2], lightAmbient3[0], lightAmbient3[1], lightAmbient3[2]);
        gl.uniform3f(ldUniform_PV[2], lightDiffuse3[0], lightDiffuse3[1], lightDiffuse3[2]);
        gl.uniform3f(lsUniform_PV[2], lightSpecular3[0], lightSpecular3[1], lightSpecular3[2]);
        gl.uniform4f(lightPositionUniform_PV[2], lightPosition3[0], lightPosition3[1], lightPosition3[2], lightPosition3[3]);

        gl.uniform3f(kaUniform_PV, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
        gl.uniform3f(kdUniform_PV, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
        gl.uniform3f(ksUniform_PV, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

        gl.uniform1f(materialShininessUniform_PV, materialShininess);
    }
    else if (counter == 2)
    {
        if (bLight == true)
        {
            gl.uniform1i(lightingEnabledUniform_PF, 1);
        }
        else
        {
            gl.uniform1i(lightingEnabledUniform_PF, 0);
        }

        gl.uniformMatrix4fv(modelMatrixUniform_PF, false, modelMatrix);
        gl.uniformMatrix4fv(viewMatrixUniform_PF, false, viewMatrix);
        gl.uniformMatrix4fv(projectionMatrixUniform_PF, false, perspectiveProjectionMatrix);

        gl.uniform3f(laUniform_PF[0], lightAmbient1[0], lightAmbient1[1], lightAmbient1[2]);
        gl.uniform3f(ldUniform_PF[0], lightDiffuse1[0], lightDiffuse1[1], lightDiffuse1[2]);
        gl.uniform3f(lsUniform_PF[0], lightSpecular1[0], lightSpecular1[1], lightSpecular1[2]);
        gl.uniform4f(lightPositionUniform_PF[0], lightPosition1[0], lightPosition1[1], lightPosition1[2], lightPosition1[3]);

        gl.uniform3f(laUniform_PF[1], lightAmbient2[0], lightAmbient2[1], lightAmbient2[2]);
        gl.uniform3f(ldUniform_PF[1], lightDiffuse2[0], lightDiffuse2[1], lightDiffuse2[2]);
        gl.uniform3f(lsUniform_PF[1], lightSpecular2[0], lightSpecular2[1], lightSpecular2[2]);
        gl.uniform4f(lightPositionUniform_PF[1], lightPosition2[0], lightPosition2[1], lightPosition2[2], lightPosition2[3]);

        gl.uniform3f(laUniform_PF[2], lightAmbient3[0], lightAmbient3[1], lightAmbient3[2]);
        gl.uniform3f(ldUniform_PF[2], lightDiffuse3[0], lightDiffuse3[1], lightDiffuse3[2]);
        gl.uniform3f(lsUniform_PF[2], lightSpecular3[0], lightSpecular3[1], lightSpecular3[2]);
        gl.uniform4f(lightPositionUniform_PF[2], lightPosition3[0], lightPosition3[1], lightPosition3[2], lightPosition3[3]);

        gl.uniform3f(kaUniform_PF, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
        gl.uniform3f(kdUniform_PF, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
        gl.uniform3f(ksUniform_PF, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

        gl.uniform1f(materialShininessUniform_PF, materialShininess);
    }

    sphere.draw();

    gl.useProgram(null);

    update();

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code
    lightAngle = lightAngle + 0.005;

    if (lightAngle >= 360.0)
    {
        lightAngle = lightAngle - 360.0;
    }    
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

// keyboard event listner
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 27:
            toggleFullScreen();
            break;

        case 70:
            counter = 2;
            break;

        case 86:
            counter = 1;
            break;

        case 76:
            counter = 1;
            if (bLight == false)
            {
                bLight = true;
            }
            else {
                bLight = false;
            }
            break;

        case 81: 
            uninitialize();
            window.close(); 
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo

    if (sphere)
    {
        sphere.deallocate();
        sphere = null;
    }

    // shader uninitialization
    if (shaderProgramObject_PF)
    {
        gl.useProgram(shaderProgramObject_PF);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject_PF);

        for (let i = 0; i < shaderObjects.length; i++) {
            gl.detachShader(shaderProgramObject_PF, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject_PF);
        shaderProgramObject_PF = null;
    }

    if (shaderProgramObject_PV)
    {
        gl.useProgram(shaderProgramObject_PV);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject_PV);

        for (let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject_PV, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject_PV);
        shaderProgramObject_PV = null;
    }
}
