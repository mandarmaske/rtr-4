// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject_PV; 
var shaderProgramObject_PF; 

var sphere = null;

// Per Vertex
var modelMatrixUniform_PV;
var viewMatrixUniform_PV;
var projectionMatrixUniform_PV;

var laUniform_PV;
var ldUniform_PV;
var lsUniform_PV;

var kaUniform_PV;
var kdUniform_PV;
var ksUniform_PV;

var lightPositionUniform_PV;
var materialShininessUniform_PV;
var lightingEnabledUniform_PV;

// Per Fragment
var modelMatrixUniform_PF;
var viewMatrixUniform_PF;
var projectionMatrixUniform_PF;

var laUniform_PF;
var ldUniform_PF;
var lsUniform_PF;

var kaUniform_PF;
var kdUniform_PF;
var ksUniform_PF;

var lightPositionUniform_PF;
var materialShininessUniform_PF;
var lightingEnabledUniform_PF;

var bLight = false;

var lightAmbient = [0.1, 0.1, 0.1, 0.1];
var lightDiffuse = [1.0, 1.0, 1.0, 1.0];
var lightSpecular = [1.0, 1.0, 1.0, 1.0];

var materialAmbient = [0.0, 0.0, 0.0, 1.0];
var materialDiffuse = [0.5, 0.2, 0.7, 1.0];
var materialSpecular = [0.7, 0.7, 0.7, 1.0];

var materialShininess = 128.0;

var lightPosition = [100.0, 100.0, 100.0, 1.0];

var perspectiveProjectionMatrix;

var counter = 1;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Per Vertex
    // Vertex Shader
    var vertexShaderSourceCode_PV =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
		"in vec4 a_position;" +
		"in vec3 a_normal;" +
		"uniform mat4 u_modelMatrix;" +
		"uniform mat4 u_viewMatrix;" +
		"uniform mat4 u_projectionMatrix;" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec4 u_lightPosition;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShininess;" +
		"uniform mediump int u_lightingEnabled;" +
		"out vec3 phong_ads_light;" +
		"void main(void)" +
		"{" +
			"if(u_lightingEnabled == 1)" +
			"{" +
				"vec3 ambient = u_la * u_ka;" +
				"vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
				"mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
				"vec3 transformedNormals = normalize(normalMatrix * a_normal);" +
				"vec3 lightDirection = normalize(vec3(u_lightPosition) - eyeCoordinates.xyz);" +
				"vec3 diffuse = u_ld * u_kd * max(dot(lightDirection, transformedNormals), 0.0);" +
				"vec3 reflectionVector = reflect(-lightDirection, transformedNormals);" +
				"vec3 viewerVector = normalize(-eyeCoordinates.xyz);" +
				"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, viewerVector), 0.0), u_materialShininess);" +
				"phong_ads_light = ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
				"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
		"}";

    var vertexShaderObject_PV = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject_PV, vertexShaderSourceCode_PV);
    gl.compileShader(vertexShaderObject_PV);

    if (gl.getShaderParameter(vertexShaderObject_PV, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject_PV);

        if (error.length > 0)
        {
            alert("Vertex Shader Per Vertex compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode_PV =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
		"in vec3 phong_ads_light;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"FragColor = vec4(phong_ads_light,1.0);" +
		"}";

    var fragmentShaderObject_PV = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject_PV, fragmentShaderSourceCode_PV);
    gl.compileShader(fragmentShaderObject_PV);

    if (gl.getShaderParameter(fragmentShaderObject_PV, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject_PV);

        if (error.length > 0) {
            alert("Fragment Shader Per Vertex compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject_PV = gl.createProgram();
    
    gl.attachShader(shaderProgramObject_PV, vertexShaderObject_PV);
    gl.attachShader(shaderProgramObject_PV, fragmentShaderObject_PV);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject_PV, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject_PV, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObject_PV);

    if (gl.getProgramParameter(shaderProgramObject_PV, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject_PV);

        if (error.length > 0)
        {
            alert("Shader Program Per Vertex link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    modelMatrixUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_modelMatrix");
    viewMatrixUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_viewMatrix");
    projectionMatrixUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_projectionMatrix");

    laUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_la");
    ldUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ld");
    lsUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ls");

    kaUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ka");
    kdUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_kd");
    ksUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ks");

    materialShininessUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_materialShininess");

    lightPositionUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_lightPosition");
    lightingEnabledUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_lightingEnabled");

    // Per Fragment
    // Vertex Shader
    var vertexShaderSourceCode_PF =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec3 a_normal;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec4 u_lightPosition;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 transformedNormals;" +
        "out vec3 lightDirection;" +
        "out vec3 viewerVector;" +
        "void main(void)" +
        "{" +
            "if(u_lightingEnabled==1)" +
            "{" +
                "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
                "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
                "transformedNormals = normalMatrix * a_normal;" +
                "lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" +
                "viewerVector = -eyeCoordinates.xyz;" +
            "}" +
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
        "}";

    var vertexShaderObject_PF = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject_PF, vertexShaderSourceCode_PF);
    gl.compileShader(vertexShaderObject_PF);

    if (gl.getShaderParameter(vertexShaderObject_PF, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject_PF);

        if (error.length > 0) {
            alert("Vertex Shader Per Fragment compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode_PF =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 transformedNormals;" +
        "in vec3 lightDirection;" +
        "in vec3 viewerVector;" +
        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_materialShininess;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
            "vec3 phong_ads_light;" +
            "if(u_lightingEnabled==1)" +
            "{" +
                "vec3 ambient = u_la * u_ka;" +
                "vec3 normalized_transformedNormals = normalize(transformedNormals);" +
                "vec3 normalized_lightDirection = normalize(lightDirection);" +
                "vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" +
                "vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" +
                "vec3 normalized_viewerVector = normalize(viewerVector);" +
                "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" +
                "phong_ads_light = ambient + diffuse + specular;" +
            "}" +
            "else" +
            "{" +
                "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
            "}" +
            "FragColor = vec4(phong_ads_light,1.0);" +
        "}";

    var fragmentShaderObject_PF = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject_PF, fragmentShaderSourceCode_PF);
    gl.compileShader(fragmentShaderObject_PF);

    if (gl.getShaderParameter(fragmentShaderObject_PF, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject_PF);

        if (error.length > 0) {
            alert("Fragment Shader Per Fragment compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject_PF = gl.createProgram();

    gl.attachShader(shaderProgramObject_PF, vertexShaderObject_PF);
    gl.attachShader(shaderProgramObject_PF, fragmentShaderObject_PF);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject_PF, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject_PF, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObject_PF);

    if (gl.getProgramParameter(shaderProgramObject_PF, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgramObject_PF);

        if (error.length > 0) {
            alert("Shader Program Per Fragment link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    modelMatrixUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_modelMatrix");
    viewMatrixUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_viewMatrix");
    projectionMatrixUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_projectionMatrix");

    laUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_la");
    ldUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ld");
    lsUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ls");

    kaUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ka");
    kdUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_kd");
    ksUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ks");

    materialShininessUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_materialShininess");

    lightPositionUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_lightPosition");
    lightingEnabledUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_lightingEnabled");

    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // Clear the screen by blue color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if (canvas.height <= 0)
        canvas.height = 1;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (counter == 1)
    {
        gl.useProgram(shaderProgramObject_PV);
    }
    else if (counter == 2)
    {
        gl.useProgram(shaderProgramObject_PF);
    }

    // transformations
    var translationMatrix = mat4.create();
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -6.0]);

    modelMatrix = translationMatrix;

    if (counter == 1)
    {
        if (bLight == true)
        {
            gl.uniform1i(lightingEnabledUniform_PV, 1);
        }
        else
        {
            gl.uniform1i(lightingEnabledUniform_PV, 0);
        }

        gl.uniformMatrix4fv(modelMatrixUniform_PV, false, modelMatrix);
        gl.uniformMatrix4fv(viewMatrixUniform_PV, false, viewMatrix);
        gl.uniformMatrix4fv(projectionMatrixUniform_PV, false, perspectiveProjectionMatrix);

        gl.uniform3f(laUniform_PV, lightAmbient[0], lightAmbient[1], lightAmbient[2]);
        gl.uniform3f(ldUniform_PV, lightDiffuse[0], lightDiffuse[1], lightDiffuse[2]);
        gl.uniform3f(lsUniform_PV, lightSpecular[0], lightSpecular[1], lightSpecular[2]);
        gl.uniform4f(lightPositionUniform_PV, lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

        gl.uniform3f(kaUniform_PV, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
        gl.uniform3f(kdUniform_PV, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
        gl.uniform3f(ksUniform_PV, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

        gl.uniform1f(materialShininessUniform_PV, materialShininess);
    }
    else if (counter == 2)
    {
        if (bLight == true)
        {
            gl.uniform1i(lightingEnabledUniform_PF, 1);
        }
        else
        {
            gl.uniform1i(lightingEnabledUniform_PF, 0);
        }

        gl.uniformMatrix4fv(modelMatrixUniform_PF, false, modelMatrix);
        gl.uniformMatrix4fv(viewMatrixUniform_PF, false, viewMatrix);
        gl.uniformMatrix4fv(projectionMatrixUniform_PF, false, perspectiveProjectionMatrix);

        gl.uniform3f(laUniform_PF, lightAmbient[0], lightAmbient[1], lightAmbient[2]);
        gl.uniform3f(ldUniform_PF, lightDiffuse[0], lightDiffuse[1], lightDiffuse[2]);
        gl.uniform3f(lsUniform_PF, lightSpecular[0], lightSpecular[1], lightSpecular[2]);
        gl.uniform4f(lightPositionUniform_PF, lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

        gl.uniform3f(kaUniform_PF, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
        gl.uniform3f(kdUniform_PF, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
        gl.uniform3f(ksUniform_PF, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

        gl.uniform1f(materialShininessUniform_PF, materialShininess);
    }

    sphere.draw();

    gl.useProgram(null);

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code

}

// keyboard event listner
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 27:
            toggleFullScreen();
            break;

        case 70:
            counter = 2;
            break;

        case 86:
            counter = 1;
            break;

        case 76:
            counter = 1;
            if (bLight == false)
            {
                bLight = true;
            }
            else {
                bLight = false;
            }
            break;

        case 81: 
            uninitialize();
            window.close(); 
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo

    if (sphere)
    {
        sphere.deallocate();
        sphere = null;
    }

    // shader uninitialization
    if (shaderProgramObject_PF)
    {
        gl.useProgram(shaderProgramObject_PF);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject_PF);

        for (let i = 0; i < shaderObjects.length; i++) {
            gl.detachShader(shaderProgramObject_PF, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject_PF);
        shaderProgramObject_PF = null;
    }

    if (shaderProgramObject_PV)
    {
        gl.useProgram(shaderProgramObject_PV);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject_PV);

        for (let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject_PV, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject_PV);
        shaderProgramObject_PV = null;
    }
}
