// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject; // var shaderProgramObject = null;

var sphere = null;

var perspectiveProjectionMatrix;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var laUniform; // light ambient
var ldUniform; // light diffuse
var lsUniform; // light specular
var lightPositionUniform;

var kaUniform;
var kdUniform;
var ksUniform;
var materialShininessUniform;

var lightingEnabledUniform;

var bLight = false;
var xRoatation = false;
var yRoatation = false;
var zRoatation = false;

var lightAmbient = [0.1, 0.1, 0.1, 1.0];
var lightDiffuse = [1.0, 1.0, 1.0, 1.0];
var lightSpecular = [1.0, 1.0, 1.0, 1.0];
var lightPosition = [100.0, 100.0, 100.0, 1.0];

var numberOfRows = 6;
var numberOfColumns = 4;

var materials = new Array(24);
var angleOfLight = 0.0;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();
    drawTwentyFourSpheres();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Vertex Shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec3 a_normal;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec4 u_lightPosition;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 transformedNormals;" +
        "out vec3 lightDirection;" +
        "out vec3 viewerVector;" +
        "void main(void)" +
        "{" +
            "if(u_lightingEnabled==1)" +
            "{" +
                "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
                "mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);" +
                "transformedNormals = normalMatrix * a_normal;" +
                "lightDirection = vec3(u_lightPosition) - eyeCoordinates.xyz;" +
                "viewerVector = -eyeCoordinates.xyz;" +
            "}" +
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
        "}";

    var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0)
        {
            alert("Vertex Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 transformedNormals;" +
        "in vec3 lightDirection;" +
        "in vec3 viewerVector;" +
        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_materialShininess;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
            "vec3 phong_ads_light;" +
            "if(u_lightingEnabled==1)" +
            "{" +
                "vec3 ambient = u_la * u_ka;" +
                "vec3 normalized_transformedNormals = normalize(transformedNormals);" +
                "vec3 normalized_lightDirection = normalize(lightDirection);" +
                "vec3 diffuse = u_ld * u_kd * max(dot(normalized_lightDirection, normalized_transformedNormals), 0.0);" +
                "vec3 reflectionVector = reflect(-normalized_lightDirection, normalized_transformedNormals);" +
                "vec3 normalized_viewerVector = normalize(viewerVector);" +
                "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewerVector), 0.0), u_materialShininess);" +
                "phong_ads_light = ambient + diffuse + specular;" +
            "}" +
            "else" +
            "{" +
                "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
            "}" +
            "FragColor = vec4(phong_ads_light,1.0);" +
        "}";

    var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert("Fragment Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject = gl.createProgram();
    
    gl.attachShader(shaderProgramObject, vertexShaderObject); 
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0)
        {
            alert("Shader Program link log : " + error);
            uninitialize();
        }
    }

    // post linking uniforms location
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix"); // uniform che location aahe (he non-zero aste)
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projectionMatrix");

    // light uniforms
    laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
    ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_lightPosition");

    // material uniforms
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_materialShininess");

    // light enabling uniform
    lightingEnabledUniform = gl.getUniformLocation(shaderProgramObject, "u_lightingEnabled");

    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // Clear the screen by blue color
    gl.clearColor(0.25, 0.25, 0.25, 1.0); // Blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    //gl.enable(gl.CULL_FACE_MODE);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if (canvas.height <= 0)
        canvas.height = 1;

    viewportWidth = canvas.width / numberOfRows;
    viewportHeight = canvas.height / numberOfRows;
    viewportInitialX = (canvas.width - (viewportWidth * numberOfColumns)) / 2;
    viewportInitialY = (canvas.height - (viewportHeight * numberOfRows)) / 2;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Use the shader program object
    gl.useProgram(shaderProgramObject);

    for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 6; j++) {
            gl.viewport(viewportInitialX + (i * viewportWidth), viewportInitialY + (j * viewportHeight), viewportWidth, viewportHeight);

            // use the shader program object
            gl.useProgram(shaderProgramObject);

            // transformations
            var tempMatrix = mat4.create();
            var modelMatrix = mat4.create();
            var viewMatrix = mat4.create();
            var modelViewProjectionMatrix = mat4.create();
            var translationMatrix = mat4.create();

            mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -10.0]);

            mat4.multiply(modelMatrix, translationMatrix, tempMatrix);
            mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelMatrix);

            gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
            gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
            gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

            //
            if (bLight == true) {

                gl.uniform3f(laUniform, 0.0, 0.0, 0.0);
                gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);
                gl.uniform3f(lsUniform, 1.0, 1.0, 1.0);

                if (xRoatation) {
                    lightPosition[0] = 0.0;
                    lightPosition[1] = (Math.sin(angleOfLight) * 10.0);
                    lightPosition[2] = (Math.cos(angleOfLight) * 10.0);
                }

                else if (yRoatation) {
                    lightPosition[0] = (Math.sin(angleOfLight) * 10.0);
                    lightPosition[1] = 0.0;
                    lightPosition[2] = (Math.cos(angleOfLight) * 10.0);
                }

                else if (zRoatation) {
                    lightPosition[0] = (Math.sin(angleOfLight) * 10.0);
                    lightPosition[1] = (Math.cos(angleOfLight) * 10.0);
                    lightPosition[2] = 0.0;
                }

                gl.uniform3fv(kaUniform, materials[(4 * i) + j].MaterialAmbient);
                gl.uniform3fv(kdUniform, materials[(4 * i) + j].MaterialDiffuse);
                gl.uniform3fv(ksUniform, materials[(4 * i) + j].MaterialSpecular);
                gl.uniform1f(materialShininessUniform, materials[(4 * i) + j].MaterialShininess);

                gl.uniform4fv(lightPositionUniform, lightPosition);
                gl.uniform1i(lightingEnabledUniform, 1);
            }
            else {
                gl.uniform1i(lightingEnabledUniform, 0);
            }

            sphere.draw();

        }
    }

    gl.useProgram(null);

    update();

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code
    angleOfLight = angleOfLight + degToRad(0.3);

    if (angleOfLight > 360.0)
    {
        angleOfLight = 0.0;
    }
}

function drawTwentyFourSpheres() {
    for (var i = 0; i < 6; i++) {
        for (var j = 0; j < 4; j++) {
            materials[(4 * i) + j] =
            {
                MaterialAmbient: new Float32Array(3),
                MaterialDiffuse: new Float32Array(3),
                MaterialSpecular: new Float32Array(3),
                MaterialShininess: 0.0
            };
        }
    }

    // ambient material
    materials[0].MaterialAmbient[0] = 0.0215; // r
    materials[0].MaterialAmbient[1] = 0.1745; // g
    materials[0].MaterialAmbient[2] = 0.0215; // b

    // diffuse material
    materials[0].MaterialDiffuse[0] = 0.07568; // r
    materials[0].MaterialDiffuse[1] = 0.61424; // g
    materials[0].MaterialDiffuse[2] = 0.07568; // b

    // specular material
    materials[0].MaterialSpecular[0] = 0.633;    // r
    materials[0].MaterialSpecular[1] = 0.727811; // g
    materials[0].MaterialSpecular[2] = 0.633;    // b

    // shininess
    materials[0].MaterialShininess = 0.6 * 128.0;

    // ***** 2nd sphere on 1st column, jade *****�
    // ambient material
    materials[1].MaterialAmbient[0] = 0.135;  // r
    materials[1].MaterialAmbient[1] = 0.2225; // g
    materials[1].MaterialAmbient[2] = 0.1575; // b

    // diffuse material
    materials[1].MaterialDiffuse[0] = 0.54; // r
    materials[1].MaterialDiffuse[1] = 0.89; // g
    materials[1].MaterialDiffuse[2] = 0.63; // b

    // specular material
    materials[1].MaterialSpecular[0] = 0.316228; // r
    materials[1].MaterialSpecular[1] = 0.316228; // g
    materials[1].MaterialSpecular[2] = 0.316228; // b

    // shininess
    materials[1].MaterialShininess = 0.1 * 128.0;

    // ***** 3rd sphere on 1st column, obsidian *****
    // ambient material
    materials[2].MaterialAmbient[0] = 0.05375; // r
    materials[2].MaterialAmbient[1] = 0.05;    // g
    materials[2].MaterialAmbient[2] = 0.06625; // b

    // diffuse material
    materials[2].MaterialDiffuse[0] = 0.18275; // r
    materials[2].MaterialDiffuse[1] = 0.17;    // g
    materials[2].MaterialDiffuse[2] = 0.22525; // b

    // specular material
    materials[2].MaterialSpecular[0] = 0.332741; // r
    materials[2].MaterialSpecular[1] = 0.328634; // g
    materials[2].MaterialSpecular[2] = 0.346435; // b

    // shininess
    materials[2].MaterialShininess = 0.3 * 128.0;

    // ***** 4th sphere on 1st column, pearl *****
    // ambient material
    materials[3].MaterialAmbient[0] = 0.25;    // r
    materials[3].MaterialAmbient[1] = 0.20725; // g
    materials[3].MaterialAmbient[2] = 0.20725; // b

    // diffuse material
    materials[3].MaterialDiffuse[0] = 1.0;   // r
    materials[3].MaterialDiffuse[1] = 0.829; // g
    materials[3].MaterialDiffuse[2] = 0.829; // b

    // specular material
    materials[3].MaterialSpecular[0] = 0.296648; // r
    materials[3].MaterialSpecular[1] = 0.296648; // g
    materials[3].MaterialSpecular[2] = 0.296648; // b

    // shininess
    materials[3].MaterialShininess = 0.088 * 128.0;

    // ***** 5th sphere on 1st column, ruby *****
    // ambient material
    materials[4].MaterialAmbient[0] = 0.1745;  // r
    materials[4].MaterialAmbient[1] = 0.01175; // g
    materials[4].MaterialAmbient[2] = 0.01175; // b


    // diffuse material
    materials[4].MaterialDiffuse[0] = 0.61424; // r
    materials[4].MaterialDiffuse[1] = 0.04136; // g
    materials[4].MaterialDiffuse[2] = 0.04136; // b


    // specular material
    materials[4].MaterialSpecular[0] = 0.727811; // r
    materials[4].MaterialSpecular[1] = 0.626959; // g
    materials[4].MaterialSpecular[2] = 0.626959; // b

    // shininess
    materials[4].MaterialShininess = 0.6 * 128.0;

    // ***** 6th sphere on 1st column, turquoise *****
    // ambient material
    materials[5].MaterialAmbient[0] = 0.1;     // r
    materials[5].MaterialAmbient[1] = 0.18725; // g
    materials[5].MaterialAmbient[2] = 0.1745;  // b


    // diffuse material
    materials[5].MaterialDiffuse[0] = 0.396;   // r
    materials[5].MaterialDiffuse[1] = 0.74151; // g
    materials[5].MaterialDiffuse[2] = 0.69102; // b


    // specular material
    materials[5].MaterialSpecular[0] = 0.297254; // r
    materials[5].MaterialSpecular[1] = 0.30829;  // g
    materials[5].MaterialSpecular[2] = 0.306678; // b

    // shininess
    materials[5].MaterialShininess = 0.1 * 128.0;

    // ***** 1st sphere on 2nd column, brass *****
    // ambient material
    materials[6].MaterialAmbient[0] = 0.329412; // r
    materials[6].MaterialAmbient[1] = 0.223529; // g
    materials[6].MaterialAmbient[2] = 0.027451; // b


    // diffuse material
    materials[6].MaterialDiffuse[0] = 0.780392; // r
    materials[6].MaterialDiffuse[1] = 0.568627; // g
    materials[6].MaterialDiffuse[2] = 0.113725; // b


    // specular material
    materials[6].MaterialSpecular[0] = 0.992157; // r
    materials[6].MaterialSpecular[1] = 0.941176; // g
    materials[6].MaterialSpecular[2] = 0.807843; // b

    // shininess
    materials[6].MaterialShininess = 0.21794872 * 128.0;

    // ***** 2nd sphere on 2nd column, bronze *****
    // ambient material
    materials[7].MaterialAmbient[0] = 0.2125; // r
    materials[7].MaterialAmbient[1] = 0.1275; // g
    materials[7].MaterialAmbient[2] = 0.054;  // b


    // diffuse material
    materials[7].MaterialDiffuse[0] = 0.714;   // r
    materials[7].MaterialDiffuse[1] = 0.4284;  // g
    materials[7].MaterialDiffuse[2] = 0.18144; // b


    // specular material
    materials[7].MaterialSpecular[0] = 0.393548; // r
    materials[7].MaterialSpecular[1] = 0.271906; // g
    materials[7].MaterialSpecular[2] = 0.166721; // b

    // shininess
    materials[7].MaterialShininess = 0.2 * 128.0;

    // ***** 3rd sphere on 2nd column, chrome *****
    // ambient material
    materials[8].MaterialAmbient[0] = 0.25; // r
    materials[8].MaterialAmbient[1] = 0.25; // g
    materials[8].MaterialAmbient[2] = 0.25; // b


    // diffuse material
    materials[8].MaterialDiffuse[0] = 0.4;  // r
    materials[8].MaterialDiffuse[1] = 0.4;  // g
    materials[8].MaterialDiffuse[2] = 0.4;  // b


    // specular material
    materials[8].MaterialSpecular[0] = 0.774597; // r
    materials[8].MaterialSpecular[1] = 0.774597; // g
    materials[8].MaterialSpecular[2] = 0.774597; // b

    // shininess
    materials[8].MaterialShininess = 0.6 * 128.0;

    // ***** 4th sphere on 2nd column, copper *****
    // ambient material
    materials[9].MaterialAmbient[0] = 0.19125; // r
    materials[9].MaterialAmbient[1] = 0.0735;  // g
    materials[9].MaterialAmbient[2] = 0.0225;  // b


    // diffuse material
    materials[9].MaterialDiffuse[0] = 0.7038;  // r
    materials[9].MaterialDiffuse[1] = 0.27048; // g
    materials[9].MaterialDiffuse[2] = 0.0828;  // b


    // specular material
    materials[9].MaterialSpecular[0] = 0.256777; // r
    materials[9].MaterialSpecular[1] = 0.137622; // g
    materials[9].MaterialSpecular[2] = 0.086014; // b

    // shininess
    materials[9].MaterialShininess = 0.1 * 128.0;

    // ***** 5th sphere on 2nd column, gold *****
    // ambient material
    materials[10].MaterialAmbient[0] = 0.24725; // r
    materials[10].MaterialAmbient[1] = 0.1995;  // g
    materials[10].MaterialAmbient[2] = 0.0745;  // b


    // diffuse material
    materials[10].MaterialDiffuse[0] = 0.75164; // r
    materials[10].MaterialDiffuse[1] = 0.60648; // g
    materials[10].MaterialDiffuse[2] = 0.22648; // b


    // specular material
    materials[10].MaterialSpecular[0] = 0.628281; // r
    materials[10].MaterialSpecular[1] = 0.555802; // g
    materials[10].MaterialSpecular[2] = 0.366065; // b

    // shininess
    materials[10].MaterialShininess = 0.4 * 128.0;


    // ***** 6th sphere on 2nd column, silver *****
    // ambient material
    materials[11].MaterialAmbient[0] = 0.19225; // r
    materials[11].MaterialAmbient[1] = 0.19225; // g
    materials[11].MaterialAmbient[2] = 0.19225; // b


    // diffuse material
    materials[11].MaterialDiffuse[0] = 0.50754; // r
    materials[11].MaterialDiffuse[1] = 0.50754; // g
    materials[11].MaterialDiffuse[2] = 0.50754; // b


    // specular material
    materials[11].MaterialSpecular[0] = 0.508273; // r
    materials[11].MaterialSpecular[1] = 0.508273; // g
    materials[11].MaterialSpecular[2] = 0.508273; // b

    // shininess
    materials[11].MaterialShininess = 0.4 * 128.0;


    // ***** 1st sphere on 3rd column, black *****
    // ambient material
    materials[12].MaterialAmbient[0] = 0.0;  // r
    materials[12].MaterialAmbient[1] = 0.0;  // g
    materials[12].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[12].MaterialDiffuse[0] = 0.01; // r
    materials[12].MaterialDiffuse[1] = 0.01; // g
    materials[12].MaterialDiffuse[2] = 0.01; // b


    // specular material
    materials[12].MaterialSpecular[0] = 0.50; // r
    materials[12].MaterialSpecular[1] = 0.50; // g
    materials[12].MaterialSpecular[2] = 0.50; // b

    // shininess
    materials[12].MaterialShininess = 0.25 * 128.0;


    // ***** 2nd sphere on 3rd column, cyan *****
    // ambient material
    materials[13].MaterialAmbient[0] = 0.0;  // r
    materials[13].MaterialAmbient[1] = 0.1;  // g
    materials[13].MaterialAmbient[2] = 0.06; // b


    // diffuse material
    materials[13].MaterialDiffuse[0] = 0.0;        // r
    materials[13].MaterialDiffuse[1] = 0.50980392; // g
    materials[13].MaterialDiffuse[2] = 0.50980392; // b


    // specular material
    materials[13].MaterialSpecular[0] = 0.50196078; // r
    materials[13].MaterialSpecular[1] = 0.50196078; // g
    materials[13].MaterialSpecular[2] = 0.50196078; // b

    // shininess
    materials[13].MaterialShininess = 0.25 * 128.0;

    // ***** 3rd sphere on 2nd column, green *****
    // ambient material
    materials[14].MaterialAmbient[0] = 0.0;  // r
    materials[14].MaterialAmbient[1] = 0.0;  // g
    materials[14].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[14].MaterialDiffuse[0] = 0.1;  // r
    materials[14].MaterialDiffuse[1] = 0.35; // g
    materials[14].MaterialDiffuse[2] = 0.1;  // b


    // specular material
    materials[14].MaterialSpecular[0] = 0.45; // r
    materials[14].MaterialSpecular[1] = 0.55; // g
    materials[14].MaterialSpecular[2] = 0.45; // b

    // shininess
    materials[14].MaterialShininess = 0.25 * 128.0;

    // ***** 4th sphere on 3rd column, red *****
    // ambient material
    materials[15].MaterialAmbient[0] = 0.0;  // r
    materials[15].MaterialAmbient[1] = 0.0;  // g
    materials[15].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[15].MaterialDiffuse[0] = 0.5;  // r
    materials[15].MaterialDiffuse[1] = 0.0;  // g
    materials[15].MaterialDiffuse[2] = 0.0;  // b


    // specular material
    materials[15].MaterialSpecular[0] = 0.7;  // r
    materials[15].MaterialSpecular[1] = 0.6;  // g
    materials[15].MaterialSpecular[2] = 0.6;  // b

    // shininess
    materials[15].MaterialShininess = 0.25 * 128.0;

    // ***** 5th sphere on 3rd column, white *****
    // ambient material
    materials[16].MaterialAmbient[0] = 0.0;  // r
    materials[16].MaterialAmbient[1] = 0.0;  // g
    materials[16].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[16].MaterialDiffuse[0] = 0.55; // r
    materials[16].MaterialDiffuse[1] = 0.55; // g
    materials[16].MaterialDiffuse[2] = 0.55; // b


    // specular material
    materials[16].MaterialSpecular[0] = 0.70; // r
    materials[16].MaterialSpecular[1] = 0.70; // g
    materials[16].MaterialSpecular[2] = 0.70; // b

    // shininess
    materials[16].MaterialShininess = 0.25 * 128.0;

    // ***** 6th sphere on 3rd column, yellow plastic *****
    // ambient material
    materials[17].MaterialAmbient[0] = 0.0;  // r
    materials[17].MaterialAmbient[1] = 0.0;  // g
    materials[17].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[17].MaterialDiffuse[0] = 0.5;  // r
    materials[17].MaterialDiffuse[1] = 0.5;  // g
    materials[17].MaterialDiffuse[2] = 0.0;  // b


    // specular material
    materials[17].MaterialSpecular[0] = 0.60; // r
    materials[17].MaterialSpecular[1] = 0.60; // g
    materials[17].MaterialSpecular[2] = 0.50; // b

    // shininess
    materials[17].MaterialShininess = 0.25 * 128.0;

    // ***** 1st sphere on 4th column, black *****
    // ambient material
    materials[18].MaterialAmbient[0] = 0.02; // r
    materials[18].MaterialAmbient[1] = 0.02; // g
    materials[18].MaterialAmbient[2] = 0.02; // b


    // diffuse material
    materials[18].MaterialDiffuse[0] = 0.01; // r
    materials[18].MaterialDiffuse[1] = 0.01; // g
    materials[18].MaterialDiffuse[2] = 0.01; // b


    // specular material
    materials[18].MaterialSpecular[0] = 0.4;  // r
    materials[18].MaterialSpecular[1] = 0.4;  // g
    materials[18].MaterialSpecular[2] = 0.4;  // b

    // shininess
    materials[18].MaterialShininess = 0.078125 * 128.0;

    // ***** 2nd sphere on 4th column, cyan *****
    // ambient material
    materials[19].MaterialAmbient[0] = 0.0;  // r
    materials[19].MaterialAmbient[1] = 0.05; // g
    materials[19].MaterialAmbient[2] = 0.05; // b


    // diffuse material
    materials[19].MaterialDiffuse[0] = 0.4;  // r
    materials[19].MaterialDiffuse[1] = 0.5;  // g
    materials[19].MaterialDiffuse[2] = 0.5;  // b


    // specular material
    materials[19].MaterialSpecular[0] = 0.04; // r
    materials[19].MaterialSpecular[1] = 0.7;  // g
    materials[19].MaterialSpecular[2] = 0.7;  // b

    // shininess
    materials[19].MaterialShininess = 0.078125 * 128.0;


    // ***** 3rd sphere on 4th column, green *****
    // ambient material
    materials[20].MaterialAmbient[0] = 0.0;  // r
    materials[20].MaterialAmbient[1] = 0.05; // g
    materials[20].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[20].MaterialDiffuse[0] = 0.4;  // r
    materials[20].MaterialDiffuse[1] = 0.5;  // g
    materials[20].MaterialDiffuse[2] = 0.4;  // b


    // specular material
    materials[20].MaterialSpecular[0] = 0.04; // r
    materials[20].MaterialSpecular[1] = 0.7;  // g
    materials[20].MaterialSpecular[2] = 0.04; // b

    // shininess
    materials[20].MaterialShininess = 0.078125 * 128.0;

    // ***** 4th sphere on 4th column, red *****
    // ambient material
    materials[21].MaterialAmbient[0] = 0.05; // r
    materials[21].MaterialAmbient[1] = 0.0;  // g
    materials[21].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[21].MaterialDiffuse[0] = 0.5;  // r
    materials[21].MaterialDiffuse[1] = 0.4;  // g
    materials[21].MaterialDiffuse[2] = 0.4;  // b


    // specular material
    materials[21].MaterialSpecular[0] = 0.7;  // r
    materials[21].MaterialSpecular[1] = 0.04; // g
    materials[21].MaterialSpecular[2] = 0.04; // b

    // shininess
    materials[21].MaterialShininess = 0.078125 * 128.0;


    // ***** 5th sphere on 4th column, white *****
    // ambient material
    materials[22].MaterialAmbient[0] = 0.05; // r
    materials[22].MaterialAmbient[1] = 0.05; // g
    materials[22].MaterialAmbient[2] = 0.05; // b


    // diffuse material
    materials[22].MaterialDiffuse[0] = 0.5;  // r
    materials[22].MaterialDiffuse[1] = 0.5;  // g
    materials[22].MaterialDiffuse[2] = 0.5;  // b


    // specular material
    materials[22].MaterialSpecular[0] = 0.7;  // r
    materials[22].MaterialSpecular[1] = 0.7;  // g
    materials[22].MaterialSpecular[2] = 0.7;  // b

    // shininess
    materials[22].MaterialShininess = 0.078125 * 128.0;

    // ***** 6th sphere on 4th column, yellow rubber *****
    // ambient material
    materials[23].MaterialAmbient[0] = 0.05; // r
    materials[23].MaterialAmbient[1] = 0.05; // g
    materials[23].MaterialAmbient[2] = 0.0;  // b


    // diffuse material
    materials[23].MaterialDiffuse[0] = 0.5;  // r
    materials[23].MaterialDiffuse[1] = 0.5;  // g
    materials[23].MaterialDiffuse[2] = 0.4;  // b


    // specular material
    materials[23].MaterialSpecular[0] = 0.7;  // r
    materials[23].MaterialSpecular[1] = 0.7;  // g
    materials[23].MaterialSpecular[2] = 0.04; // b

    // shininess
    materials[23].MaterialShininess = 0.078125 * 128.0;
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

// keyboard event listner
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 70:
            toggleFullScreen();
            break;

        case 76:
            if (bLight == false)
            {
                bLight = true;
            }
            else
            {
                bLight = false;
            }
            break;

        case 88:
            xRoatation = true;
            yRoatation = false;
            zRoatation = false;
            break;

        case 89:
            xRoatation = false;
            yRoatation = true;
            zRoatation = false;
            break;

        case 90:
            xRoatation = false;
            yRoatation = false;
            zRoatation = true;
            break;

        case 81: 
            uninitialize();
            window.close(); 
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo

    if (sphere)
    {
        sphere.deallocate();
        sphere = null;
    }

    // shader uninitialization
    if (shaderProgramObject)
    {
        gl.useProgram(shaderProgramObject);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject);

        for (let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}
