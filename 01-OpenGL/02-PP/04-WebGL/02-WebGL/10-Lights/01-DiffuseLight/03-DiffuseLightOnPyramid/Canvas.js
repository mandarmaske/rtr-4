// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject; // var shaderProgramObject = null;

var vao_pyramid;
var vbo_pyramid_position;
var vbo_pyramid_normal;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var perspectiveProjectionMatrix;

var ldUniform;
var kdUniform;
var lightpositionUniform;

var lightingEnabledUniform;

var bLight = false;

var lightDiffuse = [1.0, 1.0, 1.0, 1.0];
var materialDiffuse = [0.5, 0.5, 0.5, 1.0];
var lightPosition = [0.0, 0.0, 2.0, 1.0];

var anglePyramid = 0.0;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Vertex Shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec3 a_normal;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_kd;" +
        "uniform vec4 u_lightPosition;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 diffuse_light_color;" +
        "void main(void)" +
        "{" +
             "if(u_lightingEnabled == 1)" +
             "{" +
                  "vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;" +
                  "mat3 normalMatrix = mat3(transpose(inverse(u_viewMatrix * u_modelMatrix)));" +
                  "vec3 transformedNormals = normalize(normalMatrix * a_normal);" +
                  "vec3 lightDirection = normalize(vec3(u_lightPosition - eyeCoordinates));" +
                  "diffuse_light_color = u_ld * u_kd * max(dot(lightDirection, transformedNormals), 0.0);" +
             "}" +
             "gl_Position = u_projectionMatrix *u_viewMatrix * u_modelMatrix * a_position;" +
        "}";

    var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0)
        {
            alert("Vertex Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
		"in vec4 a_color_out;" +
		"in vec3 diffuse_light_color;" +
		"uniform int u_lightingEnabled;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"if(u_lightingEnabled == 1)" +
			"{" +
				"FragColor = vec4(diffuse_light_color, 1.0);" +
			"}" +
			"else" +
			"{" +
				"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
			"}" +
		"}";

    var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert("Fragment Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject = gl.createProgram();
    
    gl.attachShader(shaderProgramObject, vertexShaderObject); 
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0)
        {
            alert("Shader Program link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix"); 
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projectionMatrix");

	ldUniform= gl.getUniformLocation(shaderProgramObject, "u_ld");
	kdUniform= gl.getUniformLocation(shaderProgramObject, "u_kd");
	lightpositionUniform = gl.getUniformLocation(shaderProgramObject, "u_lightPosition");
	lightingEnabledUniform = gl.getUniformLocation(shaderProgramObject, "u_lightingEnabled");

    // vao & vbo related code
     var pyramidVertices = new Float32Array
    (
        [
            // front
            0.0, 1.0, 0.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0,

            // right
            0.0, 1.0, 0.0,
            1.0, -1.0, 1.0,
            1.0, -1.0, -1.0,

            // back
            0.0, 1.0, 0.0,
            1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,

            // left
            0.0, 1.0, 0.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
        ]
    );

    var pyramidNormal = new Float32Array
    (
        [
            0.0, 0.447214, 0.894427,// front-top
            0.0, 0.447214, 0.894427,// front-left
            0.0, 0.447214, 0.894427,// front-right

            0.894427, 0.447214, 0.0, // right-top
            0.894427, 0.447214, 0.0, // right-left
            0.894427, 0.447214, 0.0, // right-right

            0.0, 0.447214, -0.89442, // back-top
            0.0, 0.447214, -0.894427, // back-left
            0.0, 0.447214, -0.894427, // back-right

            -0.894427, 0.447214, 0.0, // left-top
            -0.894427, 0.447214, 0.0, // left-left
            -0.894427, 0.447214, 0.0 // left-right
        ]
    );

    // vao pyramid
    vao_pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_pyramid);

    // vbo pyramid
    vbo_pyramid_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_position);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_pyramid_normal = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pyramid_normal);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidNormal, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // Clear the screen by blue color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if (canvas.height <= 0)
        canvas.height = 1;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Use the shader program object
    gl.useProgram(shaderProgramObject);

    // transformations triangle
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var translationMatrix = mat4.create();
    var rotationMatrix_Y = mat4.create();
  
    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -4.0]);

    mat4.rotateY(rotationMatrix_Y, rotationMatrix_Y, degToRad(anglePyramid));

    mat4.multiply(modelMatrix, translationMatrix, rotationMatrix_Y);

    gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

    if (bLight == true)
    {
        gl.uniform1i(lightingEnabledUniform, 1);

        gl.uniform3f(ldUniform, lightDiffuse[0], lightDiffuse[1], lightDiffuse[2]);
        gl.uniform3f(kdUniform, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
        gl.uniform4f(lightpositionUniform, lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);
    }
    else
    {
        gl.uniform1i(lightingEnabledUniform, 0);
    }

    gl.bindVertexArray(vao_pyramid);

    gl.drawArrays(gl.TRIANGLES, 0, 12);

    gl.bindVertexArray(null);

    gl.useProgram(null);

    update();

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code
    anglePyramid = anglePyramid + 1.0;

    if (anglePyramid >= 360.0)
        anglePyramid = anglePyramid - 360.0;

}

function degToRad(degree)
{
    return (degree * Math.PI / 180.0);
}

// keyboard event listner
function keyDown()
{
    // code
    switch (event.keyCode) {
        case 70:
            toggleFullScreen();
            break;

        case 76:
            if (bLight == false) {
                bLight = true;
            }
            else {
                bLight = false;
            }
            break;

        case 81:
            uninitialize();
            window.close();
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo

    if (vbo_pyramid_normal)
    {
        gl.deleteBuffer(vbo_pyramid_normal);
        vbo_pyramid_normal = null;
    }

    if (vbo_pyramid_position)
    {
        gl.deleteBuffer(vbo_pyramid_position);
        vbo_pyramid_position = null;
    }

    // deletion/ uninitialization of vao
    if (vao_pyramid)
    {
        gl.deleteVertexArray(vao_pyramid);
        vao_pyramid = null;
    }

    // shader uninitialization
    if (shaderProgramObject)
    {
        gl.useProgram(shaderProgramObject);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject);

        for (let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}
