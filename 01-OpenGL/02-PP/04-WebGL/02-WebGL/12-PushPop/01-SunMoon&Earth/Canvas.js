// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject; // var shaderProgramObject = null;

var vao_sphere = null;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var laUniform; //light amibiet
var ldUniform; //Diffuse
var lsUniform; //Specular
var lightPositionUniform;

var kaUniform;
var kdUniform;
var ksUniform;
var materialShininessUniform;

var lightingEnabledUniform;

var bLight = false;

var day = 0;
var year = 0;

var lightAmbient = [0.1, 0.1, 0.1, 1.0];
var lightDiffuse = [1.0, 1.0, 1.0, 1.0];
var lightSpecular = [1.0, 1.0, 1.0, 1.0];
var lightPosition = [0.0, 0.0, 100.0, 1.0];

var perspectiveProjectionMatrix;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

class Stack {
    constructor(size = 64) {
        this.arr = new Array(size);
        this.top = -1;
        this.max = size;
    }
    push(matrix) {
        if (this.top === this.max - 1) {
            console.log("STACK is over flow");
        }
        else {
            this.top++;
            this.arr[this.top] = matrix;
        }
    }
    pop() {
        if (this.top <= -1) {
            console.log("Stack is under flow");
        }
        else {
            this.top--;
            return this.arr[this.top + 1];
        }
    }
}
const stack = new Stack();

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Vertex Shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec3 a_normal;" +
        "uniform mat4 u_modelMatrix;" +
        "uniform mat4 u_viewMatrix;" +
        "uniform mat4 u_projectionMatrix;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_kd;" +
        "uniform vec4 u_lightPosition;" +
        "uniform mediump int u_lightingEnabled;" +
        "out vec3 diffuse_light_color;" +
        "void main(void)" +
        "{" +
            "if(u_lightingEnabled==1)" +
            "{" +
                "vec4 eyeCoordinates= u_viewMatrix*u_modelMatrix*a_position;" +
                "mat3 normalMatrix=mat3(transpose(inverse(u_viewMatrix*u_modelMatrix)));" +
                "vec3 transformedNormals=normalize(normalMatrix*a_normal);" +
                "vec3 lightDirection= normalize(vec3(u_lightPosition- eyeCoordinates));" +
                "diffuse_light_color=u_ld*u_kd*max(dot(lightDirection,transformedNormals),0.0);" +
            "}" +
            "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;" +
        "}";

    var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0)
        {
            alert("Vertex Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
		"in vec3 diffuse_light_color;" +
		"uniform mediump int u_lightingEnabled;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
			"if(u_lightingEnabled==1)" +
			"{" +
				"FragColor=vec4(diffuse_light_color,1.0);" +
			"}" +
			"else" +
			"{" +
				"FragColor = vec4(1.0,1.0,1.0,1.0);" +
			"}" +
		"}";

    var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert("Fragment Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject = gl.createProgram();
    
    gl.attachShader(shaderProgramObject, vertexShaderObject); 
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_NORMAL, "a_normal");

    // Shader program linking
    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0)
        {
            alert("Shader Program link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix"); 
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projectionMatrix");

    ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_lightPosition");
    lightingEnabledUniform = gl.getUniformLocation(shaderProgramObject, "u_lightingEnabled");

    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // Clear the screen by blue color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if (canvas.height <= 0)
        canvas.height = 1;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Use the shader program object
    gl.useProgram(shaderProgramObject);

    // Transformations
    var translationMatrix = mat4.create();
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var previousMatriuxx = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -10.0]);
    modelMatrix = translationMatrix;
    stack.push(modelMatrix);

    gl.uniformMatrix4fv(modelMatrixUniform, gl.FALSE, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, gl.FALSE, perspectiveProjectionMatrix);

    gl.uniform1i(lightingEnabledUniform, 1);

    var materialAmbient = [0.2125, 0.1275, 0.054, 1.0];
    var materialDiffuse = [0.714, 0.4284, 0.18144, 1.0];
    var materialSpecular = [0.393548, 0.271906, 0.166721, 1.0];
    var materialShininess = 0.2 * 128;

    gl.uniform3f(laUniform, lightAmbient[0], lightAmbient[1], lightAmbient[2]);
    gl.uniform3f(ldUniform, lightDiffuse[0], lightDiffuse[1], lightDiffuse[2]);
    gl.uniform3f(lsUniform, lightSpecular[0], lightSpecular[1], lightSpecular[2]);
    gl.uniform4f(lightPositionUniform, lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

    gl.uniform3f(kaUniform, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
    gl.uniform3f(kdUniform, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
    gl.uniform3f(ksUniform, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

    gl.uniform1f(materialShininessUniform, materialShininess);

    sphere.draw();

    // Earth
    previousMatriux = stack.pop();

    // Transformations
    var scaleMatrix = mat4.create();
    var rotationMatrix1 = mat4.create();
    var rotationMatrix2 = mat4.create();
    var translationMatrix1 = mat4.create();
    modelMatrix = mat4.create();
    viewMatrix = mat4.create();

    modelMatrix = translationMatrix;
    mat4.scale(scaleMatrix, scaleMatrix, [0.5, 0.5, 0.5]);
    mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
    mat4.rotateY(rotationMatrix1, rotationMatrix1, degToRad(year));
    mat4.multiply(modelMatrix, modelMatrix, rotationMatrix1);
    mat4.translate(translationMatrix1, translationMatrix1, [8.0, 0.0, 0.0]);
    mat4.multiply(modelMatrix, modelMatrix, translationMatrix1);
    mat4.rotateY(rotationMatrix2, rotationMatrix2, degToRad(day));
    mat4.multiply(modelMatrix, modelMatrix, rotationMatrix2);

    gl.uniformMatrix4fv(modelMatrixUniform, gl.FALSE, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, gl.FALSE, perspectiveProjectionMatrix);

    gl.uniform1i(lightingEnabledUniform, 1);

    materialAmbient = [0.0, 0.1, 0.06, 1.0];
    materialDiffuse = [0.0, 0.50980392, 0.50980392, 1.0];
    materialSpecular = [0.50196078, 0.50196078, 0.50196078, 1.0];
    materialShininess = 0.25 * 128.0;

    gl.uniform3f(laUniform, lightAmbient[0], lightAmbient[1], lightAmbient[2]);
    gl.uniform3f(ldUniform, lightDiffuse[0], lightDiffuse[1], lightDiffuse[2]);
    gl.uniform3f(lsUniform, lightSpecular[0], lightSpecular[1], lightSpecular[2]);
    gl.uniform4f(lightPositionUniform, lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

    gl.uniform3f(kaUniform, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
    gl.uniform3f(kdUniform, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
    gl.uniform3f(ksUniform, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

    gl.uniform1f(materialShininessUniform, materialShininess);

    sphere.draw();

    // Moon
   
    var scaleMatrix1 = mat4.create();
    var rotationMatrix3 = mat4.create();
    var rotationMatrix4 = mat4.create();
    var translationMatrix2 = mat4.create();
    var translationMatrix1 = mat4.create();
    modelMatrix = mat4.create();
    viewMatrix = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, 0.0]);
    
    modelMatrix = translationMatrix;
    mat4.scale(scaleMatrix1, scaleMatrix1, [0.4, 0.4, 0.4]);
    mat4.multiply(modelMatrix, modelMatrix, scaleMatrix1);
    mat4.rotateY(rotationMatrix3, rotationMatrix3, degToRad(year));
    mat4.multiply(modelMatrix, modelMatrix, rotationMatrix3);
    mat4.translate(translationMatrix1, translationMatrix1, [9, 0.0, 0.0]);
    mat4.multiply(modelMatrix, modelMatrix, translationMatrix1);
    mat4.rotateY(rotationMatrix4, rotationMatrix4, degToRad(day));
    mat4.multiply(modelMatrix, modelMatrix, rotationMatrix4);
    mat4.translate(translationMatrix2, translationMatrix2, [0.01, 0.0, 0.0]);
    mat4.multiply(modelMatrix, modelMatrix, translationMatrix2);

    gl.uniformMatrix4fv(modelMatrixUniform, gl.FALSE, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, gl.FALSE, perspectiveProjectionMatrix);

    gl.uniform1i(lightingEnabledUniform, 1);

    materialAmbient = [0.0, 0.0, 0.0, 1.0];
    materialDiffuse = [0.55, 0.55, 0.55, 1.0];
    materialSpecular = [0.70, 0.70, 0.70, 1.0];
    materialShininess = 128.0;

    gl.uniform3f(laUniform, lightAmbient[0], lightAmbient[1], lightAmbient[2]);
    gl.uniform3f(ldUniform, lightDiffuse[0], lightDiffuse[1], lightDiffuse[2]);
    gl.uniform3f(lsUniform, lightSpecular[0], lightSpecular[1], lightSpecular[2]);
    gl.uniform4f(lightPositionUniform, lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

    gl.uniform3f(kaUniform, materialAmbient[0], materialAmbient[1], materialAmbient[2]);
    gl.uniform3f(kdUniform, materialDiffuse[0], materialDiffuse[1], materialDiffuse[2]);
    gl.uniform3f(ksUniform, materialSpecular[0], materialSpecular[1], materialSpecular[2]);

    gl.uniform1f(materialShininessUniform, materialShininess);
    sphere.draw();

    gl.useProgram(null);

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code

}

// keyboard event listner
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 70:
            toggleFullScreen();
            break;

        case 76:
            if (bLight == false)
            {
                bLight = true;
            }
            else {
                bLight = false;
            }
            break;

        case 68:
            day = (day + 6) % 360;
            break;

        case 100:
            day = (day - 6) % 360;
            break;

        case 89:
            year = (year + 3) % 360;
            day = (day + 6) % 360;
            break;

        case 121:
            year = (year - 3) % 360;
            day = (day - 6) % 360;
            break;

        case 81: 
            uninitialize();
            window.close(); 
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo

    if (sphere)
    {
        sphere.deallocate();
        sphere = null;
    }

    // shader uninitialization
    if (shaderProgramObject)
    {
        gl.useProgram(shaderProgramObject);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject);

        for (let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}
