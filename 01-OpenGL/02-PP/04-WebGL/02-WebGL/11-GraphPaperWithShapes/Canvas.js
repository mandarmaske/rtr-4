// JavaScript source code

var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL (OpenGL) related variables
const webGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3,
};

var shaderProgramObject; // var shaderProgramObject = null;

// variables for X Axis Line
var vao_horizontalLine;
var vbo_horizontalLine_position;
var vbo_horizontalLine_color;

// variables for Y Axis Line
var vao_verticalLine;
var vbo_verticalLine_position;
var vbo_verticalLine_color;

// variables for Horizontal Graph Lines
var vao_graphHLines;
var vbo_graphHLines_position;
var vbo_graphHLines_color;

// variables for Vertical Graph Lines
var vao_graphVLines;
var vbo_graphVLines_position; // color is reused of hlines vbo_graphHLines_color

// variables for Triangle Lines
var vao_triangleLines;
var vbo_triangleLines_position;
var vbo_triangleLines_color;

// variables for Square Lines
var vao_squareLines;
var vbo_squareLines_position;
var vbo_squareLines_color;

// variables for Vertical Graph Lines
var vao_circleLines;
var vbo_circleLines_position;
var vbo_circleLines_color;

var base = 1.2;
var height = 0.0;
var base1 = base;

var mvpMatrixUniform;
var perspectiveProjectionMatrix;

var gFlag = false;
var tFlag = false;
var sFlag = false;
var cFlag = false;

// it is like swap buffer
var request_animationframe = window.requestAnimationFrame ||         // google chrome
                             window.mozRequestAnimationFrame ||      // mozilla
                             window.webkitRequestAnimationFrame ||   // apple safari
                             window.oRequestAnimationFrame ||        // opera
                             window.msRequestAnimationFrame;         // edge
                            
function main() 
{
    // code
    canvas = document.getElementById("MDM");

    if (!canvas) {
        console.log("obtaining canvas failed\n");
    }
    else {
        console.log("canvas obtained\n");
    }

    // backup canvas dimensions - karan pudhe lagnar aahe
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // initialize
    initialize();

    // resize
    resize();

    // display
    display();

    window.addEventListener("keydown", keyDown, false); 
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);
}

function toggleFullScreen()
{
    // code
    var fullscreen_element = document.fullScreen_Element||
							 document.mozFullScreenElement||
							 document.webkitFullscreenElement||
							 document.msFullscreenElement||
							 null; // ji value as per web browser to gheil
    

    if (fullscreen_element == null) // if not fullScreen
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozrequestFullScreen)
        {
            canvas.mozrequestFullScreen();
        }
        else if (canvas.webkitrequestFullscreen)
        {
            canvas.webkitrequestFullscreen();
        }
        else if (canvas.msrequestFullscreen)
        {
            canvas.msrequestFullscreen();
        }
        bFullScreen = true;
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozexitFullScreen)
        {
            document.mozexitFullScreen();
        }
        else if (document.webkitexitFullscreen)
        {
            document.webkitexitFullscreen();
        }
        else if (document.msexitFullscreen)
        {
            document.msexitFullscreen();
        }
        bFullScreen = false;
    }
}

function initialize()
{
    // code

    // Obtain WebGL2 context
    gl = canvas.getContext("webgl2"); // webgl2 for OpenGLES 3.0

    if (!gl)
    {
        console.log("Obtaining WebGL context failed\n");
    }
    else
    {
        console.log("WebGL context obtained\n");
    }

    // Set viewport width and height of WebGL context
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // Shader Code

    // Change - "#version 300 es" + : WebGL 2.0 is based on OpenGLES 3.0

    // Vertex Shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 a_position;" +
        "in vec4 a_color;" +
        "uniform mat4 u_mvpMatrix;" +
        "out vec4 a_color_out;" +
        "void main(void)" +
        "{" +
            "gl_Position = u_mvpMatrix * a_position;" +
            "a_color_out = a_color;" +
        "}";

    var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);

    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0)
        {
            alert("Vertex Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Fragment Shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 a_color_out;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
            "FragColor = a_color_out;" +
        "}";

    var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert("Fragment Shader compilation log : " + error);
            uninitialize();
        }
    }

    // Shader Program
    shaderProgramObject = gl.createProgram();
    
    gl.attachShader(shaderProgramObject, vertexShaderObject); 
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // Pre-linking (Shader Attribute Binding)
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_POSITION, "a_position");
    gl.bindAttribLocation(shaderProgramObject, webGLMacros.AMC_ATTRIBUTE_COLOR, "a_color");

    // Shader program linking
    gl.linkProgram(shaderProgramObject);

    if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false)
    {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0)
        {
            alert("Shader Program link log : " + error);
            uninitialize();
        }
    }

    // post linking uniform location
    mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_mvpMatrix");

    // vao & vbo related code
    var horizontalLine = new Float32Array
    (
        [
            1.0, 0.0, 0.0,
		    -1.0, 0.0, 0.0,
        ]
    );

    var horizontalLineColor = new Float32Array
    (
        [
            1.0, 0.0, 0.0,
            1.0, 0.0, 0.0,
            1.0, 0.0, 0.0
        ]
    );

    var verticalLine = new Float32Array
    (
        [
            0.0, 1.0, 0.0,
            0.0, -1.0, 0.0
        ]
    );

    var verticalLineColor = new Float32Array
    (
        [
            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 1.0, 0.0
        ]
    );

    var graphHLines = new Float32Array
    (
        [
            -1.0, 1.0, 0.0, // 1
            1.0, 1.0, 0.0,

            -1.0, 0.95, 0.0, // 2
            1.0, 0.95, 0.0,

            -1.0, 0.90, 0.0, // 3
            1.0, 0.90, 0.0,

            -1.0, 0.85, 0.0, // 4
            1.0, 0.85, 0.0,

            -1.0, 0.80, 0.0, // 5
            1.0, 0.80, 0.0,

            -1.0, 0.75, 0.0, // 6
            1.0, 0.75, 0.0,

            -1.0, 0.70, 0.0, // 7
            1.0, 0.70, 0.0,

            -1.0, 0.65, 0.0, // 8
            1.0, 0.65, 0.0,

            -1.0, 0.60, 0.0, // 9
            1.0, 0.60, 0.0,

            -1.0, 0.55, 0.0, // 10
            1.0, 0.55, 0.0,

            -1.0, 0.50, 0.0, // 11
            1.0, 0.50, 0.0,

            -1.0, 0.45, 0.0, // 12
            1.0, 0.45, 0.0,

            -1.0, 0.40, 0.0, // 13
            1.0, 0.40, 0.0,

            -1.0, 0.35, 0.0, // 14
            1.0, 0.35, 0.0,

            -1.0, 0.30, 0.0, // 15
            1.0, 0.30, 0.0,

            -1.0, 0.25, 0.0, // 16
            1.0, 0.25, 0.0,

            -1.0, 0.20, 0.0, // 17
            1.0, 0.20, 0.0,

            -1.0, 0.15, 0.0, // 18
            1.0, 0.15, 0.0,

            -1.0, 0.10, 0.0, // 19
            1.0, 0.10, 0.0,

            -1.0, 0.05, 0.0, // 20
            1.0, 0.05, 0.0,

            -1.0, -1.0, 0.0, // 21
            1.0, -1.0, 0.0,

            -1.0, -0.95, 0.0, // 22
            1.0, -0.95, 0.0,

            -1.0, -0.90, 0.0, // 23
            1.0, -0.90, 0.0,

            -1.0, -0.85, 0.0, // 24
            1.0, -0.85, 0.0,

            -1.0, -0.80, 0.0, // 25
            1.0, -0.80, 0.0,

            -1.0, -0.75, 0.0, // 26
            1.0, -0.75, 0.0,

            -1.0, -0.70, 0.0, // 27
            1.0, -0.70, 0.0,

            -1.0, -0.65, 0.0, // 28
            1.0, -0.65, 0.0,

            -1.0, -0.60, 0.0, // 29
            1.0, -0.60, 0.0,

            -1.0, -0.55, 0.0, // 30
            1.0, -0.55, 0.0,

            -1.0, -0.50, 0.0, // 31
            1.0, -0.50, 0.0,

            -1.0, -0.45, 0.0, // 32
            1.0, -0.45, 0.0,

            -1.0, -0.40, 0.0, // 33
            1.0, -0.40, 0.0,

            -1.0, -0.35, 0.0, // 34
            1.0, -0.35, 0.0,

            -1.0, -0.30, 0.0, // 35
            1.0, -0.30, 0.0,

            -1.0, -0.25, 0.0, // 36
            1.0, -0.25, 0.0,

            -1.0, -0.20, 0.0, // 37
            1.0, -0.20, 0.0,

            -1.0, -0.15, 0.0, // 38
            1.0, -0.15, 0.0,

            -1.0, -0.10, 0.0, // 39
            1.0, -0.10, 0.0,

            -1.0, -0.05, 0.0, // 40
            1.0, -0.05, 0.0,
        ]
    );

    var graphVLines = new Float32Array
    (
        [
            1.0, 1.0, 0.0, // 1
            1.0, -1.0, 0.0,

            0.95, 1.0, 0.0, // 2
            0.95, -1.0, 0.0,

            0.90, 1.0, 0.0, // 3
            0.90, -1.0, 0.0,

            0.85, 1.0, 0.0, // 4
            0.85, -1.0, 0.0,

            0.80, 1.0, 0.0, // 5
            0.80, -1.0, 0.0,

            0.75, 1.0, 0.0, // 6
            0.75, -1.0, 0.0,

            0.70, 1.0, 0.0, // 7
            0.70, -1.0, 0.0,

            0.65, 1.0, 0.0, // 8
            0.65, -1.0, 0.0,

            0.60, 1.0, 0.0, // 9
            0.60, -1.0, 0.0,

            0.55, 1.0, 0.0, // 10
            0.55, -1.0, 0.0,

            0.50, 1.0, 0.0, // 11
            0.50, -1.0, 0.0,

            0.45, 1.0, 0.0, // 12
            0.45, -1.0, 0.0,

            0.40, 1.0, 0.0, // 13
            0.40, -1.0, 0.0,

            0.35, 1.0, 0.0, // 14
            0.35, -1.0, 0.0,

            0.30, 1.0, 0.0, // 15
            0.30, -1.0, 0.0,

            0.25, 1.0, 0.0, // 16
            0.25, -1.0, 0.0,

            0.20, 1.0, 0.0, // 17
            0.20, -1.0, 0.0,

            0.15, 1.0, 0.0, // 18
            0.15, -1.0, 0.0,

            0.10, 1.0, 0.0, // 19
            0.10, -1.0, 0.0,

            0.05, 1.0, 0.0, // 20
            0.05, -1.0, 0.0,

            -1.0, 1.0, 0.0, // 21
            -1.0, -1.0, 0.0,

            -0.95, 1.0, 0.0, // 22
            -0.95, -1.0, 0.0,

            -0.90, 1.0, 0.0, // 23
            -0.90, -1.0, 0.0,

            -0.85, 1.0, 0.0, // 24
            -0.85, -1.0, 0.0,

            -0.80, 1.0, 0.0, // 25
            -0.80, -1.0, 0.0,

            -0.75, 1.0, 0.0, // 26
            -0.75, -1.0, 0.0,

            -0.70, 1.0, 0.0, // 27
            -0.70, -1.0, 0.0,

            -0.65, 1.0, 0.0, // 28
            -0.65, -1.0, 0.0,

            -0.60, 1.0, 0.0, // 29
            -0.60, -1.0, 0.0,

            -0.55, 1.0, 0.0, // 30
            -0.55, -1.0, 0.0,

            -0.50, 1.0, 0.0, // 31
            -0.50, -1.0, 0.0,

            -0.45, 1.0, 0.0, // 32
            -0.45, -1.0, 0.0,

            -0.40, 1.0, 0.0, // 33
            -0.40, -1.0, 0.0,

            -0.35, 1.0, 0.0, // 34
            -0.35, -1.0, 0.0,

            -0.30, 1.0, 0.0, // 35
            -0.30, -1.0, 0.0,

            -0.25, 1.0, 0.0, // 36
            -0.25, -1.0, 0.0,

            -0.20, 1.0, 0.0, // 37
            -0.20, -1.0, 0.0,

            -0.15, 1.0, 0.0, // 38
            -0.15, -1.0, 0.0,

            -0.10, 1.0, 0.0, // 39
            -0.10, -1.0, 0.0,

            -0.05, 1.0, 0.0, // 40
            -0.05, -1.0, 0.0
        ]
    );

    var graphHVLinesColor = new Float32Array
    (
        [
            0.0, 0.0, 1.0, // 1
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 2
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 3
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 4
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 5
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 6
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 7
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 8
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 9
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 10
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 11
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 12
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 13
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 14
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 15
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 16
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 17
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 18
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 19
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 20
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 21
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 22
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 23
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 24
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 25
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 26
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 27
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 28
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 29
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 30
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 31
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 32
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 33
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 34
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 35
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 36
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 37
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 38
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 39
            0.0, 0.0, 1.0,

            0.0, 0.0, 1.0, // 40
            0.0, 0.0, 1.0,
        ]
    );

    // Horizontal Line
    // vao realted code
    vao_horizontalLine = gl.createVertexArray();
    gl.bindVertexArray(vao_horizontalLine);

    // vbo
    vbo_horizontalLine_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_horizontalLine_position);
    gl.bufferData(gl.ARRAY_BUFFER, horizontalLine, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color
    vbo_horizontalLine_color = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_horizontalLine_color);
    gl.bufferData(gl.ARRAY_BUFFER, horizontalLineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // Vertical Line
    // vao realted code
    vao_verticalLine = gl.createVertexArray();
    gl.bindVertexArray(vao_verticalLine);

    // vbo
    vbo_verticalLine_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_verticalLine_position);
    gl.bufferData(gl.ARRAY_BUFFER, verticalLine, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color
    vbo_verticalLine_color = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_verticalLine_color);
    gl.bufferData(gl.ARRAY_BUFFER, verticalLineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // Graph Horizontal Lines
    // vao realted code
    vao_graphHLines = gl.createVertexArray();
    gl.bindVertexArray(vao_graphHLines);

    // vbo
    vbo_graphHLines_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_graphHLines_position);
    gl.bufferData(gl.ARRAY_BUFFER, graphHLines, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color
    vbo_graphHLines_color = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_graphHLines_color);
    gl.bufferData(gl.ARRAY_BUFFER, graphHVLinesColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // Graph Vertical Lines
    // vao realted code
    vao_graphVLines = gl.createVertexArray();
    gl.bindVertexArray(vao_graphVLines);

    // vbo
    vbo_graphVLines_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_graphVLines_position);
    gl.bufferData(gl.ARRAY_BUFFER, graphVLines, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color
    vbo_graphHLines_color = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_graphHLines_color);
    gl.bufferData(gl.ARRAY_BUFFER, graphHVLinesColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    height = ((Math.sqrt(3)) * (base1 / 2));
    base = base / 2;
    height = height / 2;
    var median = (base1 / Math.sqrt(3)) / -4;

    var triangleLine = new Float32Array
    (
        [
            0.0, (height - median), 0.0,
            -base, -height - median, 0.0,
            -base, -height - median, 0.0,
            base, -height - median, 0.0,
            base, -height - median, 0.0,
            0.0, (height - median), 0.0
        ]
    );

    var triangleLineColor = new Float32Array
    (
        [
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0
        ]
    );

    // Triangle Line
    // vao realted code
    vao_triangleLines = gl.createVertexArray();
    gl.bindVertexArray(vao_triangleLines);

    // vbo
    vbo_triangleLines_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangleLines_position);
    gl.bufferData(gl.ARRAY_BUFFER, triangleLine, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color
    vbo_triangleLines_color = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangleLines_color);
    gl.bufferData(gl.ARRAY_BUFFER, triangleLineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    var squareLine = new Float32Array
    (
        [
            (((base1 / Math.sqrt(3)) * 2) / 2), (((base1 / Math.sqrt(3)) * 2) / 2), 0.0,
            (-(((base1 / Math.sqrt(3)) * 2) / 2)), (((base1 / Math.sqrt(3)) * 2) / 2), 0.0,

            (-(((base1 / Math.sqrt(3)) * 2) / 2)), (((base1 / Math.sqrt(3)) * 2) / 2), 0.0,
            (-(((base1 / Math.sqrt(3)) * 2) / 2)), (-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0,

            (-(((base1 / Math.sqrt(3)) * 2) / 2)), (-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0,
            (((base1 / Math.sqrt(3)) * 2) / 2), (-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0,

            (((base1 / Math.sqrt(3)) * 2) / 2), (-(((base1 / Math.sqrt(3)) * 2) / 2)), 0.0,
            (((base1 / Math.sqrt(3)) * 2) / 2), (((base1 / Math.sqrt(3)) * 2) / 2), 0.0,
        ]
    );

    var squareLineColor = new Float32Array
    (
        [
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
        ]
    );

    // Square Line
    // vao realted code
    vao_squareLines = gl.createVertexArray();
    gl.bindVertexArray(vao_squareLines);

    // vbo
    vbo_squareLines_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_squareLines_position);
    gl.bufferData(gl.ARRAY_BUFFER, squareLine, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color
    vbo_squareLines_color = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_squareLines_color);
    gl.bufferData(gl.ARRAY_BUFFER, squareLineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    var circleLine = new Float32Array
    (
        [
            0.692820, 0.0, 0.0,
            0.691455, 0.043481, 0.0,
            0.687363, 0.086790, 0.0,
            0.680561, 0.129757, 0.0,
            0.671076, 0.172212, 0.0,
            0.658945, 0.213988, 0.0,
            0.644217, 0.254921, 0.0,
            0.626948, 0.294849, 0.0,
            0.607208, 0.333614, 0.0,
            0.585074, 0.371064, 0.0,
            0.560633, 0.407051, 0.0,
            0.533982, 0.441433, 0.0,
            0.505226, 0.474075, 0.0,
            0.474477, 0.504848, 0.0,
            0.441858, 0.533630, 0.0,
            0.407497, 0.560309, 0.0,
            0.371530, 0.584778, 0.0,
            0.334097, 0.606942, 0.0,
            0.295348, 0.626713, 0.0,
            0.255434, 0.644014, 0.0,
            0.214513, 0.658775, 0.0,
            0.172746, 0.670939, 0.0,
            0.130298, 0.680457, 0.0,
            0.087337, 0.687293, 0.0,
            0.044031, 0.691420, 0.0,
            0.000552, 0.692820, 0.0,
            -0.042930, 0.691489, 0.0,
            -0.086242, 0.687432, 0.0,
            -0.129215, 0.680664, 0.0,
            -0.171677, 0.671213, 0.0,
            -0.213463, 0.659116, 0.0,
            -0.254408, 0.644420, 0.0,
            -0.294349, 0.627183, 0.0,
            -0.333130, 0.607474, 0.0,
            -0.370598, 0.585369, 0.0,
            -0.406604, 0.560957, 0.0,
            -0.441008, 0.534333, 0.0,
            -0.473673, 0.505603, 0.0,
            -0.504470, 0.474879, 0.0,
            -0.533278, 0.442283, 0.0,
            -0.559984, 0.407943, 0.0,
            -0.584482, 0.371995, 0.0,
            -0.606676, 0.334581, 0.0,
            -0.626478, 0.295847, 0.0,
            -0.643810, 0.255947, 0.0,
            -0.658604, 0.215038, 0.0,
            -0.670801, 0.173281, 0.0,
            -0.680354, 0.130840, 0.0,
            -0.687224, 0.087884, 0.0,
            -0.691384, 0.044582, 0.0,
            -0.692819, 0.001103, 0.0,
            -0.691523, -0.042379, 0.0,
            -0.687500, -0.085695, 0.0,
            -0.680767, -0.128672, 0.0,
            -0.671350, -0.171143, 0.0,
            -0.659285, -0.212939, 0.0,
            -0.644622, -0.253895, 0.0,
            -0.627417, -0.293850, 0.0,
            -0.607739, -0.332647, 0.0,
            -0.585664, -0.370132, 0.0,
            -0.561281, -0.406158, 0.0,
            -0.534684, -0.440582, 0.0,
            -0.505980, -0.473270, 0.0,
            -0.475281, -0.504092, 0.0,
            -0.442708, -0.532926, 0.0,
            -0.408389, -0.559659, 0.0,
            -0.372461, -0.584186, 0.0,
            -0.335064, -0.606409, 0.0,
            -0.296346, -0.626242, 0.0,
            -0.256459, -0.643606, 0.0,
            -0.215562, -0.658432, 0.0,
            -0.173815, -0.670663, 0.0,
            -0.131382, -0.680249, 0.0,
            -0.088431, -0.687154, 0.0,
            -0.045132, -0.691349, 0.0,
            -0.001655, -0.692818, 0.0,
            0.041829, -0.691557, 0.0,
            0.085147, -0.687568, 0.0,
            0.128130, -0.680869, 0.0,
            0.170608, -0.671486, 0.0,
            0.212414, -0.659455, 0.0,
            0.253381, -0.644824, 0.0,
            0.293350, -0.627651, 0.0,
            0.332162, -0.608003, 0.0,
            0.369665, -0.585959, 0.0,
            0.405710, -0.561604, 0.0,
            0.440156, -0.535035, 0.0,
            0.472867, -0.506357, 0.0,
            0.503713, -0.475682, 0.0,
            0.532573, -0.443132, 0.0,
            0.559334, -0.408835, 0.0,
            0.583889, -0.372926, 0.0,
            0.606142, -0.335547, 0.0,
            0.626006, -0.296844, 0.0,
            0.643402, -0.256972, 0.0,
            0.658260, -0.216086, 0.0,
            0.670524, -0.174349, 0.0,
            0.680144, -0.131924, 0.0,
            0.687083, -0.088979, 0.0,
            0.691313, -0.045683, 0.0,
            0.692817, -0.002207, 0.0,
        ]
    );

    var circleLineColor = new Float32Array
    (
        [
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
        ]
    );

    // Circle Line
    // vao realted code
    vao_circleLines = gl.createVertexArray();
    gl.bindVertexArray(vao_circleLines);

    // vbo
    vbo_circleLines_position = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circleLines_position);
    gl.bufferData(gl.ARRAY_BUFFER, circleLine, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // color
    vbo_circleLines_color = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circleLines_color);
    gl.bufferData(gl.ARRAY_BUFFER, circleLineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(webGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(webGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // Clear the screen by blue color
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Blue

    //depth related changes
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    if (canvas.height <= 0)
        canvas.height = 1;

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat(canvas.width)) / (parseFloat(canvas.height)), 0.1, 100.0);
}

function display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Use the shader program object
    gl.useProgram(shaderProgramObject);

    // transformations
    var translationMatrix = mat4.create();
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(translationMatrix, translationMatrix, [0.0, 0.0, -2.0]);

    modelViewMatrix = translationMatrix;

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

    gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix);

    if (gFlag == true)
    {
        gl.bindVertexArray(vao_graphHLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        gl.lineWidth(3);
        gl.drawArrays(gl.LINES, 0, 80);

        gl.bindVertexArray(null);

        gl.bindVertexArray(vao_graphVLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        gl.lineWidth(3);
        gl.drawArrays(gl.LINES, 0, 80);

        gl.bindVertexArray(null);

        gl.bindVertexArray(vao_horizontalLine);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        gl.lineWidth(3);
        gl.drawArrays(gl.LINES, 0, 2);

        gl.bindVertexArray(null);

        gl.bindVertexArray(vao_verticalLine);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        gl.lineWidth(3);
        gl.drawArrays(gl.LINES, 0, 2);

        gl.bindVertexArray(null);
    }

    if (tFlag == true && gFlag == true)
    {
        gl.bindVertexArray(vao_triangleLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        gl.lineWidth(3);
        gl.drawArrays(gl.LINES, 0, 6);

        gl.bindVertexArray(null);
    }

    if (sFlag == true && gFlag == true)
    {
        gl.bindVertexArray(vao_squareLines);

        // stepE 2 : draw the desired graphics/animation
        // here will be magic code
        gl.lineWidth(3);
        gl.drawArrays(gl.LINES, 0, 8);

        gl.bindVertexArray(null);
    }

    if (cFlag == true && gFlag == true)
    {
        gl.bindVertexArray(vao_circleLines);
        gl.lineWidth(3);
        gl.drawArrays(gl.LINE_LOOP, 0, 100);
        gl.bindVertexArray(null);
    }

    gl.useProgram(null);

    // double buffering emulation - real double buffering js madhye nahi to internal hoto
    request_animationframe(display, canvas);
    // 1st para semi recursion of display
    // 2nd para kona varti tar canvas var
}

function update()
{
    // code

}

// keyboard event listner
function keyDown() 
{
    // code
    switch (event.keyCode)
    {
        case 81: // case 27 escape he browser cha inbuilt aahe key
            uninitialize();
            window.close(); // not all browsers will follow this
            break;

        case 70:
            toggleFullScreen();
            break;

        case 71:
            if (gFlag == false)
            {
                gFlag = true;
            }
            else
            {   
                gFlag = false;
            }
            break;

        case 84:
            if (tFlag == false)
            {
                tFlag = true;
            }
            else
            {
                tFlag = false;
            }
            break;

        case 83:
            if (sFlag == false)
            {
                sFlag = true;
            }
            else
            {
                sFlag = false;
            }
            break;

        case 67:
            if (cFlag == false)
            {
                cFlag = true;
            }
            else
            {
                cFlag = false;
            }
            break;
    }
}

function mouseDown()
{
    // code
}

function uninitialize()
{
    // code

    // deletion/ uninitialization of vbo

    if (vbo_verticalLine_position)
    {
        gl.deleteBuffer(vbo_verticalLine_position);
        vbo_verticalLine_position = null;
    }

    if (vao_verticalLine)
    {
        gl.deleteVertexArray(vao_verticalLine);
        vao_verticalLine = null;
    }

    if (vbo_horizontalLine_position)
    {
        gl.deleteBuffer(vbo_horizontalLine_position);
        vbo_horizontalLine_position = null;
    }

    if (vao_horizontalLine)
    {
        gl.deleteVertexArray(vao_horizontalLine);
        vao_horizontalLine = null;
    }

    // shader uninitialization
    if (shaderProgramObject)
    {
        gl.useProgram(shaderProgramObject);

        var shaderObjects = gl.getAttachedShaders(shaderProgramObject);

        for (let i = 0; i < shaderObjects.length; i++)
        {
            gl.detachShader(shaderProgramObject, shaderObjects[i]);
            gl.deleteShader(shaderObjects[i]);
            shaderObjects[i] = null;
        }

        gl.useProgram(null);
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}
