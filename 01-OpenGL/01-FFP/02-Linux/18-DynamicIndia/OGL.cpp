// standard header files
#include <stdio.h> // for standard io
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()
#include <math.h>

// X11 headers (X Server cha 11th Version)
#include <X11/Xlib.h> // X Client API
#include <X11/Xutil.h> // XVisualInfo
#include <X11/XKBlib.h> // for keyboard

// OpenGL header files
#include <GL/gl.h> // for OpenGL functionality
#include <GL/glu.h> 
#include <GL/glx.h> // for bridging api's

// macro definations
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
void jetPlaneOrange(void);
void jetPlaneWhite(void);
void jetPlaneGreen(void);
void letterI1(void);
void letterA1(void);
void letterF1(void);
void LetterI(void);
void LetterN(void);
void LetterD(void);
void LetterI1(void);
void LetterA(void);
void circle(double radius_x, double radius_y, float start, float end);
void circleBorder(double radius_x, double radius_y, float start, float end);

// global variables
Display *display = NULL;
XVisualInfo *visualInfo = NULL; // it is now structure pointer (use arrow (->) for accessing members)
Colormap colorMap;
Window window;

FILE* gpFile = NULL;
Bool fullscreen = False;
Bool bActiveWindow = False;

// OpenGL related variables
GLXContext glxContext;

// Colors 
float orangeR = 0.0f;
float orangeG = 0.0f;
float orangeB = 0.0f;
float whiteR = 0.0f;
float whiteG = 0.0f;
float whiteB = 0.0f;
float greenR = 0.0f;
float greenG = 0.0f;
float greenB = 0.0f;

// Letter Traversal
float xTravLetterI = -8.5f;
float yTravLetterN = 5.5f;
float yTravLetterI = -5.5f;
float xTravLetterA = 8.5f;

// plane movement variables
float j = 90.0f;
float angleOrange1 = 0.0f;
float angleOrange2 = 270.0f;
float angleGreen1 = 0.0f;
float angleGreen2 = 0.0f;
float xTrav = 0.0f;
float xTravWhitePlane = -16.0f;

// entry-point function
int main(void)
{
	// function declarations
	void toggleFullscreen(void);
	int initialize(void);
	void resize(int, int);
	void draw(void);
	void update(void);
	void uninitialize(void);
	
	// local variables declarations
	int defaultScreen;
	int defaultDepth;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom wm_delete_window_atom;
	XEvent event;
	KeySym keySym;
	int screenWidth;
	int screenHeight;
	char keys[26];
	static int winWidth;
	static int winHeight;
	int iRetVal = 0;

	static int frameBufferAttributes[] = // all are internal #define macros of int type
	{ 
	  GLX_DOUBLEBUFFER,
	  True,
	  GLX_RGBA, 
	  GLX_RED_SIZE,
	  8,
	  GLX_GREEN_SIZE,
	  8,
	  GLX_BLUE_SIZE,
	  8,
	  GLX_ALPHA_SIZE,
	  8,
	  None // XLib style to end array inline initialization, instead of None we can also write 0. it is same
	};

	Bool bDone = False;

	// code
	
	gpFile = fopen("Log.txt", "w"); 
	
	if (gpFile == 0)
	{
		printf("fopen() Failed");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n");
	}
	
	// step 1 : open the display
	display = XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		printf("ERROR : XOpenDisplay() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 2 - get default screen from display
	defaultScreen = XDefaultScreen(display); // primary monitor milala
	
	// Step 3 : get default depth from display and default screen
	defaultDepth = XDefaultDepth(display, defaultScreen);
	
	// step 4 : 
	visualInfo = glXChooseVisual(display, defaultScreen, frameBufferAttributes); // similar ChoosePixelFormat() in Windows
	// ***bridging api frameBufferAttributes shi match honara visual de, pointer return karto glxChooseVisual
	
	if (visualInfo == NULL)
	{
		printf("ERROR : glxChooseVisual() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 5 : Fill & initialize struct XSetWindow Attributes and along with that also set colomap and event_mask
	memset(&windowAttributes, 0, sizeof(XSetWindowAttributes));
	
	windowAttributes.border_pixel = 0; // default color deto
	windowAttributes.background_pixel = XBlackPixel(display, defaultScreen); // hbrBackground -> GetStockObject 
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display, 
						     RootWindow(display, visualInfo -> screen), // true color wali visual ghetoy
						     visualInfo -> visual, 
						     AllocNone);
	windowAttributes.event_mask = ExposureMask | KeyPressMask | StructureNotifyMask | FocusChangeMask;// WM_PAINT | WM_KEYDOWN
	
	// Step 6 : Initialize global colorMap using colormap from windowAttributes 
	colorMap = windowAttributes.colormap;
	
	// step 7 : Initialize Window Styles
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;
	
	// step 8 : create the window using XCreateWindow() & do error checking
	window = XCreateWindow(display,
				RootWindow(display, visualInfo -> screen), 
				0, // X
				0, // Y
				WIN_WIDTH, // width
				WIN_HEIGHT, // height
				0, // border width
				visualInfo -> depth,
				InputOutput,
				visualInfo -> visual,
				styleMask,
				&windowAttributes
				);
	
	if(!window)
	{
		printf("ERROR : XCreateWindow() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 9 : give name to your window in it's title/caption bar
	XStoreName(display, window, "Mandar Dilip Makse RTR2021-116 - OpenGL Window");
	
	// step 10 : creating and setting window manager protocol / atom
	wm_delete_window_atom = XInternAtom(display, "WM_DELTE_WINDOW", True); // True means atom always create
	XSetWMProtocols(display, window, &wm_delete_window_atom, 1);
	
	// step 11 : Actually show the window by XMapWindow()
	XMapWindow(display, window);
	
	// centering of window
	screenWidth = XWidthOfScreen(XScreenOfDisplay(display, defaultScreen));
	screenHeight = XHeightOfScreen(XScreenOfDisplay(display, defaultScreen));
	XMoveWindow(display, window, ((screenWidth - WIN_WIDTH) / 2), ((screenHeight - WIN_HEIGHT)/ 2));

	initialize();
	
	if (iRetVal == -1)
	{
		fprintf(gpFile, "glxContext can not be obtained\n\n");
		uninitialize();
	}
	else
	{
		fprintf(gpFile, "Initialize() Successful\n\n");
	}
	
	 // step 12 : create the message loop
	 while(bDone == False) // 1) OpenGL chnage
	 {
		 while(XPending(display)) // 2) OPenGL change 2, XPending = Xlib | PeekMessage = Windows (Win32)
	 	{
	 		XNextEvent(display, &event); // a) getting next event by using XNextEvent() 
	 	
	 		switch (event.type)
	 		{
	 			case MapNotify: // WM_CREATE
	 				break;

				case FocusIn:
					bActiveWindow = True;
					break;

				case FocusOut:
					bActiveWindow = False;
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;

					resize(winWidth, winHeight);

					break;
	 			
	 			case KeyPress: // b) handling the keypress of escape
	 				keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0); // 3rd key will br symbol, 4th will be not companied by shift key | like WM_KEYDOWN
	 			
	 			switch(keySym)
	 			{
	 				case XK_Escape:
	 					bDone = True;
	 					break;
	 			}

				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL); // like WM_CHAR

				switch(keys[0])
				{
					case 'F':
					case 'f':
						if (fullscreen == False)
						{
							toggleFullscreen();
							fullscreen = True;
						}
						else
						{
							toggleFullscreen();
							fullscreen = False;
						}
						break;
				}
				
	 			break;
	 			
	 			case 33: // c) handaling message 33, wm_delete_window_atom and 33 are analogous to each other
	 				bDone = True;
	 				break;
	 		}
	 	}
		
		if (bActiveWindow == True)
		{
			// here there should be call to update()

			draw();
			
			update();
		}
	 }
	 
	 // step 13 - call uninitialize() and return()
	 uninitialize();
	
	return(0);
}

void toggleFullscreen(void)
{
	// local variables
	Atom wm_current_state_atom;
	Atom wm_fullscreen_state_atom;
	XEvent xevent;

	// code
	wm_current_state_atom = XInternAtom(display, "_NET_WM_STATE", False);
	wm_fullscreen_state_atom = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

	memset(&xevent, 0, sizeof(XEvent));

	xevent.type = ClientMessage;
	xevent.xclient.window = window;
	xevent.xclient.message_type = wm_current_state_atom;
	xevent.xclient.format = 32;
	xevent.xclient.data.l[0] = fullscreen ? 0 : 1;
	xevent.xclient.data.l[1] = wm_fullscreen_state_atom;

	XSendEvent(display, 
			   RootWindow(display, visualInfo -> screen),
			   False,
			   SubstructureNotifyMask,
			   &xevent);
}

int initialize(void)
{
	// code
	glxContext = glXCreateContext(display, visualInfo, NULL, True); // same as making create context in windows // check recoording
	
	if (glxContext == 0)
		return(-1);

	// make current context
	glXMakeCurrent(display, window, glxContext);

	// Here starts OpenGL functions
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	return(0);
}

void resize(int width, int height)
{
	// code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();

	gluPerspective(45.0f, 
				 (GLfloat)width 
		         / (GLfloat)height,
				 0.1f,
		         100.0f); 
	// height should not be 0
}

void draw(void)
{
	// code

	glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(xTravLetterI, -0.1f, -10.0f);
	LetterI();

	glLoadIdentity();
	glTranslatef(-2.0f, yTravLetterN, -10.0f);
	LetterN();

	glLoadIdentity();
	glTranslatef(0.0f, -0.1f, -10.0f);
	LetterD();

	glLoadIdentity();
	glTranslatef(2.0f, yTravLetterI, -10.0f);
	LetterI1();

	glLoadIdentity();
	glTranslatef(xTravLetterA, -0.1f, -10.0f);
	LetterA();

	if (orangeR >= 1.0f)
	{
		glLoadIdentity();
		glTranslatef(-5.5f, 6.0f, -19.0f);
		glRotatef(angleOrange1, 0.0f, 0.0f, 1.0f);
		glTranslatef((cos(2 * 3.14 * j / 180) * 0.2f) - 6.0f, (sin(2 * 3.14 * j / 180) * 1.0f) + 2.5f, 0.0f);
		glRotatef(270, 0.0f, 0.0f, 1.0f);

		if (angleOrange1 >= 90.0f)
		{
			glTranslatef(xTrav, 0.0f, 0.0f);
			if (xTrav >= 16.0f)
			{
				glTranslatef(-2.5f, 7.0f, 0.0f);
				glRotatef(angleOrange2, 0.0f, 0.0f, 1.0f);
				glTranslatef((sin(2 * 3.14 * j / 360) * 4.5f) + 2.5f, (cos(2 * 3.14 * j / 360) * 0.2f) + 2.5f, 0.0f);
				glRotatef(90, 0.0f, 0.0f, 1.0f);
			}
		}

		jetPlaneOrange();

		glLoadIdentity();
		glTranslatef(xTravWhitePlane, -0.2f, -19.0f);
		jetPlaneWhite();

		glLoadIdentity();
		glTranslatef(-5.5f, -6.4f, -19.0f);
		glRotatef(angleGreen1, 0.0f, 0.0f, 1.0f);
		glTranslatef((cos(2 * 3.14 * j / 180) * 0.2f) - 6.0f, (sin(2 * 3.14 * j / 180) * 1.0f) - 2.5f, 0.0f);
		glRotatef(90, 0.0f, 0.0f, 1.0f);

		if (angleGreen1 <= -90.0f)
		{
			glTranslatef(xTrav, 0.0f, 0.0f);
			if (xTrav >= 16.0f)
			{
				glTranslatef(-2.8f, -6.5f, 0.0f);
				glRotatef(angleGreen2, 0.0f, 0.0f, 1.0f);
				glTranslatef((cos(2 * 3.14 * j / 180) * 0.2f) + 3.3f, (sin(2 * 3.14 * j / 180) * 2.5f) + 6.5f, 0.0f);
				glRotatef(360, 0.0f, 0.0f, 1.0f);
			}
		}

		jetPlaneGreen();
	}

	glXSwapBuffers(display, window);
}

void LetterI(void)
{
	glBegin(GL_QUADS);

	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Middle Top Bar
	glVertex3f(0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.75f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(0.14f, 0.01f, 0.0f);

	// Middle Middle Bar

	glVertex3f(0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(0.14f, -0.01f, 0.0f);

	// Middle Bottom Bar
	glVertex3f(0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.14f, -0.75f, 0.0f);
	glVertex3f(0.14f, -0.75f, 0.0f);

	//glVertex3f(-0.15f, -0.75f, 0.0f);
	//glVertex3f(0.15f, -0.75f, 0.0f);

	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();
}

void LetterN(void)
{
	glBegin(GL_QUADS);

	// Left Bar
	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.55f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.01f, 0.0f);
	glVertex3f(-0.55f, 0.01f, 0.0f);

	// Middle Bar
	glVertex3f(-0.55f, 0.01f, 0.0f);
	glVertex3f(-0.8f, 0.01f, 0.0f);
	glVertex3f(-0.8f, -0.01f, 0.0f);
	glVertex3f(-0.55f, -0.01f, 0.0f);

	// Left Bar Down
	glVertex3f(-0.55f, -0.01f, 0.0f);
	glVertex3f(-0.8f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.55f, -1.0f, 0.0f);

	// Right Bar
	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.55f, 1.0f, 0.0f);
	glVertex3f(0.8f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.01f, 0.0f);
	glVertex3f(0.55f, 0.01f, 0.0f);

	// Middle Bar
	glVertex3f(0.55f, 0.01f, 0.0f);
	glVertex3f(0.8f, 0.01f, 0.0f);
	glVertex3f(0.8f, -0.01f, 0.0f);
	glVertex3f(0.55f, -0.01f, 0.0f);

	// Left Bar Down
	glVertex3f(0.55f, -0.01f, 0.0f);
	glVertex3f(0.8f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(0.55f, -1.0f, 0.0f);

	// Middle Tilted Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.5f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-0.15f, 0.01f, 0.0f);
	glVertex3f(0.15f, 0.01f, 0.0f);

	glVertex3f(0.15f, 0.01f, 0.0f);
	glVertex3f(-0.15f, 0.01f, 0.0f);
	glVertex3f(-0.138f, -0.01f, 0.0f);
	glVertex3f(0.162f, -0.01f, 0.0f);

	glVertex3f(0.162f, -0.01f, 0.0f);
	glVertex3f(-0.138f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.8f, -1.0f, 0.0f);


	glEnd();
}

void LetterI1(void)
{
	glBegin(GL_QUADS);

	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Middle Top Bar
	glVertex3f(0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.75f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(0.14f, 0.01f, 0.0f);

	// Middle Middle Bar

	glVertex3f(0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(0.14f, -0.01f, 0.0f);

	// Middle Bottom Bar
	glVertex3f(0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.14f, -0.75f, 0.0f);
	glVertex3f(0.14f, -0.75f, 0.0f);

	//glVertex3f(-0.15f, -0.75f, 0.0f);
	//glVertex3f(0.15f, -0.75f, 0.0f);

	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();
}

void LetterD(void)
{
	glBegin(GL_QUADS);

	// Top Bar
	//glColor3f(1.0f, 0.6f, 0.2f);
	glColor3f(orangeR, orangeG, orangeB);
	glVertex3f(0.55f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Left Bar
	glVertex3f(-0.3f, 0.75f, 0.0f);
	glVertex3f(-0.55f, 0.75f, 0.0f);
	//glColor3f(1.0f, 1.0f, 1.0f);
	glColor3f(whiteR, whiteG, whiteB);
	glVertex3f(-0.55f, 0.01f, 0.0f);
	glVertex3f(-0.3f, 0.01, 0.0f);

	// Left Bar Middle
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glColor3f(whiteR, whiteG, whiteB);
	glVertex3f(-0.3f, 0.01, 0.0f);
	glVertex3f(-0.55f, 0.01f, 0.0f);
	glVertex3f(-0.55f, -0.01f, 0.0f);
	glVertex3f(-0.3f, -0.01, 0.0f);

	// Left Bar Bottom
	glVertex3f(-0.3f, -0.01, 0.0f);
	glVertex3f(-0.55f, -0.01f, 0.0f);
	glColor3f(greenR, greenG, greenB);
	//glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.55f, -0.75f, 0.0f);
	glVertex3f(-0.3f, -0.75f, 0.0f);

	// Right Bar Top
	glColor3f(orangeR, orangeG, orangeB);
	glVertex3f(0.8f, 0.75f, 0.0f);
	glVertex3f(0.55f, 0.75f, 0.0f);
	glColor3f(whiteR, whiteG, whiteB);
	glVertex3f(0.55f, 0.01f, 0.0f);
	glVertex3f(0.8f, 0.01, 0.0f);

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.01, 0.0f);
	glVertex3f(0.55f, 0.01f, 0.0f);
	glVertex3f(0.55f, -0.01f, 0.0f);
	glVertex3f(0.8f, -0.01, 0.0f);

	glVertex3f(0.8f, -0.01, 0.0f);
	glVertex3f(0.55f, -0.01f, 0.0f);
	glColor3f(greenR, greenG, greenB);
	//glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.55f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	// Bottom Bar
	glVertex3f(0.55f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();
}

void LetterA(void)
{
	glBegin(GL_QUADS);

	// Left Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glVertex3f(-0.258f, -0.01f, 0.0f);

	glVertex3f(-0.258f, -0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.854f, -1.0f, 0.0f);
	glVertex3f(-0.6f, -1.0f, 0.0f);

	// Right Top Bar
	// Left Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.495f, 0.01f, 0.0f);

	glVertex3f(0.495f, 0.01f, 0.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glVertex3f(0.503f, -0.01f, 0.0f);

	glVertex3f(0.503f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.6f, -1.0f, 0.0f);
	glVertex3f(0.854f, -1.0f, 0.0f);

	if (xTravWhitePlane >= 7.5f)
	{
		// Middle Bar
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(0.222f, 0.1f, 0.0f);
		glVertex3f(-0.222f, 0.1f, 0.0f);
		glVertex3f(-0.248f, 0.03f, 0.0f);
		glVertex3f(0.248f, 0.03f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.248f, 0.03f, 0.0f);
		glVertex3f(-0.248f, 0.03f, 0.0f);
		glVertex3f(-0.268f, -0.03f, 0.0f);
		glVertex3f(0.268f, -0.03f, 0.0f);

		glColor3f(0.0745f, 0.5333f, 0.3137f);
		glVertex3f(0.268f, -0.03f, 0.0f);
		glVertex3f(-0.268f, -0.03f, 0.0f);
		glVertex3f(-0.289f, -0.1f, 0.0f);
		glVertex3f(0.289f, -0.1f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
	}

	glEnd();
}

void jetPlaneOrange(void)
{
	glBegin(GL_QUADS);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.1f, 0.005f, 0.0f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);
	glVertex3f(1.1f, -0.005f, 0.0f);

	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	//glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);

	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);

	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);

	glVertex3f(-0.4f, 0.865f, 0.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.4f, 0.86f, 0.0f);

	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);

	glVertex3f(-0.4f, -0.865f, 0.0f);
	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.4f, -0.86f, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.9f, 0.85f, 0.0f);
	glVertex3f(-0.9f, -0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-0.9, -0.2f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.7176f, 0.7176f, 0.7098f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-2.1f, 0.07f, 0.0f);
	glVertex3f(-2.1f, -0.07f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.45f, 0.5f, 0.0f);
	glVertex3f(-1.68f, 0.5f, 0.0f);
	glVertex3f(-1.74f, 0.45f, 0.0f);
	glVertex3f(-1.74f, 0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.68f, 0.25f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glEnd();

	//glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.45f, -0.5f, 0.0f);
	glVertex3f(-1.68f, -0.5f, 0.0f);
	glVertex3f(-1.74f, -0.45f, 0.0f);
	glVertex3f(-1.74f, -0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.68f, -0.25f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glEnd();

	circle(0.2f, 0.06f, 0, 0);
	circleBorder(0.2f, 0.06f, 0, 0);

	//glLineWidth(9);

	glColor3f(0.3647f, 0.5411f, 0.6588f);
	glBegin(GL_LINES);

	glVertex3f(-1.5f, 0.0f, 0.0f);
	glVertex3f(-1.1f, 0.0f, 0.0f);

	glEnd();

	//glLineWidth(1.5f);

	glBegin(GL_LINES);

	glColor3f(0.5176f, 0.5176f, 0.5098f);

	glVertex3f(1.1f, 0.0f, 0.0f);
	glVertex3f(1.2f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);

	glVertex3f(-0.9f, -0.2f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);

	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glVertex3f(-1.2f, 0.17f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.17f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);

	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	glVertex3f(-1.4f, 0.17f, 0.0f);
	glVertex3f(-1.4f, -0.17f, 0.0f);

	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);

	glVertex3f(-1.68f, 0.08f, 0.0f);
	glVertex3f(-1.78f, 0.055f, 0.0f);

	glVertex3f(-1.68f, 0.15f, 0.0f);
	glVertex3f(-1.68f, -0.15f, 0.0f);

	glVertex3f(-1.68f, -0.08f, 0.0f);
	glVertex3f(-1.78f, -0.055f, 0.0f);

	glVertex3f(-1.68f, 0.0f, 0.0f);
	glVertex3f(-1.78f, 0.0f, 0.0f);

	glVertex3f(0.25f, 0.06f, 0.0f);
	glVertex3f(0.25f, -0.06f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.1f, 0.01f, 0.0f);

	glVertex3f(-1.1f, 0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, -0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.5f, -0.01f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-1.53f, 0.0f, 0.0f);
	glVertex3f(-1.23f, 0.0f, 0.0f);

	glEnd();

	glScalef(0.08f, 0.08f, 0.0f);
	glTranslatef(-8.0f, 0.0f, -30.0f);
	letterI1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterA1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterF1();
}

void jetPlaneWhite(void)
{
	glBegin(GL_QUADS);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.1f, 0.005f, 0.0f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);
	glVertex3f(1.1f, -0.005f, 0.0f);

	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	//glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);

	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);

	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);

	glVertex3f(-0.4f, 0.865f, 0.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.4f, 0.86f, 0.0f);

	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);

	glVertex3f(-0.4f, -0.865f, 0.0f);
	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.4f, -0.86f, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.9f, 0.85f, 0.0f);
	glVertex3f(-0.9f, -0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-0.9, -0.2f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.7176f, 0.7176f, 0.7098f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-2.1f, 0.07f, 0.0f);
	glVertex3f(-2.1f, -0.07f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.45f, 0.5f, 0.0f);
	glVertex3f(-1.68f, 0.5f, 0.0f);
	glVertex3f(-1.74f, 0.45f, 0.0f);
	glVertex3f(-1.74f, 0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.68f, 0.25f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glEnd();

	//glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.45f, -0.5f, 0.0f);
	glVertex3f(-1.68f, -0.5f, 0.0f);
	glVertex3f(-1.74f, -0.45f, 0.0f);
	glVertex3f(-1.74f, -0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.68f, -0.25f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glEnd();

	circle(0.2f, 0.06f, 0, 0);
	circleBorder(0.2f, 0.06f, 0, 0);

	//glLineWidth(9);

	glColor3f(0.3647f, 0.5411f, 0.6588f);
	glBegin(GL_LINES);

	glVertex3f(-1.5f, 0.0f, 0.0f);
	glVertex3f(-1.1f, 0.0f, 0.0f);

	glEnd();

	//glLineWidth(1.5f);

	glBegin(GL_LINES);

	glColor3f(0.5176f, 0.5176f, 0.5098f);

	glVertex3f(1.1f, 0.0f, 0.0f);
	glVertex3f(1.2f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);

	glVertex3f(-0.9f, -0.2f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);

	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glVertex3f(-1.2f, 0.17f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.17f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);

	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	glVertex3f(-1.4f, 0.17f, 0.0f);
	glVertex3f(-1.4f, -0.17f, 0.0f);

	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);

	glVertex3f(-1.68f, 0.08f, 0.0f);
	glVertex3f(-1.78f, 0.055f, 0.0f);

	glVertex3f(-1.68f, 0.15f, 0.0f);
	glVertex3f(-1.68f, -0.15f, 0.0f);

	glVertex3f(-1.68f, -0.08f, 0.0f);
	glVertex3f(-1.78f, -0.055f, 0.0f);

	glVertex3f(-1.68f, 0.0f, 0.0f);
	glVertex3f(-1.78f, 0.0f, 0.0f);

	glVertex3f(0.25f, 0.06f, 0.0f);
	glVertex3f(0.25f, -0.06f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.1f, 0.01f, 0.0f);

	glVertex3f(-1.1f, 0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, -0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.5f, -0.01f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-1.53f, 0.0f, 0.0f);
	glVertex3f(-1.23f, 0.0f, 0.0f);

	glEnd();

	glScalef(0.08f, 0.08f, 0.0f);
	glTranslatef(-8.0f, 0.0f, -30.0f);
	letterI1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterA1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterF1();
}

void jetPlaneGreen(void)
{
	glBegin(GL_QUADS);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.1f, 0.005f, 0.0f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);
	glVertex3f(1.1f, -0.005f, 0.0f);

	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	//glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);

	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);

	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);

	glVertex3f(-0.4f, 0.865f, 0.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.4f, 0.86f, 0.0f);

	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);

	glVertex3f(-0.4f, -0.865f, 0.0f);
	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.4f, -0.86f, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.9f, 0.85f, 0.0f);
	glVertex3f(-0.9f, -0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-0.9, -0.2f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.7176f, 0.7176f, 0.7098f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);

	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-2.1f, 0.07f, 0.0f);
	glVertex3f(-2.1f, -0.07f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.45f, 0.5f, 0.0f);
	glVertex3f(-1.68f, 0.5f, 0.0f);
	glVertex3f(-1.74f, 0.45f, 0.0f);
	glVertex3f(-1.74f, 0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.68f, 0.25f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glEnd();

	//glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.45f, -0.5f, 0.0f);
	glVertex3f(-1.68f, -0.5f, 0.0f);
	glVertex3f(-1.74f, -0.45f, 0.0f);
	glVertex3f(-1.74f, -0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.68f, -0.25f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glEnd();

	circle(0.2f, 0.06f, 0, 0);
	circleBorder(0.2f, 0.06f, 0, 0);

	//glLineWidth(9);

	glColor3f(0.3647f, 0.5411f, 0.6588f);
	glBegin(GL_LINES);

	glVertex3f(-1.5f, 0.0f, 0.0f);
	glVertex3f(-1.1f, 0.0f, 0.0f);

	glEnd();

	//glLineWidth(1.5f);

	glBegin(GL_LINES);

	glColor3f(0.5176f, 0.5176f, 0.5098f);

	glVertex3f(1.1f, 0.0f, 0.0f);
	glVertex3f(1.2f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);

	glVertex3f(-0.9f, -0.2f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);

	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glVertex3f(-1.2f, 0.17f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.17f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);

	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	glVertex3f(-1.4f, 0.17f, 0.0f);
	glVertex3f(-1.4f, -0.17f, 0.0f);

	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);

	glVertex3f(-1.68f, 0.08f, 0.0f);
	glVertex3f(-1.78f, 0.055f, 0.0f);

	glVertex3f(-1.68f, 0.15f, 0.0f);
	glVertex3f(-1.68f, -0.15f, 0.0f);

	glVertex3f(-1.68f, -0.08f, 0.0f);
	glVertex3f(-1.78f, -0.055f, 0.0f);

	glVertex3f(-1.68f, 0.0f, 0.0f);
	glVertex3f(-1.78f, 0.0f, 0.0f);

	glVertex3f(0.25f, 0.06f, 0.0f);
	glVertex3f(0.25f, -0.06f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.1f, 0.01f, 0.0f);

	glVertex3f(-1.1f, 0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, -0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.5f, -0.01f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-1.53f, 0.0f, 0.0f);
	glVertex3f(-1.23f, 0.0f, 0.0f);

	glEnd();

	glScalef(0.08f, 0.08f, 0.0f);
	glTranslatef(-8.0f, 0.0f, -30.0f);
	letterI1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterA1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterF1();
}


void letterI1(void)
{
	glColor3f(0.0f, 0.0f, 0.5019f);
	glBegin(GL_QUADS);

	// I
	// Top Bar
	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Middle Top Bar
	glVertex3f(0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(0.14f, 0.01f, 0.0f);

	// Middle Middle Bar

	glVertex3f(0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(0.14f, -0.01f, 0.0f);

	// Middle Bottom Bar
	glVertex3f(0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.75f, 0.0f);
	glVertex3f(0.14f, -0.75f, 0.0f);

	//glVertex3f(-0.15f, -0.75f, 0.0f);
	//glVertex3f(0.15f, -0.75f, 0.0f);

	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();

}

void letterA1(void)
{
	// A
	glBegin(GL_QUADS);

	// Left Top Bar
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);

	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glVertex3f(-0.258f, -0.01f, 0.0f);

	glVertex3f(-0.258f, -0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glVertex3f(-0.854f, -1.0f, 0.0f);
	glVertex3f(-0.6f, -1.0f, 0.0f);

	// Middle Bar
	glVertex3f(0.254f, 0.1f, 0.0f);
	glVertex3f(-0.224f, 0.1f, 0.0f);
	glVertex3f(-0.288f, -0.1f, 0.0f);
	glVertex3f(0.288f, -0.1f, 0.0f);

	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.275f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);

	// Right Top Bar
	// Left Top Bar
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.495f, 0.01f, 0.0f);

	glVertex3f(0.495f, 0.01f, 0.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glVertex3f(0.503f, -0.01f, 0.0f);

	glVertex3f(0.503f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glVertex3f(0.6f, -1.0f, 0.0f);
	glVertex3f(0.854f, -1.0f, 0.0f);

	// Middle Bar
	glVertex3f(0.224f, 0.1f, 0.0f);
	glVertex3f(-0.224f, 0.1f, 0.0f);
	glVertex3f(-0.289f, -0.1f, 0.0f);
	glVertex3f(0.289f, -0.1f, 0.0f);

	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.275f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);

	glEnd();
}

void letterF1(void)
{
	// F
	glBegin(GL_QUADS);

	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	glVertex3f(-0.55f, 0.75f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.55f, -1.0f, 0.0f);

	glVertex3f(0.25f, 0.14f, 0.0f);
	glVertex3f(-0.55f, 0.14f, 0.0f);
	glVertex3f(-0.55f, -0.12f, 0.0f);
	glVertex3f(0.25f, -0.12f, 0.0f);

	glEnd();
}

void circle(double radius_x, double radius_y, float start, float end)
{
	glBegin(GL_POLYGON);
	glColor3f(0.3647f, 0.5411f, 0.6588f);
	for (int i = 0; i <= 100; i++)
	{
		double angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		double x = cos(angle) * radius_x + 0.2f;
		double y = sin(angle) * radius_y;
		glVertex2d(start + x, end + y);
	}
	glEnd();
}

void circleBorder(double radius_x, double radius_y, float start, float end)
{
	//glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i <= 100; i++)
	{
		double angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		double x = cos(angle) * radius_x + 0.2f;
		double y = sin(angle) * radius_y;
		glVertex2d(start + x, end + y);
	}
	glEnd();
}

void update(void)
{
	// code

	if (xTravLetterI <= -4.0f)
	{
		xTravLetterI = xTravLetterI + 0.009f;
	}

	if (yTravLetterN >= -0.1f)
	{
		yTravLetterN = yTravLetterN - 0.009f;
	}

	if (yTravLetterI <= -0.1f)
	{
		yTravLetterI = yTravLetterI + 0.009f;
	}

	if (xTravLetterA >= 4.0f)
	{
		xTravLetterA = xTravLetterA - 0.009f;
	}

	if (yTravLetterI >= -0.1f)
	{
		if (orangeR <= 1.0f)
		{
			orangeR = orangeR + 0.004f;
		}

		if (whiteR <= 1.0f)
		{
			whiteR = whiteR + 0.004f;
		}

		if (greenR <= 0.0745f)
		{
			greenR = greenR + 0.004f;
		}

		if (orangeG <= 0.6f)
		{
			orangeG = orangeG + 0.004f;
		}

		if (whiteG <= 1.0f)
		{
			whiteG = whiteG + 0.004f;

		}

		if (greenG <= 0.5333f)
		{
			greenG = greenG + 0.004f;
		}

		if (orangeB <= 0.2f)
		{
			orangeB = orangeB + 0.004f;
		}

		if (whiteB <= 1.0f)
		{
			whiteB = whiteB + 0.004f;
		}

		if (greenB <= 0.3137f)
		{
			greenB = greenB + 0.004f;
		}
	}

	if (orangeR >= 1.0f)
	{
		if (angleOrange1 <= 90.0f)
		{
			angleOrange1 = angleOrange1 + 0.4f;
		}

		if (xTrav >= 16.0f)
		{
			if (angleOrange2 <= 400.0f)
			{
				angleOrange2 = angleOrange2 + 0.4f;
			}
		}

		if (xTravWhitePlane <= 16.5f)
		{
			xTravWhitePlane = xTravWhitePlane + 0.038f;
		}

		if (angleGreen1 >= -90.0f)
		{
			angleGreen1 = angleGreen1 - 0.4f;
		}

		if (xTrav >= 16.0f)
		{
			if (angleGreen2 >= -120.0f)
			{
				angleGreen2 = angleGreen2 - 0.39f;
			}
		}

		if (xTrav <= 16.0f && angleOrange1 >= 90.0f || angleGreen1 >= 90.0f)
		{
			xTrav = xTrav + 0.04f;
		}
	}
}


void uninitialize(void)
{

	// function declarations
	void ToggleFullScreen(void);

	// variable declarations
	GLXContext currentContext;

	// code
	currentContext = glXGetCurrentContext();

	if (currentContext && currentContext == glxContext)
	{
		glXMakeCurrent(display, 0, 0);
	}
	

	if (glxContext)
	{
		glXDestroyContext(display, glxContext);
		glxContext = NULL;
	}

	if (visualInfo)
	{
		free(visualInfo);
		visualInfo = NULL;
	}

	if (fullscreen)
	{
		toggleFullscreen();
		fullscreen = False;
	}

	if (window)
	{
		XDestroyWindow(display, window);
	}
	
	if (colorMap)
	{
		XFreeColormap(display, colorMap);
	}
	
	if(display)
	{
		XCloseDisplay(display);
		display = NULL;
	}
	
	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); // file close keli with using files pointer
		gpFile = NULL; // bhanda(container) dhuvun kadhla use kelela
	}
}

