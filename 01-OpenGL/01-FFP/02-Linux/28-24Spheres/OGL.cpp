// standard header files
#include <stdio.h> // for standard io
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()
#include <math.h>

// X11 headers (X Server cha 11th Version)
#include <X11/Xlib.h> // X Client API
#include <X11/Xutil.h> // XVisualInfo
#include <X11/XKBlib.h> // for keyboard

// OpenGL header files
#include <GL/gl.h> // for OpenGL functionality
#include <GL/glu.h> 
#include <GL/glx.h> // for bridging api's

// macro definations
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variables
Display *display = NULL;
XVisualInfo *visualInfo = NULL; // it is now structure pointer (use arrow (->) for accessing members)
Colormap colorMap;
Window window;

FILE* gpFile = NULL;
Bool fullscreen = False;
Bool bActiveWindow = False;

// OpenGL related variables
GLXContext glxContext;

Bool gbLight = False;

GLfloat gbLightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat gbLightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat gbLightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat gbLightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f }; // Positional Light because of 1.0f
GLfloat gbLight_Model_Ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
GLfloat gbLight_Model_Local_Viewer[] = { 0.0f }; // this style is used in programmable pipeline giving value by array even for single value

GLfloat gbAngleForXRotation = 0.0f;
GLfloat gbAngleForYRotation = 0.0f;
GLfloat gbAngleForZRotation = 0.0f;

GLint gbKeyPressed = 0;

// variable to create sphere
GLUquadric* quadric = NULL;

// entry-point function
int main(void)
{
	// function declarations
	void toggleFullscreen(void);
	int initialize(void);
	void resize(int, int);
	void draw(void);
	void update(void);
	void uninitialize(void);
	
	// local variables declarations
	int defaultScreen;
	int defaultDepth;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom wm_delete_window_atom;
	XEvent event;
	KeySym keySym;
	int screenWidth;
	int screenHeight;
	char keys[26];
	static int winWidth;
	static int winHeight;
	int iRetVal = 0;

	static int frameBufferAttributes[] = // all are internal #define macros of int type
	{ 
	  GLX_DOUBLEBUFFER,
	  True,
	  GLX_RGBA, 
	  GLX_RED_SIZE,
	  8,
	  GLX_GREEN_SIZE,
	  8,
	  GLX_BLUE_SIZE,
	  8,
	  GLX_ALPHA_SIZE,
	  8,
	  GLX_DEPTH_SIZE,
	  24,
	  None // XLib style to end array inline initialization, instead of None we can also write 0. it is same
	};

	Bool bDone = False;

	// code
	
	gpFile = fopen("Log.txt", "w"); 
	
	if (gpFile == 0)
	{
		printf("fopen() Failed");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n");
	}
	
	// step 1 : open the display
	display = XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		printf("ERROR : XOpenDisplay() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 2 - get default screen from display
	defaultScreen = XDefaultScreen(display); // primary monitor milala
	
	// Step 3 : get default depth from display and default screen
	defaultDepth = XDefaultDepth(display, defaultScreen);
	
	// step 4 : 
	visualInfo = glXChooseVisual(display, defaultScreen, frameBufferAttributes); // similar ChoosePixelFormat() in Windows
	// ***bridging api frameBufferAttributes shi match honara visual de, pointer return karto glxChooseVisual
	
	if (visualInfo == NULL)
	{
		printf("ERROR : glxChooseVisual() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 5 : Fill & initialize struct XSetWindow Attributes and along with that also set colomap and event_mask
	memset(&windowAttributes, 0, sizeof(XSetWindowAttributes));
	
	windowAttributes.border_pixel = 0; // default color deto
	windowAttributes.background_pixel = XBlackPixel(display, defaultScreen); // hbrBackground -> GetStockObject 
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display, 
						     RootWindow(display, visualInfo -> screen), // true color wali visual ghetoy
						     visualInfo -> visual, 
						     AllocNone);
	windowAttributes.event_mask = ExposureMask | KeyPressMask | StructureNotifyMask | FocusChangeMask;// WM_PAINT | WM_KEYDOWN
	
	// Step 6 : Initialize global colorMap using colormap from windowAttributes 
	colorMap = windowAttributes.colormap;
	
	// step 7 : Initialize Window Styles
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;
	
	// step 8 : create the window using XCreateWindow() & do error checking
	window = XCreateWindow(display,
				RootWindow(display, visualInfo -> screen), 
				0, // X
				0, // Y
				WIN_WIDTH, // width
				WIN_HEIGHT, // height
				0, // border width
				visualInfo -> depth,
				InputOutput,
				visualInfo -> visual,
				styleMask,
				&windowAttributes
				);
	
	if(!window)
	{
		printf("ERROR : XCreateWindow() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 9 : give name to your window in it's title/caption bar
	XStoreName(display, window, "Mandar Dilip Makse RTR2021-116 - OpenGL Window");
	
	// step 10 : creating and setting window manager protocol / atom
	wm_delete_window_atom = XInternAtom(display, "WM_DELTE_WINDOW", True); // True means atom always create
	XSetWMProtocols(display, window, &wm_delete_window_atom, 1);
	
	// step 11 : Actually show the window by XMapWindow()
	XMapWindow(display, window);
	
	// centering of window
	screenWidth = XWidthOfScreen(XScreenOfDisplay(display, defaultScreen));
	screenHeight = XHeightOfScreen(XScreenOfDisplay(display, defaultScreen));
	XMoveWindow(display, window, ((screenWidth - WIN_WIDTH) / 2), ((screenHeight - WIN_HEIGHT)/ 2));

	initialize();
	
	if (iRetVal == -1)
	{
		fprintf(gpFile, "glxContext can not be obtained\n\n");
		uninitialize();
	}
	else
	{
		fprintf(gpFile, "Initialize() Successful\n\n");
	}
	
	 // step 12 : create the message loop
	 while(bDone == False) // 1) OpenGL chnage
	 {
		 while(XPending(display)) // 2) OPenGL change 2, XPending = Xlib | PeekMessage = Windows (Win32)
	 	{
	 		XNextEvent(display, &event); // a) getting next event by using XNextEvent() 
	 	
	 		switch (event.type)
	 		{
	 			case MapNotify: // WM_CREATE
	 				break;

				case FocusIn:
					bActiveWindow = True;
					break;

				case FocusOut:
					bActiveWindow = False;
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;

					resize(winWidth, winHeight);

					break;
	 			
	 			case KeyPress: // b) handling the keypress of escape
	 				keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0); // 3rd key will br symbol, 4th will be not companied by shift key | like WM_KEYDOWN
	 			
	 			switch(keySym)
	 			{
	 				case XK_Escape:
	 					bDone = True;
	 					break;
	 			}

				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL); // like WM_CHAR

				switch(keys[0])
				{
					case 'F':
					case 'f':
						if (fullscreen == False)
						{
							toggleFullscreen();
							fullscreen = True;
						}
						else
						{
							toggleFullscreen();
							fullscreen = False;
						}
						break;
						
						
					case 'L':
					case 'l':
						if (gbLight == False)
						{
							glEnable(GL_LIGHTING);
							gbLight = True;
						}
						else
						{
							glDisable(GL_LIGHTING);
							gbLight = False;
						}
						break;

					case 'X':
					case 'x':
						gbKeyPressed = 1;
						gbAngleForXRotation = 0.0f; // Reset
						break;

					case 'Y':
					case 'y':
						gbKeyPressed = 2;
						gbAngleForYRotation = 0.0f; // Reset
						break;

					case 'Z':
					case 'z':
						gbKeyPressed = 3;
						gbAngleForZRotation = 0.0f; // Reset
						break;

					default:
						gbKeyPressed = 0;
						break;
				}
				
	 			break;
	 			
	 			case 33: // c) handaling message 33, wm_delete_window_atom and 33 are analogous to each other
	 				bDone = True;
	 				break;
	 		}
	 	}
		
		if (bActiveWindow == True)
		{
			draw();
			
			update();
		}
	 }
	 
	 // step 13 - call uninitialize() and return()
	 uninitialize();
	
	return(0);
}

void toggleFullscreen(void)
{
	// local variables
	Atom wm_current_state_atom;
	Atom wm_fullscreen_state_atom;
	XEvent xevent;

	// code
	wm_current_state_atom = XInternAtom(display, "_NET_WM_STATE", False);
	wm_fullscreen_state_atom = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

	memset(&xevent, 0, sizeof(XEvent));

	xevent.type = ClientMessage;
	xevent.xclient.window = window;
	xevent.xclient.message_type = wm_current_state_atom;
	xevent.xclient.format = 32;
	xevent.xclient.data.l[0] = fullscreen ? 0 : 1;
	xevent.xclient.data.l[1] = wm_fullscreen_state_atom;

	XSendEvent(display, 
			   RootWindow(display, visualInfo -> screen),
			   False,
			   SubstructureNotifyMask,
			   &xevent);
}

int initialize(void)
{
	// function declarations
	void resize(int, int);

	// code
	glxContext = glXCreateContext(display, visualInfo, NULL, True); // same as making create context in windows // check recoording
	
	if (glxContext == 0)
		return(-1);

	// make current context
	glXMakeCurrent(display, window, glxContext);

	// Here starts OpenGL functions
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT0, GL_AMBIENT, gbLightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, gbLightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, gbLightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, gbLightPosition);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gbLight_Model_Ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, gbLight_Model_Local_Viewer);

	glEnable(GL_LIGHT0); // by deafult LIGHT0 enable asto pan explicitly use kartoy tar enable karne GL_LIGHT0

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);\

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Create Quadric
	quadric = gluNewQuadric();

	// warm up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	
	return(0);
}

void resize(int width, int height)
{
	// code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();

	gluPerspective(45.0f, 
				 (GLfloat)width 
		         / (GLfloat)height,
				 0.1f,
		         100.0f); 
	// height should not be 0
}

void draw(void)
{
	// function declarations
	void draw24Spheres(void);

	// code

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	if (gbKeyPressed == 1)
	{
		// X-Axis Rotation
		glRotatef(gbAngleForXRotation, 1.0f, 0.0f, 0.0f);
		gbLightPosition[2] = gbAngleForXRotation;
	}
	else if (gbKeyPressed == 2)
	{
		glRotatef(gbAngleForYRotation, 0.0f, 1.0f, 0.0f);
		gbLightPosition[0] = gbAngleForYRotation;
	}
	else if (gbKeyPressed == 3)
	{
		glRotatef(gbAngleForZRotation, 0.0f, 0.0f, 1.0f);
		gbLightPosition[1] = gbAngleForZRotation;
	}
	else
	{
		gbLightPosition[0] = 0.0f;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, gbLightPosition);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	draw24Spheres();
	
	glXSwapBuffers(display, window);
}

void update(void)
{
	// code
	if (gbKeyPressed == 1)
	{
		gbAngleForXRotation = gbAngleForXRotation + cos(1.0f);

		if (gbAngleForXRotation >= 360.0f)
		{
			gbAngleForXRotation = gbAngleForXRotation - cos(360.0f);
		}
	}

	if (gbKeyPressed == 2)
	{
		gbAngleForYRotation = gbAngleForYRotation + cos(1.0f);

		if (gbAngleForYRotation >= 360.0f)
		{
			gbAngleForYRotation = gbAngleForYRotation - cos(360.0f);
		}
	}

	if (gbKeyPressed == 3)
	{
		gbAngleForZRotation = gbAngleForZRotation + cos(1.0f);

		if (gbAngleForZRotation >= 360.0f)
		{
			gbAngleForZRotation = gbAngleForZRotation - cos(360.0f);
		}
	}
}

void draw24Spheres(void)
{
	// variable declarations
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess;

	// code

	// ***** 1st sphere on 1st column, emerald *****
	// ambient material
	materialAmbient[0] = 0.0215f; // r
	materialAmbient[1] = 0.1745f; // g
	materialAmbient[2] = 0.0215f; // b
	materialAmbient[3] = 1.0f;   // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.07568f; // r
	materialDiffuse[1] = 0.61424f; // g
	materialDiffuse[2] = 0.07568f; // b
	materialDiffuse[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.633f;    // r
	materialSpecular[1] = 0.727811f; // g
	materialSpecular[2] = 0.633f;    // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.6 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-10.0f, 7.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);
	

	// ***** 2nd sphere on 1st column, jade *****
	// ambient material
	materialAmbient[0] = 0.135f;  // r
	materialAmbient[1] = 0.2225f; // g
	materialAmbient[2] = 0.1575f; // b
	materialAmbient[3] = 1.0f;   // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.54f; // r
	materialDiffuse[1] = 0.89f; // g
	materialDiffuse[2] = 0.63f; // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.316228f; // r
	materialSpecular[1] = 0.316228f; // g
	materialSpecular[2] = 0.316228f; // b
	materialSpecular[3] = 1.0f;      // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.1 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-10.0f, 4.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 3rd sphere on 1st column, obsidian *****
	// ambient material
	materialAmbient[0] = 0.05375f; // r
	materialAmbient[1] = 0.05f;    // g
	materialAmbient[2] = 0.06625f; // b
	materialAmbient[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.18275f; // r
	materialDiffuse[1] = 0.17f;    // g
	materialDiffuse[2] = 0.22525f; // b
	materialDiffuse[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.332741f; // r
	materialSpecular[1] = 0.328634f; // g
	materialSpecular[2] = 0.346435f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.3 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-10.0f, 1.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 4th sphere on 1st column, pearl *****
	// ambient material
	materialAmbient[0] = 0.25f;    // r
	materialAmbient[1] = 0.20725f; // g
	materialAmbient[2] = 0.20725f; // b
	materialAmbient[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 1.0f;   // r
	materialDiffuse[1] = 0.829f; // g
	materialDiffuse[2] = 0.829f; // b
	materialDiffuse[3] = 1.0f;  // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.296648f; // r
	materialSpecular[1] = 0.296648f; // g
	materialSpecular[2] = 0.296648f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.088 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	 
	glTranslatef(-10.0f, -2.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 5th sphere on 1st column, ruby *****
	// ambient material
	materialAmbient[0] = 0.1745f;  // r
	materialAmbient[1] = 0.01175f; // g
	materialAmbient[2] = 0.01175f; // b
	materialAmbient[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.61424f; // r
	materialDiffuse[1] = 0.04136f; // g
	materialDiffuse[2] = 0.04136f; // b
	materialDiffuse[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.727811f; // r
	materialSpecular[1] = 0.626959f; // g
	materialSpecular[2] = 0.626959f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.6 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-10.0f, -5.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 6th sphere on 1st column, turquoise *****
	// ambient material
	materialAmbient[0] = 0.1f;     // r
	materialAmbient[1] = 0.18725f; // g
	materialAmbient[2] = 0.1745f;  // b
	materialAmbient[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.396f;   // r
	materialDiffuse[1] = 0.74151f; // g
	materialDiffuse[2] = 0.69102f; // b
	materialDiffuse[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.297254f; // r
	materialSpecular[1] = 0.30829f;  // g
	materialSpecular[2] = 0.306678f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.1 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-10.0f, -8.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 1st sphere on 2nd column, brass *****
	// ambient material
	materialAmbient[0] = 0.329412f; // r
	materialAmbient[1] = 0.223529f; // g
	materialAmbient[2] = 0.027451f; // b
	materialAmbient[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.780392f; // r
	materialDiffuse[1] = 0.568627f; // g
	materialDiffuse[2] = 0.113725f; // b
	materialDiffuse[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.992157f; // r
	materialSpecular[1] = 0.941176f; // g
	materialSpecular[2] = 0.807843f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.21794872 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-3.5f, 7.0f, -23.0f);

	gluSphere(quadric, 1.0f, 30, 30);
	
	// ***** 2nd sphere on 2nd column, bronze *****
	// ambient material
	materialAmbient[0] = 0.2125f; // r
	materialAmbient[1] = 0.1275f; // g
	materialAmbient[2] = 0.054f;  // b
	materialAmbient[3] = 1.0f;   // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.714f;   // r
	materialDiffuse[1] = 0.4284f;  // g
	materialDiffuse[2] = 0.18144f; // b
	materialDiffuse[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.393548f; // r
	materialSpecular[1] = 0.271906f; // g
	materialSpecular[2] = 0.166721f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.2 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-3.5f, 4.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 3rd sphere on 2nd column, chrome *****
	// ambient material
	materialAmbient[0] = 0.25f; // r
	materialAmbient[1] = 0.25f; // g
	materialAmbient[2] = 0.25f; // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.4f;  // r
	materialDiffuse[1] = 0.4f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.774597f; // r
	materialSpecular[1] = 0.774597f; // g
	materialSpecular[2] = 0.774597f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.6 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-3.5f, 1.0f, -23.0f);

	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 4th sphere on 2nd column, copper *****
	// ambient material
	materialAmbient[0] = 0.19125f; // r
	materialAmbient[1] = 0.0735f;  // g
	materialAmbient[2] = 0.0225f;  // b
	materialAmbient[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.7038f;  // r
	materialDiffuse[1] = 0.27048f; // g
	materialDiffuse[2] = 0.0828f;  // b
	materialDiffuse[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.256777f; // r
	materialSpecular[1] = 0.137622f; // g
	materialSpecular[2] = 0.086014f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.1 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-3.5f, -2.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 5th sphere on 2nd column, gold *****
	// ambient material
	materialAmbient[0] = 0.24725f; // r
	materialAmbient[1] = 0.1995f;  // g
	materialAmbient[2] = 0.0745f;  // b
	materialAmbient[3] =  1.0f;    // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.75164f; // r
	materialDiffuse[1] = 0.60648f; // g
	materialDiffuse[2] = 0.22648f; // b
	materialDiffuse[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.628281f; // r;
	materialSpecular[1] = 0.555802f; // g
	materialSpecular[2] = 0.366065f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.4 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-3.5f, -5.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 6th sphere on 2nd column, silver *****
	// ambient material
	materialAmbient[0] = 0.19225f; // r
	materialAmbient[1] = 0.19225f; // g
	materialAmbient[2] = 0.19225f; // b
	materialAmbient[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.50754f; // r
	materialDiffuse[1] = 0.50754f; // g
	materialDiffuse[2] = 0.50754f; // b
	materialDiffuse[3] = 1.0f;    // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.508273f; // r
	materialSpecular[1] = 0.508273f; // g
	materialSpecular[2] = 0.508273f; // b
	materialSpecular[3] = 1.0f;     // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.4 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(-3.5f, -8.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 1st sphere on 3rd column, black *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.01f; // r
	materialDiffuse[1] = 0.01f; // g
	materialDiffuse[2] = 0.01f; // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.50f; // r
	materialSpecular[1] = 0.50f; // g
	materialSpecular[2] = 0.50f; // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.25 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(3.5f, 7.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 2nd sphere on 3rd column, cyan *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.1f;  // g
	materialAmbient[2] = 0.06f; // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.0f;        // r
	materialDiffuse[1] = 0.50980392f; // g
	materialDiffuse[2] = 0.50980392f; // b
	materialDiffuse[3] = 1.0f;       // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.50196078f; // r
	materialSpecular[1] = 0.50196078f; // g
	materialSpecular[2] = 0.50196078f; // b
	materialSpecular[3] = 1.0f;       // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.25 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	
	glTranslatef(3.5f, 4.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 3rd sphere on 2nd column, green *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.1f;  // r
	materialDiffuse[1] = 0.35f; // g
	materialDiffuse[2] = 0.1f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.45f; // r
	materialSpecular[1] = 0.55f; // g
	materialSpecular[2] = 0.45f; // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.25 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(3.5f, 1.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 4th sphere on 3rd column, red *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.0f;  // g
	materialDiffuse[2] = 0.0f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.6f;  // g
	materialSpecular[2] = 0.6f;  // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.25 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(3.5f, -2.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 5th sphere on 3rd column, white *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.55f; // r
	materialDiffuse[1] = 0.55f; // g
	materialDiffuse[2] = 0.55f; // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.70f; // r
	materialSpecular[1] = 0.70f; // g
	materialSpecular[2] = 0.70f; // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.25 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(3.5f, -5.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 6th sphere on 3rd column, yellow plastic *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.0f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.60f; // r
	materialSpecular[1] = 0.60f; // g
	materialSpecular[2] = 0.50f; // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.25 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(3.5f, -8.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 1st sphere on 4th column, black *****
	// ambient material
	materialAmbient[0] = 0.02f; // r
	materialAmbient[1] = 0.02f; // g
	materialAmbient[2] = 0.02f; // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.01f; // r
	materialDiffuse[1] = 0.01f; // g
	materialDiffuse[2] = 0.01f; // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.4f;  // r
	materialSpecular[1] = 0.4f;  // g
	materialSpecular[2] = 0.4f;  // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.078125 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(10.0f, 7.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 2nd sphere on 4th column, cyan *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.05f; // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.4f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.5f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.04f; // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.7f;  // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.078125 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(10.0f, 4.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);

	// ***** 3rd sphere on 4th column, green *****
	// ambient material
	materialAmbient[0] = 0.0f;  // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.4f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.04f; // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.04f; // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.078125 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(10.0f, 1.0f, -23.0f);

	gluSphere(quadric, 1.0f, 30, 30);
	
	// ***** 4th sphere on 4th column, red *****
	// ambient material
	materialAmbient[0] = 0.05f; // r
	materialAmbient[1] = 0.0f;  // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.4f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.04f; // g
	materialSpecular[2] = 0.04f; // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.078125 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(10.0f, -2.0f, -23.0f);
	
	gluSphere(quadric, 1.0f, 30, 30);
	
	// ***** 5th sphere on 4th column, white *****
	// ambient material
	materialAmbient[0] = 0.05f; // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.05f; // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.5f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.7f;  // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.078125 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(10.0f, -5.0f, -23.0f);

	gluSphere(quadric, 1.0f, 30, 30);
	
	// ***** 6th sphere on 4th column, yellow rubber *****
	// ambient material
	materialAmbient[0] = 0.05f; // r
	materialAmbient[1] = 0.05f; // g
	materialAmbient[2] = 0.0f;  // b
	materialAmbient[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5f;  // r
	materialDiffuse[1] = 0.5f;  // g
	materialDiffuse[2] = 0.4f;  // b
	materialDiffuse[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7f;  // r
	materialSpecular[1] = 0.7f;  // g
	materialSpecular[2] = 0.04f; // b
	materialSpecular[3] = 1.0f; // a

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = float(0.078125 * 128);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(10.0f, -8.0f, -23.0f);

	gluSphere(quadric, 1.0f, 30, 30);
}


void uninitialize(void)
{

	// function declarations
	void ToggleFullScreen(void);

	// variable declarations
	GLXContext currentContext;

	// code
	currentContext = glXGetCurrentContext();

	if (currentContext && currentContext == glxContext)
	{
		glXMakeCurrent(display, 0, 0);
	}
	
	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (glxContext)
	{
		glXDestroyContext(display, glxContext);
		glxContext = NULL;
	}

	if (visualInfo)
	{
		free(visualInfo);
		visualInfo = NULL;
	}

	if (fullscreen)
	{
		toggleFullscreen();
		fullscreen = False;
	}

	if (window)
	{
		XDestroyWindow(display, window);
	}
	
	if (colorMap)
	{
		XFreeColormap(display, colorMap);
	}
	
	if(display)
	{
		XCloseDisplay(display);
		display = NULL;
	}
	
	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); // file close keli with using files pointer
		gpFile = NULL; // bhanda(container) dhuvun kadhla use kelela
	}
}

