// standard header files
#include <stdio.h> // for standard io
#include <stdlib.h> // for exit()
#include <memory.h> // for memset()

// X11 headers (X Server cha 11th Version)
#include <X11/Xlib.h> // X Client API
#include <X11/Xutil.h> // XVisualInfo
#include <X11/XKBlib.h> // for keyboard

// OpenGL header files
#include <GL/gl.h> // for OpenGL functionality
#include <GL/glu.h> 
#include <GL/glx.h> // for bridging api's

// macro definations
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variables
Display *display = NULL;
XVisualInfo *visualInfo = NULL; // it is now structure pointer (use arrow (->) for accessing members)
Colormap colorMap;
Window window;

FILE* gpFile = NULL;
Bool fullscreen = False;
Bool bActiveWindow = False;

// OpenGL related variables
GLXContext glxContext;

// shoulder and elbow variables
int shoulder = 0;
int elbow = 0;
int palm = 0;

// variable to create sphere
GLUquadric* quadric = NULL;

// entry-point function
int main(void)
{
	// function declarations
	void toggleFullscreen(void);
	int initialize(void);
	void resize(int, int);
	void draw(void);
	void update(void);
	void uninitialize(void);
	
	// local variables declarations
	int defaultScreen;
	int defaultDepth;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom wm_delete_window_atom;
	XEvent event;
	KeySym keySym;
	int screenWidth;
	int screenHeight;
	char keys[26];
	static int winWidth;
	static int winHeight;
	int iRetVal = 0;

	static int frameBufferAttributes[] = // all are internal #define macros of int type
	{ 
	  GLX_DOUBLEBUFFER,
	  True,
	  GLX_RGBA, 
	  GLX_RED_SIZE,
	  8,
	  GLX_GREEN_SIZE,
	  8,
	  GLX_BLUE_SIZE,
	  8,
	  GLX_ALPHA_SIZE,
	  8,
	  GLX_DEPTH_SIZE,
	  24,
	  None // XLib style to end array inline initialization, instead of None we can also write 0. it is same
	};

	Bool bDone = False;

	// code
	
	gpFile = fopen("Log.txt", "w"); 
	
	if (gpFile == 0)
	{
		printf("fopen() Failed");
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n");
	}
	
	// step 1 : open the display
	display = XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		printf("ERROR : XOpenDisplay() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 2 - get default screen from display
	defaultScreen = XDefaultScreen(display); // primary monitor milala
	
	// Step 3 : get default depth from display and default screen
	defaultDepth = XDefaultDepth(display, defaultScreen);
	
	// step 4 : 
	visualInfo = glXChooseVisual(display, defaultScreen, frameBufferAttributes); // similar ChoosePixelFormat() in Windows
	// ***bridging api frameBufferAttributes shi match honara visual de, pointer return karto glxChooseVisual
	
	if (visualInfo == NULL)
	{
		printf("ERROR : glxChooseVisual() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 5 : Fill & initialize struct XSetWindow Attributes and along with that also set colomap and event_mask
	memset(&windowAttributes, 0, sizeof(XSetWindowAttributes));
	
	windowAttributes.border_pixel = 0; // default color deto
	windowAttributes.background_pixel = XBlackPixel(display, defaultScreen); // hbrBackground -> GetStockObject 
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display, 
						     RootWindow(display, visualInfo -> screen), // true color wali visual ghetoy
						     visualInfo -> visual, 
						     AllocNone);
	windowAttributes.event_mask = ExposureMask | KeyPressMask | StructureNotifyMask | FocusChangeMask;// WM_PAINT | WM_KEYDOWN
	
	// Step 6 : Initialize global colorMap using colormap from windowAttributes 
	colorMap = windowAttributes.colormap;
	
	// step 7 : Initialize Window Styles
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;
	
	// step 8 : create the window using XCreateWindow() & do error checking
	window = XCreateWindow(display,
				RootWindow(display, visualInfo -> screen), 
				0, // X
				0, // Y
				WIN_WIDTH, // width
				WIN_HEIGHT, // height
				0, // border width
				visualInfo -> depth,
				InputOutput,
				visualInfo -> visual,
				styleMask,
				&windowAttributes
				);
	
	if(!window)
	{
		printf("ERROR : XCreateWindow() Failed\n");
		uninitialize();
		exit(1);
	}
	
	// step 9 : give name to your window in it's title/caption bar
	XStoreName(display, window, "Mandar Dilip Makse RTR2021-116 - OpenGL Window");
	
	// step 10 : creating and setting window manager protocol / atom
	wm_delete_window_atom = XInternAtom(display, "WM_DELTE_WINDOW", True); // True means atom always create
	XSetWMProtocols(display, window, &wm_delete_window_atom, 1);
	
	// step 11 : Actually show the window by XMapWindow()
	XMapWindow(display, window);
	
	// centering of window
	screenWidth = XWidthOfScreen(XScreenOfDisplay(display, defaultScreen));
	screenHeight = XHeightOfScreen(XScreenOfDisplay(display, defaultScreen));
	XMoveWindow(display, window, ((screenWidth - WIN_WIDTH) / 2), ((screenHeight - WIN_HEIGHT)/ 2));

	initialize();
	
	if (iRetVal == -1)
	{
		fprintf(gpFile, "glxContext can not be obtained\n\n");
		uninitialize();
	}
	else
	{
		fprintf(gpFile, "Initialize() Successful\n\n");
	}
	
	 // step 12 : create the message loop
	 while(bDone == False) // 1) OpenGL chnage
	 {
		 while(XPending(display)) // 2) OPenGL change 2, XPending = Xlib | PeekMessage = Windows (Win32)
	 	{
	 		XNextEvent(display, &event); // a) getting next event by using XNextEvent() 
	 	
	 		switch (event.type)
	 		{
	 			case MapNotify: // WM_CREATE
	 				break;

				case FocusIn:
					bActiveWindow = True;
					break;

				case FocusOut:
					bActiveWindow = False;
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;

					resize(winWidth, winHeight);

					break;
	 			
	 			case KeyPress: // b) handling the keypress of escape
	 				keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0); // 3rd key will br symbol, 4th will be not companied by shift key | like WM_KEYDOWN
	 			
	 			switch(keySym)
	 			{
	 				case XK_Escape:
	 					bDone = True;
	 					break;
	 			}

				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL); // like WM_CHAR

				switch(keys[0])
				{
					case 'F':
					case 'f':
						if (fullscreen == False)
						{
							toggleFullscreen();
							fullscreen = True;
						}
						else
						{
							toggleFullscreen();
							fullscreen = False;
						}
						break;
						
					case 'S':
						shoulder = (shoulder + 3) % 360; // modulus because we are saving remainder
						break;

					case 's':
						shoulder = (shoulder - 3) % 360; // modulus because we are saving remainder
						break;

					case 'E':
						elbow = (elbow + 3) % 360; // modulus because we are saving remainder
						break;

					case 'e':
						elbow = (elbow - 3) % 360; // modulus because we are saving remainder
						break;

					case 'P':
						palm = (palm + 1) % 360;
						break;

					case 'p':
						palm = (palm - 1) % 360;
						break;
					
					default:
						break;
				}
				
	 			break;
	 			
	 			case 33: // c) handaling message 33, wm_delete_window_atom and 33 are analogous to each other
	 				bDone = True;
	 				break;
	 		}
	 	}
		
		if (bActiveWindow == True)
		{
			draw();
			
			update();
		}
	 }
	 
	 // step 13 - call uninitialize() and return()
	 uninitialize();
	
	return(0);
}

void toggleFullscreen(void)
{
	// local variables
	Atom wm_current_state_atom;
	Atom wm_fullscreen_state_atom;
	XEvent xevent;

	// code
	wm_current_state_atom = XInternAtom(display, "_NET_WM_STATE", False);
	wm_fullscreen_state_atom = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

	memset(&xevent, 0, sizeof(XEvent));

	xevent.type = ClientMessage;
	xevent.xclient.window = window;
	xevent.xclient.message_type = wm_current_state_atom;
	xevent.xclient.format = 32;
	xevent.xclient.data.l[0] = fullscreen ? 0 : 1;
	xevent.xclient.data.l[1] = wm_fullscreen_state_atom;

	XSendEvent(display, 
			   RootWindow(display, visualInfo -> screen),
			   False,
			   SubstructureNotifyMask,
			   &xevent);
}

int initialize(void)
{
	// code
	glxContext = glXCreateContext(display, visualInfo, NULL, True); // same as making create context in windows // check recoording
	
	if (glxContext == 0)
		return(-1);

	// make current context
	glXMakeCurrent(display, window, glxContext);

	// Here starts OpenGL functions
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); 

	// Create Quadric
	quadric = gluNewQuadric();

	
	return(0);
}

void resize(int width, int height)
{
	// code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();

	gluPerspective(45.0f, 
				 (GLfloat)width 
		         / (GLfloat)height,
				 0.1f,
		         100.0f); 
	// height should not be 0
}

void draw(void)
{
	// code

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glPushMatrix(); // 0

	glRotatef((GLfloat)shoulder, 0.0f,  0.0f, 1.0f);
	glTranslatef(1.0f, 0.0f, 0.0f);

	glPushMatrix(); // 1

	glScalef(2.0f, 0.5f, 1.0f);

	// Now Draw The Arm

	glColor3f(0.5f, 0.35f, 0.05f);

	gluSphere(quadric, 0.5f, 10, 10);

	glPopMatrix(); //  1

	glTranslatef(1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)elbow, 0.0f, 0.0f, 1.0f);
	glTranslatef(1.0f, 0.0f, 0.0f);

	glPushMatrix(); // 3

	glScalef(2.0f, 0.5f, 1.0f);
	
	// Draw the forearm

	glColor3f(0.5f, 0.35f, 0.05f);

	gluSphere(quadric, 0.5f, 10, 10);

	glPopMatrix(); // 3

	glTranslatef(1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)palm, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.35f, 0.0f, 0.0f);

	glPushMatrix(); // 4.35

	glScalef(0.5f, 0.5, 1.0f);

	// Draw Palm

	glColor3f(0.5f, 0.35f, 0.05f);

	gluSphere(quadric, 0.5f, 10, 10);

	glPopMatrix(); // 4.35
	glPopMatrix(); // 3
	glPopMatrix(); // 1 
	glPopMatrix(); // 0
	
	glXSwapBuffers(display, window);
}

void update(void)
{
	// code
}

void uninitialize(void)
{

	// function declarations
	void ToggleFullScreen(void);

	// variable declarations
	GLXContext currentContext;

	// code
	currentContext = glXGetCurrentContext();

	if (currentContext && currentContext == glxContext)
	{
		glXMakeCurrent(display, 0, 0);
	}
	
	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (glxContext)
	{
		glXDestroyContext(display, glxContext);
		glxContext = NULL;
	}

	if (visualInfo)
	{
		free(visualInfo);
		visualInfo = NULL;
	}

	if (fullscreen)
	{
		toggleFullscreen();
		fullscreen = False;
	}

	if (window)
	{
		XDestroyWindow(display, window);
	}
	
	if (colorMap)
	{
		XFreeColormap(display, colorMap);
	}
	
	if(display)
	{
		XCloseDisplay(display);
		display = NULL;
	}
	
	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); // file close keli with using files pointer
		gpFile = NULL; // bhanda(container) dhuvun kadhla use kelela
	}
}

