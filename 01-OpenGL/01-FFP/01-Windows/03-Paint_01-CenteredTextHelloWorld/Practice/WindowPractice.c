// header file
#include<Windows.h>
#include"Window.h"

// macro definations
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass; // wndclass is our window class far Window that we will be showing on screen
	HWND hwnd; // to create unique hwnd for our Window
	MSG msg; // To take message msg variable 
	TCHAR szAppName[] = TEXT("MyPracticeWindow"); // Our Window Class Name 

	// initializing members of WNDCLASSEX wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Registering Our WNDCLASEX wndclass structure
	RegisterClassEx(&wndclass);

	// creating window in memory
	hwnd = CreateWindow(szAppName,
						TEXT("Practice Window"),
						WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME,
						((GetSystemMetrics(SM_CXSCREEN) - WIN_WIDTH) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - WIN_HEIGHT) / 2),
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL, // HWND_DESKTOP
						NULL, // Menu cha handle
						hInstance,
						NULL); // creation parameter, it is used to pass special values to WndProc

	// Showing Window On Screen
	ShowWindow(hwnd, iCmdShow); // iCmdShow -> SW_SHOWNORMAL / SW_NORMAL, Other Values SW_HIDE, SW_MINIMIZE, SW_MAXIMIZE

	// Update Window when window repaints
	UpdateWindow(hwnd); // Show Window Internally Calls UpdateWindow

	// Heart Of Program - Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// variable declarations
	PAINTSTRUCT ps;
	static RECT rc; 
	HDC hdc;
	TCHAR str[] = TEXT("I Love My India");
	static int iColorFlag = 0;

	// code
	switch (iMsg)
	{
	case WM_KEYDOWN:

		switch (wParam)
		{
		case 27:
			DestroyWindow(hwnd);
			break;

		default:
			break;
		}
	break;
		
	case WM_CHAR:

	switch (wParam)
	{
	case 'O':
	case 'o':
		iColorFlag = 1;
		InvalidateRect(hwnd, NULL, TRUE);
		break;

	case 'B':
	case 'b':
		iColorFlag = 2;
		InvalidateRect(hwnd, NULL, TRUE);
		break;

	case 'G':
	case 'g':
		iColorFlag = 3;
		InvalidateRect(hwnd, NULL, TRUE);
		break;

	default:
		iColorFlag = 0;
		InvalidateRect(hwnd, NULL, TRUE);
		break;
	}
	break;

	case WM_PAINT:
		GetClientRect(hwnd, &rc);
		hdc = BeginPaint(hwnd, &ps);
		SetBkColor(hdc, RGB(0, 0, 0));

		if (iColorFlag == 1)
			SetTextColor(hdc, RGB(255, 127, 0));
		else if (iColorFlag == 2)
			SetTextColor(hdc, RGB(0, 0, 255));
		else if (iColorFlag == 3)
			SetTextColor(hdc, RGB(0, 255, 0));	
		else
		SetTextColor(hdc, RGB(255, 255, 255));

		DrawText(hdc, str, -1, &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		EndPaint(hwnd, &ps);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


