// header file
#include <Windows.h>
#include <stdio.h> // For File I/O (fopen(), fclose(), fprintf())
#include <stdlib.h> // For exit()
#include "OGL.h" // our header file
#include <math.h>

// OpenGL header files
#include <GL/gl.h>
#include <GL/glu.h> 

// macro definatio
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define TOP_BOTTOM_RIGHT_X 1.5f
#define TOP_BOTTOM_LEFT_X -1.5f
#define TOP_Y 1.0f
#define BOTTOM_Y -1.0f

// OpenGL libraries
#pragma comment(lib, "OpenGL32.lib") // can also use for  user32.lib, gdi32.lib, kernel32.lib, if given here then no need write in link command
#pragma comment(lib, "glu32.lib") 

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void outerRect(void);
void innerTiltRect(void);
void crossLines(void);

// global variable declarations
BOOL gbActiveWindow = FALSE; // by default aapli window active nahi ahe
FILE* gpFile = NULL;
BOOL gbFullScreen = FALSE;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL; // H - HANDLE, GL - OpenGL, RC - Rendering Context

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations -> not important in sequence as prototype but it is defined in which one gets called first
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iretVal = 0;

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) // if(file open secured() != 0)
	// aadhi fopen hota nusta
	{
		MessageBox(NULL, TEXT("Creation Of Log File Failed. Exitting..."), TEXT("File I/O Error"), MB_OK); // 1st parameter is NULL because we dont have our hwnd yet, so OS la ticha handle magitla message dakhvnyasathi ani aapan NULL/HWND_DESKTOP dila
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n"); // printf prints on console. fprintf prints in file
	}

	// initilizing WNDCLASSEX wndclass structure members
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // change
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); 
	wndclass.hInstance = hInstance;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Registering wndclass in OS's Class Registry
	RegisterClassEx(&wndclass);

	// creating our window in memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("OpenGL Window - Mandar Maske RTR2021-116"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // change
						((GetSystemMetrics(SM_CXSCREEN) - WIN_WIDTH) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - WIN_HEIGHT) / 2),
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	if (hwnd == NULL)
	{
		fprintf(gpFile, "hwnd Failed !!!\n\n");
	}
	else
	{
		fprintf(gpFile, "hwnd Created Successfully\n\n");
	}

	ghwnd = hwnd;

	// initialize
	iretVal = initialize();

	if (iretVal == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n\n");
		uninitialize();
	}
	else if (iretVal == -2)
	{
		fprintf(gpFile, "SetPixelFormat is Failed\n\n");
		uninitialize();
	}
	else if (iretVal == -3)
	{
		fprintf(gpFile, "CreateOpenGlContext is Failed\n\n");
		uninitialize();
	}

	else if (iretVal == -4)
	{
		fprintf(gpFile, "Making OpenGL Context As Current Context Failed\n\n");
		uninitialize();
	}
	else
	{
		fprintf(gpFile, "Initialize() Successful\n\n");
	}

	// Showing Window On Screen
	ShowWindow(hwnd, iCmdShow);

	// foregrounding(Z Order - Top) and focusing the window
	SetForegroundWindow(hwnd);

	SetFocus(hwnd); // sends message to our Window(wndproc) WM_SETFOCUS

	// Game Loop -> glutMainLoop() in glut
	while (bDone == FALSE) //  while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) // manually loop false kela
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else // Kontahi message nasel tevha ha else block run hoil
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}

	uninitialize(); // indra and takshaq saap

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void resize(int, int);

	// variable declarations
	int retVal;

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:

		switch (wParam)
		{
		case 27:
			retVal = MessageBox(hwnd, TEXT("Are You Sure, You Want To Exit ?"), TEXT("Exit Message"), MB_YESNO);
			if (retVal == IDNO)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), No Button Is Pressed\n\n");
				break;
			}
				
			else if(retVal == IDYES)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), Yes Button Is Pressed. Program Is Now Exitting !!!\n\n");
				DestroyWindow(hwnd);
				break;
			}

		default:
			break;
		}
	break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		default:
			break;
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // resize(width, height)
		break;

	case WM_CLOSE: // close button - discipline mhanun lihane, WM_CLOSE cha call WM_DESTROY chya aadhi yeto
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle; // static to sustain values accross this function
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT); // like wndclass.cbSize =  sizeof(WNDCLASSEX)

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) // if (1 & WS_OVERLAPPEDWINDOW) -> if (1 & 1) then if ya madhe
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) // if(1 && 1) then in if
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW); // WS_OVERLAPPEDWINDOW che 5 style(WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_THICKFRAME,  WS_SYSMENU, WS_CAPTION) nako except WS_OVERLAPPED 
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW); // kadhayla ~WS_OVERLAPPED kela, Add karnyasathi Bitwise Or Waparala
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED); // x, y, width, height ignore -> Zero Why? - wp ne already SetWindowPlacement pahije tithe aaplya window la place kelay so 0 

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	
	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialization of PIXELFORMATDESCRIPTOR structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	// GetDC
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	// set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return(-2);

	// Create OpenGL Rendering Context - first bridging API
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return(-3);

	// make the rendering context as the current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE) // wglMakeCurrent() - 2nd bridging API
		return(-4);

	// Here Starts OpenGL Code...
	// clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // 4th parameter alpha color

	// warm up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// code

	// kitihi height kami keli ki height 0 nasayla havi kamit kami 1 havi
	// To Avoid divided by 0(illegal instruction / code) in future code
	if (height == 0) 
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION); // atta aamhi projection paahnaar aahot
	glLoadIdentity();

	// if else block removed -> change 3

	gluPerspective(45.0f, 
				 (GLfloat)width 
		         / (GLfloat)height,
				 0.1f,
		         100.0f); 
	// height should not be 0
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);

	outerRect();
	innerTiltRect();
	crossLines();
	
	SwapBuffers(ghdc); // native 
}

void update(void)
{
	// code
}

void outerRect(void)
{
	glBegin(GL_LINE_LOOP);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(TOP_BOTTOM_RIGHT_X, TOP_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_LEFT_X, TOP_Y, 0.0f);

	glVertex3f(TOP_BOTTOM_LEFT_X, TOP_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_LEFT_X, BOTTOM_Y, 0.0f);

	glVertex3f(TOP_BOTTOM_LEFT_X, BOTTOM_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_RIGHT_X, BOTTOM_Y, 0.0f);

	glVertex3f(TOP_BOTTOM_RIGHT_X, BOTTOM_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_RIGHT_X, TOP_Y, 0.0f);

	glEnd();
}

void innerTiltRect(void)
{
	glBegin(GL_LINE_LOOP);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, TOP_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_LEFT_X, 0.0f, 0.0f);

	glVertex3f(TOP_BOTTOM_LEFT_X, 0.0f, 0.0f);
	glVertex3f(0.0f, BOTTOM_Y, 0.0f);

	glVertex3f(0.0f, BOTTOM_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_RIGHT_X, 0.0f, 0.0f);

	glVertex3f(TOP_BOTTOM_RIGHT_X, 0.0f, 0.0f);
	glVertex3f(0.0f, TOP_Y, 0.0f);

	glEnd();
}

void crossLines(void)
{
	glBegin(GL_LINE_LOOP);

	glColor3f(1.0f, 0.5f, 0.0f);

	glVertex3f(TOP_BOTTOM_LEFT_X, TOP_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_RIGHT_X, BOTTOM_Y, 0.0f);

	glVertex3f(TOP_BOTTOM_RIGHT_X, TOP_Y, 0.0f);
	glVertex3f(TOP_BOTTOM_LEFT_X, BOTTOM_Y, 0.0f);

	glEnd();
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code

	// FullScreen astana escape press karun close karaych try kela tar window orginal state 
	// madhye yeun tyacha sukhad mrutyu hoil aapla window application 
	if (gbFullScreen)
	{
		ToggleFullScreen(); // important so taken in curly brace
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd) // ghwnd ajun asel tar DestroyWindow() la call karun ghwnd destroy kar
	{
		DestroyWindow(ghwnd); 	
		ghwnd = NULL;
	}
	
	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); // file close keli with using files pointer
		gpFile = NULL; // bhanda(container) dhuvun kadhla use kelela
	}
}

