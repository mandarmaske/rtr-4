// header files
#include<windows.h>

// macros definations
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations / prototype / signature
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int winWidth;
int winHeight;

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	RECT rect;

	// code

	// Initialization Of WNDCLASSEX Structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Registering WNDCLASSEX
	RegisterClassEx(&wndclass);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &rect, 0);

	// Create The Window
	hwnd = CreateWindow(szAppName,
						TEXT("Mandar Dilip Maske RTR2021-116"),
						WS_OVERLAPPEDWINDOW,
						(rect.right - WIN_WIDTH) / 2,
						(rect.bottom - WIN_HEIGHT) / 2,
						800,
						600,
						NULL,
						NULL,
						hInstance,
						NULL);

	// Show The Window
	ShowWindow(hwnd, iCmdShow);

	// Update The Window
	UpdateWindow(hwnd);

	// message loop - heart of the program
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

// CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// code
	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

