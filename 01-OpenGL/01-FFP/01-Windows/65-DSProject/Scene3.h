#pragma once
#include <GL/gl.h>
#include "Circle.h"

GLuint texture_grass1;
GLuint texture_scenario1;

float tx = 0.0f;

void scenario1(void)
{
	glTranslatef(tx, 0.0f, -20.0f);

	glColor3f(0.1784f, 0.1823f, 0.1980f);
	// Road
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, -4.5f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, -4.5f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -8.5f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -8.5f, 0.0f);

	glEnd();

	// Grass Across Road
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_grass1);
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -4.5f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -4.5f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	// texture city
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_scenario1);
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, 8.5f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, 8.5f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -1.0f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -1.0f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	// White lines in road
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);

	glVertex3f(14.0f, -6.2f, 0.0f);
	glVertex3f(10.0f, -6.2f, 0.0f);
	glVertex3f(10.0f, -6.7f, 0.0f);
	glVertex3f(14.0f, -6.7f, 0.0f);

	glVertex3f(6.0f, -6.2f, 0.0f);
	glVertex3f(2.0f, -6.2f, 0.0f);
	glVertex3f(2.0f, -6.7f, 0.0f);
	glVertex3f(6.0f, -6.7f, 0.0f);

	glVertex3f(-2.0f, -6.2f, 0.0f);
	glVertex3f(-6.0f, -6.2f, 0.0f);
	glVertex3f(-6.0f, -6.7f, 0.0f);
	glVertex3f(-2.0f, -6.7f, 0.0f);

	glVertex3f(-10.0f, -6.2f, 0.0f);
	glVertex3f(-14.0f, -6.2f, 0.0f);
	glVertex3f(-14.0f, -6.7f, 0.0f);
	glVertex3f(-10.0f, -6.7f, 0.0f);

	glVertex3f(-18.0f, -6.2f, 0.0f);
	glVertex3f(-22.0f, -6.2f, 0.0f);
	glVertex3f(-22.0f, -6.7f, 0.0f);
	glVertex3f(-18.0f, -6.7f, 0.0f);

	glVertex3f(-26.0f, -6.2f, 0.0f);
	glVertex3f(-30.0f, -6.2f, 0.0f);
	glVertex3f(-30.0f, -6.7f, 0.0f);
	glVertex3f(-26.0f, -6.7f, 0.0f);

	glEnd();

	// From Right To Left

	// ******************************** tree1
	glPushMatrix();
	glScalef(1.8f, 1.8f, 0.0f);

	glColor3f(0.0, 0.3921f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 6.2f, -0.07f);

	glColor3f(0.0f, 1.0f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 6.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, 0.5f);
	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glBegin(GL_TRIANGLES);

	glVertex3f(6.0f, 0.3f, 0.0f);
	glVertex3f(5.95f, -0.3f, 0.0f);
	glVertex3f(6.05f, -0.3f, 0.0f);

	glVertex3f(6.05f, -0.3f, 0.0f);
	glVertex3f(6.05f, -0.5f, 0.0f);
	glVertex3f(5.68f, 0.0f, 0.0f);

	glVertex3f(6.04f, -0.4f, 0.0f);
	glVertex3f(6.04f, -0.6f, 0.0f);
	glVertex3f(6.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(6.04f, -0.3f, 0.0f);
	glVertex3f(5.94f, -0.3f, 0.0f);
	glVertex3f(5.94f, -2.2f, 0.0f);
	glVertex3f(6.08f, -2.2f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(7.0f, -2.15f, 0.0f);
	glVertex3f(6.08f, -2.15f, 0.0f);
	glVertex3f(6.08f, -2.2f, 0.0f);
	glVertex3f(7.0f, -2.2f, 0.0f);

	glEnd();

	glPopMatrix();

	// ********************************** tree 2
	glPushMatrix();
	glScalef(1.2f, 1.2f, 0.0f);

	glColor3f(0.5019f, 0.5019f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 6.2f, -0.07f);

	glColor3f(0.6274f, 0.6274f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 6.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, 0.5f);
	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glBegin(GL_TRIANGLES);

	glVertex3f(6.0f, 0.3f, 0.0f);
	glVertex3f(5.95f, -0.3f, 0.0f);
	glVertex3f(6.05f, -0.3f, 0.0f);

	glVertex3f(6.05f, -0.3f, 0.0f);
	glVertex3f(6.05f, -0.5f, 0.0f);
	glVertex3f(5.68f, 0.0f, 0.0f);

	glVertex3f(6.04f, -0.4f, 0.0f);
	glVertex3f(6.04f, -0.6f, 0.0f);
	glVertex3f(6.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(6.04f, -0.3f, 0.0f);
	glVertex3f(5.94f, -0.3f, 0.0f);
	glVertex3f(5.94f, -2.2f, 0.0f);
	glVertex3f(6.08f, -2.2f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(7.0f, -2.15f, 0.0f);
	glVertex3f(6.08f, -2.15f, 0.0f);
	glVertex3f(6.08f, -2.2f, 0.0f);
	glVertex3f(7.0f, -2.2f, 0.0f);

	glEnd();

	glPopMatrix();

	// ********************************** tree 3
	glPushMatrix();
	glScalef(0.8f, 0.8f, 0.0f);

	glColor3f(0.0, 0.3921f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 6.2f, -0.07f);

	glColor3f(0.0f, 1.0f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 6.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, 0.5f);
	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glBegin(GL_TRIANGLES);

	glVertex3f(6.0f, 0.3f, 0.0f);
	glVertex3f(5.95f, -0.3f, 0.0f);
	glVertex3f(6.05f, -0.3f, 0.0f);

	glVertex3f(6.05f, -0.3f, 0.0f);
	glVertex3f(6.05f, -0.5f, 0.0f);
	glVertex3f(5.68f, 0.0f, 0.0f);

	glVertex3f(6.04f, -0.4f, 0.0f);
	glVertex3f(6.04f, -0.6f, 0.0f);
	glVertex3f(6.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(6.04f, -0.3f, 0.0f);
	glVertex3f(5.94f, -0.3f, 0.0f);
	glVertex3f(5.94f, -2.2f, 0.0f);
	glVertex3f(6.08f, -2.2f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(7.0f, -2.15f, 0.0f);
	glVertex3f(6.08f, -2.15f, 0.0f);
	glVertex3f(6.08f, -2.2f, 0.0f);
	glVertex3f(7.0f, -2.2f, 0.0f);

	glEnd();

	glPopMatrix();

	// ********************************** tree 4
	glPushMatrix();
	glScalef(1.8f, 1.8f, 0.0f);

	glColor3f(0.5019f, 0.5019f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 0.2f, -0.07f);

	glColor3f(0.6274f, 0.6274f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, 0.0f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glBegin(GL_TRIANGLES);

	glVertex3f(0.0f, 0.3f, 0.0f);
	glVertex3f(-0.05f, -0.3f, 0.0f);
	glVertex3f(0.05f, -0.3f, 0.0f);

	glVertex3f(0.05f, -0.3f, 0.0f);
	glVertex3f(0.05f, -0.5f, 0.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);

	glVertex3f(0.04f, -0.4f, 0.0f);
	glVertex3f(0.04f, -0.6f, 0.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(0.04f, -0.3f, 0.0f);
	glVertex3f(-0.04f, -0.3f, 0.0f);
	glVertex3f(-0.08f, -2.2f, 0.0f);
	glVertex3f(0.08f, -2.2f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(1.0f, -2.15f, 0.0f);
	glVertex3f(0.08f, -2.15f, 0.0f);
	glVertex3f(0.08f, -2.2f, 0.0f);
	glVertex3f(1.0f, -2.2f, 0.0f);

	glEnd();
	glPopMatrix();

	// ********************************** tree 5
	glPushMatrix();
	glScalef(1.2f, 1.2f, 0.0f);

	glColor3f(0.0, 0.3921f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, -5.8f, -0.07f);

	glColor3f(0.0f, 1.0f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, -6.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, 0.5f);
	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glBegin(GL_TRIANGLES);

	glVertex3f(-6.0f, 0.3f, 0.0f);
	glVertex3f(-5.95f, -0.3f, 0.0f);
	glVertex3f(-6.05f, -0.3f, 0.0f);

	glVertex3f(-6.05f, -0.3f, 0.0f);
	glVertex3f(-6.05f, -0.5f, 0.0f);
	glVertex3f(-5.68f, 0.0f, 0.0f);

	glVertex3f(-6.04f, -0.4f, 0.0f);
	glVertex3f(-6.04f, -0.6f, 0.0f);
	glVertex3f(-6.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-6.04f, -0.3f, 0.0f);
	glVertex3f(-5.94f, -0.3f, 0.0f);
	glVertex3f(-5.94f, -2.2f, 0.0f);
	glVertex3f(-6.08f, -2.2f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(-5.0f, -2.15f, 0.0f);
	glVertex3f(-5.95f, -2.15f, 0.0f);
	glVertex3f(-5.95f, -2.2f, 0.0f);
	glVertex3f(-5.0f, -2.2f, 0.0f);

	glEnd();

	glPopMatrix();


	// ********************************** tree 6
	glPushMatrix();
	glScalef(1.8f, 1.8f, 0.0f);

	glColor3f(0.5019f, 0.5019f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, -5.8f, -0.07f);

	glColor3f(0.6274f, 0.6274f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, -6.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, 0.5f);
	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glBegin(GL_TRIANGLES);

	glVertex3f(-6.0f, 0.3f, 0.0f);
	glVertex3f(-5.95f, -0.3f, 0.0f);
	glVertex3f(-6.05f, -0.3f, 0.0f);

	glVertex3f(-6.05f, -0.3f, 0.0f);
	glVertex3f(-6.05f, -0.5f, 0.0f);
	glVertex3f(-5.68f, 0.0f, 0.0f);

	glVertex3f(-6.04f, -0.4f, 0.0f);
	glVertex3f(-6.04f, -0.6f, 0.0f);
	glVertex3f(-6.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-6.04f, -0.3f, 0.0f);
	glVertex3f(-5.94f, -0.3f, 0.0f);
	glVertex3f(-5.94f, -2.2f, 0.0f);
	glVertex3f(-6.08f, -2.2f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(-5.0f, -2.15f, 0.0f);
	glVertex3f(-5.95f, -2.15f, 0.0f);
	glVertex3f(-5.95f, -2.2f, 0.0f);
	glVertex3f(-5.0f, -2.2f, 0.0f);

	glEnd();

	glPopMatrix();

	// ********************************** tree 7
	glPushMatrix();
	glScalef(1.8f, 1.8f, 0.0f);

	glColor3f(0.0, 0.3921f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, -10.8f, -0.07f);

	glColor3f(0.0f, 1.0f, 0.0f);
	MDM_DrawFilledCircle(0.6f, 0.8f, -11.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, 0.5f);
	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glBegin(GL_TRIANGLES);

	glVertex3f(-11.0f, 0.3f, 0.0f);
	glVertex3f(-10.95f, -0.3f, 0.0f);
	glVertex3f(-11.05f, -0.3f, 0.0f);

	glVertex3f(-11.05f, -0.3f, 0.0f);
	glVertex3f(-11.05f, -0.5f, 0.0f);
	glVertex3f(-10.68f, 0.0f, 0.0f);

	glVertex3f(-11.04f, -0.4f, 0.0f);
	glVertex3f(-11.04f, -0.6f, 0.0f);
	glVertex3f(-11.4f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-11.04f, -0.3f, 0.0f);
	glVertex3f(-10.94f, -0.3f, 0.0f);
	glVertex3f(-10.94f, -2.2f, 0.0f);
	glVertex3f(-11.08f, -2.2f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(-10.0f, -2.15f, 0.0f);
	glVertex3f(-10.95f, -2.15f, 0.0f);
	glVertex3f(-10.95f, -2.2f, 0.0f);
	glVertex3f(-10.0f, -2.2f, 0.0f);

	glEnd();

	glPopMatrix();
}

void scene3(void)
{
	royalEnfiled();
	scenario1();
}
