#pragma once
#include <GL/gl.h>

void humanoid(void)
{
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, -4.1f);
	glScalef(1.0f, 0.98f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluSphere(quadric, 0.3f, 50, 5);
	glPopMatrix();

	// Hair
	glPushMatrix();
	glTranslatef(0.0f, 1.12f, -4.1f);
	glScalef(1.1f, 1.0f, 1.0f);
	//glRotatef(-20.0f, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.28f, 50, 5);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.99f, -4.15f);
	glScalef(1.1f, 1.0f, 1.0f);
	//glRotatef(-25.0f, 1.0f, 0.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.28f, 50, 5);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.68f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	gluCylinder(quadric, 0.12f, 0.14f, 0.13f, 50, 50);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.72f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluCylinder(quadric, 0.125f, 0.12f, 0.1f, 50, 50);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.55f, -4.1f);
	glScalef(1.0f, 1.0f, 0.5f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glRotatef(95.0f, 1.0f, 0.0f, 0.0f);
	gluDisk(quadric, 0.0f, 0.5f, 50, 50);
	gluCylinder(quadric, 0.5f, 0.45f, 0.9f, 50, 50);
	glTranslatef(0.0f, 0.0f, 0.9f);
	gluDisk(quadric, 0.0f, 0.45f, 50, 50);
	glPopMatrix();

	// Right Arm
	glPushMatrix();
	glTranslatef(0.45f, 0.5f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(20.0f, 0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	gluCylinder(quadric, 0.1f, 0.1f, 0.5f, 50, 50);
	glPopMatrix();

	// Right Elbow
	glPushMatrix();
	glTranslatef(0.65f, 0.0f, -4.1f);
	glRotatef(20.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluCylinder(quadric, 0.1f, 0.1f, 0.5f, 50, 50);
	glPopMatrix();

	// Right Palm
	glPushMatrix();
	glTranslatef(0.65f, -0.19f, -3.55f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluSphere(quadric, 0.11f, 50, 20);
	glPopMatrix();

	// Left Arm
	glPushMatrix();
	glTranslatef(-0.45f, 0.5f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(-20.0f, 0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	gluCylinder(quadric, 0.1f, 0.1f, 0.5f, 50, 50);
	glPopMatrix();

	// Left Elbow
	glPushMatrix();
	glTranslatef(-0.65f, 0.0f, -4.1f);
	glRotatef(20.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluCylinder(quadric, 0.1f, 0.1f, 0.5f, 50, 50);
	glPopMatrix();

	// Left Palm
	glPushMatrix();
	glTranslatef(-0.65f, -0.19f, -3.55f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluSphere(quadric, 0.11f, 50, 20);
	glPopMatrix();

	// Right Leg
	glPushMatrix();
	glTranslatef(0.225f, -0.5f, -4.3f);
	glRotatef(5.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Right Leg Bottom Part
	glPushMatrix();
	glTranslatef(0.225f, -0.53f, -3.5f);
	glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Right Leg Shoes
	glPushMatrix();
	glTranslatef(0.23f, -1.18f, -2.8f);
	glRotatef(-40.0f, 1.0f, 0.0f, 0.0f);
	glScalef(0.8f, 0.6f, 1.3f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.21f, 50, 20);
	glPopMatrix();

	// Left Leg
	glPushMatrix();
	glTranslatef(-0.225f, -0.5f, -4.3f);
	glRotatef(5.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Left Leg Bottom Part
	glPushMatrix();
	glTranslatef(-0.225f, -0.53f, -3.5f);
	glRotatef(50.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Left Leg Shoes
	glPushMatrix();
	glTranslatef(-0.23f, -1.18f, -2.8f);
	glRotatef(-40.0f, 1.0f, 0.0f, 0.0f);
	glScalef(0.8f, 0.6f, 1.3f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.21f, 50, 20);
	glPopMatrix();
}