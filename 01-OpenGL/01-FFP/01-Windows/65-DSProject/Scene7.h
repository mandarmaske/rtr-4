#pragma once
#include <GL/gl.h>
#include <GL/glu.h> 

GLuint texture_snow1;
GLuint texture_scenario4;
float b1 = 0.0f;
float b2 = 0.0f;
float counter = 0.0f;

void scene7(void)
{
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -7.0f);

	glColor4f(0.7f, 0.7f, 0.7f, b1);
	glBindTexture(GL_TEXTURE_2D, texture_snow1);
	// Road
	glBegin(GL_QUADS);

	glTexCoord2d(0.5f, 1.0f);
	glVertex3f(16.0f, -0.5f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-8.0f, -0.5f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-8.0f, -8.5f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -8.5f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glColor4f(0.5f, 0.5f, 0.5f, b1);
	glBindTexture(GL_TEXTURE_2D, texture_scenario4);
	// Road
	glBegin(GL_QUADS);

	glTexCoord2d(0.5f, 1.0f);
	glVertex3f(16.0f, 3.5f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-8.0f, 3.5f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-8.0f, -0.5f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -0.5f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glLoadIdentity();
	glTranslatef(-0.7f, 0.0f, -5.0f);
	glScalef(0.1f, 0.1f, 0.0f);

	glColor4f(0.6901f, 0.7686f, 0.9705f, b2);
	glBegin(GL_QUADS);

	// Mouth
	glVertex3f(7.0f, 6.0f, 0.0f);
	glVertex3f(5.0f, 5.5f, 0.0f);
	glVertex3f(6.25f, 3.0f, 0.0f);
	glVertex3f(7.02f, 4.5f, 0.0f);

	// Head
	glVertex3f(5.0f, 5.5f, 0.0f);
	glVertex3f(4.0f, 5.7f, 0.0f);
	glVertex3f(2.0f, 4.5f, 0.0f);
	glVertex3f(6.25f, 3.0f, 0.0f);

	// Ear
	glVertex3f(2.0f, 4.5f, 0.0f);
	glVertex3f(1.8f, 4.0f, 0.0f);
	glVertex3f(1.6f, 3.0f, 0.0f);
	glVertex3f(1.9150f, 2.8f, 0.0f);

	// Neck
	glVertex3f(6.25f, 3.0f, 0.0f);
	glVertex3f(2.0f, 4.5f, 0.0f);
	glVertex3f(1.8f, 0.5f, 0.0f);
	glVertex3f(6.0f, 1.0f, 0.0f);

	// Neck Below Part
	glVertex3f(6.0f, 1.0f, 0.0f);
	glVertex3f(1.8f, 0.5f, 0.0f);
	glVertex3f(1.3f, -0.5f, 0.0f);
	glVertex3f(5.9f, -0.5f, 0.0f);

	// From Front To Ribs Part
	glVertex3f(5.9f, -0.5f, 0.0f);
	glVertex3f(1.3f, -0.5f, 0.0f);
	glVertex3f(-4.0f, -2.0f, 0.0f);
	glVertex3f(5.9f, -3.0f, 0.0f);

	// From Front To Ribs Below Part
	glVertex3f(5.9f, -3.0f, 0.0f);
	glVertex3f(-3.803f, -2.02f, 0.0f);
	glVertex3f(-4.0f, -5.0f, 0.0f);
	glVertex3f(5.8f, -5.0f, 0.0f);

	// Back Part
	glVertex3f(-3.8f, -2.0f, 0.0f);
	glVertex3f(-7.0f, -2.2f, 0.0f);
	glVertex3f(-7.0f, -5.0f, 0.0f);
	glVertex3f(-4.0f, -5.0f, 0.0f);

	// Belly
	glVertex3f(5.8f, -5.0f, 0.0f);
	glVertex3f(-8.5f, -5.0f, 0.0f);
	glVertex3f(-7.5f, -6.5f, 0.0f);
	glVertex3f(5.5f, -7.0f, 0.0f);

	// Remaining Belly
	glVertex3f(5.5f, -7.0f, 0.0f);
	glVertex3f(-7.5f, -6.5f, 0.0f);
	glVertex3f(-6.0f, -7.5f, 0.0f);
	glVertex3f(5.0f, -7.5f, 0.0f);

	// Tail Side Part
	glVertex3f(-7.0f, -2.2f, 0.0f);
	glVertex3f(-8.5f, -3.0f, 0.0f);
	glVertex3f(-8.5f, -5.0f, 0.0f);
	glVertex3f(-7.0f, -5.0f, 0.0f);

	// Tail
	glVertex3f(-7.0f, -2.2f, 0.0f);
	glVertex3f(-8.5f, -1.7f, 0.0f);
	glVertex3f(-10.0f, -2.1f, 0.0f);
	glVertex3f(-8.5f, -3.0f, 0.0f);

	glVertex3f(-8.5f, -1.7f, 0.0f);
	glVertex3f(-9.7f, -1.1f, 0.0f);
	glVertex3f(-11.31f, -1.1f, 0.0f);
	glVertex3f(-10.0f, -2.1f, 0.0f);

	glVertex3f(-11.7f, 0.1f, 0.0f);
	glVertex3f(-12.6f, 0.1f, 0.0f);
	glVertex3f(-11.3f, -1.1f, 0.0f);
	glVertex3f(-9.7f, -1.1f, 0.0f);

	glVertex3f(-12.3f, 0.4f, 0.0f);
	glVertex3f(-12.8f, 0.48f, 0.0f);
	glVertex3f(-12.6f, 0.1f, 0.0f);
	glVertex3f(-11.7f, 0.1f, 0.0f);

	// Tail Side To Leg Side Part
	glVertex3f(-8.5f, -5.0f, 0.0f);
	glVertex3f(-8.5f, -3.1f, 0.0f);
	glVertex3f(-10.0f, -4.83f, 0.0f);
	glVertex3f(-7.5f, -6.5f, 0.0f);

	glVertex3f(-6.0f, -7.5f, 0.0f);
	glVertex3f(-10.0f, -4.83f, 0.0f);
	glVertex3f(-10.0f, -7.0f, 0.0f);
	glVertex3f(-7.0f, -8.5f, 0.0f);

	// Belly Round Shape
	glVertex3f(5.0f, -7.5f, 0.0f);
	glVertex3f(-6.0f, -7.5f, 0.0f);
	glVertex3f(-6.5f, -8.0f, 0.0f);
	glVertex3f(3.0f, -8.5f, 0.0f);

	glVertex3f(3.0f, -8.5f, 0.0f);
	glVertex3f(-6.5f, -8.0f, 0.0f);
	glVertex3f(-7.0f, -8.5f, 0.0f);
	glVertex3f(2.0f, -9.0f, 0.0f);

	// Behind Legs
	glVertex3f(-7.0f, -8.5f, 0.0f);
	glVertex3f(-10.0f, -7.0f, 0.0f);
	glVertex3f(-11.9f, -11.0f, 0.0f);
	glVertex3f(-10.0f, -12.0f, 0.0f);

	glVertex3f(-10.0f, -7.0f, 0.0f);
	glVertex3f(-10.0f, -4.83f, 0.0f);
	glVertex3f(-11.3f, -8.0f, 0.0f);
	glVertex3f(-11.9f, -11.0f, 0.0f);

	// Behind Actual Legs
	glVertex3f(-10.0f, -12.0f, 0.0f);
	glVertex3f(-11.9f, -11.0f, 0.0f);
	glVertex3f(-12.3f, -12.0f, 0.0f);
	glVertex3f(-10.9f, -13.0f, 0.0f);

	glVertex3f(-10.9f, -13.0f, 0.0f);
	glVertex3f(-12.3f, -12.0f, 0.0f);
	glVertex3f(-12.8f, -13.0f, 0.0f);
	glVertex3f(-10.9f, -13.0f, 0.0f);

	glVertex3f(-10.9f, -13.0f, 0.0f);
	glVertex3f(-12.8f, -13.0f, 0.0f);
	glVertex3f(-12.9f, -14.0f, 0.0f);
	glVertex3f(-11.4f, -14.0f, 0.0f);

	glVertex3f(-11.4f, -14.0f, 0.0f);
	glVertex3f(-12.9f, -14.0f, 0.0f);
	glVertex3f(-12.9f, -14.5f, 0.0f);
	glVertex3f(-11.5f, -14.5f, 0.0f);

	glVertex3f(-11.5f, -14.5f, 0.0f);
	glVertex3f(-12.9f, -14.5f, 0.0f);
	glVertex3f(-13.2f, -15.5f, 0.0f);
	glVertex3f(-11.8f, -15.525f, 0.0f);

	glVertex3f(-12.2f, -15.5f, 0.0f);
	glVertex3f(-13.2f, -15.5f, 0.0f);
	glVertex3f(-13.33f, -16.7f, 0.0f);
	glVertex3f(-12.2f, -16.7f, 0.0f);

	glVertex3f(-11.4f, -15.6f, 0.0f);
	glVertex3f(-12.2f, -15.5f, 0.0f);
	glVertex3f(-12.2f, -16.7f, 0.0f);
	glVertex3f(-11.2f, -16.7f, 0.0f);

	// Behind Other Leg
	glVertex3f(-5.0f, -8.611f, 0.0f);
	glVertex3f(-7.0f, -8.5f, 0.0f);
	glVertex3f(-9.57f, -11.5f, 0.0f);
	glVertex3f(-6.5f, -11.5f, 0.0f);

	glVertex3f(-6.5f, -11.5f, 0.0f);
	glVertex3f(-9.57f, -11.5f, 0.0f);
	glVertex3f(-9.57f, -12.5f, 0.0f);
	glVertex3f(-7.5f, -12.5f, 0.0f);

	glVertex3f(-7.5f, -12.5f, 0.0f);
	glVertex3f(-9.57f, -12.5f, 0.0f);
	glVertex3f(-9.97f, -13.5f, 0.0f);
	glVertex3f(-8.6f, -13.5f, 0.0f);

	glVertex3f(-8.6f, -13.5f, 0.0f);
	glVertex3f(-9.97f, -13.5f, 0.0f);
	glVertex3f(-9.87f, -14.0f, 0.0f);
	glVertex3f(-8.6f, -14.7f, 0.0f);

	glVertex3f(-8.6f, -14.7f, 0.0f);
	glVertex3f(-9.87f, -14.0f, 0.0f);
	glVertex3f(-9.7f, -16.0f, 0.0f);
	glVertex3f(-8.6f, -16.0f, 0.0f);

	glVertex3f(-8.2f, -16.0f, 0.0f);
	glVertex3f(-9.7f, -16.0f, 0.0f);
	glVertex3f(-9.7f, -17.25f, 0.0f);
	glVertex3f(-7.6f, -17.25f, 0.0f);

	// Front Legs From Right

	// Gap
	glVertex3f(5.5f, -7.0f, 0.0f);
	glVertex3f(5.1f, -7.4f, 0.0f);
	glVertex3f(3.0f, -9.5f, 0.0f);
	glVertex3f(5.0f, -9.5f, 0.0f);

	glVertex3f(5.1f, -7.45f, 0.0f);
	glVertex3f(2.0f, -9.0f, 0.0f);
	glVertex3f(2.0f, -9.2f, 0.0f);
	glVertex3f(3.0f, -9.5f, 0.0f);

	glVertex3f(5.0f, -9.5f, 0.0f);
	glVertex3f(3.0f, -9.5f, 0.0f);
	glVertex3f(3.0f, -12.5f, 0.0f);
	glVertex3f(4.7f, -12.0f, 0.0f);

	glVertex3f(4.7f, -12.0f, 0.0f);
	glVertex3f(3.0f, -12.5f, 0.0f);
	glVertex3f(3.1f, -13.5f, 0.0f);
	glVertex3f(4.6f, -13.0f, 0.0f);

	glVertex3f(4.6f, -13.0f, 0.0f);
	glVertex3f(3.1f, -13.5f, 0.0f);
	glVertex3f(3.3f, -15.5f, 0.0f);
	glVertex3f(4.5f, -15.0f, 0.0f);

	glVertex3f(4.5f, -15.0f, 0.0f);
	glVertex3f(3.3f, -15.5f, 0.0f);
	glVertex3f(3.5f, -16.0f, 0.0f);
	glVertex3f(4.8f, -16.0f, 0.0f);

	glVertex3f(5.3f, -16.0f, 0.0f);
	glVertex3f(3.5f, -16.0f, 0.0f);
	glVertex3f(3.7f, -17.25f, 0.0f);
	glVertex3f(5.7f, -17.25f, 0.0f);

	// Other Front Leg
	glVertex3f(2.0f, -9.0f, 0.0f);
	glVertex3f(0.0f, -8.89f, 0.0f);
	glVertex3f(0.3f, -13.0f, 0.0f);
	glVertex3f(1.8f, -13.0f, 0.0f);

	glVertex3f(1.8f, -13.0f, 0.0f);
	glVertex3f(0.3f, -13.0f, 0.0f);
	glVertex3f(0.5f, -16.0f, 0.0f);
	glVertex3f(1.7f, -16.0f, 0.0f);

	glVertex3f(2.1f, -16.0f, 0.0f);
	glVertex3f(0.5f, -16.0f, 0.0f);
	glVertex3f(0.5f, -17.0f, 0.0f);
	glVertex3f(2.4f, -17.0f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(-3.2f, -0.1f, -5.0f);
	glScalef(1.0f, 0.9f, 0.9f);
	glRotatef(270.0f, 0.0f, 1.0f, 0.0f);

	glPushMatrix();
	glTranslatef(0.0f, 1.0f, -4.1f);
	glScalef(1.0f, 0.98f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor4f(0.9098f, 0.7450f, 0.6745f, b1);
	gluSphere(quadric, 0.3f, 50, 5);
	glPopMatrix();

	// Hair
	glPushMatrix();
	glTranslatef(0.0f, 1.12f, -4.1f);
	glScalef(1.1f, 1.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor4f(0.0f, 0.0f, 0.0f, b1);
	gluSphere(quadric, 0.28f, 50, 5);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.99f, -4.15f);
	glScalef(1.1f, 1.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor4f(0.0f, 0.0f, 0.0f, b1);
	gluSphere(quadric, 0.28f, 50, 5);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.68f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.1f, 0.1f, 0.1f, b1);
	gluCylinder(quadric, 0.12f, 0.14f, 0.13f, 50, 50);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.72f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.9098f, 0.7450f, 0.6745f, b1);
	gluCylinder(quadric, 0.125f, 0.12f, 0.1f, 50, 50);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.55f, -4.1f);
	glScalef(1.0f, 1.0f, 0.5f);
	glColor4f(0.1f, 0.1f, 0.1f, b1);
	glRotatef(95.0f, 1.0f, 0.0f, 0.0f);
	gluDisk(quadric, 0.0f, 0.5f, 50, 50);
	gluCylinder(quadric, 0.5f, 0.45f, 0.9f, 50, 50);
	glTranslatef(0.0f, 0.0f, 0.9f);
	gluDisk(quadric, 0.0f, 0.45f, 50, 50);
	glPopMatrix();

	// Left Arm
	glPushMatrix();
	glTranslatef(0.45f, 0.4f, -4.1f);
	glRotatef(42.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.1f, 0.1f, 0.1f, b1);
	gluCylinder(quadric, 0.1f, 0.1f, 0.78f, 50, 50);
	glPopMatrix();

	// Left Elbow
	glPushMatrix();
	glTranslatef(0.45f, -0.1f, -3.555f);
	glRotatef(-15.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.9098f, 0.7450f, 0.6745f, b1);
	gluCylinder(quadric, 0.1f, 0.1f, 0.71f, 50, 50);
	glPopMatrix();

	// Left Palm
	glPushMatrix();
	glTranslatef(0.3f, 0.11f, -2.75f);
	glScalef(1.0f, 1.0f, 1.2f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor4f(0.9098f, 0.7450f, 0.6745f, b1);
	gluSphere(quadric, 0.15f, 50, 20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.225f, -0.5f, -4.3f);
	glRotatef(10.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.5f, 0.5f, 0.5f, b1);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Right Leg Bottom Part
	glPushMatrix();
	glTranslatef(0.17f, -0.55f, -3.5f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.5f, 0.5f, 0.5f, b1);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Right Leg Shoes
	glPushMatrix();
	glTranslatef(0.23f, -1.5f, -3.38f);
	glScalef(0.8f, 0.6f, 1.3f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor4f(0.5294f, 0.2274f, 0.2117f, b1);
	gluSphere(quadric, 0.21f, 50, 20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.225f, -0.5f, -4.3f);
	glRotatef(89.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.5f, 0.5f, 0.5f, b1);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Right Leg Bottom Part
	glPushMatrix();
	glTranslatef(-0.17f, -1.41f, -4.95f);
	glRotatef(10.0f, 1.0f, 0.0f, 0.0f);
	glColor4f(0.5f, 0.5f, 0.5f, b1);
	gluCylinder(quadric, 0.15f, 0.15f, 0.8f, 50, 50);
	glPopMatrix();

	// Right Leg Shoes
	glPushMatrix();
	glTranslatef(-0.23f, -1.5f, -5.1f);
	glRotatef(110.0f, 1.0f, 0.0f, 0.0f);
	glScalef(0.8f, 0.6f, 1.3f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor4f(0.5294f, 0.2274f, 0.2117f, b1);
	gluSphere(quadric, 0.21f, 50, 20);
	glPopMatrix();
}
