#pragma once
#include <GL/gl.h>

#define TERRIANMAP_X 257
#define TERRIANMAP_Z 257
#define MAP_SCALE 5.0
#define MAX_HEIGHT 20

GLuint texture_snow;

unsigned char* imageData = NULL;
float terrainHeightMap[TERRIANMAP_X][TERRIANMAP_Z][3];

float tx3 = -697.0f;
float ty3 = -122.0f;
float tz3 = 95.0f;

void Terrain(void)
{
	glPushMatrix();
	glTranslatef(tx3, ty3, tz3);

	//gluLookAt(0.0f, 0.0f, 10.0f, 0.0f, -3.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	//glScalef(0.5f, 1.0f, 1.0f);
	//glRotatef(angleCube, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_snow);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	for (int z = 0; z < TERRIANMAP_Z - 1; z++)
	{
		glBegin(GL_TRIANGLE_STRIP);
		for (int x = 0; x < TERRIANMAP_X - 1; x++)
		{
			float scaledHeight = terrainHeightMap[x][z][1] / MAP_SCALE;
			float nextScaledHeight = terrainHeightMap[x][z + 1][1] / MAP_SCALE;
			float color = 0.5f + 0.5f * scaledHeight / MAX_HEIGHT;
			float nextColor = 0.5f + 0.5f * nextScaledHeight / MAX_HEIGHT;

			glColor3f(color, color, color);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(terrainHeightMap[x][z][0], terrainHeightMap[x][z][1], terrainHeightMap[x][z][2]);
			glColor3f(color, color, color);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(terrainHeightMap[x + 1][z][0], terrainHeightMap[x + 1][z][1], terrainHeightMap[x + 1][z][2]);
			glColor3f(nextColor, nextColor, nextColor);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(terrainHeightMap[x][z + 1][0], terrainHeightMap[x][z + 1][1], terrainHeightMap[x][z + 1][2]);
			glColor3f(nextColor, nextColor, nextColor);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(terrainHeightMap[x + 1][z + 1][0], terrainHeightMap[x + 1][z + 1][1], terrainHeightMap[x + 1][z + 1][2]);
		}
		glEnd();
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();
}

void scene6(void)
{
	Terrain();
}
