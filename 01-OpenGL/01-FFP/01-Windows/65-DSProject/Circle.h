#pragma once
#include <GL/gl.h>
#include <math.h> 

void MDM_DrawFilledCircle(double radius_x, double radius_y, float start, float end)
{
	glBegin(GL_POLYGON);
	for (int i = 0; i <= 100; i++)
	{
		double angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		double x = cos(angle) * radius_x;
		double y = sin(angle) * radius_y;
		glVertex2d(start + x, end + y);
	}
	glEnd();
}