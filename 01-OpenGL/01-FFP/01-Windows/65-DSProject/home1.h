#pragma once
#include <GL/gl.h>
#include <GL/glu.h>

GLuint texture_roof1;

void cube1(void)
{
	glColor3f(1.0f, 1.0f, 0.7490f);
	glBegin(GL_QUADS);

	// Front Face
	//glColor3f(0.8f, 0.4f, 0.0f);
	glVertex3f(1.0f, 0.5f, 1.0f);
	glVertex3f(-1.0f, 0.5f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// Right Face
	//glColor3f(0.8f, 0.4f, 0.0f);
	glVertex3f(1.0f, 0.5f, -3.0f);
	glVertex3f(1.0f, 0.5f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -3.0f);

	// Back Face
	//glColor3f(0.8f, 0.4f, 0.0f);
	glVertex3f(-1.0f, 0.5f, -3.0f);
	glVertex3f(1.0f, 0.5f, -3.0f);
	glVertex3f(1.0f, -1.0f, -3.0f);
	glVertex3f(-1.0f, -1.0f, -3.0f);

	// Left Face
	//glColor3f(0.8f, 0.4f, 0.0f);
	glVertex3f(-1.0f, 0.5f, 1.0f);
	glVertex3f(-1.0f, 0.5f, -3.0f);
	glVertex3f(-1.0f, -1.0f, -3.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	// Bottom Face - same like Top Face when it dropped down like shattered glass
	glColor3f(0.8f, 0.4f, 0.0f);
	glVertex3f(1.0f, -1.0f, -3.0f);
	glVertex3f(-1.0f, -1.0f, -3.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

}

void woodenPillars1(void)
{
	// wooden pillar front right side

	glColor3f(0.3019f, 0.149f, 0.0f);

	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(1.03f, 0.5f, 1.05f);
	glVertex3f(0.95f, 0.5f, 1.05f);
	glVertex3f(0.95f, -1.0f, 1.05f);
	glVertex3f(1.03f, -1.0f, 1.05f);

	// Right Face
	glVertex3f(1.03f, 0.5f, 0.95f);
	glVertex3f(1.03f, 0.5f, 1.05f);
	glVertex3f(1.03f, -1.0f, 1.05f);
	glVertex3f(1.03f, -1.0f, 0.95f);

	// Back Face Of Right Face
	glVertex3f(1.0f, 0.5f, 0.95f);
	glVertex3f(1.03f, 0.5f, 0.95f);
	glVertex3f(1.03f, -1.0f, 0.95f);
	glVertex3f(1.0f, -1.0f, 0.95f);

	//Left Face
	glVertex3f(0.95f, 0.5f, 1.05f);
	glVertex3f(0.95f, 0.5f, 1.0f);
	glVertex3f(0.95f, -1.0f, 1.0f);
	glVertex3f(0.95f, -1.0f, 1.05f);

	glEnd();

	// wooden pillar front left side

	glColor3f(0.3019f, 0.149f, 0.0f);
	glBegin(GL_QUADS);

	// Front Face 
	glVertex3f(-0.95f, 0.5f, 1.05f);
	glVertex3f(-1.03f, 0.5f, 1.05f);
	glVertex3f(-1.03f, -1.0f, 1.05f);
	glVertex3f(-0.95f, -1.0f, 1.05f);

	// Left Face
	glVertex3f(-1.03f, 0.5f, 0.95f);
	glVertex3f(-1.03f, 0.5f, 1.05f);
	glVertex3f(-1.03f, -1.0f, 1.05f);
	glVertex3f(-1.03f, -1.0f, 0.95f);

	// Back Face Of Left Face
	glVertex3f(-1.0f, 0.5f, 0.95f);
	glVertex3f(-1.03f, 0.5f, 0.95f);
	glVertex3f(-1.03f, -1.0f, 0.95f);
	glVertex3f(-1.0f, -1.0f, 0.95f);

	// Right Face
	glVertex3f(-0.95f, 0.5f, 1.05f);
	glVertex3f(-0.95f, 0.5f, 1.0f);
	glVertex3f(-0.95f, -1.0f, 1.0f);
	glVertex3f(-0.95f, -1.0f, 1.05f);

	glEnd();

	// wooden pillar back right side

	glColor3f(0.3019f, 0.149f, 0.0f);
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(1.03f, 0.5f, -3.05f);
	glVertex3f(0.95f, 0.5f, -3.05f);
	glVertex3f(0.95f, -1.0f, -3.05f);
	glVertex3f(1.03f, -1.0f, -3.05f);

	// Left Face
	glVertex3f(1.03f, 0.5f, -2.95f);
	glVertex3f(1.03f, 0.5f, -3.05f);
	glVertex3f(1.03f, -1.0f, -3.05f);
	glVertex3f(1.03f, -1.0f, -2.95f);

	// Back Face Of Right Face
	glVertex3f(1.0f, 0.5f, -2.95f);
	glVertex3f(1.03f, 0.5f, -2.95f);
	glVertex3f(1.03f, -1.0f, -2.95f);
	glVertex3f(1.0f, -1.0f, -2.95f);

	//Left Face
	glVertex3f(0.95f, 0.5f, -3.05f);
	glVertex3f(0.95f, 0.5f, -3.0f);
	glVertex3f(0.95f, -1.0f, -3.0f);
	glVertex3f(0.95f, -1.0f, -3.05f);

	glEnd();

	// wooden pillar back left side

	glColor3f(0.3019f, 0.149f, 0.0f);
	glBegin(GL_QUADS);

	// Front Face 

	glVertex3f(-0.95f, 0.5f, -3.05f);
	glVertex3f(-1.03f, 0.5f, -3.05f);
	glVertex3f(-1.03f, -1.0f, -3.05f);
	glVertex3f(-0.95f, -1.0f, -3.05f);

	// Left Face
	glVertex3f(-1.03f, 0.5f, -2.95f);
	glVertex3f(-1.03f, 0.5f, -3.05f);
	glVertex3f(-1.03f, -1.0f, -3.05f);
	glVertex3f(-1.03f, -1.0f, -2.95f);

	// Back Face Of Left Face
	glVertex3f(-1.0f, 0.5f, -2.95f);
	glVertex3f(-1.03f, 0.5f, -2.95f);
	glVertex3f(-1.03f, -1.0f, -2.95f);
	glVertex3f(-1.0f, -1.0f, -2.95f);

	// Right Face
	glVertex3f(-0.95f, 0.5f, -3.05f);
	glVertex3f(-0.95f, 0.5f, -3.0f);
	glVertex3f(-0.95f, -1.0f, -3.0f);
	glVertex3f(-0.95f, -1.0f, -3.05f);

	glEnd();

	// Horizontal Wooden Pillar Front Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(-1.03f, 0.58f, 1.05f);
	glVertex3f(1.03f, 0.58f, 1.05f);
	glVertex3f(1.03f, 0.5f, 1.05f);
	glVertex3f(-1.03f, 0.5f, 1.05f);

	glVertex3f(1.03f, 0.58f, 1.0f);
	glVertex3f(-1.03f, 0.58f, 1.0f);
	glVertex3f(-1.03f, 0.58f, 1.05f);
	glVertex3f(1.03f, 0.58f, 1.05f);

	glVertex3f(1.03f, 0.5f, 1.0f);
	glVertex3f(-1.03f, 0.5f, 1.0f);
	glVertex3f(-1.03f, 0.5f, 1.05f);
	glVertex3f(1.03f, 0.5f, 1.05f);

	glEnd();

	// Horizontal Wooden Pillar Back Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(-1.03f, 0.58f, -3.05f);
	glVertex3f(1.03f, 0.58f, -3.05f);
	glVertex3f(1.03f, 0.5f, -3.05f);
	glVertex3f(-1.03f, 0.5f, -3.05f);

	glVertex3f(1.03f, 0.58f, -3.0f);
	glVertex3f(-1.03f, 0.58f, -3.0f);
	glVertex3f(-1.03f, 0.58f, -3.05f);
	glVertex3f(1.03f, 0.58f, -3.05f);

	glVertex3f(1.03f, 0.5f, -3.0f);
	glVertex3f(-1.03f, 0.5f, -3.0f);
	glVertex3f(-1.03f, 0.5f, -3.05f);
	glVertex3f(1.03f, 0.5f, -3.05f);

	glEnd();


	// Horizontal Wooden Pillar Right Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(1.03f, 0.58f, -3.05f);
	glVertex3f(1.03f, 0.58f, 1.05f);
	glVertex3f(1.03f, 0.5f, 1.05f);
	glVertex3f(1.03f, 0.5f, -3.05f);

	glVertex3f(0.95f, 0.58f, -3.05f);
	glVertex3f(0.95f, 0.58f, 1.05f);
	glVertex3f(1.03f, 0.58f, 1.05f);
	glVertex3f(1.03f, 0.58f, -3.05f);

	glVertex3f(0.95f, 0.5f, -3.05f);
	glVertex3f(0.95f, 0.5f, 1.05f);
	glVertex3f(1.03f, 0.5f, 1.05f);
	glVertex3f(1.03f, 0.5f, -3.05f);

	glEnd();

	// Horizontal Wooden Pillar Left Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(-1.03f, 0.58f, -3.05f);
	glVertex3f(-1.03f, 0.58f, 1.05f);
	glVertex3f(-1.03f, 0.5f, 1.05f);
	glVertex3f(-1.03f, 0.5f, -3.05f);

	glVertex3f(-0.95f, 0.58f, -3.05f);
	glVertex3f(-0.95f, 0.58f, 1.05f);
	glVertex3f(-1.03f, 0.58f, 1.05f);
	glVertex3f(-1.03f, 0.58f, -3.05f);

	glVertex3f(-0.95f, 0.5f, -3.05f);
	glVertex3f(-0.95f, 0.5f, 1.05f);
	glVertex3f(-1.03f, 0.5f, 1.05f);
	glVertex3f(-1.03f, 0.5f, -3.05f);

	glEnd();

	// Triangular Wooden Pillars Front Right Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(1.25f, 0.5f, 1.05f);
	glVertex3f(0.0f, 1.2f, 1.05f);
	glVertex3f(0.0f, 1.12f, 1.05f);
	glVertex3f(1.25f, 0.4f, 1.05f);

	glVertex3f(1.25f, 0.5f, 0.95f);
	glVertex3f(0.0f, 1.2f, 0.95f);
	glVertex3f(0.0f, 1.12f, 0.95f);
	glVertex3f(1.25f, 0.4f, 0.95f);

	glVertex3f(1.25f, 0.5f, 0.95f);
	glVertex3f(0.0f, 1.2f, 0.95f);
	glVertex3f(0.0f, 1.2f, 1.05f);
	glVertex3f(1.25f, 0.5f, 1.05f);

	glVertex3f(1.25f, 0.4f, 0.95f);
	glVertex3f(0.0f, 1.12f, 0.95f);
	glVertex3f(0.0f, 1.12f, 1.05f);
	glVertex3f(1.25f, 0.4f, 1.05f);

	glVertex3f(1.25f, 0.5f, 0.95f);
	glVertex3f(1.25f, 0.5f, 1.05f);
	glVertex3f(1.25f, 0.4f, 1.05f);
	glVertex3f(1.25f, 0.4f, 0.95f);

	glEnd();

	// Triangular Wooden Pillars Front Left Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(-1.25f, 0.5f, 1.05f);
	glVertex3f(0.0f, 1.2f, 1.05f);
	glVertex3f(0.0f, 1.12f, 1.05f);
	glVertex3f(-1.25f, 0.4f, 1.05f);

	glVertex3f(-1.25f, 0.5f, 0.95f);
	glVertex3f(0.0f, 1.2f, 0.95f);
	glVertex3f(0.0f, 1.12f, 0.95f);
	glVertex3f(-1.25f, 0.4f, 0.95f);

	glVertex3f(-1.25f, 0.5f, 0.95f);
	glVertex3f(0.0f, 1.2f, 0.95f);
	glVertex3f(0.0f, 1.2f, 1.05f);
	glVertex3f(-1.25f, 0.5f, 1.05f);

	glVertex3f(-1.25f, 0.4f, 0.95f);
	glVertex3f(0.0f, 1.12f, 0.95f);
	glVertex3f(0.0f, 1.12f, 1.05f);
	glVertex3f(-1.25f, 0.4f, 1.05f);

	glVertex3f(-1.25f, 0.5f, 0.95f);
	glVertex3f(-1.25f, 0.5f, 1.05f);
	glVertex3f(-1.25f, 0.4f, 1.05f);
	glVertex3f(-1.25f, 0.4f, 0.95f);

	glEnd();

	// Triangular Wooden Pillars Back Right Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(1.25f, 0.5f, -3.05f);
	glVertex3f(0.0f, 1.2f, -3.05f);
	glVertex3f(0.0f, 1.12f, -3.05f);
	glVertex3f(1.25f, 0.4f, -3.05f);

	glVertex3f(1.25f, 0.5f, -2.95f);
	glVertex3f(0.0f, 1.2f, -2.95f);
	glVertex3f(0.0f, 1.12f, -2.95f);
	glVertex3f(1.25f, 0.4f, -2.95f);

	glVertex3f(1.25f, 0.5f, -2.95f);
	glVertex3f(0.0f, 1.2f, -2.95f);
	glVertex3f(0.0f, 1.2f, -3.05f);
	glVertex3f(1.25f, 0.5f, -3.05f);

	glVertex3f(1.25f, 0.4f, -2.95f);
	glVertex3f(0.0f, 1.12f, -2.95f);
	glVertex3f(0.0f, 1.12f, -3.05f);
	glVertex3f(1.25f, 0.4f, -3.05f);

	glVertex3f(1.25f, 0.5f, -2.95f);
	glVertex3f(1.25f, 0.5f, -3.05f);
	glVertex3f(1.25f, 0.4f, -3.05f);
	glVertex3f(1.25f, 0.4f, -2.95f);

	glEnd();

	// Triangular Wooden Pillars Front Left Side

	glBegin(GL_QUADS);

	glColor3f(0.3019f, 0.149f, 0.0f);

	glVertex3f(-1.25f, 0.5f, -3.05f);
	glVertex3f(0.0f, 1.2f, -3.05f);
	glVertex3f(0.0f, 1.12f, -3.05f);
	glVertex3f(-1.25f, 0.4f, -3.05f);

	glVertex3f(-1.25f, 0.5f, -2.95f);
	glVertex3f(0.0f, 1.2f, -2.95f);
	glVertex3f(0.0f, 1.12f, -2.95f);
	glVertex3f(-1.25f, 0.4f, -2.95f);

	glVertex3f(-1.25f, 0.5f, -2.95f);
	glVertex3f(0.0f, 1.2f, -2.95f);
	glVertex3f(0.0f, 1.2f, -3.05f);
	glVertex3f(-1.25f, 0.5f, -3.05f);

	glVertex3f(-1.25f, 0.4f, -2.95f);
	glVertex3f(0.0f, 1.12f, -2.95f);
	glVertex3f(0.0f, 1.12f, -3.05f);
	glVertex3f(-1.25f, 0.4f, -3.05f);

	glVertex3f(-1.25f, 0.5f, -2.95f);
	glVertex3f(-1.25f, 0.5f, -3.05f);
	glVertex3f(-1.25f, 0.4f, -3.05f);
	glVertex3f(-1.25f, 0.4f, -2.95f);

	glEnd();
}

void pyramid1(void)
{
	glColor3f(1.0f, 1.0f, 0.7490f);
	//glColor3f(0.8f, 0.4f, 0.0f);

	glBegin(GL_TRIANGLES);

	// Front Triangle
	glVertex3f(0.0f, 1.2f, 1.0f);
	glVertex3f(-1.0f, 0.58f, 1.0f);
	glVertex3f(1.0f, 0.58f, 1.0f);

	// Back Triangle
	glVertex3f(0.0f, 1.2f, -3.0f);
	glVertex3f(-1.0f, 0.58f, -3.0f);
	glVertex3f(1.0f, 0.58f, -3.0f);

	glEnd();

	// Roof Quads Other Than Triangles

	glBindTexture(GL_TEXTURE_2D, texture_roof1);
	glBegin(GL_QUADS);

	//glColor3f(0.9f, 0.5f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 1.17f, -2.95f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, 1.17f, 0.95f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.25f, 0.47f, 0.95f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.25f, 0.47f, -2.95f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, texture_roof1);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 1.17f, -2.95f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.0f, 1.17f, 0.95f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.25f, 0.47f, 0.95f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.25f, 0.47f, -2.95f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void beautification1(void)
{
	glBegin(GL_QUADS);

	// Door
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.001f, 0.3f, -1.3f);
	glVertex3f(1.001f, 0.3f, -0.7f);
	glVertex3f(1.001f, -0.88f, -0.7f);
	glVertex3f(1.001f, -0.88f, -1.3f);

	// Inner Door
	glVertex3f(0.99f, 0.3f, -0.3f);
	glVertex3f(0.99f, 0.3f, 1.7f);
	glVertex3f(0.99f, -0.88f, 1.7f);
	glVertex3f(0.99f, -0.88f, -0.3f);

	// Right Side Windows 
	glColor3f(0.0f, 1.0f, 1.0f);
	// Right Window
	glVertex3f(1.001f, 0.1f, -2.5f);
	glVertex3f(1.001f, 0.1f, -2.0f);
	glVertex3f(1.001f, -0.4f, -2.0f);
	glVertex3f(1.001f, -0.4f, -2.5f);

	glVertex3f(0.995f, 0.1f, -1.5f);
	glVertex3f(0.995f, 0.1f, -1.0f);
	glVertex3f(0.995f, -0.4f, -1.0f);
	glVertex3f(0.995f, -0.4f, -1.5f);

	// Left Window
	glVertex3f(1.001f, 0.1f, 0.0f);
	glVertex3f(1.001f, 0.1f, 0.5f);
	glVertex3f(1.001f, -0.4f, 0.5f);
	glVertex3f(1.001f, -0.4f, 0.0f);

	// Front Side Window
	glVertex3f(0.3f, 0.1f, 1.001f);
	glVertex3f(-0.3f, 0.1f, 1.001f);
	glVertex3f(-0.3f, -0.4f, 1.001f);
	glVertex3f(0.3f, -0.4f, 1.001f);

	// Back Side Window
	glVertex3f(0.3f, 0.1f, -3.001f);
	glVertex3f(-0.3f, 0.1f, -3.001f);
	glVertex3f(-0.3f, -0.4f, -3.001f);
	glVertex3f(0.3f, -0.4f, -3.001f);

	glVertex3f(0.3f, 0.1f, -2.995f);
	glVertex3f(-0.3f, 0.1f, -2.995f);
	glVertex3f(-0.3f, -0.4f, -2.995f);
	glVertex3f(0.3f, -0.4f, -2.995f);

	// Lower Strip Front Face
	glColor3f(0.25f, 0.066f, 0.0f);
	glVertex3f(1.0f, -0.88f, 1.001f);
	glVertex3f(-1.0f, -0.88f, 1.001f);
	glVertex3f(-1.0f, -1.0f, 1.001f);
	glVertex3f(1.0f, -1.0f, 1.001f);

	// Lower Strip Right Face
	glVertex3f(1.001f, -0.88f, -3.0f);
	glVertex3f(1.001f, -0.88f, 1.0f);
	glVertex3f(1.001f, -1.0f, 1.0f);
	glVertex3f(1.001f, -1.0f, -3.0f);

	// Lower Strip Back Face
	glVertex3f(1.0f, -0.88f, -3.001f);
	glVertex3f(-1.0f, -0.88f, -3.001f);
	glVertex3f(-1.0f, -1.0f, -3.001f);
	glVertex3f(1.0f, -1.0f, -3.001f);

	// Lower Strip Left Face
	glVertex3f(-1.001f, -0.88f, -3.0f);
	glVertex3f(-1.001f, -0.88f, 1.0f);
	glVertex3f(-1.001f, -1.0f, 1.0f);
	glVertex3f(-1.001f, -1.0f, -3.0f);

	// Door Top Strip

	glVertex3f(1.01f, 0.2f, -1.4f);
	glVertex3f(1.01f, 0.2f, -0.8f);
	glVertex3f(1.01f, 0.3f, -0.8f);
	glVertex3f(1.01f, 0.3f, -1.4f);

	// Door Right Strip
	glVertex3f(1.01f, 0.3f, -1.4f);
	glVertex3f(1.01f, 0.3f, -1.29f);
	glVertex3f(1.01f, -0.88f, -1.29f);
	glVertex3f(1.01f, -0.88f, -1.4f);

	// Door Left Strip
	glVertex3f(1.01f, 0.3f, -0.7f);
	glVertex3f(1.01f, 0.3f, -0.8f);
	glVertex3f(1.01f, -0.88f, -0.8f);
	glVertex3f(1.01f, -0.88f, -0.7f);

	// Right Window Strip Top
	glVertex3f(1.001f, 0.2f, -2.6f);
	glVertex3f(1.001f, 0.2f, -1.9f);
	glVertex3f(1.001f, 0.1f, -1.9f);
	glVertex3f(1.001f, 0.1f, -2.6f);

	//  Right Window Strip Right
	glVertex3f(1.001f, 0.1f, -2.6f);
	glVertex3f(1.001f, 0.1f, -2.5f);
	glVertex3f(1.001f, -0.4f, -2.5f);
	glVertex3f(1.001f, -0.4f, -2.6f);

	// Right Window Strip Bottom
	glVertex3f(1.001f, -0.4f, -2.6f);
	glVertex3f(1.001f, -0.4f, -1.9f);
	glVertex3f(1.001f, -0.5f, -1.9f);
	glVertex3f(1.001f, -0.5f, -2.6f);

	//  Right Window Strip Left
	glVertex3f(1.001f, 0.1f, -2.0f);
	glVertex3f(1.001f, 0.1f, -1.9f);
	glVertex3f(1.001f, -0.4f, -1.9f);
	glVertex3f(1.001f, -0.4f, -2.0f);

	// Left Window Strip Top
	glVertex3f(1.001f, 0.2f, -0.1f);
	glVertex3f(1.001f, 0.2f, 0.6f);
	glVertex3f(1.001f, 0.1f, 0.6f);
	glVertex3f(1.001f, 0.1f, -0.1f);

	// Left Window Strip Right
	glVertex3f(1.001f, 0.1f, -0.1f);
	glVertex3f(1.001f, 0.1f, 0.0f);
	glVertex3f(1.001f, -0.4f, 0.0f);
	glVertex3f(1.001f, -0.4f, -0.1f);

	// Left Window Strip Bottom
	glVertex3f(1.001f, -0.4f, -0.1f);
	glVertex3f(1.001f, -0.4f, 0.6f);
	glVertex3f(1.001f, -0.5f, 0.6f);
	glVertex3f(1.001f, -0.5f, -0.1f);

	// Left Window Strip Left
	glVertex3f(1.001f, 0.1f, 0.5f);
	glVertex3f(1.001f, 0.1f, 0.6f);
	glVertex3f(1.001f, -0.4f, 0.6f);
	glVertex3f(1.001f, -0.4f, 0.5f);


	// Front Side Windows Strip

	// Top Strip
	glVertex3f(0.4f, 0.2f, 1.001f);
	glVertex3f(-0.4f, 0.2f, 1.001f);
	glVertex3f(-0.4f, 0.1f, 1.001f);
	glVertex3f(0.4f, 0.1f, 1.001f);

	// Right Strip
	glVertex3f(0.4f, 0.1f, 1.001f);
	glVertex3f(0.3f, 0.1f, 1.001f);
	glVertex3f(0.3f, -0.4f, 1.001f);
	glVertex3f(0.4f, -0.4f, 1.001f);

	// Bottom Strip
	glVertex3f(0.4f, -0.4f, 1.001f);
	glVertex3f(-0.4f, -0.4f, 1.001f);
	glVertex3f(-0.4f, -0.5f, 1.001f);
	glVertex3f(0.4f, -0.5f, 1.001f);

	// Left Strip

	glVertex3f(-0.3f, 0.1f, 1.001f);
	glVertex3f(-0.4f, 0.1f, 1.001f);
	glVertex3f(-0.4f, -0.4f, 1.001f);
	glVertex3f(-0.3f, -0.4f, 1.001f);


	// Back Side Windows Strip

	// Top Strip
	glVertex3f(0.4f, 0.2f, -3.001f);
	glVertex3f(-0.4f, 0.2f, -3.001f);
	glVertex3f(-0.4f, 0.1f, -3.001f);
	glVertex3f(0.4f, 0.1f, -3.001f);

	// Right Strip
	glVertex3f(0.4f, 0.1f, -3.001f);
	glVertex3f(0.3f, 0.1f, -3.001f);
	glVertex3f(0.3f, -0.4f, -3.001f);
	glVertex3f(0.4f, -0.4f, -3.001f);

	// Bottom Strip
	glVertex3f(0.4f, -0.4f, -3.001f);
	glVertex3f(-0.4f, -0.4f, -3.001f);
	glVertex3f(-0.4f, -0.5f, -3.001f);
	glVertex3f(0.4f, -0.5f, -3.001f);

	// Left Strip

	glVertex3f(-0.3f, 0.1f, -3.001f);
	glVertex3f(-0.4f, 0.1f, -3.001f);
	glVertex3f(-0.4f, -0.4f, -3.001f);
	glVertex3f(-0.3f, -0.4f, -3.001f);

	// Right Side 

	// Right Window Design (+)
	glVertex3f(1.01f, 0.1f, -2.28f);
	glVertex3f(1.01f, 0.1f, -2.23f);
	glVertex3f(1.01f, -0.4f, -2.23f);
	glVertex3f(1.01f, -0.4f, -2.28f);

	glVertex3f(1.01f, -0.12f, -2.5f);
	glVertex3f(1.01f, -0.12f, -2.0f);
	glVertex3f(1.01f, -0.17f, -2.0f);
	glVertex3f(1.01f, -0.17f, -2.5f);

	// Inner Right Window Design (+)
	glVertex3f(0.99f, 0.1f, -1.28f);
	glVertex3f(0.99f, 0.1f, -1.23f);
	glVertex3f(0.99f, -0.4f, -1.23f);
	glVertex3f(0.99f, -0.4f, -1.28f);

	glVertex3f(0.99f, -0.12f, -1.5f);
	glVertex3f(0.99f, -0.12f, -1.0f);
	glVertex3f(0.99f, -0.17f, -1.0f);
	glVertex3f(0.99f, -0.17f, -1.5f);

	// Left Window Design (+)
	glVertex3f(1.01f, 0.1f, 0.28f);
	glVertex3f(1.01f, 0.1f, 0.23f);
	glVertex3f(1.01f, -0.4f, 0.23f);
	glVertex3f(1.01f, -0.4f, 0.28f);

	glVertex3f(1.01f, -0.12f, 0.0f);
	glVertex3f(1.01f, -0.12f, 0.5f);
	glVertex3f(1.01f, -0.17f, 0.5f);
	glVertex3f(1.01f, -0.17f, 0.0f);

	// Front Side design 
	glVertex3f(0.03f, 0.1f, 1.01f);
	glVertex3f(-0.03f, 0.1f, 1.01f);
	glVertex3f(-0.03f, -0.4f, 1.01f);
	glVertex3f(0.03f, -0.4f, 1.01f);

	glVertex3f(0.3f, -0.13f, 1.01f);
	glVertex3f(-0.3f, -0.13f, 1.01f);
	glVertex3f(-0.3f, -0.18f, 1.01f);
	glVertex3f(0.3f, -0.18f, 1.01f);

	// Back Side design 
	glVertex3f(0.03f, 0.1f, -3.01f);
	glVertex3f(-0.03f, 0.1f, -3.01f);
	glVertex3f(-0.03f, -0.4f, -3.01f);
	glVertex3f(0.03f, -0.4f, -3.01f);

	glVertex3f(0.3f, -0.13f, -3.01f);
	glVertex3f(-0.3f, -0.13f, -3.01f);
	glVertex3f(-0.3f, -0.18f, -3.01f);
	glVertex3f(0.3f, -0.18f, -3.01f);

	// Inner Back
	glVertex3f(0.03f, 0.1f, -2.99f);
	glVertex3f(-0.03f, 0.1f, -2.99f);
	glVertex3f(-0.03f, -0.4f, -2.99f);
	glVertex3f(0.03f, -0.4f, -2.99f);

	glVertex3f(0.3f, -0.13f, -2.99f);
	glVertex3f(-0.3f, -0.13f, -2.99f);
	glVertex3f(-0.3f, -0.18f, -2.99f);
	glVertex3f(0.3f, -0.18f, -2.99f);

	glEnd();

	//	glColor3f(0.25f, 0.066f, 0.0f);
		/*
		glColor3f(0.9215f, 0.9019f, 0.6745f);
		glBegin(GL_LINES);
		// Grass On The Roof
		for (GLint i = 0; i < 260; i++)
		{
			glVertex3f(0.0f, 1.1709f, grassRP);
			glVertex3f(1.29f, 0.4709f, grassRP);
			grassRP = grassRP - 0.015f;
		}
		glEnd();

		glBegin(GL_LINES);
		// Grass On The Roof
		for (GLint i = 0; i < 260; i++)
		{
			glVertex3f(0.0f, 1.1709f, grassLP);
			glVertex3f(-1.29f, 0.4709f, grassLP);
			grassLP = grassLP - 0.015f;
		}
		glEnd();
		*/
}


void home1(void)
{
	cube1();
	woodenPillars1();
	pyramid1();
	beautification1();
}