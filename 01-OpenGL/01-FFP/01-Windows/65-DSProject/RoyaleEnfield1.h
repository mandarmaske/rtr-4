#pragma once
#include <GL/gl.h>
#include "Circle.h"

void humanoid11(void)
{
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, -4.1f);
	glScalef(1.0f, 0.98f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluSphere(quadric, 0.3f, 50, 5);
	glPopMatrix();

	// Hair
	glPushMatrix();
	glTranslatef(0.0f, 1.12f, -4.1f);
	glScalef(1.1f, 1.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.28f, 50, 5);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.99f, -4.15f);
	glScalef(1.1f, 1.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.28f, 50, 5);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.68f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.6470f, 0.1647f, 0.1647f);
	gluCylinder(quadric, 0.12f, 0.14f, 0.13f, 50, 50);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.72f, -4.1f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluCylinder(quadric, 0.125f, 0.12f, 0.1f, 50, 50);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.55f, -4.1f);
	glScalef(1.0f, 1.0f, 0.5f);
	glColor3f(0.6470f, 0.1647f, 0.1647f);
	glRotatef(95.0f, 1.0f, 0.0f, 0.0f);
	gluDisk(quadric, 0.0f, 0.5f, 50, 50);
	gluCylinder(quadric, 0.5f, 0.45f, 0.9f, 50, 50);
	glTranslatef(0.0f, 0.0f, 0.9f);
	gluDisk(quadric, 0.0f, 0.45f, 50, 50);
	glPopMatrix();

	// Right Arm
	glPushMatrix();
	glTranslatef(-0.45f, 0.4f, -4.1f);
	glRotatef(43.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.6470f, 0.1647f, 0.1647f);
	gluCylinder(quadric, 0.1f, 0.1f, 0.78f, 50, 50);
	glPopMatrix();

	// Right Elbow
	glPushMatrix();
	glTranslatef(0.49f, 0.05f, -3.555f);
	glRotatef(15.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluCylinder(quadric, 0.1f, 0.1f, 0.71f, 50, 50);
	glPopMatrix();

	// Right Palm
	glPushMatrix();
	glTranslatef(-0.3f, -0.3f, -2.7f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.9098f, 0.7450f, 0.6745f);
	gluSphere(quadric, 0.15f, 50, 20);
	glPopMatrix();

	// Right Leg
	glPushMatrix();
	glTranslatef(0.225f, -0.5f, -4.3f);
	glRotatef(20.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.4588f, 0.5058f, 0.4196f);
	gluCylinder(quadric, 0.15f, 0.15f, 0.9f, 50, 50);
	glPopMatrix();

	// Right Leg Bottom Part
	glPushMatrix();
	glTranslatef(0.17f, -0.8f, -3.5f);
	glRotatef(80.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(0.4588f, 0.5058f, 0.4196f);
	gluCylinder(quadric, 0.15f, 0.15f, 0.8f, 50, 50);
	glPopMatrix();

	// Right Leg Shoes
	glPushMatrix();
	glTranslatef(0.23f, -1.76f, -3.25f);
	glRotatef(-20.0f, 1.0f, 0.0f, 0.0f);
	glScalef(0.8f, 0.6f, 1.3f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_POLYGON);
	glColor3f(0.5294f, 0.2274f, 0.2117f);
	gluSphere(quadric, 0.21f, 50, 20);
	glPopMatrix();
}

void royalEnfiled1(void)
{
	glPushMatrix();
	glTranslatef(0.0f, -4.5f, -15.0f);
	glScalef(0.5f, 0.5f, 0.5f);
	glBegin(GL_QUADS);

	glColor3f(0.8f, 0.0f, 0.0f);
	// Bike Tank
	glVertex3f(-1.0f, 2.8f, 0.0f);
	glVertex3f(-3.0f, 2.7f, 0.0f);
	glVertex3f(-3.17f, 2.5f, 0.0f);
	glVertex3f(-0.6f, 2.5f, 0.0f);

	glVertex3f(-0.6f, 2.5f, 0.0f);
	glVertex3f(-3.17f, 2.5f, 0.0f);
	glVertex3f(-3.37f, 1.8f, 0.0f);
	glVertex3f(0.0f, 1.8f, 0.0f);

	glVertex3f(0.0f, 1.8f, 0.0f);
	glVertex3f(-3.37f, 1.8f, 0.0f);
	glVertex3f(-3.27f, 1.6f, 0.0f);
	glVertex3f(-0.1f, 1.6f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	// HeadLight
	glVertex3f(-3.21f, 2.6f, 0.0f);
	glVertex3f(-4.67f, 2.6f, 0.0f);
	glVertex3f(-4.67f, 1.8f, 0.0f);
	glVertex3f(-3.47f, 1.8f, 0.0f);

	// Bike Tank Behind Part
	glVertex3f(-0.0f, 2.6f, 0.0f);
	glVertex3f(-0.65f, 2.6f, 0.0f);
	glVertex3f(0.07f, 1.8f, 0.0f);
	glVertex3f(0.7f, 1.8f, 0.0f);

	glVertex3f(0.7f, 1.8f, 0.0f);
	glVertex3f(0.07f, 1.8f, 0.0f);
	glVertex3f(-0.05f, 1.6f, 0.0f);
	glVertex3f(0.6f, 1.6f, 0.0f);

	// Seat
	glColor3f(0.5450f, 0.2705f, 0.0745f);
	glVertex3f(2.7f, 1.8f, 0.0f);
	glVertex3f(0.75f, 1.8f, 0.0f);
	glVertex3f(0.65f, 1.6f, 0.0f);
	glVertex3f(2.8f, 1.6f, 0.0f);

	// Seat Back Cushion
	glVertex3f(2.95f, 2.1f, 0.0f);
	glVertex3f(2.73f, 1.8f, 0.0f);
	glVertex3f(2.83f, 1.6f, 0.0f);
	glVertex3f(3.2f, 2.1f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	// Back Metal Part
	glVertex3f(5.2f, 2.1f, 0.0f);
	glVertex3f(3.4f, 2.3f, 0.0f);
	glVertex3f(2.88f, 1.6f, 0.0f);
	glVertex3f(5.8f, 1.6f, 0.0f);

	// Middle Metal Part Below Seat
	glVertex3f(2.88f, 1.55f, 0.0f);
	glVertex3f(-0.05f, 1.55f, 0.0f);
	glVertex3f(1.0f, 0.15f, 0.0f);
	glVertex3f(4.0f, 0.15f, 0.0f);

	// After Tires Shapes
	glVertex3f(-2.47f, 1.4f, 0.0f);
	glVertex3f(-3.37f, 1.4f, 0.0f);
	glVertex3f(-2.27f, -1.35f, 0.0f);
	glVertex3f(-1.55f, -0.1f, 0.0f);

	// Middle Part Trapezoid Shape
	glVertex3f(1.0f, 0.12f, 0.0f);
	glVertex3f(-1.55f, 0.12f, 0.0f);
	glVertex3f(-2.3f, -1.4f, 0.0f);
	glVertex3f(2.8f, -1.4f, 0.0f);

	// Handles
	glVertex3f(-3.21f, 3.5f, 0.0f);
	glVertex3f(-3.71f, 2.7f, 0.0f);
	glVertex3f(-3.31f, 2.6f, 0.0f);
	glVertex3f(-2.99f, 3.2f, 0.0f);

	glVertex3f(-2.81f, 3.57f, 0.0f);
	glVertex3f(-3.31f, 3.55f, 0.0f);
	glVertex3f(-2.99f, 3.1f, 0.0f);
	glVertex3f(-2.69f, 3.2f, 0.0f);

	glVertex3f(-2.11f, 3.6f, 0.0f);
	glVertex3f(-2.81f, 3.57f, 0.0f);
	glVertex3f(-2.69f, 3.2f, 0.0f);
	glVertex3f(-2.11f, 3.4f, 0.0f);

	/*
	glVertex3f(-3.91f, 2.8f, 0.0f);
	glVertex3f(-4.61f, 2.9f, 0.0f);
	glVertex3f(-4.61f, 2.8f, 0.0f);
	glVertex3f(-4.01f, 2.6f, 0.0f);
	*/

	glColor3f(0.7568f, 0.7647f, 0.7686f);
	// Steel after tire shapes
	glVertex3f(-2.6f, 0.9f, 0.0f);
	glVertex3f(-2.9f, 0.7f, 0.0f);
	glVertex3f(-2.8f, 0.45f, 0.0f);
	glVertex3f(-2.45f, 0.6f, 0.0f);

	glVertex3f(-2.55f, 0.6f, 0.0f);
	glVertex3f(-2.8f, 0.45f, 0.0f);
	glVertex3f(-1.9f, -1.1f, 0.0f);
	glVertex3f(-1.7f, -0.9f, 0.0f);

	glVertex3f(-2.65f, 1.6f, 0.0f);
	glVertex3f(-2.9f, 1.6f, 0.0f);
	glVertex3f(-2.8f, 1.4f, 0.0f);
	glVertex3f(-2.55f, 1.4f, 0.0f);

	// Back Tire Attaching Part
	glVertex3f(1.12f, 0.0f, 0.0f);
	glVertex3f(1.54f, -0.4f, 0.0f);
	glVertex3f(4.6f, -0.8f, 0.0f);
	glVertex3f(4.8f, -0.6f, 0.0f);

	// Engine
	glVertex3f(-0.2f, 1.5f, 0.0f);
	glVertex3f(-1.9f, 1.5f, 0.0f);
	glVertex3f(-1.5f, 0.3f, 0.0f);
	glVertex3f(-0.5f, 0.3f, 0.0f);

	// Shock Absorbers
	glVertex3f(-3.61f, 1.8f, 0.0f);
	glVertex3f(-4.01f, 1.8f, 0.0f);
	glVertex3f(-4.81f, -0.8f, 0.0f);
	glVertex3f(-4.41f, -0.8f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.93f, 2.8f, 0.0f);
	glVertex3f(-0.65f, 2.6f, 0.0f);
	glVertex3f(-0.0f, 2.6f, 0.0f);

	glVertex3f(2.88f, 1.6f, 0.0f);
	glVertex3f(5.8f, 1.6f, 0.0f);
	glVertex3f(4.0f, 0.2f, 0.0f);


	// Bike Handles
	glVertex3f(-3.91f, 2.8f, 0.0f);
	glVertex3f(-4.01f, 2.6f, 0.0f);
	glVertex3f(-3.21f, 2.6f, 0.0f);


	glEnd();

	glLineWidth(2);
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-3.37f, 1.8f, 0.0f);
	glVertex3f(0.0f, 1.8f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(2.88f, 1.6f, 0.0f);
	glVertex3f(5.8f, 1.6f, 0.0f);

	glVertex3f(-0.2f, 1.5f, 0.0f);
	glVertex3f(-1.9f, 1.5f, 0.0f);

	glVertex3f(-0.26f, 1.3f, 0.0f);
	glVertex3f(-1.85f, 1.3f, 0.0f);

	glVertex3f(-0.3f, 1.1f, 0.0f);
	glVertex3f(-1.78f, 1.1f, 0.0f);

	glVertex3f(-0.34f, 0.9f, 0.0f);
	glVertex3f(-1.74f, 0.9f, 0.0f);

	glVertex3f(-0.38f, 0.7f, 0.0f);
	glVertex3f(-1.69f, 0.7f, 0.0f);

	glVertex3f(-0.46f, 0.5f, 0.0f);
	glVertex3f(-1.60f, 0.5f, 0.0f);

	glEnd();

	MDM_DrawFilledCircle(0.3f, 0.3f, -0.5f, -1.1f);
	MDM_DrawFilledCircle(1.2, 0.6, -0.6f, -0.2f);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-3.4f, -1.5f, -9.0f);
	glScalef(1.0f, 0.8f, 0.8f);
	glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
	humanoid11();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.1f, -4.5f, -15.5f);
	glScalef(0.5f, 0.5f, 0.5f);
	glColor3f(0.2784f, 0.2745f, 0.29411f);
	MDM_DrawFilledCircle(1.85f, 1.85f, -4.81f, -0.8f);
	MDM_DrawFilledCircle(1.85f, 1.85f, 4.81f, -0.8f);

	glColor3f(0.4431f, 0.4745f, 0.4901f);
	MDM_DrawFilledCircle(1.35f, 1.35f, -4.81f, -0.8f);
	MDM_DrawFilledCircle(1.35f, 1.35f, 4.81f, -0.8f);

	glBegin(GL_QUADS);
	glColor3f(0.15f, 0.15f, 0.15f);
	glVertex3f(7.7f, -2.65f, 0.0f);
	glVertex3f(-4.81f, -2.65f, 0.0f);
	glVertex3f(-4.81f, -2.8f, 0.0f);
	glVertex3f(7.7f, -2.8f, 0.0f);

	glEnd();

	glPopMatrix();
}