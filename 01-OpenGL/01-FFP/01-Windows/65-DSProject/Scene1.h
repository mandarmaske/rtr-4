#pragma once
#include <GL/gl.h>

// Scene1 camera variables
float ex1 = 3.0f;
float ey1 = 33.0f;
float ez1 = 16.0f;
float cx1 = 0.0f;
float cy1 = -120.0f;
float cz1 = 0.0f;

void scene1(void)
{
	glTranslatef(0.0f, 0.0f, -6.0f);

	gluLookAt(ex1, ey1, ez1, cx1, cy1, cz1 - 250, 0.0f, 1.0f, 0.0f);

	glPushMatrix();
	glTranslatef(0.0f, 20.06f, -50.0f);
	//glTranslatef(tx, ty, tz);
	GroundSky(50.0f, 50.0f, 100.0f);
	glPopMatrix();

	glPushMatrix();

	glTranslatef(-3.2f, 0.5f, -48.0f);
	glRotatef(-90, 0.0f, 1.0f, 0.0f);
	glScalef(7.0f, 5.5f, 6.0f);

	home();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(3.0f, -2.5f, -10.0f);
	glScalef(2.0f, 2.0f, 2.0f);
	fences();
	glPopMatrix();
}