#pragma once
#include <GL/gl.h>

GLuint texture_tv;
GLuint texture_furniture;

void tv(void)
{
	glTranslatef(0.0f, 0.0f, -2.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_tv);
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(0.55f, 0.45f, -0.1f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-0.55f, 0.45f, -0.1f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-0.55f, -0.25f, -0.1f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(0.55f, -0.25f, -0.1f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_furniture);
	// ************* Furniture TV
	glBegin(GL_QUADS);

	// Front 
	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(0.7f, -0.3f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-0.7f, -0.3f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-0.7f, -0.7f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(0.7f, -0.7f, 0.0f);

	// Right
	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(0.7f, -0.3f, -0.5f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(0.7f, -0.3f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(0.7f, -0.7f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(0.7f, -0.7f, -0.5f);

	// Back
	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(0.7f, -0.3f, -0.5f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-0.7f, -0.3f, -0.5f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-0.7f, -0.7f, -0.5f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(0.7f, -0.7f, -0.5f);

	// Left
	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(-0.7f, -0.3f, -0.5f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-0.7f, -0.3f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-0.7f, -0.7f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(-0.7f, -0.7f, -0.5f);

	// Top
	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(-0.7f, -0.3f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(0.7f, -0.3f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(0.7f, -0.3f, -0.5f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(-0.7f, -0.3f, -0.5f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);

	// ********* Screen - Front Face

	// Top
	glVertex3f(0.55f, 0.5f, -0.1f);
	glVertex3f(-0.55f, 0.5f, -0.1f);
	glVertex3f(-0.55f, 0.45f, -0.1f);
	glVertex3f(0.55f, 0.45f, -0.1f);

	// Bottom
	glVertex3f(0.55f, -0.25f, -0.1f);
	glVertex3f(-0.55f, -0.25f, -0.1f);
	glVertex3f(-0.55f, -0.3f, -0.1f);
	glVertex3f(0.55f, -0.3f, -0.1f);

	// Right
	glVertex3f(0.6f, 0.5f, -0.1f);
	glVertex3f(0.55f, 0.5f, -0.1f);
	glVertex3f(0.55f, -0.3f, -0.1f);
	glVertex3f(0.6f, -0.3f, -0.1f);

	// Left

	// Right
	glVertex3f(-0.6f, 0.5f, -0.1f);
	glVertex3f(-0.55f, 0.5f, -0.1f);
	glVertex3f(-0.55f, -0.3f, -0.1f);
	glVertex3f(-0.6f, -0.3f, -0.1f);

	// ******* Body ************

	// Top
	glVertex3f(-0.6f, 0.5f, -0.1f);
	glVertex3f(0.6f, 0.5f, -0.1f);
	glVertex3f(0.6f, 0.5f, -0.4f);
	glVertex3f(-0.6f, 0.5f, -0.4f);

	// Bottom
	// Top
	glVertex3f(-0.6f, -0.3f, -0.1f);
	glVertex3f(0.6f, -0.3f, -0.1f);
	glVertex3f(0.6f, -0.3f, -0.4f);
	glVertex3f(-0.6f, -0.3f, -0.4f);

	// Right Face
	glVertex3f(0.6f, 0.5f, -0.4f);
	glVertex3f(0.6f, 0.5f, -0.1f);
	glVertex3f(0.6f, -0.3f, -0.1f);
	glVertex3f(0.6f, -0.3f, -0.4f);

	// Back Face
	glVertex3f(0.6f, 0.5f, -0.4f);
	glVertex3f(-0.6f, 0.5f, -0.4f);
	glVertex3f(-0.6f, -0.3f, -0.4f);
	glVertex3f(0.6f, -0.3f, -0.4f);

	// Left
	glVertex3f(-0.6f, 0.5f, -0.4f);
	glVertex3f(-0.6f, 0.5f, -0.1f);
	glVertex3f(-0.6f, -0.3f, -0.1f);
	glVertex3f(-0.6f, -0.3f, -0.4f);

	glEnd();

}
