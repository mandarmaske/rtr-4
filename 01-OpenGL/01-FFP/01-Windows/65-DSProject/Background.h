#pragma once
#include <GL/gl.h>
#include <GL/glu.h>

GLuint texture_left;
GLuint texture_back;
GLuint texture_right;
GLuint texture_front;
GLuint texture_top;
GLuint texture_bottom;
GLuint texture;

void GroundSky(GLfloat scalex, GLfloat scaley, GLfloat scalez)
{
	glScalef(scalex, scaley, scalez);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTranslatef(0.0, 0.5, 0.0);

	glColor3f(1.0f, 1.0f, 1.0f);
	
	glBegin(GL_QUADS);

	//Front Face
	//Right top
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.5f, 1.0f, 1.0f);
	//left top
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.5f, 1.0f, 1.0f);
	//left bottom
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.5f, -1.0f, 1.0f);
	//Right Bottom
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.5f, -1.0f, 1.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_QUADS);
	//Right Face
	//Right Top
	//glTranslatef(0.5f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.5f, 1.0f, -1.0f);
	//Left top
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.5f, 1.0f, 1.0f);
	//left bottom
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.5f, -1.0f, 1.0f);
	//Right Bottom
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.5f, -1.0f, -1.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_QUADS);
	//Back face
	//right top
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.5f, 1.0f, -1.0f);
	//Left Top
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.5f, 1.0f, -1.0f);
	//Left Bottom
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.5f, -1.0f, -1.0f);
	//Right Bottom
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.5f, -1.0f, -1.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_QUADS);
	//Left Face
	//Right Top
	//glTranslatef(-0.5f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.5f, 1.0f, 1.0f);
	//Left top
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.5f, 1.0f, -1.0f);
	//Left bottom
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.5f, -1.0f, -1.0f);
	//right Bottom
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.5f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glBindTexture(GL_TEXTURE_2D, texture);
	
	glBegin(GL_QUADS);
	//Top Surface
	//Right Top
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.5f, 1.0f, -1.0f);
	//Left top
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.5f, 1.0f, -1.0f);
	//Left bottom
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.5f, 1.0f, 1.0f);
	// Right bottom
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.5f, 1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glBindTexture(GL_TEXTURE_2D, texture_bottom);
	glColor3f(0.5f, 0.5f, 0.5f);
	glBegin(GL_QUADS);
	//Bottom Surface
	//Right Top
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.5f, -1.0f, -1.0f);
	//Left top
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.5f, -1.0f, -1.0f);
	//Left Bottom
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.5f, -1.0f, 1.0f);
	//Right Bottom
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.5f, -1.0f, 1.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}