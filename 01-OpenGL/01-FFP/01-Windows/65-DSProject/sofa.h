#pragma once
#include <GL/gl.h>

GLuint texture_sofa;

void sofa(void)
{
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_sofa);
	// *********** Left Side Cushion Stand ****************
	glBegin(GL_QUADS);

	// Front Face
	glTexCoord2f(0.5f, 0.5f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.5f);
	glVertex3f(-0.6f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.6f, -0.5f, 0.0f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 0.0f);

	// Right Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.4f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, -0.6f);

	// Back Face
	glTexCoord2f(0.5f, 0.5f);
	glVertex3f(-0.4f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 0.5f);
	glVertex3f(-0.6f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.6f, -0.5f, -0.6f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(-0.4f, -0.5f, -0.6f);

	// Right Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.6f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.6f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.6f, -0.5f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.6f, -0.5f, -0.6f);

	// Top Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.6f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, 0.2f, -0.6f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.6f, 0.2f, -0.6f);

	// ******** Right Side Cushion Stand ***********
	// Front Face
	glTexCoord2f(0.5f, 0.5f);
	glVertex3f(0.4f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.5f);
	glVertex3f(0.6f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.6f, -0.5f, 0.0f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(0.4f, -0.5f, 0.0f);

	// Right Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.4f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.4f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, -0.6f);

	// Back Face
	glTexCoord2f(0.5f, 0.5f);
	glVertex3f(0.4f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 0.5f);
	glVertex3f(0.6f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.6f, -0.5f, -0.6f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(0.4f, -0.5f, -0.6f);

	// Right Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.6f, 0.2f, -0.6f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.6f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.6f, -0.5f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.6f, -0.5f, -0.6f);

	// Top Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.6f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.4f, 0.2f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.4f, 0.2f, -0.6f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.6f, 0.2f, -0.6f);

	glEnd();

	glBegin(GL_QUADS);
	// ************ Middle cushion horizontal **********

	// Front Face 
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, 0.0f);

	// Top Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.4f, 0.0f, -0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.4f, 0.0f, -0.5f);

	// *********** Back Sofa Cushion
	// Front Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.4f, 0.7f, -0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.4f, 0.7f, -0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, -0.5f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, -0.5f);

	// Right Face
	glTexCoord2f(0.5f, 0.5f);
	glVertex3f(0.4f, 0.7f, -0.65f);
	glTexCoord2f(0.0f, 0.5f);
	glVertex3f(0.4f, 0.7f, -0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, -0.5f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(0.4f, -0.5f, -0.65f);

	// Back Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.4f, 0.7f, -0.65f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.4f, 0.7f, -0.65f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, -0.65f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, -0.65f);

	// Left Face
	glTexCoord2f(0.5f, 0.5f);
	glVertex3f(-0.4f, 0.7f, -0.65f);
	glTexCoord2f(0.0f, 0.5f);
	glVertex3f(-0.4f, 0.7f, -0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.4f, -0.5f, -0.5f);
	glTexCoord2f(0.5f, 0.0f);
	glVertex3f(-0.4f, -0.5f, -0.65f);

	// Top Face
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-0.4f, 0.7f, -0.5f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(0.4f, 0.7f, -0.5f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(0.4f, 0.7f, -0.65f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-0.4f, 0.7f, -0.65f);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}