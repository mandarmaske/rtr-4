// header file
#include <Windows.h>
#include <stdio.h> // For File I/O (fopen(), fclose(), fprintf())
#include <stdlib.h> // For exit()
#define STB_IMAGE_IMPLEMENTATION
#include"stb_image.h"
#include "OGL.h" // our header file
#include "Background.h"
#include "home.h"
#include "Fences.h"

char str[255];
// variable to create sphere, cylinders
GLUquadric* quadric = NULL;

#include "RoyaleEnfield.h"
#include "RoyaleEnfield1.h"
#include "RoyaleEnfield2.h"
#include "Scene1.h"
#include "Scene2.h"
#include "Scene3.h"
#include "Scene4.h"
#include "Scene5.h"
#include "Scene6.h"
#include "Scene7.h"
#include "Scene8.h"
#include "TextureFunc.h"
#include <math.h>

// OpenGL header files
#include <GL/gl.h>
#include <GL/glu.h> 

// macro definatio
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

GLuint texture_1;
GLuint texture_2;
GLuint nFontList;

float blend1 = 0.0f;
float blend2 = 1.0f;
float blend3 = 0.0f;
float blend4 = 1.0f;

float tx5 = 0.0f;
float ty5 = -4.0f;
float tz5 = -6.0f;

BOOL bgFlag = FALSE;

// OpenGL libraries
#pragma comment(lib, "OpenGL32.lib") // can also use for  user32.lib, gdi32.lib, kernel32.lib, if given here then no need write in link command
#pragma comment(lib, "glu32.lib") 

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
BOOL gbActiveWindow = FALSE; // by default aapli window active nahi ahe
FILE* gpFile = NULL;
BOOL gbFullScreen = FALSE;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL; // H - HANDLE, GL - OpenGL, RC - Rendering Context

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations -> not important in sequence as prototype but it is defined in which one gets called first
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iretVal = 0;

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) // if(file open secured() != 0)
	// aadhi fopen hota nusta
	{
		MessageBox(NULL, TEXT("Creation Of Log File Failed. Exitting..."), TEXT("File I/O Error"), MB_OK); // 1st parameter is NULL because we dont have our hwnd yet, so OS la ticha handle magitla message dakhvnyasathi ani aapan NULL/HWND_DESKTOP dila
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n"); // printf prints on console. fprintf prints in file
	}

	// initilizing WNDCLASSEX wndclass structure members
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // change
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); 
	wndclass.hInstance = hInstance;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Registering wndclass in OS's Class Registry
	RegisterClassEx(&wndclass);

	// creating our window in memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("OpenGL Window - Mandar Maske RTR2021-116"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // change
						((GetSystemMetrics(SM_CXSCREEN) - WIN_WIDTH) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - WIN_HEIGHT) / 2),
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	if (hwnd == NULL)
	{
		fprintf(gpFile, "hwnd Failed !!!\n\n");
	}
	else
	{
		fprintf(gpFile, "hwnd Created Successfully\n\n");
	}

	ghwnd = hwnd;

	// initialize
	iretVal = initialize();

	PlaySound("Song.wav", NULL, SND_ASYNC | SND_FILENAME);

	if (iretVal == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n\n");
		uninitialize();
	}
	else if (iretVal == -2)
	{
		fprintf(gpFile, "SetPixelFormat is Failed\n\n");
		uninitialize();
	}
	else if (iretVal == -3)
	{
		fprintf(gpFile, "CreateOpenGlContext is Failed\n\n");
		uninitialize();
	}

	else if (iretVal == -4)
	{
		fprintf(gpFile, "Making OpenGL Context As Current Context Failed\n\n");
		uninitialize();
	}

	else if (iretVal == -5)
	{
		fprintf(gpFile, "Left texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -6)
	{
		fprintf(gpFile, "Back texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -7)
	{
		fprintf(gpFile, "Right texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -8)
	{
		fprintf(gpFile, "Front texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -9)
	{
		fprintf(gpFile, "Top texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -10)
	{
		fprintf(gpFile, "Bottom texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -11)
	{
		fprintf(gpFile, "texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -12)
	{
		fprintf(gpFile, "Roof texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -13)
	{
		fprintf(gpFile, "tv texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -14)
	{
		fprintf(gpFile, "furniture texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -15)
	{
		fprintf(gpFile, "sofa texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -16)
	{
		fprintf(gpFile, "roof1 texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -17)
	{
		fprintf(gpFile, "grass1 texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -18)
	{
		fprintf(gpFile, "scenario1 texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -19)
	{
		fprintf(gpFile, "grass2 texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -20)
	{
		fprintf(gpFile, "scenario2 texture is failed to load\n\n");
		uninitialize();
	}

	else if (iretVal == -21)
	{
		fprintf(gpFile, "Desert texture Failed\n\n");
		uninitialize();
	}

	else if (iretVal == -22)
	{
		fprintf(gpFile, "Scenario3 texture Failed\n\n");
		uninitialize();
	}

	if (iretVal == -23)
	{
		fprintf(gpFile, "Snow Texture Successfully not Created\n");
		uninitialize();
	}

	if (iretVal == -24)
	{
		fprintf(gpFile, "HeightMap Failed\n");
		uninitialize();
	}

	if (iretVal == -25)
	{
		fprintf(gpFile, "1 Texture Successfully not Created\n");
		uninitialize();
	}

	if (iretVal == -26)
	{
		fprintf(gpFile, "2 Texture Successfully not Created\n");
		uninitialize();
	}

	else
	{
		fprintf(gpFile, "Initialize() Successful\n\n");
	}

	// Showing Window On Screen
	ShowWindow(hwnd, iCmdShow);

	// foregrounding(Z Order - Top) and focusing the window
	SetForegroundWindow(hwnd);

	SetFocus(hwnd); // sends message to our Window(wndproc) WM_SETFOCUS

	// Game Loop -> glutMainLoop() in glut
	while (bDone == FALSE) //  while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) // manually loop false kela
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else // Kontahi message nasel tevha ha else block run hoil
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}

	uninitialize(); // indra and takshaq saap

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void resize(int, int);

	// variable declarations
	int retVal;

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:

		switch (wParam)
		{
		case 27:
			retVal = MessageBox(hwnd, TEXT("Are You Sure, You Want To Exit ?"), TEXT("Exit Message"), MB_YESNO);
			if (retVal == IDNO)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), No Button Is Pressed\n\n");
				break;
			}
				
			else if(retVal == IDYES)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), Yes Button Is Pressed. Program Is Now Exitting !!!\n\n");
				DestroyWindow(hwnd);
				break;
			}

		default:
			break;
		}
	break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		case 'X':
			ex2 = ex2 + 1.0f;
			break;

		case 'x':
			ex2 = ex2 - 1.0f;
			break;

		case 'Y':
			ey2 = ey2 + 1.0f;
			break;

		case 'y':
			ey2 = ey2 - 1.0f;
			break;

		case 'Z':
			ez2 = ez2 + 1.0f;
			break;

		case 'z':
			ez2 = ez2 - 1.0f;
			break;

		case 'A':
			cx2 = cx2 + 15.0f;
			break;

		case 'a':
			cx2 = cx2 - 15.0f;
			break;

		case 'W':
			cy2 = cy2 + 3.0f;
			break;

		case 'w':
			cy2 = cy2 - 1.0f;
			break;

		case 'S':
			cz2 = cz2 + 15.0f;
			break;

		case 's':
			cz2 = cz2 - 15.0f;
			break;

		default:
			break;
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // resize(width, height)
		break;

	case WM_CLOSE: // close button - discipline mhanun lihane, WM_CLOSE cha call WM_DESTROY chya aadhi yeto
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle; // static to sustain values accross this function
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT); // like wndclass.cbSize =  sizeof(WNDCLASSEX)

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) // if (1 & WS_OVERLAPPEDWINDOW) -> if (1 & 1) then if ya madhe
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) // if(1 && 1) then in if
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW); // WS_OVERLAPPEDWINDOW che 5 style(WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_THICKFRAME,  WS_SYSMENU, WS_CAPTION) nako except WS_OVERLAPPED 
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW); // kadhayla ~WS_OVERLAPPED kela, Add karnyasathi Bitwise Or Waparala
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED); // x, y, width, height ignore -> Zero Why? - wp ne already SetWindowPlacement pahije tithe aaplya window la place kelay so 0 

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	void SetupRC(HDC hDC);
	BOOL LoadGLTexture(GLuint*, TCHAR[]);
	void initializeTerrian(void);
	BOOL loadHeightMapData(char* filename);
	
	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialization of PIXELFORMATDESCRIPTOR structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // 24 bit also can be done 

	// GetDC
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	// set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return(-2);

	// Create OpenGL Rendering Context - first bridging API
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return(-3);

	// make the rendering context as the current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE) // wglMakeCurrent() - 2nd bridging API
		return(-4);

	SetupRC(ghdc);

	// Here Starts OpenGL Code...
	// clear the screen using black color

	// Depth Related Changes 1 ***********************************************************
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // change 2

	if (LoadGLTexture(&texture_left, MAKEINTRESOURCE(ID_BITMAP_LEFT)) == FALSE)
		return(-5);

	if (LoadGLTexture(&texture_back, MAKEINTRESOURCE(ID_BITMAP_BACK)) == FALSE)
		return(-6);

	if (LoadGLTexture(&texture_right, MAKEINTRESOURCE(ID_BITMAP_RIGHT)) == FALSE)
		return(-7);

	if (LoadGLTexture(&texture_front, MAKEINTRESOURCE(ID_BITMAP_FRONT)) == FALSE)
		return(-8);

	if (LoadGLTexture(&texture_top, MAKEINTRESOURCE(ID_BITMAP_TOP)) == FALSE)
		return(-9);

	if (LoadGLTexture(&texture_bottom, MAKEINTRESOURCE(ID_BITMAP_BOTTOM)) == FALSE)
		return(-10);

	if (LoadGLTexture(&texture, MAKEINTRESOURCE(ID_BITMAP_TEXTURE)) == FALSE)
		return(-11);

	if (LoadGLTexture(&texture_roof, MAKEINTRESOURCE(ID_BITMAP_ROOF)) == FALSE)
		return(-12);

	if (LoadGLTexture(&texture_tv, MAKEINTRESOURCE(ID_BITMAP_TV)) == FALSE)
		return(-13);

	if (LoadGLTexture(&texture_furniture, MAKEINTRESOURCE(ID_BITMAP_FURNITURE)) == FALSE)
		return(-14);

	if (LoadGLTexture(&texture_sofa, MAKEINTRESOURCE(ID_BITMAP_SOFA)) == FALSE)
		return(-15);

	if (LoadGLTexture(&texture_roof1, MAKEINTRESOURCE(ID_BITMAP_ROOF1)) == FALSE)
		return(-16);

	if (LoadGLTexture(&texture_grass1, MAKEINTRESOURCE(ID_BITMAP_GRASS1)) == FALSE)
		return(-17);

	if (LoadGLTexture(&texture_scenario1, MAKEINTRESOURCE(ID_BITMAP_SCENARIO1)) == FALSE)
		return(-18);

	if (LoadGLTexture(&texture_grass2, MAKEINTRESOURCE(ID_BITMAP_GRASS2)) == FALSE)
		return(-19);

	if (LoadGLTexture(&texture_scenario2, MAKEINTRESOURCE(ID_BITMAP_SCENARIO2)) == FALSE)
		return(-20);

	if (LoadGLTexture(&texture_desert, MAKEINTRESOURCE(ID_BITMAP_DESERT)) == FALSE)
		return(-21);

	if (LoadGLTexture(&texture_scenario3, MAKEINTRESOURCE(ID_BITMAP_SCENARIO3)) == FALSE)
		return(-22);

	if (LoadGLTexture(&texture_snow, MAKEINTRESOURCE(ID_BITMAP_SNOW)) == FALSE)
		return(-23);

	if (loadHeightMapData("Terrain.bmp") == FALSE)
	{
		return (-24);
	}

	if (LoadGLTexture(&texture_1, MAKEINTRESOURCE(ID_BITMAP_1)) == FALSE)
		return(-25);

	if (LoadGLTexture(&texture_2, MAKEINTRESOURCE(ID_BITMAP_2)) == FALSE)
		return(-26);

	if (LoadGLTexture(&texture_snow1, MAKEINTRESOURCE(ID_BITMAP_SNOW1)) == FALSE)
		return(-27);

	if (LoadGLTexture(&texture_scenario4, MAKEINTRESOURCE(ID_BITMAP_SCENARIO4)) == FALSE)
		return(-28);

	if (LoadGLTexture(&texture_111, MAKEINTRESOURCE(ID_BITMAP_111)) == FALSE)
		return(-29);

	if (LoadGLTexture(&texture_22, MAKEINTRESOURCE(ID_BITMAP_22)) == FALSE)
		return(-30);

	if (LoadGLTexture(&texture_3, MAKEINTRESOURCE(ID_BITMAP_3)) == FALSE)
		return(-31);

	if (LoadGLTexture(&texture_4, MAKEINTRESOURCE(ID_BITMAP_4)) == FALSE)
		return(-32);

	if (LoadGLTexture(&texture_5, MAKEINTRESOURCE(ID_BITMAP_5)) == FALSE)
		return(-33);

	if (LoadGLTexture(&texture_6, MAKEINTRESOURCE(ID_BITMAP_6)) == FALSE)
		return(-34);

	if (LoadGLTexture(&texture_7, MAKEINTRESOURCE(ID_BITMAP_7)) == FALSE)
		return(-35);

	if (LoadGLTexture(&texture_8, MAKEINTRESOURCE(ID_BITMAP_8)) == FALSE)
		return(-36);

	if (LoadGLTexture(&texture_9, MAKEINTRESOURCE(ID_BITMAP_9)) == FALSE)
		return(-37);

	if (LoadGLTexture(&texture_10, MAKEINTRESOURCE(ID_BITMAP_10)) == FALSE)
		return(-38);

	if (LoadGLTexture(&texture_11, MAKEINTRESOURCE(ID_BITMAP_11)) == FALSE)
		return(-39);

	initializeTerrian();

	// Create Quadric
	quadric = gluNewQuadric();

	// enabling the texture
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// warm up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void SetupRC(HDC hDC)
{
	// Set up the font characteristics
	HFONT hFont;
	GLYPHMETRICSFLOAT agmf[128]; // Throw away
	LOGFONT logfont;
	logfont.lfHeight = -10;
	logfont.lfWidth = 0;
	logfont.lfEscapement = 0;
	logfont.lfOrientation = 0;
	logfont.lfWeight = FW_BOLD;
	logfont.lfItalic = FALSE;
	logfont.lfUnderline = FALSE;
	logfont.lfStrikeOut = FALSE;
	logfont.lfCharSet = ANSI_CHARSET;
	logfont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logfont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logfont.lfQuality = DEFAULT_QUALITY;
	logfont.lfPitchAndFamily = DEFAULT_PITCH;
	//strcpy(logfont.lfFaceName, "Arial");
	strcpy(logfont.lfFaceName, "News Gothic MT");

	// Create the font and display list
	hFont = CreateFontIndirect(&logfont);
	SelectObject(hDC, hFont);
	// Create display lists for glyphs 0 through 128 with 0.1 extrusion
	// and default deviation. The display list numbering starts at 1000
	// (it could be any number).
	nFontList = glGenLists(128);
	wglUseFontOutlines(hDC, 0, 128, nFontList, 0.0f, 1.0f, WGL_FONT_POLYGONS, agmf);
	DeleteObject(hFont);
}

void initializeTerrian(void)
{
	for (int z = 0; z < TERRIANMAP_Z; z++)
	{
		for (int x = 0; x < TERRIANMAP_X; x++)
		{
			terrainHeightMap[x][z][0] = float(x) * MAP_SCALE;
			//terrainHeightMap[x][z][1] = 0.0;
			terrainHeightMap[x][z][1] = (float)imageData[(x + z * TERRIANMAP_Z) * 3] * 1;
			terrainHeightMap[x][z][2] = -float(z) * MAP_SCALE;
		}
	}
}

BOOL loadHeightMapData(char* filename)
{
	BOOL hResult = TRUE;
	int imgWidth, imgHeight, imgChannel;

	imageData = stbi_load(filename, &imgWidth, &imgHeight, &imgChannel, 0);
	if (imageData == NULL)
	{
		fprintf(gpFile, " Image data not loaded\n");
		return false;
	}
	return true;
}

void resize(int width, int height)
{
	// code

	// kitihi height kami keli ki height 0 nasayla havi kamit kami 1 havi
	// To Avoid divided by 0(illegal instruction / code) in future code
	if (height == 0) 
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION); // atta aamhi projection paahnaar aahot
	glLoadIdentity();

	// if else block removed -> change 3

	gluPerspective(45.0f, 
				 (GLfloat)width 
		         / (GLfloat)height,
				 0.1f,
		         2500.0f); 
	// height should not be 0
}

void display(void)
{
	if (bgFlag == TRUE)
		glClearColor(0.25f, 0.25f, 0.25f, 1.0f); // 4th parameter alpha color
	else
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // 4th parameter alpha color

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (blend4 >= 0.0f)
	{
		glPushMatrix();
		glTranslatef(0.1f, 0.0f, -3.0f);
		if (blend2 >= 0.0f)
		{
			if (blend1 <= 1.0f)
				glColor4f(1.0f, 1.0f, 1.0f, blend1);
			if (blend1 >= 1.0f && blend2 >= 0.0f)
				glColor4f(1.0f, 1.0f, 1.0f, blend2);

			glBindTexture(GL_TEXTURE_2D, texture_1);
			glBegin(GL_QUADS);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(2.11f, 1.25f, 0.0f);
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-2.31f, 1.25f, 0.0f);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-2.31f, -1.25f, 0.0f);
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(2.11f, -1.25f, 0.0f);

			glEnd();
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		if (blend2 <= 0.0f && blend4 >= 0.0f)
		{

			if (blend3 <= 1.0f)
				glColor4f(1.0f, 1.0f, 1.0f, blend3);
			if (blend3 >= 1.0f && blend4 >= 0.0f)
				glColor4f(1.0f, 1.0f, 1.0f, blend4);

			glBindTexture(GL_TEXTURE_2D, texture_2);
			glBegin(GL_QUADS);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(2.11f, 1.25f, 0.0f);
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-2.31f, 1.25f, 0.0f);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-2.31f, -1.25f, 0.0f);
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(2.11f, -1.25f, 0.0f);

			glEnd();
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		glPopMatrix();
	}
	
	if (ez1 >= -46.0f && blend4 <= 0.0f)
	{
		sprintf(str, "ex1 = %f  ey1 = %f  ez1 = %f cx1 = %f cy1 = %f cz1 = %f ", ex1, ey1, ez1, cx1, cy1, cz1);
		SetWindowTextA(ghwnd, str);
		scene1();
	}

	if (ez1 < -46.0f && ez2 > -9.9f)
	{
		sprintf(str, "ex2 = %f  ey2 = %f  ez2 = %f cx2 = %f cy2 = %f cz2 = %f ", ex2, ey2, ez2, cx2, cy2, cz2);
		SetWindowTextA(ghwnd, str);
		scene2();
	}

	if (ez2 < -9.9f && tx <= 13.97f)
	{
		sprintf(str, "tx = %f", tx);
		SetWindowTextA(ghwnd, str);
		scene3();
	}

	if (tx > 13.97f && tx1 <= 13.97f)
	{
		sprintf(str, "tx1 = %f", tx1);
		SetWindowTextA(ghwnd, str);
		scene4();
	}

	if (tx1 > 13.97f && tx2 <= 13.97f)
	{
		sprintf(str, "tx2 = %f", tx2);
		SetWindowTextA(ghwnd, str);
		scene5();
	}

	if (tx2 > 13.97f && tz3 <= 594.0f)
	{
		bgFlag = TRUE;
		sprintf(str, "%f  %f  %f ", tx3, ty3, tz3);
		SetWindowTextA(ghwnd, str);
		scene6();
	}

	if (tz3 >= 594.0f && counter <= 0.2345f)
	{
		bgFlag = FALSE;
		scene7();
	}

	if (counter >= 0.2345f && blend22 >= 0.0f)
	{
		scene8();
	}

	if (blend22 <= 0.0f)
	{
		glTranslatef(tx5, ty5, tz5);

		sprintf(str, "%f  %f  %f ", tx5, ty5, tz5);
		SetWindowTextA(ghwnd, str);

		glPushMatrix();
		glTranslatef(-0.75f, 0.0f, 0.0f);
		glScalef(0.3f, 0.3f, 0.0f);
		glColor3f(0.9333f, 0.4235f, 0.3019f);
		glListBase(nFontList);
		glCallLists(10, GL_UNSIGNED_BYTE, "Created By");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.25f, -0.5f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glColor3f(0.2549f, 0.4117f, 0.8823f);
		glListBase(nFontList);
		glCallLists(12, GL_UNSIGNED_BYTE, "Mandar Maske");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.15f, -1.5f, 0.0f);
		glScalef(0.3f, 0.3f, 0.0f);
		glColor3f(0.9333f, 0.4235f, 0.3019f);
		glListBase(nFontList);
		glCallLists(15, GL_UNSIGNED_BYTE, "Technology Used");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-4.05f, -2.0f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glColor3f(0.2549f, 0.4117f, 0.8823f);
		glListBase(nFontList);
		glCallLists(44, GL_UNSIGNED_BYTE, "OpenGL (Fixed Function Pipeline) & Win32 SDK");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.65f, -2.5f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(18, GL_UNSIGNED_BYTE, "Primitive Geometry");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.77f, -3.0f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(8, GL_UNSIGNED_BYTE, "Textures");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.67f, -3.5f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(6, GL_UNSIGNED_BYTE, "Camera");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.77f, -4.0f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(8, GL_UNSIGNED_BYTE, "Blending");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.93f, -4.5f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(9, GL_UNSIGNED_BYTE, "PlaySound");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.35f, -5.0f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(14, GL_UNSIGNED_BYTE, "Font Rendering");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.6f, -5.5f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(7, GL_UNSIGNED_BYTE, "Terrain");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-2.4f, -6.0f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(28, GL_UNSIGNED_BYTE, "Data Structure - Linked List");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.2f, -7.0f, 0.0f);
		glScalef(0.3f, 0.3f, 0.0f);
		glColor3f(0.9333f, 0.4235f, 0.3019f);
		glListBase(nFontList);
		glCallLists(15, GL_UNSIGNED_BYTE, "Acknowledgments");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-3.7f, -7.5f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glColor3f(0.2549f, 0.4117f, 0.8823f);
		glListBase(nFontList);
		glCallLists(39, GL_UNSIGNED_BYTE, "Thanks To All My Fragment Group Members");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-3.05f, -8.0f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(33, GL_UNSIGNED_BYTE, "And Group Leader - Sanchit Gharat");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.35f, -9.0f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glColor3f(0.9333f, 0.4235f, 0.3019f);
		glListBase(nFontList);
		glCallLists(14, GL_UNSIGNED_BYTE, "Special Thanks");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-3.2f, -9.6f, 0.0f);
		glScalef(0.5f, 0.5f, 0.0f);
		glColor3f(0.2549f, 0.4117f, 0.8823f);
		glListBase(nFontList);
		glCallLists(27, GL_UNSIGNED_BYTE, "Pradnya Vijay Gokhale Ma'am");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-3.3f, -10.2f, 0.0f);
		glScalef(0.5f, 0.5f, 0.0f);
		glListBase(nFontList);
		glCallLists(28, GL_UNSIGNED_BYTE, "Dr. Rama Vijay Gokhale Ma'am");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.45f, -10.8f, 0.0f);
		glScalef(0.4f, 0.4f, 0.0f);
		glListBase(nFontList);
		glCallLists(3, GL_UNSIGNED_BYTE, "And");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-2.5f, -11.4f, 0.0f);
		glScalef(0.5f, 0.5f, 0.0f);
		glListBase(nFontList);
		glCallLists(24, GL_UNSIGNED_BYTE, "Dr. Vijay D. Gokhale Sir");
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-2.3f, -17.2f, 0.0f);
		glScalef(1.0f, 1.0f, 0.0f);
		glColor3f(1.0f, 0.0784f, 0.5764f);
		glListBase(nFontList);
		glCallLists(9, GL_UNSIGNED_BYTE, "Thank You");
		glPopMatrix();
	}
	
	SwapBuffers(ghdc); // native 
}

void update(void)
{
	// code
	if (blend1 <= 1.0f)
	{
		blend1 = blend1 + 0.00109f;
	}

	if (blend1 >= 1.0f && blend2 >= 0.0f)
	{
		blend2 = blend2 - 0.00109f;
	}

	if (blend2 <= 0.0f && blend3 <= 1.0f)
	{
		blend3 = blend3 + 0.00109f;
	}

	if (blend3 >= 1.0f && blend4 >= 0.0f)
	{
		blend4 = blend4 - 0.00109f;
	}

	if (ey1 >= 0.0f && blend4 <= 0.0f)
	{
		ey1 = ey1 - 0.01f;
	}

	if (cy1 <= 2.0f && blend4 <= 0.0f)
	{
		cy1 = cy1 + 0.037f;
	}

	if (ey1 < 0.0f && cy1 > 2.0f && ez1 >= -46.0f)
	{
		ez1 = ez1 - 0.03f;
	}

	if (ez1 < -46.0f && ex2 >= 0.0f)
	{
		ex2 = ex2 - 0.002f;
	}

	if (ex2 <= 0.0f && cy2 > -4.0f)
	{
		cy2 = cy2 - 0.0005f;
	}

	if (cy2 < -2.0f && ez2 > -9.9f)
	{
		ez2 = ez2 - 0.00345f;
	}

	if (ez2 < -9.9f && tx <= 13.97f)
	{
		tx = tx + 0.005f;
	}

	if (tx > 13.97f && tx1 <= 13.97f)
	{
		tx1 = tx1 + 0.005f;
	}

	if (tx1 > 13.97f && tx2 <= 13.97f)
	{
		tx2 = tx2 + 0.005f;
	}

	if (ty3 >= -186.0f && tx2 > 13.97f)
	{
		ty3 = ty3 - 0.09f;
	}

	if (ty3 < -186.0f && tz3 <= 594.0f)
	{
		tz3 = tz3 + 0.1f;
	}

	if (b1 <= 1.0f && tz3 >= 594.0f)
	{
		b1 = b1 + 0.05f;
	}

	if (b1 >= 1.0f && b2 <= 0.5f)
	{
		b2 = b2 + 0.002f;
	}

	if (b2 >= 0.5f && counter <= 0.2345f)
	{
		counter = counter + 0.00025f;
	}

	// 1
	if (blend111 <= 1.0f && counter >= 0.2345f)
	{
		blend111 = blend111 + 0.031f;
	}

	if (pic1count <= 1.0f && blend111 >= 1.0f)
	{
		pic1count = pic1count + 0.0024f;
	}

	if (pic1count >= 1.0f && blend222 >= 0.0f)
	{
		blend222 = blend222 - 0.031f;
	}

	// 2
	if (blend222 <= 0.0f && blend33 <= 1.0f)
	{
		blend33 = blend33 + 0.031f;
	}

	if (pic2count <= 1.0f && blend33 >= 1.0f)
	{
		pic2count = pic2count + 0.0024f;
	}

	if (pic2count >= 1.0f && blend44 >= 0.0f)
	{
		blend44 = blend44 - 0.031f;
	}

	// 3
	if (blend44 <= 0.0f && blend5 <= 1.0f)
	{
		blend5 = blend5 + 0.031f;
	}

	if (pic3count <= 1.0f && blend5 >= 1.0f)
	{
		pic3count = pic3count + 0.0024f;
	}

	if (pic3count >= 1.0f && blend6 >= 0.0f)
	{
		blend6 = blend6 - 0.031f;
	}

	// 4
	if (blend6 <= 0.0f && blend7 <= 1.0f)
	{
		blend7 = blend7 + 0.031f;
	}

	if (pic4count <= 1.0f && blend7 >= 1.0f)
	{
		pic4count = pic4count + 0.0024f;
	}

	if (pic4count >= 1.0f && blend8 >= 0.0f)
	{
		blend8 = blend8 - 0.031f;
	}

	// 5
	if (blend8 <= 0.0f && blend9 <= 1.0f)
	{
		blend9 = blend9 + 0.031f;
	}

	if (pic5count <= 1.0f && blend9 >= 1.0f)
	{
		pic5count = pic5count + 0.0024f;
	}

	if (pic5count >= 1.0f && blend10 >= 0.0f)
	{
		blend10 = blend10 - 0.031f;
	}

	// 6
	if (blend10 <= 0.0f && blend11 <= 1.0f)
	{
		blend11 = blend11 + 0.031f;
	}

	if (pic6count <= 1.0f && blend11 >= 1.0f)
	{
		pic6count = pic6count + 0.0024f;
	}

	if (pic6count >= 1.0f && blend12 >= 0.0f)
	{
		blend12 = blend12 - 0.031f;
	}

	// 7
	if (blend12 <= 0.0f && blend13 <= 1.0f)
	{
		blend13 = blend13 + 0.031f;
	}

	if (pic7count <= 1.0f && blend13 >= 1.0f)
	{
		pic7count = pic7count + 0.0024f;
	}

	if (pic7count >= 1.0f && blend14 >= 0.0f)
	{
		blend14 = blend14 - 0.031f;
	}

	// 8
	if (blend14 <= 0.0f && blend15 <= 1.0f)
	{
		blend15 = blend15 + 0.031f;
	}

	if (pic8count <= 1.0f && blend15 >= 1.0f)
	{
		pic8count = pic8count + 0.0024f;
	}

	if (pic8count >= 1.0f && blend16 >= 0.0f)
	{
		blend16 = blend16 - 0.031f;
	}

	// 9
	if (blend16 <= 0.0f && blend17 <= 1.0f)
	{
		blend17 = blend17 + 0.031f;
	}

	if (pic9count <= 1.0f && blend17 >= 1.0f)
	{
		pic9count = pic9count + 0.0024f;
	}

	if (pic9count >= 1.0f && blend18 >= 0.0f)
	{
		blend18 = blend18 - 0.031f;
	}

	// 10
	if (blend18 <= 0.0f && blend19 <= 1.0f)
	{
		blend19 = blend19 + 0.031f;
	}

	if (pic10count <= 1.0f && blend19 >= 1.0f)
	{
		pic10count = pic10count + 0.0024f;
	}

	if (pic10count >= 1.0f && blend20 >= 0.0f)
	{
		blend20 = blend20 - 0.031f;
	}

	// 11
	if (blend20 <= 0.0f && blend21 <= 1.0f)
	{
		blend21 = blend21 + 0.031f;
	}

	if (pic11count <= 1.0f && blend21 >= 1.0f)
	{
		pic11count = pic11count + 0.00055f;
	}

	if (pic11count >= 1.0f && blend22 >= 0.0f)
	{
		blend22 = blend22 - 0.031f;
	}

	if (blend22 <= 0.0f)
	{
		if (ty5 < 16.9f)
		{
			ty5 = ty5 + 0.0035f;
		}
	}
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code

	// FullScreen astana escape press karun close karaych try kela tar window orginal state 
	// madhye yeun tyacha sukhad mrutyu hoil aapla window application 
	if (gbFullScreen)
	{
		ToggleFullScreen(); // important so taken in curly brace
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (newTreeData)
	{
		free(newTreeData);
		newTreeData = NULL;
	}

	if (texture_bottom)
	{
		glDeleteTextures(1, &texture_bottom);
	}

	if (texture_top)
	{
		glDeleteTextures(1, &texture_top);
	}

	if (texture_front)
	{
		glDeleteTextures(1, &texture_front);
	}

	if (texture_right)
	{
		glDeleteTextures(1, &texture_right);
	}

	if (texture_back)
	{
		glDeleteTextures(1, &texture_back);
	}

	if (texture_left)
	{
		glDeleteTextures(1, &texture_left);
	}

	if (texture)
	{
		glDeleteTextures(1, &texture);
	}

	if (texture_roof)
	{
		glDeleteTextures(1, &texture_roof);
	}

	if (texture_tv)
	{
		glDeleteTextures(1, &texture_tv);
	}

	if (texture_furniture)
	{
		glDeleteTextures(1, &texture_furniture);
	}

	if (texture_sofa)
	{
		glDeleteTextures(1, &texture_sofa);
	}

	if (texture_roof1)
	{
		glDeleteTextures(1, &texture_roof1);
	}

	if (texture_grass1)
	{
		glDeleteTextures(1, &texture_grass1);
	}

	if (texture_scenario1)
	{
		glDeleteTextures(1, &texture_scenario1);
	}

	if (texture_grass2)
	{
		glDeleteTextures(1, &texture_grass2);
	}

	if (texture_scenario2)
	{
		glDeleteTextures(1, &texture_scenario2);
	}

	if (texture_desert)
	{
		glDeleteTextures(1, &texture_desert);
	}

	if (texture_scenario3)
	{
		glDeleteTextures(1, &texture_scenario3);
	}

	if (texture_snow)
	{
		glDeleteTextures(1, &texture_snow);
	}

	if (texture_1)
	{
		glDeleteTextures(1, &texture_1);
	}

	if (texture_2)
	{
		glDeleteTextures(1, &texture_2);
	}

	if (texture_111)
	{
		glDeleteTextures(1, &texture_111);
	}

	if (texture_22)
	{
		glDeleteTextures(1, &texture_22);
	}

	if (texture_3)
	{
		glDeleteTextures(1, &texture_3);
	}

	if (texture_4)
	{
		glDeleteTextures(1, &texture_4);
	}

	if (texture_5)
	{
		glDeleteTextures(1, &texture_5);
	}

	if (texture_6)
	{
		glDeleteTextures(1, &texture_6);
	}

	if (texture_7)
	{
		glDeleteTextures(1, &texture_7);
	}

	if (texture_8)
	{
		glDeleteTextures(1, &texture_8);
	}

	if (texture_9)
	{
		glDeleteTextures(1, &texture_9);
	}

	if (texture_10)
	{
		glDeleteTextures(1, &texture_10);
	}

	if (texture_11)
	{
		glDeleteTextures(1, &texture_11);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd) // ghwnd ajun asel tar DestroyWindow() la call karun ghwnd destroy kar
	{
		DestroyWindow(ghwnd); 	
		ghwnd = NULL;
	}
	
	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); // file close keli with using files pointer
		gpFile = NULL; // bhanda(container) dhuvun kadhla use kelela
	}
}

