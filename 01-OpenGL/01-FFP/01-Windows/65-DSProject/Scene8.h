#pragma once
#include <GL/gl.h>
#include <GL/glu.h> 

GLuint texture_111;
GLuint texture_22;
GLuint texture_3;
GLuint texture_4;
GLuint texture_5;
GLuint texture_6;
GLuint texture_7;
GLuint texture_8;
GLuint texture_9;
GLuint texture_10;
GLuint texture_11;

float blend111 = 0.0f;
float blend222 = 1.0f;
float pic1count = 0.0f;

float blend33 = 0.0f;
float blend44 = 1.0f;
float pic2count = 0.0f;

float blend5 = 0.0f;
float blend6 = 1.0f;
float pic3count = 0.0f;

float blend7 = 0.0f;
float blend8 = 1.0f;
float pic4count = 0.0f;

float blend9 = 0.0f;
float blend10 = 1.0f;
float pic5count = 0.0f;

float blend11 = 0.0f;
float blend12 = 1.0f;
float pic6count = 0.0f;

float blend13 = 0.0f;
float blend14 = 1.0f;
float pic7count = 0.0f;

float blend15 = 0.0f;
float blend16 = 1.0f;
float pic8count = 0.0f;

float blend17 = 0.0f;
float blend18 = 1.0f;
float pic9count = 0.0f;

float blend19 = 0.0f;
float blend20 = 1.0f;
float pic10count = 0.0f;

float blend21 = 0.0f;
float blend22 = 1.0f;
float pic11count = 0.0f;

void scene8(void)
{
	glTranslatef(0.1f, 0.0f, -3.0f);

	glPushMatrix();
	if (blend222 >= 0.0f)
	{
		if (blend111 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend111);
		if (blend111 >= 1.0f && blend222 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend222);

		glBindTexture(GL_TEXTURE_2D, texture_111);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	//sprintf(str, "blend3 = %f pic2count = %f blend4 = %f", blend3, pic2count, blend4);
	//SetWindowTextA(ghwnd, str);

	glPushMatrix();
	if (blend222 <= 0.0f && blend44 >= 0.0f)
	{
		if (blend33 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend33);
		if (blend33 >= 1.0f && blend44 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend44);

		glBindTexture(GL_TEXTURE_2D, texture_22);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend44 <= 0.0f && blend6 >= 0.0f)
	{
		if (blend5 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend5);
		if (blend5 >= 1.0f && blend6 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend6);

		glBindTexture(GL_TEXTURE_2D, texture_3);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend6 <= 0.0f && blend8 >= 0.0f)
	{
		if (blend7 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend7);
		if (blend7 >= 1.0f && blend8 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend8);

		glBindTexture(GL_TEXTURE_2D, texture_4);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend8 <= 0.0f && blend10 >= 0.0f)
	{
		if (blend9 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend9);
		if (blend9 >= 1.0f && blend10 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend10);

		glBindTexture(GL_TEXTURE_2D, texture_5);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend10 <= 0.0f && blend12 >= 0.0f)
	{
		if (blend11 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend11);
		if (blend11 >= 1.0f && blend12 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend12);

		glBindTexture(GL_TEXTURE_2D, texture_6);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend12 <= 0.0f && blend14 >= 0.0f)
	{
		if (blend13 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend13);
		if (blend13 >= 1.0f && blend14 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend14);

		glBindTexture(GL_TEXTURE_2D, texture_7);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend14 <= 0.0f && blend16 >= 0.0f)
	{
		if (blend15 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend15);
		if (blend15 >= 1.0f && blend16 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend16);

		glBindTexture(GL_TEXTURE_2D, texture_8);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend16 <= 0.0f && blend18 >= 0.0f)
	{
		if (blend17 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend17);
		if (blend17 >= 1.0f && blend18 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend18);

		glBindTexture(GL_TEXTURE_2D, texture_9);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend18 <= 0.0f && blend20 >= 0.0f)
	{
		if (blend19 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend19);
		if (blend19 >= 1.0f && blend20 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend20);

		glBindTexture(GL_TEXTURE_2D, texture_10);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glPushMatrix();
	if (blend20 <= 0.0f && blend22 >= 0.0f)
	{
		if (blend21 <= 1.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend21);
		if (blend21 >= 1.0f && blend22 >= 0.0f)
			glColor4f(1.0f, 1.0f, 1.0f, blend22);

		glBindTexture(GL_TEXTURE_2D, texture_11);
		glBegin(GL_QUADS);

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(2.11f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-2.31f, 1.25f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-2.31f, -1.25f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(2.11f, -1.25f, 0.0f);

		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();
}