#pragma once
#include <GL/gl.h>

GLuint texture_desert;
GLuint texture_scenario3;

float tx2 = 0.0f;

void scenario3(void)
{
	glTranslatef(tx2, 0.0f, -20.0f);

	glColor3f(0.0784f, 0.0823f, 0.0980f);
	// Road
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, -4.0f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, -4.0f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -8.5f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -8.5f, 0.0f);

	glEnd();

	// Grass Across Road
	glColor3f(0.25f, 0.25f, 0.25f);
	glBindTexture(GL_TEXTURE_2D, texture_desert);
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -4.0f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -4.0f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	// texture city
	glColor3f(0.5f, 0.7f, 0.7f);
	glBindTexture(GL_TEXTURE_2D, texture_scenario3);
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, 8.5f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, 8.5f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -1.0f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -1.0f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	// White lines in road
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);

	glVertex3f(14.0f, -6.2f, 0.0f);
	glVertex3f(10.0f, -6.2f, 0.0f);
	glVertex3f(10.0f, -6.7f, 0.0f);
	glVertex3f(14.0f, -6.7f, 0.0f);

	glVertex3f(6.0f, -6.2f, 0.0f);
	glVertex3f(2.0f, -6.2f, 0.0f);
	glVertex3f(2.0f, -6.7f, 0.0f);
	glVertex3f(6.0f, -6.7f, 0.0f);

	glVertex3f(-2.0f, -6.2f, 0.0f);
	glVertex3f(-6.0f, -6.2f, 0.0f);
	glVertex3f(-6.0f, -6.7f, 0.0f);
	glVertex3f(-2.0f, -6.7f, 0.0f);

	glVertex3f(-10.0f, -6.2f, 0.0f);
	glVertex3f(-14.0f, -6.2f, 0.0f);
	glVertex3f(-14.0f, -6.7f, 0.0f);
	glVertex3f(-10.0f, -6.7f, 0.0f);

	glVertex3f(-18.0f, -6.2f, 0.0f);
	glVertex3f(-22.0f, -6.2f, 0.0f);
	glVertex3f(-22.0f, -6.7f, 0.0f);
	glVertex3f(-18.0f, -6.7f, 0.0f);

	glVertex3f(-26.0f, -6.2f, 0.0f);
	glVertex3f(-30.0f, -6.2f, 0.0f);
	glVertex3f(-30.0f, -6.7f, 0.0f);
	glVertex3f(-26.0f, -6.7f, 0.0f);

	glEnd();
}

void scene5(void)
{
	royalEnfiled12();
	scenario3();
}