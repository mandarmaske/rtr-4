#pragma once
#include <GL/gl.h>

void fences(void)
{
	// Left Column of Gate
	glColor3f(0.5607f, 0.4980f, 0.5176f);
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(-2.0f, 2.0f, -5.5f);
	glVertex3f(-3.0f, 2.0f, -5.5f);
	glVertex3f(-3.0f, -1.0f, -5.5f);
	glVertex3f(-2.0f, -1.0f, -5.5f);

	// Right Face
	glVertex3f(-2.0f, 2.0f, -6.5f);
	glVertex3f(-2.0f, 2.0f, -5.5f);
	glVertex3f(-2.0f, -1.0f, -5.5f);
	glVertex3f(-2.0f, -1.0f, -6.5f);

	// Left Face
	glVertex3f(-3.0f, 2.0f, -6.5f);
	glVertex3f(-3.0f, 2.0f, -5.5f);
	glVertex3f(-3.0f, -1.0f, -5.5f);
	glVertex3f(-3.0f, -1.0f, -6.5f);

	// Back Face
	glVertex3f(-2.0f, 2.0f, -6.5f);
	glVertex3f(-3.0f, 2.0f, -6.5f);
	glVertex3f(-3.0f, -1.0f, -6.5f);
	glVertex3f(-2.0f, -1.0f, -6.5f);

	glEnd();

	// Left Column top Cube
	glColor3f(0.8843f, 0.6607f, 0.4294f);
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(-1.7f, 2.5f, -5.2f);
	glVertex3f(-3.3f, 2.5f, -5.2f);
	glVertex3f(-3.3f, 2.0f, -5.2f);
	glVertex3f(-1.7f, 2.0f, -5.2f);

	// Right Face
	glVertex3f(-1.7f, 2.5f, -6.8f);
	glVertex3f(-1.7f, 2.5f, -5.2f);
	glVertex3f(-1.7f, 2.0f, -5.2f);
	glVertex3f(-1.7f, 2.0f, -6.8f);

	// Left Face
	glVertex3f(-3.3f, 2.5f, -6.8f);
	glVertex3f(-3.3f, 2.5f, -5.2f);
	glVertex3f(-3.3f, 2.0f, -5.2f);
	glVertex3f(-3.3f, 2.0f, -6.8f);

	// Back Face
	glVertex3f(-1.7f, 2.5f, -6.8f);
	glVertex3f(-3.3f, 2.5f, -6.8f);
	glVertex3f(-3.3f, 2.0f, -6.8f);
	glVertex3f(-1.7f, 2.0f, -6.8f);

	// Top Face
	glVertex3f(-3.3f, 2.5f, -5.2f);
	glVertex3f(-1.7f, 2.5f, -5.2f);
	glVertex3f(-1.7f, 2.5f, -6.8f);
	glVertex3f(-3.3f, 2.5f, -6.8f);

	// Bottom Face
	glVertex3f(-3.3f, 2.0f, -5.2f);
	glVertex3f(-1.7f, 2.0f, -5.2f);
	glVertex3f(-1.7f, 2.0f, -6.8f);
	glVertex3f(-3.3f, 2.0f, -6.8f);

	glEnd();

	glColor3f(0.5607f, 0.4980f, 0.5176f);
	// Right Column of Gate
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(2.0f, 2.0f, -5.5f);
	glVertex3f(3.0f, 2.0f, -5.5f);
	glVertex3f(3.0f, -1.0f, -5.5f);
	glVertex3f(2.0f, -1.0f, -5.5f);

	// Right Face
	glVertex3f(2.0f, 2.0f, -6.5f);
	glVertex3f(2.0f, 2.0f, -5.5f);
	glVertex3f(2.0f, -1.0f, -5.5f);
	glVertex3f(2.0f, -1.0f, -6.5f);

	// Left Face
	glVertex3f(3.0f, 2.0f, -6.5f);
	glVertex3f(3.0f, 2.0f, -5.5f);
	glVertex3f(3.0f, -1.0f, -5.5f);
	glVertex3f(3.0f, -1.0f, -6.5f);

	// Back Face
	glVertex3f(2.0f, 2.0f, -6.5f);
	glVertex3f(3.0f, 2.0f, -6.5f);
	glVertex3f(3.0f, -1.0f, -6.5f);
	glVertex3f(2.0f, -1.0f, -6.5f);

	glEnd();

	// Right Column top Cube
	glColor3f(0.8843f, 0.6607f, 0.4294f);
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(1.7f, 2.5f, -5.2f);
	glVertex3f(3.3f, 2.5f, -5.2f);
	glVertex3f(3.3f, 2.0f, -5.2f);
	glVertex3f(1.7f, 2.0f, -5.2f);

	// Right Face
	glVertex3f(1.7f, 2.5f, -6.8f);
	glVertex3f(1.7f, 2.5f, -5.2f);
	glVertex3f(1.7f, 2.0f, -5.2f);
	glVertex3f(1.7f, 2.0f, -6.8f);

	// Left Face
	glVertex3f(3.3f, 2.5f, -6.8f);
	glVertex3f(3.3f, 2.5f, -5.2f);
	glVertex3f(3.3f, 2.0f, -5.2f);
	glVertex3f(3.3f, 2.0f, -6.8f);

	// Back Face
	glVertex3f(1.7f, 2.5f, -6.8f);
	glVertex3f(3.3f, 2.5f, -6.8f);
	glVertex3f(3.3f, 2.0f, -6.8f);
	glVertex3f(1.7f, 2.0f, -6.8f);

	// Top Face
	glVertex3f(3.3f, 2.5f, -5.2f);
	glVertex3f(1.7f, 2.5f, -5.2f);
	glVertex3f(1.7f, 2.5f, -6.8f);
	glVertex3f(3.3f, 2.5f, -6.8f);

	// Bottom Face
	glVertex3f(3.3f, 2.0f, -5.2f);
	glVertex3f(1.7f, 2.0f, -5.2f);
	glVertex3f(1.7f, 2.0f, -6.8f);
	glVertex3f(3.3f, 2.0f, -6.8f);

	glEnd();

	// *********************** Wall Fence
	// Front Left Side Fence
	glColor3f(0.9372f, 0.8705f, 0.8039f);
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(-3.0f, 1.8f, -5.7f);
	glVertex3f(-16.0f, 1.8f, -5.7f);
	glVertex3f(-16.0f, -1.0f, -5.7f);
	glVertex3f(-3.0f, -1.0f, -5.7f);

	glColor3f(0.8509f, 0.6980f, 0.5411f);
	// Top Face
	glVertex3f(-16.0f, 1.8f, -5.7f);
	glVertex3f(-3.0f, 1.8f, -5.7f);
	glVertex3f(-3.0f, 1.8f, -6.3f);
	glVertex3f(-16.0f, 1.8f, -6.3f);

	glColor3f(0.9372f, 0.8705f, 0.8039f);
	// Back Face
	glVertex3f(-3.0f, 1.8f, -6.3f);
	glVertex3f(-16.0f, 1.8f, -6.3f);
	glVertex3f(-16.0f, -1.0f, -6.3f);
	glVertex3f(-3.0f, -1.0f, -6.3f);

	glEnd();

	// Front Right Side Fence
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(3.0f, 1.8f, -5.7f);
	glVertex3f(16.0f, 1.8f, -5.7f);
	glVertex3f(16.0f, -1.0f, -5.7f);
	glVertex3f(3.0f, -1.0f, -5.7f);

	glColor3f(0.8509f, 0.6980f, 0.5411f);
	// Top Face
	glVertex3f(16.0f, 1.8f, -5.7f);
	glVertex3f(3.0f, 1.8f, -5.7f);
	glVertex3f(3.0f, 1.8f, -6.3f);
	glVertex3f(16.0f, 1.8f, -6.3f);

	glColor3f(0.9372f, 0.8705f, 0.8039f);
	// Back Face
	glVertex3f(3.0f, 1.8f, -6.3f);
	glVertex3f(16.0f, 1.8f, -6.3f);
	glVertex3f(16.0f, -1.0f, -6.3f);
	glVertex3f(3.0f, -1.0f, -6.3f);

	glEnd();

	//********* Left Side Fence
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(-16.0f, 1.8f, -5.7f);
	glVertex3f(-16.6f, 1.8f, -5.7f);
	glVertex3f(-16.6f, -1.0f, -5.7f);
	glVertex3f(-16.0f, -1.0f, -5.7f);

	// Right Face
	glVertex3f(-16.0f, 1.8f, -28.7f);
	glVertex3f(-16.0f, 1.8f, -5.7f);
	glVertex3f(-16.0f, -1.0f, -5.7f);
	glVertex3f(-16.0f, -1.0f, -28.7f);

	// Left Face
	glVertex3f(-16.6f, 1.8f, -28.7f);
	glVertex3f(-16.6f, 1.8f, -5.7f);
	glVertex3f(-16.6f, -1.0f, -5.7f);
	glVertex3f(-16.6f, -1.0f, -28.7f);

	glColor3f(0.8509f, 0.6980f, 0.5411f);
	// Top Face
	glVertex3f(-16.6f, 1.8f, -5.7f);
	glVertex3f(-16.0f, 1.8f, -5.7f);
	glVertex3f(-16.0f, 1.8f, -28.7f);
	glVertex3f(-16.6f, 1.8f, -28.7f);

	glEnd();

	glColor3f(0.9372f, 0.8705f, 0.8039f);
	//********* Right Side Fence
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(16.0f, 1.8f, -5.7f);
	glVertex3f(16.6f, 1.8f, -5.7f);
	glVertex3f(16.6f, -1.0f, -5.7f);
	glVertex3f(16.0f, -1.0f, -5.7f);

	// Right Face
	glVertex3f(16.0f, 1.8f, -28.7f);
	glVertex3f(16.0f, 1.8f, -5.7f);
	glVertex3f(16.0f, -1.0f, -5.7f);
	glVertex3f(16.0f, -1.0f, -28.7f);

	// Left Face
	glVertex3f(16.6f, 1.8f, -28.7f);
	glVertex3f(16.6f, 1.8f, -5.7f);
	glVertex3f(16.6f, -1.0f, -5.7f);
	glVertex3f(16.6f, -1.0f, -28.7f);

	glColor3f(0.8509f, 0.6980f, 0.5411f);
	// Top Face
	glVertex3f(16.6f, 1.8f, -5.7f);
	glVertex3f(16.0f, 1.8f, -5.7f);
	glVertex3f(16.0f, 1.8f, -28.7f);
	glVertex3f(16.6f, 1.8f, -28.7f);

	glEnd();

	glColor3f(0.9372f, 0.8705f, 0.8039f);
	//********* Back Fence
	glBegin(GL_QUADS);

	// Front Face
	glVertex3f(16.6f, 1.8f, -28.7f);
	glVertex3f(-16.6f, 1.8f, -28.7f);
	glVertex3f(-16.6f, -1.0f, -28.7f);
	glVertex3f(16.6f, -1.0f, -28.7f);

	// Right Face 
	glVertex3f(16.6f, 1.8f, -29.3f);
	glVertex3f(16.6f, 1.8f, -28.7f);
	glVertex3f(16.6f, -1.0f, -28.7f);
	glVertex3f(16.6f, -1.0f, -29.3f);

	// Back Face
	glVertex3f(16.6f, 1.8f, -29.3f);
	glVertex3f(-16.6f, 1.8f, -29.3f);
	glVertex3f(-16.6f, -1.0f, -29.3f);
	glVertex3f(16.6f, -1.0f, -29.3f);

	// Left Face 
	glVertex3f(-16.6f, 1.8f, -29.3f);
	glVertex3f(-16.6f, 1.8f, -28.7f);
	glVertex3f(-16.6f, -1.0f, -28.7f);
	glVertex3f(-16.6f, -1.0f, -29.3f);

	glColor3f(0.8509f, 0.6980f, 0.5411f);
	// Top Face
	glVertex3f(-16.6f, 1.8f, -28.7f);
	glVertex3f(16.6f, 1.8f, -28.7f);
	glVertex3f(16.6f, 1.8f, -29.3f);
	glVertex3f(-16.6f, 1.8f, -29.3f);

	glEnd();
}