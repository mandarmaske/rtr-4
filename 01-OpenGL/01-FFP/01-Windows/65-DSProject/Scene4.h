#pragma once
#include <GL/gl.h>
#include "Circle.h"

GLuint texture_grass2;
GLuint texture_scenario2;

float tx1 = 0.0f;

// changes in uninitialize

typedef struct treeData
{
	float xdata;
	struct treeData* next;
}tree_x;

tree_x* head = NULL;
tree_x* newTreeData;

tree_x* createTreeData(float xValue)
{
	newTreeData = (tree_x*)malloc(sizeof(tree_x));
	newTreeData->xdata = xValue;
	newTreeData->next = NULL;
	return newTreeData;
}

void add_treeData(float xValue)
{
	tree_x* nz;
	nz = createTreeData(xValue);
	nz->next = head;
	head = nz;
}

void scenario2(void)
{
	glTranslatef(tx1, 0.0f, -20.0f);

	glColor3f(0.1784f, 0.1823f, 0.1980f);
	// Road
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, -4.0f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, -4.0f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -8.5f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -8.5f, 0.0f);

	glEnd();

	// Grass Across Road
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_grass2);
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, -1.0f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -4.0f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -4.0f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	// texture Moutain
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_scenario2);
	glBegin(GL_QUADS);

	glTexCoord2d(1.0f, 1.0f);
	glVertex3f(16.0f, 8.5f, 0.0f);
	glTexCoord2d(0.0f, 1.0f);
	glVertex3f(-30.0f, 8.5f, 0.0f);
	glTexCoord2d(0.0f, 0.0f);
	glVertex3f(-30.0f, -1.0f, 0.0f);
	glTexCoord2d(1.0f, 0.0f);
	glVertex3f(16.0f, -1.0f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	// White lines in road
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);

	glVertex3f(14.0f, -6.2f, 0.0f);
	glVertex3f(10.0f, -6.2f, 0.0f);
	glVertex3f(10.0f, -6.7f, 0.0f);
	glVertex3f(14.0f, -6.7f, 0.0f);

	glVertex3f(6.0f, -6.2f, 0.0f);
	glVertex3f(2.0f, -6.2f, 0.0f);
	glVertex3f(2.0f, -6.7f, 0.0f);
	glVertex3f(6.0f, -6.7f, 0.0f);

	glVertex3f(-2.0f, -6.2f, 0.0f);
	glVertex3f(-6.0f, -6.2f, 0.0f);
	glVertex3f(-6.0f, -6.7f, 0.0f);
	glVertex3f(-2.0f, -6.7f, 0.0f);

	glVertex3f(-10.0f, -6.2f, 0.0f);
	glVertex3f(-14.0f, -6.2f, 0.0f);
	glVertex3f(-14.0f, -6.7f, 0.0f);
	glVertex3f(-10.0f, -6.7f, 0.0f);

	glVertex3f(-18.0f, -6.2f, 0.0f);
	glVertex3f(-22.0f, -6.2f, 0.0f);
	glVertex3f(-22.0f, -6.7f, 0.0f);
	glVertex3f(-18.0f, -6.7f, 0.0f);

	glVertex3f(-26.0f, -6.2f, 0.0f);
	glVertex3f(-30.0f, -6.2f, 0.0f);
	glVertex3f(-30.0f, -6.7f, 0.0f);
	glVertex3f(-26.0f, -6.7f, 0.0f);

	glEnd();

	glTranslatef(0.0f, 1.2f, 0.0f);
	glScalef(1.5f, 2.0f, 0.0f);

	// From Right To Left

	// **************************** Tree1

	glPushMatrix();
	glColor3f(0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f, 1.0f, 0.0f);
	glVertex3f(7.4f, 0.1f, 0.0f);
	glVertex3f(8.6f, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.05f, 0.6f, 0.0f);
	glVertex3f(7.95f, 0.6f, 0.0f);
	glVertex3f(7.1f, -0.6f, 0.0f);
	glVertex3f(8.9f, -0.6f, 0.0f);

	glVertex3f(8.55f, -0.6f, 0.0f);
	glVertex3f(7.45f, -0.6f, 0.0f);
	glVertex3f(6.85f, -1.4f, 0.0f);
	glVertex3f(9.15f, -1.4f, 0.0f);

	glEnd();

	glColor3f(0.0, 0.3921f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f, 1.0f, 0.0f);
	glVertex3f(7.58f, 0.09f, 0.0f);
	glVertex3f(8.61f, 0.09f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.42f, 0.09f, 0.0f);
	glVertex3f(7.85f, 0.09f, 0.0f);
	glVertex3f(7.4f, -0.61f, 0.0f);
	glVertex3f(8.91f, -0.61f, 0.0f);

	glVertex3f(8.55f, -0.55f, 0.0f);
	glVertex3f(7.75f, -0.55f, 0.0f);
	glVertex3f(7.2f, -1.41f, 0.0f);
	glVertex3f(9.16f, -1.41f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glVertex3f(8.15f, -1.41f, 0.0f);
	glVertex3f(7.85f, -1.41f, 0.0f);
	glVertex3f(7.85f, -2.27f, 0.0f);
	glVertex3f(8.15f, -2.27f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(9.35f, -2.22f, 0.0f);
	glVertex3f(8.15f, -2.22f, 0.0f);
	glVertex3f(8.15f, -2.27f, 0.0f);
	glVertex3f(9.35f, -2.27f, 0.0f);

	glEnd();
	glPopMatrix();

	// ********************* Tree 2

	glPushMatrix();
	add_treeData(4.0f);

	glColor3f(0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.4f - head->xdata, 0.1f, 0.0f);
	glVertex3f(8.6f - head->xdata, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.05f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.95f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.1f - head->xdata, -0.6f, 0.0f);
	glVertex3f(8.9f - head->xdata, -0.6f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.6f, 0.0f);
	glVertex3f(7.45f - head->xdata, -0.6f, 0.0f);
	glVertex3f(6.85f - head->xdata, -1.4f, 0.0f);
	glVertex3f(9.15f - head->xdata, -1.4f, 0.0f);

	glEnd();

	glColor3f(0.0, 0.3921f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.58f - head->xdata, 0.09f, 0.0f);
	glVertex3f(8.61f - head->xdata, 0.09f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.42f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.85f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.4f - head->xdata, -0.61f, 0.0f);
	glVertex3f(8.91f - head->xdata, -0.61f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.75f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.2f - head->xdata, -1.41f, 0.0f);
	glVertex3f(9.16f - head->xdata, -1.41f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glVertex3f(8.15f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85 - head->xdata, -2.27f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(9.35f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);
	glVertex3f(9.35f - head->xdata, -2.27f, 0.0f);

	glEnd();
	glPopMatrix();

	// ********************* Tree 3

	head = head->next;
	glPushMatrix();

	add_treeData(8.0f);

	glColor3f(0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.4f - head->xdata, 0.1f, 0.0f);
	glVertex3f(8.6f - head->xdata, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.05f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.95f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.1f - head->xdata, -0.6f, 0.0f);
	glVertex3f(8.9f - head->xdata, -0.6f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.6f, 0.0f);
	glVertex3f(7.45f - head->xdata, -0.6f, 0.0f);
	glVertex3f(6.85f - head->xdata, -1.4f, 0.0f);
	glVertex3f(9.15f - head->xdata, -1.4f, 0.0f);

	glEnd();

	glColor3f(0.0, 0.3921f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.58f - head->xdata, 0.09f, 0.0f);
	glVertex3f(8.61f - head->xdata, 0.09f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.42f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.85f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.4f - head->xdata, -0.61f, 0.0f);
	glVertex3f(8.91f - head->xdata, -0.61f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.75f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.2f - head->xdata, -1.41f, 0.0f);
	glVertex3f(9.16f - head->xdata, -1.41f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glVertex3f(8.15f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85 - head->xdata, -2.27f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(9.35f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);
	glVertex3f(9.35f - head->xdata, -2.27f, 0.0f);

	glEnd();
	glPopMatrix();

	// ********************* Tree 4

	head = head->next;
	glPushMatrix();

	add_treeData(12.0f);

	glColor3f(0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.4f - head->xdata, 0.1f, 0.0f);
	glVertex3f(8.6f - head->xdata, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.05f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.95f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.1f - head->xdata, -0.6f, 0.0f);
	glVertex3f(8.9f - head->xdata, -0.6f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.6f, 0.0f);
	glVertex3f(7.45f - head->xdata, -0.6f, 0.0f);
	glVertex3f(6.85f - head->xdata, -1.4f, 0.0f);
	glVertex3f(9.15f - head->xdata, -1.4f, 0.0f);

	glEnd();

	glColor3f(0.0, 0.3921f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.58f - head->xdata, 0.09f, 0.0f);
	glVertex3f(8.61f - head->xdata, 0.09f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.42f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.85f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.4f - head->xdata, -0.61f, 0.0f);
	glVertex3f(8.91f - head->xdata, -0.61f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.75f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.2f - head->xdata, -1.41f, 0.0f);
	glVertex3f(9.16f - head->xdata, -1.41f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glVertex3f(8.15f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85 - head->xdata, -2.27f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(9.35f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);
	glVertex3f(9.35f - head->xdata, -2.27f, 0.0f);

	glEnd();
	glPopMatrix();


	// ********************* Tree 5

	head = head->next;
	glPushMatrix();

	add_treeData(16.0f);

	glColor3f(0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.4f - head->xdata, 0.1f, 0.0f);
	glVertex3f(8.6f - head->xdata, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.05f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.95f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.1f - head->xdata, -0.6f, 0.0f);
	glVertex3f(8.9f - head->xdata, -0.6f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.6f, 0.0f);
	glVertex3f(7.45f - head->xdata, -0.6f, 0.0f);
	glVertex3f(6.85f - head->xdata, -1.4f, 0.0f);
	glVertex3f(9.15f - head->xdata, -1.4f, 0.0f);

	glEnd();

	glColor3f(0.0, 0.3921f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.58f - head->xdata, 0.09f, 0.0f);
	glVertex3f(8.61f - head->xdata, 0.09f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.42f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.85f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.4f - head->xdata, -0.61f, 0.0f);
	glVertex3f(8.91f - head->xdata, -0.61f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.75f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.2f - head->xdata, -1.41f, 0.0f);
	glVertex3f(9.16f - head->xdata, -1.41f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glVertex3f(8.15f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85 - head->xdata, -2.27f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(9.35f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);
	glVertex3f(9.35f - head->xdata, -2.27f, 0.0f);

	glEnd();
	glPopMatrix();


	// ********************* Tree 6

	head = head->next;
	glPushMatrix();

	add_treeData(20.0f);

	glColor3f(0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.4f - head->xdata, 0.1f, 0.0f);
	glVertex3f(8.6f - head->xdata, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.05f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.95f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.1f - head->xdata, -0.6f, 0.0f);
	glVertex3f(8.9f - head->xdata, -0.6f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.6f, 0.0f);
	glVertex3f(7.45f - head->xdata, -0.6f, 0.0f);
	glVertex3f(6.85f - head->xdata, -1.4f, 0.0f);
	glVertex3f(9.15f - head->xdata, -1.4f, 0.0f);

	glEnd();

	glColor3f(0.0, 0.3921f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.58f - head->xdata, 0.09f, 0.0f);
	glVertex3f(8.61f - head->xdata, 0.09f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.42f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.85f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.4f - head->xdata, -0.61f, 0.0f);
	glVertex3f(8.91f - head->xdata, -0.61f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.75f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.2f - head->xdata, -1.41f, 0.0f);
	glVertex3f(9.16f - head->xdata, -1.41f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glVertex3f(8.15f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85 - head->xdata, -2.27f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(9.35f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);
	glVertex3f(9.35f - head->xdata, -2.27f, 0.0f);

	glEnd();
	glPopMatrix();


	// ********************* Tree 7

	head = head->next;
	glPushMatrix();

	add_treeData(24.0f);

	glColor3f(0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.4f - head->xdata, 0.1f, 0.0f);
	glVertex3f(8.6f - head->xdata, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.05f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.95f - head->xdata, 0.6f, 0.0f);
	glVertex3f(7.1f - head->xdata, -0.6f, 0.0f);
	glVertex3f(8.9f - head->xdata, -0.6f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.6f, 0.0f);
	glVertex3f(7.45f - head->xdata, -0.6f, 0.0f);
	glVertex3f(6.85f - head->xdata, -1.4f, 0.0f);
	glVertex3f(9.15f - head->xdata, -1.4f, 0.0f);

	glEnd();

	glColor3f(0.0, 0.3921f, 0.0f);
	glBegin(GL_TRIANGLES);

	glVertex3f(8.0f - head->xdata, 1.0f, 0.0f);
	glVertex3f(7.58f - head->xdata, 0.09f, 0.0f);
	glVertex3f(8.61f - head->xdata, 0.09f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(8.42f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.85f - head->xdata, 0.09f, 0.0f);
	glVertex3f(7.4f - head->xdata, -0.61f, 0.0f);
	glVertex3f(8.91f - head->xdata, -0.61f, 0.0f);

	glVertex3f(8.55f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.75f - head->xdata, -0.55f, 0.0f);
	glVertex3f(7.2f - head->xdata, -1.41f, 0.0f);
	glVertex3f(9.16f - head->xdata, -1.41f, 0.0f);

	glColor3f(0.4627f, 0.3607f, 0.2823f);
	glVertex3f(8.15f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85f - head->xdata, -1.41f, 0.0f);
	glVertex3f(7.85 - head->xdata, -2.27f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);

	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(9.35f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.22f, 0.0f);
	glVertex3f(8.15f - head->xdata, -2.27f, 0.0f);
	glVertex3f(9.35f - head->xdata, -2.27f, 0.0f);

	glEnd();
	glPopMatrix();
}

void scene4(void)
{
	royalEnfiled1();
	scenario2();
}