#pragma once
#include <GL/gl.h>
#include "home1.h"
#include "tv.h"
#include "sofa.h"
#include "humanoid.h"

// Scene2 camera related variables
float ex2 = 3.0f;
float ey2 = 0.0f;
float ez2 = 3.5f;
float cx2 = 0.0f;
float cy2 = -2.0f;
float cz2 = 0.0f;

void scene2(void)
{
	gluLookAt(ex2, ey2, ez2, cx2, cy2, cz2 - 10.0f, 0.0f, 1.0f, 0.0f);
	glPushMatrix();
	glScalef(4.0f, 4.0f, 4.0f);
	home1();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -2.3f, -2.08f);
	glScalef(2.5f, 2.5f, 3.5f);
	tv();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -3.3f, -3.5f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glScalef(1.5f, 1.5f, 2.0f);
	sofa();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -2.7f, -7.0f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//glScalef(1.0f, 1.0f, 1.0f);
	humanoid();
	glPopMatrix();
}