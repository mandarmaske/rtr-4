Step 1 -> a) in global space
		below #include <GL/gl.h>
		#include <GL/glu.h> // change 1 

	    b) below #pragma comment(lib, "OpenGL32.lib")
		 #pragma comment(lib, "glu32.lib") // change 2

Step 2 -> a) in resize() below glLoadIdentity() 
		if else block removed -> change 3
		gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f); // height should not be 0

Step 3 -> a) in display() below glClear(GL_COLOR_BUFFER_BIT);
		// change 4 - Perspective
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();