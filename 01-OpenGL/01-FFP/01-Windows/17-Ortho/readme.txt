Step 1 -> in resize() below glViewport()

// Step 1 ->
	glMatrixMode(GL_PROJECTION); // atta aamhi projection paahnaar aahot
	glLoadIdentity();

	if (width <= height)
	{
		glOrtho(-100.0f, // left
				100.0f,  // right
				(-100.0f * ((GLfloat) height / (GLfloat) width)), // bottom
				(100.0f * ((GLfloat) height / (GLfloat) width)), // top
				-100.0f, // near or front
				100.0f); // back
	}
	else // height <= width
	{
		glOrtho((-100.0f * ((GLfloat) width / (GLfloat) height)),
				(100.0f * ((GLfloat) width / (GLfloat) height)),
				-100.0f,
				100.0f,
				-100.0f,
				100.0f);
	}

Step 2 -> in display()

	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 50.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-50.0f, -50.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(50.0f, -50.0f, 0.0f);

	glEnd();

o/p - pahila brown color(Red Color interpolate zhalela color) aala mag resize kela ki triangle disla

Step 3 -> in initialize()

1st
// function declarations
	void resize(int, int); 

before return(0);
// warm up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

Step 4 -> in display() below glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();




