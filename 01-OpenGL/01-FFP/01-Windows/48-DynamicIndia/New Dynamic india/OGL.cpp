// header file
#include <Windows.h>
#include <stdio.h> // For File I/O (fopen(), fclose(), fprintf())
#include <stdlib.h> // For exit()
#include "OGL.h" // our header file
#include <math.h>

// OpenGL header files
#include <GL/gl.h>
#include <GL/glu.h> 

// macro definatio
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// OpenGL libraries
#pragma comment(lib, "OpenGL32.lib") // can also use for  user32.lib, gdi32.lib, kernel32.lib, if given here then no need write in link command
#pragma comment(lib, "glu32.lib") 

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void jetPlaneOrange(void);
void jetPlaneWhite(void);
void jetPlaneGreen(void);
void letterI1(void);
void letterA1(void);
void letterF1(void);
void LetterI(void);
void LetterN(void);
void LetterD(void);
void LetterI1(void);
void LetterA(void);
void circle(double radius_x, double radius_y, float start, float end);
void circle_s2(double radius_x, double radius_y, float start, float end);
void circleBorder(double radius_x, double radius_y, float start, float end);
void filled_circle(double radius_x, double radius_y, float start, float end);

// global variable declarations
BOOL gbActiveWindow = FALSE; // by default aapli window active nahi ahe
FILE* gpFile = NULL;
BOOL gbFullScreen = FALSE;
BOOL gbDblClk = FALSE;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL; // H - HANDLE, GL - OpenGL, RC - Rendering Context

// Colors 
float orangeR = 0.0f;
float orangeG = 0.0f;
float orangeB = 0.0f;
float whiteR = 0.0f;
float whiteG = 0.0f;
float whiteB = 0.0f;
float greenR = 0.0f;
float greenG = 0.0f;
float greenB = 0.0f;

// Letter Traversal
float xTravLetterI = -8.5f;
float yTravLetterN = 5.5f;
float yTravLetterI = -5.5f;
float xTravLetterA = 8.5f;

// plane movement variables
float j = 90.0f;
float angleOrange1 = 0.0f;
float angleOrange2 = 270.0f;
float angleGreen1 = 0.0f;
float angleGreen2 = 0.0f;
float xTrav = 0.0f;
float xTravWhitePlane = -16.0f;

float c1R = 0.0f;
float c1G = 0.0f;
float c2R = 0.0f;
float c2G = 0.0f;
float c3R = 0.0f;
float c3G = 0.0f;

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations -> not important in sequence as prototype but it is defined in which one gets called first
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iretVal = 0;

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) // if(file open secured() != 0)
	// aadhi fopen hota nusta
	{
		MessageBox(NULL, TEXT("Creation Of Log File Failed. Exitting..."), TEXT("File I/O Error"), MB_OK); // 1st parameter is NULL because we dont have our hwnd yet, so OS la ticha handle magitla message dakhvnyasathi ani aapan NULL/HWND_DESKTOP dila
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n"); // printf prints on console. fprintf prints in file
	}

	// initilizing WNDCLASSEX wndclass structure members
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS; // change
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); 
	wndclass.hInstance = hInstance;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Registering wndclass in OS's Class Registry
	RegisterClassEx(&wndclass);

	// creating our window in memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("Mandar Maske RTR2021-116 => Dynamic India"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // change
						((GetSystemMetrics(SM_CXSCREEN) - WIN_WIDTH) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - WIN_HEIGHT) / 2),
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	if (hwnd == NULL)
	{
		fprintf(gpFile, "hwnd Failed !!!\n\n");
	}
	else
	{
		fprintf(gpFile, "hwnd Created Successfully\n\n");
	}

	ghwnd = hwnd;

	// initialize
	iretVal = initialize();

	if (iretVal == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n\n");
		uninitialize();
	}
	else if (iretVal == -2)
	{
		fprintf(gpFile, "SetPixelFormat is Failed\n\n");
		uninitialize();
	}
	else if (iretVal == -3)
	{
		fprintf(gpFile, "CreateOpenGlContext is Failed\n\n");
		uninitialize();
	}

	else if (iretVal == -4)
	{
		fprintf(gpFile, "Making OpenGL Context As Current Context Failed\n\n");
		uninitialize();
	}
	else
	{
		fprintf(gpFile, "Initialize() Successful\n\n");
	}

	// Showing Window On Screen
	ShowWindow(hwnd, iCmdShow);

	PlaySound(MAKEINTRESOURCE(MYSONG), NULL, SND_ASYNC | SND_RESOURCE);

	// foregrounding(Z Order - Top) and focusing the window
	SetForegroundWindow(hwnd);

	SetFocus(hwnd); // sends message to our Window(wndproc) WM_SETFOCUS

	// Game Loop -> glutMainLoop() in glut
	while (bDone == FALSE) //  while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) // manually loop false kela
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else // Kontahi message nasel tevha ha else block run hoil
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}

	uninitialize(); // indra and takshaq saap

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	void resize(int, int);

	// variable declarations
	int retVal;

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_LBUTTONDBLCLK:

		if (gbDblClk == FALSE)
		{
			gbDblClk = TRUE;
		}
		else
		{
			gbDblClk = FALSE;
		}

		break;

	case WM_KEYDOWN:

		switch (wParam)
		{
		case 27:
			retVal = MessageBox(hwnd, TEXT("Are You Sure, You Want To Exit ?"), TEXT("Exit Message"), MB_YESNO);
			if (retVal == IDNO)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), No Button Is Pressed\n\n");
				break;
			}
				
			else if(retVal == IDYES)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), Yes Button Is Pressed. Program Is Now Exitting !!!\n\n");
				DestroyWindow(hwnd);
				break;
			}

		default:
			break;
		}
	break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		default:
			break;
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); // resize(width, height)
		break;

	case WM_CLOSE: // close button - discipline mhanun lihane, WM_CLOSE cha call WM_DESTROY chya aadhi yeto
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle; // static to sustain values accross this function
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT); // like wndclass.cbSize =  sizeof(WNDCLASSEX)

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) // if (1 & WS_OVERLAPPEDWINDOW) -> if (1 & 1) then if ya madhe
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) // if(1 && 1) then in if
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW); // WS_OVERLAPPEDWINDOW che 5 style(WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_THICKFRAME,  WS_SYSMENU, WS_CAPTION) nako except WS_OVERLAPPED 
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW); // kadhayla ~WS_OVERLAPPED kela, Add karnyasathi Bitwise Or Waparala
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED); // x, y, width, height ignore -> Zero Why? - wp ne already SetWindowPlacement pahije tithe aaplya window la place kelay so 0 

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	
	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialization of PIXELFORMATDESCRIPTOR structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	// GetDC
	ghdc = GetDC(ghwnd);

	// choose pixel format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	// set the choosen pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
		return(-2);

	// Create OpenGL Rendering Context - first bridging API
	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
		return(-3);

	// make the rendering context as the current context
	if (wglMakeCurrent(ghdc, ghrc) == FALSE) // wglMakeCurrent() - 2nd bridging API
		return(-4);

	// Here Starts OpenGL Code...
	// clear the screen using black color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // 4th parameter alpha color

	// warm up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// code

	// kitihi height kami keli ki height 0 nasayla havi kamit kami 1 havi
	// To Avoid divided by 0(illegal instruction / code) in future code
	if (height == 0) 
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION); // atta aamhi projection paahnaar aahot
	glLoadIdentity();

	// if else block removed -> change 3

	gluPerspective(45.0f, 
				 (GLfloat)width 
		         / (GLfloat)height,
				 0.1f,
		         100.0f); 
	// height should not be 0
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	if (angleGreen2 >= -120.0f)
	{
		glLoadIdentity();
		glTranslatef(xTravLetterI, -0.1f, -10.0f);
		LetterI();

		glLoadIdentity();
		glTranslatef(-2.0f, yTravLetterN, -10.0f);
		LetterN();

		glLoadIdentity();
		glTranslatef(0.0f, -0.1f, -10.0f);
		LetterD();

		glLoadIdentity();
		glTranslatef(2.0f, yTravLetterI, -10.0f);
		LetterI1();

		glLoadIdentity();
		glTranslatef(xTravLetterA, -0.1f, -10.0f);
		LetterA();

		if (orangeR >= 1.0f)
		{
			glLoadIdentity();
			glTranslatef(-5.5f, 6.0f, -19.0f);
			glRotatef(angleOrange1, 0.0f, 0.0f, 1.0f);
			glTranslatef((cos(2 * 3.14 * j / 180) * 0.2f) - 6.0f, (sin(2 * 3.14 * j / 180) * 1.0f) + 2.5f, 0.0f);
			glRotatef(270, 0.0f, 0.0f, 1.0f);

			if (angleOrange1 >= 90.0f)
			{
				glTranslatef(xTrav, 0.0f, 0.0f);
				if (xTrav >= 16.0f)
				{
					glTranslatef(-2.5f, 7.0f, 0.0f);
					glRotatef(angleOrange2, 0.0f, 0.0f, 1.0f);
					glTranslatef((sin(2 * 3.14 * j / 360) * 4.5f) + 2.5f, (cos(2 * 3.14 * j / 360) * 0.2f) + 2.5f, 0.0f);
					glRotatef(90, 0.0f, 0.0f, 1.0f);
				}
			}

			jetPlaneOrange();

			glLoadIdentity();
			glTranslatef(xTravWhitePlane, -0.2f, -19.0f);
			jetPlaneWhite();

			glLoadIdentity();
			glTranslatef(-5.5f, -6.4f, -19.0f);
			glRotatef(angleGreen1, 0.0f, 0.0f, 1.0f);
			glTranslatef((cos(2 * 3.14 * j / 180) * 0.2f) - 6.0f, (sin(2 * 3.14 * j / 180) * 1.0f) - 2.5f, 0.0f);
			glRotatef(90, 0.0f, 0.0f, 1.0f);

			if (angleGreen1 <= -90.0f)
			{
				glTranslatef(xTrav, 0.0f, 0.0f);
				if (xTrav >= 16.0f)
				{
					glTranslatef(-2.8f, -6.5f, 0.0f);
					glRotatef(angleGreen2, 0.0f, 0.0f, 1.0f);
					glTranslatef((cos(2 * 3.14 * j / 180) * 0.2f) + 3.3f, (sin(2 * 3.14 * j / 180) * 2.5f) + 6.5f, 0.0f);
					glRotatef(360, 0.0f, 0.0f, 1.0f);
				}
			}

			jetPlaneGreen();
		}
	}

	if (angleGreen2 <= -120.0f)
	{
		glLoadIdentity();

		glTranslatef(0.0f, -4.0f, -50.0f);
		//glRotatef(180, 0.0f, 1.0f, 0.0f);
		//glRotatef(270, 0.0f, 0.0f, 1.0f);


		glBegin(GL_QUADS);

		glColor3f(c1R, c1G, 0.0f);
		//glColor3f(0.8f, 0.9f, 0.0f);
		glVertex3f(50.0f, 25.5f, 0.0f);
		glVertex3f(-50.0f, 25.5f, 0.0f);
		glColor3f(c2R, c2G, 0.0f);
		//glColor3f(1.0f, 0.42f, 0.0f);
		glVertex3f(-50.0f, 5.0f, 0.0f);
		glVertex3f(50.0f, 5.0f, 0.0f);

		glVertex3f(50.0f, 5.0f, 0.0f);
		glVertex3f(-50.0f, 5.0f, 0.0f);
		glColor3f(c3R, c3G, 0.0f);
		//glColor3f(1.0f, 0.3f, 0.0f);
		glVertex3f(-50.0f, -5.0f, 0.0f);
		glVertex3f(50.0f, -5.0f, 0.0f);

		glVertex3f(50.0f, -5.0f, 0.0f);
		glVertex3f(-50.0f, -5.0f, 0.0f);
		//glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-50.0f, -17.5f, 0.0f);
		glVertex3f(50.0f, -17.5f, 0.0f);

		glEnd();

		glColor3f(0.8f, 0.9f, 0.0f);
		glColor3f(1.0f, 0.42f, 0.0f);
		glColor3f(1.0f, 0.3f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);

		glBegin(GL_QUADS);

		glVertex3f(2.2f, -3.0f, 0.0f);
		glVertex3f(-2.2f, -3.0f, 0.0f);
		glVertex3f(-2.2f, -4.5f, 0.0f);
		glVertex3f(2.2f, -4.5f, 0.0f);

		glVertex3f(4.5f, -4.5f, 0.0f);
		glVertex3f(-4.5f, -4.5f, 0.0f);
		glVertex3f(-5.5f, -6.0f, 0.0f);
		glVertex3f(5.5f, -6.0f, 0.0f);

		glVertex3f(7.5f, -6.0f, 0.0f);
		glVertex3f(-7.5f, -6.0f, 0.0f);
		glVertex3f(-9.5f, -17.5f, 0.0f);
		glVertex3f(9.5f, -17.5f, 0.0f);

		glVertex3f(0.2f, -2.2f, 0.0f);
		glVertex3f(-0.2f, -2.2f, 0.0f);
		glVertex3f(-0.2f, -3.0f, 0.0f);
		glVertex3f(0.2f, -3.0f, 0.0f);

		glVertex3f(0.15f, -0.5f, 0.0f);
		glVertex3f(-0.15f, -0.5f, 0.0f);
		glVertex3f(-0.15f, -2.2f, 0.0f);
		glVertex3f(0.15f, -2.2f, 0.0f);

		glVertex3f(0.3f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.5f, 0.0f);
		glVertex3f(-0.15f, -0.5f, 0.0f);
		glVertex3f(0.15f, -0.5f, 0.0f);

		glVertex3f(0.6f, 0.6f, 0.0f);
		glVertex3f(0.25f, 0.6f, 0.0f);
		glVertex3f(0.25f, 0.4f, 0.0f);
		glVertex3f(0.6f, 0.4f, 0.0f);

		glVertex3f(-0.25f, 0.6f, 0.0f);
		glVertex3f(-0.45f, 0.6f, 0.0f);
		glVertex3f(-0.45f, 0.4f, 0.0f);
		glVertex3f(-0.25f, 0.4f, 0.0f);

		glVertex3f(-0.45f, 0.4f, 0.0f);
		glVertex3f(-0.45f, 0.6f, 0.0f);
		glVertex3f(-1.4f, -0.4f, 0.0f);
		glVertex3f(-1.2f, -0.4f, 0.0f);

		glVertex3f(0.4f, -0.2f, 0.0f);
		glVertex3f(0.15f, -0.2f, 0.0f);
		glVertex3f(0.15f, -0.4f, 0.0f);
		glVertex3f(0.4f, -0.4f, 0.0f);

		glVertex3f(-0.15f, -0.2f, 0.0f);
		glVertex3f(-1.2f, -0.2f, 0.0f);
		glVertex3f(-1.2f, -0.4f, 0.0f);
		glVertex3f(-0.15f, -0.4f, 0.0f);

		glVertex3f(0.6f, -0.1f, 0.0f);
		glVertex3f(0.15f, -0.1f, 0.0f);
		glVertex3f(0.15f, -0.25f, 0.0f);
		glVertex3f(0.6f, -0.25f, 0.0f);

		glVertex3f(-0.15f, -0.1f, 0.0f);
		glVertex3f(-1.0f, -0.1f, 0.0f);
		glVertex3f(-1.0f, -0.2f, 0.0f);
		glVertex3f(-0.15f, -0.2f, 0.0f);


		glVertex3f(0.4f, 0.7f, 0.0f);
		glVertex3f(-0.35f, 0.7f, 0.0f);
		glVertex3f(-0.35f, 0.5f, 0.0f);
		glVertex3f(0.4f, 0.5f, 0.0f);

		glVertex3f(0.47f, 3.0f, 0.0f);
		glVertex3f(-0.47f, 3.0f, 0.0f);
		glVertex3f(-0.4f, 0.7f, 0.0f);
		glVertex3f(0.4f, 0.7f, 0.0f);


		glVertex3f(0.35f, 3.6f, 0.0f);
		glVertex3f(-0.35f, 3.5f, 0.0f);
		glVertex3f(-0.35f, 3.0f, 0.0f);
		glVertex3f(0.35f, 3.0f, 0.0f);

		glVertex3f(0.5f, 3.3f, 0.0f);
		glVertex3f(0.15f, 3.35f, 0.0f);
		glVertex3f(0.15f, 3.1f, 0.0f);
		glVertex3f(0.5f, 3.2f, 0.0f);

		//glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.75f, 8.0f, 0.0f);
		glVertex3f(-0.4f, 8.0f, 0.0f);
		glVertex3f(-0.4f, 3.4f, 0.0f);
		glVertex3f(0.6f, 3.6f, 0.0f);

		glVertex3f(0.45f, 10.0f, 0.0f);
		glVertex3f(-0.4f, 10.0f, 0.0f);
		glVertex3f(-0.4f, 8.0f, 0.0f);
		glVertex3f(0.45f, 8.0f, 0.0f);

		//glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.55f, 11.0f, 0.0f);
		glVertex3f(-0.55f, 11.0f, 0.0f);
		glVertex3f(-0.45f, 10.0f, 0.0f);
		glVertex3f(0.45f, 10.0f, 0.0f);

		glVertex3f(0.75f, 11.3f, 0.0f);
		glVertex3f(-0.55f, 11.3f, 0.0f);
		glVertex3f(-0.55f, 11.0f, 0.0f);
		glVertex3f(0.75f, 11.0f, 0.0f);

		glVertex3f(0.95f, 12.0f, 0.0f);
		glVertex3f(-0.55f, 12.0f, 0.0f);
		glVertex3f(-0.55f, 11.3f, 0.0f);
		glVertex3f(0.95f, 11.3f, 0.0f);

		//	glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(2.8f, 13.2f, 0.0f);
		glVertex3f(1.5f, 13.0f, 0.0f);
		glVertex3f(-2.3f, 9.9f, 0.0f);
		glVertex3f(-1.7f, 9.86f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.3f, 8.03f, 0.0f);
		glVertex3f(-1.0f, 7.4f, 0.0f);
		glVertex3f(-1.0f, 6.5f, 0.0f);
		glVertex3f(-0.3f, 6.5f, 0.0f);

		//glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.9f, 7.41f, 0.0f);
		glVertex3f(-1.5f, 7.15f, 0.0f);
		glVertex3f(-1.5f, 6.53f, 0.0f);
		glVertex3f(-0.9f, 6.53f, 0.0f);

		//	glColor3f(0.0f, 0.0f, 0.0f); 
		glVertex3f(-0.4f, 7.0f, 0.0f);
		glVertex3f(-0.78, 7.0f, 0.0f);
		glVertex3f(-0.74f, 3.6f, 0.0f);
		glVertex3f(-0.4f, 3.6f, 0.0f);

		glVertex3f(-0.74f, 4.0f, 0.0f);
		glVertex3f(-1.0f, 3.9f, 0.0f);
		glVertex3f(-1.0f, 3.7f, 0.0f);
		glVertex3f(-0.74f, 3.6f, 0.0f);


		glVertex3f(-1.0f, 3.8f, 0.0f);
		glVertex3f(-1.2f, 3.6f, 0.0f);
		glVertex3f(-0.7f, 3.6f, 0.0f);
		glVertex3f(-1.0f, 3.8f, 0.0f);

		glVertex3f(-0.9f, 3.8f, 0.0f);
		glVertex3f(-1.2f, 4.0f, 0.0f);
		glVertex3f(-1.2f, 3.6f, 0.0f);
		glVertex3f(-0.9f, 3.8f, 0.0f);


		glVertex3f(-0.9f, 6.7f, 0.0f);
		glVertex3f(-1.4f, 7.0f, 0.0f);
		glVertex3f(-1.4f, 6.4f, 0.0f);
		glVertex3f(-0.9f, 6.6f, 0.0f);


		glVertex3f(-1.2f, 6.9f, 0.0f);
		glVertex3f(-1.48f, 7.0f, 0.0f);
		glVertex3f(-1.38f, 3.8f, 0.0f);
		glVertex3f(-1.2f, 3.6f, 0.0f);

		//glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.74f, 7.0f, 0.0f);
		glVertex3f(-1.0f, 6.7f, 0.0f);
		glVertex3f(-1.0f, 6.6f, 0.0f);
		glVertex3f(-0.74f, 6.4f, 0.0f);

		glVertex3f(0.7f, 7.0f, 0.0f);
		glVertex3f(0.55f, 5.0f, 0.0f);
		glVertex3f(0.6f, 3.6f, 0.0f);
		glVertex3f(0.78f, 3.8f, 0.0f);

		glVertex3f(0.82f, 8.0f, 0.0f);
		glVertex3f(0.7f, 8.0f, 0.0f);
		glVertex3f(0.7f, 3.8f, 0.0f);
		glVertex3f(0.82f, 3.8f, 0.0f);

		//glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(2.5f, 5.3f, 0.0f);
		glVertex3f(0.82f, 5.2f, 0.0f);
		glVertex3f(0.82f, 3.8f, 0.0f);
		glVertex3f(2.5f, 4.0f, 0.0f);

		glVertex3f(3.2f, 5.1f, 0.0f);
		glVertex3f(2.5f, 5.3f, 0.0f);
		glVertex3f(2.5f, 4.0f, 0.0f);
		glVertex3f(3.2f, 3.8f, 0.0f);


		glVertex3f(3.8f, 4.8f, 0.0f);
		glVertex3f(3.2f, 5.1f, 0.0f);
		glVertex3f(3.2f, 3.78f, 0.0f);
		glVertex3f(3.8f, 3.45f, 0.0f);


		glVertex3f(1.6f, 3.9f, 0.0f);
		glVertex3f(1.2f, 3.87f, 0.0f);
		glVertex3f(1.32f, 3.67f, 0.0f);
		glVertex3f(1.47f, 3.67f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		//glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(1.2f, 7.7f, 0.0f);
		glVertex3f(0.82f, 7.8f, 0.0f);
		glVertex3f(0.82f, 4.8f, 0.0f);
		glVertex3f(1.2f, 5.0f, 0.0f);

		//glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(2.9f, 8.3f, 0.0f);
		glVertex3f(1.2f, 7.7f, 0.0f);
		glVertex3f(1.2f, 6.35f, 0.0f);
		glVertex3f(2.1f, 6.7f, 0.0f);

		//glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(3.5f, 8.5f, 0.0f);
		glVertex3f(2.9f, 8.3f, 0.0f);
		glVertex3f(2.0f, 6.9f, 0.0f);
		glVertex3f(2.4, 6.8f, 0.0f);

		//glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(3.65f, 8.3f, 0.0f);
		glVertex3f(3.5f, 8.5f, 0.0f);
		glVertex3f(2.4f, 6.8f, 0.0f);
		glVertex3f(3.4f, 7.2f, 0.0f);

		glEnd();

		circle_s2(0.6f, 0.7f, 1.4f, 5.77f);
		circle_s2(0.5f, 0.7f, 1.42f, 5.77f);

		glLineWidth(6);

		glBegin(GL_LINES);

		glVertex3f(1.1f, 5.95f, 0.0f);
		glVertex3f(1.5f, 5.9f, 0.0f);

		glVertex3f(1.5f, 5.9f, 0.0f);
		glVertex3f(1.8f, 5.65f, 0.0f);

		glEnd();

		glRotatef(40, 0.0f, 0.0f, 1.0f);
		filled_circle(2.45f, 2.8f, 7.5f, 8.6f);
	}
	
	SwapBuffers(ghdc); // native 
}

void LetterI(void)
{
	glBegin(GL_QUADS);

	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Middle Top Bar
	glVertex3f(0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.75f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(0.14f, 0.01f, 0.0f);

	// Middle Middle Bar

	glVertex3f(0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(0.14f, -0.01f, 0.0f);

	// Middle Bottom Bar
	glVertex3f(0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.14f, -0.75f, 0.0f);
	glVertex3f(0.14f, -0.75f, 0.0f);

	//glVertex3f(-0.15f, -0.75f, 0.0f);
	//glVertex3f(0.15f, -0.75f, 0.0f);

	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();
}

void LetterN(void)
{
	glBegin(GL_QUADS);

	// Left Bar
	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.55f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, 0.01f, 0.0f);
	glVertex3f(-0.55f, 0.01f, 0.0f);

	// Middle Bar
	glVertex3f(-0.55f, 0.01f, 0.0f);
	glVertex3f(-0.8f, 0.01f, 0.0f);
	glVertex3f(-0.8f, -0.01f, 0.0f);
	glVertex3f(-0.55f, -0.01f, 0.0f);

	// Left Bar Down
	glVertex3f(-0.55f, -0.01f, 0.0f);
	glVertex3f(-0.8f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.55f, -1.0f, 0.0f);

	// Right Bar
	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.55f, 1.0f, 0.0f);
	glVertex3f(0.8f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.01f, 0.0f);
	glVertex3f(0.55f, 0.01f, 0.0f);

	// Middle Bar
	glVertex3f(0.55f, 0.01f, 0.0f);
	glVertex3f(0.8f, 0.01f, 0.0f);
	glVertex3f(0.8f, -0.01f, 0.0f);
	glVertex3f(0.55f, -0.01f, 0.0f);

	// Left Bar Down
	glVertex3f(0.55f, -0.01f, 0.0f);
	glVertex3f(0.8f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(0.55f, -1.0f, 0.0f);

	// Middle Tilted Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-0.5f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-0.15f, 0.01f, 0.0f);
	glVertex3f(0.15f, 0.01f, 0.0f);

	glVertex3f(0.15f, 0.01f, 0.0f);
	glVertex3f(-0.15f, 0.01f, 0.0f);
	glVertex3f(-0.138f, -0.01f, 0.0f);
	glVertex3f(0.162f, -0.01f, 0.0f);

	glVertex3f(0.162f, -0.01f, 0.0f);
	glVertex3f(-0.138f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.8f, -1.0f, 0.0f);


	glEnd();
}

void LetterI1(void)
{
	glBegin(GL_QUADS);

	// Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Middle Top Bar
	glVertex3f(0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.75f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(0.14f, 0.01f, 0.0f);

	// Middle Middle Bar

	glVertex3f(0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(0.14f, -0.01f, 0.0f);

	// Middle Bottom Bar
	glVertex3f(0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.14f, -0.75f, 0.0f);
	glVertex3f(0.14f, -0.75f, 0.0f);

	//glVertex3f(-0.15f, -0.75f, 0.0f);
	//glVertex3f(0.15f, -0.75f, 0.0f);

	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();
}

void LetterD(void)
{
	glBegin(GL_QUADS);

	// Top Bar
	//glColor3f(1.0f, 0.6f, 0.2f);
	glColor3f(orangeR, orangeG, orangeB);
	glVertex3f(0.55f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Left Bar
	glVertex3f(-0.3f, 0.75f, 0.0f);
	glVertex3f(-0.55f, 0.75f, 0.0f);
	//glColor3f(1.0f, 1.0f, 1.0f);
	glColor3f(whiteR, whiteG, whiteB);
	glVertex3f(-0.55f, 0.01f, 0.0f);
	glVertex3f(-0.3f, 0.01, 0.0f);

	// Left Bar Middle
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glColor3f(whiteR, whiteG, whiteB);
	glVertex3f(-0.3f, 0.01, 0.0f);
	glVertex3f(-0.55f, 0.01f, 0.0f);
	glVertex3f(-0.55f, -0.01f, 0.0f);
	glVertex3f(-0.3f, -0.01, 0.0f);

	// Left Bar Bottom
	glVertex3f(-0.3f, -0.01, 0.0f);
	glVertex3f(-0.55f, -0.01f, 0.0f);
	glColor3f(greenR, greenG, greenB);
	//glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.55f, -0.75f, 0.0f);
	glVertex3f(-0.3f, -0.75f, 0.0f);

	// Right Bar Top
	glColor3f(orangeR, orangeG, orangeB);
	glVertex3f(0.8f, 0.75f, 0.0f);
	glVertex3f(0.55f, 0.75f, 0.0f);
	glColor3f(whiteR, whiteG, whiteB);
	glVertex3f(0.55f, 0.01f, 0.0f);
	glVertex3f(0.8f, 0.01, 0.0f);

	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8f, 0.01, 0.0f);
	glVertex3f(0.55f, 0.01f, 0.0f);
	glVertex3f(0.55f, -0.01f, 0.0f);
	glVertex3f(0.8f, -0.01, 0.0f);

	glVertex3f(0.8f, -0.01, 0.0f);
	glVertex3f(0.55f, -0.01f, 0.0f);
	glColor3f(greenR, greenG, greenB);
	//glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.55f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	// Bottom Bar
	glVertex3f(0.55f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();
}

void LetterA(void)
{
	glBegin(GL_QUADS);

	// Left Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glVertex3f(-0.258f, -0.01f, 0.0f);

	glVertex3f(-0.258f, -0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-0.854f, -1.0f, 0.0f);
	glVertex3f(-0.6f, -1.0f, 0.0f);

	// Right Top Bar
	// Left Top Bar
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.495f, 0.01f, 0.0f);

	glVertex3f(0.495f, 0.01f, 0.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glVertex3f(0.503f, -0.01f, 0.0f);

	glVertex3f(0.503f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(0.6f, -1.0f, 0.0f);
	glVertex3f(0.854f, -1.0f, 0.0f);

	if (xTravWhitePlane >= 7.5f)
	{
		// Middle Bar
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(0.222f, 0.1f, 0.0f);
		glVertex3f(-0.222f, 0.1f, 0.0f);
		glVertex3f(-0.248f, 0.03f, 0.0f);
		glVertex3f(0.248f, 0.03f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.248f, 0.03f, 0.0f);
		glVertex3f(-0.248f, 0.03f, 0.0f);
		glVertex3f(-0.268f, -0.03f, 0.0f);
		glVertex3f(0.268f, -0.03f, 0.0f);

		glColor3f(0.0745f, 0.5333f, 0.3137f);
		glVertex3f(0.268f, -0.03f, 0.0f);
		glVertex3f(-0.268f, -0.03f, 0.0f);
		glVertex3f(-0.289f, -0.1f, 0.0f);
		glVertex3f(0.289f, -0.1f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
	}

	glEnd();
}

void jetPlaneOrange(void)
{
	glBegin(GL_QUADS);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.1f, 0.005f, 0.0f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);
	glVertex3f(1.1f, -0.005f, 0.0f);

	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	//glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);

	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);

	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);

	glVertex3f(-0.4f, 0.865f, 0.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.4f, 0.86f, 0.0f);

	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);

	glVertex3f(-0.4f, -0.865f, 0.0f);
	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.4f, -0.86f, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.9f, 0.85f, 0.0f);
	glVertex3f(-0.9f, -0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-0.9, -0.2f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.7176f, 0.7176f, 0.7098f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-2.1f, 0.07f, 0.0f);
	glVertex3f(-2.1f, -0.07f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.45f, 0.5f, 0.0f);
	glVertex3f(-1.68f, 0.5f, 0.0f);
	glVertex3f(-1.74f, 0.45f, 0.0f);
	glVertex3f(-1.74f, 0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.68f, 0.25f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glEnd();

	//glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.45f, -0.5f, 0.0f);
	glVertex3f(-1.68f, -0.5f, 0.0f);
	glVertex3f(-1.74f, -0.45f, 0.0f);
	glVertex3f(-1.74f, -0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.68f, -0.25f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glEnd();

	circle(0.2f, 0.06f, 0, 0);
	circleBorder(0.2f, 0.06f, 0, 0);

	//glLineWidth(9);

	glColor3f(0.3647f, 0.5411f, 0.6588f);
	glBegin(GL_LINES);

	glVertex3f(-1.5f, 0.0f, 0.0f);
	glVertex3f(-1.1f, 0.0f, 0.0f);

	glEnd();

	//glLineWidth(1.5f);

	glBegin(GL_LINES);

	glColor3f(0.5176f, 0.5176f, 0.5098f);

	glVertex3f(1.1f, 0.0f, 0.0f);
	glVertex3f(1.2f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);

	glVertex3f(-0.9f, -0.2f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);

	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glVertex3f(-1.2f, 0.17f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.17f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);

	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	glVertex3f(-1.4f, 0.17f, 0.0f);
	glVertex3f(-1.4f, -0.17f, 0.0f);

	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);

	glVertex3f(-1.68f, 0.08f, 0.0f);
	glVertex3f(-1.78f, 0.055f, 0.0f);

	glVertex3f(-1.68f, 0.15f, 0.0f);
	glVertex3f(-1.68f, -0.15f, 0.0f);

	glVertex3f(-1.68f, -0.08f, 0.0f);
	glVertex3f(-1.78f, -0.055f, 0.0f);

	glVertex3f(-1.68f, 0.0f, 0.0f);
	glVertex3f(-1.78f, 0.0f, 0.0f);

	glVertex3f(0.25f, 0.06f, 0.0f);
	glVertex3f(0.25f, -0.06f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.1f, 0.01f, 0.0f);

	glVertex3f(-1.1f, 0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, -0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.5f, -0.01f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-1.53f, 0.0f, 0.0f);
	glVertex3f(-1.23f, 0.0f, 0.0f);

	glEnd();

	glScalef(0.08f, 0.08f, 0.0f);
	glTranslatef(-8.0f, 0.0f, -30.0f);
	letterI1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterA1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterF1();
}

void jetPlaneWhite(void)
{
	glBegin(GL_QUADS);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.1f, 0.005f, 0.0f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);
	glVertex3f(1.1f, -0.005f, 0.0f);

	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	//glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);

	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);

	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);

	glVertex3f(-0.4f, 0.865f, 0.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.4f, 0.86f, 0.0f);

	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);

	glVertex3f(-0.4f, -0.865f, 0.0f);
	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.4f, -0.86f, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.9f, 0.85f, 0.0f);
	glVertex3f(-0.9f, -0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-0.9, -0.2f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.7176f, 0.7176f, 0.7098f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-2.1f, 0.07f, 0.0f);
	glVertex3f(-2.1f, -0.07f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.45f, 0.5f, 0.0f);
	glVertex3f(-1.68f, 0.5f, 0.0f);
	glVertex3f(-1.74f, 0.45f, 0.0f);
	glVertex3f(-1.74f, 0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.68f, 0.25f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glEnd();

	//glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.45f, -0.5f, 0.0f);
	glVertex3f(-1.68f, -0.5f, 0.0f);
	glVertex3f(-1.74f, -0.45f, 0.0f);
	glVertex3f(-1.74f, -0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.68f, -0.25f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glEnd();

	circle(0.2f, 0.06f, 0, 0);
	circleBorder(0.2f, 0.06f, 0, 0);

	//glLineWidth(9);

	glColor3f(0.3647f, 0.5411f, 0.6588f);
	glBegin(GL_LINES);

	glVertex3f(-1.5f, 0.0f, 0.0f);
	glVertex3f(-1.1f, 0.0f, 0.0f);

	glEnd();

	//glLineWidth(1.5f);

	glBegin(GL_LINES);

	glColor3f(0.5176f, 0.5176f, 0.5098f);

	glVertex3f(1.1f, 0.0f, 0.0f);
	glVertex3f(1.2f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);

	glVertex3f(-0.9f, -0.2f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);

	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glVertex3f(-1.2f, 0.17f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.17f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);

	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	glVertex3f(-1.4f, 0.17f, 0.0f);
	glVertex3f(-1.4f, -0.17f, 0.0f);

	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);

	glVertex3f(-1.68f, 0.08f, 0.0f);
	glVertex3f(-1.78f, 0.055f, 0.0f);

	glVertex3f(-1.68f, 0.15f, 0.0f);
	glVertex3f(-1.68f, -0.15f, 0.0f);

	glVertex3f(-1.68f, -0.08f, 0.0f);
	glVertex3f(-1.78f, -0.055f, 0.0f);

	glVertex3f(-1.68f, 0.0f, 0.0f);
	glVertex3f(-1.78f, 0.0f, 0.0f);

	glVertex3f(0.25f, 0.06f, 0.0f);
	glVertex3f(0.25f, -0.06f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.1f, 0.01f, 0.0f);

	glVertex3f(-1.1f, 0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, -0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.5f, -0.01f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-1.53f, 0.0f, 0.0f);
	glVertex3f(-1.23f, 0.0f, 0.0f);

	glEnd();

	glScalef(0.08f, 0.08f, 0.0f);
	glTranslatef(-8.0f, 0.0f, -30.0f);
	letterI1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterA1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterF1();
}

void jetPlaneGreen(void)
{
	glBegin(GL_QUADS);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.1f, 0.005f, 0.0f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);
	glVertex3f(1.1f, -0.005f, 0.0f);

	glColor3f(0.5176f, 0.5176f, 0.5098f);
	glVertex3f(1.0f, 0.05f, 0.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);
	glVertex3f(1.0f, -0.05, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	//glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);
	glVertex3f(0.7f, -0.1f, 0.0f);

	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.17f, 0.0f);
	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);
	glVertex3f(0.35f, -0.17f, 0.0f);

	glVertex3f(0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);
	glVertex3f(0.05f, -0.25f, 0.0f);

	//glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.875f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);

	glVertex3f(-0.4f, 0.865f, 0.0f);
	glVertex3f(-0.45f, 0.875f, 0.0f);
	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.4f, 0.86f, 0.0f);

	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.875f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);

	glVertex3f(-0.4f, -0.865f, 0.0f);
	glVertex3f(-0.45f, -0.875f, 0.0f);
	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.4f, -0.86f, 0.0f);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	//glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.6f, 0.85f, 0.0f);
	glVertex3f(-0.9f, 0.85f, 0.0f);
	glVertex3f(-0.9f, -0.85f, 0.0f);
	glVertex3f(-0.6f, -0.85f, 0.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-0.9, -0.2f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.2f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.7176f, 0.7176f, 0.7098f);
	glVertex3f(-1.4f, 0.2f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.4f, -0.2f, 0.0f);

	glColor3f(0.0745f, 0.5333f, 0.3137f);
	glVertex3f(-1.78f, -0.1f, 0.0f);
	glVertex3f(-1.78f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-2.1f, 0.07f, 0.0f);
	glVertex3f(-2.1f, -0.07f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

	glColor3f(0.5176f, 0.7215f, 0.78431f);
	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.45f, 0.5f, 0.0f);
	glVertex3f(-1.68f, 0.5f, 0.0f);
	glVertex3f(-1.74f, 0.45f, 0.0f);
	glVertex3f(-1.74f, 0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.68f, 0.25f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glEnd();

	//glColor3f(1.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.45f, -0.5f, 0.0f);
	glVertex3f(-1.68f, -0.5f, 0.0f);
	glVertex3f(-1.74f, -0.45f, 0.0f);
	glVertex3f(-1.74f, -0.22f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.68f, -0.25f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glEnd();

	circle(0.2f, 0.06f, 0, 0);
	circleBorder(0.2f, 0.06f, 0, 0);

	//glLineWidth(9);

	glColor3f(0.3647f, 0.5411f, 0.6588f);
	glBegin(GL_LINES);

	glVertex3f(-1.5f, 0.0f, 0.0f);
	glVertex3f(-1.1f, 0.0f, 0.0f);

	glEnd();

	//glLineWidth(1.5f);

	glBegin(GL_LINES);

	glColor3f(0.5176f, 0.5176f, 0.5098f);

	glVertex3f(1.1f, 0.0f, 0.0f);
	glVertex3f(1.2f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex3f(-0.9f, 0.2f, 0.0f);
	glVertex3f(-0.05f, 0.25f, 0.0f);

	glVertex3f(-0.9f, -0.2f, 0.0f);
	glVertex3f(-0.05f, -0.25f, 0.0f);

	glVertex3f(-0.45f, 0.845f, 0.0f);
	glVertex3f(-0.95f, 0.845f, 0.0f);

	glVertex3f(-0.45f, -0.845f, 0.0f);
	glVertex3f(-0.95f, -0.845f, 0.0f);

	glVertex3f(-1.2f, 0.2f, 0.0f);
	glVertex3f(-1.2f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.2f, 0.0f);
	glVertex3f(-1.2f, -0.17f, 0.0f);

	glVertex3f(-1.2f, 0.17f, 0.0f);
	glVertex3f(-1.68f, 0.17f, 0.0f);

	glVertex3f(-1.2f, -0.17f, 0.0f);
	glVertex3f(-1.68f, -0.17f, 0.0f);

	glVertex3f(0.75f, 0.09f, 0.0f);
	glVertex3f(0.75f, -0.09f, 0.0f);

	glVertex3f(-1.4f, 0.17f, 0.0f);
	glVertex3f(-1.4f, -0.17f, 0.0f);

	glVertex3f(-1.78f, 0.1f, 0.0f);
	glVertex3f(-1.78f, -0.1f, 0.0f);

	glVertex3f(-1.68f, 0.08f, 0.0f);
	glVertex3f(-1.78f, 0.055f, 0.0f);

	glVertex3f(-1.68f, 0.15f, 0.0f);
	glVertex3f(-1.68f, -0.15f, 0.0f);

	glVertex3f(-1.68f, -0.08f, 0.0f);
	glVertex3f(-1.78f, -0.055f, 0.0f);

	glVertex3f(-1.68f, 0.0f, 0.0f);
	glVertex3f(-1.78f, 0.0f, 0.0f);

	glVertex3f(0.25f, 0.06f, 0.0f);
	glVertex3f(0.25f, -0.06f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.1f, 0.01f, 0.0f);

	glVertex3f(-1.1f, 0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, -0.01f, 0.0f);
	glVertex3f(-1.1f, -0.01f, 0.0f);

	glVertex3f(-1.5f, 0.01f, 0.0f);
	glVertex3f(-1.5f, -0.01f, 0.0f);

	glColor3f(0.8625f, 0.9625f, 1.0f);
	glVertex3f(-1.53f, 0.0f, 0.0f);
	glVertex3f(-1.23f, 0.0f, 0.0f);

	glEnd();

	glScalef(0.08f, 0.08f, 0.0f);
	glTranslatef(-8.0f, 0.0f, -30.0f);
	letterI1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterA1();

	glTranslatef(2.0f, 0.0f, 0.0f);
	letterF1();
}


void letterI1(void)
{
	glColor3f(0.0f, 0.0f, 0.5019f);
	glBegin(GL_QUADS);

	// I
	// Top Bar
	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	// Middle Top Bar
	glVertex3f(0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.75f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(0.14f, 0.01f, 0.0f);

	// Middle Middle Bar

	glVertex3f(0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, 0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(0.14f, -0.01f, 0.0f);

	// Middle Bottom Bar
	glVertex3f(0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.01f, 0.0f);
	glVertex3f(-0.14f, -0.75f, 0.0f);
	glVertex3f(0.14f, -0.75f, 0.0f);

	//glVertex3f(-0.15f, -0.75f, 0.0f);
	//glVertex3f(0.15f, -0.75f, 0.0f);

	glVertex3f(0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.8f, -0.75f, 0.0f);
	glVertex3f(0.8f, -0.75f, 0.0f);

	glEnd();

}

void letterA1(void)
{
	// A
	glBegin(GL_QUADS);

	// Left Top Bar
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);

	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.495f, 0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glVertex3f(-0.258f, -0.01f, 0.0f);

	glVertex3f(-0.258f, -0.01f, 0.0f);
	glVertex3f(-0.503f, -0.01f, 0.0f);
	glVertex3f(-0.854f, -1.0f, 0.0f);
	glVertex3f(-0.6f, -1.0f, 0.0f);

	// Middle Bar
	glVertex3f(0.254f, 0.1f, 0.0f);
	glVertex3f(-0.224f, 0.1f, 0.0f);
	glVertex3f(-0.288f, -0.1f, 0.0f);
	glVertex3f(0.288f, -0.1f, 0.0f);

	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.275f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);

	// Right Top Bar
	// Left Top Bar
	glVertex3f(0.12f, 1.0f, 0.0f);
	glVertex3f(-0.12f, 1.0f, 0.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.495f, 0.01f, 0.0f);

	glVertex3f(0.495f, 0.01f, 0.0f);
	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glVertex3f(0.503f, -0.01f, 0.0f);

	glVertex3f(0.503f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);
	glVertex3f(0.6f, -1.0f, 0.0f);
	glVertex3f(0.854f, -1.0f, 0.0f);

	// Middle Bar
	glVertex3f(0.224f, 0.1f, 0.0f);
	glVertex3f(-0.224f, 0.1f, 0.0f);
	glVertex3f(-0.289f, -0.1f, 0.0f);
	glVertex3f(0.289f, -0.1f, 0.0f);

	glVertex3f(0.254f, 0.01f, 0.0f);
	glVertex3f(-0.254f, 0.01f, 0.0f);
	glVertex3f(-0.275f, -0.01f, 0.0f);
	glVertex3f(0.258f, -0.01f, 0.0f);

	glEnd();
}

void letterF1(void)
{
	// F
	glBegin(GL_QUADS);

	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(0.8f, 0.75f, 0.0f);

	glVertex3f(-0.55f, 0.75f, 0.0f);
	glVertex3f(-0.8f, 0.75f, 0.0f);
	glVertex3f(-0.8f, -1.0f, 0.0f);
	glVertex3f(-0.55f, -1.0f, 0.0f);

	glVertex3f(0.25f, 0.14f, 0.0f);
	glVertex3f(-0.55f, 0.14f, 0.0f);
	glVertex3f(-0.55f, -0.12f, 0.0f);
	glVertex3f(0.25f, -0.12f, 0.0f);

	glEnd();
}

void circle(double radius_x, double radius_y, float start, float end)
{
	glBegin(GL_POLYGON);
	glColor3f(0.3647f, 0.5411f, 0.6588f);
	for (int i = 0; i <= 100; i++)
	{
		double angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		double x = cos(angle) * radius_x + 0.2f;
		double y = sin(angle) * radius_y;
		glVertex2d(start + x, end + y);
	}
	glEnd();
}

void circle_s2(double radius_x, double radius_y, float start, float end)
{
	glLineWidth(3);

	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i <= 100; i++)
	{
		double angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		double x = cos(angle) * radius_x;
		double y = sin(angle) * radius_y;
		glVertex2d(start + x, end + y);
	}
	glEnd();

}

void circleBorder(double radius_x, double radius_y, float start, float end)
{
	//glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i <= 100; i++)
	{
		double angle = 2 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		double x = cos(angle) * radius_x + 0.2f;
		double y = sin(angle) * radius_y;
		glVertex2d(start + x, end + y);
	}
	glEnd();
}

void filled_circle(double radius_x, double radius_y, float start, float end)
{
	glLineWidth(3);

	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i <= 100; i++)
	{
		double angle = 1 * 3.14 * i / 100; // 2 * 180 * position you want / how many points you want that number
		double x = cos(angle) * radius_x;
		double y = sin(angle) * radius_y;
		glVertex2d(start + x, end + y);
	}
	glEnd();
}

void update(void)
{
	// code

	if (xTravLetterI <= -4.0f)
	{
		xTravLetterI = xTravLetterI + 0.00725f;
	}

	if (yTravLetterN >= -0.1f)
	{
		yTravLetterN = yTravLetterN - 0.009f;
	}

	if (yTravLetterI <= -0.1f)
	{
		yTravLetterI = yTravLetterI + 0.009f;
	}

	if (xTravLetterA >= 4.0f)
	{
		xTravLetterA = xTravLetterA - 0.00725f;
	}

	if (yTravLetterI >= -0.1f)
	{
		if (orangeR <= 1.0f)
		{
			orangeR = orangeR + 0.004f;
		}

		if (whiteR <= 1.0f)
		{
			whiteR = whiteR + 0.004f;
		}

		if (greenR <= 0.0745f)
		{
			greenR = greenR + 0.004f;
		}

		if (orangeG <= 0.6f)
		{
			orangeG = orangeG + 0.004f;
		}

		if (whiteG <= 1.0f)
		{
			whiteG = whiteG + 0.004f;

		}

		if (greenG <= 0.5333f)
		{
			greenG = greenG + 0.004f;
		}

		if (orangeB <= 0.2f)
		{
			orangeB = orangeB + 0.004f;
		}

		if (whiteB <= 1.0f)
		{
			whiteB = whiteB + 0.004f;
		}

		if (greenB <= 0.3137f)
		{
			greenB = greenB + 0.004f;
		}
	}

	if (orangeR >= 1.0f)
	{
		if (angleOrange1 <= 90.0f)
		{
			angleOrange1 = angleOrange1 + 0.4f;
		}

		if (xTrav >= 16.0f)
		{
			if (angleOrange2 <= 400.0f)
			{
				angleOrange2 = angleOrange2 + 0.4f;
			}
		}

		if (xTravWhitePlane <= 16.5f)
		{
			xTravWhitePlane = xTravWhitePlane + 0.038f;
		}

		if (angleGreen1 >= -90.0f)
		{
			angleGreen1 = angleGreen1 - 0.4f;
		}

		if (xTrav >= 16.0f)
		{
			if (angleGreen2 >= -120.0f)
			{
				angleGreen2 = angleGreen2 - 0.39f;
			}
		}

		if (xTrav <= 16.0f && angleOrange1 >= 90.0f || angleGreen1 >= 90.0f)
		{
			xTrav = xTrav + 0.04f;
		}

		if (angleGreen2 <= -120.0f)
		{
			if (c3R <= 1.0f)
			{
				c3R = c3R + 0.0045f;
			}

			if (c3G <= 0.3f)
			{
				c3G = c3G + 0.001f;
			}

			if (c1R <= 1.0f)
			{
				c1R = c1R + 0.0025f;
			}

			if (c1G <= 0.7f)
			{
				c1G = c1G + 0.002f;
			}

			if (c2R <= 1.0f)
			{
				c2R = c2R + 0.0035f;
			}

			if (c2G <= 0.4f)
			{
				c2G = c2G + 0.001f;
			}
		}
	}

	
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code
	fprintf(gpFile, "orange R = %f\n\n", orangeR);
	// FullScreen astana escape press karun close karaych try kela tar window orginal state 
	// madhye yeun tyacha sukhad mrutyu hoil aapla window application 
	if (gbFullScreen)
	{
		ToggleFullScreen(); // important so taken in curly brace
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (ghwnd) // ghwnd ajun asel tar DestroyWindow() la call karun ghwnd destroy kar
	{
		DestroyWindow(ghwnd); 	
		ghwnd = NULL;
	}
	
	if (gpFile)
	{
		fprintf(gpFile, "Log File Successfully Closed.\n");
		fclose(gpFile); // file close keli with using files pointer
		gpFile = NULL; // bhanda(container) dhuvun kadhla use kelela
	}
}

