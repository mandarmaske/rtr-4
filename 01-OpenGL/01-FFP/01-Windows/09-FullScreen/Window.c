// header files
#include<Windows.h>
#include"Window.h"

// global function declarations / prototype / signature
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declaration
HWND ghwnd = NULL; // g for global
BOOL gbFullScreen = FALSE; // local static kela tari chalala asta

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");

	// code

	// Initialization Of WNDCLASSEX Structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Registering WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create The Window
	hwnd = CreateWindow(szAppName,
						TEXT("Mandar Dilip Maske RTR2021-116"),
						WS_OVERLAPPEDWINDOW,
						((GetSystemMetrics(SM_CXSCREEN) - 800) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - 600) / 2),
						800,
						600,
						NULL,
						NULL,
						hInstance,
						NULL);

	ghwnd = hwnd; // assigning hwnd to ghwnd to pass hwnd to handle of window to GetWindowLong () in ToggleFullScree() 

	// Show The Window
	ShowWindow(hwnd, iCmdShow);

	// Update The Window
	UpdateWindow(hwnd);

	// message loop - heart of the program
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

// CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
			case 'f':
				ToggleFullScreen();
				break;

		default:
			break;
		}
	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle; // static to sustain values accross this function
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT); // like wndclass.cbSize =  sizeof(WNDCLASSEX)

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE); // current window style saved
		if (dwStyle & WS_OVERLAPPEDWINDOW) // if (1 & WS_OVERLAPPEDWINDOW) -> if (1 & 1) then if ya madhe
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) // if(1 && 1) then in if
			{
				SetWindowLong(ghwnd, GWL_STYLE,  dwStyle & ~WS_OVERLAPPEDWINDOW); // WS_OVERLAPPEDWINDOW che 5 style(WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_THICKFRAME,  WS_SYSMENU, WS_CAPTION) not required except WS_OVERLAPPED 
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE,  dwStyle | WS_OVERLAPPEDWINDOW); // kadhayla ~WS_OVERLAPPED kela, Add karnyasathi Bitwise Or Waparala
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED); // x, y, width, height ignore -> Zero Why? - wp ne already SetWindowPlacement pahije tithe aaplya window la place kelay so 0 

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

