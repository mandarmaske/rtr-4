// header file
#include<Windows.h>

// macro definations
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass; // wndclass is our window class far Window that we will be showing on screen
	HWND hwnd; // to create unique hwnd for our Window
	MSG msg; // To take message msg variable 
	TCHAR szAppName[] = TEXT("MyPracticeWindow"); // Our Window Class Name 

	// initializing members of WNDCLASSEX wndclass structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Registering Our WNDCLASEX wndclass structure
	RegisterClassEx(&wndclass);

	// creating window in memory
	hwnd = CreateWindow(szAppName,
						TEXT("Practice Window"),
						WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME,
						((GetSystemMetrics(SM_CXSCREEN) - WIN_WIDTH) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - WIN_HEIGHT) / 2),
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL, // HWND_DESKTOP
						NULL, // Menu cha handle
						hInstance,
						NULL); // creation parameter, it is used to pass special values to WndProc

	// Showing Window On Screen
	ShowWindow(hwnd, iCmdShow); // iCmdShow -> SW_SHOWNORMAL / SW_NORMAL, Other Values SW_HIDE, SW_MINIMIZE, SW_MAXIMIZE

	// Update Window when window repaints
	UpdateWindow(hwnd); // Show Window Internally Calls UpdateWindow

	// Heart Of Program - Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// code
	switch (iMsg)
	{
	case WM_CREATE:
		MessageBox(hwnd, TEXT("WM_CREATE Message Has Arrived"), TEXT("Message"), MB_OK);
		break;

	case WM_CLOSE:
		MessageBox(hwnd, TEXT("WM_CLOSE Message Has Arrived"), TEXT("Message"), MB_OK);
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		MessageBox(hwnd, TEXT("WM_DESTROY Message Has Arrived"), TEXT("Message"), MB_OK);
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


