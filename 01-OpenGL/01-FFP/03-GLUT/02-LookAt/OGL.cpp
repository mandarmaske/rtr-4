// header file
#include <GL/freeglut.h>

// global variable
bool bFullScreen = false;

// entry-point function
int main(int argc, char *argv[])
{
	// local function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);  // callback declaration, winmain declarator ani wndclass he yaat include ahe

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); // pfd.dwFlags = PFD_DOUBLEBUFFER; and pfd.iPixelType = PFD_TYPE_RGBA; and GLUT_DEPTH in GLUT is analogus with pfd.cDepthBits in native

	glutInitWindowSize(800, 600); // width and height 
	glutInitWindowPosition(100, 100); // x and y
	glutCreateWindow("Satat Kriya"); // CreateWindow(), ShowWindow(), SetForegroundWindow(), SetFocus()

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void resize(int width, int height)
{
	// code

	if (height < 0)
		height = 1;

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glFrustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.5f, 20.0f);

}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(7.0f, 2.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // view transformation
	glScalef(1.0f, 2.0f, 1.0f); // Model Transformation

	glutWireCube(1.0f);

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) // x and y are wParam and lParam
{
	// code
	switch (key)
	{
	case 27: // WM_KEYDOWN
		glutLeaveMainLoop();
		break;

	case 'F': // WM_CHAR
		case 'f':
			if (bFullScreen == false)
			{
				glutFullScreen();
				bFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bFullScreen = false;
			}
		break;

	default:
		break;
	}
}

void mouse(int button, int state, int x, int y) // mouse ch button, tyachi state, wParam, lParam - Parameters
{
	// code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		glutLeaveMainLoop();
		break;

	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}
