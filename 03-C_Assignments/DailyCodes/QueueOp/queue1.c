// header files
#include <stdio.h>
#include <stdlib.h>

// macro definations
#define NUM_SIZE 5

int front = -1;
int rear = -1;
int iArray[NUM_SIZE] = { 0, 0, 0, 0, 0 };

// entry-point function
int main(void)
{
	// function declarations
	void enqueue(int);
	void dequeue();
	void display(void);
	void peek(void);

	// variable declarations
	int no = 0;
	int choice = 0;
	
	// code
	printf("\n\n");
	do 
	{

		printf("***************** Menu *****************\n\n");
		printf("1. To Add Element In The Queue\n");
		printf("2. To Delete Element In The Queue\n");
		printf("3. Display\n");
		printf("4. Peek\n");
		printf("0. Exit\n\n");

		printf("Enter Your Choice : ");
		scanf("%d", &choice);
		printf("\n\n");

		switch (choice)
		{
		case 0:
			printf("Exitting Now !!!\n");
			break;

		case 1:
			printf("Enter Element You Want To Add : ");
			scanf("%d", &no);
			printf("\n\n");
			enqueue(no);
			break;

		case 2:
			dequeue();
			break;

		case 3:
			printf("\n");
			display();
			printf("\n\n");
			break;

		case 4:
			printf("\n");
			peek();
			printf("\n\n");
			break;

		default:
			printf("You Have Entered Wrong Choice\n");
			break;
		}

	} while (choice != 0);

	return(0);
}

void enqueue(int num)
{
	if (rear == NUM_SIZE - 1)
	{
		printf("Queue Is Full\n\n");
	}
	else if (front == -1 && rear == -1)
	{
		front++;
		rear++;
		iArray[rear] = num;
	}
	else
	{
		rear++;
		iArray[rear] = num;
	}
}

void dequeue(void)
{
	if (front == -1 && rear == -1)
	{
		printf("queue is empty\n\n");
	}
	else if (front == rear)
	{
		front = -1;
		rear = -1;
	}
	else
	{
		iArray[rear] = 0;
		rear--;
	}
}

void display()
{
	for (int i = front; i <= rear; i++)
	{
		printf("Array[%d] : %d\n", i, iArray[i]);
	}
}

void peek(void)
{
	printf("The Current Rear Value is : %d\n", iArray[rear]);
	printf("The Current Front Value is : %d\n", iArray[front]);
}

