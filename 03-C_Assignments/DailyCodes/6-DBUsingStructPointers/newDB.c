// header file
#include <stdio.h>
#include <stdlib.h>

// macro definations
#define NUM_STUDENTS 2
#define NAME_LENGTH 20
#define GENDER_LENGTH 7
#define DATE_OF_BIRTH_LENGTH 11
#define NUM_SUBJECTS 3

// struct definations
struct MyDb
{
	char name[NAME_LENGTH];
	char gender[GENDER_LENGTH];
	char date_of_birth[DATE_OF_BIRTH_LENGTH];
	int mathMarks;
	int englishMarks;
	int marathiMarks;
	int marksAddition;
	float average;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	float MyAverage(int);

	// variable declaration
	struct MyDb* students[NUM_STUDENTS] = {NULL};

	// code

	printf("\n\n");

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		students[i] = (struct MyDb*)malloc(NUM_STUDENTS * sizeof(struct MyDb));

		if (students[i] == NULL)
		{
			printf("Failed To Allocate Memory To  !!! Exitting Now...\n\n", NUM_STUDENTS);
			exit(0);
		}
		else
		{
			printf("Memory Allocation For Array Of struct[%d] Succeeded !!!\n\n", i);
		}
	}
	
	

	// Input
	printf("\n\n<--*----*----*---Enter The Student Details---*----*----*->");
	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("\n\nEnter Name : ");
		scanf("%s", students[i]->name);
		printf("Enter Gender (Male / Female) : ");
		scanf("%s", students[i]->gender);
		printf("Enter Date Of Birth (In dd-mm-yyyy) : ");
		scanf("%s", students[i]->date_of_birth);
		printf("Enter Math Marks : ");
		scanf("%d", &students[i]->mathMarks);
		printf("Enter English Marks : ");
		scanf("%d", &students[i]->englishMarks);
		printf("Enter Marathi Marks : ");
		scanf("%d", &students[i]->marathiMarks);

		students[i]->marksAddition = students[i]->mathMarks + students[i]->englishMarks + students[i]->marathiMarks;

		students[i]->average = MyAverage(students[i]->marksAddition);
	}

	// output
	printf("\n\n");
	printf("<--*----*----*---Student Database Details---*----*----*->\n\n");

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Student %d ========> \n\n", i + 1);

		printf("Name : %s\n", students[i]->name);
		printf("Gender : %s\n", students[i]->gender);
		printf("Date Of Birth : %s\n", students[i]->date_of_birth);
		printf("Math Marks : %d\n", students[i]->mathMarks);
		printf("English Marks : %d\n", students[i]->englishMarks);
		printf("Marathi Marks : %d\n", students[i]->marathiMarks);
		printf("Average Of Marks : %.2f\n", students[i]->average);
		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}

	for (int i = (NUM_STUDENTS - 1); i >= 0; i--)
	{
			free(students[i]);
			students[i] = NULL;
			printf("students[%d] allocated memory is freed successfully\n", i);
	}
	
	
	return(0);
}

float MyAverage(int totalMarks)
{
	float average = 0.0f;

	average = (float) totalMarks / 3;

	return(average);
}
