// header file
#include <stdio.h>

// macro defination
#define NUM_STUDENT 4
#define NUM_SUBJECTS 3

// entry-point function
int main(void)
{
	// function declarations
	float MyAverage(int);

	// variable declarations
	int marks[NUM_STUDENT][NUM_SUBJECTS] = { 0 };
	float average[NUM_STUDENT] = { 0.0f };
	int marksAddition[NUM_STUDENT] = {0};

	printf("Enter Student Details : \n\n");

	for (int i = 0; i < NUM_STUDENT; i++)
	{
		printf("\nEnter Marks : \n");
		for (int n = 0; n < NUM_SUBJECTS; n++)
		{
			if (n == 0)
			{
				printf("Maths : ");
				scanf("%d", &marks[i][n]);
			}
			else if (n == 1)
			{
				printf("English : ");
				scanf("%d", &marks[i][n]);
			}
			else if (n == 2)
			{
				printf("Marathi : ");
				scanf("%d", &marks[i][n]);
			}
		}

		for (int o = 0; o < NUM_SUBJECTS; o++)
		{
			marksAddition[i] = marksAddition[i] + marks[i][o];
		}

		average[i] = MyAverage(marksAddition[i]);
	}

	printf("\n\nStudent Details : \n\n");

	for (int i = 0; i < NUM_STUDENT; i++)
	{
		printf("Marks Of Student %d : \n", i + 1);
		for (int j = 0; j < NUM_SUBJECTS; j++)
		{
			if (j == 0)
			{
				printf("Maths : %d\n", marks[i][j]);
			}
			else if (j == 1)
			{
				printf("English : %d\n", marks[i][j]);
			}
			else if (j == 2)
			{
				printf("Marathi : %d\n", marks[i][j]);
			}
		}
		printf("Average Of Student : %.2f\n", average[i]);
	}

	return(0);
}

float MyAverage(int totalMarks)
{
	float average = 0.0f;

	average = (float) totalMarks / 3;

	return(average);
}