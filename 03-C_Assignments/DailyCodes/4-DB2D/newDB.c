// header files
#include <stdio.h>
#include <string.h>

// macro defination
#define NUM_STUDENT 3
#define NAME_LENGTH 20
#define GENDER_LENGTH 7
#define DATE_OF_BIRTH_LENGTH 11
#define NUM_SUBJECTS 3

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	float MyAverage(int);

	// variable declarations
	char name[NUM_STUDENT][NAME_LENGTH] = { '\0' };
	char gender[NUM_STUDENT][GENDER_LENGTH] = { '\0' };
	char date_of_birth[NUM_STUDENT][DATE_OF_BIRTH_LENGTH] = { '\0' };
	int marks[NUM_STUDENT][NUM_SUBJECTS] = {0};
	float average[NUM_STUDENT] = { 0.0f };
	int marksAddition[NUM_STUDENT] = { 0 };
	char tempStrName[NAME_LENGTH] = { '\0' };
	char tempStrGender[GENDER_LENGTH] = { '\0' };
	char tempStrDOB[DATE_OF_BIRTH_LENGTH] = { '\0' };

	printf("\n\n<--*----*----*---Enter The Student Details---*----*----*->\n\n");

	for (int i = 0; i < NUM_STUDENT; i++)
	{
		printf("Enter Name : ");
		scanf("%s", &tempStrName);

		for (int j = 0; j < strlen(tempStrName); j++)
		{
			name[i][j] = tempStrName[j];
		}

		printf("Enter Gender (Male / Female) : ");
		scanf("%s", &tempStrGender);

		for (int k = 0; k < strlen(tempStrGender); k++)
		{
			gender[i][k] = tempStrGender[k];
		}

		printf("Enter Date Of Birth (In This Format dd-mm-yyyy) : ");
		scanf("%s", &tempStrDOB);

		for (int m = 0; m < strlen(tempStrDOB); m++)
		{
			date_of_birth[i][m] = tempStrDOB[m];
		}

		for (int n = 0; n < NUM_SUBJECTS; n++)
		{
			if (n == 0)
			{
				printf("Enter Maths Marks : ");
				scanf("%d", &marks[i][n]);
			}
			else if (n == 1)
			{
				printf("Enter English Marks : ");
				scanf("%d", &marks[i][n]);
			}
			else if (n == 2)
			{
				printf("Enter Marathi Marks : ");
				scanf("%d", &marks[i][n]);
			}
		}
		
		for (int o = 0; o < NUM_SUBJECTS; o++)
		{
			marksAddition[i] = marksAddition[i] + marks[i][o];
		}

		average[i] = MyAverage(marksAddition[i]);

		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}

	printf("<--*----*----*---Student Database Details---*----*----*->\n\n");

	for (int i = 0; i < NUM_STUDENT; i++)
	{

		printf("Student %d ===> \n\n", i + 1);

		printf("Name \t\t : ");
		for (int j = 0; j < strlen(name[i]); j++)
		{
			printf("%c", name[i][j]);
		}
		printf("\n");

		printf("Gender \t\t : ");
		for (int k = 0; k < strlen(gender[i]); k++)
		{
			printf("%c", gender[i][k]);
		}
		printf("\n");

		printf("Date Of Birth \t : ");
		for (int l = 0; l < strlen(date_of_birth[i]); l++)
		{
			printf("%c", date_of_birth[i][l]);
		}
		printf("\n");

		for (int m = 0; m < NUM_SUBJECTS; m++)
		{
			if (m == 0)
			{
				printf("Maths Marks \t : %d\n", marks[i][m]);
			}
			else if (m == 1)
			{
				printf("English Marks \t : %d\n", marks[i][m]);
			}
			else if (m == 2)
			{
				printf("Marathi Marks \t : %d\n", marks[i][m]);
			}
		}
		printf("Average Marks \t : %.2f %%\n", average[i]);

		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}

	return(0);
}

float MyAverage(int totalMarks)
{
	float average = 0.0f;

	average = (float) totalMarks / 3;

	return(average);
}
