// header file
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

// macro definations
#define NUM_STUDENTS 2
#define NAME_LENGTH 20
#define GENDER_LENGTH 7
#define DATE_OF_BIRTH_LENGTH 11
#define NUM_SUBJECTS 3

// struct definations
struct MyDb
{
	char name[NAME_LENGTH];
	char gender[GENDER_LENGTH];
	char date_of_birth[DATE_OF_BIRTH_LENGTH];
	int mathMarks;
	int englishMarks;
	int marathiMarks;
	int marksAddition;
	float average;
};

int index = -1;
struct MyDb* students[NUM_STUDENTS] = { NULL };

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	float MyAverage(int);
	int push(void);
	int pop(void);
	void display(void);

	// variable declarations
	char choice = '\0';

	// code
	printf("\n\n");
	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		students[i] = (struct MyDb*)malloc(NUM_STUDENTS * sizeof(struct MyDb));

		if (students[i] == NULL)
		{
			printf("Failed To Allocate Memory To  !!! Exitting Now...\n\n", NUM_STUDENTS);
			exit(0);
		}
		else
		{
			printf("Memory Allocation For Array Of struct[%d] Succeeded !!!\n\n", i);
		}
	}

	do
	{
		printf("***************** MENU *****************\n\n");
		printf("1. Push Operation\n");
		printf("2. Pop Operation\n");
		printf("3. Display The Stack Values\n");
		printf("0. To Exit\n");

		printf("\nEnter Your Choice : ");
		choice = getch();
		printf("%c\n", choice);

		switch (choice)
		{

		case '0':
			printf("Exitting Now !!!\n");
			break;

		case '1': // Push
			push();
			break;

		case '2': // Pop
			pop();
			break;

		case '3': // Display
			display();
			break;

		default:
			printf("\nYou Entered The Wrong Choice\n\n");
			break;
		}
	} while (choice != '0');

	printf("\n\n");

	for (int i = (NUM_STUDENTS - 1); i >= 0; i--)
	{
			free(students[i]);
			students[i] = NULL;
			printf("students[%d] allocated memory is freed successfully\n", i);
	}
	
	
	return(0);
}

float MyAverage(int totalMarks)
{
	float average = 0.0f;

	average = (float) totalMarks / 3;

	return(average);
}

int push(void)
{
	if (index >= (NUM_STUDENTS - 1))
	{
		printf("\nStack Is Overflow\n\n");
		return(-1);
	}
	else
	{
		index++;
		printf("\n\nPushing Data On The Stack Having Index : %d\n\n", index);
		printf("<--*----*----*---Enter The Student Details---*----*----*->");
		printf("\n\nEnter Name : ");
		scanf("%s", students[index]->name);
		printf("Enter Gender (Male / Female) : ");
		scanf("%s", students[index]->gender);
		printf("Enter Date Of Birth (In dd-mm-yyyy) : ");
		scanf("%s", students[index]->date_of_birth);
		printf("Enter Math Marks : ");
		scanf("%d", &students[index]->mathMarks);
		printf("Enter English Marks : ");
		scanf("%d", &students[index]->englishMarks);
		printf("Enter Marathi Marks : ");
		scanf("%d", &students[index]->marathiMarks);

		students[index]->marksAddition = students[index]->mathMarks + students[index]->englishMarks + students[index]->marathiMarks;

		students[index]->average = MyAverage(students[index]->marksAddition);
		printf("\n\n");
		return(0);
	}
}

int pop(void)
{
	if (index < 0)
	{
		printf("\nStack Is Underflow\n\n");
		return(-1);
	}
	else
	{
		printf("\n\nPopping The Data From The Stack Having Index : %d\n\n", index);
		//students[students->index] = { NULL };
		students[index]->name[NAME_LENGTH] = NULL;
		students[index]->gender[GENDER_LENGTH] = NULL;
		students[index]->date_of_birth[DATE_OF_BIRTH_LENGTH] = NULL;
		students[index]->mathMarks = 0;
		students[index]->englishMarks = 0;
		students[index]->marathiMarks = 0;
		students[index]->marksAddition = 0;
		students[index]->average = 0.0f;
		index--;
		return(0);
	}
}

void display(void)
{
	printf("\n\n");
	printf("<--*----*----*---Student Database Details---*----*----*->\n\n");

	for (int i = index; i >= 0; i--)
	{
		printf("Name : %s\n", students[i]->name);
		printf("Gender : %s\n", students[i]->gender);
		printf("Date Of Birth : %s\n", students[i]->date_of_birth);
		printf("Math Marks : %d\n", students[i]->mathMarks);
		printf("English Marks : %d\n", students[i]->englishMarks);
		printf("Marathi Marks : %d\n", students[i]->marathiMarks);
		printf("Average Of Marks : %.2f\n", students[i]->average);
		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}
}

