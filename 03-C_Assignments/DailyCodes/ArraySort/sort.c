// header file
#include <stdio.h>

// macro defination
#define NUM_STUDENTS 5

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int rollNoArr[NUM_STUDENTS] = { 0 };
	int temp = 0;

	printf("\n\n");

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		scanf("%d", &rollNoArr[i]);
	}
	printf("\n\n");

	// Before Sorting
	printf("Unsorted Array\n");
	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Array Element %d : %d\n", i, rollNoArr[i]);
	}

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		if (i != NUM_STUDENTS - 1)
		{
			if (rollNoArr[i] > rollNoArr[i + 1])
			{
				temp = rollNoArr[i];
				rollNoArr[i] = rollNoArr[i + 1];
				rollNoArr[i + 1] = temp;
			}
		}
	}

	printf("\n\n");

	// After Sorting 
	printf("Sorted Array\n");
	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Array Element %d : %d\n", i, rollNoArr[i]);
	}

	return(0);
}