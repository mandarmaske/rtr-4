// header files
#include <stdio.h>
#include <stdlib.h>

// macro defination
// macro defination
#define NUM_STUDENTS 3
#define NAME_LENGTH 10
#define GENDER_LENGTH 7

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	char** name = NULL;
	char** gender = NULL;
	int Num_Student = 3;

	// code

	name = (char**)malloc(NUM_STUDENTS * sizeof(char));

	if (name == NULL)
	{
		printf("Failed To Allocate Memory To %d Rows Of 2D Character Array !!! Exitting Now...\n\n", NUM_STUDENTS);
		exit(0);
	}
	else
	{
		printf("Memory Allocation To %d Rows Of 2D Character Array Succeeded !!!\n\n", NUM_STUDENTS);
	}

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		name[i] = (char*)malloc(NAME_LENGTH * sizeof(char));

		if (name[i] == NULL)
		{
			printf("Failed To Allocate Memory To Columns Of Row %d Of 2D Character Array !!! Exitting Now...\n\n", i);
			exit(0);
		}
		else
		{
			printf("Memory Allocation To Columns To Row %d Of 2D Character Array Of Name Succeeded !!!\n\n", i);
		}
	}

	gender = (char**)malloc(NUM_STUDENTS * sizeof(char));

	if (gender == NULL)
	{
		printf("Failed To Allocate Memory To %d Rows Of 2D Character Array !!! Exitting Now...\n\n", NUM_STUDENTS);
		exit(0);
	}
	else
	{
		printf("Memory Allocation To %d Rows Of 2D Integer Array Of Gender Succeeded !!!\n\n", NUM_STUDENTS);
	}

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		gender[i] = (char*)malloc(GENDER_LENGTH * sizeof(char));

		if (gender[i] == NULL)
		{
			printf("Failed To Allocate Memory To Columns Of Row %d Of 2D Chracter Array !!! Exitting Now...\n\n", i);
			exit(0);
		}
		else
		{
			printf("Memory Allocation To Columns To Row %d Of 2D Integer Array Of Gender Succeeded !!!\n\n", i);
		}
	}


	// Input 
	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Enter Name : ");
		scanf("%s", name[i]);

		printf("Enter Gender : ");
		scanf("%s", gender[i]);
	}

	printf("\n\n");

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Name : %s\n", name[i]);
		printf("Gender : %s\n", gender[i]);
	}

	for (int i = (Num_Student - 1); i >= 0; i--)
	{
		if (name[i])
		{
			free(name[i]);
			name[i] = NULL;
			printf("Memory Allocated To Row %d Has Been Successfully Freed!!!\n\n", i);
		}
	}

	if (name)
	{
		free(name);
		name = NULL;
		printf("Memory Allocated To name array Has Been Successfully Freed !!!\n\n");
	}

	for (int i = (Num_Student - 1); i >= 0; i--)
	{
		if (gender[i])
		{
			free(gender[i]);
			gender[i] = NULL;
			printf("Memory Allocated To Row %d Has Been Successfully Freed!!!\n\n", i);
		}
	}

	if (gender)
	{
		free(gender);
		gender = NULL;
		printf("Memory Allocated To gender array Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}