// header files
#include <stdio.h>
#include <stdlib.h>

// macro definations
#define NUM_STUDENTS 4
#define NAME_LENGTH 10
#define GENDER_LENGTH 7
#define DOB_LENGTH 11
#define NUM_SUBJECTS 3

// entry-point function
int main(void)
{
	// function declarations
	float MyAverage(int);

	//variable declarations
	char* name[NUM_STUDENTS] = { '\0' };
	char* gender[NUM_STUDENTS] = { '\0' };
	char* DateOfBirth[NUM_STUDENTS] = { '\0' };
	int* Marks[NUM_STUDENTS] = { 0 };
	int marksAddition[NUM_STUDENTS] = { 0 };
	float* average = NULL;

	average = (float*)malloc(sizeof(float) * NUM_STUDENTS);

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		name[i] = (char*)malloc(sizeof(char) * NAME_LENGTH);
		gender[i] = (char*)malloc(sizeof(char) * GENDER_LENGTH);
		DateOfBirth[i] = (char*)malloc(sizeof(char) * DOB_LENGTH);
		Marks[i] = (int*)malloc(sizeof(int) * NUM_SUBJECTS);

		if (name[i] == NULL)
		{
			printf("Name Array Memory Allocation Failed, Exitting Now!!!\n\n");
			exit(0);
		}

		if (gender[i] == NULL)
		{
			printf("Gender Array Memory Allocation Failed, Exitting Now!!!\n\n");
			exit(0);
		}

		if (DateOfBirth[i] == NULL)
		{
			printf("Date Of Birth Array Memory Allocation Failed, Exitting Now!!!\n\n");
			exit(0);
		}

		if (Marks[i] == NULL)
		{
			printf("Marks Array Memory Allocation Failed, Exitting Now!!!\n\n");
			exit(0);
		}

		if (average == NULL)
		{
			printf("Average Array Memory Allocation Failed, Exitting Now!!!\n\n");
			exit(0);
		}
	}

	printf("\n\n<--*----*----*---Enter The Student Details---*----*----*->\n\n");

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Enter Name : ");
		scanf("%s", name[i]);
		printf("Enter Gender : ");
		scanf("%s", gender[i]);
		printf("Enter Date Of Birth : ");
		scanf("%s", DateOfBirth[i]);

		for (int j = 0; j < NUM_SUBJECTS; j++)
		{
			if (j == 0)
			{
				printf("Enter Maths Marks : ");
				scanf("%d", &Marks[i][j]);
			}
			else if (j == 1)
			{
				printf("Enter English Marks : ");
				scanf("%d", &Marks[i][j]);
			}
			else if (j == 2)
			{
				printf("Enter Marathi Marks : ");
				scanf("%d", &Marks[i][j]);
			}
		}

		for (int k = 0; k < NUM_SUBJECTS; k++)
		{
			marksAddition[i] = marksAddition[i] + Marks[i][k];
		}

		average[i] = MyAverage(marksAddition[i]);

		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}

	printf("<--*----*----*---Student Database Details---*----*----*->\n\n");

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Student %d ===> \n\n", i + 1);

		printf("Name \t\t : %s\n", name[i]);
		printf("Gender \t\t : %s\n", gender[i]);
		printf("Date Of Birth \t : %s\n", DateOfBirth[i]);
		
		for (int j = 0; j < NUM_SUBJECTS; j++)
		{
			if (j == 0)
			{
				printf("Maths Marks \t : %d\n", Marks[i][j]);
			}
			else if (j == 1)
			{
				printf("Maths Marks \t : %d\n", Marks[i][j]);
			}
			else if (j == 2)
			{
				printf("Maths Marks \t : %d\n", Marks[i][j]);
			}
		}

		printf("Average Marks \t : %.2f %%\n", average[i]);

		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}


	for (int i = (NUM_STUDENTS - 1); i >= 0; i--)
	{
		free(name[i]);
		name[i] = NULL;

		free(gender[i]);
		gender[i] = NULL;

		free(DateOfBirth[i]);
		DateOfBirth[i] = NULL;

		free(Marks[i]);
		Marks[i] = NULL;
	}

	printf("Memory Allocated For Name Array Has Been Successfully Freed !!!\n\n");

	printf("Memory Allocated For Gender Array Has Been Successfully Freed !!!\n\n");

	printf("Memory Allocated For Date Of Birth Array Has Been Successfully Freed !!!\n\n");

	printf("Memory Allocated For Marks Array Has Been Successfully Freed !!!\n\n");

	if (average)
	{
		free(average);
		average = NULL;
		printf("Memory Allocated For Average Array Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}

float MyAverage(int totalMarks)
{
	float average = 0.0f;

	average = (float) totalMarks / 3;

	return(average);
}
