// header files
#include <stdio.h>

// macro definations
#define ARRAY_SIZE 5

// global variable declarations
extern int a[ARRAY_SIZE];

void push(void)
{
	if (a[0] == 0)
	{
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			if (a[i] == 0)
			{
				a[i] = (i + 1) * 10;
			}
			else
				break;
		}
	}
}

void pop(void)
{

	for (int i = ARRAY_SIZE; i >= 0; i--)
	{
		if (a[i] != 0)
		{
			a[i] = 0;
		}
		else
			break;
	}
}

void printPushData(void)
{
	printf("\nStack is FULL\n");

	printf("The Values Of Stacks Are By Index : \n");

	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		printf("a[%d] : %d\n", i + 1, a[i]);
	}
}

void printPopData(void)
{
	printf("\nStack is empty now\n");

	printf("The Values Of Stacks Are By Index : \n");

	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		printf("a[%d] : %d\n", i + 1, a[i]);
	}
}