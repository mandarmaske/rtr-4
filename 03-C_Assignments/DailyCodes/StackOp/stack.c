// header files
#include <stdio.h>

// macro defination
#define ARRAY_SIZE 5

// global variable declarations
int a[ARRAY_SIZE] = { 0 };

// entry-point function
int main(void)
{
	// function declarations
	void push(void);
	void pop(void);
	void printPushData(void);
	void printPopData(void);
	
	push(); 
	printPushData();
	pop();
	printPopData();

	return(0);
}