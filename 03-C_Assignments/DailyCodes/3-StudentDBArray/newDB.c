// header files
#include <stdio.h>
#include <string.h>

// macro definitions
#define NUM_STUDENTS 4
#define MAX_NAME_LENGTH 10
#define NAME_OF_SUB 3
#define DOB_LENGTH 11
#define TEMP_SIZE 15

// entry-point function
int main(void)
{
	// function declaration
	float MyAverage(int, int, int);

	// variable declarations
	char nameOfStudentsArr[ NUM_STUDENTS * MAX_NAME_LENGTH ] = {'\0'};
	char studentGenderArr[NUM_STUDENTS] = {'\0'};
	char studentDOBArr[NUM_STUDENTS * DOB_LENGTH] = {'\0'};
	int marksOfMathsArr[NUM_STUDENTS] = { 0 };
	int marksOfEnglishArr[NUM_STUDENTS] = { 0 };
	int marksOfMarathiArr[NUM_STUDENTS] = { 0 };
	float avgMarksArr[NUM_STUDENTS] = { 0.0f };
	int count;
	int temp = 0;
	int index = 0;
	int indexGender = 0;
	int indexDOB = 0;
	int indexMarks = 0;
	int indexAverage = 0;
	int tempMarks[NAME_OF_SUB] = { 0 };
	char tempStr[MAX_NAME_LENGTH] = { '\0' };
	char tempStrBD[6] = { '\0' };
	char temp1[TEMP_SIZE] = { '\0' };

	// code

	printf("\n\n<--*----*----*---Enter The Student Details---*----*----*->\n\n");

	for (count = 0; count < NUM_STUDENTS; count++)
	{
		// 1) Get Name Of Student
		printf("Enter the name of student : ");
		scanf("%s", &tempStr);

		for (int i = 0; i < strlen(tempStr); i++)
		{
			nameOfStudentsArr[index] = tempStr[i];
			index++;
		}
		nameOfStudentsArr[index] = '_';
		index++;

		// 2) get gender of student

		printf("Enter the gender of student (Male / Female) : ");
		scanf("%s", &tempStrBD);

		for (int j = 0; j < strlen(tempStrBD); j++)
		{
			studentGenderArr[indexGender] = tempStrBD[j];
			indexGender++;
		}
		studentGenderArr[indexGender] = '_';
		indexGender++;

		// 3) get dob of student

		printf("Enter Students Date Of Birth (In dd-mm-yyyy format) : ");
		scanf("%s", &temp1);

		for (int l = 0; l < strlen(temp1); l++)
		{
			studentDOBArr[indexDOB] = temp1[l];
			indexDOB++;
		}
		studentDOBArr[indexDOB] = '_';
		indexDOB++;

		// 4) get marks of 3 subjects

		printf("Enter Marks Of Maths : ");
		scanf("%d", &marksOfMathsArr[count]);

		printf("Enter Marks Of English : ");
		scanf("%d", &marksOfEnglishArr[count]);

		printf("Enter Marks Of Marathi : ");
		scanf("%d", &marksOfMarathiArr[count]);

		// 5) calculate average of students

		avgMarksArr[count] = MyAverage(marksOfMathsArr[count], marksOfEnglishArr[count], marksOfMarathiArr[count]);

		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}

	printf("<--*----*----*---Student Database Details---*----*----*->\n\n");


	int nCount = 0, oCount = 0, pCount = 0, qCount = 0, rCount = 0;

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Name : ");
		for (int j = nCount; j < strlen(nameOfStudentsArr); j++)
		{
			if (nameOfStudentsArr[j] != '_')
			{
				printf("%c", nameOfStudentsArr[j]);
				nCount++;
			}
			else
			{
				nCount++;
				break;
			}
		}

		printf("\nGender : ");
		for (int l = pCount; l < strlen(studentGenderArr); l++)
		{
			if (studentGenderArr[l] != '_')
			{
				printf("%c", studentGenderArr[l]);
				pCount++;
			}
			else
			{
				pCount++;
				break;
			}
		}


		printf("\nDOB : ");
		for (int m = qCount; m < strlen(studentDOBArr); m++)
		{
			if (studentDOBArr[m] != '_')
			{
				printf("%c", studentDOBArr[m]);
				qCount++;
			}
			else
			{
				qCount++;
				break;
			}
		}

		printf("\nMaths Marks : %d\n", marksOfMathsArr[i]);
		printf("English Marks: %d\n", marksOfEnglishArr[i]);
		printf("Marathi Marks : %d\n", marksOfMarathiArr[i]);

		printf("Average Marks : %.2f\n", avgMarksArr[i]);

		printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n");
	}

	return(0);
}

float MyAverage(int maths, int english, int marathi)
{
	// variable declaration
	float average = 0.0f;

	average = (float) (maths + english + marathi) / NAME_OF_SUB;

	return(average);
}
