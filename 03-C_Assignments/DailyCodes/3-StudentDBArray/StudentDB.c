// header files
#include <stdio.h>

// macro definations
#define NAME_LENGTH 30

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	char name[NAME_LENGTH] = { '\0' };

	printf("\n\n");
	printf("Enter Name Of 5 Students : \n\n");

	for (int i = 0; i < NAME_LENGTH; i++)
	{
		scanf("%c", &name[i]);
	}

	printf("\n\n");
	printf("Printing 5 Student Names : \n\n");

	for (int i = 0; i < NAME_LENGTH; i++)
	{
		printf("%c", name[i]);
	}
	printf("\n");
}