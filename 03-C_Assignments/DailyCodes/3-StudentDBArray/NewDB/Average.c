// header files
#include <stdio.h>

// macro definitions
#define NUM_STUDENTS 2
#define NAME_OF_SUB 3

int index = 0;

// entry - point function
int main(void)
{
	// function declaration
	float MyAverage(int, int, int);

	int marksOfMathsArr[NUM_STUDENTS] = { 0 };
	int marksOfEnglishArr[NUM_STUDENTS] = { 0 };
	int marksOfMarathiArr[NUM_STUDENTS] = { 0 };
	float avgMarksArr[NUM_STUDENTS] = { 0.0f };


	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Enter Marks Of Maths : ");
		scanf("%d", &marksOfMathsArr[i]);

		printf("Enter Marks Of English : ");
		scanf("%d", &marksOfEnglishArr[i]);

		printf("Enter Marks Of Marathi : ");
		scanf("%d", &marksOfMarathiArr[i]);
	}

	printf("Showing Details You have entered : \n");
	for(int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("Student %d Data : \n", i + 1);
		printf("Maths : %d\n", marksOfMathsArr[i]);
		printf("English : %d\n", marksOfEnglishArr[i]);
		printf("Marathi : %d\n", marksOfMarathiArr[i]);
	}

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		avgMarksArr[i] = MyAverage(marksOfMathsArr[i], marksOfEnglishArr[i], marksOfMarathiArr[i]);
	}

	// printing Average
	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("%f\n", avgMarksArr[i]);
	}

	return(0);
}

float MyAverage(int maths, int english, int marathi)
{
	// variable declaration
	float average = 0.0f;

	average = (maths + english + marathi) / NAME_OF_SUB;

	return(average);
}
