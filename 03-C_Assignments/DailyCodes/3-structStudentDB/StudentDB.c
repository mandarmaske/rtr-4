// header files
#include <stdio.h>

// macro definations
#define NO_OF_STUDENTS 2
#define LENGTH_NAME 21
#define BirthDate_Length 15

// struct definations
struct StudentDB
{
	char name[LENGTH_NAME];
	char gender;
	char birthdate[BirthDate_Length];
	int maths, english, marathi;
	int avg_marks;
} StudentRec[NO_OF_STUDENTS];


// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	void MyGetS(char[], int);

	// variable declarations
	float avg;

	printf("Mandar Dilip Maske RTR2021-116\n\n");
	printf("Students Data Base =========>\n\n");

	for (int i = 0; i < NO_OF_STUDENTS; i++)
	{
		printf("Enter Details of Student %d\n\n", i+1);

		printf("Enter Student Name : ");
		MyGetS(StudentRec[i].name, LENGTH_NAME);

		printf("Enter Gender (M/F) : ");
		StudentRec[i].gender = getch();
		printf("%c\n", StudentRec[i].gender);

		printf("Enter Birthdate (In This Format DD-MM-YYYY) : ");
		MyGetS(StudentRec[i].birthdate, BirthDate_Length);

		printf("\n\n");
		printf("Enter Marks :- \n");
		printf("Maths : ");
		scanf("%d", &StudentRec[i].maths);

		printf("English : ");
		scanf("%d", &StudentRec[i].english);

		printf("Marathi : ");
		scanf("%d", &StudentRec[i].marathi);
		printf("\n\n");
	}

	printf("These are the details you have entered :- \n\n");

	for (int i = 0; i < NO_OF_STUDENTS; i++)
	{
		printf("********************&&************************\n\n");

		printf("Name\t\t\t : %s\n", StudentRec[i].name);
		printf("Gender\t\t\t : %c\n", StudentRec[i].gender);
		printf("Birthdate\t\t : %s\n", StudentRec[i].birthdate);
		printf("Maths Marks\t\t : %d\n", StudentRec[i].maths);
		printf("English Marks\t\t : %d\n", StudentRec[i].english);
		printf("Marathi Marks\t\t : %d\n", StudentRec[i].marathi);

		avg = (StudentRec[i].maths + StudentRec[i].english + StudentRec[i].marathi) / 3;

		printf("Average Marks\t\t : %f\n", avg);
	}

	return(0);
}

void MyGetS(char mystr[], int length)
{
	int i = 0;
	char ch = '\0';

	do
	{
		ch = getch();
		mystr[i] = ch;
		printf("%c", mystr[i]);
		i++;
	} while ((ch != '\r') && (i < length));

	if (i == length)
		mystr[i - 1] = '\0';
	else
		mystr[i] = '\0';
}
