// Initialization / Defination / Declaration  
// how we can write constants in different ways - 1

// header files
#include<stdio.h>

// entry-point function
int main(void)
{
	// code 
	// a = 5; - error because it is not declared that which type of data 'a' have or how many memory it will require

	int a = 5; // Intialization / Defination
	const int b = 20;
	// b = 10; -  error

	printf("\na = %d\n\nb = %d\n", a, b);

	return(0);
}
