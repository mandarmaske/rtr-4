// how we can write constants in different ways - 2
// header file
#include<stdio.h>
#define MYVAR_PI 3.1426 // constant value ahe so cannot change

// entry-point function
int main(void)
{
	// code
	printf("\nValue of PI is %lf\n", MYVAR_PI); // %lf lavala tar value 3.142600
	printf("\nValue of PI is %g\n", MYVAR_PI);  // %g lavala tar fraction madhle pudhche numbers 0 astil
	                                            // for e.g 3.142600 ti value madhle 0's he kadhale jatat ani aaplyala value milte
												// 3.1426
	return(0);
}