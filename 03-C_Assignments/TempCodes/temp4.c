// how we can write constants in different ways - 3
// header file
#include<stdio.h>

enum
{
	ONE,
	TWO,
	THREE,
	FOUR
};

/* Other method for enum - takes only values as numbers or characters
	enum // will start from ONE = 1 then 2, 3 then at from FOUR numbering will be changed  
	{
		ONE = 1,
		TWO,
		THREE,
		FOUR = 6,
		FIVE
	}
*/

// entry-point function
int main(void)
{
	// code
	printf("\nONE\t = %ld\n\nTWO\t = %ld\n\nTHREE\t = %ld\n\nFOUR\t = %ld\n", ONE, TWO, THREE, FOUR);

	return(0);
}