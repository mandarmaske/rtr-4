// header file
#include<stdio.h>

// defining struct
struct mdm_MyData
{
	int mdm_i;
	float mdm_f;
	char mdm_c;
} mdm_Data;

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	int mdm_size_of_struct;

	mdm_size_of_struct = sizeof(mdm_Data);
	printf("Size Of Struct = %d\n\n", mdm_size_of_struct);

	return(0);
}