// header files
#include <stdio.h>
#include <stdlib.h>

// macro definations
#define ARRAY_SIZE 5

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int iArray[ARRAY_SIZE] = { 0 };

	// code

	for (int i = 0; i < 5; i++)
	{
		iArray[i] = (i + 1) * 10;
	}

	printf("\n\n");

	for (int i = 0; i < 5; i++)
	{
		printf("the value at index %d is %d\n",i,  iArray[i]);
	}

	// malloc() (void*)malloc(size_t)
	int* iPtr = NULL;

	iPtr = (int*)malloc(ARRAY_SIZE * sizeof(int));

	if (iPtr == NULL)
	{
		printf("Memory Allocation Failed...\n");
		return(-1);
	}

	for (int i = 0; i < 5; i++)
	{
		*(iPtr + i)  = (i + 1) * 100;
	}

	for (int i = 0; i < 5; i++)
	{
		printf("the value at index %d is %d\n", i, *(iPtr+ i));
	}

	free(iPtr);

	iPtr = NULL;

	return(0);
}