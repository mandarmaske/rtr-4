// header file
#include <stdio.h>
#include <stdlib.h>

// macro defination
#define ARRAY_SIZE 5

// entry-point function
int main(void)
{
	// variable declarations
	int row = 0;
	int column = 0;
	int** iArray = NULL;
	int i, j;

	// code

	printf("Enter No. of row you want : ");
	scanf("%d", &row);

	printf("Enter No. of column you want : ");
	scanf("%d", &column);

	// arr[2][4]

	iArray = (int**)malloc(row * sizeof(int*));

	if (iArray == NULL)
	{
		printf("Memory Allocation Failed...\n");
		exit(0);
	}

	for (int i = 0; i < row; i++)
	{
		iArray[i] = (int*)malloc(column * sizeof(int));

		if (iArray[i] == NULL)
		{
			printf("Memory Allocation for row %d is Failed\n", *iArray[i]);
			exit(0);
		}
	}

	printf("Enter 2D Array Values : \n");

	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < column; j++)
		{
			scanf("%d", &iArray[i][j]);
		}
	}

	printf("\n\n");
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < column; j++)
		{
			printf("%d\t", iArray[i][j]);
		}
		printf("\n");
	}
		

	return(0);
}