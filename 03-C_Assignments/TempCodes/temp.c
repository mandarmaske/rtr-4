// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// code
	int a = 20;
	const int b; // garbage value will be initialized
	// b = 10; - error

	printf("Value of a is %d\n", a);
	printf("Value of b is %d\n", b);

	return(0);
}
