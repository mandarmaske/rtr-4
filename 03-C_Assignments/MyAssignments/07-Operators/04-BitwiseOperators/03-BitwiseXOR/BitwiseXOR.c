// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// function declaration / prototype
	void PrintBinaryFormOfNumber(unsigned int);

	// variable declarations
	unsigned int mdm_a;
	unsigned int mdm_b;
	unsigned int mdm_result;

	// code
	printf("\n\n");
	printf("Enter a number\nA = ");
	scanf("%u", &mdm_a);

	printf("\n\n");
	printf("Enter another number\nB = ");
	scanf("%u", &mdm_b);

	printf("\n\n\n");
	mdm_result = mdm_a ^ mdm_b;
	printf("Bitwise XOR-ing of\nA = %u (Decimal) and B = %u (Decimal) gives result %u (Decimal)\n\n", mdm_a, mdm_b, mdm_result);

	PrintBinaryFormOfNumber(mdm_a);
	PrintBinaryFormOfNumber(mdm_b);
	PrintBinaryFormOfNumber(mdm_result);

	return(0);
}

void PrintBinaryFormOfNumber(unsigned int mdm_decimalNumber)
{
	// variable declarations
	unsigned int mdm_quotient, mdm_remainder;
	unsigned int mdm_num;
	unsigned int mdm_binary_array[8];
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 8; mdm_i++)
		mdm_binary_array[mdm_i] = 0;

	printf("The Binary Form of the Decimal Integer %u is \t=\t", mdm_decimalNumber);
	mdm_num = mdm_decimalNumber;

	mdm_i = 7;
	while (mdm_num != 0)
	{
		mdm_quotient = mdm_num / 2;
		mdm_remainder = mdm_num % 2;
		mdm_binary_array[mdm_i] = mdm_remainder;
		mdm_num = mdm_quotient;
		mdm_i--;
	}

	for (mdm_i = 0; mdm_i < 8; mdm_i++)
		printf("%u", mdm_binary_array[mdm_i]);

	printf("\n\n");
}
