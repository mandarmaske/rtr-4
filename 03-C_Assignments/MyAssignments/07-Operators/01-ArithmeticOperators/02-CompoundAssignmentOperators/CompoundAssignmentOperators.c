// header file
#include<stdio.h>

// entry-point function 
int main(void)
{
	// variable declarations
	int mdm_a;
	int mdm_b;
	int mdm_x;

	// code
	printf("\n\n");
	printf("Enter a number : ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter another number : ");
	scanf("%d", &mdm_b);

	printf("\n\n");

	/*
		Since all the following 5 cases, the operand on the left 'mdm_a' is getting repeated immediately on the right 
		(e.g mdm_a = mdm_a + mdm_b or mdm_a = mdm_a - mdm_b)
		We are using Compound Assignment Operators which are +=, -=, *=, /= and %=
		Since, 'mdm_a' will be assigned the value of (mdm_a + mdm_b) at the expression (mdm_a += mdm_b),
		we must save the original value of 'mdm_a' to another variable (mdm_x) - temporary variable
	*/
	
	mdm_x = mdm_a;

	mdm_a += mdm_b; // mdm_a = mdm_a + mdm_b;
	printf("The Addition of mdm_a = %d and mdm_b = %d which is assigned to mdm_a gives %d\n", mdm_x, mdm_b, mdm_a);

	/*
		Value of 'mdm_a' altered in the above expression is used here...
		Since, 'mdm_a' will be assigned the value of (mdm_a - mdm_b) at the expression (mdm_a -= mdm_b),
		We must save the orinal value of 'mdm_a' to another variable (mdm_x) - temporary variable
	*/

	mdm_x = mdm_a;
	mdm_a -= mdm_b; // mdm_a = mdm_a - mdm_b;
	printf("The Subtraction of mdm_a = %d and mdm_b = %d which is assigned to mdm_a gives %d\n", mdm_x, mdm_b, mdm_a);

	/*
		Value of 'mdm_a' altered in the above expression is used here...
		Since, 'mdm_a' will be assigned the value of (mdm_a * mdm_b) at the expression (mdm_a *= mdm_b),
		We must save the orinal value of 'mdm_a' to another variable (mdm_x) - temporary variable
	*/

	mdm_x = mdm_a;
	mdm_a *= mdm_b;
	printf("The Multiplication of mdm_a = %d and mdm_b = %d which is assigned to mdm_a gives %d\n", mdm_x, mdm_b, mdm_a);

	/*
		Value of 'mdm_a' altered in the above expression is used here...
		Since, 'mdm_a' will be assigned the value of (mdm_a / mdm_b) at the expression (mdm_a /= mdm_b),
		We must save the orinal value of 'mdm_a' to another variable (mdm_x) - temporary variable
	*/

	mdm_x = mdm_a;
	mdm_a /= mdm_b;
	printf("The Division of mdm_a = %d and mdm_b = %d which is assigned to mdm_a gives Quotient %d\n", mdm_x, mdm_b, mdm_a);

	/*
		Value of 'mdm_a' altered in the above expression is used here...
		Since, 'mdm_a' will be assigned the value of (mdm_a % mdm_b) at the expression (mdm_a %= mdm_b),
		We must save the orinal value of 'mdm_a' to another variable (mdm_x) - temporary variable
	*/

	mdm_x = mdm_a;
	mdm_a %= mdm_b;
	printf("The Division of mdm_a = %d and mdm_b = %d which is assigned to mdm_a gives remainder %d\n", mdm_x, mdm_b, mdm_a);

	printf("\n\n");

	return(0);
}
