// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a = 5;
	int mdm_b = 10;

	// code
	printf("\n\n");
	printf("Using Increment Operators\n");
	printf("mdm_a = %d\n", mdm_a);    // 5 
	printf("mdm_a = %d\n", mdm_a++);  // 5
	printf("mdm_a = %d\n", mdm_a);    // 6
	printf("mdm_a = %d\n", ++mdm_a);  // 7

	printf("\n");
	printf("Using Decrement Operators\n");
	printf("mdm_b = %d\n", mdm_b);    // 10
	printf("mdm_b = %d\n", mdm_b++);  // 10
	printf("mdm_b = %d\n", mdm_b);    // 11
	printf("mdm_b = %d\n", --mdm_b);  // 10

	printf("\n");

	return(0);
}
