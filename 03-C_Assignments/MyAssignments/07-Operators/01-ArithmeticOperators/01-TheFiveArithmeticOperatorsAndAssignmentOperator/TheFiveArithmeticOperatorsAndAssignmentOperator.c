// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a;
	int mdm_b;
	int mdm_result;

	// code
	printf("\n\n");
	printf("Enter a Number : ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &mdm_b);

	printf("\n\n");

	/*
		The Following are the 5 Arithmetic Operators +, -, *, / and %
		Also, the resultants of the Arithmetic Operators In all the below five cases have been assigned to a variable 'mdm_result'
		using Assignment Operator (=)
	*/

	mdm_result = mdm_a + mdm_b;
	printf("The Addition of mdm_a = %d and mdm_b = %d gives %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = mdm_a - mdm_b;
	printf("The Subtraction of mdm_a = %d and mdm_b = %d gives %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = mdm_a * mdm_b;
	printf("The Multiplication of mdm_a = %d and mdm_b = %d gives %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = mdm_a / mdm_b;
	printf("The Division of mdm_a = %d and mdm_b = %d gives Quotient %d\n", mdm_a, mdm_b, mdm_result);;

	mdm_result = mdm_a % mdm_b;
	printf("The Division of mdm_a = %d and mdm_b = %d gives remainder %d\n", mdm_a, mdm_b, mdm_result);

	printf("\n\n");

	return(0);
}
