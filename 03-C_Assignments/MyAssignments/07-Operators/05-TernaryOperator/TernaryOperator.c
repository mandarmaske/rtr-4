// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a = 0, mdm_b = 0;
	int mdm_p = 0, mdm_q = 0;
	char mdm_chResult_1 = 0, mdm_chResult_2 = 0;
	int mdm_iResult_1 = 0, mdm_iResult_2 = 0;

	// code
	printf("\n\n");

	mdm_a = 7;
	mdm_b = 5;
	mdm_chResult_1 = (mdm_a > mdm_b) ? 'A' : 'B';
	mdm_iResult_1 = (mdm_a > mdm_b) ? mdm_a : mdm_b;
	printf("Ternary Operator Answer 1 ------> %c and %d\n\n", mdm_chResult_1, mdm_iResult_1);

	mdm_p = 30;
	mdm_q = 30;
	mdm_chResult_2 = (mdm_p != mdm_q) ? 'P' : 'Q';
	mdm_iResult_2 = (mdm_p != mdm_q) ? mdm_p : mdm_q;
	printf("Ternary Operator Answer 2 ------> %c and %d\n\n", mdm_chResult_2, mdm_iResult_2);

	printf("\n");

	return(0);
}
