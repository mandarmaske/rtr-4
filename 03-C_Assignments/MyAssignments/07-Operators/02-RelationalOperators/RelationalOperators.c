// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a;
	int mdm_b;
	int mdm_result;

	// code
	printf("\n\n");
	printf("Enter a number\nA = ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter another number\nB = ");
	scanf("%d", &mdm_b);

	printf("\n\n");
	printf("If mdm_Result = 0, It is 'False'.\n");
	printf("If mdm_result = 1, It is 'True'.\n");
	
	printf("\n\n");
	mdm_result = (mdm_a < mdm_b);
	printf("For Expression : (A < B)\n");
	printf("A = %d is less than B = %d						  Answer = %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = (mdm_a > mdm_b);
	printf("For Expression : (A > B)\n");
	printf("A = %d is greater than B = %d					\t  Answer = %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = (mdm_a <= mdm_b);
	printf("For Expression : (A <= B)\n");
	printf("A = %d is less than or equal to B = %d			\t\t  Answer = %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = (mdm_a >= mdm_b);
	printf("For Expression : (A >= B)\n");
	printf("A = %d is greater than or equal to B = %d	    \t\t\t  Answer = %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = (mdm_a == mdm_b);
	printf("For Expression : (A == B)\n");
	printf("A = %d is equal to B = %d						  Answer = %d\n", mdm_a, mdm_b, mdm_result);

	mdm_result = (mdm_a != mdm_b);
	printf("For Expression : (A != B)\n");
	printf("A = %d is not equal to B = %d                       \t\t\t  Answer = %d\n", mdm_a, mdm_b, mdm_result);

	printf("\n\n");
	
	return(0);
}
