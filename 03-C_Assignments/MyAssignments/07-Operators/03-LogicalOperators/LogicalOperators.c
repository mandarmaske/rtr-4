// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a;
	int mdm_b;
	int mdm_c;
	int mdm_result;

	// code
	printf("\n\n");
	printf("Enter 1st number\nA = ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter 2nd number\nB = ");
	scanf("%d", &mdm_b);

	printf("\n\n");
	printf("Enter 3rd number\nC = ");
	scanf("%d", &mdm_c);

	printf("\n\n");
	printf("If Answer = 0, it is 'False'.\n");
	printf("If Answer = 1, it is 'True'.\n");

	printf("\n\n");
	mdm_result = (mdm_a <= mdm_b) && (mdm_b != mdm_c);
	printf("LOGICAL AND (&&) : Answer is TRUE (1) If and only if BOTH Conditions are TRUE. The Answer is FALSE(0), if any ONE or BOTH Conditions Are FALSE.\n\n");
	printf("A = %d is Less Than or Equal to B = %d AND B = %d is not equal to C = %d \tAnswer = %d\n\n", mdm_a, mdm_b, mdm_b, mdm_c, mdm_result);

	mdm_result = (mdm_b >= mdm_a) || (mdm_a == mdm_c);
	printf("LOGICAL OR (||) : Answer is False (0) If and only if BOTH Conditions are FALSE. The Answer is TRUE (1), if Any One Or Both Conditions are TRUE.\n\n");
	printf("B = %d is Greater Than or Equal to A = %d OR A = %d is Equal to C = %d \tAnswer = %d\n\n", mdm_b, mdm_a, mdm_a, mdm_c, mdm_result);

	mdm_result = !mdm_a;
	printf("A = %d and using LOGICAL NOT (!) Operator on A gives Result = %d\n\n", mdm_a, mdm_result);

	mdm_result = !mdm_b;
	printf("B = %d and using LOGICAL NOT (!) Operator on B gives Result = %d\n\n", mdm_b, mdm_result);

	mdm_result = !mdm_c;
	printf("C = %d and using LOGICAL NOT (!) Operator on C gives Result = %d\n\n", mdm_c, mdm_result);

	return(0);
}
