// header file
#include<stdio.h>

// global typedef
typedef int mdm_Int; // "type" int has been re"def"ined as "mdm_Int"... Now "mdm_Int" can be treated just like "int"

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	mdm_Int mdm_Add(mdm_Int, mdm_Int);

	// typedefs
	typedef int mdm_Int; // "type" int has been re"def"ined as "mdm_Int"... Now "mdm_Int" can be treated just like "int"
	typedef float mdm_Float; // "type" float has been re"def"ined as "mdm_Float"... Now "mdm_Float" can be treated just like "float"
	typedef char mdm_Char; // "type" char has been re"def"ined as "mdm_Char"... Now "mdm_Char" can be treated just like "char"
	typedef double mdm_Double; // "type" double has been re"def"ined as "mdm_Double"... Now "mdm_Double" can be treated just like "double"

	// ******** Just Like Win32 SDK ********
	typedef unsigned int UINT;
	typedef UINT HANDLE;
	typedef HANDLE HWND;
	typedef HANDLE HINSTANCE;

	// variable declarations
	mdm_Int mdm_a = 10, mdm_i;
	mdm_Int mdm_iArray[] = { 9, 18, 27, 36, 45, 54, 63, 72, 81, 90 };

	mdm_Float mdm_f_avg = 30.9f;
	const mdm_Float mdm_f_avg_pi = 3.14f;

	mdm_Char mdm_ch = '*';
	mdm_Char mdm_chArray1[] = "Hello";
	mdm_Char mdm_chArray2[][10] = { "RTR", "Batch", "2021-2023" };

	mdm_Double mdm_d = 8.0452327;

	// ******** Just Random Numbers - They Have Nothing To Do With Any WINDOW'S HANDLE Or INSTANCE HANDLE !!! This is just for understanding ********
	UINT mdm_uint = 341;
	HANDLE mdm_handle = 934;
	HWND mdm_hwnd = 834;
	HINSTANCE mdm_hInstance = 1414;

	// code
	printf("\n\n");
	printf("Type mdm_Int variable mdm_a = %d\n", mdm_a);
	printf("\n\n");

	for (mdm_i = 0; mdm_i < sizeof(mdm_iArray) / sizeof(mdm_Int); mdm_i++)
	{
		printf("Type 'mdm_Int' array variable mdm_iArray[%d] = %d\n", mdm_i, mdm_iArray[mdm_i]);
	}

	printf("\n\n");
	printf("Type mdm_Float variable mdm_f = %f\n", mdm_f_avg);
	printf("Type mdm_Float variable constant mdm_f_avg_pi = %f\n", mdm_f_avg_pi);

	printf("\n\n");
	printf("Type mdm_Double variable mdm_d = %lf\n", mdm_d);

	printf("\n\n");
	printf("Type mdm_Char variable mdm_ch = %c\n", mdm_ch);

	printf("\n\n");
	printf("Type mdm_Char array variable mdm_chArray1[] = %s\n", mdm_chArray1);

	printf("\n\n");
	for (mdm_i = 0; mdm_i < sizeof(mdm_chArray2) / sizeof(mdm_chArray2[0]); mdm_i++)
	{
		printf("%s ", mdm_chArray2[mdm_i]);
	}

	printf("\n\n");

	printf("\n\n");
	printf("Type UINT variable mdm_uint = %u\n\n", mdm_uint);
	printf("Type HANDLE variable mdm_handle = %u\n\n", mdm_handle);
	printf("Type HWND variable mdm_hwnd = %u\n\n", mdm_hwnd);
	printf("Type HINSTANCE variable mdm_hInstance = %u\n\n", mdm_hInstance);
	printf("\n\n");

	mdm_Int mdm_x = 90;
	mdm_Int mdm_y = 30;
	mdm_Int mdm_ret;

	mdm_ret = mdm_Add(mdm_x, mdm_y);
	printf("mdm_ret = %d\n\n", mdm_ret);

	return(0);
}

mdm_Int mdm_Add(mdm_Int mdm_a, mdm_Int mdm_b)
{
	// code
	mdm_Int mdm_c;
	mdm_c = mdm_a + mdm_b;
	return(mdm_c);
}

