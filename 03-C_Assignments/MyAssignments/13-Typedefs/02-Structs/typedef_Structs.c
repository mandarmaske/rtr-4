// header file
#include<stdio.h>

// constant value declaration
#define MDM_MAX_NAME_LENGTH 100

// defining struct
struct mdm_Employee
{
	char mdm_name[MDM_MAX_NAME_LENGTH];
	unsigned int mdm_age;
	char mdm_gender;
	double mdm_salary;
};

// defining struct
struct mdm_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// typedefs
	typedef struct mdm_Employee MDM_EMPLOYEE_TYPE;
	typedef struct mdm_MyData MDM_DATA_TYPE;

	// variable declarations
	struct mdm_Employee mdm_emp = { "Sunny", 25, 'M', 100000.00 };
	MDM_EMPLOYEE_TYPE mdm_emp_typedef = { "Bunny", 23, 'F', 20400.00 };

	struct mdm_MyData mdm_md = { 30, 11.45f, 26.122017, 'X' };
	MDM_DATA_TYPE mdm_md_typedef;

	// code
	mdm_md_typedef.mdm_i = 9;
	mdm_md_typedef.mdm_f = 1.5f;
	mdm_md_typedef.mdm_d = 8.041997;
	mdm_md_typedef.mdm_c = 'M';

	printf("\n\n");
	printf("struct mdm_Employee : \n\n");
	printf("mdm_emp.mdm_name = %s\n", mdm_emp.mdm_name);
	printf("mdm_emp.mdm_age = %d\n", mdm_emp.mdm_age);
	printf("mdm_emp.mdm_gender = %c\n", mdm_emp.mdm_gender);
	printf("mdm_emp.mdm_salary = %lf\n", mdm_emp.mdm_salary);

	printf("\n\n");
	printf("MDM_EMPLOYEE_TYPE : \n\n");
	printf("mdm_emp_typedef.mdm_name = %s\n", mdm_emp_typedef.mdm_name);
	printf("mdm_emp_typedef.mdm_age = %d\n", mdm_emp_typedef.mdm_age);
	printf("mdm_emp_typedef.mdm_gender = %c\n", mdm_emp_typedef.mdm_gender);
	printf("mdm_emp_typedef.mdm_salary = %lf\n", mdm_emp_typedef.mdm_salary);

	printf("\n\n");
	printf("struct mdm_MyData : \n\n");
	printf("mdm_md.mdm_i = %d\n", mdm_md.mdm_i);
	printf("mdm_md.mdm_f = %f\n", mdm_md.mdm_f);
	printf("mdm_md.mdm_d = %lf\n", mdm_md.mdm_d);
	printf("mdm_md.mdm_c = %c\n", mdm_md.mdm_c);

	printf("\n\n");
	printf("MDM_DATA_TYPE : \n\n");
	printf("mdm_md_typedef.mdm_i = %d\n", mdm_md_typedef.mdm_i);
	printf("mdm_md_typedef.mdm_f = %f\n", mdm_md_typedef.mdm_f);
	printf("mdm_md_typedef.mdm_d = %lf\n", mdm_md_typedef.mdm_d);
	printf("mdm_md_typedef.mdm_c = %c\n", mdm_md_typedef.mdm_c);

	printf("\n\n");

	return(0);
}

