// header file
#include<stdio.h>

// macro defination
#define MDM_PI_VERSION 5 // Change value and see the output. Also, comment this line and number 34 and see the output

#ifdef MDM_PI_VERSION
#if MDM_PI_VERSION <= 0
	#define MDM_PI 3.14
#elif MDM_PI_VERSION == 1
	#define MDM_PI 3.1415
#elif MDM_PI_VERSION == 2
	#define MDM_PI 3.141592
#elif MDM_PI_VERSION == 3
	#define MDM_PI 3.14159265
#elif MDM_PI_VERSION == 4
	#define MDM_PI 3.1415926535
#elif MDM_PI_VERSION == 5
	#define MDM_PI 3.141592652589
#else
	#define MDM_PI 3.141592652589793
#endif // #if
#endif // #ifdef

#ifndef MDM_PI_VERSION
	#define MDM_PI 0.0
#endif

int main(int argc, char* argv[], char* envp[])
{
	// code
	printf("\n\n");
	printf("Hello World !!!\n");

	printf("\n\n");
	printf("PI Version Selected = %d\n", MDM_PI_VERSION);
	printf("PI Value = %.15lf\n\n", MDM_PI);

	return(0);
}