// header files
#include<stdio.h>
#include<stdarg.h>

// macro definations
#define MDM_NUM_TO_BE_FOUND 3
#define MDM_NUM_ELEMENTS 10

// entry - point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototypes
	void MDM_FindNumber(int, int, ...);

	// code
	printf("\n\n");

	MDM_FindNumber(MDM_NUM_TO_BE_FOUND, MDM_NUM_ELEMENTS, 3, 5, 9, 2, 3, 6, 9, 3, 1, 3);

	return(0);
}

void MDM_FindNumber(int mdm_num_to_be_found, int mdm_num, ...) // Variadic Function
{
	// function prototypes
	int MDM_va_FindNumber(int, int, va_list);

	// variable declarations
	int mdm_count = 0;
	int mdm_n;
	va_list mdm_numbers_list;

	// code
	va_start(mdm_numbers_list, mdm_num);

	mdm_count = MDM_va_FindNumber(mdm_num_to_be_found, mdm_num, mdm_numbers_list);

	if (mdm_count == 0)
		printf("Numbers %d Could Not Be Found !!!\n\n", mdm_num_to_be_found);
	else
		printf("Number %d Found %d Times !!!\n\n", mdm_num_to_be_found, mdm_count);

	va_end(mdm_numbers_list);
}


int MDM_va_FindNumber(int mdm_num_to_be_found, int mdm_num, va_list mdm_list)
{
	// variable declarations
	int mdm_count_of_num = 0;
	int mdm_n;

	// code
	while (mdm_num)
	{
		mdm_n = va_arg(mdm_list, int);
		if (mdm_n == mdm_num_to_be_found)
			mdm_count_of_num++;

		mdm_num--;
	}

	return(mdm_count_of_num);
}


