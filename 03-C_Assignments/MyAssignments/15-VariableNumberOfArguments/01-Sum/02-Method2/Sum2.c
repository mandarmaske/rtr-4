// header files
#include<stdio.h>
#include<stdarg.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int MDM_CalculateSum(int, ...);

	// variable declaration
	int mdm_answer;

	// code
	printf("\n\n");

	mdm_answer = MDM_CalculateSum(5, 10, 20, 30, 40, 50);
	printf("Answer = %d\n\n", mdm_answer);

	mdm_answer = MDM_CalculateSum(10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
	printf("Answer = %d\n\n", mdm_answer);

	mdm_answer = MDM_CalculateSum(0);
	printf("Answer = %d\n\n", mdm_answer);

	return(0);
}

int MDM_CalculateSum(int mdm_num, ...) // Variadic Function
{
	// function prototype
	int MDM_va_CalculateSum(int, va_list);

	// variable declarations
	int mdm_sum = 0;
	va_list mdm_numbers_list;

	// code
	va_start(mdm_numbers_list, mdm_num);

	mdm_sum = MDM_va_CalculateSum(mdm_num, mdm_numbers_list);

	va_end(mdm_numbers_list);

	return(mdm_sum);
}

int MDM_va_CalculateSum(int mdm_num, va_list mdm_list)
{
	// variable declarations
	int mdm_n;
	int mdm_sum_total = 0;

	// code
	while (mdm_num)
	{
		mdm_n = va_arg(mdm_list, int);
		mdm_sum_total = mdm_sum_total + mdm_n;
		mdm_num--;
	}

	return(mdm_sum_total);
}


