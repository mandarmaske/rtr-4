// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// code

	printf("\n\n");

	printf("Size of 'int'	       :  %ld Bytes\n", sizeof(int));
	printf("Size of 'unsigned int' :  %ld Bytes\n", sizeof(unsigned int));
	printf("Size of 'long'         :  %ld Bytes\n", sizeof(long));
	printf("Size of 'long long'    :  %ld Bytes\n", sizeof(long long));
	printf("\n");

	printf("Size of 'float'        :  %ld Bytes\n", sizeof(float));
	printf("Size of 'double'       :  %ld Bytes\n", sizeof(double));
	printf("Size of 'long double'  :  %ld Bytes\n", sizeof(long double));
	printf("\n");

	return(0);
}
