// header file 
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_a;

	// code
	printf("\n\n");

	mdm_a = 5;
	if (mdm_a) // no-zero positive value
	{
		printf("if block 1 : 'A' Exists And Has Value = %d !!!\n\n", mdm_a);
	}

	mdm_a = -5;
	if (mdm_a) // non-zero negative value
	{
		printf("if block 2 : 'A' Exists And Has Value = %d !!!\n\n", mdm_a);
	}

	mdm_a = 0;
	if (mdm_a) // zero value
	{
		printf("if block 3 : 'A' exists and has value = %d !!!\n\n", mdm_a);
	}

	printf("All three if-statements are done !!!\n\n");

	return(0);
}
