// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_age;

	// code
	printf("\n\n");
	printf("Enter Your Age = ");
	scanf("%d", &mdm_age);
	printf("\n\n");

	if (mdm_age >= 18)
	{
		printf("You are eligible for Voting\n\n");
	}

	return(0);
}
