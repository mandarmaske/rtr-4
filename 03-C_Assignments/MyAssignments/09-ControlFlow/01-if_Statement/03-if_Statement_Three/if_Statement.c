// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_num;

	// code
	printf("\n\n");

	printf("Enter a Value\nNUM = ");
	scanf("%d", &mdm_num);
	printf("\n\n");

	if (mdm_num < 0)
	{
		printf("NUM = %d is less than 0, it's a negative number\n\n", mdm_num);
	}

	if ((mdm_num > 0) && (mdm_num <= 100))
	{
		printf("NUM = %d is greater than 0 and it is less than 100\n\n", mdm_num);
	}

	if ((mdm_num > 100) && (mdm_num <= 200))
	{
		printf("NUM = %d is between 101 and 200\n\n", mdm_num);
	}

	if ((mdm_num > 200) && (mdm_num <= 300))
	{
		printf("NUM = %d is between 201 and 300\n\n", mdm_num);
	}

	if ((mdm_num > 300) && (mdm_num <= 400))
	{
		printf("NUM = %d is between 301 and 400\n\n", mdm_num);
	}

	if ((mdm_num > 400) && (mdm_num <= 500))
	{
		printf("NUM = %d is between 401 and 500\n\n", mdm_num);
	}

	if (mdm_num > 500)
	{
		printf("NUM = %d is greater than 500\n\n", mdm_num);
	}

	return(0);
}
