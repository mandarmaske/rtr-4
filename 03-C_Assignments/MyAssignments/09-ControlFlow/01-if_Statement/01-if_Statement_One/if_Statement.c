// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a, mdm_b, mdm_p;

	// code
	mdm_a = 9;
	mdm_b = 30;
	mdm_p = 30;

	printf("\n\n");

	if (mdm_a < mdm_b) // 1
	{
		printf("A is less than B.\n\n");
	}

	if (mdm_b != mdm_p) // 0
	{
		printf("A is not equal to P.\n\n"); // will not execute
	}

	printf("Both comparisons have been done.\n\n");
	
	return(0);
}
