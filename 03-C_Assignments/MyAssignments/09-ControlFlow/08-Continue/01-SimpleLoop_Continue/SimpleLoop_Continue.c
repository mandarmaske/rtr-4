// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i;

	// code
	printf("\n\n");

	printf("Printing Even Numbers from 0 to 100 : \n\n\n");

	for (mdm_i = 0; mdm_i <= 100; mdm_i++)
	{
		// condition for a number to be Even Number -> division of number by 2 leaves no remainder (remainder = 0)
		// if remainder is not 0, the number is Odd Number

		if (mdm_i % 2 != 0 || mdm_i == 0)
			continue;

		else
			printf("\t%d\n", mdm_i);
	}

	printf("\n");

	return(0);
}
