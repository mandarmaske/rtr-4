// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j;

	// code
	printf("\n\n");

	printf("Outer Loop Prints Odd Numbers Between 1 and 10\n\n");
	printf("Inner Loop Prints Even Numbers Between 1 and 10 for Every Odd Number Printed by Outer Loop\n\n");

	// condition for a number to be Even Number -> division of number by 2 leaves no remainder (remainder = o)
	// condition for a number to be Odd Number -> division of number by 2 leaves remainder (remainder = 1 (Usually))

	for (mdm_i = 1; mdm_i <= 10; mdm_i++)
	{
		if (mdm_i % 2 != 0) // if number (1-Usually) is Odd
		{
			for (mdm_j = 1; mdm_j <= 10; mdm_j++)
			{
				if (mdm_j % 2 == 0)
					printf("\ti = %d\tj = %d\n", mdm_i, mdm_j);
				else
					continue;
			}

			printf("\n");
		}
		else
			continue;
	}

	return(0);
}