// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i;
	char mdm_ch;

	// code
	printf("\n\n");

	printf("Printing Even Numbers from 0 to 100 : \n\n");
	printf("To Print Even Numbers Press Any Character Except 'Q' or 'q'\n\n");
	printf("Enter Character 'Q' or 'q' to Exit Loop : \n\n");

	for (mdm_i = 1; mdm_i <= 100; mdm_i++)
	{
		if (mdm_i % 2 == 0)
		{
			printf("\t%d\n", mdm_i);
			mdm_ch = getch();
			continue;
		}	
		if (mdm_ch == 'Q' || mdm_ch == 'q')
			break;
	}

	printf("\n\n");
	printf("Exitting the Loop...\n\n");
	printf("\n");

	return(0);
}
