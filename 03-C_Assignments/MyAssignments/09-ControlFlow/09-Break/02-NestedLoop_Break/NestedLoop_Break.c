// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_i, mdm_j;

	// code
	printf("\n\n");

	for (mdm_i = 1; mdm_i <= 20; mdm_i++)
	{
		for (mdm_j = 1; mdm_j <= 20; mdm_j++)
		{
			if (mdm_j > mdm_i)
				break;
			else
				printf("* ");
		}

		printf("\n\n\n");
	}

	return(0);
}
