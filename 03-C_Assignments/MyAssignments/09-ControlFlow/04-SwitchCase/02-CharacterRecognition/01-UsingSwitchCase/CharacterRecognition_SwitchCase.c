// header files
#include<stdio.h> // for printf();
#include<conio.h> // for getch();

// ASCII Values for 'A' to 'Z' -> 65 to 90
#define CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define CHAR_ALPHABET_UPPER_CASE_ENDING 90

// ASCII Values for 'a' to 'z' -> 97 to 122
#define CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define CHAR_ALPHABET_LOWER_CASE_ENDING 122

// ASCII Values for '0' to '9' -> 48 to 57
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

// entry-point function
int main(void)
{

	// variable declarations
	char mdm_ch;
	int mdm_ch_value;

	// code
	printf("\n\n");

	printf("Enter a character : ");
	mdm_ch = getch();
	printf("\n\n");

	switch (mdm_ch)
	{
	// fall through condition
	case 'A':
	case 'a':

	case 'E':
	case 'e':

	case 'I':
	case 'i':

	case 'O':
	case 'o':

	case 'U':
	case 'u':
		printf("Character \'%c\' Entered by you, is a Vowel Character from the English Alphabet !!!\n\n", mdm_ch);
		break;

	default:
		mdm_ch_value = (int)mdm_ch;
		// if the character has ASCII value between 65 and 90 OR between 97 and 122, it is still a letter of the Alphabet, but it is a 'CONSONANT' and not a 'VOWEL'
		if ((mdm_ch_value >= CHAR_ALPHABET_UPPER_CASE_BEGINNING && mdm_ch_value <= CHAR_ALPHABET_UPPER_CASE_ENDING) || (mdm_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING && mdm_ch_value <= CHAR_ALPHABET_LOWER_CASE_ENDING))
		{
			printf("Character \'%c\' Entered by you, is a CONSONANT CHARACTER from the ENGLISH Alphabet\n\n", mdm_ch);
		}

		else if (mdm_ch_value >= CHAR_DIGIT_BEGINNING && mdm_ch_value <= CHAR_DIGIT_ENDING)
		{
			printf("Character \'%c\' Enter by you, is a DIGIT CHARACTER\n\n", mdm_ch);
		}

		else
		{
			printf("Character \'%c\' entered by you, is a special character\n\n", mdm_ch);
		}
		break;
	}

	printf("\nSwitch Case ends here...\n\n");

	return(0);
}
