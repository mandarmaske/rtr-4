// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a, mdm_b;
	int mdm_result;
	char mdm_option, mdm_option_division;

	// code
	printf("\n\n");

	printf("Enter Value for A : ");
	scanf("%d", &mdm_a);
	printf("Enter Value for B : ");
	scanf("%d", &mdm_b);

	printf("\nEnter option in character : \n");
	printf("'A' or 'a' for Addition : \n");
	printf("'S' or 's' for Subtraction : \n");
	printf("'M' or 'm' for Multiplication : \n");
	printf("'D' or 'd' for Division : \n\n");

	printf("Enter Option : ");
	mdm_option = getch();

	printf("\n\n");

	switch (mdm_option)
	{
	// fall through condition for 'A' and 'a'
	case 'A':
	case 'a':
		mdm_result = mdm_a + mdm_b;
		printf("Addition of A = %d and B = %d gives Result = %d\n\n", mdm_a, mdm_b, mdm_result);
		break;

	// fall through condition for 'S' and 's'
	case 'S':
	case 's':
		if (mdm_a >= mdm_b)
		{
			mdm_result = mdm_a - mdm_b;
			printf("Subtraction of A = %d and B = %d gives Result = %d\n\n", mdm_a, mdm_b, mdm_result);
		}
		else
		{
			mdm_result = mdm_b - mdm_a;
			printf("Subtraction of B = %d and A = %d gives Result = %d\n\n", mdm_b, mdm_a, mdm_result);
		}
		break;

	// fall through condition for 'M' and 'm'
	case 'M':
	case 'm':
		mdm_result = mdm_a * mdm_b;
		printf("Multiplication of A = %d and B = %d gives Result = %d\n\n", mdm_a, mdm_b, mdm_result);
		break;

	// fall through condition for 'D' and 'd'
	case 'D':
	case 'd':
		printf("Enter option in Character : \n");
		printf("'Q' or 'q' or '/' for quotient upon Division : \n");
		printf("'R' or 'r' or '%%' for remainder upon division : \n");

		printf("\nEnter Option : ");
		mdm_option_division = getch();
		printf("\n\n");

		switch (mdm_option_division)
		{
		case 'Q':
		case 'q':
		case '/':
			if (mdm_a >= mdm_b)
			{
				mdm_result = mdm_a / mdm_b;
				printf("Division of A = %d and B = %d gives Quotient = %d\n\n", mdm_a, mdm_b, mdm_result);
			}
			else
			{
				mdm_result = mdm_b / mdm_a;
				printf("Division of B = %d and A = %d gives Quotient = %d\n\n", mdm_b, mdm_a, mdm_result);
			}
		break; // break of case 'Q', 'q', '/'

		// fall through condition 'R', 'r' and '%'
		case 'R':
		case 'r':
		case '%':
			if (mdm_a >= mdm_b)
			{
				mdm_result = mdm_a % mdm_b;
				printf("Division of A = %d and B = %d gives Remainder = %d\n\n", mdm_a, mdm_b, mdm_result);
			}
			else
			{
				mdm_result = mdm_b % mdm_a;
				printf("Division of B = %d and A = %d gives Remainder = %d\n\n", mdm_b, mdm_a, mdm_result);
			}
		break; // break of case 'R', 'r', '%'

		default: // default case for switch(mdm_option_division)
			printf("Invalid Character %c entered for Division, Please try again...\n\n", mdm_option_division);
			break; // break of default switch(mdm_option_division)
		}
	break; // break of 'D' and 'd'

	default: // default case of switch(mdm_option)
		printf("Entered Invalid Character %c, please try again...\n\n", mdm_option);
		break; 

	} // ending curly brace of switch(mdm_option)

	printf("Switch Case Block Complete\n\n");

	return(0);
}
