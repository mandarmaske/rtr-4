// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_a, mdm_b;
	int mdm_result;
	char mdm_option, mdm_option_division;

	// code
	printf("\n\n");

	printf("Enter a value for A : ");
	scanf("%d", &mdm_a);
	printf("Enter a value for B : ");
	scanf("%d", &mdm_b);

	printf("\n\nEnter option in character : \n\n");
	printf("'A' or 'a' for Addition : \n");
	printf("'S' or 's' for Subtraction : \n");
	printf("'M' or 'm' for Multiplication : \n");
	printf("'D' or 'd' for Division : \n\n");

	printf("Enter A Option : ");
	mdm_option = getch();
	printf("\n\n");

	if (mdm_option == 'A' || mdm_option == 'a')
	{
		mdm_result = mdm_a + mdm_b;
		printf("The Addition of A = %d and B = %d gives Result = %d\n\n", mdm_a, mdm_b, mdm_result);
	}
	else if (mdm_option == 'S' || mdm_option == 's')
	{
		if (mdm_a >= mdm_b)
		{
			mdm_result = mdm_a - mdm_b;
			printf("The Subtraction of A = %d and B = %d gives Result = %d\n\n", mdm_a, mdm_b, mdm_result);
		}
		else
		{
			mdm_result = mdm_b - mdm_a;
			printf("The Subtraction of B = %d and A = %d gives Result = %d\n\n", mdm_b, mdm_a, mdm_result);
		}
	}
	else if (mdm_option == 'M' || mdm_option == 'm')
	{
		mdm_result = mdm_a * mdm_b;
		printf("The Multiplication of A = %d and B = %d gives Result = %d\n\n", mdm_a, mdm_b, mdm_result);
	}
	else if (mdm_option == 'D' || mdm_option == 'd')
	{
		printf("Enter option in character : \n");
		printf("'Q' or 'q' or '/' for Quotient upon Division : \n");
		printf("'R' or 'r' or '%%' for Remainder upon Division : \n\n");

		printf("Enter Option : ");
		mdm_option_division = getch();

		printf("\n\n");

		if (mdm_option_division == 'Q' || mdm_option_division == 'q' || mdm_option_division == '/')
		{
			if (mdm_a >= mdm_b)
			{
				mdm_result = mdm_a / mdm_b;
				printf("The Division of A = %d and B = %d gives Quotient = %d\n\n", mdm_a, mdm_b, mdm_result);
			}
			else
			{
				mdm_result = mdm_b / mdm_a;
				printf("The Division of B = %d and A = %d gives Quotient = %d\n\n", mdm_b, mdm_a, mdm_result);
			}
		}
		else if (mdm_option_division == 'R' || mdm_option_division == 'r' || mdm_option_division == '%')
		{
			if (mdm_a >= mdm_b)
			{
				mdm_result = mdm_a % mdm_b;
				printf("The Division of A = %d and B = %d gives Remainder = %d\n\n", mdm_a, mdm_b, mdm_result);
			}
			else
			{
				mdm_result = mdm_b % mdm_a;
				printf("The Division of B = %d and A = %d gives Remainder = %d\n\n", mdm_b, mdm_a, mdm_result);
			}
		}
		else
		{
			printf("You Entered Invalid Character %c !!!\n\n", mdm_option_division);
		}
	}
	else
	{
		printf("You Entered a Invalid Character %c !!!\n\n", mdm_option);
	}

	printf("if-else-if-else Ladder ends here...\n\n");
	return(0);
}
