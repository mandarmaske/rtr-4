// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_num_month;

	// code
	printf("\n\n");

	printf("Enter Month Number : ");
	scanf("%d", &mdm_num_month);
	printf("\n\n");

	switch (mdm_num_month)
	{
	case 1:
		printf("Month Number : %d is January\n\n", mdm_num_month);
		break;

	case 2:
		printf("Month Number : %d is February\n\n", mdm_num_month);
		break;

	case 3:
		printf("Month Number : %d is March\n\n", mdm_num_month);
		break;

	case 4:
		printf("Month Number : %d is April\n\n", mdm_num_month);
		break;

	case 5:
		printf("Month Number : %d is May\n\n", mdm_num_month);
		break;

	case 6:
		printf("Month Number : %d is June\n\n", mdm_num_month);
		break;

	case 7:
		printf("Month Number : %d is July\n\n", mdm_num_month);
		break;

	case 8:
		printf("Month Number : %d is August\n\n", mdm_num_month);
		break;

	case 9:
		printf("Month Number : %d is September\n\n", mdm_num_month);
		break;

	case 10:
		printf("Month Number : %d is October\n\n", mdm_num_month);
		break;

	case 11:
		printf("Month Number : %d is November\n\n", mdm_num_month);
		break;

	case 12:
		printf("Month Number : %d is December\n\n", mdm_num_month);
		break;

	default:
		printf("You Entered Wrong Month Number !!!\n\n");
		break;
	}

	printf("\nSwitch Case Block Complete\n\n");

	return(0);
}
