// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_num_month;

	// code
	printf("\n\n");

	printf("Enter Month Number : ");
	scanf("%d", &mdm_num_month);
	printf("\n\n");

	if (mdm_num_month == 1) // like 'case 1' 
		printf("Month Number : %d is January\n\n", mdm_num_month);

	else if (mdm_num_month == 2) // like 'case 2' 
		printf("Month Number : %d is February\n\n", mdm_num_month);

	else if (mdm_num_month == 3) // like 'case 3' 
		printf("Month Number : %d is March\n\n", mdm_num_month);

	else if (mdm_num_month == 4) // like 'case 4' 
		printf("Month Number : %d is April\n\n", mdm_num_month);

	else if (mdm_num_month == 5) // like 'case 5' 
		printf("Month Number : %d is May\n\n", mdm_num_month);

	else if (mdm_num_month == 6) // like 'case 6' 
		printf("Month Number : %d is June\n\n", mdm_num_month);

	else if (mdm_num_month == 7) // like 'case 7' 
		printf("Month Number : %d is July\n\n", mdm_num_month);

	else if (mdm_num_month == 8) // like 'case 8' 
		printf("Month Number : %d is August\n\n", mdm_num_month);

	else if (mdm_num_month == 9) // like 'case 9' 
		printf("Month Number : %d is September\n\n", mdm_num_month);

	else if (mdm_num_month == 10) // like 'case 10' 
		printf("Month Number : %d is October\n\n", mdm_num_month);

	else if (mdm_num_month == 11) // like 'case 11' 
		printf("Month Number : %d is November\n\n", mdm_num_month);

	else if (mdm_num_month == 12) // like 'case 12' 
		printf("Month Number : %d is December\n\n", mdm_num_month);

	else
		printf("You Entered Wrong Month Number !!!\n\n");

	printf("\nif - else - if - else ladder ends here\n\n");

	return(0);
}
