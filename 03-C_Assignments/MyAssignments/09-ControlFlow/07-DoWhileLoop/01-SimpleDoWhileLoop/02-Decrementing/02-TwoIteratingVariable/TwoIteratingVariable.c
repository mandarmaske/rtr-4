// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j;

	// code
	printf("\n\n");

	printf("Printing Digits from 10 to 1 and 100 to 10 : \n\n");

	mdm_i = 10;
	mdm_j = 100;

	do
	{
		printf("\t%d\t%d\n", mdm_i, mdm_j);
		mdm_i--;
		mdm_j = mdm_j - 10;
	} while (mdm_i >= 1, mdm_j >= 10);

	printf("\n\n");

	return(0);
}
