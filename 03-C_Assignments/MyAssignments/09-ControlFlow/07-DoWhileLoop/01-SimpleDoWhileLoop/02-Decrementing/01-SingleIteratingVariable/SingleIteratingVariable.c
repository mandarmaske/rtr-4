// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_i;

	// code
	printf("\n\n");

	printf("Printing Digits 10 to 1 : \n\n");

	mdm_i = 10;

	do
	{
		printf("\t%d\n", mdm_i);
		mdm_i--;
	} while (mdm_i >= 1);

	printf("\n\n");

	return(0);
}
