// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	float mdm_f, mdm_f_num = 1.7f;

	// code
	printf("\n\n");

	printf("Printing Numbers from %f to %f : \n\n\n", mdm_f_num, (mdm_f_num * 10.0f));

	mdm_f = mdm_f_num;

	do
	{
		printf("\t%f\n", mdm_f);
		mdm_f = mdm_f + mdm_f_num;
	} while (mdm_f <= (mdm_f_num * 10.0f));

	printf("\n\n");

	return(0);
}
