// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declaration
	char mdm_option, mdm_ch = '\0';

	// code
	printf("\n\n");
	printf("Once the Infinite Loop Begins, Enter 'Q' or 'q' to quit the Infinite Loop : \n\n\n");

	do
	{
		do
		{
			printf("In Loop...\n");
			mdm_ch = getch();
		} while (mdm_ch != 'Q' && mdm_ch != 'q');

		printf("\n\n");
		printf("Exitting User Controlled Infinte Loop...\n");
		printf("\n\n");
		printf("Do you bwant to Begin User Controlled Infinite Loop Again ?...(Y / y - Yes, Any Other Key - No) : \n\n\n");
		mdm_option = getch();

	} while (mdm_option == 'Y' || mdm_option == 'y');

	printf("Program Ends Here...\n\n\n");

	return(0);
}
