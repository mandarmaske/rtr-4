// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declaration
	char mdm_option, mdm_ch = '\0';

	// code
	printf("\n\n");
	printf("Once Infinite Loop Begins, Enter 'Q' or 'q' to quit the Infinite Loop : \n\n");
	printf("Enter 'Y' or 'y' to initiate User Controlled Infinite Loop : \n\n\n");
	mdm_option = getch();

	if (mdm_option == 'Y' || mdm_option == 'y')
	{
		do
		{
			printf("Infinite Loop...\n");
			mdm_ch = getch(); // control flow awaits for character Input
			if (mdm_ch == 'Q' || mdm_ch == 'q')
				break; // will leave the infinite loop

		} while (1); // Infinite Loop - Always true

		printf("\n\n");
		printf("Exitting User Controlled Infinite Loop...");
		printf("\n\n");
	}
	else
		printf("You Must Press 'Y' or 'y' to initiate The User Controlled Infinite Loop...\n\n");

	return(0);
}
