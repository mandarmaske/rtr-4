// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i_num, mdm_num, mdm_i;

	// code
	printf("\n\n");

	printf("Enter an Integer Value From which Iteration must Begin : ");
	scanf("%d", &mdm_i_num);

	printf("How many digits do you want to print %d onwards : ", mdm_i_num);
	scanf("%d", &mdm_num);

	printf("\n\n");
	mdm_i = mdm_i_num;

	do
	{
		printf("\t%d\n", mdm_i);
		mdm_i++;
	} while (mdm_i <= (mdm_i_num + mdm_num));

	printf("\n\n");

	return(0);
}
