// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j, mdm_c;

	// code
	printf("\n\n");

	mdm_i = 0;

	do 
	{
		mdm_j = 0;

		do
		{
			mdm_c = ((mdm_i & 0x8) == 0) ^ ((mdm_j & 0x8) == 0);

			if (mdm_c == 0)
				printf("  ");

			if (mdm_c == 1)
				printf("* ");

			mdm_j++;
		} while (mdm_j < 64);

		mdm_i++;
		printf("\n\n");
	} while (mdm_i < 64);

	return(0);
}
