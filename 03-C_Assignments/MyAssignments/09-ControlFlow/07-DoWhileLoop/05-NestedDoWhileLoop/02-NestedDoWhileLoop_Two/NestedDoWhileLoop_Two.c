// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_count, mdm_i, mdm_j, mdm_k;

	// code
	printf("\n\n");

	mdm_i = 1;
	mdm_count = 1;

	do
	{
		mdm_j = 1;

		do
		{
			mdm_k = 1;

			do
			{
				printf("\tCount = %d\tI = %d\tJ = %d\tK = %d\n", mdm_count, mdm_i, mdm_j, mdm_k);
				mdm_k++;
				mdm_count++;
			} while (mdm_k <= 3);

			mdm_j++;
		} while (mdm_j <= 5);

		mdm_i++;
		printf("\n\n");
	} while (mdm_i <= 10);

	return(0);
}
