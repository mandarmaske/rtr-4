// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_i_num, mdm_num, mdm_i;

	// code
	printf("\n\n");

	printf("Enter an integer value from which Iteration Must Begin : ");
	scanf("%d", &mdm_i_num);

	printf("How many integers do you want to print from %d onwards ? : ", mdm_i_num);
	scanf("%d", &mdm_num);

	printf("\n\nPrinting Digits %d to %d : \n\n", mdm_i_num, (mdm_i_num + mdm_num));

	mdm_i = mdm_i_num;

	while (mdm_i <= (mdm_i_num + mdm_num))
	{
		printf("\t%d\n", mdm_i);
		mdm_i++;
	}

	printf("\n\n");

	return(0);
}
