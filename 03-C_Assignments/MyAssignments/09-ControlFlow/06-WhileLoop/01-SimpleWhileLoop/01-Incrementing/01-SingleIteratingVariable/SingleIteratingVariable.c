// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_i;

	// code
	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n\n");

	mdm_i = 1;

	while (mdm_i <= 10)
	{
		printf("\t%d\n", mdm_i);
		mdm_i++;
	}

	printf("\n\n");

	return(0);
}
