// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_i, mdm_j;

	// code
	printf("\n\n");

	printf("Printing Digits 10 to 1 and 100 to 10 : \n\n\n");
	
	mdm_i = 10;
	mdm_j = 100;

	while ((mdm_i >= 1), (mdm_j >= 10))
	{
		printf("\t%d  %d\n", mdm_i, mdm_j);
		mdm_i--;
		mdm_j = mdm_j - 10;
	}

	printf("\n\n");

	return(0);
}
