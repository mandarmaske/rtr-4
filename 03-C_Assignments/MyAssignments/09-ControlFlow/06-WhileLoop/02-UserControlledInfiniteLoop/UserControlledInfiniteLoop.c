// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declarations
	char mdm_option, mdm_ch = '\0';

	// code
	printf("\n\n");

	printf("Once infinite loop begins, Enter 'Q' or 'q' to quit the Infinite Loop : \n\n");
	printf("Enter 'Y' or 'y' to initiate User Controlled Infinite Loop : \n\n");
	printf("Enter Option : ");
	mdm_option = getch();
	printf("\n\n");

	if (mdm_option == 'Y' || mdm_option == 'y')
	{
		while (1) // Infinite Loop
		{
			printf("Infinite Loop...\n");
			mdm_ch = getch(); 
			if (mdm_ch == 'Q' || mdm_ch == 'q')
				break; // User Controlled Exitting From Infinite Loop
		}

		printf("\n\n");
		printf("Exitting User Controlled Infinite Loop...");
		printf("\n\n");
	}
	else
		printf("You Must Press 'Y' or 'y' to initiate the User Controlled Infinite Loop...Please Try Again...\n\n");

	printf("Infinite Loop Ends Here...\n\n");

	return(0);
}
