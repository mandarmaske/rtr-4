// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	float mdm_f;
	float mdm_f_num = 1.7f; // Simply change this value ONLY to get different Output

	// code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n\n", mdm_f_num, (mdm_f_num * 10.0f));

	mdm_f = mdm_f_num;

	while (mdm_f <= (mdm_f_num * 10.0f))
	{
		printf("\t%f\n", mdm_f);
		mdm_f = mdm_f + mdm_f_num;
	}

	printf("\n\n");

	return(0);
}
