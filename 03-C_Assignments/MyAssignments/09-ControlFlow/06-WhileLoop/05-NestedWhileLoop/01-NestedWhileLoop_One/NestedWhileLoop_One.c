// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j, mdm_count;

	// code
	printf("\n\n");

	mdm_i = 1;
	mdm_count = 1;

	while (mdm_i <= 10)
	{
		mdm_j = 1;

		while (mdm_j <= 5)
		{
			printf("\tCount = %d\tI = %d\tJ = %d\n", mdm_count, mdm_i, mdm_j);
			mdm_j++;
			mdm_count++;
		}
		mdm_i++;
		printf("\n\n");
	}

	return(0);
}
