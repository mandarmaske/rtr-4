// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j, mdm_c;

	// code
	printf("\n\n");

	mdm_i = 0;

	while (mdm_i < 64)
	{
		mdm_j = 0;

		while (mdm_j < 64)
		{
			mdm_c = ((mdm_i & 0x8) == 0) ^ ((mdm_j & 0x8) == 0);

			if (mdm_c == 0)
				printf("  ");

			if (mdm_c == 1)
				printf("* ");

			mdm_j++;
		}

		printf("\n\n");
		mdm_i++;
	}

	return(0);
}
