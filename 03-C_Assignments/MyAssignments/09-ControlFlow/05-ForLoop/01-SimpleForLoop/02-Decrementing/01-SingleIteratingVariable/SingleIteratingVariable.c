// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_i;

	// code
	printf("\n\n");

	printf("Printing Digits 10 to 1 : \n\n");

	for (mdm_i = 10; mdm_i >= 1; mdm_i--)
	{
		printf("\t%d\n", mdm_i);
	}

	printf("\n\n");

	return(0);
}
