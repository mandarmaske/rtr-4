// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j;

	// code
	printf("\n\n");

	printf("Printing Digits 1 to 10 and 10 to 100 : \n\n");

	for (mdm_i = 1, mdm_j = 10; mdm_i <= 10, mdm_j <= 100; mdm_i++, mdm_j = mdm_j + 10)
	{
		printf("\t%d \t%d\n", mdm_i, mdm_j);
	}

	printf("\n\n");

	return(0);
}
