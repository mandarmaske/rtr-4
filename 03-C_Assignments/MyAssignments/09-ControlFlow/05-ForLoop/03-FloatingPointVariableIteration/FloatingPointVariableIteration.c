// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	float mdm_f;
	float mdm_f_num = 1.7f; // simply change this value ONLY to get differen output
	
	// code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n\n", mdm_f_num, (mdm_f_num * 10.0f));

	for (mdm_f = mdm_f_num; mdm_f <= (mdm_f_num * 10.0f); mdm_f = mdm_f + mdm_f_num)
	{
		printf("\t%f\n", mdm_f);
	}

	printf("\n");

	return(0);
}
