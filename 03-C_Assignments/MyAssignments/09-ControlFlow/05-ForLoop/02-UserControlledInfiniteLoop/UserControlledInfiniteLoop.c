// header files
#include<stdio.h>
#include<conio.h>

// entry-point function
int main(void)
{
	// variable declarations
	char mdm_option, mdm_ch = '\0'; // \0 is null termination character

	// code
	printf("\n\n");

	printf("Once the infinite loop begins, Enter 'Q' or 'q' to quit the Infinite For Loop : \n\n");
	printf("Enter 'Y' or 'y' to initiate User Controlled Infinite Loop : ");
	mdm_option = getch();
	printf("\n\n");

	if (mdm_option == 'Y' || mdm_option == 'y')
	{
		for (;;) // Infinite Loop
		{
			printf("In Loop...\n");
			mdm_ch = getch();
			if (mdm_ch == 'Q' || mdm_ch == 'q')
				break; // User Controlled Infinite Loop
		}
	}

	printf("\n\n");
	printf("Exiting the User Controlled Infinite Loop...\n\n");

	return(0);
}
