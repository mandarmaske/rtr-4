// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i_num, mdm_num, mdm_i;

	// code
	printf("\n\n");

	printf("Enter An integer value from which iteration must Begin : ");
	scanf("%d", &mdm_i_num);
	printf("How many Digits do you want to print from %d onwards : ", mdm_i_num);
	scanf("%d", &mdm_num);
	printf("\n\n");

	printf("Printing Digits %d to %d : \n\n\n", mdm_i_num, (mdm_i_num + mdm_num));

	for (mdm_i = mdm_i_num; mdm_i <= (mdm_i_num + mdm_num); mdm_i++)
	{
		printf("\t%d\n", mdm_i);
	}

	printf("\n");

	return(0);
}
