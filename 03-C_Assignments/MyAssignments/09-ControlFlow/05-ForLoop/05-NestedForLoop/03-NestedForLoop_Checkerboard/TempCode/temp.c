// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j, mdm_c;

	// code
	printf("\n\n");

	for (mdm_i = 0; mdm_i < 8; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < 8; mdm_j++)
		{
			mdm_c = ((mdm_i & 0x1) == 0) ^ ((mdm_j & 0x4) == 0);

			if (mdm_c == 0)
				printf("  ");

			if (mdm_c == 1)
				printf("* ");
		}

		printf("\n\n");
	}

	return(0);
}
