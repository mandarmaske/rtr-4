// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j, mdm_k, mdm_count;

	// code
	printf("\n\n");

	mdm_count = 1;

	for (mdm_i = 1; mdm_i <= 10; mdm_i++)
	{
		for (mdm_j = 1; mdm_j <= 5; mdm_j++)
		{
			for (mdm_k = 1; mdm_k <= 3; mdm_k++)
			{
				printf("Count = %d\tI = %d,\tJ = %d\tand\tK = %d\n", mdm_count, mdm_i, mdm_j, mdm_k);
				mdm_count++;
			}
		}
		printf("\n\n");
	}

	printf("\nNested For Loop - Two Ends Here...");
	printf("\n\n");

	return(0);
}
