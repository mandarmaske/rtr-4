// header file
#include<stdio.h>

// entry-point function
int main(void)
{
    // variable declaration
	int mdm_num;

	// code
	printf("\n\n");

	printf("Enter a value\nNUM = ");
	scanf("%d", &mdm_num);
	printf("\n\n");

	// if-else-if ladder begins here...
	if (mdm_num < 0)
		printf("NUM = %d is less than 0, it's a negative number\n\n", mdm_num);

	else if ((mdm_num >= 0) && (mdm_num <= 100))
		printf("NUM = %d is in between 0 to 100\n\n", mdm_num);

	else if ((mdm_num > 100) && (mdm_num <= 200))
		printf("NUM = %d is in between 101 to 200\n\n", mdm_num);

	else if ((mdm_num > 200) && (mdm_num <= 300))
		printf("NUM = %d is in between 201 to 300\n\n", mdm_num);

	else if ((mdm_num > 300) && (mdm_num <= 400))
		printf("NUM = %d is in between 301 to 400\n\n", mdm_num);

	else if ((mdm_num > 400) && (mdm_num <= 500))
		printf("NUM = %d is in between 400 and 500\n\n", mdm_num);

	else if (mdm_num > 500)
		printf("NUM = %d is greater than 500\n\n", mdm_num);

	else // this is the terminating 'else' of this 'if-else-if-else' ladder
		printf("Invalid Value Entered\n\n");

	return(0);
}