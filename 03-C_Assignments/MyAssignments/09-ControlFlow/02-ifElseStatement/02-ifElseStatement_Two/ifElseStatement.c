// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_age;

	// code
	printf("\n\n");
	printf("Enter your Age = ");
	scanf("%d", &mdm_age);
	printf("\n\n");
	if (mdm_age >= 18)
	{
		printf("Entering if-block...\n\n");
		printf("You are eligible for voting");
	}
	else
	{
		printf("Entering else-block...\n\n");
		printf("You are not eligible for voting\n\n");
	}
	printf("if-else pair done\n\n");

	return(0);
}
