// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_num;

	// code
	printf("\n\n");

	printf("Enter Value\nNUM = ");
	scanf("%d", &mdm_num);
	printf("\n\n");

	if (mdm_num < 0) // if-01
	{
		printf("NUM = %d is less than 0 means it's a negative number\n\n", mdm_num);
	}
	else // else-01
	{
		if ((mdm_num > 0) && (mdm_num <= 100)) // if-02
		{
			printf("NUM = %d is greater than 0 and it is less than or equal to 100\n\n", mdm_num);
		}
		else // else - 02
		{
			if ((mdm_num > 100) && (mdm_num <= 200)) // if-03
			{
				printf("NUM = %d is greater than 100 and it is less than or equal to 200\n\n", mdm_num);
			}
			else // else-03
			{
				if ((mdm_num > 200) && (mdm_num <= 300)) // if-04
				{
					printf("NUM = %d is greater than 200 and it is less than or equal to 300\n\n", mdm_num);
				}
				else // else-04
				{
					if ((mdm_num > 300) && (mdm_num <= 400)) // if-05
					{
						printf("NUM = %d is greater than 300 and it is less than or equal to 400\n\n", mdm_num);
					}
					else // else-05
					{
						if ((mdm_num > 400) && (mdm_num <= 500)) // if-06
						{
							printf("NUM = %d is greater than 400 and it is less than or equal to 500\n\n", mdm_num);
						}
						else // else-06
						{
							printf("NUM = %d is greater than 500\n\n", mdm_num);
						} // closing curly brace of else-06
					} // closing curly brace of else-05
				} // closing curly brace of else-04
			} // closing curly brace of else-03
		} // closing curly brace of else-02
	} // closing curly brace of else-01

	return(0);
}