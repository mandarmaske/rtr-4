// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declaration
	int mdm_a, mdm_b, mdm_p;

	// code
	mdm_a = 9;
	mdm_b = 30;
	mdm_p = 30;

	// *** FIRST if-else PAIR ***
	printf("\n\n");
	if (mdm_a < mdm_b)
	{
		printf("Entering First if-block...\n\n");
		printf("A is less than B !!!\n\n");
	}
	else
	{
		printf("Entering First else-block...\n\n");
		printf("A is not less than B");
	}
	printf("FIRST if-else pair is done\n\n");

	// *** SECOND if-else pair ***
	printf("\n\n");
	if (mdm_b != mdm_p)
	{
		printf("Entering SECOND if-block...\n\n");
		printf("B is not equal to P\n\n");
	}
	else
	{
		printf("Entering SECOND else-block...\n\n");
		printf("B is equal to P\n\n");
	}
	printf("SECOND if-else pair is done\n\n");

	return(0);
}
