// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations

	// Inline Initialization
	int mdm_iArray[5][3][2] = { { {9, 18}, {27, 36}, {45, 54} },
								{ {8, 16}, {24, 32}, {40, 48} },
								{ {7, 14}, {21, 28}, {35, 42} },
								{ {6, 12}, {18, 24}, {30, 36} },
								{ {5, 10}, {15, 20}, {25, 30} } };

	int mdm_int_size;
	int mdm_iArray_size;
	int mdm_iArray_num_elements, mdm_iArray_width, mdm_iArray_height, mdm_iArray_depth;


	// code
	printf("\n\n");

	mdm_int_size = sizeof(int);

	mdm_iArray_size = sizeof(mdm_iArray);
	printf("Size Of Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_size);

	mdm_iArray_width = mdm_iArray_size / sizeof(mdm_iArray[0]);
	printf("Number Of Rows (Width) In Three Dimensional (3D) Integer Array Is : %d\n\n", mdm_iArray_width);

	mdm_iArray_height = sizeof(mdm_iArray[0]) / sizeof(mdm_iArray[0][0]);
	printf("Number Of Columns (Height) In Three Dimensional (3D) Integer Array Is : %d\n\n", mdm_iArray_height);

	mdm_iArray_depth = sizeof(mdm_iArray[0][0]) / mdm_int_size;
	printf("Depth In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_depth);

	mdm_iArray_num_elements = mdm_iArray_width * mdm_iArray_height * mdm_iArray_depth;
	printf("Number Of Elements In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_num_elements);

	printf("\n\n");
	printf("Elements In Integer 3D Array : \n\n");

	// **** Piece-Meal Display

	// **** Row 1 ****
	printf("**** Row 1 ****\n\n");
	printf("**** Column 1 ****\n");
	printf("mdm_iArray[0][0][0] = %d\n", mdm_iArray[0][0][0]);
	printf("mdm_iArray[0][0][1] = %d\n\n", mdm_iArray[0][0][1]);

	printf("**** Column 2 ****\n");
	printf("mdm_iArray[0][1][0] = %d\n", mdm_iArray[0][1][0]);
	printf("mdm_iArray[0][1][1] = %d\n\n", mdm_iArray[0][1][1]);

	printf("**** Column 3 ****\n");
	printf("mdm_iArray[0][2][0] = %d\n", mdm_iArray[0][2][0]);
	printf("mdm_iArray[0][2][1] = %d\n", mdm_iArray[0][2][1]);
	printf("\n\n");

	printf("**** Row 2 ****\n\n");
	printf("**** Column 1 ****\n");
	printf("mdm_iArray[1][0][0] = %d\n", mdm_iArray[1][0][0]);
	printf("mdm_iArray[1][0][1] = %d\n\n", mdm_iArray[1][0][1]);

	printf("**** Column 2 ****\n");
	printf("mdm_iArray[1][1][0] = %d\n", mdm_iArray[1][1][0]);
	printf("mdm_iArray[1][1][1] = %d\n\n", mdm_iArray[1][1][1]);

	printf("**** Column 3 ****\n");
	printf("mdm_iArray[1][2][0] = %d\n", mdm_iArray[1][2][0]);
	printf("mdm_iArray[1][2][1] = %d\n", mdm_iArray[1][2][1]);
	printf("\n\n");

	printf("**** Row 3 ****\n\n");
	printf("**** Column 1 ****\n");
	printf("mdm_iArray[2][0][0] = %d\n", mdm_iArray[2][0][0]);
	printf("mdm_iArray[2][0][1] = %d\n\n", mdm_iArray[2][0][1]);

	printf("**** Column 2 ****\n");
	printf("mdm_iArray[2][1][0] = %d\n", mdm_iArray[2][1][0]);
	printf("mdm_iArray[2][1][1] = %d\n\n", mdm_iArray[2][1][1]);

	printf("**** Column 3 ****\n");
	printf("mdm_iArray[2][2][0] = %d\n", mdm_iArray[2][2][0]);
	printf("mdm_iArray[2][2][1] = %d\n", mdm_iArray[2][2][1]);
	printf("\n\n");

	printf("**** Row 4 ****\n\n");
	printf("**** Column 1 ****\n");
	printf("mdm_iArray[3][0][0] = %d\n", mdm_iArray[3][0][0]);
	printf("mdm_iArray[3][0][1] = %d\n\n", mdm_iArray[3][0][1]);

	printf("**** Column 2 ****\n");
	printf("mdm_iArray[3][1][0] = %d\n", mdm_iArray[3][1][0]);
	printf("mdm_iArray[3][1][1] = %d\n\n", mdm_iArray[3][1][1]);

	printf("**** Column 3 ****\n");
	printf("mdm_iArray[3][2][0] = %d\n", mdm_iArray[3][2][0]);
	printf("mdm_iArray[3][2][1] = %d\n", mdm_iArray[3][2][1]);
	printf("\n\n");

	printf("**** Row 5 ****\n\n");
	printf("**** Column 1 ****\n");
	printf("mdm_iArray[4][0][0] = %d\n", mdm_iArray[4][0][0]);
	printf("mdm_iArray[4][0][1] = %d\n\n", mdm_iArray[4][0][1]);

	printf("**** Column 2 ****\n");
	printf("mdm_iArray[4][1][0] = %d\n", mdm_iArray[4][1][0]);
	printf("mdm_iArray[4][1][1] = %d\n\n", mdm_iArray[4][1][1]);

	printf("**** Column 3 ****\n");
	printf("mdm_iArray[4][2][0] = %d\n", mdm_iArray[4][2][0]);
	printf("mdm_iArray[4][2][1] = %d\n", mdm_iArray[4][2][1]);
	printf("\n\n");

	return(0);
}

