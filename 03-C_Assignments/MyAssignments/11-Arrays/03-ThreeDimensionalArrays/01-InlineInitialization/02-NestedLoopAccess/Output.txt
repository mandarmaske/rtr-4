C:\MyProjects\RTR_4_2021-116\03-C_Assignments\MyAssignments\11-Arrays\03-ThreeDimensionalArrays\01-InlineInitialization\02-NestedLoopAccess>cl.exe /c /EHsc ThreeDimensionalIntegerArray.c
Microsoft (R) C/C++ Optimizing Compiler Version 19.29.30136 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

ThreeDimensionalIntegerArray.c

C:\MyProjects\RTR_4_2021-116\03-C_Assignments\MyAssignments\11-Arrays\03-ThreeDimensionalArrays\01-InlineInitialization\02-NestedLoopAccess>link.exe ThreeDimensionalIntegerArray.obj
Microsoft (R) Incremental Linker Version 14.29.30136.0
Copyright (C) Microsoft Corporation.  All rights reserved.


C:\MyProjects\RTR_4_2021-116\03-C_Assignments\MyAssignments\11-Arrays\03-ThreeDimensionalArrays\01-InlineInitialization\02-NestedLoopAccess>ThreeDimensionalIntegerArray.exe


Size Of Three Dimensional (3D) Integer Array Is = 120

Number Of Rows (Width) In Three Dimensional (3D) Integer Array Is = 5

Number Of Columns (Height) In Three Dimensional (3D) Integer Array Is = 3

Depth Of Three Dimensional (3D) Integer Array Is = 2

The Total Number Of Elements In Three Dimensional (3D) Integer Array Is = 30



**** Row 1 ****

**** Column 1 ****
mdm_iArray[0][0][0] = 9
mdm_iArray[0][0][1] = 18

**** Column 2 ****
mdm_iArray[0][1][0] = 27
mdm_iArray[0][1][1] = 36

**** Column 3 ****
mdm_iArray[0][2][0] = 45
mdm_iArray[0][2][1] = 54

**** Row 2 ****

**** Column 1 ****
mdm_iArray[1][0][0] = 8
mdm_iArray[1][0][1] = 16

**** Column 2 ****
mdm_iArray[1][1][0] = 24
mdm_iArray[1][1][1] = 32

**** Column 3 ****
mdm_iArray[1][2][0] = 40
mdm_iArray[1][2][1] = 48

**** Row 3 ****

**** Column 1 ****
mdm_iArray[2][0][0] = 7
mdm_iArray[2][0][1] = 14

**** Column 2 ****
mdm_iArray[2][1][0] = 21
mdm_iArray[2][1][1] = 28

**** Column 3 ****
mdm_iArray[2][2][0] = 35
mdm_iArray[2][2][1] = 42

**** Row 4 ****

**** Column 1 ****
mdm_iArray[3][0][0] = 6
mdm_iArray[3][0][1] = 12

**** Column 2 ****
mdm_iArray[3][1][0] = 18
mdm_iArray[3][1][1] = 24

**** Column 3 ****
mdm_iArray[3][2][0] = 30
mdm_iArray[3][2][1] = 36

**** Row 5 ****

**** Column 1 ****
mdm_iArray[4][0][0] = 5
mdm_iArray[4][0][1] = 10

**** Column 2 ****
mdm_iArray[4][1][0] = 15
mdm_iArray[4][1][1] = 20

**** Column 3 ****
mdm_iArray[4][2][0] = 25
mdm_iArray[4][2][1] = 30


C:\MyProjects\RTR_4_2021-116\03-C_Assignments\MyAssignments\11-Arrays\03-ThreeDimensionalArrays\01-InlineInitialization\02-NestedLoopAccess>