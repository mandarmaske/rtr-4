// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration

	// In-Line Initialization
	int mdm_iArray[5][3][2] = { { {9, 18}, {27, 36}, {45, 54} },
								{ {8, 16}, {24, 32}, {40, 48} },
								{ {7, 14}, {21, 28}, {35, 42} },
								{ {6, 12}, {18, 24}, {30, 36} },
								{ {5, 10}, {15, 20}, {25, 30} } };

	int mdm_int_size;
	int mdm_iArray_size;
	int mdm_iArray_num_elements, mdm_iArray_width, mdm_iArray_height, mdm_iArray_depth;
	int mdm_i, mdm_j, mdm_k;

	// code
	printf("\n\n");

	mdm_int_size = sizeof(int);

	mdm_iArray_size = sizeof(mdm_iArray);
	printf("Size Of Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_size);

	mdm_iArray_width = mdm_iArray_size / sizeof(mdm_iArray[0]);
	printf("Number Of Rows (Width) In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_width);

	mdm_iArray_height = sizeof(mdm_iArray[0]) / sizeof(mdm_iArray[0][0]);
	printf("Number Of Columns (Height) In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_height);

	mdm_iArray_depth = sizeof(mdm_iArray[0][0]) / mdm_int_size;
	printf("Depth Of Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_depth);

	mdm_iArray_num_elements = mdm_iArray_width * mdm_iArray_height * mdm_iArray_depth;
	printf("The Total Number Of Elements In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_num_elements);

	printf("\n\n");
	for (mdm_i = 0; mdm_i < mdm_iArray_width; mdm_i++)
	{
		printf("**** Row %d ****\n\n", (mdm_i + 1));
		for (mdm_j = 0; mdm_j < mdm_iArray_height; mdm_j++)
		{
			printf("**** Column %d ****\n", (mdm_j + 1));
			for (mdm_k = 0; mdm_k < mdm_iArray_depth; mdm_k++)
			{
				printf("mdm_iArray[%d][%d][%d] = %d\n", mdm_i, mdm_j, mdm_k, mdm_iArray[mdm_i][mdm_j][mdm_k]);
			}
			printf("\n");
		}
	}

	return(0);
}

