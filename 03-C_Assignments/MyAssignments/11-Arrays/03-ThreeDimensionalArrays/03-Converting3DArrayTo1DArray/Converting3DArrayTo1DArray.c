// header file
#include<stdio.h>

// constant value definition
#define MDM_NUM_ROWS 5
#define MDM_NUM_COLUMNS 3
#define MDM_NUM_DEPTHS 2

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations

	// In-Line Initialization
	int mdm_iArray[MDM_NUM_ROWS][MDM_NUM_COLUMNS][MDM_NUM_DEPTHS] = { { {9, 18}, {27, 36}, {45, 54} },
																	  { {8, 16}, {24, 32}, {40, 48} },
																	  { {7, 14}, {21, 28}, {35, 42} },
																	  { {6, 12}, {18, 24}, {30, 36} },
																	  { {5, 10}, {15, 20}, {25, 30} } };
	int mdm_i, mdm_j, mdm_k;
	int mdm_iArray_1D[MDM_NUM_ROWS * MDM_NUM_COLUMNS * MDM_NUM_DEPTHS]; // 5 * 3 * 2 Elements = 30 Elements In 1D Array

	// code

	// **** Display 3D Array ****
	printf("\n\n");
	printf("Elements In The 3D Array : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		printf("**** Row %d ****\n\n", (mdm_i + 1));
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("**** Column %d ****\n", (mdm_j + 1));
			for (mdm_k = 0; mdm_k < MDM_NUM_DEPTHS; mdm_k++)
			{
				printf("mdm_iArray[%d][%d][%d] = %d\n", mdm_i, mdm_j, mdm_k, mdm_iArray[mdm_i][mdm_j][mdm_k]);
			}

			printf("\n\n");
		}
	}

	// **** Converting 3D To 1D ****
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			for (mdm_k = 0; mdm_k < MDM_NUM_DEPTHS; mdm_k++)
			{
				mdm_iArray_1D[(mdm_i * MDM_NUM_COLUMNS * MDM_NUM_DEPTHS) + (mdm_j * MDM_NUM_DEPTHS) + mdm_k] = mdm_iArray[mdm_i][mdm_j][mdm_k];
			}
		}
	}

	// **** Display 1D Array ****
	printf("\n\n");
	printf("Elements In The 1D Integer Array : \n\n");
	for (mdm_i = 0; mdm_i < (MDM_NUM_ROWS * MDM_NUM_COLUMNS * MDM_NUM_DEPTHS); mdm_i++)
	{
		printf("mdm_iArray_1D[%d] = %d\n\n", mdm_i, mdm_iArray_1D[mdm_i]);
	}

	return(0);
}

