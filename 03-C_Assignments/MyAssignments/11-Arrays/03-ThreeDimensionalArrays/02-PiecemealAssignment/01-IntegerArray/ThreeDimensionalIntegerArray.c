// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration

	int mdm_iArray[5][3][2];
	int mdm_int_size;
	int mdm_iArray_size;
	int mdm_iArray_num_elements, mdm_iArray_width, mdm_iArray_height, mdm_iArray_depth;
	int mdm_i, mdm_j, mdm_k;

	// code
	printf("\n\n");

	mdm_int_size = sizeof(int);

	mdm_iArray_size = sizeof(mdm_iArray);
	printf("Size Of Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_size);

	mdm_iArray_width = mdm_iArray_size / sizeof(mdm_iArray[0]);
	printf("Number Of Rows (Width) In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_width);

	mdm_iArray_height = sizeof(mdm_iArray[0]) / sizeof(mdm_iArray[0][0]);
	printf("Number Of Columns (Height) In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_height);

	mdm_iArray_depth = sizeof(mdm_iArray[0][0]) / mdm_int_size;
	printf("Depth Of Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_depth);

	mdm_iArray_num_elements = mdm_iArray_width * mdm_iArray_height * mdm_iArray_depth;
	printf("The Total Number Of Elements In Three Dimensional (3D) Integer Array Is = %d\n\n", mdm_iArray_num_elements);


	// **** Piece-Meal Assignment ****

	// **** Row 1 ****

	// **** Column 1 ****
	mdm_iArray[0][0][0] = 8;
	mdm_iArray[0][0][1] = 16;

	// **** Column 2 ****
	mdm_iArray[0][1][0] = 24;
	mdm_iArray[0][1][1] = 32;

	// **** Column 3 ****
	mdm_iArray[0][2][0] = 40;
	mdm_iArray[0][2][1] = 48;

	// **** Row 2 ****

	// **** Column 1 ****
	mdm_iArray[1][0][0] = 7;
	mdm_iArray[1][0][1] = 14;

	// **** Column 2 ****
	mdm_iArray[1][1][0] = 21;
	mdm_iArray[1][1][1] = 28;

	// **** Column 3 ****
	mdm_iArray[1][2][0] = 35;
	mdm_iArray[1][2][1] = 42;

	// **** Row 3 ****

	// **** Column 1 ****
	mdm_iArray[2][0][0] = 6;
	mdm_iArray[2][0][1] = 12;

	// **** Column 2 ****
	mdm_iArray[2][1][0] = 18;
	mdm_iArray[2][1][1] = 24;

	// **** Column 3 ****
	mdm_iArray[2][2][0] = 30;
	mdm_iArray[2][2][1] = 36;

	// **** Row 4 ****

	// **** Column 1 ****
	mdm_iArray[3][0][0] = 5;
	mdm_iArray[3][0][1] = 10;

	// **** Column 2 ****
	mdm_iArray[3][1][0] = 15;
	mdm_iArray[3][1][1] = 20;

	// **** Column 3 ****
	mdm_iArray[3][2][0] = 25;
	mdm_iArray[3][2][1] = 30;

	// **** Row 5 ****

	// **** Column 1 ****
	mdm_iArray[4][0][0] = 4;
	mdm_iArray[4][0][1] = 8;

	// **** Column 2 ****
	mdm_iArray[4][1][0] = 12;
	mdm_iArray[4][1][1] = 16;

	// **** Column 3 ****
	mdm_iArray[4][2][0] = 20;
	mdm_iArray[4][2][1] = 24;

	// 3D Integer Array Display Output

	for (mdm_i = 0; mdm_i < mdm_iArray_width; mdm_i++)
	{
		printf("**** Row %d ****\n\n", (mdm_i + 1));
		for (mdm_j = 0; mdm_j < mdm_iArray_height; mdm_j++)
		{
			printf("**** Column %d ****\n", (mdm_j + 1));
			for (mdm_k = 0; mdm_k < mdm_iArray_depth; mdm_k++)
			{
				printf("mdm_iArray[%d][%d][%d] = %d\n", mdm_i, mdm_j, mdm_k, mdm_iArray[mdm_i][mdm_j][mdm_k]);
			}
			printf("\n");
		}

		printf("\n");
	}

	return(0);
}