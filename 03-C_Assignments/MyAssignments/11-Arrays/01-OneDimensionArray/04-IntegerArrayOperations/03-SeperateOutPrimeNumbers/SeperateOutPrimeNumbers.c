// header file
#include<stdio.h>

#define MDM_NUM_ELEMENTS 10

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// variable declarations
	int mdm_iArray[10];
	int mdm_i, mdm_num, mdm_j, mdm_count = 0;

	// code
	printf("\n\n");

	// ******** Array Elements Input ********
	printf("Enter Integer Elements For Array : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		scanf("%d", &mdm_num);

		// if 'num' is negative ( < 0 ), then convert it to positive by -1
		if (mdm_num < 0)
			mdm_num = -1 * mdm_num;

		mdm_iArray[mdm_i] = mdm_num;
	}

	// ******** Printing An Entire Array ********
	printf("\n\n");
	printf("Array Elements Are : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		printf("%d\n", mdm_iArray[mdm_i]);
	}
	
	// ******** Seperating Out Even Numbers From Array Elements ********
	printf("\n\n");
	printf("Prime Numbers Amongst The Array Elements Are : \n\n");

	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		for (mdm_j = 1; mdm_j <= mdm_iArray[mdm_i]; mdm_j++)
		{
			if ((mdm_iArray[mdm_i] % mdm_j) == 0)
				mdm_count++;
		}
			// NUMBER 1 Is Neither A Prime Number Nor Consonant
			// IF A Number Is Prime, It Is ONLY Divisible By 1 And Itself.
			// Hence, If A Number Is Prime, The Value Of 'mdm_count' Will Be Exactly 2.
			// If The Value Of 'mdm_count' Is Greater Than 2, The Number Is Divisible By Numbers Other Than 1 And Itself And Hence, It Is NOT PRIME
			// The Value Of 'mdm_count' Will Be 1 Only If mdm_iArray[mdm_i] Is 1

			if (mdm_count == 2)
				printf("%d\n", mdm_iArray[mdm_i]);

			mdm_count = 0; // RESET 'mdm_count' to 0 For Checking The Next Number...
	}

	return(0);
}
