// header file
#include<stdio.h>

#define MDM_NUM_ELEMENTS 10

// entry-point function
int main(int argc, char *argv, char *envp[])
{
	// variable declarations
	int mdm_iArray[MDM_NUM_ELEMENTS];
	int mdm_i, mdm_sum = 0;

	// code
	printf("\n\n");

	// ******** Array Elements Input ********
	printf("Enter Integer Elements For Array : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		scanf("%d", &mdm_iArray[mdm_i]);
	}

	// ******** Seperating Out Even Numbers From Array Elements ********
	printf("\n\n");
	printf("Even Numbers Amongst The Array Elements Are : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		if ((mdm_iArray[mdm_i] % 2) == 0)
			printf("%d\n", mdm_iArray[mdm_i]);
	}

	// ******** Seperating Out Odd Numbers From Array Elements ********
	printf("\n\n");
	printf("Odd Numbers Amongst The Array Elements Are : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		if ((mdm_iArray[mdm_i] % 2) != 0)
			printf("%d\n", mdm_iArray[mdm_i]);
	}

	return(0);
}
