// header file
#include<stdio.h>

#define MDM_NUM_ELEMENTS 10

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// variable declarations
	int mdm_iArray[MDM_NUM_ELEMENTS];
	int mdm_i, mdm_sum = 0;

	// code
	printf("\n\n");
	printf("Enter Integer Elements For Array : \n\n");

	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		scanf("%d", &mdm_iArray[mdm_i]);
	}

	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS; mdm_i++)
	{
		mdm_sum = mdm_sum + mdm_iArray[mdm_i];
	}

	printf("\n\n");
	printf("Sum Of All Elements Of Array = %d\n\n", mdm_sum);

	return(0);
}
