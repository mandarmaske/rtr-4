// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	void mdm_MyStrcat(char[], char[]);

	// variable declarations
	char mdm_chArray_One[MDM_MAX_STRING_LENGTH], mdm_chArray_Two[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter First String : \n\n");
	gets_s(mdm_chArray_One, MDM_MAX_STRING_LENGTH);

	printf("\n");
	printf("Enter Second String : \n\n");
	gets_s(mdm_chArray_Two, MDM_MAX_STRING_LENGTH);

	// **** String Concat ****
	printf("\n\n");
	printf("******** Before Concatenation ********");
	printf("\n\n");
	printf("The Original First String Entered By You (i.e : 'mdm_chArray_One') Is : \n\n");
	printf("%s\n\n", mdm_chArray_One);

	printf("\n");
	printf("The Orginal Second String Entered By You (i.e : 'mdm_chArray_Two') Is : \n\n");
	printf("%s\n\n", mdm_chArray_Two);

	mdm_MyStrcat(mdm_chArray_One, mdm_chArray_Two);

	printf("\n\n");
	printf("******** After Concatenation ********");
	printf("\n\n");
	printf("'mdm_chArray_One[]' : \n\n");
	printf("%s\n\n", mdm_chArray_One);

	printf("\n");
	printf("'mdm_chArray_Two[]' : \n\n");
	printf("%s\n\n", mdm_chArray_Two);

	return(0);
}

void mdm_MyStrcat(char mdm_str_destination[], char mdm_str_source[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations
	int mdm_iStringLength_Source = 0, mdm_iStringLength_Destination = 0;
	int mdm_i, mdm_j;

	// code
	mdm_iStringLength_Source = mdm_MyStrlen(mdm_str_source);
	mdm_iStringLength_Destination = mdm_MyStrlen(mdm_str_destination);

	// Array Indices Begin From 0, Hence, Last Valid Index Of Array Will Always Be (Length - 1)
	// So, Concatenation Must Begin From Index Number Equal To Length Of The Array 'str_destination'
	// We Need To Put The Character Which Is At First Index Of 'str_source' To The (Last Index + 1) Of 'str_destination'

	for (mdm_i = mdm_iStringLength_Destination, mdm_j = 0; mdm_j < mdm_iStringLength_Source; mdm_i++, mdm_j++)
	{
		mdm_str_destination[mdm_i] = mdm_str_source[mdm_j];
	}

	mdm_str_destination[mdm_i] = '\0';
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character (\0) ****
	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}


