// header files
#include<stdio.h>
#include<string.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	char mdm_chArray_One[MDM_MAX_STRING_LENGTH], mdm_chArray_Two[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String

	// code

	// **** STRING INPUT ****
	printf("\n\n");
	printf("Enter First String : \n\n");
	gets_s(mdm_chArray_One, MDM_MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter Second String : \n\n");
	gets_s(mdm_chArray_Two, MDM_MAX_STRING_LENGTH);
	
	// **** String Concat ****
	printf("\n\n");
	printf("******** Before Concatenation ********");
	printf("\n\n");
	printf("The Original First String Entered By You (i.e : 'mdm_chArray_One[]') Is : \n\n");
	printf("%s\n\n", mdm_chArray_One);

	printf("\n");
	printf("The Original Second String Entered By You (i.e : 'mdm_chArray_Two[]') Is : \n\n");
	printf("%s\n\n", mdm_chArray_Two);

	strcat(mdm_chArray_One, mdm_chArray_Two);

	printf("\n\n");
	printf("******** After Concatenation ********");
	printf("\n\n");
	printf("'mdm_chArray_One[]' Is : \n\n");
	printf("%s\n\n", mdm_chArray_One);

	printf("\n");
	printf("'mdm_chArray_Two[]' Is : \n\n");
	printf("%s\n\n", mdm_chArray_Two);

	return(0);
}


