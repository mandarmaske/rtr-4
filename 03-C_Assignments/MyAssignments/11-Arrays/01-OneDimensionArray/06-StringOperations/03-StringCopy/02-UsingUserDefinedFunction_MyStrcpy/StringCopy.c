// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	void mdm_MyStrcpy(char[], char[]);

	// variable declaration
	char mdm_chArray_Original[MDM_MAX_STRING_LENGTH], mdm_chArray_Copy[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String 

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray_Original, MDM_MAX_STRING_LENGTH);

	// **** String Copy ****
	mdm_MyStrcpy(mdm_chArray_Copy, mdm_chArray_Original);

	// **** String Output ****
	printf("\n\n");
	printf("The Original String Entered By You (i.e 'mdm_chArray_Original') Is : \n\n");
	printf("%s\n", mdm_chArray_Original);

	printf("\n\n");
	printf("The Copied String (i.e 'mdm_chArray_Copy') : \n\n");
	printf("%s\n\n", mdm_chArray_Copy);

	return(0);
}

void mdm_MyStrcpy(char mdm_str_destination[], char mdm_str_source[])
{

	// function prototype
	int mdm_MyStrlen(char mdm_str[]);

	// variable declaration
	int mdm_j;
	int mdm_iStringLength;

	// code
	mdm_iStringLength = mdm_MyStrlen(mdm_str_source);

	for (mdm_j = 0; mdm_j < mdm_iStringLength; mdm_j++)
		mdm_str_destination[mdm_j] = mdm_str_source[mdm_j];

	mdm_str_destination[mdm_j] = '\0';
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String By Detecting The First Occurence Of Null-Terminating Character (\0) ****

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}
