// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv, char* envp[])
{
	// variable declarations
	char mdm_chArray_Original[MDM_MAX_STRING_LENGTH], mdm_chArray_Copy[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray_Original, MDM_MAX_STRING_LENGTH);

	// **** String Copy ****
	strcpy(mdm_chArray_Copy, mdm_chArray_Original);

	// **** String Output ****
	printf("\n\n");
	printf("The Original String Entered By You (i.e : 'mdm_chArray_Original') Is : \n\n");
	printf("%s\n", mdm_chArray_Original);

	printf("\n\n");
	printf("The Copied String (i.e : 'mdm_chArray_Copy') : \n\n");
	printf("%s\n\n", mdm_chArray_Copy);

	return(0);
}
