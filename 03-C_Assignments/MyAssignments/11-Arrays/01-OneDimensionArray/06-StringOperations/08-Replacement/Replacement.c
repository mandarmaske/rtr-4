// **** This Program Replaces All Vowels In The Input String With The *(asterisk) Symbol ****
// **** For Example, Orginal String 'Mandar Dilip Maske From ASTROMEDICOMP RTR Batch' will become 'M*nd*r D*l*p M*sk* Fr*m *STR*M*D*C*MP RTR B*tch'

// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototypes
	int mdm_MyStrlen(char[]);
	void mdm_MyStrcpy(char[], char[]);

	// variable declarations
	char mdm_chArray_Original[MDM_MAX_STRING_LENGTH], mdm_chArray_VowelsReplaced[MDM_MAX_STRING_LENGTH];
	int mdm_iStringLength;
	int mdm_i;

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray_Original, MDM_MAX_STRING_LENGTH);

	// **** String Output ****
	mdm_MyStrcpy(mdm_chArray_VowelsReplaced, mdm_chArray_Original);

	mdm_iStringLength = mdm_MyStrlen(mdm_chArray_VowelsReplaced);

	for (mdm_i = 0; mdm_i < mdm_iStringLength; mdm_i++)
	{
		switch (mdm_chArray_VowelsReplaced[mdm_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			mdm_chArray_VowelsReplaced[mdm_i] = '*';
			break;

		default:
			break;
		}
	}

	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n\n", mdm_chArray_Original);

	printf("\n");
	printf("String After Replaced Of Vowels By '*' Is : \n\n");
	printf("%s\n\n", mdm_chArray_VowelsReplaced);

	return(0);
}

void mdm_MyStrcpy(char mdm_str_destination[], char mdm_str_source[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declaration
	int mdm_iStringLength = 0;
	int mdm_j;

	// code
	mdm_iStringLength = mdm_MyStrlen(mdm_str_source);

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		mdm_str_destination[mdm_j] = mdm_str_source[mdm_j];
	}

	mdm_str_destination[mdm_j] = '\0';

}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declaration
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character '\0' ****

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}

