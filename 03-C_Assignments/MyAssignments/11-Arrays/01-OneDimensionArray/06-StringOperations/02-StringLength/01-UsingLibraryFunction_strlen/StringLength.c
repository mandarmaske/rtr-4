// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration 
	char mdm_chArray[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String
	int mdm_iStringLength = 0;

	// code

	// *** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	// *** String Length ***
	printf("\n\n");
	mdm_iStringLength = strlen(mdm_chArray);
	printf("Length Of String Is  = %d Characters !!!\n\n", mdm_iStringLength);

	return(0);
}

