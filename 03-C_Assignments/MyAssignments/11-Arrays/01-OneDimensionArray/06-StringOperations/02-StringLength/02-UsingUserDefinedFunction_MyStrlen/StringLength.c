// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declaration
	char mdm_chArray[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String
	int mdm_iStringLength;

	// code
	// *** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	// *** String Output ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", mdm_chArray);

	// *** String Length ***
	printf("\n\n");
	mdm_iStringLength = mdm_MyStrlen(mdm_chArray);
	printf("Length Of String Is = %d Characters !!!\n\n", mdm_iStringLength);

	return(0);
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declaration
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character (\0) ***
	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}

