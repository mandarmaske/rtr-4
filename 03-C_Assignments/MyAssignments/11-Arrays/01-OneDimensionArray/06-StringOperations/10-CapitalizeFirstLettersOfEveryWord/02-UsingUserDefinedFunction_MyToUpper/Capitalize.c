// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512
#define MDM_SPACE ' '
#define MDM_FULLSTOP '.'
#define MDM_EXCLAMATION '!'
#define MDM_QUESTION_MARK '?'
#define MDM_COMMA ','

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int mdm_MyStrlen(char[]);
	char mdm_MyToUpper(char);

	// variable declarations
	char mdm_chArray[MDM_MAX_STRING_LENGTH], mdm_chArray_CapitalizedFirstLetterOfEveryWord[MDM_MAX_STRING_LENGTH]; // A Character Array Is String
	int mdm_iStringLength;
	int mdm_i, mdm_j;

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	mdm_iStringLength = mdm_MyStrlen(mdm_chArray);
	mdm_j = 0;

	for (mdm_i = 0; mdm_i < mdm_iStringLength; mdm_i++)
	{
		if (mdm_i == 0) // First Letter Of Any Sentence Must Be Capital Letter
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = mdm_MyToUpper(mdm_chArray[mdm_i]);

		else if (mdm_chArray[mdm_i] == MDM_SPACE) // First Letter Of Every Word In The Sentence Must Be A Capital Letter, Words Are Seperated By Spaces
		{
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = mdm_chArray[mdm_i];
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j + 1] = mdm_MyToUpper(mdm_chArray[mdm_i + 1]);

			// since, already two characters (At Indices 'mdm_i' And 'mdm_i + 1' Have Been Considered In This else-if Block... We Are Extra-Incrementing 'mdm_i' and mdm_j By 1)
			mdm_j++;
			mdm_i++;
		}

		else if ((mdm_chArray[mdm_i] == MDM_FULLSTOP || mdm_chArray[mdm_i] == MDM_COMMA || mdm_chArray[mdm_i] == MDM_EXCLAMATION || mdm_chArray[mdm_i] == MDM_QUESTION_MARK) && (mdm_chArray[mdm_i] != MDM_SPACE)) // First Letter Of Every Word After Punctuation Mark, In The Sentence Must Be A Capital Letter, Words Are Seperated By Punctuations.
		{
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = mdm_chArray[mdm_i];
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j + 1] = MDM_SPACE;
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j + 2] = mdm_MyToUpper(mdm_chArray[mdm_i + 1]);

			// Since, Already Two Characters (At Indices 'mdm_i' And 'mdm_i + 1' Have Been Considered In This else-if Block...We Are Extra-Incrementing 'mdm_i' By 1)
			// Since, Already Three Characters (At Indices 'mdm_j' And (mdm_j + 1) And (mdm_j + 2) Have Been Considered In This else-if Block...We Are Extra-Incrementing 'mdm_j' by 2

			mdm_j = mdm_j + 2;
			mdm_i++;
		}

		else
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = mdm_chArray[mdm_i];

		mdm_j++;
	}

	mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = '\0';

	// **** String Output ****
	printf("\n\n");
	printf("String After Capitalizing First Letter Of Every Word : \n\n");
	printf("%s\n\n", mdm_chArray_CapitalizedFirstLetterOfEveryWord);

	return(0);
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character '\0' ****

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}

char mdm_MyToUpper(char mdm_ch)
{
	// variable declarations
	int mdm_num;
	int mdm_c;

	// code

	// ASCII Value Of 'a' (97), ASCII Value Of 'A' 65 = 32
	// This Subtraction Will Give The Exact Difference Between The Upper And Lower Case Counterparts Of Each Letter Of The Alphabet
	// If This Difference Is Subtracted From The ASCII Value Of A Lower Case Letter, The Resultant ASCII Value Will Be That Of Its Upper Case Letter !!!
	// ASCII Values Of 'a' to 'z' -> 97 to 122
	// ASCII Values Of 'A' to 'Z' -> 65 to 90

	mdm_num = 'a' - 'A';

	if ((int)mdm_ch >= 97 && (int)mdm_ch <= 122)
	{
		mdm_c = (int)mdm_ch - mdm_num;
		return((char)mdm_c);
	}
	else
		return(mdm_ch);
}

