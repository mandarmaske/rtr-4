// header files
#include<stdio.h>
#include<ctype.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations
	char mdm_chArray[MDM_MAX_STRING_LENGTH], mdm_chArray_CapitalizedFirstLetterOfEveryWord[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String
	int mdm_iStringLength;
	int mdm_i, mdm_j;

	// code

	// **** String Output ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	mdm_iStringLength = mdm_MyStrlen(mdm_chArray);
	mdm_j = 0;

	for (mdm_i = 0; mdm_i < mdm_iStringLength; mdm_i++)
	{
		if (mdm_i == 0)
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = toupper(mdm_chArray[mdm_i]);

		else if (mdm_chArray[mdm_i] == ' ')
		{
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = mdm_chArray[mdm_i];
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j + 1] = toupper(mdm_chArray[mdm_i + 1]);

			// Since, Already Two Characters (At Indices 'mdm_i' And 'mdm_i + 1' Have Been Considered In This else-if Block... We Are Extra-Incrementing 'mdm_i' And 'mdm_j' By 1)
			mdm_j++;
			mdm_i++;
		}

		else
			mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = mdm_chArray[mdm_i];

		mdm_j++;
	}

	mdm_chArray_CapitalizedFirstLetterOfEveryWord[mdm_j] = '\0';

	// **** String Output ****
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n\n", mdm_chArray);

	printf("\n");
	printf("String After Capitalizing First Letter Of Every Word : \n\n");
	printf("%s\n\n", mdm_chArray_CapitalizedFirstLetterOfEveryWord);

	return(0);
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}


