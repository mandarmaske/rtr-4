// header files
#include<stdio.h>
#include<string.h> // for strrev()

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	char mdm_chArray_Original[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray_Original, MDM_MAX_STRING_LENGTH);

	// **** String Output ****
	printf("\n\n");
	printf("The Original String Entered By You (i.e : 'mdm_chArray_Original[]') Is : \n\n");
	printf("%s\n\n", mdm_chArray_Original);

	printf("\n");
	printf("The Reversed String Of 'mdm_chArray_Original' Is : \n\n");
	printf("%s\n\n", strrev(mdm_chArray_Original));

	return(0);
}

