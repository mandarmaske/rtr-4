// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	void mdm_MyStrrev(char[], char[]);

	// variable declarations
	char mdm_chArray_Original[MDM_MAX_STRING_LENGTH], mdm_chArray_Reversed[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String
	
	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray_Original, MDM_MAX_STRING_LENGTH);

	// **** String Reverse ****
	mdm_MyStrrev(mdm_chArray_Reversed, mdm_chArray_Original);

	// **** String Output ****
	printf("\n\n");
	printf("The Original String Entered By You (i.e mdm_chArray_Original[]) Is : \n\n");
	printf("%s\n\n", mdm_chArray_Original);

	printf("\n");
	printf("The Reversed String Of 'mdm_chArray_Original' Is : \n\n");
	printf("%s\n\n\n", mdm_chArray_Reversed);

	return(0);
}

void mdm_MyStrrev(char mdm_destination[], char mdm_source[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations
	int mdm_i, mdm_j, mdm_len;
	int mdm_iStringLength = 0;

	// code
	mdm_iStringLength = mdm_MyStrlen(mdm_source);

	// Array Indices Begin From 0, Hence, Last Index Will Always Be (Length - 1)
	mdm_len = mdm_iStringLength - 1;

	// We Need To Put The Character Which Is At Last Index Of 'mdm_str_source' To The First Index Of 'mdm_str_destination'
	// And Second-Last Character Of 'mdm_str_source' To The Second Character Of 'mdm_str_destination' and so on...

	for (mdm_i = 0, mdm_j = mdm_len; mdm_i < mdm_iStringLength, mdm_j >= 0; mdm_i++, mdm_j--)
	{
		mdm_destination[mdm_i] = mdm_source[mdm_j];
	}

	mdm_destination[mdm_i] = '\0';

}


int mdm_MyStrlen(char mdm_str[])
{
	// variable declaration
	int mdm_j;
	int mdm_string_length = 0;

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}
