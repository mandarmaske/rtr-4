// header file
#include<stdio.h>

// constant value declaration
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations
	char mdm_chArray[MDM_MAX_STRING_LENGTH], mdm_chArray_SpacesRemoved[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String
	int mdm_iStringLength;
	int mdm_i, mdm_j;

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	mdm_iStringLength = mdm_MyStrlen(mdm_chArray);
	mdm_j = 0;

	for (mdm_i = 0; mdm_i < mdm_iStringLength; mdm_i++)
	{
		if (mdm_chArray[mdm_i] == ' ')
			continue;
		else
		{
			mdm_chArray_SpacesRemoved[mdm_j] = mdm_chArray[mdm_i];
			mdm_j++;
		}
	}

	mdm_chArray_SpacesRemoved[mdm_j] = '\0';

	// **** String Output ****
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n\n", mdm_chArray);

	printf("\n");
	printf("String After Removal Of Spaces Is : \n\n");
	printf("%s\n\n", mdm_chArray_SpacesRemoved);

	return(0);
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String, By Detecting The First Occurences Of Null-Terminating Character '\0' ****

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}

