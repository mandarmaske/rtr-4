// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv, char* envp[])
{
	// variable declarations
	char mdm_chArray[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String

	// code
	printf("\n\n");
	printf("Enter a string : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n\n", mdm_chArray);

	return(0);
}

