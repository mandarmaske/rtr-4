// header file
#include<stdio.h>

// constant value definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv, char* envp[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations
	char mdm_chArray[MDM_MAX_STRING_LENGTH]; // A Character Array Is A String 
	int mdm_iStringLength;
	int mdm_count_Aa = 0, mdm_count_Ee = 0, mdm_count_Ii = 0, mdm_count_Oo = 0, mdm_count_Uu = 0;// Initial Count = 0
	int mdm_i;

	// code

	// **** String Input ****
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	// **** String Output ****
	printf("\n\n");
	printf("String Entered By You : \n\n");
	printf("%s\n\n", mdm_chArray);

	mdm_iStringLength = mdm_MyStrlen(mdm_chArray);

	for (mdm_i = 0; mdm_i < mdm_iStringLength; mdm_i++)
	{
		switch (mdm_chArray[mdm_i])
		{
		case 'A':
		case 'a':
			mdm_count_Aa++;
			break;

		case 'E':
		case 'e':
			mdm_count_Ee++;
			break;

		case 'I':
		case 'i':
			mdm_count_Ii++;
			break;

		case 'O':
		case 'o':
			mdm_count_Oo++;
			break;

		case 'U':
		case 'u':
			mdm_count_Uu++;
			break;

		default:
			break;

		}
	}

	printf("\n\n");
	printf("In The String Entered By You, The Vowels And The Number Of Their Occurences Are Follows : \n\n");
	printf("A or a Has Occured = %d Times !!!\n\n", mdm_count_Aa);
	printf("E or e Has Occured = %d Times !!!\n\n", mdm_count_Ee);
	printf("I or i Has Occured = %d Times !!!\n\n", mdm_count_Ii);
	printf("O or o Has Occured = %d Times !!!\n\n", mdm_count_Oo);
	printf("U or u Has Occured = %d Times !!!\n\n", mdm_count_Uu);

	return(0);
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character (\0) ****
	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}

