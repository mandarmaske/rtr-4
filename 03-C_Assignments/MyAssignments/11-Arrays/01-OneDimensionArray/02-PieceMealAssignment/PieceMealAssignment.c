// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArrayOne[10];
	int mdm_iArrayTwo[20];

	// code

	// ******** iArray ********
	mdm_iArrayOne[0] = 3;
	mdm_iArrayOne[1] = 6;
	mdm_iArrayOne[2] = 9;
	mdm_iArrayOne[3] = 12;
	mdm_iArrayOne[4] = 15;
	mdm_iArrayOne[5] = 18;
	mdm_iArrayOne[6] = 21;
	mdm_iArrayOne[7] = 24;
	mdm_iArrayOne[8] = 27;
	mdm_iArrayOne[9] = 30;

	printf("\n\n");
	printf("Piece-Meal (Hard-Coded) Assignment And Display Of Elements to Array 'mdm_iArrayOne[]' : \n\n");
	printf("1st Element Of Array 'mdm_iArrayOne[]' Or Element At 0th Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[0]);
	printf("2nd Element Of Array 'mdm_iArrayOne[]' Or Element At 1st Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[1]);
	printf("3rd Element Of Array 'mdm_iArrayOne[]' Or Element At 2nd Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[2]);
	printf("4th Element Of Array 'mdm_iArrayOne[]' Or Element At 3rd Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[3]);
	printf("5th Element Of Array 'mdm_iArrayOne[]' Or Element At 4th Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[4]);
	printf("6th Element Of Array 'mdm_iArrayOne[]' Or Element At 5th Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[5]);
	printf("7th Element Of Array 'mdm_iArrayOne[]' Or Element At 6th Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[6]);
	printf("8th Element Of Array 'mdm_iArrayOne[]' Or Element At 7th Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[7]);
	printf("9th Element Of Array 'mdm_iArrayOne[]' Or Element At 8th Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[8]);
	printf("10th Element Of Array 'mdm_iArrayOne[]' Or Element At 9th Index Of Array 'mdm_iArrayOne[]' = %d\n", mdm_iArrayOne[9]);

	// ******** iArrayTwo[] ********
	printf("\n\n");

	printf("Enter 1st Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[0]);
	printf("Enter 2nd Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[1]);
	printf("Enter 3rd Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[2]);
	printf("Enter 4th Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[3]);
	printf("Enter 5th Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[4]);
	printf("Enter 6th Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[5]);
	printf("Enter 7th Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[6]);
	printf("Enter 8th Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[7]);
	printf("Enter 9th Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[8]);
	printf("Enter 10th Element Of Array 'mdm_iArrayTwo[]' : ");
	scanf("%d", &mdm_iArrayTwo[9]);

	printf("\n\n");
	printf("Piece-Meal (User Input) Assignment And Display Of Elements to Array 'mdm_iArrayTwo[]' : \n\n");
	printf("1st Element Of Array 'mdm_iArrayTwo[]' Or Element At 0th Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[0]);
	printf("2nd Element Of Array 'mdm_iArrayTwo[]' Or Element At 1st Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[1]);
	printf("3rd Element Of Array 'mdm_iArrayTwo[]' Or Element At 2nd Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[2]);
	printf("4th Element Of Array 'mdm_iArrayTwo[]' Or Element At 3rd Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[3]);
	printf("5th Element Of Array 'mdm_iArrayTwo[]' Or Element At 4th Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[4]);
	printf("6th Element Of Array 'mdm_iArrayTwo[]' Or Element At 5th Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[5]);
	printf("7th Element Of Array 'mdm_iArrayTwo[]' Or Element At 6th Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[6]);
	printf("8th Element Of Array 'mdm_iArrayTwo[]' Or Element At 7th Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[7]);
	printf("9th Element Of Array 'mdm_iArrayTwo[]' Or Element At 8th Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[8]);
	printf("10th Element Of Array 'mdm_iArrayTwo[]' Or Element At 9th Index Of Array 'mdm_iArrayTwo[]' = %d\n", mdm_iArrayTwo[9]);

	return(0);
}
