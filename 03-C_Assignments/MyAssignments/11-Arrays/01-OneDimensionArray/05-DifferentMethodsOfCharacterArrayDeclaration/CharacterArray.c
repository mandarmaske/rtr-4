// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv, char* envp[])
{
	// variable declarations
	char mdm_chArray1[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P', '\0' }; // Must Give \0 Explicitly For Proper Initialization
	char mdm_chArray2[9] = { 'W', 'E', 'L', 'C', 'O', 'M', 'E', 'S', '\0' }; // Must give \0 Explicitly For Proper Initialization
	char mdm_chArray3[] = { 'Y', 'O', 'U', '\0'}; // Must Give \0 Explicitly For Proper Initialization
	char mdm_chArray4[] = "TO"; // \0 is assumed, size is given as 3, although string has only 2 characters
	char mdm_chArray5[] = "REAL TIME RENDERING RENDERING BATCH OF 2021-2023"; // \0 is assumed, size is given as 40, although string has 39 characters

	char mdm_chArray_WithoutNullTerminator[] = { 'H', 'e', 'l', 'l', 'o' };

	// code
	printf("\n\n");

	printf("Size Of mdm_chArray1[] : %lu\n", sizeof(mdm_chArray1));
	printf("Size Of mdm_chArray2[] : %lu\n", sizeof(mdm_chArray2));
	printf("Size Of mdm_chArray3[] : %lu\n", sizeof(mdm_chArray3));
	printf("Size Of mdm_chArray4[] : %lu\n", sizeof(mdm_chArray4));
	printf("Size Of mdm_chArray5[] : %lu\n", sizeof(mdm_chArray5));

	printf("\n\n");

	printf("The Strings Are : \n\n");
	printf("mdm_chArray1 : %s\n", mdm_chArray1);
	printf("mdm_chArray2 : %s\n", mdm_chArray2);
	printf("mdm_chArray3 : %s\n", mdm_chArray3);
	printf("mdm_chArray4 : %s\n", mdm_chArray4);
	printf("mdm_chArray5 : %s\n", mdm_chArray5);

	printf("\n\n");

	printf("Size Of chArray_WithoutNullTerminator : %lu\n\n", sizeof(mdm_chArray_WithoutNullTerminator));
	printf("mdm_chArray_WithoutNullTerminator : %s\n\n", mdm_chArray_WithoutNullTerminator); // will display garbage value at the end of string dur to abscence of \0

	return(0);
}
