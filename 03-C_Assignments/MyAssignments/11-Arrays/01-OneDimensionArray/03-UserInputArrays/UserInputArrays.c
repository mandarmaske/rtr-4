// header file
#include<stdio.h>

// MACRO CONSTANT USED AS ARRAY SIZE IN SUBSCRIPT AND AS ARRAY LENGTH.
// HENCE, THIS PROGRAM'S ARRAY'S SIZES CAN BE SIMPLY CHANGED BY CHANGING THESE FOLLOWING 3 GLOBAL MACRO CONSTANT VALUES, BEFORE COMPILING, LINKING AND EXECUTING THE PROGRAM !!!

#define MDM_INT_ARRAY_NUM_ELEMENTS 5
#define MDM_FLOAT_ARRAY_NUM_ELEMENTS 3
#define MDM_CHAR_ARRAY_NUM_ELEMENTS 15

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// variable declaration
	int mdm_iArray[MDM_INT_ARRAY_NUM_ELEMENTS];
	float mdm_fArray[MDM_FLOAT_ARRAY_NUM_ELEMENTS];
	char mdm_cArray[MDM_CHAR_ARRAY_NUM_ELEMENTS];
	int mdm_i;

	// code

	// ******** ARRAY ELEMENTS INPUT ********
	printf("\n\n");
	printf("Enter Elements For 'Integer' Array : \n\n");

	for (mdm_i = 0; mdm_i < MDM_INT_ARRAY_NUM_ELEMENTS; mdm_i++)
	{
		scanf("%d", &mdm_iArray[mdm_i]);
	}

	printf("\n\n");
	printf("Enter Elements For 'Floating-Point' Array : \n\n");

	for (mdm_i = 0; mdm_i < MDM_FLOAT_ARRAY_NUM_ELEMENTS; mdm_i++)
	{
		scanf("%f", &mdm_fArray[mdm_i]);
	}

	printf("\n\n");
	printf("Enter Elements For 'Character' Array : \n\n");

	for (mdm_i = 0; mdm_i < MDM_CHAR_ARRAY_NUM_ELEMENTS; mdm_i++)
	{
		mdm_cArray[mdm_i] = getch();
		printf("%c", mdm_cArray[mdm_i]);
	}

	// ******** ARRAY ELEMENTS OUTPUT ********
	printf("\n\n");
	printf("Integer Array Entered By You : \n\n");

	for (mdm_i = 0; mdm_i < MDM_INT_ARRAY_NUM_ELEMENTS; mdm_i++)
	{
		printf("%d\n", mdm_iArray[mdm_i]);
	}

	printf("\n\n");
	printf("Floating Point Entered By You : \n\n");

	for (mdm_i = 0; mdm_i < MDM_FLOAT_ARRAY_NUM_ELEMENTS; mdm_i++)
	{
		printf("%f\n", mdm_fArray[mdm_i]);
	}

	printf("\n\n");
	printf("Character Array Entered By You : \n\n");

	for (mdm_i = 0; mdm_i < MDM_CHAR_ARRAY_NUM_ELEMENTS; mdm_i++)
	{
		printf("%c\n", mdm_cArray[mdm_i]);
	}

	return(0);
}
