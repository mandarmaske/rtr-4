// header file
#include<stdio.h>

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// variable declarations
	int mdm_iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int mdm_int_size;
	int mdm_iArray_size;
	int mdm_iArray_num_elements;

	float mdm_fArray[] = { 1.2f, 2.4f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int mdm_float_size;
	int mdm_fArray_size;
	int mdm_fArray_num_elements;

	char mdm_cArray[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P' };
	int mdm_char_size;
	int mdm_cArray_size;
	int mdm_cArray_num_elements;

	// code

	// ************** mdm_iArray *****************
	printf("\n\n");
	printf("In-Line Initialization And Piece-Meal Display Of Elements Of Array 'mdm_iArray[]' : \n\n");
	printf("mdm_iArray[0] (1st Element)  = %d\n", mdm_iArray[0]);
	printf("mdm_iArray[1] (2nd Element)  = %d\n", mdm_iArray[1]);
	printf("mdm_iArray[2] (3rd Element)  = %d\n", mdm_iArray[2]);
	printf("mdm_iArray[3] (4th Element)  = %d\n", mdm_iArray[3]);
	printf("mdm_iArray[4] (5th Element)  = %d\n", mdm_iArray[4]);
	printf("mdm_iArray[5] (6th Element)  = %d\n", mdm_iArray[5]);
	printf("mdm_iArray[6] (7th Element)  = %d\n", mdm_iArray[6]);
	printf("mdm_iArray[7] (8th Element)  = %d\n", mdm_iArray[7]);
	printf("mdm_iArray[8] (9th Element)  = %d\n", mdm_iArray[8]);
	printf("mdm_iArray[9] (10th Element) = %d\n", mdm_iArray[9]);

	mdm_int_size = sizeof(int);
	mdm_iArray_size = sizeof(mdm_iArray);
	mdm_iArray_num_elements = mdm_iArray_size / mdm_int_size;

	printf("\nSize of Data type 'int'                               = %d Bytes\n", mdm_int_size);
	printf("Number Of Elements In 'int' Array 'mdm_iArray[]'      = %d Elements\n", mdm_iArray_num_elements);
	printf("Size of Array 'mdm_iArray[]' (%d Elements * %d bytes)  = %d Bytes\n", mdm_iArray_num_elements, mdm_int_size, mdm_iArray_size);

	// ************** mdm_fArray *****************
	printf("\n\n");
	printf("In line Initialization And Piece-Meal Display Of Elements Of Array 'mdm_fArray[]' : \n\n");
	printf("mdm_fArray[0] (1st Element)  = %f\n", mdm_fArray[0]);
	printf("mdm_fArray[1] (2nd Element)  = %f\n", mdm_fArray[1]);
	printf("mdm_fArray[2] (3rd Element)  = %f\n", mdm_fArray[2]);
	printf("mdm_fArray[3] (4th Element)  = %f\n", mdm_fArray[3]);
	printf("mdm_fArray[4] (5th Element)  = %f\n", mdm_fArray[4]);
	printf("mdm_fArray[5] (6th Element)  = %f\n", mdm_fArray[5]);
	printf("mdm_fArray[6] (7th Element)  = %f\n", mdm_fArray[6]);
	printf("mdm_fArray[7] (8th Element)  = %f\n", mdm_fArray[7]);
	printf("mdm_fArray[8] (9th Element)  = %f\n", mdm_fArray[8]);
	printf("mdm_fArray[9] (10th Element) = %f\n", mdm_fArray[9]);

	mdm_float_size = sizeof(float);
	mdm_fArray_size = sizeof(mdm_fArray);
	mdm_fArray_num_elements = mdm_fArray_size / mdm_float_size;
	printf("\nSize of Data Type 'float'                             = %d Bytes\n", mdm_float_size);
	printf("Number Of Elements In 'float' Array 'mdm_fArray[]'    = %d Elements\n", mdm_fArray_num_elements);
	printf("Size Of Array 'mdm_fArray[]' (%d Elements * %d Bytes)   = %d Bytes\n", mdm_fArray_num_elements, mdm_float_size, mdm_fArray_size);
	
	// ************** mdm_cArray *****************
	printf("\n\n");
	printf("In Line Initialization And Piece-Meal Display Of Elements Of Array 'mdm_cArray[]' : \n\n");
	printf("mdm_cArray[0] (1st Element)   = %c\n", mdm_cArray[0]);
	printf("mdm_cArray[1] (2nd Element)   = %c\n", mdm_cArray[1]);
	printf("mdm_cArray[2] (3rd Element)   = %c\n", mdm_cArray[2]);
	printf("mdm_cArray[3] (4th Element)   = %c\n", mdm_cArray[3]);
	printf("mdm_cArray[4] (5th Element)   = %c\n", mdm_cArray[4]);
	printf("mdm_cArray[5] (6th Element)   = %c\n", mdm_cArray[5]);
	printf("mdm_cArray[6] (7th Element)   = %c\n", mdm_cArray[6]);
	printf("mdm_cArray[7] (8th Element)   = %c\n", mdm_cArray[7]);
	printf("mdm_cArray[8] (9th Element)   = %c\n", mdm_cArray[8]);
	printf("mdm_cArray[9] (10th Element)  = %c\n", mdm_cArray[9]);
	printf("mdm_cArray[10] (11th Element) = %c\n", mdm_cArray[10]);
	printf("mdm_cArray[11] (12th Element) = %c\n", mdm_cArray[11]);
	printf("mdm_cArray[12] (13th Element) = %c\n", mdm_cArray[12]);

	mdm_char_size = sizeof(char);
	mdm_cArray_size = sizeof(mdm_cArray);
	mdm_cArray_num_elements = mdm_cArray_size / mdm_char_size;
	printf("\nSize of Data type 'char'                              = %d Bytes\n", mdm_char_size);
	printf("Number Of Elements In 'char' Array 'mdm_cArray[]'     = %d Elements\n", mdm_cArray_num_elements);
	printf("Size Of Array 'mdm_cArray[]' (%d Elements * %d Bytes)  = %d Bytes\n", mdm_cArray_num_elements, mdm_char_size, mdm_cArray_size);

	return(0);
}
