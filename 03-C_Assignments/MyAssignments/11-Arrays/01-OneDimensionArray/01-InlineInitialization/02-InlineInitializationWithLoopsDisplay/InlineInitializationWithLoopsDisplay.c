// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{

	// variable declarations
	int mdm_iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int mdm_int_size;
	int mdm_iArray_size;
	int mdm_iArray_num_elements;

	float mdm_fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int mdm_float_size;
	int mdm_fArray_size;
	int mdm_fArray_num_elements;

	char mdm_cArray[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P' };
	int mdm_char_size;
	int mdm_cArray_size;
	int mdm_cArray_num_elements;

	int mdm_i;

	// code

	// ******** mdm_iArray ********
	printf("\n\n");
	printf("In-Line Initialization And Loop (for) Display Of Elements Of Array 'mdm_iArray[]' : \n\n");

	mdm_int_size = sizeof(int);
	mdm_iArray_size = sizeof(mdm_iArray);
	mdm_iArray_num_elements = mdm_iArray_size / mdm_int_size;

	for (mdm_i = 0; mdm_i < mdm_iArray_num_elements; mdm_i++)
	{
		printf("mdm_iArray[%d] (Element %d) = %d\n", mdm_i, (mdm_i + 1), mdm_iArray[mdm_i]);
	}

	printf("\n\n"); 
	printf("Size Of Data Type 'int'                              = %d Bytes\n", mdm_int_size);
	printf("Number Of Elements In 'int' Array 'mdm_iArray[]'     = %d Elements\n", mdm_iArray_num_elements);
	printf("Size Of Array 'mdm_iArray[]' (%d Elements * %d Bytes) = %d Bytes\n", mdm_iArray_num_elements, mdm_int_size, mdm_iArray_size);


	// ******** mdm_fArray ********
	printf("\n\n");
	printf("In-line initialization And Loop (while) Display Of Elements Of Array 'mdm_fArray[]' :\n\n");

	mdm_float_size = sizeof(float);
	mdm_fArray_size = sizeof(mdm_fArray);
	mdm_fArray_num_elements = mdm_fArray_size / mdm_float_size;

	mdm_i = 0;
	while (mdm_i < mdm_fArray_num_elements)
	{
		printf("mdm_fArray[%d] (Element %d) = %f\n", mdm_i, (mdm_i + 1), mdm_fArray[mdm_i]);
		mdm_i++;
	}

	printf("\n\n");
	printf("Size of Data Type 'float'                           = %d Bytes\n", mdm_float_size);
	printf("Number Of Elements In 'float' Array 'mdm_fArray[]'  = %d Elements\n", mdm_fArray_num_elements);
	printf("Size Of Array 'mdm_fArray' (%d Elements * %d Bytes)   = %d Bytes\n", mdm_fArray_num_elements, mdm_float_size, mdm_fArray_size);
	
	// ******** mdm_cArray ********
	printf("\n\n");
	printf("In-Line Initialization And Loop (do while) Display Of Elements Of Array 'mdm_cArray[]' : \n\n");

	mdm_char_size = sizeof(char);
	mdm_cArray_size = sizeof(mdm_cArray);
	mdm_cArray_num_elements = mdm_cArray_size / mdm_char_size;

	mdm_i = 0;
	do
	{
		printf("'mdm_cArray[%d]' (Element %d) : %c\n", mdm_i, (mdm_i + 1), mdm_cArray[mdm_i]);
		mdm_i++;
	} while (mdm_i < mdm_cArray_num_elements);

	printf("\n\n");
	printf("Size of Data Type 'char'                               = %d Bytes\n", mdm_char_size);
	printf("Number Of Elements in 'char' Array 'mdm_cArray[]'      = %d Elements\n", mdm_cArray_num_elements);
	printf("Size Of Array 'mdm_cArray[]' (%d Elements * %d Bytes)   = %d Bytes\n", mdm_cArray_num_elements, mdm_char_size, mdm_cArray_size);

	return(0);
}
