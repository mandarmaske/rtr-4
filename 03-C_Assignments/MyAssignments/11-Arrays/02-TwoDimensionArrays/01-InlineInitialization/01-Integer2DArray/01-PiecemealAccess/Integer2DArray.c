// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} }; // In-Line Initialization
	int mdm_int_size;
	int mdm_iArray_size;
	int mdm_iArray_num_elements, mdm_iArray_num_rows, mdm_iArray_num_columns;

	// code
	printf("\n\n");

	mdm_int_size = sizeof(int);
	mdm_iArray_size = sizeof(mdm_iArray);
	printf("Size of Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_size);

	mdm_iArray_num_rows = mdm_iArray_size / sizeof(mdm_iArray[0]);
	printf("Number Of Rows In Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_num_rows);

	mdm_iArray_num_columns = sizeof(mdm_iArray[0]) / mdm_int_size;
	printf("Number Of Columns In Two Dimensional (2D) Integer Array Is : %d\n\n", mdm_iArray_num_columns);

	mdm_iArray_num_elements = mdm_iArray_num_rows * mdm_iArray_num_columns;
	printf("The Total Number Of Elements In Two Dimensional (2D) Integer Array Is : %d\n\n", mdm_iArray_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n\n");

	// **** Array Indices Begin From 0, Hence, 1st Row Is Actually 0th Row And 1st Column Is Actually 0th Column ****

	// *** Row 1 ***
	printf("*** Row 1 ***\n");
	printf("mdm_iArray[0][0] = %d\n", mdm_iArray[0][0]); // *** Column 1 *** (0th Element) -> 1
	printf("mdm_iArray[0][1] = %d\n", mdm_iArray[0][1]); // *** Column 2 *** (1st Element) -> 2
	printf("mdm_iArray[0][2] = %d\n", mdm_iArray[0][2]); // *** Column 3 *** (2nd Element) -> 3

	printf("\n\n");

	// *** Row 2 ***
	printf("*** Row 2 ***\n");
	printf("mdm_iArray[1][0] = %d\n", mdm_iArray[1][0]); // *** Column 1 *** (0th Element) -> 2
	printf("mdm_iArray[1][1] = %d\n", mdm_iArray[1][1]); // *** Column 2 *** (1th Element) -> 4
	printf("mdm_iArray[1][2] = %d\n", mdm_iArray[1][2]); // *** Column 3 *** (2nd Element) -> 6

	printf("\n\n");

	// *** Row 3 ***
	printf("*** Row 3 ***\n");
	printf("mdm_iArray[2][0] = %d\n", mdm_iArray[2][0]); // *** Column 1 *** (0th Element) -> 3
	printf("mdm_iArray[2][1] = %d\n", mdm_iArray[2][1]); // *** Column 2 *** (1st Element) -> 6
	printf("mdm_iArray[2][2] = %d\n", mdm_iArray[2][2]); // *** Column 3 *** (2nd Element) -> 9

	printf("\n\n");

	// *** Row 4 ***
	printf("*** Row 4 ***\n");
	printf("mdm_iArray[3][0] = %d\n", mdm_iArray[3][0]); // *** Column 1 *** (0th Element) -> 4
	printf("mdm_iArray[3][1] = %d\n", mdm_iArray[3][1]); // *** Column 2 *** (1st Element) -> 8
	printf("mdm_iArray[3][2] = %d\n", mdm_iArray[3][2]); // *** Column 3 *** (2nd Element) -> 12

	printf("\n\n");

	// *** Row 5 ***
	printf("*** Row 5 ***\n");
	printf("mdm_iArray[4][0] = %d\n", mdm_iArray[4][0]); // *** Column 1 *** (0th Element) -> 5
	printf("mdm_iArray[4][1] = %d\n", mdm_iArray[4][1]); // *** Column 2 *** (1st Element) -> 10
	printf("mdm_iArray[4][2] = %d\n", mdm_iArray[4][2]); // *** Column 3 *** (2nd Element) -> 15

	printf("\n\n");

	return(0);
}

