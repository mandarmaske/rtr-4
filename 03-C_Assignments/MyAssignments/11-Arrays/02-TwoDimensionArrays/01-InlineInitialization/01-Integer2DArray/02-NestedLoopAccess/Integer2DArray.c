// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv, char* envp[])
{
	// variable declaration
	int mdm_iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} }; // In-Line Initialization
	int mdm_int_size, mdm_iArray_size;
	int mdm_iArray_num_elements, mdm_iArray_num_rows, mdm_iArray_num_columns;
	int mdm_i, mdm_j;

	// code
	printf("\n\n");

	mdm_int_size = sizeof(int);
	mdm_iArray_size = sizeof(mdm_iArray);
	printf("Size Of Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_size);

	mdm_iArray_num_rows = mdm_iArray_size / sizeof(mdm_iArray[0]);
	printf("The Number Of Rows In Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_num_rows);

	mdm_iArray_num_columns = sizeof(mdm_iArray[0]) / sizeof(mdm_int_size);
	printf("The Number Of Columns In Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_num_columns);

	mdm_iArray_num_elements = mdm_iArray_num_rows * mdm_iArray_num_columns;
	printf("The Total Number Of Elements In Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_num_elements);

	printf("\n");
	printf("Elements In The 2D Array (By Nested Loop) : \n\n\n");

	for (mdm_i = 0; mdm_i < mdm_iArray_num_rows; mdm_i++)
	{
		printf("*** Row %d ***\n", (mdm_i + 1));

		for (mdm_j = 0; mdm_j < mdm_iArray_num_columns; mdm_j++)
		{
			printf("mdm_iArray[%d][%d] = %d\n", mdm_i, mdm_j, mdm_iArray[mdm_i][mdm_j]);
		}
		printf("\n\n");
	}

	return(0);
}

