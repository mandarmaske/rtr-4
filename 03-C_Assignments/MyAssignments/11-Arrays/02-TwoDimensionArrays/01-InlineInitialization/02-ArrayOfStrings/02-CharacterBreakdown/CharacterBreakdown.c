// header file
#include<stdio.h>

// constant value defination
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations

	/*
		*** A 'string' Is An Array Of Characters... so char[] Is A 'char' Array And Hence, char[] Is A 'string'
		*** An Array Of 'char' Arrays Is An Array Of Strings !!! ***
		*** Hence, char[] Is One 'char' Array And Hence, Is One String ***
		*** Hence, char[] Is One char[][] Is An Array Of char Arrays And Hence, Is An Array Of Strings ***
	*/

	/*
		Here, the string array can allow a maximum number of 10 strings(10 rows) and each of these 10 (rows)strings can have
		only upto 15 characters maximum (15 Columns)
	*/

	char mdm_strArray[10][15] = { "Hello!",
							  "Welcome",
							  "To",
							  "Real",
							  "Time",
							  "Rendering",
							  "Batch",
							  "(2021-2023)",
							  "Of",
							  "AstroMediComp." }; // In-Line Initialization

	int mdm_istrLengths[10]; // 1D Integer Array - Stores lengths of those strings at corresponding indices in 'mdm_strArray[]' e.g : mdm_iStrLengths[0] will be the length of string at mdm_strArray[0],
	// mdm_iStrLengths will be the length of string at strArray[1]...10 strings, 10 lengths...
	int mdm_strArray_size;
	int mdm_strArray_num_rows;
	int mdm_i, mdm_j;

	// code
	mdm_strArray_size = sizeof(mdm_strArray);
	mdm_strArray_num_rows = mdm_strArray_size / sizeof(mdm_strArray[0]);

	// storing in lengths of all the strings...
	for (mdm_i = 0; mdm_i < mdm_strArray_num_rows; mdm_i++)
		mdm_istrLengths[mdm_i] = mdm_MyStrlen(mdm_strArray[mdm_i]);

	printf("\n\n");

	printf("The Entire String Array : \n\n");
	for (mdm_i = 0; mdm_i < mdm_strArray_num_rows; mdm_i++)
		printf("%s ", mdm_strArray[mdm_i]);

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");

	// Since, char[][] is an Array of strings, referencing only by the row number (first []) will give the row or string
	// The Column Number (Second []) is the particular character in that string / row

	for (mdm_i = 0; mdm_i < mdm_strArray_num_rows; mdm_i++)
	{
		printf("String Number %d => %s\n\n", (mdm_i + 1), mdm_strArray[mdm_i]);
		for (mdm_j = 0; mdm_j < mdm_istrLengths[mdm_i]; mdm_j++)
		{
			printf("Character %d = %c\n", (mdm_j + 1), mdm_strArray[mdm_i][mdm_j]);
		}
		printf("\n\n");
	}

	return(0);
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// *** Determining Exact Length Of The String, By Detecting The First Occurence Of Null Terminating Character (\0) ***
	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}