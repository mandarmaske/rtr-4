// header file
#include<stdio.h>

// constant value defination
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations

	/*
		*** A 'string' Is An Array Of Characters... so char[] Is A 'char' Array And Hence, char[] Is A 'string'	
		*** An Array Of 'char' Arrays Is An Array Of Strings !!! ***
		*** Hence, char[] Is One 'char' Array And Hence, Is One String ***
		*** Hence, char[] Is One char[][] Is An Array Of char Arrays And Hence, Is An Array Of Strings ***
	*/

	/*
		Here, the string array can allow a maximum number of 10 strings(10 rows) and each of these 10 (rows)strings can have
		only upto 15 characters maximum (15 Columns)
	*/

	char mdm_strArray[10][15] = { "Hello!",
							  "Welcome",
							  "To",
							  "Real",
							  "Time",
							  "Rendering",
							  "Batch",
							  "(2021-2023)",
							  "Of",
							  "AstroMediComp." }; // In-Line Initialization

	int mdm_char_size;
	int mdm_strArray_size;
	int mdm_strArray_num_elements, mdm_strArray_num_rows, mdm_strArray_num_columns;
	int mdm_strActual_num_chars = 0;
	int mdm_i;

	// code
	printf("\n\n");

	mdm_char_size = sizeof(char);

	mdm_strArray_size = sizeof(mdm_strArray);
	printf("Size Of Two Dimensional (2D) Character Array (String Array) Is = %d\n\n", mdm_strArray_size);

	mdm_strArray_num_rows = mdm_strArray_size / sizeof(mdm_strArray[0]);
	printf("The Total Number Of Rows In Two Dimensional (2D) Character Arrays (String Arrays) = %d\n\n", mdm_strArray_num_rows);

	mdm_strArray_num_columns = sizeof(mdm_strArray[0]) / mdm_char_size;
	printf("The Total Number Of Columns In Two Dimensional (2D) Character Arrays (String Arrays) = %d\n\n", mdm_strArray_num_columns);

	mdm_strArray_num_elements = mdm_strArray_num_rows * mdm_strArray_num_columns;
	printf("Maximum Number Of Elements (Characters) In Two Dimensional (2D) Character Array (String Array) Is = %d\n\n", mdm_strArray_num_elements);

	for (mdm_i = 0; mdm_i < mdm_strArray_num_rows; mdm_i++)
	{
		mdm_strActual_num_chars = mdm_strActual_num_chars + mdm_MyStrlen(mdm_strArray[mdm_i]);
	}
	printf("Actual Number Of Elements (Characters) In Two Dimensional (2D) Character Array (String Array) Is = %d\n\n", mdm_strActual_num_chars);

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");

	// Since, char[][] is an array of strings, referencing only by the row number (first[]) will give the row or the string
	// The column number (second []) is the particular character in the string / row

	printf("%s ", mdm_strArray[0]);
	printf("%s ", mdm_strArray[1]);
	printf("%s ", mdm_strArray[2]);
	printf("%s ", mdm_strArray[3]);
	printf("%s ", mdm_strArray[4]);
	printf("%s ", mdm_strArray[5]);
	printf("%s ", mdm_strArray[6]);
	printf("%s ", mdm_strArray[7]);
	printf("%s ", mdm_strArray[8]);
	printf("%s \n\n", mdm_strArray[9]);

	return(0);
}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// *** Determining Exact Length Of The String, By Detecting The First Occurence Of Null Terminating Character (\0) ***

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}
	return(mdm_string_length);
}

