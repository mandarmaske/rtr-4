// header file
#include<stdio.h>

// constant value definition
#define MDM_NUM_ROWS 5
#define MDM_NUM_COLUMNS 3

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	int mdm_iArray_2D[MDM_NUM_ROWS][MDM_NUM_COLUMNS]; // Total Number Of Elements = MDM_NUM_ROWS * MDM_NUM_COLUMNS
	int mdm_iArray_1D[MDM_NUM_ROWS * MDM_NUM_COLUMNS];
	int mdm_i, mdm_j;
	int mdm_num;

	// code
	printf("Enter Elements Of Your Choice To Fill Up The Integer 2D Array : \n\n");

	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		printf("For Row Number %d : \n", (mdm_i + 1));
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("The Element Number %d : \n", (mdm_j + 1));
			scanf("%d", &mdm_num);
			mdm_iArray_2D[mdm_i][mdm_j] = mdm_num;
		}

		printf("\n\n");
	}

	// **** Display Of 2D Array ****
	printf("\n\n");
	printf("Two Dimensional (2D) Array Of Integers : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		printf("******** Row %d ********\n", (mdm_i + 1));
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("mdm_iArray_2D[%d][%d] = %d\n", mdm_i, mdm_j, mdm_iArray_2D[mdm_i][mdm_j]);
		}

		printf("\n\n");
	}

	// **** Converting 2D Integer Array To 1D Integer Array ****
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			mdm_iArray_1D[(mdm_i * MDM_NUM_COLUMNS) + mdm_j] = mdm_iArray_2D[mdm_i][mdm_j];
		}
	}

	// **** Printing 1D Array ****
	printf("\n\n");
	printf("One Dimensional (1D) Array Of Integers : \n\n");
	for (mdm_i = 0; mdm_i < (MDM_NUM_ROWS * MDM_NUM_COLUMNS); mdm_i++)
	{
		printf("mdm_iArray_1D[%d] = %d\n\n", mdm_i, mdm_iArray_1D[mdm_i]);
	}

	printf("\n\n");

	return(0);
}


