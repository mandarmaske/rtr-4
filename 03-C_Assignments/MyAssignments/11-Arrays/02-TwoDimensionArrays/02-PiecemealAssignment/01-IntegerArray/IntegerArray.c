// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv, char* envp[])
{
	// variable declarations
	int mdm_iArray[3][5]; // 3 Rows (0, 1, 2) And 5 Columns (0, 1, 2, 3, 4)
	int mdm_int_size;
	int mdm_iArray_size;
	int mdm_iArray_num_elements, mdm_iArray_num_rows, mdm_iArray_num_columns;
	int mdm_i, mdm_j;

	// code
	printf("\n\n");

	mdm_int_size = sizeof(int);

	mdm_iArray_size = sizeof(mdm_iArray);
	printf("The Size Of Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_size);

	mdm_iArray_num_rows = mdm_iArray_size / sizeof(mdm_iArray[0]);
	printf("The Total Number Of Rows In Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_num_rows);

	mdm_iArray_num_columns = sizeof(mdm_iArray[0]) / mdm_int_size;
	printf("The Total Number Of Columns In Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_num_columns);

	mdm_iArray_num_elements = mdm_iArray_num_rows * mdm_iArray_num_columns;
	printf("The Total Number Of Elements In Two Dimensional (2D) Integer Array Is = %d\n\n", mdm_iArray_num_elements);

	printf("\n\n");
	printf("Elements In 2D Array : \n\n");

	// ******** Piece-Meal Assignment ********
	
	// **** Row 1 ****
	mdm_iArray[0][0] = 21;
	mdm_iArray[0][1] = 42;
	mdm_iArray[0][2] = 63;
	mdm_iArray[0][3] = 84;
	mdm_iArray[0][4] = 105;

	// **** Row 2 ****
	mdm_iArray[1][0] = 22;
	mdm_iArray[1][1] = 44;
	mdm_iArray[1][2] = 66;
	mdm_iArray[1][3] = 88;
	mdm_iArray[1][4] = 110;

	// **** Row 3 ****
	mdm_iArray[2][0] = 23;
	mdm_iArray[2][1] = 46;
	mdm_iArray[2][2] = 69;
	mdm_iArray[2][3] = 92;
	mdm_iArray[2][4] = 115;

	// **** Display ****
	for (mdm_i = 0; mdm_i < mdm_iArray_num_rows; mdm_i++)
	{
		printf("***** Row %d *****\n", (mdm_i + 1));

		for (mdm_j = 0; mdm_j < mdm_iArray_num_columns; mdm_j++)
		{
			printf("mdm_iArray[%d][%d] = %d\n", mdm_i, mdm_j, mdm_iArray[mdm_i][mdm_j]);
		}
		printf("\n\n");
	}

	return(0);
}

