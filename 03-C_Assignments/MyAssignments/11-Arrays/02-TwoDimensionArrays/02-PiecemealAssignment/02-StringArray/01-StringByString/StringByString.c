// header file
#include<stdio.h>

// constant value defination
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv, char* envp[])
{
	// function prototype
	void mdm_MyStrcpy(char[], char[]);

	// variable declarations
	/*
		*** A 'string' Is An Array Of Characters... so char[] Is A 'char' Array And Hence, char[] Is A 'string'
		*** An Array Of 'char' Arrays Is An Array Of Strings !!! ***
		*** Hence, char[] Is One 'char' Array And Hence, Is One String ***
		*** Hence, char[] Is One char[][] Is An Array Of char Arrays And Hence, Is An Array Of Strings ***
	*/

	/*
		Here, the string array can allow a maximum number of 10 strings(10 rows) and each of these 10 (rows)strings can have
		only upto 15 characters maximum (15 Columns)
	*/
	char mdm_strArray[5][10]; // 5 Rows (0, 1, 2, 3, 4) -> 5 Strings (Each String Can Have Maximum Of 10 Characters)
	int mdm_char_size;
	int mdm_strArray_size;
	int mdm_strArray_num_elements, mdm_strArray_num_rows, mdm_strArray_num_columns;
	int mdm_i;

	// code
	printf("\n\n");

	mdm_char_size = sizeof(char);

	mdm_strArray_size = sizeof(mdm_strArray);
	printf("The Size Of Two Dimensional (2D) Character Array (String Array) Is = %d\n\n", mdm_strArray_size);

	mdm_strArray_num_rows = mdm_strArray_size / sizeof(mdm_strArray[0]);
	printf("The Number Of Rows Of Two Dimensional (2D) Character Array (String Array) Is = %d\n\n", mdm_strArray_num_rows);

	mdm_strArray_num_columns = sizeof(mdm_strArray[0]) / mdm_char_size;
	printf("The Number Of Columns Of Two Dimensional (2D) Character Array (String Array) Is = %d\n\n", mdm_strArray_num_columns);

	mdm_strArray_num_elements = mdm_strArray_num_rows * mdm_strArray_num_columns;
	printf("The Total Number Of Elements Of Two Dimensional (2D) Character Array (String Array) Is = %d\n\n", mdm_strArray_num_elements);

	// Piece-Meal Assignment
	mdm_MyStrcpy(mdm_strArray[0], "My");
	mdm_MyStrcpy(mdm_strArray[1], "Name");
	mdm_MyStrcpy(mdm_strArray[2], "Is");
	mdm_MyStrcpy(mdm_strArray[3], "Mandar");
	mdm_MyStrcpy(mdm_strArray[4], "Maske");

	printf("\n");
	printf("The Strings In The 2D Character Array Are : \n\n");

	for (mdm_i = 0; mdm_i < mdm_strArray_num_rows; mdm_i++)
		printf("%s ", mdm_strArray[mdm_i]);

	printf("\n\n");

	return(0);
}

void mdm_MyStrcpy(char mdm_str_destination[], char mdm_str_source[])
{
	// function prototype
	int mdm_MyStrlen(char[]);

	// variable declarations
	int mdm_iStringLength = 0;
	int mdm_j;

	// code
	mdm_iStringLength = mdm_MyStrlen(mdm_str_source);
	for (mdm_j = 0; mdm_j < mdm_iStringLength; mdm_j++)
		mdm_str_destination[mdm_j] = mdm_str_source[mdm_j];

	mdm_str_destination[mdm_j] = '\0';

}

int mdm_MyStrlen(char mdm_str[])
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// *** Determining Exact Length Of The String, By Detecting The First Occurence Of Null Terminating Character (\0) ***

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}

