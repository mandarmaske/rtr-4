// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations

	/*
		*** A 'string' Is An Array Of Characters... so char[] Is A 'char' Array And Hence, char[] Is A 'string'
		*** An Array Of 'char' Arrays Is An Array Of Strings !!! ***
		*** Hence, char[] Is One 'char' Array And Hence, Is One String ***
		*** Hence, char[] Is One char[][] Is An Array Of char Arrays And Hence, Is An Array Of Strings ***
	*/

	/*
		Here, the string array can allow a maximum number of 10 strings(10 rows) and each of these 10 (rows)strings can have
		only upto 15 characters maximum (15 Columns)
	*/

	char mdm_strArray[5][10]; // 5 Rows (0, 1, 2, 3, 4) -> 5 Strings (Each String Can Have Maximum Of 10 Characters)
	int mdm_char_size;
	int mdm_strArray_size;
	int mdm_strArray_num_elements, mdm_strArray_num_rows, mdm_strArray_num_columns;
	int mdm_i;

	// code
	printf("\n\n");

	mdm_char_size = sizeof(char);

	mdm_strArray_size = sizeof(mdm_strArray);
	printf("Size Of Two Dimensional (2D) Character Array (String Array) = %d\n\n", mdm_strArray_size);

	mdm_strArray_num_rows = mdm_strArray_size / sizeof(mdm_strArray[0]);
	printf("The Total Number Of Rows Of Two Dimensional (2D) Character Array (String Array) = %d\n\n", mdm_strArray_num_rows);

	mdm_strArray_num_columns = sizeof(mdm_strArray[0]) / mdm_char_size;
	printf("The Total Number Of Columns Of Two Dimensional (2D) Character Array (String Array) = %d\n\n", mdm_strArray_num_columns);

	mdm_strArray_num_elements = mdm_strArray_num_rows * mdm_strArray_num_columns;
	printf("The Total Number OF Elements Of Two Dimensional (2D) Character Array (String Array) = %d\n\n", mdm_strArray_num_elements);

	// **** Piece-Meal Assignment ****

	// **** Row 1 / String 1 ****
	mdm_strArray[0][0] = 'M';
	mdm_strArray[0][1] = 'Y';
	mdm_strArray[0][2] = '\0'; // Null Terminating Character

	// **** Row 2 / String 2 ****
	mdm_strArray[1][0] = 'N';
	mdm_strArray[1][1] = 'A';
	mdm_strArray[1][2] = 'M';
	mdm_strArray[1][3] = 'E';
	mdm_strArray[1][4] = '\0'; // Null Terminating Character

	// **** Row 2 / String 3 ****
	mdm_strArray[2][0] = 'I';
	mdm_strArray[2][1] = 'S';
	mdm_strArray[2][2] = '\0'; // Null Terminating Character

	// **** Row 3 / String 4 ****
	mdm_strArray[3][0] = 'M';
	mdm_strArray[3][1] = 'A';
	mdm_strArray[3][2] = 'N';
	mdm_strArray[3][3] = 'D';
	mdm_strArray[3][4] = 'A';
	mdm_strArray[3][5] = 'R';
	mdm_strArray[3][6] = '\0'; // Null Terminating Character

	// **** Row 4 / String 5 ****
	mdm_strArray[4][0] = 'M';
	mdm_strArray[4][1] = 'A';
	mdm_strArray[4][2] = 'S';
	mdm_strArray[4][3] = 'K';
	mdm_strArray[4][4] = 'E';
	mdm_strArray[4][5] = '\0'; // Null Terminating Character

	printf("\n\n");
	printf("The Strings In The 2D Character Array : \n\n");

	for (mdm_i = 0; mdm_i < mdm_strArray_num_rows; mdm_i++)
		printf("%s ", mdm_strArray[mdm_i]);

	printf("\n\n");

	return(0);
}


