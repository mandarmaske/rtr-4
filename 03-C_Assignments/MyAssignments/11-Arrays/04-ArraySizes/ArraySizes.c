// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArray_One[5];
	int mdm_iArray_Two[5][3];
	int mdm_iArray_Three[100][100][5];

	int mdm_rows_2D;
	int mdm_columns_2D;

	int mdm_rows_3D;
	int mdm_columns_3D;
	int mdm_depths_3D;

	// code
	printf("\n\n");
	printf("The Size Of 1D Integer Array 'mdm_iArray_One' = %lu\n", sizeof(mdm_iArray_One));
	printf("The Number Of elements In 1D Integer Array 'mdm_iArray_One' = %lu\n", (sizeof(mdm_iArray_One) / sizeof(int)));

	printf("\n\n");
	printf("The Size Of 2D Integer Array 'mdm_iArray_Two' = %lu\n", sizeof(mdm_iArray_Two));
	printf("The Number Of Rows In 2D Integer Array 'mdm_iArray_Two' = %lu\n", (sizeof(mdm_iArray_Two) / sizeof(mdm_iArray_Two[0])));
	mdm_rows_2D = sizeof(mdm_iArray_Two) / sizeof(mdm_iArray_Two[0]);

	printf("The Number Of Column In 2D Integer Array 'mdm_iArray_Two' = %lu\n", (sizeof(mdm_iArray_Two[0]) / sizeof(int)));
	mdm_columns_2D = sizeof(mdm_iArray_Two[0]) / sizeof(int);

	printf("The Number Of Elements In Total In 2D Array 'mdm_iArrayTwo' = %d\n", (mdm_rows_2D * mdm_columns_2D));

	printf("\n\n");
	printf("The Size Of 3D Integer Array 'mdm_iArray_Three' = %lu\n", sizeof(mdm_iArray_Three));

	printf("The Number Of Rows In 3D Integer Array 'mdm_iArray_Three' = %lu\n", (sizeof(mdm_iArray_Three) / sizeof(mdm_iArray_Three[0])));
	mdm_rows_3D = sizeof(mdm_iArray_Three) / sizeof(mdm_iArray_Three[0]);

	printf("The Number Of Columns In 3D Integer Array 'mdm_iArray_Three' = %lu\n", (sizeof(mdm_iArray_Three[0]) / sizeof(mdm_iArray_Three[0][0])));
	mdm_columns_3D = sizeof(mdm_iArray_Three[0]) / sizeof(mdm_iArray_Three[0][0]);

	printf("The Number Of Depths In 3D Integer Array 'mdm_iArray_Three' = %lu\n", (sizeof(mdm_iArray_Three[0][0]) / sizeof(mdm_iArray_Three[0][0][0])));
	mdm_depths_3D = sizeof(mdm_iArray_Three[0][0]) / sizeof(mdm_iArray_Three[0][0][0]);

	printf("The Total Number Of Elements In 3D Integer Array 'mdm_iArray_Three' = %lu\n", (mdm_rows_3D * mdm_columns_3D * mdm_depths_3D));
	printf("\n");

	return(0);
}

