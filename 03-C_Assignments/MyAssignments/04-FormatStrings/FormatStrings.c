// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// code

	printf("\n\n");
	printf("*************************************************************************");
	printf("\n\n");

	printf("Hello Everyone!!!\n\n");

	int mdm_a = 23;
	printf("Integer Decimal value of 'mdm_a'     =  %d\n", mdm_a);
	printf("Integer Octal value of 'mdm_a'       =  %o\n", mdm_a);
	printf("Integer Hexadecimal value of 'mdm_a' =  %x\n", mdm_a); // %x - Hexadecimal letters in lowercase 
	printf("Integer Hexadecimal value of 'mdm_a' =  %X\n\n", mdm_a); // %X = Hexadecimal letters in uppercase

	char mdm_ch = 'M';
	printf("Charcter ch = %c\n", mdm_ch);
	char str[] = "\"AstroMediComp Real Time Rendering Batch\"";
	printf("String str = %s\n\n", str);

	long mdm_num = 2147285842L;
	printf("long integer 'mdm_num' = %ld\n\n", mdm_num);

	unsigned int mdm_b = 34;
	printf("Unsigned integer mdm_b = %u\n\n", mdm_b);

	float mdm_f = 3012.199951f;
	printf("Floating point number with just %%f 'mdm_f' = %f\n", mdm_f);
	printf("Floating point number with %%4.2f 'mdm_f' = %4.2f\n", mdm_f);
	printf("Floating point number with %%6.5f 'mdm_f' = %6.5f\n\n", mdm_f);

	double mdm_pi = 3.14159265358979323846;
	printf("Double precision floating point number without exponential = %lf\n", mdm_pi);
	printf("Double precision floating point number with exponential(Lower Case) = %e\n", mdm_pi);
	printf("Double precision floating point number with exponential(Upper Case) = %E\n", mdm_pi);
	printf("Double precision floating point Hexdecimal Number of 'mdm_pi' = %a (Lower Case)\n", mdm_pi);
	printf("Double precision floating point Hexdecimal Number of 'mdm_pi' = %A (Upper Case)\n\n", mdm_pi);

	printf("*************************************************************************");
	printf("\n\n");

	return(0);
}
