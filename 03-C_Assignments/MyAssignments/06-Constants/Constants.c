// header file
#include<stdio.h>

#define MDM_PI 3.1415926535897932

#define MDM_STRING "AstroMediComp"

/*
if First constant is not assigned a value, it is assumed to be 0 i.e : 'SUNDAY' will be 0
And the best part of the constants are assigned consecutive integer values from 0 onwards i.e: 'MONDAY' will be 1, 'TUESDAY' will 2,
and so on...
*/ 

// un-named enums
enum 
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
};

enum
{
	JANUARY = 1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER
};

// named enums
enum NUMBERS
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE = 5,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN
};

enum boolean
{
	TRUE = 1,
	FALSE = 0
};

// entry-point function
int main(void)
{
	// local constant declarations
	const double epsilon = 0.000001;

	// code
	printf("\n\n");
	printf("Local Constant Epsilon = %lf\n\n", epsilon);

	printf("Sunday is day number    = %d\n", SUNDAY);
	printf("MONDAY is day number    = %d\n", MONDAY);
	printf("TUESDAY is day number   = %d\n", TUESDAY);
	printf("WEDNESDAY is day number = %d\n", WEDNESDAY);
	printf("THURSDAY is day number  = %d\n", THURSDAY);
	printf("FRIDAY is a day number  = %d\n", FRIDAY);
	printf("SATURDAY is a day       = %d\n\n", SATURDAY);

	printf("ONE is a enum Number    = %d\n", ONE);
	printf("TWO is a enum Number    = %d\n", TWO);
	printf("THREE is a enum Number  = %d\n", THREE);
	printf("FOUR is a enum Number   = %d\n", FOUR);
	printf("FIVE is a enum Number   = %d\n", FIVE);
	printf("SIX is a enum Number    = %d\n", SIX);
	printf("SEVEN is a enum Number  = %d\n", SEVEN);
	printf("EIGHT is a enum Number  = %d\n", EIGHT);
	printf("NINE is a enum Number   = %d\n", NINE);
	printf("TEN is a enum Number    = %d\n\n", TEN);

	printf("JANUARY is month number   = %d\n", JANUARY);
	printf("FEBRUARY is month number  = %d\n", FEBRUARY);
	printf("MARCH is month number     = %d\n", MARCH);
	printf("APRIL is month number     = %d\n", APRIL);
	printf("MAY is month number       = %d\n", MAY);
	printf("JUNE is month number      = %d\n", JUNE);
	printf("JULY is month number      = %d\n", JULY);
	printf("AUGUST is month number    = %d\n", AUGUST);
	printf("SEPTEMBER is month number = %d\n", SEPTEMBER);
	printf("OCTOBER is month number   = %d\n", OCTOBER);
	printf("NOVEMBER is month number  = %d\n", NOVEMBER);
	printf("DECEMBER is month number  = %d\n\n", DECEMBER);

	printf("Value of TRUE is  = %d\n", TRUE);
	printf("Value of FALSE is = %d\n", FALSE);
	
	printf("MDM_PI Macro value = %.10lf\n\n", MDM_PI);
	printf("Area of Circle of Radius 2 units = %f\n\n", (MDM_PI * 2.0f * 2.0f));
	// pi * r * r = area of circle of radius 'r'

	printf(MDM_STRING);
	printf("\n\n");

	printf("MDM_STRING is : %s\n", MDM_STRING);

	printf("\n\n");
	return(0);
}
