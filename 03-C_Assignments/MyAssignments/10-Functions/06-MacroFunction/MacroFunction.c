// header file
#include<stdio.h>

// Macro Function
#define MAX_NUMBER(mdm_a, mdm_b) ((mdm_a > mdm_b) ? mdm_a : mdm_b)

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iNum1, mdm_iNum2, mdm_iResult;
	float mdm_fNum1, mdm_fNum2, mdm_fResult;

	// code

	// Compairing Integer Values
	printf("\n\n\n");

	printf("Enter a Number for A : ");
	scanf("%d", &mdm_iNum1);
	printf("\n\n");

	printf("Enter an Integer for B : ");
	scanf("%d", &mdm_iNum2);
	printf("\n\n");

	mdm_iResult = MAX_NUMBER(mdm_iNum1, mdm_iNum2);

	printf("The Result of Macro Function MAX_NUMBER() : %d\n", mdm_iResult);

	// Comapiring Floating-Point Values
	printf("\n\n");

	printf("Enter a Floating Point Number for X : ");
	scanf("%f", &mdm_fNum1);
	printf("\n\n");

	printf("Enter a Floating Point Number for Y : ");
	scanf("%f", &mdm_fNum2);
	printf("\n\n");

	mdm_fResult = MAX_NUMBER(mdm_fNum1, mdm_fNum2);

	printf("The Result of Macro Function MAX_NUMBER() : %4.2f\n", mdm_fResult);
	printf("\n\n");

	return(0);
}
