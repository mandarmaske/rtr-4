// header file
#include<stdio.h>

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// variable declarations
	unsigned int mdm_num;

	// function prototype / declaration / signature
	void recursive(unsigned int);

	// code
	printf("\n\n");

	printf("Enter Any Number : ");
	scanf("%d", &mdm_num);

	printf("\n\n");

	printf("Output Of Recursive Function : \n\n");

	recursive(mdm_num);

	return(0);
}

// global scope

void recursive(unsigned int mdm_n)
{
	// code

	if (mdm_n > 0)
	{
		printf("NUM = %d\n\n", mdm_n);
		recursive(mdm_n - 1);
	}
}

// global scope
