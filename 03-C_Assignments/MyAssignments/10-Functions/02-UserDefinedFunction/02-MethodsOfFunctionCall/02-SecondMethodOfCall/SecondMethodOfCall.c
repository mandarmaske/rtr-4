// header file
#include<stdio.h>

// User Defined Functions : Method Of Calling Function 2
// Calling only two function directly in main(), rest of the functions trace their call indirectly to main()

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// function signature / prototype / declarations
	void Display_Information(void);
	void Function_Country(void);

	// code
	Display_Information(); // function call
	Function_Country();

	return(0);
}

// User Defined Functions - Definitions...

void Display_Information(void) // function definition
{
	// function declarations / prototypes / signatures
	void Function_My(void);
	void Function_Name(void);
	void Function_Is(void);
	void Function_FirstName(void);
	void Function_MiddleName(void);
	void Function_Surname(void);
	void Function_OfAMC();

	// code

	// function calls
	Function_My();
	Function_Name();
	Function_Is();
	Function_FirstName();
	Function_MiddleName();
	Function_Surname();
	Function_OfAMC();
}

void Function_My(void) // function definition
{
	// code
	printf("\n\n");

	printf("My ");
}

void Function_Name(void) // function definition
{
	// code
	printf("Name ");
}

void Function_Is(void) // function definition
{
	// code
	printf("Is ");
}

void Function_FirstName(void) // function definition
{
	// code
	printf("Mandar ");
}

void Function_MiddleName(void) // function definition
{
	// code
	printf("Dilip ");
}

void Function_Surname(void) // function definition
{
	// code
	printf("Maske ");
}

void Function_OfAMC(void) // function definition
{
	// code
	printf("Of \"AstroMediComp\"");
}

void Function_Country(void) // function definition
{
	// code
	printf("\n\n");

	printf("I Live In \"INDIA\"\n\n");
}
