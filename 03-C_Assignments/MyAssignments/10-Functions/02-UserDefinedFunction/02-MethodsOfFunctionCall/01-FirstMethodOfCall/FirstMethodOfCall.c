// header file
#include<stdio.h>

// User Defined Function : Method Of Calling Function 1
// Calling All Functions In main() Directly

// entry-point function -> main() -> Valid return type(int) and 3 parameters(int argc, char *argv[], char *envp[])
int main(int argc, char* argv, char* envp[])
{
	// function prototype / signature / declaration
	void MyAddition(void);
	int MySubtraction(void);
	void MyMultiplication(int, int);
	int MyDivision(int, int);

	// variable declarations
	int mdm_result_subtraction;
	int mdm_a_multiplication, mdm_b_multiplication;
	int mdm_a_division, mdm_b_division, mdm_result_division;

	// code

	// Addition
	MyAddition(); // function call

	// Subtraction
	mdm_result_subtraction = MySubtraction(); // function call
	printf("\n\n");
	printf("The Subtraction = %d\n\n", mdm_result_subtraction);

	// Multiplication
	printf("\n\n");

	printf("Enter an integer for A for Multiplication = ");
	scanf("%d", &mdm_a_multiplication);

	printf("Enter an integer for B for Multiplication = ");
	scanf("%d", &mdm_b_multiplication);

	MyMultiplication(mdm_a_multiplication, mdm_b_multiplication); // function call

	// Division
	printf("\n\n");

	printf("Enter an integer for A for Division = ");
	scanf("%d", &mdm_a_division);

	printf("Enter an integer for B for Division = ");
	scanf("%d", &mdm_b_division);

	mdm_result_division = MyDivision(mdm_a_division, mdm_b_division); // function call

	printf("\n\n");
	printf("The Division of %d and %d is %d", mdm_a_division, mdm_b_division, mdm_result_division);

	printf("\n\n");

	return(0);
}

void MyAddition(void) // function definition
{
	// variable declaration
	int mdm_a, mdm_b, mdm_sum;

	// code
	printf("\n\n");

	printf("Enter an integer for A for Addition : ");
	scanf("%d", &mdm_a);

	printf("Enter an integer for B for Addition : ");
	scanf("%d", &mdm_b);

	printf("\n\n");

	mdm_sum = mdm_a + mdm_b;

	printf("The Sum of %d and %d is %d\n\n", mdm_a, mdm_b, mdm_sum);
	printf("\n\n");
}

int MySubtraction(void) // function definition
{
	// variable declaration
	int mdm_a, mdm_b, mdm_subtract;

	// code

	printf("Enter an integer for A for Subtraction : ");
	scanf("%d", &mdm_a);

	printf("Enter an integer for B for Subtraction : ");
	scanf("%d", &mdm_b);

	mdm_subtract = mdm_a - mdm_b;

	return(mdm_subtract);
}

void MyMultiplication(int mdm_a, int mdm_b) // function definition
{
	// variable declaration
	int mdm_multiplication; // local variable to MyMultiplication()

	// code

	printf("\n\n");

	mdm_multiplication = mdm_a * mdm_b;

	printf("The Multiplication of %d and %d is %d", mdm_a, mdm_b, mdm_multiplication);
	printf("\n\n");
}

int MyDivision(int mdm_a, int mdm_b) // function definition
{
	// variable declaration
	int mdm_division_quotient;

	// code
	if (mdm_a < mdm_b)
	{
		mdm_division_quotient = mdm_b / mdm_a;
		printf("The Division of %d and %d is %d", mdm_b, mdm_a, mdm_division_quotient);
	}
	else
		mdm_division_quotient = mdm_a / mdm_b;

	return(mdm_division_quotient);
}
