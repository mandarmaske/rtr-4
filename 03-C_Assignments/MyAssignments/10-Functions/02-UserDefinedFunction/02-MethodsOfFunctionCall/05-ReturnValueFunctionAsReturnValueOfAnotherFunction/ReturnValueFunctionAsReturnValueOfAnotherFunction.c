// header file
#include<stdio.h>

// entry-point function
int main(int argc, char *argv[], char *env[])
{
	// function prototype / declaration / definition
	int MyAddition(int, int);

	// variable declarations
	int mdm_r;
	int mdm_num1, mdm_num2;

	// code
	mdm_num1 = 10;
	mdm_num2 = 20;

	printf("\n\n");
	printf("%d + %d = %d\n", mdm_num1, mdm_num2, MyAddition(mdm_num1, mdm_num2));
	printf("\n\n");

	return(0);
}

int MyAddition(int mdm_a, int mdm_b) // function definition - MyAddition()
{
	// function prototype
	int add(int, int);

	// code
	return(add(mdm_a, mdm_b));
}

int add(int mdm_x, int mdm_y)
{
	// code
	return(mdm_x + mdm_y);
}

