// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype / declaration / signature
	int MyAddition(int, int);

	// variable declarations 
	int mdm_result;
	int mdm_num1, mdm_num2;
	int mdm_num3, mdm_num4;

	// code
	mdm_num1 = 10;
	mdm_num2 = 20;
	mdm_num3 = 30;
	mdm_num4 = 40;

	mdm_result = MyAddition(MyAddition(mdm_num1, mdm_num2), MyAddition(mdm_num3, mdm_num4)); // return value of MyAddition() is sent as parameter to another call to MyAddition()

	printf("\n\n");
	printf("%d + %d + %d + %d = %d\n", mdm_num1, mdm_num2, mdm_num3, mdm_num4, mdm_result);
	printf("\n\n");

	return(0);
}

// function definition of MyAddition() 
int MyAddition(int mdm_a, int mdm_b)
{
	// variable declaration
	int mdm_sum;

	// code
	mdm_sum = mdm_a + mdm_b;

	return(mdm_sum);
}

