// header file
#include<stdio.h>

// User Defined Function : Method Of Calling Function 3
// Calling only ONE function directly in main(), rest of the functions trace their call indirectly to main()

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype / declaration / signature
	void Function_Country(void);

	// code
	Function_Country(); // function call

	return(0);
}

void Function_Country(void) // function definition
{
	// function declaration / prototype / signature
	void Function_OfAMC(void);

	// code
	Function_OfAMC(); // function call

	printf("\n\n");

	printf("I Live In \"INDIA\"");

	printf("\n\n");
}

void Function_OfAMC(void) // function definition
{
	// function prototype / declaration / signature
	void Function_Surname(void);

	// code
	Function_Surname(); // function call

	printf(" Of \"AstroMediComp\"");
}

void Function_Surname(void) // function definition
{
	// function prototype / declaration / signature
	void Function_MiddleName(void);

	// code
	Function_MiddleName(); // function call
	printf(" Maske");
}

void Function_MiddleName(void) // function definition
{
	// function prototype / declaration / signature
	void Function_FirstName(void);

	// code
	Function_FirstName(); // function call
	printf(" Dilip");
}

void Function_FirstName(void) // function definition
{ 
	// function prototype / declaration / signature
	void Function_Is(void);

	// code
	Function_Is(); // function call
	printf(" Mandar");
}

void Function_Is(void) // function definition
{
	// function prototype / declaration / signature
	void Function_Name(void);

	// code
	Function_Name(); // function call
	printf(" Is");
}

void Function_Name(void) // function definition
{
	// function prototype / declaration / signature
	void Function_My(void);

	// code
	Function_My(); // function call
	printf(" Name");
}

void Function_My(void)
{
	// code
	printf("\n\n");
	printf("My");

}

