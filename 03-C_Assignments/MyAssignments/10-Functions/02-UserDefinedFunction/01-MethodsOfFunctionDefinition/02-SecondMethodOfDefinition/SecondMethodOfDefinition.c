// header file
#include<stdio.h>

// entry-point function -> main() -> Valid return type(int) and 3 Parameters (int argc, char *argv[],char *envp[])
int main(int argc, char *argv[], char *envp[])
{
	// function prototype / declaration / signature
	int MyAddition(void);

	// variable declarations : local variables to main()
	int mdm_result;

	// code
	mdm_result = MyAddition(); // function call

	printf("\n\n");
	printf("Sum = %d", mdm_result);
	printf("\n\n");

	return(0);
}

// User Defined Function : Method Of Definition
// Have Return Value, No Parameters

int MyAddition(void)
{
	// variable declarations
	int mdm_a, mdm_b, mdm_sum;

	printf("\n\n");

	printf("Enter An Integer for A : ");
	scanf("%d", &mdm_a);

	printf("Enter An Integer for b : ");
	scanf("%d", &mdm_b);

	mdm_sum = mdm_a + mdm_b;

	return(mdm_sum);
}

