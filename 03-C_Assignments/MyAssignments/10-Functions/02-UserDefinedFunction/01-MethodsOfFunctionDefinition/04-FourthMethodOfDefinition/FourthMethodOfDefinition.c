// header file
#include<stdio.h>

// entry-point function -> main() -> Valid return type(int) and 3 parameters(int argc, char *argv[], char *envp[])
int main(int argc, char* argv[], char* envp[])
{
	// function declaration / prototype / signature
	int MyAddition(int, int);

	// variable declarations : local variables to main()
	int mdm_a, mdm_b, mdm_result;

	// code
	printf("\n\n");

	printf("Enter An Integer For A : ");
	scanf("%d", &mdm_a);

	printf("Enter An Integer For B : ");
	scanf("%d", &mdm_b);

	printf("\n\n");
	mdm_result = MyAddition(mdm_a, mdm_b); // function call
	printf("Sum of %d and %d is %d", mdm_a, mdm_b, mdm_result);
	printf("\n\n");

	return(0);
}

// User Defined Function : Method Of Definition
// Have Return Value And Parameters

int MyAddition(int mdm_x, int mdm_y)
{
	// variable declaration
	int mdm_sum;

	// code
	mdm_sum = mdm_x + mdm_y;
	return(mdm_sum);
}
