// header file
#include<stdio.h>

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// function prototype / declaration / signature
	void MyAddition(int, int);

	// variable declaration
	int mdm_a, mdm_b;

	// code
	printf("\n\n");
	printf("Enter An Integer For A : ");
	scanf("%d", &mdm_a);

	printf("Enter An Integer For B : ");
	scanf("%d", &mdm_b);

	printf("\n\n");
	MyAddition(mdm_a, mdm_b); // function call

	return(0);
}

// User Defined Function : Method Of Definition
// No Return Value, Have Parameters

void MyAddition(int mdm_x, int mdm_y)
{
	// variable declaration
	int mdm_sum;

	mdm_sum = mdm_x + mdm_y;
	printf("The Sum Of %d and %d is %d", mdm_x, mdm_y, mdm_sum);
	printf("\n\n");
}
