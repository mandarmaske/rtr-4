// header file
#include<stdio.h>

// entry-point function -> main() -> Valid Return Type (int) and 3 Parameters (int argc, char *argv[], char * envp[])
int main(int argc, char *argv[], char *envp[])
{
	// function prototype / function declaration / function signature
	void MyAddition(void);

	// code
	MyAddition(); // function call

	return(0);
}

// User Defined Function : Method Of Definition
// No Return Value, No Parameters

void MyAddition(void)
{
	// variable declarations
	int mdm_a, mdm_b, mdm_sum;

	// code
	printf("\n\n");

	printf("Enter An Integer Value For A : ");
	scanf("%d", &mdm_a);

	printf("Enter An Integer Value For B : ");
	scanf("%d", &mdm_b);

	printf("\n\n");

	mdm_sum = mdm_a + mdm_b;

	printf("The Sum %d and %d is %d\n\n", mdm_a, mdm_b, mdm_sum);

}
