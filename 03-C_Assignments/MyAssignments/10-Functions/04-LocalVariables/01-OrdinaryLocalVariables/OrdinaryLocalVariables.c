// header file
#include<stdio.h>

// global scope

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// local scope of main() begins

	// variable declarations
	// 'mdm_a' is Local Variable. It is local to main() only
	int mdm_a = 5;

	// function prototype
	void change_count(void);

	// code
	printf("\n\n");
	printf("A = %d\n\n", mdm_a);

	// Since, local_count is an ordinary local variable of change_count(), it will NOT retain it's value from previous call to change_count();
	// local_count is intialized to 0
	// local_count = local + 1 = 0 + 1 = 1
	change_count();

	return(0);

	// local scope of main() ends
}

// global scope

void change_count(void)
{
	// local scope of change_count() begins

	// variable declarations
	// local_count is a Local Variable. It is local to change_count() only
	int local_count = 0;

	// code
	local_count = local_count + 1;
	printf("Local Count = %d\n\n", local_count);

	// local scope of change_count() ends
}

// global scope 
