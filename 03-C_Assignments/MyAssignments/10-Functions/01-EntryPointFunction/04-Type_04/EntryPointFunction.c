// header file
#include<stdio.h>

// entry-point function -> main() -> valid return type (int) and 2 parameters (int argc, char *argv[])
int main(int argc, char* argv[])
{
	// variable declaration
	int mdm_i;

	// code
	printf("\n\n");
	printf("Hello, World!!!\n\n");
	printf("Number of Command Line Arguments = %d\n\n", argc);

	printf("Command Line Arguments Passed to this program are : \n\n");
	for (mdm_i = 0; mdm_i < argc; mdm_i++)
	{
		printf("Command Line Argument Number %d = %s\n", (mdm_i + 1), argv[mdm_i]);
	}

	printf("\n");

	return(0);
}
