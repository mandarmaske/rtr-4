// header file
#include<stdio.h>

// entry-point functions -> Valid Return Type (int) and 3 Parameters (int argc, char *argv[], char *envp[])
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	int mdm_i;

	// code
	printf("\n\n");
	printf("Hello World!!!\n\n"); // library function

	printf("Number of Command Line Arguments = %d\n\n", argc);
	printf("Command Line Arguments Passed To This Program Are : \n\n");

	for (mdm_i = 0; mdm_i <= argc; mdm_i++)
	{
		printf("Command Line Argument Number %d = %s\n\n", (mdm_i + 1), argv[mdm_i]);
	}
	printf("\n");

	printf("First 20 Environmental Variables Passed To This Program Are : \n\n");

	for (mdm_i = 0; mdm_i < 20; mdm_i++)
	{
		printf("Environmental Variable Number %d = %s\n", (mdm_i + 1), envp[mdm_i]);
	}
	printf("\n\n");

	return(0);
}
