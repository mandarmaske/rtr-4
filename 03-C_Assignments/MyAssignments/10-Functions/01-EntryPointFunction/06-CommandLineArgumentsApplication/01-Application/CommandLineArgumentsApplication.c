// header file
#include<stdio.h>  // 'stdio.h' contains declaration of 'printf()'
#include<ctype.h>  // 'ctype.h' contains declaration of 'atoi()'
#include<stdlib.h> // 'stdlib.h' contains declaration of 'exit()'

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// variable declarations
	int mdm_i;
	int mdm_num;
	int mdm_sum = 0;

	// code
	if (argc == 1)
	{
		printf("\n\n");
		printf("No Numbers Given For Addition !!! Exitting now...\n\n");
		printf("Usage : CommandLineArgumentsApplication <First Number> <Second Number>...\n\n");
		exit(0);
	}

	// This program adds all Command Line Arguments given in integer from only and outputs the Sum
	// Due to use of atoi(), all Command Line Arguments of types other than 'int' are ignored

	printf("\n\n");
	printf("Sum of all integer Command Line Arguments Is : \n\n");

	// loop starts from mdm_i = 1 because, mdm_i = 0 will result in 'argv[mdm_i]' = 'argv[0]' 
    //  which is the name of the program itself i.e CommandLineArgumentsApplication.exe 
	for (mdm_i = 1; mdm_i < argc; mdm_i++)  
	{
		mdm_num = atoi(argv[mdm_i]);
		mdm_sum = mdm_sum + mdm_num;
	}

	printf("Sum = %d\n\n", mdm_sum);

	return(0);
}