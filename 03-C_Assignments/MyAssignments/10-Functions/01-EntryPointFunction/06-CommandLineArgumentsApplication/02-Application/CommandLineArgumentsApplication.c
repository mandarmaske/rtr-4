// header files
#include<stdio.h>
#include<stdlib.h>

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// variable declaration
	int mdm_i;

	// code
	// hi condition false(0) zhali tarach Full Name print hoil, kevha False hoil when argc = 4
	if (argc != 4) // program name + first name + middle name + surname = 4 command line arguments are required
	{
		printf("\n\n");
		printf("Invalid Usage !!! Exitting Now...\n\n");
		printf("Usage : CommandLineArgumentsApplication <first name> <middle name> <surname>\n\n");
		exit(0);
	}

	// this programs prints your full name as entered in the command line arguments
	printf("\n\n");
	printf("Your Full Name Is : ");

	// loop starts from mdm_i = 1 because mdm_i = 0 will result in 'argv[mdm_i]' = 'argv[0]' which is the name of the program itself i.e 'CommandLineArgumentApplication.exe'
	for (mdm_i = 1; mdm_i < argc; mdm_i++)
	{
		printf("%s ", argv[mdm_i]);
	}

	printf("\n\n");

	return(0);
}
