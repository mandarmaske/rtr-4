// header file
#include<stdio.h>

// global scope

// if not initialized by us, global variables are initialized to their zero values(with respect to their data types i.e 0 for int, 0.0 for float and double, etc) by default.
// but still, for good programming discipline, we shall explicitly initialize our global variable with 0

int mdm_global_count = 0;

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// function prototypes
	void change_count_one(void);
	void change_count_two(void);
	void change_count_three(void);

	// code
	printf("\n");

	printf("main() : Value of Global Count = %d\n", mdm_global_count);

	change_count_one();
	change_count_two();
	change_count_three();

	printf("\n");

	return(0);
}

// global scope

void change_count_one(void)
{
	// code
	mdm_global_count = 100;
	printf("change_count_one() : Value of Global Count = %d\n", mdm_global_count);
}

// global scope

void change_count_two(void)
{
	// code
	mdm_global_count = mdm_global_count + 1;
	printf("change_count_two() : Value of Global Count = %d\n", mdm_global_count);
}

// global scope

void change_count_three(void)
{
	// code
	mdm_global_count = mdm_global_count + 10;
	printf("change_count_three() : Values of Global Count = %d\n", mdm_global_count);
}

// global scope
