// header file
#include<stdio.h>

// global scope

// entry-point function
int main(int argc, char *argv[], char *envp[])
{
	// function prototype / declaration / signature
	void change_count(void);

	// variable declaration
	extern int mdm_global_count;

	// code
	printf("\n\n");
	printf("Value of global_count before change_count() = %d\n", mdm_global_count);
	change_count();
	printf("Value of global_count after change_count() = %d\n", mdm_global_count);
	printf("\n\n");

	return(0);
}

// global scope
// global_count as a global variable
// since, it is declared before change_count(), it can be accessed and used as any ordinary global variable in change_count()
// since, it is declared after main(), it must be first re-declared in main() as an external global variable by means of the 'extern' keyword and the type of the variable.
// Once this is done, it can be used as an ordinary global variable in main as well

int mdm_global_count = 0;

void change_count(void)
{
	// code
	mdm_global_count = 5;
	printf("Value of global_count in change_count() = %d\n", mdm_global_count);
}

