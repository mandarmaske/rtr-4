// global scope
// mdm_global_count is global variable declared in source code file ExternalGlobalVariablesInMultiplesFiles.c
// to access it in this file, it must first along with the 'extern' keyword and it's proper datatype
// then, it can be used as any ordinary global variable throughout this file as well

// header file
#include<stdio.h>

extern int mdm_global_count;

void change_count_two(void)
{
	// code
	mdm_global_count = mdm_global_count + 1;
	printf("Change_count_two() : Value of Global Count in File1 = %d\n", mdm_global_count);
}
