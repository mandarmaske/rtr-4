// header file
#include<stdio.h>

// global scope
int mdm_global_count = 0;

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	void change_count_one(void);
	void change_count_two(void);   // function defines in File1.c
	void change_count_three(void); // function defines in File2.c

	// code
	printf("\n\n");

	change_count_one();
	change_count_two();   // function defines in File1.c
	change_count_three(); // function defines in File2.c

	return(0);
}

void change_count_one(void)
{
	// code
	mdm_global_count = mdm_global_count + 1;
	printf("change_count_one() : Value of Global Count in Main File = %d\n", mdm_global_count);
}
