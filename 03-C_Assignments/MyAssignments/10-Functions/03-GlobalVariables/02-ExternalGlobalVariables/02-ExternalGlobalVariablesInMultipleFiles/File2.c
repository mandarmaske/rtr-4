// global scope
// mdm_global_count is global variable declared in source code file ExternalGlobalVariablesInMultiplesFiles.c
// to access it in this file, it must first along with the 'extern' keyword and it's proper datatype
// then, it can be used as any ordinary global variable throughout this file as well
// Here, Re-declaring 'mdm_global_count' as a Local Variable' using 'extern' keyword within change_count_three()

// header file
#include<stdio.h>

void change_count_three(void)
{
	// code
	extern int mdm_global_count;
	mdm_global_count = mdm_global_count + 1;
	printf("change_count_three() : Value of Global Count in File2 = %d\n\n", mdm_global_count);
}