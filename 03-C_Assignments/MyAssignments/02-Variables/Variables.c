 // header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations / definations / initializations
	int mdm_i = 5;
	float mdm_f = 5.60f;			// display as mdm_f = 5.600000
	double mdm_d = 432.3434336;
	char mdm_c = 'M';

	// code
	printf("\n\n");

	printf("\tmdm_i = %d\n", mdm_i);
	printf("\tmdm_f = %1.2f\n", mdm_f);
	printf("\tmdm_d = %lf\n", mdm_d);
	printf("\tmdm_c = %c\n", mdm_c);

	printf("\n\n");

	mdm_i = 20;
	mdm_f = 7.56f;
	mdm_d = 153.67544;
	mdm_c = 'm';

	printf("\tmdm_i = %d\n", mdm_i);
	printf("\tmdm_f = %1.2f\n", mdm_f);
	printf("\tmdm_d = %lf\n", mdm_d);
	printf("\tmdm_c = %c\n", mdm_c);

	printf("\n\n");

	return(0);
}
