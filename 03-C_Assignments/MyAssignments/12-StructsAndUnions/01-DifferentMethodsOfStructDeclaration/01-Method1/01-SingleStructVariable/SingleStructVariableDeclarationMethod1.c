// header file
#include<stdio.h>

// defining struct
struct mdm_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
} mdm_Data; // Declaraing A Single Struct Variable Of Type 'struct mdm_MyData' globally...

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_i_size;
	int mdm_f_size;
	int mdm_d_size;
	int mdm_c_size;
	int struct_mdm_MyData_size;

	// code
	// Assaigning Data Values To The Data Members Of 'struct mdm_MyData'
	mdm_Data.mdm_i = 30;
	mdm_Data.mdm_f = 11.45f;
	mdm_Data.mdm_d = 4.2995;
	mdm_Data.mdm_c = 'M';

	// Displaying Values Of The Data Members
	printf("\n\n");
	printf("Data Memebers Of 'struct mdm_MyData' Are : \n\n");
	printf("mdm_i = %d\n", mdm_Data.mdm_i);
	printf("mdm_f = %f\n", mdm_Data.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data.mdm_d);
	printf("mdm_c = %c\n", mdm_Data.mdm_c);

	// Calculating Sizes (In Bytes) Of The Data Memebers Of 'struct mdm_MyData'
	mdm_i_size = sizeof(mdm_Data.mdm_i);
	mdm_f_size = sizeof(mdm_Data.mdm_f);
	mdm_d_size = sizeof(mdm_Data.mdm_d);
	mdm_c_size = sizeof(mdm_Data.mdm_c);

	// Displaying Sizes (In Bytes) Of The Data Members Of 'struct mdm_MyData'
	printf("\n\n");
	printf("Sizes (In Bytes) Of Data Memebers Of 'struct_MyData' Are : \n\n");
	printf("Size Of 'mdm_i' = %d Bytes\n", mdm_i_size);
	printf("Size Of 'mdm_f' = %d Bytes\n", mdm_f_size);
	printf("Size Of 'mdm_d' = %d Bytes\n", mdm_d_size);
	printf("Size Of 'mdm_c' = %d Bytes\n", mdm_c_size);

	// Calculating Size (In Bytes) Of The Entire 'struct mdm_MyData'
	struct_mdm_MyData_size = sizeof(mdm_Data); // can also give struct name -> sizeof(mdm_MyData)

	// Displaying Sizes (In Bytes) Of The Entire 'struct mdm_MyData'
	printf("\n\n");
	printf("Size Of 'struct mdm_MyData' = %d\n\n", struct_mdm_MyData_size);

	return(0);
}

