// header file
#include<stdio.h>

// defining struct
struct mdm_MyPoint
{
	int mdm_x;
	int mdm_y;
} mdm_Point; // declaraing a single variable of type 'struct mdm_MyPoint' globally...

// defining struct
struct mdm_MyPointProperties
{
	int mdm_quadrant;
	char mdm_axis_location[10];
} mdm_Point_Properties; // declaraing a single variable of type 'mdm_MyPointProperties' globally...

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// code
	// User Input For The Data Memebers Of 'struct_MyPoint' variable 'point_A'
	printf("\n\n");
	printf("Enter X-Coordinate For Point A : ");
	scanf("%d", &mdm_Point.mdm_x);
	printf("Enter Y-Coordinate For Point B : ");
	scanf("%d", &mdm_Point.mdm_y);

	printf("\n\n");
	printf("Point Co-ordinate (mdm_x, mdm_y) Are : (%d, %d) !!!\n\n", mdm_Point.mdm_x, mdm_Point.mdm_y);

	if (mdm_Point.mdm_x == 0 && mdm_Point.mdm_y == 0)
		printf("The Point Is The Origin (%d, %d) !!!\n\n", mdm_Point.mdm_x, mdm_Point.mdm_y);
	else // At least One Of The Two Values (Either 'X' Or 'Y' Or Both) Is A Non-Zero Value...
	{
		if (mdm_Point.mdm_x == 0) // If 'X' Is Zero... Obviously 'Y' Is The Non Zero Value
		{
			if (mdm_Point.mdm_y < 0)
				strcpy(mdm_Point_Properties.mdm_axis_location, "Negative Y");

			if (mdm_Point.mdm_y > 0)
				strcpy(mdm_Point_Properties.mdm_axis_location, "Positive Y");

			mdm_Point_Properties.mdm_quadrant = 0; // A Point Lying On Any Of The Co-ordinate Axes Is Not A Part Of ANY Quadrant...
			printf("The Point Lies On The %s Axis !!!\n\n", mdm_Point_Properties.mdm_axis_location);
		}
		else if (mdm_Point.mdm_y == 0) // If 'Y' Is Zero... Obviously 'X' Is The Non-Zero Value
		{
			if (mdm_Point.mdm_x < 0)
				strcpy(mdm_Point_Properties.mdm_axis_location, "Negative X");

			if (mdm_Point.mdm_x > 0)
				strcpy(mdm_Point_Properties.mdm_axis_location, "Positive X");

			mdm_Point_Properties.mdm_quadrant = 0; // A Point Lying On Any Of The Co-ordinate Axes Is NOT A Part Of Any Quadrant...
			printf("The Point Lies On The %s Axis !!!\n\n", mdm_Point_Properties.mdm_axis_location);
		}
		else // Both 'X' And 'Y' Are Non-Zero
		{
			mdm_Point_Properties.mdm_axis_location[0] = '\0'; // A Point Lying In Any Of The 4 Quadrants Cannot Be Lying On Any Of The Coordinate Axes...

			if (mdm_Point.mdm_x > 0 && mdm_Point.mdm_y > 0)
				mdm_Point_Properties.mdm_quadrant = 1; // 'X' Is +ve and 'Y' Is -ve
			else if (mdm_Point.mdm_x < 0 && mdm_Point.mdm_y > 0)
				mdm_Point_Properties.mdm_quadrant = 2; // 'X' Is -ve And 'Y' Is +ve
			else if (mdm_Point.mdm_x < 0 && mdm_Point.mdm_y < 0)
				mdm_Point_Properties.mdm_quadrant = 3; // 'X' Is -ve And 'Y' Is -ve
			else
				mdm_Point_Properties.mdm_quadrant = 4; // 'X' Is +ve And 'Y' Is -ve

			printf("The Point Lies In Quadrant Number %d !!!\n\n", mdm_Point_Properties.mdm_quadrant);
		}
	}

	return(0);
}