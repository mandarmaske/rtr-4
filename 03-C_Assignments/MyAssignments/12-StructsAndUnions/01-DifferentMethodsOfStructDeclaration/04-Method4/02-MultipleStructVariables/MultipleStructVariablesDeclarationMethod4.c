// header file
#include<stdio.h>

// defining struct
struct mdm_MyPoint
{
	int mdm_x;
	int mdm_y;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	struct mdm_MyPoint mdm_Point_A, mdm_Point_B, mdm_Point_C, mdm_Point_D, mdm_Point_E; // Declaring 5 struct variables of type 'struct mdm_MyPoint' locally...

	// code
	// Assigning Data Values In Data Memebers Of 'struct mdm_MyPoint' variable 'mdm_Point_A'
	mdm_Point_A.mdm_x = 1;
	mdm_Point_A.mdm_y = 7;

	// Assigning Data Values In Data Memebers Of 'struct mdm_MyPoint' variable 'mdm_Point_B'
	mdm_Point_B.mdm_x = 11;
	mdm_Point_B.mdm_y = 0;

	// Assigning Data Values In Data Memebers Of 'struct mdm_MyPoint' variable 'mdm_Point_C'
	mdm_Point_C.mdm_x = 19;
	mdm_Point_C.mdm_y = 40;

	// Assigning Data Values In Data Memebers Of 'struct mdm_MyPoint' variable 'mdm_Point_D'
	mdm_Point_D.mdm_x = 53;
	mdm_Point_D.mdm_y = 2;

	// Assigning Data Values In Data Memebers Of 'struct mdm_MyPoint' variable 'mdm_Point_E'
	mdm_Point_E.mdm_x = 77;
	mdm_Point_E.mdm_y = 66;

	// Displaying Values Of The Data Members Of 'struct mdm_MyPoint' (all variables)
	printf("\n\n");
	printf("The Co-ordinates (mdm_x, mdm_y) Of Point 'A' Are : (%d, %d)\n\n", mdm_Point_A.mdm_x, mdm_Point_A.mdm_y);
	printf("The Co-ordinates (mdm_x, mdm_y) Of Point 'B' Are : (%d, %d)\n\n", mdm_Point_B.mdm_x, mdm_Point_B.mdm_y);
	printf("The Co-ordinates (mdm_x, mdm_y) Of Point 'C' Are : (%d, %d)\n\n", mdm_Point_C.mdm_x, mdm_Point_C.mdm_y);
	printf("The Co-ordinates (mdm_x, mdm_y) Of Point 'D' Are : (%d, %d)\n\n", mdm_Point_D.mdm_x, mdm_Point_D.mdm_y);
	printf("The Co-ordinates (mdm_x, mdm_y) Of Point 'E' Are : (%d, %d)\n\n", mdm_Point_E.mdm_x, mdm_Point_E.mdm_y);

	return(0);
}