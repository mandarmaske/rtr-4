// header file
#include<stdio.h>

// defining struct
struct mdm_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	struct mdm_MyData mdm_Data; // Declaraing a single struct variable of type 'struct mdm_MyData' locally

	// variable declarations
	int mdm_i_size;
	int mdm_f_size;
	int mdm_d_size;
	int struct_mdm_MyData_size;

	// code
	// Assigning Data Values To The Data Memebers Of 'struct mdm_MyData'
	mdm_Data.mdm_i = 30;
	mdm_Data.mdm_f = 11.453f;
	mdm_Data.mdm_d = 1.2995;

	// Displaying Data Values To The Data Members Of 'struct mdm_MyData'
	printf("\n\n");
	printf("Data Members Of 'struct mdm_MyData' Are : \n\n");
	printf("mdm_i = %d\n", mdm_Data.mdm_i);
	printf("mdm_f = %f\n", mdm_Data.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data.mdm_d);

	// Calculating Sizes (In Bytes) Of The Data Members Of 'struct mdm_MyData'
	mdm_i_size = sizeof(mdm_Data.mdm_i);
	mdm_f_size = sizeof(mdm_Data.mdm_f);
	mdm_d_size = sizeof(mdm_Data.mdm_d);

	// Displaying Sizes (In Bytes) Of The Data Members Of 'struct mdm_MyData'
	printf("\n\n");
	printf("Sizes (In Bytes) Of Data Members Of 'struct mdm_MyData' Are : \n\n");
	printf("Size of 'mdm_i' = %d\n", mdm_i_size);
	printf("Size of 'mdm_f' = %d\n", mdm_f_size);
	printf("Size of 'mdm_d' = %d\n", mdm_d_size);

	// Calculating Size (In Bytes) Of The Entire 'struct mdm_MyData'
	struct_mdm_MyData_size = sizeof(struct mdm_MyData); // can also give struct name -> sizeof(mdm_MyData)

	// Displaying Sizes (In Bytes) Of The Entire 'struct mdm_MyData'
	printf("\n\n");
	printf("Size of 'struct mdm_MyData' : %d Bytes\n\n", struct_mdm_MyData_size);

	return(0);
}

