// header file
#include<stdio.h>

// defining struct 
struct mdm_MyPoint
{
	int mdm_x;
	int mdm_y;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	struct mdm_MyPoint mdm_Point_A, mdm_Point_B, mdm_Point_C, mdm_Point_D, mdm_Point_E; // declaraing a 5 struct variables

	// code
	// User Input For The Data Members Of 'struct mdm_MyPoint' variable 'mdm_Point_A'
	printf("\n\n");
	printf("Enter X Co-ordinate For Point 'A' : ");
	scanf("%d", &mdm_Point_A.mdm_x);
	printf("Enter Y Co-ordinate For Point 'A' : ");
	scanf("%d", &mdm_Point_A.mdm_y);

	// User Input For The Data Members Of 'struct mdm_MyPoint' variable 'mdm_Point_B'
	printf("\n\n");
	printf("Enter X Co-ordinate For Point 'B' : ");
	scanf("%d", &mdm_Point_B.mdm_x);
	printf("Enter Y Co-ordinate For Point 'B' : ");
	scanf("%d", &mdm_Point_B.mdm_y);

	// User Input For The Data Members Of 'struct mdm_MyPoint' variable 'mdm_Point_C'
	printf("\n\n");
	printf("Enter X Co-ordinate For Point 'C' : ");
	scanf("%d", &mdm_Point_C.mdm_x);
	printf("Enter Y Co-ordinate For Point 'C' : ");
	scanf("%d", &mdm_Point_C.mdm_y);

	// User Input For The Data Members Of 'struct mdm_MyPoint' variable 'mdm_Point_D'
	printf("\n\n");
	printf("Enter X Co-ordinate For Point 'D' : ");
	scanf("%d", &mdm_Point_D.mdm_x);
	printf("Enter Y Co-ordinate For Point 'D' : ");
	scanf("%d", &mdm_Point_D.mdm_y);

	// User Input For The Data Members Of 'struct mdm_MyPoint' variable 'mdm_Point_E'
	printf("\n\n");
	printf("Enter X Co-ordinate For Point 'E' : ");
	scanf("%d", &mdm_Point_E.mdm_x);
	printf("Enter Y Co-ordinate For Point 'E' : ");
	scanf("%d", &mdm_Point_E.mdm_y);

	// Displaying Values Of Data Members Of 'struct mdm_MyPoint' (All Variables)
	printf("\n\n");
	printf("Co-ordinates (mdm_x, mdm_y) Of Point A Are : (%d, %d)\n\n", mdm_Point_A.mdm_x, mdm_Point_A.mdm_y);
	printf("Co-ordinates (mdm_x, mdm_y) Of Point B Are : (%d, %d)\n\n", mdm_Point_B.mdm_x, mdm_Point_B.mdm_y);
	printf("Co-ordinates (mdm_x, mdm_y) Of Point C Are : (%d, %d)\n\n", mdm_Point_C.mdm_x, mdm_Point_C.mdm_y);
	printf("Co-ordinates (mdm_x, mdm_y) Of Point D Are : (%d, %d)\n\n", mdm_Point_D.mdm_x, mdm_Point_D.mdm_y);
	printf("Co-ordinates (mdm_x, mdm_y) Of Point E Are : (%d, %d)\n\n", mdm_Point_E.mdm_x, mdm_Point_E.mdm_y);

	return(0);
}