// header files
#include<stdio.h>
#include<conio.h>

struct mdm_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	struct mdm_MyData mdm_Data; // declarainga single struct variable

	// code
	// User Input For Values Of Data Members Of 'struct mdm_MyData'
	printf("\n\n");

	printf("Enter Integer Value For Data Member 'mdm_i' of 'struct mdm_MyData mdm_Data' : ");
	scanf("%d", &mdm_Data.mdm_i);

	printf("Enter Floating-Point Value For Data Member 'mdm_f' of 'struct mdm_MyData mdm_Data' : ");
	scanf("%f", &mdm_Data.mdm_f);

	printf("Enter A 'Double' Value For Data Member 'mdm_d' of 'struct mdm_MyData mdm_Data' : ");
	scanf("%lf", &mdm_Data.mdm_d);

	printf("Enter A Character Value For Data Member 'mdm_c' Of 'struct mdm_MyData mdm_Data' : ");
	mdm_Data.mdm_c = getch();
	printf("%c", mdm_Data.mdm_c);

	// Display Values Of Data Members Of 'struct mdm_MyData mdm_Data'
	printf("\n\n");
	printf("mdm_i = %d\n", mdm_Data.mdm_i);
	printf("mdm_f = %f\n", mdm_Data.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data.mdm_d);
	printf("mdm_c = %c\n\n", mdm_Data.mdm_c);

	return(0);
}