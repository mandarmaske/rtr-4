// header file
#include<stdio.h>

// struct definition
struct MDM_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	struct MDM_MyData mdm_data;

	// code
	// Assigning Data Values To The Data Members Of 'struct MDM_MyData'
	mdm_data.mdm_i = 30;
	mdm_data.mdm_f = 11.425f;
	mdm_data.mdm_d = 1.2999;
	mdm_data.mdm_c = 'M';

	// Displaying Values Of The Data Members Of 'struct MDM_MyData'
	printf("\n\n");
	printf("Data Members Of 'struct MDM_MyData' Are : \n\n");
	printf("mdm_i = %d\n", mdm_data.mdm_i);
	printf("mdm_f = %f\n", mdm_data.mdm_f);
	printf("mdm_d = %lf\n", mdm_data.mdm_d);
	printf("mdm_c = %c\n", mdm_data.mdm_c);

	printf("\n\n");
	printf("Addresses Of Data Members Of 'struct MDM_MyData' Are : \n\n");
	printf("'mdm_i' Occupies Addresses  From %p\n", &mdm_data.mdm_i);
	printf("'mdm_f' Occupies Addresses  From %p\n", &mdm_data.mdm_f);
	printf("'mdm_d' Occupies Addresses  From %p\n", &mdm_data.mdm_d);
	printf("'mdm_c' Occupies Addresses  From %p\n\n", &mdm_data.mdm_c);

	printf("Starting Addresses Of 'struct MDM_MyData' variable 'mdm_data' = %p\n\n", &mdm_data);

	return(0);
}

