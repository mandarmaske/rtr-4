// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_length, mdm_breadth, mdm_area;

	// struct definition
	struct MDM_MyPoint
	{
		int mdm_x;
		int mdm_y;
	};
	struct MDM_Rectangle
	{
		struct MDM_MyPoint mdm_Point1;
		struct MDM_MyPoint mdm_Point2;
	};

	// struct variable declaration
	struct MDM_Rectangle mdm_Rect = { {2, 3}, {5, 6} };

	// code
	mdm_length = mdm_Rect.mdm_Point2.mdm_y - mdm_Rect.mdm_Point1.mdm_y;
	if (mdm_length < 0)
		mdm_length = mdm_length * -1;

	mdm_breadth = mdm_Rect.mdm_Point2.mdm_x - mdm_Rect.mdm_Point1.mdm_x;
	if (mdm_breadth < 0)
		mdm_breadth = mdm_breadth * -1;

	mdm_area = mdm_length * mdm_breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d\n\n", mdm_length);
	printf("Breadth Of Rectangle = %d\n\n", mdm_breadth);
	printf("Area Of Rectangle = %d\n\n", mdm_area);

	return(0);
}

