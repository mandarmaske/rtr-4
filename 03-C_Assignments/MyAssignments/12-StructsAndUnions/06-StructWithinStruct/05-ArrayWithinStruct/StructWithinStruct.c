// header file
#include<stdio.h>

// struct definition
struct MDM_MyNumber
{
	int mdm_num;
	int mdm_num_table[10];
};

struct MDM_NumTables
{
	struct MDM_MyNumber mdm_a;
	struct MDM_MyNumber mdm_b;
	struct MDM_MyNumber mdm_c;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	struct MDM_NumTables mdm_tables;
	int mdm_i;

	// code
	mdm_tables.mdm_a.mdm_num = 2;

	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_tables.mdm_a.mdm_num_table[mdm_i] = mdm_tables.mdm_a.mdm_num * (mdm_i + 1);

	printf("\n\n");
	printf("Table Of %d : \n\n", mdm_tables.mdm_a.mdm_num);
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("%d * %d = %d\n", mdm_tables.mdm_a.mdm_num, (mdm_i + 1), mdm_tables.mdm_a.mdm_num_table[mdm_i]);

	mdm_tables.mdm_b.mdm_num = 3;

	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_tables.mdm_b.mdm_num_table[mdm_i] = mdm_tables.mdm_b.mdm_num * (mdm_i + 1);

	printf("\n\n");
	printf("Table Of %d : \n\n", mdm_tables.mdm_b.mdm_num);
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("%d * %d = %d\n", mdm_tables.mdm_b.mdm_num, (mdm_i + 1), mdm_tables.mdm_b.mdm_num_table[mdm_i]);

	mdm_tables.mdm_c.mdm_num = 4;

	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_tables.mdm_c.mdm_num_table[mdm_i] = mdm_tables.mdm_c.mdm_num * (mdm_i + 1);

	printf("\n\n");
	printf("Table Of %d : \n\n", mdm_tables.mdm_c.mdm_num);
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("%d * %d = %d\n", mdm_tables.mdm_c.mdm_num, (mdm_i + 1), mdm_tables.mdm_c.mdm_num_table[mdm_i]);


	return(0);
}

