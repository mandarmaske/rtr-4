// header file
#include<stdio.h>

// struct defination
struct MDM_Rectangle
{
	struct MDM_MyPoint
	{
		int mdm_x;
		int mdm_y;
	} mdm_Point1, mdm_Point2;
} mdm_Rect;

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_length, mdm_breadth, mdm_area;

	// code
	printf("\n\n");
	printf("Enter Leftmost X-Coordinate Of Rectangle : ");
	scanf("%d", &mdm_Rect.mdm_Point1.mdm_x);

	printf("\n\n");
	printf("Enter Bottommost Y-Coordinate Of Rectangle : ");
	scanf("%d", &mdm_Rect.mdm_Point1.mdm_y);

	printf("\n\n");
	printf("Enter RightMost X-Coordinate Of Rectangle : ");
	scanf("%d", &mdm_Rect.mdm_Point2.mdm_x);

	printf("\n\n");
	printf("Enter Topmost Y-Coordinate Of Rectangle : ");
	scanf("%d", &mdm_Rect.mdm_Point2.mdm_y);

	mdm_length = mdm_Rect.mdm_Point2.mdm_y - mdm_Rect.mdm_Point1.mdm_y;
	if (mdm_length < 0)
		mdm_length = mdm_length * -1;

	mdm_breadth = mdm_Rect.mdm_Point2.mdm_x - mdm_Rect.mdm_Point1.mdm_x;
	if (mdm_breadth < 0)
		mdm_breadth = mdm_breadth * -1;

	mdm_area = mdm_length * mdm_breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d\n\n", mdm_length);
	printf("Breadth Of Rectangle = %d\n\n", mdm_breadth);
	printf("Area Of Rectangle = %d\n\n", mdm_area);

	return(0);
}

