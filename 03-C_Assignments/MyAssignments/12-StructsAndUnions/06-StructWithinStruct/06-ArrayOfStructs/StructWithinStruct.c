// header file
#include<stdio.h>

// struct definition
struct MDM_MyNumber
{
	int mdm_num;
	int mdm_num_table[10];
};

struct MDM_NumTables
{
	struct MDM_MyNumber mdm_n;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	struct MDM_NumTables mdm_tables[10]; // An Array Of 10 'struct MDM_NumTables'
	int mdm_i, mdm_j;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
	{
		mdm_tables[mdm_i].mdm_n.mdm_num = (mdm_i + 1);
	}

	for (mdm_i = 0; mdm_i < 10; mdm_i++)
	{
		printf("\n\n");
		printf("Table Of %d : \n\n", mdm_tables[mdm_i].mdm_n.mdm_num);
		for (mdm_j = 0; mdm_j < 10; mdm_j++)
		{
			mdm_tables[mdm_i].mdm_n.mdm_num_table[mdm_j] = mdm_tables[mdm_i].mdm_n.mdm_num * (mdm_j + 1);
			printf("%d * %d = %d\n", mdm_tables[mdm_i].mdm_n.mdm_num, (mdm_j + 1), mdm_tables[mdm_i].mdm_n.mdm_num_table[mdm_j]);
		}
	}

	return(0);
}