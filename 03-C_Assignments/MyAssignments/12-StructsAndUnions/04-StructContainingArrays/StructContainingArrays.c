// header files
#include<stdio.h>
#include<string.h>

// macro declaration
#define MDM_INT_ARRAY_SIZE 10
#define MDM_FLOAT_ARRAY_SIZE 5
#define MDM_CHAR_ARRAY_SIZE 26

#define MDM_NUM_STRINGS 10
#define MDM_MAX_CHARACTERS_PER_STRING 20

#define MDM_ALPHABET_BEGINNING 65 // 'A'

// structs definations
struct MDM_MyData1
{
	int mdm_iArray[MDM_INT_ARRAY_SIZE];
	float mdm_fArray[MDM_FLOAT_ARRAY_SIZE];
};

struct MDM_MyData2
{
	char mdm_chArray[MDM_CHAR_ARRAY_SIZE];
	char mdm_strArray[MDM_NUM_STRINGS][MDM_MAX_CHARACTERS_PER_STRING];
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	struct MDM_MyData1 mdm_data1;
	struct MDM_MyData2 mdm_data2;
	int mdm_i;

	// code
	// **** Piece-Meal Assignment (Hard-Coded) ****
	mdm_data1.mdm_fArray[0] = 0.1f;
	mdm_data1.mdm_fArray[1] = 1.2f;
	mdm_data1.mdm_fArray[2] = 2.3f;
	mdm_data1.mdm_fArray[3] = 3.4f;
	mdm_data1.mdm_fArray[4] = 4.5f;

	// **** Loop Assignment (User Input) ****
	printf("\n\n");
	printf("Enter %d Integers : \n\n", MDM_INT_ARRAY_SIZE);
	for (mdm_i = 0; mdm_i < MDM_INT_ARRAY_SIZE; mdm_i++)
		scanf("%d", &mdm_data1.mdm_iArray[mdm_i]);

	// **** Loop Assignment (Hard-Coded) ****
	printf("\n\n");
	for (mdm_i = 0; mdm_i < MDM_CHAR_ARRAY_SIZE; mdm_i++)
		mdm_data2.mdm_chArray[mdm_i] = (char)(mdm_i + MDM_ALPHABET_BEGINNING);

	// **** Piece-Meal Assignment (Hard-Coded) ****
	strcpy(mdm_data2.mdm_strArray[0], "Welcome !!! ");
	strcpy(mdm_data2.mdm_strArray[1], "This ");
	strcpy(mdm_data2.mdm_strArray[2], "Is ");
	strcpy(mdm_data2.mdm_strArray[3], "ASTROMEDICOMP'S ");
	strcpy(mdm_data2.mdm_strArray[4], "Real ");
	strcpy(mdm_data2.mdm_strArray[5], "Time ");
	strcpy(mdm_data2.mdm_strArray[6], "Rendering ");
	strcpy(mdm_data2.mdm_strArray[7], "Batch ");
	strcpy(mdm_data2.mdm_strArray[8], "Of ");
	strcpy(mdm_data2.mdm_strArray[9], "2021-2023 !!! ");

	// **** Displaying Data Members Of 'struct MDM_MyData1' And Their Values ****
	printf("\n\n");
	printf("Members Of 'struct MDM_MyData1' Along With Their Assigned Values Are : \n\n");

	printf("\n\n");
	printf("Integer Array (mdm_data1.mdm_iArray[]) : \n\n");
	for (mdm_i = 0; mdm_i < MDM_INT_ARRAY_SIZE; mdm_i++)
		printf("mdm_data1.mdm_iArray[%d] = %d\n", mdm_i, mdm_data1.mdm_iArray[mdm_i]);

	printf("\n\n");
	printf("Floating-Point Array (mdm_data1.mdm_fArray[]) : \n\n");
	for (mdm_i = 0; mdm_i < MDM_FLOAT_ARRAY_SIZE; mdm_i++)
		printf("mdm_data1.mdm_fArray[%d] = %f\n", mdm_i, mdm_data1.mdm_fArray[mdm_i]);

	// **** Displaying Data Memebers Of 'struct MDM_MyData2' And Their Values ****
	printf("\n\n");
	printf("Members Of 'struct MDM_MyData2' Along With Their Assigned Values Are : \n\n");

	printf("\n\n");
	printf("Characters Array (mdm_data2.mdm_chArray[]) : \n\n");
	for (mdm_i = 0; mdm_i < MDM_CHAR_ARRAY_SIZE; mdm_i++)
		printf("%c ", mdm_data2.mdm_chArray[mdm_i]);
	printf("\n\n");

	printf("\n\n");
	printf("String Array (mdm_data2.mdm_strArray[]) : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_STRINGS; mdm_i++)
		printf("%s", mdm_data2.mdm_strArray[mdm_i]);

	printf("\n\n");

	return(0);
}

