// header file
#include<stdio.h>

// defining struct
struct mdm_MyData
{
	int mdm_i; 
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

struct mdm_MyData mdm_Data = { 9, 8.2f, 9.87654, 'L' };

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// code
	// Display Values Of The Data Members Of 'struct mdm_MyData mdm_Data'
	printf("\n\n");
	printf("Data Members Of 'struct mdm_MyData' Are : \n\n");
	printf("mdm_i = %d\n", mdm_Data.mdm_i);
	printf("mdm_f = %f\n", mdm_Data.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data.mdm_d);
	printf("mdm_c = %c\n\n", mdm_Data.mdm_c);

	return(0);
}

