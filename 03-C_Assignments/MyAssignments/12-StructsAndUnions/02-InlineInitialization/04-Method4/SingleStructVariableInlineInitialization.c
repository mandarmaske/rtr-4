// header file
#include<stdio.h>

// defining struct
struct mdm_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations

	// 35 will be assigned to 'mdm_i' of 'mdm_Data1'
	// 3.9f will be assigned to 'mdm_f' of 'mdm_Data1'
	// 1.23456 will be assigned to 'mdm_d' of 'mdm_Data1'
	// 'V' will assigned to 'mdm_c' of 'mdm_Data1'
	struct mdm_MyData mdm_Data1 = { 25, 3.9f, 1.23456, 'V' }; // Inline Initialization

	// 'P' will assigned to 'mdm_i' of 'mdm_Data2'... but 'P' is a character (char) and 'mdm_i' is an 'int'... so 'P' Is Converted into it's decimal integer ASCII value (80) and (80) is assigned to 'mdm_i' of 'mdm_Data2'
	// 6.2f will be assigned to 'mdm_f' of 'mdm_Data2'
	// 12.199523 will be assigned to 'mdm_d' of 'mdm_Data2'
	// 68 will be assigned to 'mdm_c' of 'mdm_Data2'... But 68 is an integer (int) and 'mdm_c' is a 'char'... so 68 is considered as a decimal ASCII Value and it's Corresponding Character ('D') is assigned to 'mdm_c' of 'mdm_Data2'
	struct mdm_MyData mdm_Data2 = { 'P', 6.2f, 12.199523, 68 }; // Inline Initialization

	// 36 will be assigned to 'mdm_i' of mdm_Data3
	// 'G' is 'char', but 'mdm_f' of 'mdm_Data3' is 'float'... hence, 'G' is convert to it's decimal integer ASCII Value (71) and this value is converted to 'mdm_f' of 'mdm_Data3'
	// 0.000000 will be assigned to 'mdm_d' of 'mdm_Data3'
	// No Character will be assigned to 'mdm_c' of 'mdm_Data3'
	struct mdm_MyData mdm_Data3 = { 36, 'G' }; // Inline Initialization

	// 79 will be assigned to 'mdm_i' of 'mdm_Data4'
	// 0.000000 will be assigned to 'mdm_f' of 'mdm_Data4'
	// 0.000000 will be assigned to 'mdm_d' of 'mdm_Data4'
	// No Character will be assigned to 'mdm_c' of 'mdm_Data4'
	struct mdm_MyData mdm_Data4 = { 79 }; // Inline Initialization

	// code
	// Displaying Values Of The Data Members Of 'struct mdm_MyData mdm_Data1'
	printf("\n\n");
	printf("Data Members Of 'struct mdm_MyData mdm_Data1' Are : \n\n");
	printf("mdm_i = %d\n", mdm_Data1.mdm_i);
	printf("mdm_f = %f\n", mdm_Data1.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data1.mdm_d);
	printf("mdm_c = %c\n", mdm_Data1.mdm_c);

	printf("\n\n");
	printf("Data Members Of 'struct mdm_MyData mdm_Data2' Are : \n\n");
	printf("mdm_i = %d\n", mdm_Data2.mdm_i);
	printf("mdm_f = %f\n", mdm_Data2.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data2.mdm_d);
	printf("mdm_c = %c\n", mdm_Data2.mdm_c);

	printf("\n\n");
	printf("Data Members Of 'struct mdm_MyData mdm_Data3' Are : \n\n");
	printf("mdm_i = %d\n", mdm_Data3.mdm_i);
	printf("mdm_f = %f\n", mdm_Data3.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data3.mdm_d);
	printf("mdm_c = %c\n", mdm_Data3.mdm_c);

	printf("\n\n");
	printf("Data Members Of 'struct mdm_MyData mdm_Data4' Are : \n\n");
	printf("mdm_i = %d\n", mdm_Data4.mdm_i);
	printf("mdm_f = %f\n", mdm_Data4.mdm_f);
	printf("mdm_d = %lf\n", mdm_Data4.mdm_d);
	printf("mdm_c = %c\n\n", mdm_Data4.mdm_c);

	return(0);
}

