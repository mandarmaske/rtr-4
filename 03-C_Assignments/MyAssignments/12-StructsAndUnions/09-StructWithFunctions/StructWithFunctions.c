// header file
#include<stdio.h>

// struct definition
struct MDM_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	struct MDM_MyData MDM_AddStructMembers(struct MDM_MyData, struct MDM_MyData, struct MDM_MyData);

	// variable declarations
	struct MDM_MyData mdm_data1, mdm_data2, mdm_data3, mdm_answer_data;

	// code
	// 
	// **** mdm_data1 ****
	printf("\n\n\n\n");
	printf("******** Data1 ********\n\n");
	printf("Enter Integer Value For 'mdm_i' Of 'struct MDM_MyData data1' : ");
	scanf("%d", &mdm_data1.mdm_i);

	printf("\n\n");
	printf("Enter Floating-Point Value For 'mdm_f' Of 'struct MDM_MyData mdm_data1' : ");
	scanf("%f", &mdm_data1.mdm_f);

	printf("\n\n");
	printf("Enter 'Double' Value For 'mdm_d' Of 'struct MDM_MyData mdm_data1' : ");
	scanf("%lf", &mdm_data1.mdm_d);

	printf("\n\n");
	printf("Enter Character Value For 'mdm_c' Of 'struct MDM_MyData mdm_data1' : ");
	mdm_data1.mdm_c = getch();
	printf("%c", mdm_data1.mdm_c);
	
	// **** mdm_data2 ****
	printf("\n\n\n\n");
	printf("******** Data2 ********\n\n");
	printf("Enter Integer Value For 'mdm_i' Of 'struct MDM_MyData data2' : ");
	scanf("%d", &mdm_data2.mdm_i);

	printf("\n\n");
	printf("Enter Floating-Point Value For 'mdm_f' Of 'struct MDM_MyData mdm_data2' : ");
	scanf("%f", &mdm_data2.mdm_f);

	printf("\n\n");
	printf("Enter 'Double' Value For 'mdm_d' Of 'struct MDM_MyData mdm_data2' : ");
	scanf("%lf", &mdm_data2.mdm_d);

	printf("\n\n");
	printf("Enter Character Value For 'mdm_c' Of 'struct MDM_MyData mdm_data2' : ");
	mdm_data2.mdm_c = getch();
	printf("%c", mdm_data2.mdm_c);

	// **** mdm_data3 ****
	printf("\n\n\n\n");
	printf("******** Data3 ********\n\n");
	printf("Enter Integer Value For 'mdm_i' Of 'struct MDM_MyData data3' : ");
	scanf("%d", &mdm_data3.mdm_i);

	printf("\n\n");
	printf("Enter Floating-Point Value For 'mdm_f' Of 'struct MDM_MyData mdm_data3' : ");
	scanf("%f", &mdm_data3.mdm_f);

	printf("\n\n");
	printf("Enter 'Double' Value For 'mdm_d' Of 'struct MDM_MyData mdm_data3' : ");
	scanf("%lf", &mdm_data3.mdm_d);

	printf("\n\n");
	printf("Enter Character Value For 'mdm_c' Of 'struct MDM_MyData mdm_data3' : ");
	mdm_data3.mdm_c = getch();
	printf("%c", mdm_data3.mdm_c);
	

	// **** Calling Function MDM_AddStructMembers() Which Accepts Three Variables Of  Type 'struct MDM_MyData' As Parameter And Adds Up The Respective Members And Returns The Answer In Another struct of Same Type ****
	mdm_answer_data = MDM_AddStructMembers(mdm_data1, mdm_data2, mdm_data3);

	printf("\n\n\n\n");
	printf("******** Answer ********\n\n");
	printf("mdm_answer_data.mdm_i = %d\n", mdm_answer_data.mdm_i);
	printf("mdm_answer_data.mdm_f = %f\n", mdm_answer_data.mdm_f);
	printf("mdm_answer_data.mdm_d = %lf\n\n", mdm_answer_data.mdm_d);
	
	mdm_answer_data.mdm_c = mdm_data1.mdm_c;
	printf("answer_data.c (from data1) = %c\n\n", mdm_answer_data.mdm_c);

	mdm_answer_data.mdm_c = mdm_data2.mdm_c;
	printf("answer_data.c (from data2) = %c\n\n", mdm_answer_data.mdm_c);

	mdm_answer_data.mdm_c = mdm_data3.mdm_c;
	printf("answer_data.c (from data3) = %c\n\n", mdm_answer_data.mdm_c);

	return(0);
}

struct MDM_MyData MDM_AddStructMembers(struct MDM_MyData mdm_One, struct MDM_MyData mdm_Two, struct MDM_MyData mdm_Three)
{
	// variable declarations
	struct MDM_MyData mdm_answer;

	// code
	mdm_answer.mdm_i = mdm_One.mdm_i + mdm_Two.mdm_i + mdm_Three.mdm_i;
	mdm_answer.mdm_f = mdm_One.mdm_f + mdm_Two.mdm_f + mdm_Three.mdm_f;
	mdm_answer.mdm_d = mdm_One.mdm_d + mdm_Two.mdm_d + mdm_Three.mdm_f;

	return(mdm_answer);
}

