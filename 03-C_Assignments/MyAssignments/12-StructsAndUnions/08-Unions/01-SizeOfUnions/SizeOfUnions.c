// header file
#include<stdio.h>

// struct definition
struct MDM_MyStruct
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

union MDM_MyUnion
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	struct MDM_MyStruct mdm_s;
	union MDM_MyUnion mdm_u;

	// code
	printf("\n\n");
	printf("Size Of 'struct MDM_MyStruct mdm_s' = %lu\n", sizeof(mdm_s));
	printf("Size Of 'struct MDM_MyUnion mdm_u' = %lu\n\n", sizeof(mdm_u));

	return(0);
}
