// header file
#include<stdio.h>

// struct definition
struct MDM_MyStruct
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

union MDM_MyUnion
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	struct MDM_MyStruct mdm_s;
	union MDM_MyUnion mdm_u;

	// code
	printf("\n\n");
	printf("Members Of Struct Are : \n\n");

	mdm_s.mdm_i = 9;
	mdm_s.mdm_f = 8.7f;
	mdm_s.mdm_d = 1.234567;
	mdm_s.mdm_c = 'M';

	printf("mdm_s.mdm_i = %d\n\n", mdm_s.mdm_i);
	printf("mdm_s.mdm_f = %f\n\n", mdm_s.mdm_f);
	printf("mdm_s.mdm_d = %lf\n\n", mdm_s.mdm_d);
	printf("mdm_s.mdm_c = %c\n\n", mdm_s.mdm_c);

	printf("Addresses Of Members Of Struct Are : \n\n");
	printf("mdm_s.mdm_i = %p\n\n", &mdm_s.mdm_i);
	printf("mdm_s.mdm_f = %p\n\n", &mdm_s.mdm_f);
	printf("mdm_s.mdm_d = %p\n\n", &mdm_s.mdm_d);
	printf("mdm_s.mdm_c = %p\n\n", &mdm_s.mdm_c);

	printf("struct MDM_MyStruct mdm_s = %p (Address)\n\n", &mdm_s);

	printf("\n\n");
	printf("Members Of Union Are  : \n\n");

	mdm_u.mdm_i = 3;
	mdm_u.mdm_f = 4.5f;
	mdm_u.mdm_d = 5.12764;
	mdm_u.mdm_c = 'A';

	printf("mdm_u.mdm_i = %d\n\n", mdm_u.mdm_i);
	printf("mdm_u.mdm_f = %f\n\n", mdm_u.mdm_f);
	printf("mdm_u.mdm_d = %lf\n\n", mdm_u.mdm_d);
	printf("mdm_u.mdm_c = %c\n\n", mdm_u.mdm_c);

	printf("Addresses Of Members Of Union u1 Are : \n\n");
	printf("mdm_u.mdm_i = %p\n\n", &mdm_u.mdm_i);
	printf("mdm_u.mdm_f = %p\n\n", &mdm_u.mdm_f);
	printf("mdm_u.mdm_d = %p\n\n", &mdm_u.mdm_d);
	printf("mdm_u.mdm_c = %p\n\n", &mdm_u.mdm_c);

	printf("struct MDM_MyUnion mdm_u = %p (Address)\n\n", &mdm_u);

	return(0);
}
