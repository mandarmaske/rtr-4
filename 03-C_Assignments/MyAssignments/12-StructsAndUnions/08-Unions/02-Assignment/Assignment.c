// header file
#include<stdio.h>

union MDM_MyUnion
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
	char mdm_c;
};

int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	union MDM_MyUnion mdm_u1, mdm_u2;

	// code

	// **** MDM_MyUnion mdm_u1 ****
	printf("\n\n");
	printf("Members Of Union mdm_u1 Are  : \n\n");

	mdm_u1.mdm_i = 6;
	mdm_u1.mdm_f = 1.2f;
	mdm_u1.mdm_d = 8.333333;
	mdm_u1.mdm_c = 'S';

	printf("mdm_u1.mdm_i = %d\n\n", mdm_u1.mdm_i);
	printf("mdm_u1.mdm_f = %f\n\n", mdm_u1.mdm_f);
	printf("mdm_u1.mdm_d = %lf\n\n", mdm_u1.mdm_d);
	printf("mdm_u1.mdm_c = %c\n\n", mdm_u1.mdm_c);

	printf("Addresses Of Members Of Union u1 Are : \n\n");
	printf("mdm_u1.mdm_i = %p\n\n", &mdm_u1.mdm_i);
	printf("mdm_u1.mdm_f = %p\n\n", &mdm_u1.mdm_f);
	printf("mdm_u1.mdm_d = %p\n\n", &mdm_u1.mdm_d);
	printf("mdm_u1.mdm_c = %p\n\n", &mdm_u1.mdm_c);

	printf("MDM_MyUnion mdm_u1 = %p\n\n", &mdm_u1);

	// **** MDM_MyUnion mdm_u2 ****
	printf("\n\n");
	printf("Members Of Union mdm_u2 Are  : \n\n");
	
	mdm_u2.mdm_i = 3;
	mdm_u2.mdm_f = 4.5f;
	mdm_u2.mdm_d = 5.12345;
	mdm_u2.mdm_c = 'A';

	printf("mdm_u2.mdm_i = %d\n\n", mdm_u2.mdm_i);
	printf("mdm_u2.mdm_f = %f\n\n", mdm_u2.mdm_f);
	printf("mdm_u2.mdm_d = %lf\n\n", mdm_u2.mdm_d);
	printf("mdm_u2.mdm_c = %c\n\n", mdm_u2.mdm_c);

	printf("Addresses Of Members Of Union u2 Are : \n\n");
	printf("mdm_u2.mdm_i = %p\n\n", &mdm_u2.mdm_i);
	printf("mdm_u2.mdm_f = %p\n\n", &mdm_u2.mdm_f);
	printf("mdm_u2.mdm_d = %p\n\n", &mdm_u2.mdm_d);
	printf("mdm_u2.mdm_c = %p\n\n", &mdm_u2.mdm_c);

	printf("MDM_MyUnion mdm_u2 = %p\n\n", &mdm_u2);

	return(0);
}

