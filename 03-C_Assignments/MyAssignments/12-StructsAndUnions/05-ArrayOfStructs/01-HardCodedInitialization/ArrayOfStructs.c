// header file
#include<stdio.h>
#include<string.h>

// macros defination
#define MDM_NAME_LENGTH 100
#define MDM_MARITAL_STATUS 10

// struct defination
struct MDM_Employee
{
	char mdm_name[MDM_NAME_LENGTH];
	int mdm_age;
	float mdm_salary;
	char mdm_sex;
	char mdm_marital_status[MDM_MARITAL_STATUS];
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	struct MDM_Employee mdm_EmployeeRecord[5]; // An Array Of 5 Structs - Each Being Type 'struct MDM_Employee'

	char mdm_employee_1[] = "Rajesh";
	char mdm_employee_2[] = "Sameer";
	char mdm_employee_3[] = "Kalyani";
	char mdm_employee_4[] = "Sonali";
	char mdm_employee_5[] = "Shantanu";

	int mdm_i;


	// Code
	// **** Hard-Coded Initialization Of Array Of 'struct MDM_Employee' ****

	// **** Employee 1 **** 
	strcpy(mdm_EmployeeRecord[0].mdm_name, mdm_employee_1);
	mdm_EmployeeRecord[0].mdm_age = 30;
	mdm_EmployeeRecord[0].mdm_sex = 'M';
	mdm_EmployeeRecord[0].mdm_salary = 50000.0f;
	strcpy(mdm_EmployeeRecord[0].mdm_marital_status, "Unmarried");

	// **** Employee 2 ****
	strcpy(mdm_EmployeeRecord[1].mdm_name, mdm_employee_2);
	mdm_EmployeeRecord[1].mdm_age = 32;
	mdm_EmployeeRecord[1].mdm_sex = 'M';
	mdm_EmployeeRecord[1].mdm_salary = 60000.0f;
	strcpy(mdm_EmployeeRecord[1].mdm_marital_status, "Married");

	// **** Employee 3 ****
	strcpy(mdm_EmployeeRecord[2].mdm_name, mdm_employee_3);
	mdm_EmployeeRecord[2].mdm_age = 29;
	mdm_EmployeeRecord[2].mdm_sex = 'F';
	mdm_EmployeeRecord[2].mdm_salary = 62000.0f;
	strcpy(mdm_EmployeeRecord[2].mdm_marital_status, "Unmarried");

	// **** Employee 4 ****
	strcpy(mdm_EmployeeRecord[3].mdm_name, mdm_employee_4);
	mdm_EmployeeRecord[3].mdm_age = 33;
	mdm_EmployeeRecord[3].mdm_sex = 'F';
	mdm_EmployeeRecord[3].mdm_salary = 55000.0f;
	strcpy(mdm_EmployeeRecord[3].mdm_marital_status, "Married");

	// **** Employee 5 ****
	strcpy(mdm_EmployeeRecord[4].mdm_name, mdm_employee_5);
	mdm_EmployeeRecord[4].mdm_age = 35;
	mdm_EmployeeRecord[4].mdm_sex = 'M';
	mdm_EmployeeRecord[4].mdm_salary = 50000.0f;
	strcpy(mdm_EmployeeRecord[4].mdm_marital_status, "Married");

	// **** Display ****
	printf("\n\n");
	printf("******** Displaying Employee Records ********\n\n");
	for (mdm_i = 0; mdm_i < 5; mdm_i++)
	{
		printf("******** Employee Number %d ********\n\n", (mdm_i + 1));
		printf("Name               : %s\n", mdm_EmployeeRecord[mdm_i].mdm_name);
		printf("Age                : %d\n", mdm_EmployeeRecord[mdm_i].mdm_age);

		if ((mdm_EmployeeRecord[mdm_i].mdm_sex == 'M') || (mdm_EmployeeRecord[mdm_i].mdm_sex == 'm'))
			printf("Sex                : Male\n");
		else 
			printf("Sex                : Female\n");

		printf("Salary             : Rs. %f\n", mdm_EmployeeRecord[mdm_i].mdm_salary);
		printf("Marital Status     : %s\n", mdm_EmployeeRecord[mdm_i].mdm_marital_status);

		printf("\n\n");

	}

	return(0);
}

