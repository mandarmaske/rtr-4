// header files
#include<stdio.h>
#include<ctype.h>
#include<string.h>

// macro defination
#define MDM_MAX_STRING_LENGTH 1024

// struct defination
struct MDM_CharacterCount
{
	char mdm_ch;
	int mdm_chCount;
} mdm_character_and_count[] = { {'A', 0}, // mdm_charcter_and_count[0].mdm_ch = 'A' & mdm_character_and_count[0].mdm_chCount = 0
								{'B', 0}, // mdm_charcter_and_count[1].mdm_ch = 'B' & mdm_character_and_count[1].mdm_chCount = 0
								{'C', 0}, // mdm_charcter_and_count[2].mdm_ch = 'C' & mdm_character_and_count[2].mdm_chCount = 0
								{'D', 0},
								{'E', 0},
								{'F', 0},
								{'G', 0},
								{'H', 0},
								{'I', 0},
								{'J', 0},
								{'K', 0},
								{'L', 0},
								{'M', 0},
								{'N', 0},
								{'O', 0},
								{'P', 0},
								{'Q', 0},
								{'R', 0},
								{'S', 0},
								{'T', 0},
								{'U', 0},
								{'V', 0},
								{'W', 0},
								{'X', 0}, 
								{'Y', 0},
								{'Z', 0}}; // mdm_charcter_and_count[25].mdm_ch = 'Z' & mdm_character_and_count[25].mdm_chCount = 0

#define MDM_SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS sizeof(mdm_character_and_count)
#define MDM_SIZE_OF_ONE_STRUCT_FROM_THE_ARRRAY_OF_STRUCTS sizeof(mdm_character_and_count[0])
#define MDM_NUM_ELEMENTS_IN_ARRAY (MDM_SIZE_OF_ENTIRE_ARRAY_OF_STRUCTS / MDM_SIZE_OF_ONE_STRUCT_FROM_THE_ARRRAY_OF_STRUCTS)

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	char mdm_str[MDM_MAX_STRING_LENGTH];
	int mdm_i, mdm_j, mdm_actual_string_length;

	// code
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(mdm_str, MDM_MAX_STRING_LENGTH);

	mdm_actual_string_length = strlen(mdm_str);

	printf("\n\n");
	printf("The String You Have Entered Is : \n\n");
	printf("%s\n\n", mdm_str);

	for (mdm_i = 0; mdm_i < mdm_actual_string_length; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_ELEMENTS_IN_ARRAY; mdm_j++) // Run Every Character Of The Input String Through The Entire Alphabet (A To Z)
		{
			mdm_str[mdm_i] = toupper(mdm_str[mdm_i]); // If Input Character Is In Lower Case, Turn It To Upper Case For Comparison

			if (mdm_str[mdm_i] == mdm_character_and_count[mdm_j].mdm_ch) // If Character Is Present 
				mdm_character_and_count[mdm_j].mdm_chCount++; // Increment Its Count By 1
		}
	}

	printf("\n\n");
	printf("The Number Of Occurences Of All Characters From Alphabet Are As Follows : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ELEMENTS_IN_ARRAY; mdm_i++)
	{
		printf("Character %c = %d\n", mdm_character_and_count[mdm_i].mdm_ch, mdm_character_and_count[mdm_i].mdm_chCount);
	}

	printf("\n\n");

	return(0);
}



