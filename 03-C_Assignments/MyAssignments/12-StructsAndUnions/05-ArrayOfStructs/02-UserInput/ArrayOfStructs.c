// header file
#include <stdio.h>
#include <ctype.h>

// macro defination
#define MDM_NUM_EMPLOYEES 5 // Simply Change This constant value to have as many number of Employee Records as you please...
#define MDM_NAME_LENGTH 100

// struct defination
struct MDM_Employee
{
	char mdm_name[MDM_NAME_LENGTH];
	int mdm_age;
	char mdm_sex;
	float mdm_salary;
	char mdm_marital_status;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype / declaration / signature
	void MDM_GetMyString(char[], int);

	// variable declarations
	struct MDM_Employee mdm_EmployeeRecord[MDM_NUM_EMPLOYEES];// An Array Of <MDM_NUM_EMPLOYEES> structs - Each Being Type Of 'struct MDM_Employee'
	int mdm_i;

	// code
	// ******** User Input Initialization Of Array Of 'struct MDM_Employee' ********
	for (mdm_i = 0; mdm_i < MDM_NUM_EMPLOYEES; mdm_i++)
	{
		printf("\n\n\n\n");
		printf("******** Data Entry For Employee Number %d ********\n", (mdm_i + 1));

		printf("\n\n");
		printf("Enter Employee Name : ");
		MDM_GetMyString(mdm_EmployeeRecord[mdm_i].mdm_name, MDM_NAME_LENGTH);

		printf("\n\n\n");
		printf("Enter Employee's Age (In Years) : ");
		scanf("%d", &mdm_EmployeeRecord[mdm_i].mdm_age);

		printf("\n\n");
		printf("Enter Employee's Sex (M/m For Male, F/f For Female) : ");
		mdm_EmployeeRecord[mdm_i].mdm_sex = getch();
		mdm_EmployeeRecord[mdm_i].mdm_sex = toupper(mdm_EmployeeRecord[mdm_i].mdm_sex);
		printf("%c", mdm_EmployeeRecord[mdm_i].mdm_sex);

		printf("\n\n\n");
		printf("Enter Employee's Salary (In Indiam Rupees) : ");
		scanf("%f", &mdm_EmployeeRecord[mdm_i].mdm_salary);

		printf("\n\n");
		printf("Is The Employee Married? (Y/y For Yes, N/n For No) : ");
		mdm_EmployeeRecord[mdm_i].mdm_marital_status = getch();
		mdm_EmployeeRecord[mdm_i].mdm_marital_status = toupper(mdm_EmployeeRecord[mdm_i].mdm_marital_status);
		printf("%c", mdm_EmployeeRecord[mdm_i].mdm_marital_status);
	}

	// **** Display ****
	printf("\n\n\n\n");
	printf("******** Displaying Employee Records ********\n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_EMPLOYEES; mdm_i++)
	{
		printf("******** Employee Number %d ********\n\n", (mdm_i + 1));
		printf("Name               : %s\n", mdm_EmployeeRecord[mdm_i].mdm_name);
		printf("Age                : %d\n", mdm_EmployeeRecord[mdm_i].mdm_age);

		if (mdm_EmployeeRecord[mdm_i].mdm_sex == 'M')
			printf("Sex                : Male\n");
		else if (mdm_EmployeeRecord[mdm_i].mdm_sex == 'F')
			printf("Sex                : Female\n");
		else
			printf("Sex                : Invalid Data Entered\n");

		printf("Salary             : Rs. %f\n", mdm_EmployeeRecord[mdm_i].mdm_salary);

		if (mdm_EmployeeRecord[mdm_i].mdm_marital_status == 'Y')
			printf("Marital Status     : Married\n");
		else if (mdm_EmployeeRecord[mdm_i].mdm_marital_status == 'N')
			printf("Marital Status     : Unmarried\n");
		else
			printf("Marital Status     : Invalid Data Entered\n");

		printf("\n\n");

	}


	return(0);
}

// **** Simple Rudimentry Implementation Of gets_s() ****
// **** Implemented Due To Different Behaviour of gets_s / fgets() / fscanf() On Different Platforms ****
// **** Backspace / Character Deletion And Arrow Key Cursor Movement Not Implemented ****

void MDM_GetMyString(char mdm_str[], int mdm_str_size)
{
	// variable declarations
	int  mdm_i;
	char mdm_ch = '\0';

	// code
	mdm_i = 0;

	do
	{
		mdm_ch = getch();
		mdm_str[mdm_i] = mdm_ch;
		printf("%c", mdm_str[mdm_i]);
		mdm_i++;
	} while ((mdm_ch != '\r') && (mdm_i < mdm_str_size)); // '\r' is not allowed, will delete written words before

	if (mdm_i == mdm_str_size)
		mdm_str[mdm_i - 1] = '\0';
	else
		mdm_str[mdm_i] = '\0';

}

