// header files
#include<stdio.h>
#include<stdlib.h>

// macro defination
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declaration
	char* MDM_ReplaceVowelsWithHashSymbol(char*);

	// variable declarations
	char mdm_string[MDM_MAX_STRING_LENGTH];
	char* mdm_replaced_string = NULL;

	// code
	printf("\n\n");
	printf("Enter String : ");
	gets_s(mdm_string, MDM_MAX_STRING_LENGTH);

	mdm_replaced_string = MDM_ReplaceVowelsWithHashSymbol(mdm_string);

	if (mdm_replaced_string == NULL)
	{
		printf("MDM_ReplaceVowelsWithHashSymbol() Function Has Failed !!! Exitting Now...\n\n");
		exit(0);
	}

	printf("\n\n");
	printf("Replaced String Is : \n\n");
	printf("%s\n\n", mdm_replaced_string);

	if (mdm_replaced_string)
	{
		free(mdm_replaced_string);
		mdm_replaced_string = NULL;
	}

	return(0);
}


char* MDM_ReplaceVowelsWithHashSymbol(char* mdm_str)
{
	// function prototype
	void mdm_MyStrcpy(char*, char*);
	int mdm_MyStrlen(char*);

	// variable declarations
	char* mdm_new_string = NULL;
	int mdm_i;

	// code
	mdm_new_string = (char*)malloc(mdm_MyStrlen(mdm_str) * sizeof(char));
	if (mdm_new_string == NULL)
	{
		printf("Could Not Allocate Memory For New String !!! Exitting Now...\n\n");
		return(NULL);
	}

	mdm_MyStrcpy(mdm_new_string, mdm_str);
	for (mdm_i = 0; mdm_i < mdm_MyStrlen(mdm_new_string); mdm_i++)
	{
		switch (mdm_new_string[mdm_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			mdm_new_string[mdm_i] = '#';
			break;

		default:
			break;
		}
	}

	return(mdm_new_string);
}

void mdm_MyStrcpy(char* mdm_str_destination, char* mdm_str_source)
{

	// function prototype
	int mdm_MyStrlen(char*);

	// variable declaration
	int mdm_j;
	int mdm_iStringLength;

	// code
	mdm_iStringLength = mdm_MyStrlen(mdm_str_source);

	for (mdm_j = 0; mdm_j < mdm_iStringLength; mdm_j++)
		*(mdm_str_destination + mdm_j) = *(mdm_str_source + mdm_j);

	*(mdm_str_destination + mdm_j) = '\0';
}

int mdm_MyStrlen(char* mdm_str)
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String By Detecting The First Occurence Of Null-Terminating Character (\0) ****

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}

