// header file
#include<stdio.h>

enum
{
	NEGATIVE = -1,
	ZERO,
	POSITIVE
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declaration
	int MDM_Difference(int, int, int* );

	// variable declaration
	int mdm_a;
	int mdm_b;
	int mdm_answer, mdm_ret;

	// code
	printf("\n\n");
	printf("Enter Value Of 'A' : ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter Value Of 'B' : ");
	scanf("%d", &mdm_b);

	mdm_ret = MDM_Difference(mdm_a, mdm_b, &mdm_answer);

	printf("\n\n");
	printf("Difference Of %d And %d = %d\n\n", mdm_a, mdm_b, mdm_answer);

	if (mdm_ret == POSITIVE)
		printf("The Diffrence Of %d And %d Is Positive !!!\n\n", mdm_a, mdm_b);

	else if (mdm_ret == NEGATIVE)
		printf("The Difference Of %d And %d Is Negative !!!\n\n", mdm_a, mdm_b);

	else
		printf("The Difference Of %d And %d Is Zero !!!\n\n", mdm_a, mdm_b);

	return(0);
}

// We Want Our Function MDM_Difference() To Perform 2 Jobs...
// One, Is To Subtract The Input Numbers ('mdm_y' From 'mdm_x') And The Second, Is To Tell Whether The Difference Of 'mdm_x' And 'mdm_y' Is POSITIVE or NEGATIVE Or ZERO...
// But Any Function Has Only One Valid Return Value, Then How Can We Manage To Return Two Values To The Calling Function?
// This Is Where Parameterized Return Value Comes Into The Picture...
// We Can Return The Actual Difference Of 'mdm_x' And 'mdm_y', That Is, The Actual Answer Value, Via OUT-PARAMETER / PARAMETERIZED RETURN VALUE
// And We Can Return The Status Of The Answer (POSITIVE / NEGATIVE / ZERO) Via The Actual Return Value Of The Function...

int MDM_Difference(int mdm_x, int mdm_y, int* mdm_diff)
{
	// code
	*mdm_diff = mdm_x - mdm_y;

	if (*mdm_diff > 0)
		return(POSITIVE);

	else if (*mdm_diff < 0)
		return(NEGATIVE);

	else
		return(ZERO);
}


