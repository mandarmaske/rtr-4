// header files
#include<stdio.h>
#include<stdlib.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	void MDM_MultiplyArrayElementsByNumber(int*, int, int);

	// variable declarations
	int* mdm_iArray = NULL;
	int mdm_num_elements;
	int mdm_i, mdm_num;

	// code
	printf("\n\n");
	printf("Enter How Many Elements You Want In The Integer Array : ");
	scanf("%d", &mdm_num_elements);

	mdm_iArray = (int*)malloc(mdm_num_elements * sizeof(int));
	if (mdm_iArray == NULL)
	{
		printf("Memory Allocation To 'mdm_iArray' Has Failed !!! Exitting Now...\n\n");
		exit(0);
	}

	printf("\n\n");
	printf("Enter %d Elements For The Integer Array : \n\n", mdm_num_elements);
	for (mdm_i = 0; mdm_i < mdm_num_elements; mdm_i++)
		scanf("%d", &mdm_iArray[mdm_i]);

	// **** One ****
	printf("\n\n");
	printf("Array Before Passing To Function MDM_MultiplyArrayElementsByNumber() : \n\n");
	for (mdm_i = 0; mdm_i < mdm_num_elements; mdm_i++)
		printf("mdm_iArray[%d] = %d\n", mdm_i, mdm_iArray[mdm_i]);

	printf("\n\n");
	printf("Enter The Number By Which You Want To Multiply Each Array Element : ");
	scanf("%d", &mdm_num);

	MDM_MultiplyArrayElementsByNumber(mdm_iArray, mdm_num_elements, mdm_num);

	printf("\n\n");
	printf("Array Returned By Function MDM_MultiplyArrayElementsByNumber() : \n\n");
	for (mdm_i = 0; mdm_i < mdm_num_elements; mdm_i++)
		printf("mdm_iArray[%d] = %d\n", mdm_i, mdm_iArray[mdm_i]);

	if (mdm_iArray)
	{
		free(mdm_iArray);
		mdm_iArray = NULL;
		printf("\n\n");
		printf("Memory Allocated To 'mdm_iArray' Has Been Successfully Freed\n\n");
	}

	return(0);
}

void MDM_MultiplyArrayElementsByNumber(int* mdm_arr, int mdm_iNumElements, int mdm_n)
{
	// variable declarations
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < mdm_iNumElements; mdm_i++)
		mdm_arr[mdm_i] = mdm_arr[mdm_i] * mdm_n;
}

