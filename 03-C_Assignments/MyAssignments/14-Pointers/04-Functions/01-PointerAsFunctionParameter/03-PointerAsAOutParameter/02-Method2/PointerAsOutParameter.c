// header files
#include<stdio.h>
#include<stdlib.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	void MDM_MathematicalOperations(int, int, int*, int*, int*, int*, int*);

	// variable declarations
	int mdm_a;
	int mdm_b;
	int *mdm_answer_sum = NULL;
	int *mdm_answer_difference = NULL;
	int *mdm_answer_product = NULL;
	int *mdm_answer_quotient = NULL;
	int *mdm_answer_remainder = NULL;

	// code
	printf("\n\n");
	printf("Enter Value Of 'A' : ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter Value Of 'B' : ");
	scanf("%d", &mdm_b);

	// Passing Addresses To Function... Function Will Fill Them Up With Values... Hence, They Go Into The Function As Address Parameters And Come Out Of The Function As Address Parameters And Come Out Of The Function Filled With Valid Values 
	// Thus, (&mdm_answer, &mdm_answer_difference, &mdm_answer_product, &mdm_answer_quotient, &mdm_answer_remainder) Are Called "Out Parameters" Or "Parameterized Return Values"... Return Values Of Functions Coming Via Parameters
	// Hence, Although Each Function Has Only One Return Value, Using The Concept Of "Parameterized Return Values", Our Function "MDM_MathematicalOperations()" Has Given Us 5 Return Values !!!

	mdm_answer_sum = (int*)malloc(1 * sizeof(int));
	if (mdm_answer_sum == NULL)
	{
		printf("Could Not Allocate Memory For 'mdm_answer_sum'. Exitting Now....\n\n");
		exit(0);
	}

	mdm_answer_difference = (int*)malloc(1 * sizeof(int));
	if (mdm_answer_difference == NULL)
	{
		printf("Could Not Allocate Memory For 'mdm_answer_difference'. Exitting Now....\n\n");
		exit(0);
	}

	mdm_answer_product = (int*)malloc(1 * sizeof(int));
	if (mdm_answer_product == NULL)
	{
		printf("Could Not Allocate Memory For 'mdm_answer_product'. Exitting Now....\n\n");
		exit(0);
	}

	mdm_answer_quotient = (int*)malloc(1 * sizeof(int));
	if (mdm_answer_quotient == NULL)
	{
		printf("Could Not Allocate Memory For 'mdm_answer_quotient'. Exitting Now....\n\n");
		exit(0);
	}

	mdm_answer_remainder = (int*)malloc(1 * sizeof(int));
	if (mdm_answer_remainder == NULL)
	{
		printf("Could Not Allocate Memory For 'mdm_answer_remainder'. Exitting Now....\n\n");
		exit(0);
	}

	MDM_MathematicalOperations(mdm_a, mdm_b, mdm_answer_sum, mdm_answer_difference, mdm_answer_product, mdm_answer_quotient, mdm_answer_remainder);

	printf("\n\n");
	printf("******** Result ********\n\n");
	printf("Sum = %d\n\n", *mdm_answer_sum);
	printf("Difference = %d\n\n", *mdm_answer_difference);
	printf("Product = %d\n\n", *mdm_answer_product);
	printf("Quotient = %d\n\n", *mdm_answer_quotient);
	printf("Remainder = %d\n\n", *mdm_answer_remainder);

	if (mdm_answer_remainder)
	{
		free(mdm_answer_remainder);
		mdm_answer_remainder = NULL;
		printf("Memory Allocated For 'mdm_answer_remainder' Successfully Freed !!!\n\n");
	}

	if (mdm_answer_quotient)
	{
		free(mdm_answer_quotient);
		mdm_answer_quotient = NULL;
		printf("Memory Allocated For 'mdm_answer_quotient' Successfully Freed !!!\n\n");
	}

	if (mdm_answer_product)
	{
		free(mdm_answer_product);
		mdm_answer_product = NULL;
		printf("Memory Allocated For 'mdm_answer_product' Successfully Freed !!!\n\n");
	}

	if (mdm_answer_difference)
	{
		free(mdm_answer_difference);
		mdm_answer_difference = NULL;
		printf("Memory Allocated For 'mdm_answer_difference' Successfully Freed !!!\n\n");
	}

	if (mdm_answer_sum)
	{
		free(mdm_answer_sum);
		mdm_answer_sum = NULL;
		printf("Memory Allocated For 'mdm_answer_sum' Successfully Freed !!!\n\n");
	}

	return(0);
}

void MDM_MathematicalOperations(int mdm_x, int mdm_y, int* mdm_sum, int* mdm_difference, int* mdm_product, int* mdm_quotient, int* mdm_remainder)
{
	// code
	*mdm_sum = mdm_x + mdm_y; // Value At Address 'mdm_sum' = (mdm_x + mdm_y)
	*mdm_difference = mdm_x - mdm_y; // Value At Address 'mdm_difference' = (mdm_x - mdm_y)
	*mdm_product = mdm_x * mdm_y; // Value At Address 'mdm_product' = (mdm_x * mdm_y)
	*mdm_quotient = mdm_x / mdm_y; // Value At Address 'mdm_quotient' = (mdm_x / mdm_y)
	*mdm_remainder = mdm_x % mdm_y; // Value At Address 'mdm_remainder' = (mdm_x % mdm_y)
}

