// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	void MDM_MathematicalOperations(int, int, int*, int*, int*, int*, int*);

	// variable declarations
	int mdm_a;
	int mdm_b;
	int mdm_answer_sum;
	int mdm_answer_difference;
	int mdm_answer_product;
	int mdm_answer_quotient;
	int mdm_answer_remainder;

	// code
	printf("\n\n");
	printf("Enter Value Of 'A' : ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter Value Of 'B' : ");
	scanf("%d", &mdm_b);

	// Passing Addresses To Function... Function Will Fill Them Up With Values... Hence, They Go Into The Function As Address Parameters And Come Out Of The Function As Address Parameters And Come Out Of The Function Filled With Valid Values 
	// Thus, (&mdm_answer, &mdm_answer_difference, &mdm_answer_product, &mdm_answer_quotient, &mdm_answer_remainder) Are Called "Out Parameters" Or "Parameterized Return Values"... Return Values Of Functions Coming Via Parameters
	// Hence, Although Each Function Has Only One Return Value, Using The Concept Of "Parameterized Return Values", Our Function "MDM_MathematicalOperations()" Has Given Us 5 Return Values !!!

	MDM_MathematicalOperations(mdm_a, mdm_b, &mdm_answer_sum, &mdm_answer_difference, &mdm_answer_product, &mdm_answer_quotient, &mdm_answer_remainder);

	printf("\n\n");
	printf("******** Result ********\n\n");
	printf("Sum = %d\n\n", mdm_answer_sum);
	printf("Difference = %d\n\n", mdm_answer_difference);
	printf("Product = %d\n\n", mdm_answer_product);
	printf("Quotient = %d\n\n", mdm_answer_quotient);
	printf("Remainder = %d\n\n", mdm_answer_remainder);

	return(0);
}

void MDM_MathematicalOperations(int mdm_x, int mdm_y, int* mdm_sum, int* mdm_difference, int* mdm_product, int* mdm_quotient, int* mdm_remainder)
{
	// code
	*mdm_sum = mdm_x + mdm_y; // Value At Address 'mdm_sum' = (mdm_x + mdm_y)
	*mdm_difference = mdm_x - mdm_y; // Value At Address 'mdm_difference' = (mdm_x - mdm_y)
	*mdm_product = mdm_x * mdm_y; // Value At Address 'mdm_product' = (mdm_x * mdm_y)
	*mdm_quotient = mdm_x / mdm_y; // Value At Address 'mdm_quotient' = (mdm_x / mdm_y)
	*mdm_remainder = mdm_x % mdm_y; // Value At Address 'mdm_remainder' = (mdm_x % mdm_y)
}

