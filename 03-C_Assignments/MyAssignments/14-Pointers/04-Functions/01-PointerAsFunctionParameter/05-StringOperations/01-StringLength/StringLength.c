// header files
#include<stdio.h>
#include<stdlib.h>

// macro defination
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	int MDM_MyStrlen(char*);

	// variable declarations
	char* mdm_chArray = NULL; // Character Array Can Be Represented By A Char Pointer To Mark The Base Address (char*)
	int mdm_iStringLength = 0;

	// code
	printf("\n\n");
	mdm_chArray = (char*)malloc(MDM_MAX_STRING_LENGTH * sizeof(char));

	if (mdm_chArray == NULL)
	{
		printf("Memory Allocation To Character Array Failed !!! Exitting Now...\n\n");
		exit(0);
	}

	// **** String Input ****
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray, MDM_MAX_STRING_LENGTH);

	// **** String Output ****
	printf("\n\n");
	printf("String Entered By You : \n\n");
	printf("%s\n\n", mdm_chArray);

	// **** String Length ****
	printf("\n\n");
	mdm_iStringLength = MDM_MyStrlen(mdm_chArray);
	printf("Length Of String Is = %d Characters !!!\n\n", mdm_iStringLength);

	if (mdm_chArray)
	{
		free(mdm_chArray);
		mdm_chArray = NULL;
	}

	return(0);
}

int MDM_MyStrlen(char* mdm_str)
{
	// variable declarations
	int mdm_i;
	int mdm_String_Length = 0;

	// code
	// **** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character(\0) ****
	for (mdm_i = 0; mdm_i < MDM_MAX_STRING_LENGTH; mdm_i++)
	{
		if (*(mdm_str + mdm_i) == '\0')
			break;
		else
			mdm_String_Length++;
	}

	return(mdm_String_Length);
}
