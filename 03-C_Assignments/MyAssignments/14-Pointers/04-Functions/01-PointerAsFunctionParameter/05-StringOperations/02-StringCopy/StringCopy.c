// header files
#include<stdio.h>
#include<stdlib.h>

// macro definition
#define MDM_MAX_STRING_LENGTH 512

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype
	void mdm_MyStrcpy(char*, char*);
	int mdm_MyStrlen(char*);

	// variable declaration
	char* mdm_chArray_Original = NULL;
	char* mdm_chArray_Copy = NULL; // A Character Array Is A String 
	int mdm_Original_String_Length;

	// code

	// **** String Input ****
	printf("\n\n");
	mdm_chArray_Original = (char*)malloc(MDM_MAX_STRING_LENGTH * sizeof(char));
	if (mdm_chArray_Original == NULL)
	{
		printf("Memory Allocation For Original String Failed !!! Exitting Now...\n\n");
		exit(0);
	}
	printf("Enter A String : \n\n");
	gets_s(mdm_chArray_Original, MDM_MAX_STRING_LENGTH);
	mdm_Original_String_Length = mdm_MyStrlen(mdm_chArray_Original);
	mdm_chArray_Copy = (char*)malloc(mdm_Original_String_Length * sizeof(char));
	if (mdm_chArray_Copy == NULL)
	{
		printf("Memory Allocation For Copied String Failed !!! Exitting Now...\n\n");
		exit(0);
	}

	// **** String Copy ****
	mdm_MyStrcpy(mdm_chArray_Copy, mdm_chArray_Original);

	// **** String Output ****
	printf("\n\n");
	printf("The Original String Entered By You (i.e 'mdm_chArray_Original') Is : \n\n");
	printf("%s\n", mdm_chArray_Original);

	printf("\n\n");
	printf("The Copied String (i.e 'mdm_chArray_Copy') : \n\n");
	printf("%s\n\n", mdm_chArray_Copy);

	if (mdm_chArray_Copy)
	{
		free(mdm_chArray_Copy);
		mdm_chArray_Copy = NULL;
		printf("\n\n");
		printf("Memory Allocated For Copied String Has Been Successfully Freed\n\n");
	}

	if (mdm_chArray_Original)
	{
		free(mdm_chArray_Original);
		mdm_chArray_Original = NULL;
		printf("\n\n");
		printf("Memory Allocated For Original String Has Been Successfully Freed\n\n");
	}

	return(0);
}

void mdm_MyStrcpy(char* mdm_str_destination, char* mdm_str_source)
{

	// function prototype
	int mdm_MyStrlen(char*);

	// variable declaration
	int mdm_j;
	int mdm_iStringLength;

	// code
	mdm_iStringLength = mdm_MyStrlen(mdm_str_source);

	for (mdm_j = 0; mdm_j < mdm_iStringLength; mdm_j++)
		*(mdm_str_destination + mdm_j) = *(mdm_str_source + mdm_j);

	*(mdm_str_destination + mdm_j) = '\0';
}

int mdm_MyStrlen(char* mdm_str)
{
	// variable declarations
	int mdm_j;
	int mdm_string_length = 0;

	// code
	// **** Determining Exact Length Of The String By Detecting The First Occurence Of Null-Terminating Character (\0) ****

	for (mdm_j = 0; mdm_j < MDM_MAX_STRING_LENGTH; mdm_j++)
	{
		if (mdm_str[mdm_j] == '\0')
			break;
		else
			mdm_string_length++;
	}

	return(mdm_string_length);
}
