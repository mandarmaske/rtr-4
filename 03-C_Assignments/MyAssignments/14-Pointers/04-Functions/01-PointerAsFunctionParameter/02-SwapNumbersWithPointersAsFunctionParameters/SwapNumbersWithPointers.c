// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	void MDM_SwapNumbers(int* , int* );

	// Variable declarations
	int mdm_a;
	int mdm_b;

	// code
	printf("\n\n");
	printf("Enter Value For 'A' : ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter Value For 'B' : ");
	scanf("%d", &mdm_b);

	printf("\n\n");
	printf("******** Before Swapping (in main()) ********\n\n");
	printf("Value of 'A' = %d\n\n", mdm_a);
	printf("Value Of 'B' = %d\n\n", mdm_b);

	MDM_SwapNumbers(&mdm_a, &mdm_b); // ******** Arguments Passed 'By Refrence / Address' ********

	printf("\n\n");
	printf("******** After Swapping (in main()) ********\n\n");
	printf("Value of 'A' = %d\n\n", mdm_a);
	printf("Value Of 'B' = %d\n\n", mdm_b);

	return(0);
}

// Address of 'mdm_a' is copied into 'mdm_x' and Address of 'mdm_b' is copied into 'mdm_y'... So,  '&mdm_a' and 'mdm_x' are pointing to One and the Same address and '&mdm_b' and 'mdm_y' are pointing to One and the Same Address...
// Swapping takes place between 'Value at address of mdm_x' (Value at &mdm_a i.e : mdm_a) and 'Value at address of mdm_y' (value at &mdm_b i.e :  mdm_b)
// Hence, Swappingin this case takes place between '*mdm_x' and '*mdm_y' As Well As Between 'mdm_a' and 'mdm_b'...

void MDM_SwapNumbers(int* mdm_x, int* mdm_y) 
{
	// variable declarations
	int mdm_temp;

	// code
	printf("\n\n");
	printf("******** Before Swappng (in MDM_SwapNumbers) *********\n\n");
	printf("Value of 'X' = %d\n\n", *mdm_x);
	printf("Value Of 'Y' = %d\n\n", *mdm_y);

	mdm_temp = *mdm_x;
	*mdm_x = *mdm_y;
	*mdm_y = mdm_temp;

	printf("******** After Swappng (in MDM_SwapNumbers) *********\n\n");
	printf("Value of 'X' = %d\n\n", *mdm_x);
	printf("Value Of 'Y' = %d\n\n", *mdm_y);
}

