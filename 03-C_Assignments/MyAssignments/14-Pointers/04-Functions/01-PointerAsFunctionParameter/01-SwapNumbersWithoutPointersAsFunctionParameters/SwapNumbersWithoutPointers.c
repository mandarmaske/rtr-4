// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	void MDM_SwapNumbers(int, int);

	// Variable declarations
	int mdm_a;
	int mdm_b;

	// code
	printf("\n\n");
	printf("Enter Value For 'A' : ");
	scanf("%d", &mdm_a);

	printf("\n\n");
	printf("Enter Value For 'B' : ");
	scanf("%d", &mdm_b);

	printf("\n\n");
	printf("******** Before Swapping (in main()) ********\n\n");
	printf("Value of 'A' = %d\n\n", mdm_a);
	printf("Value Of 'B' = %d\n\n", mdm_b);

	MDM_SwapNumbers(mdm_a, mdm_b); // ******** Arguments Passed 'By Value' ********

	printf("\n\n");
	printf("******** After Swapping (in main()) ********\n\n");
	printf("Value of 'A' = %d\n\n", mdm_a);
	printf("Value Of 'B' = %d\n\n", mdm_b);

	return(0);
}

void MDM_SwapNumbers(int mdm_x, int mdm_y) // Value Of 'mdm_a' is copied into 'mdm_x' and value Of 'mdm_b' is copied into 'mdm_y' ... Swapping Takes Place Between 'mdm_x' and 'mdm_y', not between 'mdm_a' and 'mdm_b'
{
	// variable declarations
	int mdm_temp;

	// code
	printf("\n\n");
	printf("******** Before Swappng (in MDM_SwapNumbers) *********\n\n");
	printf("Value of 'X' = %d\n\n", mdm_x);
	printf("Value Of 'Y' = %d\n\n", mdm_y);

	mdm_temp = mdm_x;
	mdm_x = mdm_y;
	mdm_y = mdm_temp;

	printf("******** After Swappng (in MDM_SwapNumbers) *********\n\n");
	printf("Value of 'X' = %d\n\n", mdm_x);
	printf("Value Of 'Y' = %d\n\n", mdm_y);
}

