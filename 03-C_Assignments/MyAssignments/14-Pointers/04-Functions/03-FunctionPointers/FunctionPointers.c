// header files
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	int mdm_AddIntegers(int, int);
	int mdm_SubtractIntegers(int, int);
	float mdm_AddFloats(float, float);

	// variable declarations
	typedef int (*MDM_AddIntsFnPtr)(int, int);
	MDM_AddIntsFnPtr mdm_ptrAddTwoIntegers = NULL;
	MDM_AddIntsFnPtr mdm_ptrFunc = NULL;

	typedef float (*MDM_AddFloatsFnPtr)(float, float);
	MDM_AddFloatsFnPtr mdm_ptrAddTwoFloats = NULL;

	int mdm_iAnswer = 0;
	float mdm_fAnswer = 0.0f;

	// code
	mdm_ptrAddTwoIntegers = mdm_AddIntegers;
	mdm_iAnswer = mdm_ptrAddTwoIntegers(9, 30);
	printf("\n\n");
	printf("Sum Of Integers = %d\n\n", mdm_iAnswer);

	mdm_ptrFunc = mdm_SubtractIntegers;
	mdm_iAnswer = mdm_ptrFunc(9, 30);
	printf("\n\n");
	printf("Subtraction Of Integers = %d\n\n", mdm_iAnswer);

	mdm_ptrAddTwoFloats = mdm_AddFloats;
	mdm_fAnswer = mdm_ptrAddTwoFloats(11.45f, 8.2f);
	printf("\n\n");
	printf("Sum Of Floating-Point Numbers = %f\n\n", mdm_fAnswer);

	return(0);
}

int mdm_AddIntegers(int mdm_a, int mdm_b)
{
	// variable declarations
	int mdm_c;

	// code
	mdm_c = mdm_a + mdm_b;
	return(mdm_c);
}

int mdm_SubtractIntegers(int mdm_a, int mdm_b)
{
	// variable declarations
	int mdm_c;

	// code
	if (mdm_a > mdm_b)
		mdm_c = mdm_a - mdm_b;
	else
		mdm_c = mdm_b - mdm_a;

	return(mdm_c);
}

float mdm_AddFloats(float mdm_fnum1, float mdm_fnum2)
{
	// variable declarations
	float mdm_ans;

	// code
	mdm_ans = mdm_fnum1 + mdm_fnum2;
	return(mdm_ans);
}

