// header files
#include<stdio.h>

// defining struct
struct MDM_MyData
{
	int* mdm_iptr;
	int mdm_i;

	float* mdm_fptr;
	float mdm_f;

	double* mdm_dptr;
	double mdm_d;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	struct MDM_MyData mdm_Data;

	// code
	mdm_Data.mdm_i = 9;
	mdm_Data.mdm_iptr = &mdm_Data.mdm_i;

	mdm_Data.mdm_f = 11.45f;
	mdm_Data.mdm_fptr = &mdm_Data.mdm_f;

	mdm_Data.mdm_d = 1.224466;
	mdm_Data.mdm_dptr = &mdm_Data.mdm_d;

	printf("\n\n");
	printf("mdm_i = %d\n", *(mdm_Data.mdm_iptr));
	printf("Address Of 'mdm_i' = %p\n\n", mdm_Data.mdm_iptr);

	printf("mdm_f = %f\n", *(mdm_Data.mdm_fptr));
	printf("Address Of 'mdm_f' = %p\n\n", mdm_Data.mdm_fptr);

	printf("mdm_d = %lf\n", *(mdm_Data.mdm_dptr));
	printf("Address Of 'mdm_d' = %p\n\n", mdm_Data.mdm_dptr);

	return(0);
}


