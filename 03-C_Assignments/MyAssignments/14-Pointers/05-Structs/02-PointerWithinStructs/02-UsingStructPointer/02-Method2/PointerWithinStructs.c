// header files
#include<stdio.h>
#include<stdlib.h>

// defining struct
struct MDM_MyData
{
	int* mdm_iptr;
	int mdm_i;

	float* mdm_fptr;
	float mdm_f;

	double* mdm_dptr;
	double mdm_d;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declaration
	struct MDM_MyData* mdm_pData = NULL;

	// code
	printf("\n\n");
	mdm_pData = (struct MDM_MyData*)malloc(sizeof(struct MDM_MyData));
	if (mdm_pData == NULL)
	{
		printf("Failed To Allocate Memory To 'struct MDM_MyData' !!! Exitting Now...\n\n");
		exit(0);
	}
	else
		printf("Successfully Allocated Memory To 'struct MDM_MyData' !!!\n\n");

	mdm_pData -> mdm_i = 9;
	mdm_pData -> mdm_iptr = &mdm_pData -> mdm_i;

	mdm_pData -> mdm_f = 11.45f;
	mdm_pData -> mdm_fptr = &mdm_pData -> mdm_f;

	mdm_pData -> mdm_d = 1.224466;
	mdm_pData -> mdm_dptr = &mdm_pData -> mdm_d;

	printf("\n\n");
	printf("mdm_i = %d\n", mdm_pData -> mdm_i);
	printf("Address Of 'mdm_i' = %p\n\n", mdm_pData -> mdm_iptr);

	printf("mdm_f = %f\n", mdm_pData -> mdm_f);
	printf("Address Of 'mdm_f' = %p\n\n", mdm_pData -> mdm_fptr);

	printf("mdm_d = %lf\n", mdm_pData -> mdm_d);
	printf("Address Of 'mdm_d' = %p\n\n", mdm_pData -> mdm_dptr);

	if (mdm_pData)
	{
		free(mdm_pData);
		mdm_pData = NULL;
		printf("Memory Allocated To 'struct MDM_MyData' Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}


