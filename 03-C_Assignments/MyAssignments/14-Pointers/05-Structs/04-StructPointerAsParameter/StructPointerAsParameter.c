// header files
#include<stdio.h>
#include<stdlib.h>

// defining struct
struct MDM_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declaration
	void MDM_ChangeValues(struct MDM_MyData*);

	// variable declarations
	struct MDM_MyData* mdm_pData = NULL;

	// code
	printf("\n\n");

	mdm_pData = (struct MDM_MyData*)malloc(sizeof(struct MDM_MyData));
	if (mdm_pData == NULL)
	{
		printf("Failed To Allocate Memory To 'struct MDM_MyData' !!! Exitting Now...\n\n");
		exit(0);
	}
	else
		printf("Successfully Allocated Memory To 'struct MDM_MyData' !!!\n\n");

	// Assigning Data Values To The Data Members Of 'struct MDM_MyData'
	mdm_pData -> mdm_i = 30;
	mdm_pData -> mdm_f = 11.45f;
	mdm_pData -> mdm_d = 1.234455;

	// Displaying Values Of The Data Members Of 'struct MDM_MyData'
	printf("\n\n");
	printf("Data Members Of 'struct MDM_MyData' Are : \n\n");
	printf("mdm_i = %d\n", mdm_pData -> mdm_i);
	printf("mdm_f = %f\n", mdm_pData -> mdm_f);
	printf("mdm_d = %lf\n", mdm_pData -> mdm_d);

	MDM_ChangeValues(mdm_pData);

	// Displaying Values Of The Data Members Of 'struct MDM_MyData'
	printf("\n\n");
	printf("Data Members Of 'struct MDM_MyData' Are : \n\n");
	printf("mdm_i = %d\n", mdm_pData -> mdm_i);
	printf("mdm_f = %f\n", mdm_pData -> mdm_f);
	printf("mdm_d = %lf\n", mdm_pData -> mdm_d);

	printf("\n\n");
	if (mdm_pData)
	{
		free(mdm_pData);
		mdm_pData = NULL;
		printf("Memory Allocated To 'struct MDM_MyData' Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}

void MDM_ChangeValues(struct MDM_MyData* mdm_pParam_Data)
{
	// code

	mdm_pParam_Data -> mdm_i = 9;
	mdm_pParam_Data -> mdm_f = 8.2f;
	mdm_pParam_Data -> mdm_d = 6.199288;

	// Can Also Do This As...
	/*
	(*mdm_pParam_Data).mdm_i = 9;
	(*mdm_pParam_Data).mdm_f = 8.2f;
	(*mdm_pParam_Data).mdm_d = 6.199288;
	*/

}