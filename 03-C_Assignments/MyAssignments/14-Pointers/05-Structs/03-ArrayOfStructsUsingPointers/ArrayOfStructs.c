// header file
#include <stdio.h>
#include <ctype.h>

// macro defination
#define MDM_NAME_LENGTH 100
#define MDM_MARITAL_STATUS 10 

// struct defination
struct MDM_Employee
{
	char mdm_name[MDM_NAME_LENGTH];
	int mdm_age;
	char mdm_sex;
	float mdm_salary;
	char mdm_marital_status;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function prototype / declaration / signature
	void MDM_GetMyString(char[], int);

	// variable declarations
	struct MDM_Employee* mdm_pEmployeeRecord = NULL;
	int mdm_i, mdm_num_employees;

	// code
	printf("\n\n");
	printf("Enter Number Of Employees Whose Details You Want To Record : ");
	scanf("%d", &mdm_num_employees);

	printf("\n\n");
	mdm_pEmployeeRecord = (struct MDM_Employee*)malloc(sizeof(struct MDM_Employee) * mdm_num_employees);
	if (mdm_pEmployeeRecord == NULL)
	{
		printf("Failed To Allocate Memory For %d Employees !!! Exitting Now \n\n", mdm_num_employees);
		exit(0);
	}
	else
		printf("Successfully Allocated Memory For %d Employees !!!\n\n", mdm_num_employees);

	// ******** User Input Initialization Of Array Of 'struct MDM_Employee' ********
	for (mdm_i = 0; mdm_i < mdm_num_employees; mdm_i++)
	{
		printf("\n\n\n");
		printf("******** Data Entry For Employee Number %d ********\n", (mdm_i + 1));

		printf("\n\n");
		printf("Enter Employee Name : ");
		MDM_GetMyString(mdm_pEmployeeRecord[mdm_i].mdm_name, MDM_NAME_LENGTH);

		printf("\n\n\n");
		printf("Enter Employee's Age (In Years) : ");
		scanf("%d", &mdm_pEmployeeRecord[mdm_i].mdm_age);

		printf("\n\n");
		printf("Enter Employee's Sex (M/m For Male, F/f For Female) : ");
		mdm_pEmployeeRecord[mdm_i].mdm_sex = getch();
		mdm_pEmployeeRecord[mdm_i].mdm_sex = toupper(mdm_pEmployeeRecord[mdm_i].mdm_sex);
		printf("%c", mdm_pEmployeeRecord[mdm_i].mdm_sex);

		printf("\n\n\n");
		printf("Enter Employee's Salary (In Indiam Rupees) : ");
		scanf("%f", &mdm_pEmployeeRecord[mdm_i].mdm_salary);

		printf("\n\n");
		printf("Is The Employee Married? (Y/y For Yes, N/n For No) : ");
		mdm_pEmployeeRecord[mdm_i].mdm_marital_status = getch();
		mdm_pEmployeeRecord[mdm_i].mdm_marital_status = toupper(mdm_pEmployeeRecord[mdm_i].mdm_marital_status);
		printf("%c", mdm_pEmployeeRecord[mdm_i].mdm_marital_status);
	}

	// **** Display ****
	printf("\n\n\n\n");
	printf("******** Displaying Employee Records ********\n\n");
	for (mdm_i = 0; mdm_i < mdm_num_employees; mdm_i++)
	{
		printf("******** Employee Number %d ********\n\n", (mdm_i + 1));
		printf("Name               : %s\n", mdm_pEmployeeRecord[mdm_i].mdm_name);
		printf("Age                : %d\n", mdm_pEmployeeRecord[mdm_i].mdm_age);

		if (mdm_pEmployeeRecord[mdm_i].mdm_sex == 'M')
			printf("Sex                : Male\n");
		else if (mdm_pEmployeeRecord[mdm_i].mdm_sex == 'F')
			printf("Sex                : Female\n");
		else
			printf("Sex                : Invalid Data Entered\n");

		printf("Salary             : Rs. %f\n", mdm_pEmployeeRecord[mdm_i].mdm_salary);

		if (mdm_pEmployeeRecord[mdm_i].mdm_marital_status == 'Y')
			printf("Marital Status     : Married\n");
		else if (mdm_pEmployeeRecord[mdm_i].mdm_marital_status == 'N')
			printf("Marital Status     : Unmarried\n");
		else
			printf("Marital Status     : Invalid Data Entered\n");

		printf("\n\n");

	}

	if (mdm_pEmployeeRecord)
	{
		free(mdm_pEmployeeRecord);
		mdm_pEmployeeRecord = NULL;
		printf("Memory Allocated To %d Employees Has Been Successfully Freed !!!\n\n", mdm_num_employees);
	}


	return(0);
}

// **** Simple Rudimentry Implementation Of gets_s() ****
// **** Implemented Due To Different Behaviour of gets_s / fgets() / fscanf() On Different Platforms ****
// **** Backspace / Character Deletion And Arrow Key Cursor Movement Not Implemented ****

void MDM_GetMyString(char mdm_str[], int mdm_str_size)
{
	// variable declarations
	int  mdm_i;
	char mdm_ch = '\0';

	// code
	mdm_i = 0;

	do
	{
		mdm_ch = getch();
		mdm_str[mdm_i] = mdm_ch;
		printf("%c", mdm_str[mdm_i]);
		mdm_i++;
	} while ((mdm_ch != '\r') && (mdm_i < mdm_str_size)); // '\r' is not allowed, will delete written words before

	if (mdm_i == mdm_str_size)
		mdm_str[mdm_i - 1] = '\0';
	else
		mdm_str[mdm_i] = '\0';

}

