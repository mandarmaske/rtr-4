// header files
#include<stdio.h>
#include<stdlib.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int* mdm_ptr_iArray = NULL; // It is good discipline to initialize any pointer with null addresss to prevent any garbage value getting into it, that way, it makes it easy to check for success or failure of memory allocation later on after malloc()...
	unsigned int mdm_intArrayLength = 0;
	int mdm_i;

	// code
	printf("\n\n");
	printf("Enter The Number Of Elements You Want In Your Integer Array: ");
	scanf("%d", &mdm_intArrayLength);

	// **** Allocating As Much Memory Required To The Integer Array ****
	// **** Memory Required For Integer Array = Size In Bytes Of One Integer * Number Of Integers To Be Stored In Array ****
	// **** To Allocate Said Amount Of Memory, Function malloc() Will Be Used ****
	// **** malloc() Will Allocate Said Amount Of Memory And Will Return The Initial / Starting / Base Address Of The Allocated Memory, Which Must Be Captured In A Pointer Variable ****
	// **** Using This Base Address, The Integer Array Can Be Accessed And Used ****

	mdm_ptr_iArray = (int*)malloc(sizeof(int) * mdm_intArrayLength);
	if (mdm_ptr_iArray == NULL) // If mdm_ptr_iArray Is Still Null, Even After Call To malloc(), It Means malloc() Has Failed To Allocate Memory And No Address Has Been Returned By malloc() Has Failed To Allocate Memory And No Address Has Been Returned By malloc() in mdm_ptr_iArray...
	{
		printf("\n\n");
		printf("Memory Allocation For Integer Array Has Failed !!! Exitting Now... \n\n");
		exit(0);
	}
	else // If mdm_ptr_iArray Is Not Null, It Means It Must Contain A Valid Address Which Is Returned By malloc(), Hence, malloc() Has Succeded In Memory Allocation...
	{
		printf("\n\n");
		printf("Memory Allocation For Integer Array Has Succeded !!!\n\n");
		printf("Memory Addresses From %p to %p Have Been Allocated To Integer Array !!!\n\n", mdm_ptr_iArray, (mdm_ptr_iArray + (mdm_intArrayLength - 1)));
	}

	printf("\n\n");
	printf("Enter %d Elements For The Integer For The Integer Array : \n\n", mdm_intArrayLength);
	for (mdm_i = 0; mdm_i < mdm_intArrayLength; mdm_i++)
		scanf("%d", (mdm_ptr_iArray) + mdm_i);

	printf("\n\n");
	printf("The Integer Array Entered By You, Consisting Of %d Elements : \n\n", mdm_intArrayLength);
	for (mdm_i = 0; mdm_i < mdm_intArrayLength; mdm_i++)
	{
		printf("mdm_ptr_iArray[%d] = %d\t \t \t At Address &mdm_ptr_iArray[%d] : %p\n\n", mdm_i, mdm_ptr_iArray[mdm_i], mdm_i, &mdm_ptr_iArray[mdm_i]);
	}

	printf("\n\n");
	for (mdm_i = 0; mdm_i < mdm_intArrayLength; mdm_i++)
	{
		printf("*(mdm_ptr_iArray + %d) = %d \t \t At Address (mdm_ptr_iArray + %d) : %p\n\n", mdm_i, *(mdm_ptr_iArray + mdm_i), mdm_i, (mdm_ptr_iArray + mdm_i));
	}

	// **** Checking , If Memory Is Still Allocated By Checking  Validity Of Base Address 'mdm_ptr_iArray' ****
	// **** If Address Is Valid, That Is If 'mdm_ptr_iArray' Exists, That Is, If It Is Not Null, Memory Is Still Allocated ****
	// **** In That Case, The The Allocated Memory Must Be Freed **** 
	// **** Memory Is Allocated Using malloc() And Free Using free() ****
	// **** Once, Memory Is Free Using free(), The Base Address Must Be Cleaned, That Is, It Must Be Re-Initialized To 'Null' To Keep Away Garbage Values. This Is Not Compulsory, But It Is Good Coding Discipline **** 

	if (mdm_ptr_iArray)
	{
		free(mdm_ptr_iArray);
		mdm_ptr_iArray = NULL;

		printf("\n\n");
		printf("Memory Allocated For Integer Array Has Been Successfully Freed !!!\n\n");
	}

	printf("*(mdm_ptr_iArray + %d) = %d \n", 2, *(mdm_ptr_iArray + 2));

	return(0);
}
