// header files
#include<stdio.h>
#include<stdlib.h>

// macro definations
#define MDM_NUM_ROWS 5
#define MDM_NUM_COLUMNS 3 

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int* mdm_iArray[MDM_NUM_ROWS]; // A 2D Array which will have 5 Rows And Number Of Columns Can Be Decided Later...
	int mdm_i, mdm_j;

	// code
	printf("\n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		mdm_iArray[mdm_i] = (int*)malloc(MDM_NUM_COLUMNS * sizeof(int));
		if (mdm_iArray[mdm_i] == NULL)
		{
			printf("Failed To Allocate Memory To Row %d Of 2D Integer Array !!! Exitting Now...\n\n", mdm_i);
			exit(0);
		}
		else
		{
			printf("Memory Allocation To Row %d Of 2D Integer Array Succeeded !!!\n\n", mdm_i);
		}
	}

	// **** Assigning Values To 2D Array... ****
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			mdm_iArray[mdm_i][mdm_j] = (mdm_i + 1) * (mdm_j + 1); 
		}
	}
	
	// **** Displaying 2D Array ****
	printf("\n\n");
	printf("2D Integer Array Elements : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("mdm_iArray[%d][%d] = %d \n", mdm_i, mdm_j, mdm_iArray[mdm_i][mdm_j]);
		}
		printf("\n\n");
	}

	// **** Freeing Memory Assigned To 2D Array (Must Be Done In Reverse Order)
	for (mdm_i = (MDM_NUM_ROWS - 1); mdm_i >= 0; mdm_i--)
	{
		free(mdm_iArray[mdm_i]);
		mdm_iArray[mdm_i] = NULL;
		printf("Memory Allocated To Row %d Of 2D Integer Array Has Been Successfully Freed !!!\n\n", mdm_i);
	}

	return(0);
}

