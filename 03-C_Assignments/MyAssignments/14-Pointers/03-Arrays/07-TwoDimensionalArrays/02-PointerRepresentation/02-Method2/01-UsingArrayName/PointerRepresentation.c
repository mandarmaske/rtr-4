// header file
#include<stdio.h>

// macro definations
#define MDM_NUM_ROWS 5
#define MDM_NUM_COLUMNS 3 

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArray[MDM_NUM_ROWS][MDM_NUM_COLUMNS];
	int mdm_i, mdm_j;

	// code
	// **** Name Of An Array Itself Is It's Base Address ****
	// **** Hence, 'mdm_iArray' Is Base Address Of 2D Integer Array mdm_iArray[][]
	
	// mdm_iArray[5][3] -> mdm_iArray Is 2D Array Having 5 Rows And 3 Coloumns. Each Of These 5 Rows Is A 1D Integer Array Of 3 Integers ****  
	// mdm_iArray[0] -> Is The 0th Row ... Hence, Is The Base Address Of Row 0 
	// mdm_iArray[1] -> Is The 1th Row ... Hence, Is The Base Address Of Row 1
	// mdm_iArray[2] -> Is The 2th Row ... Hence, Is The Base Address Of Row 2 
	// mdm_iArray[3] -> Is The 3th Row ... Hence, Is The Base Address Of Row 3 
	// mdm_iArray[4] -> Is The 4th Row ... Hence, Is The Base Address Of Row 4

	// (mdm_iArray[0] + 0) -> Address Of 0th Integer From Base Address Of 0th Row (mdm_iArray[0][0])
	// (mdm_iArray[0] + 1) -> Address Of 1st Integer From Base Address Of 0th Row (mdm_iArray[0][1])
	// (mdm_iArray[0] + 2) -> Address Of 2nd Integer From Base Address Of 0th Row (mdm_iArray[0][2])

	// (mdm_iArray[1] + 0) -> Address Of 0th Integer From Base Address Of 1st Row (mdm_iArray[1][0])
	// (mdm_iArray[1] + 1) -> Address Of 1st Integer From Base Address Of 1st Row (mdm_iArray[1][1])
	// (mdm_iArray[1] + 2) -> Address Of 2nd Integer From Base Address Of 1st Row (mdm_iArray[1][2])

	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			*(*(mdm_iArray + mdm_i) + mdm_j) = (mdm_i + 1) * (mdm_j + 1); // 'mdm_iArray[mdm_i]' Can Be Treated As 1D Array Using Pointers...
		}
	}

	printf("\n\n");
	printf("2D Integer Array Elements Along With Addresses : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("*(*(mdm_iArray + %d) + %d) = %d \t \t At Address (*(mdm_iArray + %d) + %d) : %p\n", mdm_i, mdm_j, *(*(mdm_iArray + mdm_i) + mdm_j), mdm_i, mdm_j, (*(mdm_iArray + mdm_i) + mdm_j));
		}
		printf("\n\n");
	}

	return(0);
}

