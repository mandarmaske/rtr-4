// header files
#include<stdio.h>
#include<stdlib.h>

// macro definations
#define MDM_NUM_ROWS 5
#define MDM_NUM_COLUMNS 3 

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_i, mdm_j;
	int** mdm_ptr_iArray = NULL;

	// code
	// **** Every Row Of A 2D Array Is An Integer Array Itself Comprising Of 'MDM_NUM_COLUMNS' Integer Elements ****
	// **** There Are 5 Rows And 3 Columns In A 2D Integer Array, Each Of The The 5 Rows Is 1D Array Of 3 Integers ****  
	// **** Hence, Each Of These 5 Rows Themselves Being Arrays, Will Be The Base Addresses Of Their Respective Rows ****
	printf("\n\n");

	// **** Memory Allocation **** 
	mdm_ptr_iArray = (int**)malloc(MDM_NUM_ROWS * sizeof(int*)); // mdm_ptr_iArray Is The Name And Base Address Of 1D Array Containing 5 Integer Pointers to 5 Integer Array... So It Is An Array Containing elements of data type (int*)
	if (mdm_ptr_iArray == NULL)
	{
		printf("Memory Allocation To The 1D Array Of Base Addresses Of %d Rows Failed !!! Exitting Now...\n\n", MDM_NUM_ROWS);
		exit(0);
	}
	else
	{
		printf("Memory Allocation To The 1D Array Of Base Addresses Of %d Rows Has Succeeded !!!\n\n", MDM_NUM_ROWS);
	}
	
	// **** Allocating Memory To Each Row ****
	printf("\n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		mdm_ptr_iArray[mdm_i] = (int*)malloc(MDM_NUM_COLUMNS * sizeof(int)); // mdm_ptr_iArray[mdm_i] is the base address of ith
		if (mdm_ptr_iArray[mdm_i] == NULL)
		{
			printf("Memory Allocation To The 1D Array Of Base Addresses Of %d Rows Failed !!! Exitting Now...\n\n", mdm_i);
			exit(0);
		}
		else
		{
			printf("Memory Allocation To The 1D Array Of Base Addresses Of %d Rows Has Succeeded !!!\n\n", mdm_i);
		}
	}

	// **** Assigning Values **** 
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			*(*(mdm_ptr_iArray + mdm_i) + mdm_j) = (mdm_i + 1) * (mdm_j + 1); // 'mdm_ptr_iArray[mdm_i]' Can Be Treated As 1D Array Using Pointers...
		}
	}

	// **** Displaying Values ****
	printf("\n\n");
	printf("2D Integer Array Elements Along With Addresses : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("mdm_ptr_iArray_Row[%d][%d] = %d \t \t At Address &mdm_ptr_iArray_Row[%d][%d] : %p\n\n", mdm_i, mdm_j, mdm_ptr_iArray[mdm_i][mdm_j], mdm_i, mdm_j, &mdm_ptr_iArray[mdm_i][mdm_j]);
		}
		printf("\n\n");
	}

	// **** Freeing Allocated Memory ****

	// **** Freeing Memory Of Each Row ****
	for (mdm_i = (MDM_NUM_ROWS - 1); mdm_i >= 0; mdm_i--)
	{
		if (*(mdm_ptr_iArray + mdm_i)) // if(mdm_ptr_iArray[mdm_i])
		{
			free(*(mdm_ptr_iArray + mdm_i)); // free(mdm_ptr_iArray[mdm_i])
			*(mdm_ptr_iArray + mdm_i) = NULL; // mdm_ptr_iArray[mdm_i] = NULL;
			printf("Memory Allocated To Row %d Has Been Successfully Freed !!!\n\n", mdm_i);
		}
	}

	// **** Freeing Memory Of mdm_ptr_iArray Which Is The Array Of 5 Integer Pointers... That It Is An Array Having 5 Integer Addresses (Type int*) ****
	printf("\n\n");
	if (mdm_ptr_iArray)
	{
		free(mdm_ptr_iArray);
		mdm_ptr_iArray = NULL;
		printf("Memory Allocated To mdm_ptr_iArray Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}

