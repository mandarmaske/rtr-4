// header file
#include<stdio.h>

// macro definations
#define MDM_NUM_ROWS 5
#define MDM_NUM_COLUMNS 3 

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArray[MDM_NUM_ROWS][MDM_NUM_COLUMNS];
	int mdm_i, mdm_j;
	int* mdm_ptr_iArray_Row = NULL;

	// code
	// **** Every Row Of A 2D Array Is An Integer Array Itself Comprising Of 'MDM_NUM_COLUMNS' Integer Elements ****
	// **** There Are 5 Rows And 3 Columns In A 2D Integer Array, Each Of The The 5 Rows Is 1D Array Of 3 Integers ****  
	// **** Hence, Each Of These 5 Rows Themselves Being Arrays, Will Be The Base Addresses Of Their Respective Rows ****

	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		mdm_ptr_iArray_Row = mdm_iArray[mdm_i]; // 'mdm_iArray[mdm_i]' Is The Base Address Of The 'mdm_i'th Row 
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			*(mdm_ptr_iArray_Row + mdm_j) = (mdm_i + 1) * (mdm_j + 1); // 'mdm_ptr_iArray_Row' Can Be Treated As 1D Array Using Pointers
		}
	}

	printf("\n\n");
	printf("2D Integer Array Elements Along With Addresses : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		mdm_ptr_iArray_Row = mdm_iArray[mdm_i];  
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("*(mdm_ptr_iArray_Row + %d) = %d \t \t At Address (mdm_ptr_iArray_Row + mdm_j) : %p\n\n", mdm_j, *(mdm_ptr_iArray_Row + mdm_j), (mdm_ptr_iArray_Row + mdm_j));
		}
		printf("\n\n");
	}

	return(0);
}

