// header files
#include<stdio.h>
#include<stdlib.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int** mdm_ptr_iArray = NULL; // A Pointer to Pointer To Integer... But Can Also Hold Base Address Of A 2D Array Which Will Can Have Any Number Of Rows And Any Number Of Columns 
	int mdm_i, mdm_j;
	int mdm_num_rows, mdm_num_columns;

	// code

	// **** Accept Number Of Rows 'mdm_num_rows' From User ****
	printf("\n\n");
	printf("Enter Number Of Rows : ");
	scanf("%d", &mdm_num_rows);

	// **** Accept Number Of Columns 'mdm_num_columns' From User ****
	printf("Enter Number Of Columns : ");
	scanf("%d", &mdm_num_columns);

	// **** Allocating Memory To 1D Array Consisting Of Base Address Of Rows ****
	printf("\n\n");
	printf("******** Memory Allocation To 1D Integer Array ********\n\n");
	mdm_ptr_iArray = (int**)malloc(mdm_num_rows * sizeof(int*));
	if (mdm_ptr_iArray == NULL)
	{
		printf("Failed To Allocate Memory To %d Rows Of 2D Integer Array !!! Exitting Now...\n\n", mdm_num_rows);
		exit(0);
	}
	else
		printf("Memory Allocation To %d Rows Of 2D Integer Array Succeeded !!!\n\n, mdm_num_rows", mdm_num_rows);

	// **** Allocating Memory To Each Row Which Is A 1D Array Containing Consisting Of Columns Which Contain The Actual Integers ****
	for (mdm_i = 0; mdm_i < mdm_num_rows; mdm_i++)
	{
		mdm_ptr_iArray[mdm_i] = (int*)malloc(mdm_num_columns * sizeof(int)); // Allocating Memory (Number Of Columns * size of 'int') To Row 'i'
		if (mdm_ptr_iArray[mdm_i] == NULL)
		{
			printf("Failed To Allocate Memory To Columns Of Row %d Of 2D Integer Array !!! Exitting Now...\n\n", mdm_i);
			exit(0);
		}
		else
		{
			printf("Memory Allocation To Columns To Row %d Of 2D Integer Array Succeeded !!!\n\n", mdm_i);
		}
	}

	// **** Filling Up Values ****
	for (mdm_i = 0; mdm_i < mdm_num_rows; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < mdm_num_columns; mdm_j++)
		{
			mdm_ptr_iArray[mdm_i][mdm_j] = (mdm_i + 1) * (mdm_j + 1); // Can Also Use : *(*(mdm_ptr_iArray[mdm_i]) + mdm_j) = (mdm_i + 1) * (mdm_j + 1);
		}
	}

	// **** Displaying Values ****
	for (mdm_i = 0; mdm_i < mdm_num_rows; mdm_i++)
	{
		printf("Base Address Of Row %d : mdm_ptr_iArray[%d] = %p \t At Address : %p\n", mdm_i, mdm_i, mdm_ptr_iArray[mdm_i], &mdm_ptr_iArray[mdm_i]);
	}

	printf("\n\n");
	for (mdm_i = 0; mdm_i < mdm_num_rows; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < mdm_num_columns; mdm_j++)
		{
			printf("mdm_ptr_iArray[%d][%d] = %d \t At Address : %p\n", mdm_i, mdm_j, mdm_ptr_iArray[mdm_i][mdm_j], &mdm_ptr_iArray[mdm_i][mdm_j]); // Can Also Use : *(*(mdm_ptr_iArray[mdm_i]) + mdm_j) for value and *(mdm_ptr_iArray[mdm_i]) + mdm_j for address...
		}
		printf("\n");
	}

	// **** Freeing Memory Allocated To Each Row ****
	for (mdm_i = (mdm_num_rows - 1); mdm_i >= 0; mdm_i--)
	{
		if (mdm_ptr_iArray[mdm_i])
		{
			free(mdm_ptr_iArray[mdm_i]);
			mdm_ptr_iArray[mdm_i] = NULL;
			printf("Memory Allocated To Row %d Has Been Successfully Freed!!!\n\n", mdm_i);
		}
	}

	// **** Freeing Memory Allocated To 1D Array Consisting Of Base Address Of Rows ****
	if (mdm_ptr_iArray)
	{
		free(mdm_ptr_iArray);
		mdm_ptr_iArray = NULL;
		printf("Memory Allocated To mdm_ptr_iArray Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}