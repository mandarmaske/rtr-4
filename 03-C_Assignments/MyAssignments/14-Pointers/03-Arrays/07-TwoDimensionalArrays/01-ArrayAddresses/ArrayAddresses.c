// header files
#include<stdio.h>
#include<stdlib.h>

// macro definations
#define MDM_NUM_ROWS 5 
#define MDM_NUM_COLUMNS 3

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArray[MDM_NUM_ROWS][MDM_NUM_COLUMNS];
	int mdm_i, mdm_j;

	// code
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
			mdm_iArray[mdm_i][mdm_j] = (mdm_i + 1) * (mdm_j + 1);
	}

	printf("\n\n");
	printf("2D Integer Array Elements Along With Addresses : \n\n");
	for (mdm_i = 0; mdm_i < MDM_NUM_ROWS; mdm_i++)
	{
		for (mdm_j = 0; mdm_j < MDM_NUM_COLUMNS; mdm_j++)
		{
			printf("mdm_iArray[%d][%d] = %d \t \t At Address : %p\n", mdm_i, mdm_j, mdm_iArray[mdm_i][mdm_j], &mdm_iArray[mdm_i][mdm_j]);
		}
		printf("\n\n");
	}

	return(0);
}

