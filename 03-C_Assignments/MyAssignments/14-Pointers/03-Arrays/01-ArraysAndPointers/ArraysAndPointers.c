// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArray[] = { 12, 24, 36, 48, 60, 72, 84, 96, 108, 120 };
	float mdm_fArray[] = { 9.8f, 8.7f, 7.6f, 6.5f, 5.4f };
	double mdm_dArray[] = { 1.222222, 2.333333, 3.444444 };
	char mdm_cArray[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P' };

	//code
	printf("\n\n");
	printf("Integer Array Elements And The Address They Occupy Are As Follows : \n\n");
	printf("mdm_iArray[0]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 0), (mdm_iArray + 0));
	printf("mdm_iArray[1]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 1), (mdm_iArray + 1));
	printf("mdm_iArray[2]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 2), (mdm_iArray + 2));
	printf("mdm_iArray[3]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 3), (mdm_iArray + 3));
	printf("mdm_iArray[4]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 4), (mdm_iArray + 4));
	printf("mdm_iArray[5]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 5), (mdm_iArray + 5));
	printf("mdm_iArray[6]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 6), (mdm_iArray + 6));
	printf("mdm_iArray[7]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 7), (mdm_iArray + 7));
	printf("mdm_iArray[8]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 8), (mdm_iArray + 8));
	printf("mdm_iArray[9]    =   %d   \t At Address   :   %p\n", *(mdm_iArray + 9), (mdm_iArray + 9));

	printf("\n\n");
	printf("Float Array Elements And The Address They Occupy Are As Follows : \n\n");
	printf("mdm_fArray[0]    =   %f   \t At Address   :   %p\n", *(mdm_fArray + 0), (mdm_fArray + 0));
	printf("mdm_fArray[1]    =   %f   \t At Address   :   %p\n", *(mdm_fArray + 1), (mdm_fArray + 1));
	printf("mdm_fArray[2]    =   %f   \t At Address   :   %p\n", *(mdm_fArray + 2), (mdm_fArray + 2));
	printf("mdm_fArray[3]    =   %f   \t At Address   :   %p\n", *(mdm_fArray + 3), (mdm_fArray + 3));
	printf("mdm_fArray[4]    =   %f   \t At Address   :   %p\n", *(mdm_fArray + 4), (mdm_fArray + 4));

	printf("\n\n");
	printf("Double Array Elements And The Address They Occupy Are As Follows : \n\n");
	printf("mdm_dArray[0]    =   %lf   \t At Address   :   %p\n", *(mdm_dArray + 0), (mdm_dArray + 0));
	printf("mdm_dArray[1]    =   %lf   \t At Address   :   %p\n", *(mdm_dArray + 1), (mdm_dArray + 1));
	printf("mdm_dArray[2]    =   %lf   \t At Address   :   %p\n", *(mdm_dArray + 2), (mdm_dArray + 2));

	printf("\n\n");
	printf("Character Array Elements And The Address They Occupy Are As Follows : \n\n");
	printf("mdm_cArray[0]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 0), (mdm_cArray + 0));
	printf("mdm_cArray[1]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 1), (mdm_cArray + 1));
	printf("mdm_cArray[2]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 2), (mdm_cArray + 2));
	printf("mdm_cArray[3]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 3), (mdm_cArray + 3));
	printf("mdm_cArray[4]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 4), (mdm_cArray + 4));
	printf("mdm_cArray[5]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 5), (mdm_cArray + 5));
	printf("mdm_cArray[6]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 6), (mdm_cArray + 6));
	printf("mdm_cArray[7]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 7), (mdm_cArray + 7));
	printf("mdm_cArray[8]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 8), (mdm_cArray + 8));
	printf("mdm_cArray[9]    =   %c   \t At Address   :   %p\n", *(mdm_cArray + 9), (mdm_cArray + 9));
	printf("mdm_cArray[10]   =   %c   \t At Address   :   %p\n", *(mdm_cArray + 10), (mdm_cArray + 10));
	printf("mdm_cArray[11]   =   %c   \t At Address   :   %p\n", *(mdm_cArray + 11), (mdm_cArray + 11));
	printf("mdm_cArray[12]   =   %c   \t At Address   :   %p\n\n", *(mdm_cArray + 12), (mdm_cArray + 12));

	return(0);
}