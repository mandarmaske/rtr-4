// header files
#include<stdio.h>
#include<stdlib.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// function declarations
	void MDM_MyAlloc(int**, unsigned int);

	// variable declarations
	int* mdm_piArray = NULL;
	unsigned int mdm_num_elements;
	int mdm_i;

	// code
	printf("\n\n");
	printf("How Many Elements You Want In Integer Array ? \n\n");
	scanf("%u", &mdm_num_elements);

	printf("\n\n");
	MDM_MyAlloc(&mdm_piArray, mdm_num_elements);

	printf("Enter %u Elements To Fill Up Your Integer Array : \n\n", mdm_num_elements);
	for (mdm_i = 0; mdm_i < mdm_num_elements; mdm_i++)
		scanf("%d", &mdm_piArray[mdm_i]);

	printf("\n\n");
	printf("The %u Elements Entered By You In The Integer Array : \n\n", mdm_num_elements);
	for (mdm_i = 0; mdm_i < mdm_num_elements; mdm_i++)
		printf("%u\n", mdm_piArray[mdm_i]);

	printf("\n\n");
	if (mdm_piArray)
	{
		free(mdm_piArray);
		mdm_piArray = NULL;
		printf("Memory Allocated Has Now Been Successfully Freed!!! \n\n");
	}

	return(0);
}

void MDM_MyAlloc(int** mdm_ptr, unsigned int mdm_NumberOfElements)
{
	// code
	*mdm_ptr = (int*)malloc(mdm_NumberOfElements * sizeof(int));
	if (*mdm_ptr == NULL)
	{
		printf("Could Not Allocate Memory !!! Exitting Now... \n\n");
		exit(0);
	}

	printf("MDM_MyAlloc() Has Successfully Allocated %lu Bytes For Integer Array !!!\n\n", (mdm_NumberOfElements * sizeof(int)));
}