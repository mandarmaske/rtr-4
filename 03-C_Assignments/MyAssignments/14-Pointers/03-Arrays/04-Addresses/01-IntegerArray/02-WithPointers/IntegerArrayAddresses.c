// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	int mdm_iArray[10];
	int* mdm_ptr_iArray = NULL;
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_iArray[mdm_i] = (mdm_i + 1) * 3;

	// **** Name Of Any Array Is Its Base Address ****
	// **** Hence, 'mdm_iArray' Is The Base Address Of Array mdm_iArray[] Or 'mdm_iArray' Is The Address Of Element mdm_iArray[0] ****
	// **** Assigning Base Address Of Array 'mdm_iArray[]' To Integer Pointer 'mdm_ptr_iArray' ****

	mdm_ptr_iArray = mdm_iArray; // mdm_ptr_iArray = &mdm_iArray[0];

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_iArray[%d] = %d\n", mdm_i, *(mdm_ptr_iArray + mdm_i));

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_iArray[%d] = %d \t Address = %p\n", mdm_i, *(mdm_ptr_iArray + mdm_i), (mdm_ptr_iArray + mdm_i));

	printf("\n\n");

	return(0);
}