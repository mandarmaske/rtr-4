// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	int mdm_iArray[10];
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_iArray[mdm_i] = (mdm_i + 1) * 3;

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_iArray[%d] = %d\n", mdm_i, mdm_iArray[mdm_i]);

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_iArray[%d] = %d \t Address = %p\n", mdm_i, mdm_iArray[mdm_i], &mdm_iArray[mdm_i]);

	return(0);
}