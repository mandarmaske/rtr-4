// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	double mdm_dArray[10];
	double* mdm_ptr_dArray = NULL;
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_dArray[mdm_i] = (float)(mdm_i + 1) * 1.333333f;

	// **** Name Of Any Array Is Its Base Address ****
	// **** Hence, 'mdm_dArray' Is The Base Address Of Array mdm_dArray[] Or 'mdm_dArray' Is The Address Of Element mdm_dArray[0] ****
	// **** Assigning Base Address Of Array 'mdm_dArray[]' To Integer Pointer 'mdm_ptr_dArray' ****

	mdm_ptr_dArray = mdm_dArray; // mdm_ptr_dArray = &mdm_dArray[0];

	printf("\n\n");
	printf("Elements Of The 'double' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_dArray[%d] = %lf\n", mdm_i, *(mdm_ptr_dArray + mdm_i));

	printf("\n\n");
	printf("Elements Of The 'double' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_dArray[%d] = %lf \t Address = %p\n", mdm_i, *(mdm_ptr_dArray + mdm_i), (mdm_ptr_dArray + mdm_i));

	printf("\n\n");

	return(0);
}