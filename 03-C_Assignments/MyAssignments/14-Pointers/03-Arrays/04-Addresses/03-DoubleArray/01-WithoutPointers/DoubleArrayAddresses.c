// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	double mdm_dArray[10];
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_dArray[mdm_i] = (float)(mdm_i + 1) * 1.333333f;

	printf("\n\n");
	printf("Elements Of The 'double' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_dArray[%d] = %lf\n", mdm_i, mdm_dArray[mdm_i]);

	printf("\n\n");
	printf("Elements Of The 'double' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_dArray[%d] = %lf \t Address = %p\n", mdm_i, mdm_dArray[mdm_i], &mdm_dArray[mdm_i]);

	return(0);
}