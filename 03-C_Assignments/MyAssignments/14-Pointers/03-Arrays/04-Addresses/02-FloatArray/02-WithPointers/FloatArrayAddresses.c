// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	float mdm_fArray[10];
	float* mdm_ptr_fArray = NULL;
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_fArray[mdm_i] = (mdm_i + 1) * 1.5f;

	// **** Name Of Any Array Is Its Base Address ****
	// **** Hence, 'mdm_fArray' Is The Base Address Of Array mdm_fArray[] Or 'mdm_fArray' Is The Address Of Element mdm_fArray[0] ****
	// **** Assigning Base Address Of Array 'mdm_fArray[]' To Integer Pointer 'mdm_ptr_fArray' ****

	mdm_ptr_fArray = mdm_fArray; // mdm_ptr_fArray = &mdm_fArray[0];

	printf("\n\n");
	printf("Elements Of The 'float' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_fArray[%d] = %f\n", mdm_i, *(mdm_ptr_fArray + mdm_i));

	printf("\n\n");
	printf("Elements Of The 'float' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_fArray[%d] = %f \t Address = %p\n", mdm_i, *(mdm_ptr_fArray + mdm_i), (mdm_ptr_fArray + mdm_i));

	printf("\n\n");

	return(0);
}