// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	float mdm_fArray[10];
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_fArray[mdm_i] = (float)(mdm_i + 1) * 1.5f;

	printf("\n\n");
	printf("Elements Of The 'float' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_fArray[%d] = %f\n", mdm_i, mdm_fArray[mdm_i]);

	printf("\n\n");
	printf("Elements Of The 'float' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_fArray[%d] = %f \t Address = %p\n", mdm_i, mdm_fArray[mdm_i], &mdm_fArray[mdm_i]);

	return(0);
}