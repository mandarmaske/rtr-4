// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	char mdm_cArray[10];
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_cArray[mdm_i] = (char)(mdm_i + 65);

	printf("\n\n");
	printf("Elements Of The 'char' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_cArray[%d] = %c\n", mdm_i, mdm_cArray[mdm_i]);

	printf("\n\n");
	printf("Elements Of The 'char' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_cArray[%d] = %c \t Address = %p\n", mdm_i, mdm_cArray[mdm_i], &mdm_cArray[mdm_i]);

	return(0);
}