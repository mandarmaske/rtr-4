// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// Variable Declarations
	char mdm_cArray[10];
	char* mdm_ptr_cArray = NULL;
	int mdm_i;

	// code
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		mdm_cArray[mdm_i] = (char)(mdm_i + 65);

	// **** Name Of Any Array Is Its Base Address ****
	// **** Hence, 'mdm_cArray' Is The Base Address Of Array mdm_cArray[] Or 'mdm_cArray' Is The Address Of Element mdm_cArray[0] ****
	// **** Assigning Base Address Of Array 'mdm_cArray[]' To Integer Pointer 'mdm_ptr_cArray' ****

	mdm_ptr_cArray = mdm_cArray; // mdm_ptr_cArray = &mdm_cArray[0];

	printf("\n\n");
	printf("Elements Of The 'char' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_cArray[%d] = %c\n", mdm_i, *(mdm_ptr_cArray + mdm_i));

	printf("\n\n");
	printf("Elements Of The 'char' Array : \n\n");
	for (mdm_i = 0; mdm_i < 10; mdm_i++)
		printf("mdm_cArray[%d] = %c \t Address = %p\n", mdm_i, *(mdm_ptr_cArray + mdm_i), (mdm_ptr_cArray + mdm_i));

	printf("\n\n");

	return(0);
}