// header file
#include<stdio.h>
#include<stdlib.h>

// macros defination
#define MDM_INT_SIZE sizeof(int)
#define MDM_FLOAT_SIZE sizeof(float)
#define MDM_DOUBLE_SIZE sizeof(double)
#define MDM_CHAR_SIZE sizeof(char)

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int* mdm_ptr_iArray = NULL;
	unsigned int mdm_iArrayLength = 0;

	float* mdm_ptr_fArray = NULL;
	unsigned int mdm_fArrayLength = 0;

	double* mdm_ptr_dArray = NULL;
	unsigned int mdm_dArrayLength = 0;

	char* mdm_ptr_cArray = NULL;
	unsigned int mdm_cArrayLength = 0;

	int mdm_i;

	// code

	// **** Integer Array **** 
	printf("\n\n");
	printf("Enter The Number Of Elements You Want In The Integer Array : ");
	scanf("%u", &mdm_iArrayLength);

	mdm_ptr_iArray = (int*)malloc(MDM_INT_SIZE * mdm_iArrayLength);
	if (mdm_ptr_iArray == NULL)
	{
		printf("\n\n");
		printf("Memory Allocation For Integer Array Failed !!! Exitting Now... \n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory Allocation For Integer Array Succeded !!!\n\n");
	}

	printf("\n\n");
	printf("Enter The %d Integer Elements To Fill Up The Integer Array : \n\n", mdm_iArrayLength);
	for (mdm_i = 0; mdm_i < mdm_iArrayLength; mdm_i++)
		scanf("%d", (mdm_ptr_iArray + mdm_i));


	// **** Float Array ****
	printf("\n\n");
	printf("Enter The Number Of Elements You Want In The 'float' Array : ");
	scanf("%u", &mdm_fArrayLength);

	mdm_ptr_fArray = (float*)malloc(MDM_FLOAT_SIZE * mdm_fArrayLength);
	if (mdm_ptr_fArray == NULL)
	{
		printf("\n\n");
		printf("Memory Allocation For Floating-Point Array Failed !!! Exitting Now... \n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory Allocation For Floating-Point Array Succeded !!!\n\n");
	}

	printf("\n\n");
	printf("Enter The %d Floating-Point Elements To Fill Up The Float Array : \n\n", mdm_fArrayLength);
	for (mdm_i = 0; mdm_i < mdm_fArrayLength; mdm_i++)
		scanf("%f", (mdm_ptr_fArray + mdm_i));

	// **** Double Array ****
	printf("\n\n");
	printf("Enter The Number Of Elements You Want In The 'double' Array : ");
	scanf("%u", &mdm_dArrayLength);

	mdm_ptr_dArray = (double*)malloc(MDM_DOUBLE_SIZE * mdm_dArrayLength);
	if (mdm_ptr_dArray == NULL)
	{
		printf("\n\n");
		printf("Memory Allocation For Double Array Failed !!! Exitting Now... \n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory Allocation For Double Array Succeded !!!\n\n");
	}

	printf("\n\n");
	printf("Enter The %d Double Elements To Fill Up The Integer Array : \n\n", mdm_dArrayLength);
	for (mdm_i = 0; mdm_i < mdm_dArrayLength; mdm_i++)
		scanf("%lf", (mdm_ptr_dArray + mdm_i));

	// **** Char Array ****
	printf("\n\n");
	printf("Enter The Number Of Elements You Want In The 'char' Array : ");
	scanf("%u", &mdm_cArrayLength);

	mdm_ptr_cArray = (char*)malloc(MDM_CHAR_SIZE * mdm_cArrayLength);
	if (mdm_ptr_cArray == NULL)
	{
		printf("\n\n");
		printf("Memory Allocation For Char Array Failed !!! Exitting Now... \n\n");
		exit(0);
	}
	else
	{
		printf("\n\n");
		printf("Memory Allocation For Char Array Succeded !!!\n\n");
	}

	printf("\n\n");
	printf("Enter The %d Character Elements To Fill Up The Character Array : \n\n", mdm_cArrayLength);
	for (mdm_i = 0; mdm_i < mdm_cArrayLength; mdm_i++)
	{
		*(mdm_ptr_cArray + mdm_i) = getch();
		printf("%c", *(mdm_ptr_cArray + mdm_i));
	}

	// ******** Display Of Arrays ********

	// **** Integer Array ****
	printf("\n\n");
	printf("The Integer Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", mdm_iArrayLength);
	for (mdm_i = 0; mdm_i < mdm_iArrayLength; mdm_i++)
		printf(" %d \t \t At Address : %p\n", *(mdm_ptr_iArray + mdm_i), (mdm_ptr_iArray + mdm_i));

	// **** Float Array ****
	printf("\n\n");
	printf("The Float Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", mdm_fArrayLength);
	for (mdm_i = 0; mdm_i < mdm_fArrayLength; mdm_i++)
		printf(" %f \t \t At Address : %p\n", *(mdm_ptr_fArray + mdm_i), (mdm_ptr_fArray + mdm_i));

	// **** Double Array ****
	printf("\n\n");
	printf("The Double Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", mdm_dArrayLength);
	for (mdm_i = 0; mdm_i < mdm_dArrayLength; mdm_i++)
		printf(" %lf \t \t At Address : %p\n", *(mdm_ptr_dArray + mdm_i), (mdm_ptr_dArray + mdm_i));

	// **** Char Array ****
	printf("\n\n");
	printf("The Char Array Entered By You And Consisting Of %d Elements Is As Follows : \n\n", mdm_cArrayLength);
	for (mdm_i = 0; mdm_i < mdm_cArrayLength; mdm_i++)
		printf(" %c \t \t At Address : %p\n", *(mdm_ptr_cArray + mdm_i), (mdm_ptr_cArray + mdm_i));

	// ******** Freeing Memory Occupied By Arrays (In Reverse Order Of Allocation) ********
	if (mdm_ptr_cArray)
	{
		free(mdm_ptr_cArray);
		mdm_ptr_cArray = NULL;
		printf("\n\n");
		printf("Memory Occupied By Character Array Has Been Successfully Freed !!!\n\n");
	}

	if (mdm_ptr_dArray)
	{
		free(mdm_ptr_dArray);
		mdm_ptr_dArray = NULL;
		printf("\n\n");
		printf("Memory Occupied By Double Array Has Been Successfully Freed !!!\n\n");
	}

	if (mdm_ptr_fArray)
	{
		free(mdm_ptr_fArray);
		mdm_ptr_fArray = NULL;
		printf("\n\n");
		printf("Memory Occupied By Floating Point Array Has Been Successfully Freed !!!\n\n");
	}

	if (mdm_ptr_iArray)
	{
		free(mdm_ptr_iArray);
		mdm_ptr_iArray = NULL;
		printf("\n\n");
		printf("Memory Occupied By Integer Array Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}