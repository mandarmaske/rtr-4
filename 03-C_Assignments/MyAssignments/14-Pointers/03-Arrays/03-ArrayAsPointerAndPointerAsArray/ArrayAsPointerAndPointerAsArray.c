// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_iArray[] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 }; // Integer Array
	int* mdm_ptr_iArray = NULL; // Integer Pointer

	// code

	// **** Using Array Name As  A Pointer i.e :
	// Value Of xth Element Of mdm_iArray : mdm_iArray[x] And Address Of xth Element Of mdm_iArray : &mdm_iArray[mdm_x] ****
	printf("\n\n");
	printf("**** Using Array Name As A Pointer i.e :Value Of xth Element Of mdm_iArray : mdm_iArray[x] And Address Of xth Element Of mdm_iArray : &mdm_iArray[mdm_x] ****\n\n");
	printf("Integer Array Elements And Their Addresses : \n\n");
	printf("*(mdm_iArray + 0) = %d \t \t At Address &(mdm_iArray + 0) : %pt\n", *(mdm_iArray + 0), (mdm_iArray + 0));
	printf("*(mdm_iArray + 1) = %d \t \t At Address &(mdm_iArray + 2) : %pt\n", *(mdm_iArray + 1), (mdm_iArray + 1));
	printf("*(mdm_iArray + 2) = %d \t \t At Address &(mdm_iArray + 2) : %pt\n", *(mdm_iArray + 2), (mdm_iArray + 2));
	printf("*(mdm_iArray + 3) = %d \t \t At Address &(mdm_iArray + 3) : %pt\n", *(mdm_iArray + 3), (mdm_iArray + 3));
	printf("*(mdm_iArray + 4) = %d \t \t At Address &(mdm_iArray + 4) : %pt\n", *(mdm_iArray + 4), (mdm_iArray + 4));
	printf("*(mdm_iArray + 5) = %d \t \t At Address &(mdm_iArray + 5) : %pt\n", *(mdm_iArray + 5), (mdm_iArray + 5));
	printf("*(mdm_iArray + 6) = %d \t \t At Address &(mdm_iArray + 6) : %pt\n", *(mdm_iArray + 6), (mdm_iArray + 6));
	printf("*(mdm_iArray + 7) = %d \t \t At Address &(mdm_iArray + 7) : %pt\n", *(mdm_iArray + 7), (mdm_iArray + 7));
	printf("*(mdm_iArray + 8) = %d \t \t At Address &(mdm_iArray + 8) : %pt\n", *(mdm_iArray + 8), (mdm_iArray + 8));
	printf("*(mdm_iArray + 9) = %d \t At Address &(mdm_iArray + 9) : %pt\n\n", *(mdm_iArray + 9), (mdm_iArray + 9));

	// Assigning Base Addresss Of Integer Array 'mdm_iArray' To Integer Pointer 'mdm_ptr_iArray'
	// Name Of Any Array Is It's Own Base Address
	mdm_ptr_iArray = mdm_iArray; // Same As mdm_ptr_iArray = &mdm_iArray[0]

	// **** Using Pointer As Array Name i.e : Value Of xth Element Of mdm_iArray : *(mdm_ptr_iArray + x) And Address Of xth Element Of mdm_iArray : (mdm_ptr_iArray) **** 
	printf("\n\n");
	printf("Using Pointer As Array Name i.e : Value Of xth Element Of mdm_iArray : *(mdm_ptr_iArray + x) And Address Of xth Element Of mdm_iArray : (mdm_ptr_iArray) ****\n\n");
	printf("Integer Array Elements And Their Addresses : \n\n");
	printf("mdm_ptr_iArray[0] = %d \t \t At Address &mdm_ptr_iArray[0] : %p\n", mdm_ptr_iArray[0], &mdm_ptr_iArray[0]);
	printf("mdm_ptr_iArray[1] = %d \t \t At Address &mdm_ptr_iArray[1] : %p\n", mdm_ptr_iArray[1], &mdm_ptr_iArray[1]);
	printf("mdm_ptr_iArray[2] = %d \t \t At Address &mdm_ptr_iArray[2] : %p\n", mdm_ptr_iArray[2], &mdm_ptr_iArray[2]);
	printf("mdm_ptr_iArray[3] = %d \t \t At Address &mdm_ptr_iArray[3] : %p\n", mdm_ptr_iArray[3], &mdm_ptr_iArray[3]);
	printf("mdm_ptr_iArray[4] = %d \t \t At Address &mdm_ptr_iArray[4] : %p\n", mdm_ptr_iArray[4], &mdm_ptr_iArray[4]);
	printf("mdm_ptr_iArray[5] = %d \t \t At Address &mdm_ptr_iArray[5] : %p\n", mdm_ptr_iArray[5], &mdm_ptr_iArray[5]);
	printf("mdm_ptr_iArray[6] = %d \t \t At Address &mdm_ptr_iArray[6] : %p\n", mdm_ptr_iArray[6], &mdm_ptr_iArray[6]);
	printf("mdm_ptr_iArray[7] = %d \t \t At Address &mdm_ptr_iArray[7] : %p\n", mdm_ptr_iArray[7], &mdm_ptr_iArray[7]);
	printf("mdm_ptr_iArray[8] = %d \t \t At Address &mdm_ptr_iArray[8] : %p\n", mdm_ptr_iArray[8], &mdm_ptr_iArray[8]);
	printf("mdm_ptr_iArray[9] = %d \t At Address &mdm_ptr_iArray[9] : %p\n\n", mdm_ptr_iArray[9], &mdm_ptr_iArray[9]);

	return(0);
}