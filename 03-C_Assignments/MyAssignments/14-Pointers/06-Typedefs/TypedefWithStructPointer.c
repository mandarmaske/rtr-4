// header files
#include<stdio.h>
#include<stdlib.h>

// defining struct
struct MDM_MyData
{
	int mdm_i;
	float mdm_f;
	double mdm_d;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_i_size;
	int mdm_f_size;
	int mdm_d_size;
	int mdm_struct_MDM_MyData_size;
	int mdm_pointer_to_struct_MDM_MyDara_size;

	typedef struct MDM_MyData* mdm_DataPtr;

	mdm_DataPtr mdm_pData;

	// code
	printf("\n\n");

	mdm_pData = (mdm_DataPtr)malloc(sizeof(struct MDM_MyData));
	if (mdm_pData == NULL)
	{
		printf("Failed To Allocate Memory To 'struct MDM_MyData' !!! Exitting Now...\n\n");
		exit(0);
	}
	else
		printf("Successfully Allocated Memory To 'struct mdm_MyData' !!!\n\n");

	// Assigning Data Values To The Data Members Of 'struct MDM_MyData'
	(*mdm_pData).mdm_i = 30;
	(*mdm_pData).mdm_f = 11.45f;
	(*mdm_pData).mdm_d = 1.234455;

	// Displaying Values Of The Data Members Of 'struct MDM_MyData'
	printf("\n\n");
	printf("Data Members Of 'struct MDM_MyData' Are : \n\n");
	printf("mdm_i = %d\n", (*mdm_pData).mdm_i);
	printf("mdm_f = %f\n", (*mdm_pData).mdm_f);
	printf("mdm_d = %lf\n", (*mdm_pData).mdm_d);

	// Calculating Sizes (In Bytes) Of The Data Members Of 'struct MDM_MyData'
	mdm_i_size = sizeof((*mdm_pData).mdm_i);
	mdm_f_size = sizeof((*mdm_pData).mdm_f);
	mdm_d_size = sizeof((*mdm_pData).mdm_d);

	// Displaying Sizes (In Bytes) Of The Data Members Of 'struct MDM_MyData'
	printf("\n\n");
	printf("Sizes (In Bytes) Of Data Members Of 'struct MDM_MyData' Are : \n\n");
	printf("Size Of 'mdm_i' = %d Bytes\n", mdm_i_size);
	printf("Size Of 'mdm_f' = %d Bytes\n", mdm_f_size);
	printf("Size Of 'mdm_d' = %d Bytes\n", mdm_d_size);

	// Calculating Size (In Bytes) Of The Entire 'struct MDM_MyData'
	mdm_struct_MDM_MyData_size = sizeof(struct MDM_MyData);
	mdm_pointer_to_struct_MDM_MyDara_size = sizeof(struct MDM_MyData*);

	// Displaying Sizes(In Bytes) Of The Entire 'struct MDM_MyData'
	printf("\n\n");
	printf("Size Of 'struct MDM_MyData' : %d Bytes\n\n", mdm_struct_MDM_MyData_size);
	printf("Size Of Pointer To 'struct MDM_MyData' : %d\n\n", mdm_pointer_to_struct_MDM_MyDara_size);

	if (mdm_pData)
	{
		free(mdm_pData);
		mdm_pData = NULL;
		printf("Memory Allocated To 'struct MDM_MyData' Has Been Successfully Freed !!!\n\n");
	}

	return(0);
}

