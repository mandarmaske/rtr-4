// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_num = 5;
	int* const mdm_ptr = &mdm_num; // Read This Line From Right To Left -> "mdm_ptr is a constant (const) pointer (*) to integer (int)."

	// code
	printf("\n\n");
	printf("Current Value Of 'mdm_num' = %d\n", mdm_num);
	printf("Current 'mdm_ptr' (Address Of 'mdm_num') = %p\n", mdm_ptr);

	// The following lines does NOT Give Error... as we Are modifying the value of the variable individually
	mdm_num++;
	printf("\n\n");
	printf("After mdm_num++, value of 'mdm_num' = %d\n", mdm_num);

	// The following lines gives error and is hence commented out.
	// We cannot alter 'mdm_ptr' value as 'mdm_ptr' is a "Constant Pointer To Variable Integer"
	// With respect to the pointer, the value it points to is not constant but the pointer itself is constant.
	// Uncomment is and see the error
	
	// mdm_ptr++;

	// The following lines does not give error
	// we do not get error because we are changing the value at a constant pointer (addresss).
	// The pointer is constant. The value to which the pointer points is NOT CONSTANT
	(*mdm_ptr)++;

	printf("\n\n");
	printf("After (*mdm_ptr)++ value of 'mdm_ptr' = %p\n", mdm_ptr);
	printf("Value at this 'mdm_ptr' = %d\n", *mdm_ptr);
	printf("\n\n");

	return(0);
}

// Conclusion : 
// As 'mdm_ptr' is a "constant pointer to variable integer". - We can change the value stored at addresss 'mdm_ptr' but we cannot change the 'mdm_ptr' (Address) itself
// We can change the value of the variable (mdm_num) individually - whose address is contained in 'mdm_ptr'
// We can also change the "The Value At Address Of mdm_ptr" - we can change the value of 'mdm_num' with respect to 'mdm_ptr' -> (*ptr)++ is allowed
// We Cannot change the value of 'mdm_ptr' -> That is we cannot store a new address inside 'mdm_ptr' -> So, mdm_ptr++ is NOT ALLOWED