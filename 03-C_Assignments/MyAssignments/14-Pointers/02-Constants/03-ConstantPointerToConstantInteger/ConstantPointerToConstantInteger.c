// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_num = 5;
	const int* const mdm_ptr = &mdm_num; // Read This Line From Right To Left -> "mdm_ptr is a constant (const) pointer (*) to integer (int) constant (const)."

	// code
	printf("\n\n");
	printf("Current Value Of 'mdm_num' = %d\n", mdm_num);
	printf("Current 'mdm_ptr' (Address Of 'mdm_num') = %p\n", mdm_ptr);

	// The following lines does NOT Give Error... as we Are modifying the value of the variable individually
	mdm_num++;
	printf("\n\n");
	printf("After mdm_num++, value of 'mdm_num' = %d\n", mdm_num);

	// The following lines gives error and is hence commented out.
	// We cannot alter 'mdm_ptr' value as 'mdm_ptr' is a "Constant Pointer To Constant Integer"
	// With respect to the pointer, the value it points to is constant and the pointer itself is constant.
	// Uncomment is and see the error

	// mdm_ptr++;
	
	// The following line also gives error and is hence commented out.
	// We cannot alter the value stored in 'mdm_ptr' as 'mdm_ptr' is "A Constant Pointer To Constant Integer"
	// With respect to the pointer, the value it points to is constant and the pointer itself is constant.
	// Uncomment is and see the error

	// (*mdm_ptr)++;

	return(0);
}

// Conclusion : 
// As 'mdm_ptr' is a "constant pointer to constant integer". - We cannot change the value stored at addresss 'mdm_ptr' and we cannot change the 'mdm_ptr' (Address) itself
// We can change the value of the variable (mdm_num) individually - whose address is contained in 'mdm_ptr'
// We cannot also change the "The Value At Address Of mdm_ptr" - we cannot change the value of 'mdm_num' with respect to 'mdm_ptr' -> (*ptr)++ is NOW ALLOWED
// We Cannot change the value of 'mdm_ptr' -> That is we cannot store a new address inside 'mdm_ptr' -> So, mdm_ptr++ is NOT ALLOWED

