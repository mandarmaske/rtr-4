// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_num = 5;
	const int* mdm_ptr = NULL; // Read This Line From Right To Left -> "mdm_ptr is a pointer(*) to integer (int) constant(const)."

	// code
	mdm_ptr = &mdm_num;
	printf("\n\n");
	printf("Current Value Of 'mdm_num' = %d\n", mdm_num);
	printf("Current 'mdm_ptr' (Address Of 'mdm_num') = %p\n", mdm_ptr);

	// The following lines does NOT Give Error... as we Are modifying the value of the variable individually
	mdm_num++;
	printf("\n\n");
	printf("After mdm_num++, value of 'mdm_num' = %d\n", mdm_num);

	// The following lines gives error and is hence commented out.
	// We cannot alter the value stored in "A Pointer To Constant Integer"
	// With respect to the pointer, the value it points to Should be constant.
	// Uncomment this and see the error

	// (*mdm_ptr)++;

	// The following lines does not give error
	// we do not get error because we are changing the pointer (address).
	// The pointer is not constant. The value to which the pointer points is constant
	mdm_ptr++;

	printf("\n\n");
	printf("After mdm_ptr++ value of 'mdm_ptr' = %p\n", mdm_ptr);
	printf("Value at this new 'mdm_ptr' = %d\n", *mdm_ptr);
	printf("\n\n");

	return(0);
}

// Conclusion : 
// As 'mdm_ptr' is a "variable pointer to constant integer". - We can change the value of pointer 'mdm_ptr'
// We can change the value of the variable (mdm_num) individually - whose address is contained in 'mdm_ptr'
// So, in short, we cannot change "The Value At Address Of mdm_ptr" - we cannot change the value of 'mdm_num' with respect to 'mdm_ptr' -> (*ptr)++ is Not Allowed
// But, we can change the address 'mdm_ptr' itself -> So, mdm_ptr++ is allowed

