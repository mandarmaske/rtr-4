// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_num;
	int* mdm_ptr = NULL;
	int* mdm_copy_ptr = NULL;

	// code
	mdm_num = 5;
	mdm_ptr = &mdm_num;

	printf("\n\n");
	printf("******** Before mdm_copy_ptr = mdm_ptr ********\n\n");
	printf("mdm_num                = %d\n", mdm_num);
	printf("&mdm_num               = %p\n", &mdm_num);
	printf("*(&mdm_num)            = %d\n", *(&mdm_num));
	printf("mdm_ptr                = %p\n", mdm_ptr);
	printf("*mdm_ptr               = %d\n", *mdm_ptr);

	// 'mdm_ptr' is an integer pointer variable...that it can hold the address of any integer variable only 
	// 'mdm_copy_ptr' is another integer pointer variable
	// If mdm_ptr = &mdm_num... 'mdm_ptr' will contain address of integer variable 'mdm_num'
	// If 'mdm_ptr' is assigned to 'mdm_copy_ptr', 'mdm_copy_ptr' will also contain address of integer variable 'mdm_num'
	// Hence, now both 'mdm_ptr' and 'mdm_copy_ptr' will pointer to 'mdm_num'

	mdm_copy_ptr = mdm_ptr;

	printf("\n\n");
	printf("******** After mdm_copy_ptr = mdm_ptr ********\n\n");
	printf("mdm_num                = %d\n", mdm_num);
	printf("&mdm_num               = %p\n", &mdm_num);
	printf("*(&mdm_num)            = %d\n", *(&mdm_num));
	printf("mdm_ptr                = %p\n", mdm_ptr);
	printf("*mdm_ptr               = %d\n", *mdm_ptr);
	printf("mdm_copy_ptr           = %p\n", mdm_copy_ptr);
	printf("*mdm_copy_ptr          = %d\n", *mdm_copy_ptr);

	return(0);
}

