// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_num;
	int* mdm_ptr = NULL;
	int** mdm_pptr = NULL; // Declarations Method1 :- **mdm_pptr Is A Variable Of Type 'int'

	// code
	mdm_num = 10;

	printf("\n\n");
	printf("******** Before mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'                 = %d\n\n", mdm_num);
	printf("Address Of 'mdm_num'               = %p\n\n", &mdm_num);
	printf("Value At Address Of 'mdm_num'      = %d\n\n", *(&mdm_num));

	// Assigning Address Of Variable 'mdm_num' to pointer variable 'mdm_ptr'
	// 'mdm_ptr' now contains address of 'mdm_num'... hence, 'mdm_ptr' Is SAME as &mdm_num
	mdm_ptr = &mdm_num;

	printf("\n\n");
	printf("******** After mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'                 = %d\n\n", mdm_num);
	printf("Address Of 'mdm_num'               = %p\n\n", mdm_ptr);
	printf("Value At Address Of 'mdm_num'      = %d\n\n", *mdm_ptr);

	// Assigning address of variable 'mdm_ptr' to pointer-to-pointer variable 'mdm_pptr'
	// 'mdm_pptr' now contains the address of 'mdm_ptr' which in turn contains the address of 'mdm_num'
	// Hence, 'mdm_pptr' is SAME as '&mdm_ptr'
	// 'mdm_ptr' is SAME as '&mdm_num'
	// Hence, mdm_pptr = &mdm_ptr = &(&mdm_num)
	// If mdm_ptr = &mdm_num and *mdm_ptr = *(&mdm_num) = value at address of 'mdm_num'
	// Then, mdm_pptr = &mdm_ptr and *mdm_pptr = *(&mdm_ptr) = mdm_ptr = value at address of 'mdm_ptr' i.e : 'mdm_ptr' i.e : address of mdm_num
	// Then, **mdm_pptr = **(&mdm_ptr) = *(*(&mdm_ptr) = *mdm_ptr = *(&mdm_num) = mdm_num = 10)
	// Hence, mdm_num = *(&mdm_num) = *(mdm_ptr) = *(*mdm_pptr) = **mdm_pptr

	mdm_pptr = &mdm_ptr;

	printf("\n\n");
	printf("******** After mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'					  = %d\n\n", mdm_num);
	printf("Address Of 'mdm_num' (mdm_ptr)				  = %p\n\n", mdm_ptr);
	printf("Address Of 'mdm_ptr' (mdm_pptr)                           = %p\n\n", mdm_pptr);
	printf("Value At Address Of 'mdm_ptr' (*mdm_pptr)                 = %p\n\n", *mdm_pptr);
	printf("Value At Address Of 'mdm_num' (*mdm_ptr) (*mdm_pptr)      = %d\n\n", **mdm_pptr);

	return(0);
}