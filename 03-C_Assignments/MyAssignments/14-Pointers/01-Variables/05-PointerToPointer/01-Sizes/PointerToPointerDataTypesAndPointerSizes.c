// header file
#include<stdio.h>

// defining struct
struct MDM_Employee
{
	char mdm_name;
	int mdm_age;
	float mdm_salary;
	char mdm_sex;
	char mdm_marital_status;
};

//entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// code
	printf("\n\n");
	printf("Sizes Of Data Types And Pointers To Those Respective Data Types Are : \n\n");
	printf("Size Of (int) : %d \t \t Size Of Pointer To int (int*) : %d\t \t Size Of Pointer To Pointer To int (int**) : %d\n\n", sizeof(int), sizeof(int*), sizeof(int**));
	printf("Size Of (float) : %d \t \t Size Of Pointer To float (float*) : %d\t \t Size Of Pointer To Pointer To float (float**) : %d\n\n", sizeof(float), sizeof(float*), sizeof(float**));
	printf("Size Of (double) : %d \t \t Size Of Pointer To double (double*) : %d\t Size Of Pointer To Pointer To double (double**) : %d\n\n", sizeof(double), sizeof(double*), sizeof(double**));
	printf("Size Of (char) : %d \t \t Size Of Pointer To char (char*) : %d\t \t Size Of Pointer To Pointer To char (char**) : %d\n\n\n", sizeof(char), sizeof(char*), sizeof(char**));
	printf("Size Of (struct MDM_Employee) : %d \tSize Of Pointer To struct MDM_Employee (struct MDM_Employee*) : %d\tSize Of Pointer To Pointer To struct MDM_Employee (struct MDM_Employee**) : %d\n\n", sizeof(struct MDM_Employee), sizeof(struct MDM_Employee*), sizeof(struct MDM_Employee**));


	return(0);
}

