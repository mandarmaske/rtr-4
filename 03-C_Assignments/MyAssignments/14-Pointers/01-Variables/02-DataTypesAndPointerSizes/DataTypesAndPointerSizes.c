// header file
#include<stdio.h>

// struct definition
struct MDM_Employee
{
	char mdm_name[100];
	int mdm_age;
	float mdm_salary;
	char mdm_sex;
	char mdm_marital_status;
};

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// code
	printf("\n\n");
	printf("Sizes Of Data Types And Pointers To Those Respective Data Types Are : \n\n");
	printf("Size of (int)                  : %d\t\t Size Of Pointer to 'int' (int*)        :        %d\n\n", sizeof(int), sizeof(int*));
	printf("Size of (float)                : %d\t\t Size Of Pointer to 'float' (float*)    :        %d\n\n", sizeof(float), sizeof(float*));
	printf("Size of (double)               : %d\t\t Size Of Pointer to 'double' (double*)  :        %d\n\n", sizeof(double), sizeof(double*));
	printf("Size Of (char)                 : %d\t\t Size Of Pointer to 'char' (char*)      :        %d\n\n\n\n", sizeof(char), sizeof(char*));
	printf("Size Of (struct MDM_Employee)  : %d\t\t Size Of Pointer to 'struct MDM_Employee' (struct MDM_Employee*) : %d\n\n", sizeof(struct MDM_Employee), sizeof(struct MDM_Employee*));

	return(0);
}