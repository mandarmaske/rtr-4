// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	char mdm_ch;
	char* mdm_ptr = NULL; // Declaration Method 2 : 'ptr' is a variable of type 'char*'

	// code
	mdm_ch = 'N';

	printf("\n\n");
	printf("******** Before mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'              = %c\n\n", mdm_ch);
	printf("Address Of 'mdm_num'            = %p\n\n", &mdm_ch);
	printf("Value At Address Of 'mdm_num'   = %c\n\n", *(&mdm_ch));

	// Assigning address of variable 'mdm_num' to pointer variable 'mdm_ptr'
	// 'mdm_ptr' now contains address of 'mdm_num'... hence, 'mdm_ptr' is same as '&mdm_num'
	mdm_ptr = &mdm_ch;

	printf("\n\n");
	printf("******** After mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'              = %c\n\n", mdm_ch);
	printf("Address Of 'mdm_num'            = %p\n\n", mdm_ptr);
	printf("Value At Address Of 'mdm_num'   = %c\n\n", *mdm_ptr);

	return(0);
}