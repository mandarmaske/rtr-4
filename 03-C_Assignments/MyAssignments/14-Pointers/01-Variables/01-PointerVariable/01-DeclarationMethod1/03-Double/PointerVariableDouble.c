// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	double mdm_num;
	double *mdm_ptr = NULL; // Declaration Method 1 : '*ptr' is a variable of type 'double'

	// code
	mdm_num = 2.34343434;

	printf("\n\n");
	printf("******** Before mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'              = %lf\n\n", mdm_num);
	printf("Address Of 'mdm_num'            = %p\n\n", &mdm_num);
	printf("Value At Address Of 'mdm_num'   = %lf\n\n", *(&mdm_num));

	// Assigning address of variable 'mdm_num' to pointer variable 'mdm_ptr'
	// 'mdm_ptr' now contains address of 'mdm_num'... hence, 'mdm_ptr' is same as '&mdm_num'
	mdm_ptr = &mdm_num;

	printf("\n\n");
	printf("******** After mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'              = %lf\n\n", mdm_num);
	printf("Address Of 'mdm_num'            = %p\n\n", mdm_ptr);
	printf("Value At Address Of 'mdm_num'   = %lf\n\n", *mdm_ptr);

	return(0);
}