// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_num;
	int *mdm_ptr = NULL; // Declaration Method 1 : '*ptr' is a variable of type 'int'

	// code
	mdm_num = 10;

	printf("\n\n");
	printf("******** Before mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'              = %d\n\n", mdm_num);
	printf("Address Of 'mdm_num'            = %p\n\n", &mdm_num);
	printf("Address Of 'mdm_num'            = %u\n\n", &mdm_num);
	printf("Value At Address Of 'mdm_num'   = %d\n\n", *(&mdm_num));

	// Assigning address of variable 'mdm_num' to pointer variable 'mdm_ptr'
	// 'mdm_ptr' now contains address of 'mdm_num'... hence, 'mdm_ptr' is same as '&mdm_num'
	mdm_ptr = &mdm_num;

	printf("\n\n");
	printf("******** After mdm_ptr = &mdm_num ********\n\n");
	printf("Value Of 'mdm_num'              = %d\n\n", mdm_num);
	printf("Address Of 'mdm_num'            = %p\n\n", mdm_ptr);
	printf("Value At Address Of 'mdm_num'   = %d\n\n", *mdm_ptr);

	return(0);
}