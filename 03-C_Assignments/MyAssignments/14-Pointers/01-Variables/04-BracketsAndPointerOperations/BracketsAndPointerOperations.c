// header file
#include<stdio.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	int mdm_num;
	int* mdm_ptr = NULL;
	int mdm_ans;

	// code
	mdm_num = 5;
	mdm_ptr = &mdm_num;

	printf("\n\n");
	printf("mdm_num        = %d\n", mdm_num);
	printf("&mdm_num       = %p\n", &mdm_num);
	printf("*(&mdm_num)    = %d\n", *(&mdm_num));
	printf("mdm_ptr        = %p\n", mdm_ptr);
	printf("*mdm_ptr       = %d\n", *mdm_ptr);

	printf("\n\n");

	// Add 10 to 'mdm_ptr' which is the address of 'mdm_num'...
	// Hence, 10 will be added to address of 'mdm_num' and resultant address will be displayed
	printf("Answer Of (mdm_ptr + 10)    = %p\n", mdm_ptr + 10);

	// Add 10 to 'mdm_ptr' which is address of 'mdm_num' and give value at the new address...
	// Hence, 10 will be added to the address of 'mdm_num' and the value at resultant address will be displayed...
	printf("Answer Of *(mdm_ptr + 10)   = %d\n", *(mdm_ptr + 10));

	// Add 10 to '*mdm_ptr' which is the value at address of 'mdm_num' (i.e : 'mdm_num' i.e : 5) and give new value, without any change in address...
	// Hence, 10 will be added to the '*mdm_ptr' (mdm_num = 5) and the resultant value will be given (*mdm_ptr + 10) = (mdm_num + 10) = 15
	printf("Answer Of (*mdm_ptr + 10)   = %d\n", (*mdm_ptr + 10));

	// **** Associativity Of * (Value At Address) And ++ And -- Operators Is From Right To Left ****
	// (Right To Left) Consider value *mdm_ptr... Pre-increment *mdm_ptr... That is, value at address 'mdm_ptr' i.e : *mdm_ptr is pre-incremented (++*mdm_ptr)
	++*mdm_ptr; // *mdm_ptr is preincremented... *mdm_ptr is 5... After Execution Of This Statement... *mdm_ptr = 6
	printf("Answer Of ++*mdm_ptr        = %d\n", *mdm_ptr); // Brackets Not Neccessary For Pre-Increment / Pre-Decrement

	// (Right To Left) Post-Increment address mdm_ptr... That is, address 'mdm_ptr' i.e : mdm_ptr is Post-Incremented (mdm_ptr++) and then the value at 
	// the new address is displayed (*ptr++)
	*mdm_ptr++; // Incorrect method of Post-Incrementing a value using pointer...
	printf("Answer Of *ptr++            = %d\n", *mdm_ptr); // Brackets Are Necessary For Post-Increment / Post-Decrement
	  
	// (Right To Left) Post-Increment Value *mdm_ptr... That is, value at address 'mdm_ptr' i.e *mdm_ptr is Post-Incremented (*ptr)++
	mdm_ptr = &mdm_num;
	(*mdm_ptr)++; // Correct Method Of Post-Incrementing a Value using Pointer... 'mdm_ptr' is 6... at next statement *mdm_ptr = 7 (Post-Increment)
	printf("Answer Of (*ptr)++          = %d\n\n", *mdm_ptr); // Brackets Are Necessary for Post-Increment / Post-Decrement

	return(0);
}

