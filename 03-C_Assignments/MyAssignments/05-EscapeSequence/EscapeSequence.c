// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// code
	printf("\n\n");
	printf("Going on next line using \\n Escape Sequnce\n\n");
	printf("Demonstrating \t Horizontal \t TAB \t Using \t \\t Escape Sequence\n\n");
	printf("\"This is a Double Quoted output\" done using \\\" \\\" escape sequence\n\n");
	printf("\'This is a Single Quoted output\' done using \\\' \\\' escape sequence\n\n");
	printf("BACKSPACE turned to BACKSPACE\b using \\b Escape Sequence\n\n");

	printf("\rDemonstrating Carriage Return using \\r Escape Sequence\n");
	printf("Demonstrating \rCarriage Return using \\r Escape Sequence\n");
	printf("Demostrating Carriage \rReturn using \\r Escape Sequence\n\n");

	printf("Demonstrating \x41 using \\xhh Escape Sequence\n\n"); 
	/*
		0x41 is the Hexadecimal Code for letter 'A'.
	    'xhh' is a placeholder for 'x' followed by 2 digits (hh), altogether forming a hexadecimal number.
	*/  
	printf("Demonstrating \102 using \\ooo Escape Sequence\n\n");
	/*
		102 is the octal code for letter 'B', 'ooo' is the place-holder for 3 digits forming an octal number
	*/
	printf("\n\n");

	return(0);
}
