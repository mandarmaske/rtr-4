// header files
#include<stdio.h>
#include<conio.h>
#include<string.h>

// entry-point function
int main(int argc, char* argv[], char* envp[])
{
	// variable declarations
	char mdm_ch, mdm_ch_i;
	unsigned int mdm_ascii_ch = 0;

	// code
	printf("\n\n");
	printf("Enter The First Character Of First Name : ");
	mdm_ch = getch();

	mdm_ch = toupper(mdm_ch);

	for (mdm_ch_i = 'A'; mdm_ch_i <= 'Z'; mdm_ch_i++)
	{
		if (mdm_ch == mdm_ch_i)
		{
			mdm_ascii_ch = (unsigned int)mdm_ch;
			goto mdm_result_output; // Program Flow jumps Directly to label 'mdm_result_output'
		}
	}

	printf("\n\n");
	printf("Goto Statements not executed, so printing \"Hello, World\" !!!\n\n"); // will be omitted if 'goto' statement is executed

mdm_result_output : // label itself does not alter flow of program, following code is executed regardless of whether goto statement is executed or not
	printf("\n\n");

	if (mdm_ascii_ch == 0)
	{
		printf("You must have a strange name! Could not find the character '%c' in the entire English Alphabet!\n\n", mdm_ch);
	}
	else
	{
		printf("Character '%c' found. It has ASCII value %u.\n\n", mdm_ch, mdm_ascii_ch);
	}

	printf("\n");

	return(0);
}