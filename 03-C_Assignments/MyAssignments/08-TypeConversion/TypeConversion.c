// header file
#include<stdio.h>

// entry-point function
int main(void)
{
	// variable declarations
	int mdm_i, mdm_j;
	char mdm_ch1, mdm_ch2;

	int mdm_a, mdm_result_int;
	float mdm_f, mdm_result_float;

	int mdm_iExplicit;
	float mdm_fExplicit;

	// code
	printf("\n\n");

	// INTERCONVERSION AND IMPLICIT TYPE-CASTING BETWEEN 'char' AND 'int' TYPES....
	mdm_i = 70;
	mdm_ch1 = mdm_i;
	printf("I = %d\n", mdm_i);
	printf("Character 1 (after CH1 = I) = %c\n\n", mdm_ch1);

	mdm_ch2 = 'Q';
	mdm_j = mdm_ch2;
	printf("mdm_ch2 = %c\n", mdm_ch2);
	printf("J (after J = CH2 (Character 2) ) = %d\n\n", mdm_j);

	// IMPLICIT CONVERSION OF 'int' to 'float'...
	mdm_a = 5;
	mdm_f = 7.8f;
	mdm_result_float = mdm_a + mdm_f;
	printf("Integer A = %d and Floating-Point Number F = %f Added Gives Floating-Point Sum = %1.2f\n\n", mdm_a, mdm_f, mdm_result_float);

	mdm_result_int = mdm_a + mdm_f;
	printf("Integer A = %d and Floating-Point Number F = %f Added Gives Integer Sum = %d\n\n", mdm_a, mdm_f, mdm_result_int);

	// EXPLICIT TYPE_CASTING USING CAST OPERATOR...
	mdm_fExplicit = 30.122143f;
	mdm_iExplicit = (int)mdm_fExplicit;
	printf("Floating Point Number which will be Type Casted Explicitly = %f\n", mdm_fExplicit);
	printf("Resultant Integer After Explicit Type Casting of %f = %d\n\n", mdm_fExplicit, mdm_iExplicit);

	return(0);
}
