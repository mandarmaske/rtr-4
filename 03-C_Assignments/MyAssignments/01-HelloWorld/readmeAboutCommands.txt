cl.exe can compile and also link our programs

Compile Command of cl.exe - cl.exe /c /EHsc ProgramName.c/cpp
link Command of cl.exe - cl.exe /link ProgramName.obj
Normal Link Command - link.exe ProgramName.obj

If we did not use /c or /link then cl.exe(Compiler Collection) will do both compile and link -

C:\MyProjects\RTR_4_2021-116\03-CAssignments\HelloWorld>cl.exe /EHsc HelloWorld.c
Microsoft (R) C/C++ Optimizing Compiler Version 19.29.30136 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

HelloWorld.c
Microsoft (R) Incremental Linker Version 14.29.30136.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:HelloWorld.exe
HelloWorld.obj