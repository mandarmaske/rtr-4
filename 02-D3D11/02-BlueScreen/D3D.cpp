// header file
#include <Windows.h>
#include <stdio.h> // For File I/O (fopen(), fclose(), fprintf())
#include <stdlib.h> // For exit()
#include <math.h>
#include "D3D.h" // our header file

// D3D11 related header files
#include <dxgi.h> // DirectX Graphics Infrastructure
#include <d3d11.h>

// D3D related libraries
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")

// D3D11 related global variables
IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;
float clearColor[4];

// macro defination
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
BOOL gbActiveWindow = FALSE; // by default aapli window active nahi ahe
FILE* gpFile = NULL;
BOOL gbFullScreen = FALSE;
HWND ghwnd = NULL;

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations -> not important in sequence as prototype but it is defined in which one gets called first
	HRESULT initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	HRESULT hr = S_OK;

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) // if(file open secured() != 0)
	// aadhi fopen hota nusta
	{
		MessageBox(NULL, TEXT("Creation Of Log File Failed. Exitting..."), TEXT("File I/O Error"), MB_OK); // 1st parameter is NULL because we dont have our hwnd yet, so OS la ticha handle magitla message dakhvnyasathi ani aapan NULL/HWND_DESKTOP dila
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n"); // printf prints on console. fprintf prints in file
		fclose(gpFile);
	}

	// initilizing WNDCLASSEX wndclass structure members
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Registering wndclass in OS's Class Registry
	RegisterClassEx(&wndclass);

	// creating our window in memory
	hwnd = CreateWindow(szAppName,
						TEXT("Mandar Maske RTR2021-116"),
						WS_OVERLAPPEDWINDOW,
						((GetSystemMetrics(SM_CXSCREEN) - WIN_WIDTH) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - WIN_HEIGHT) / 2),
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	if (hwnd == NULL)
	{
		fprintf(gpFile, "hwnd Failed !!!\n\n");
	}
	else
	{
		fprintf(gpFile, "hwnd Created Successfully\n\n");
	}

	ghwnd = hwnd;

	// initialize
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize function failed\n\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize function loaded successfully\n\n");
		fclose(gpFile); 
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// Showing Window On Screen
	ShowWindow(hwnd, iCmdShow);

	// foregrounding(Z Order - Top) and focusing the window
	SetForegroundWindow(hwnd);

	SetFocus(hwnd); // sends message to our Window(wndproc) WM_SETFOCUS

	// Game Loop -> glutMainLoop() in glut
	while (bDone == FALSE) //  while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) // manually loop false kela
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else // Kontahi message nasel tevha ha else block run hoil
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}

	uninitialize(); // indra and takshaq saap

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	HRESULT resize(int, int);
	void uninitialize(void);

	// variable declarations
	int retVal;
	HRESULT hr = S_OK;

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);// bluscreen chya pudhe saglya programs madhye -> return(0); 
		break; // as this is retained mode graphics there is WM_PAINT to Paint, OpenGL is not came in the picture now

	case WM_KEYDOWN:

		switch (wParam)
		{
		case 27:
			retVal = MessageBox(hwnd, TEXT("Are You Sure, You Want To Exit ?"), TEXT("Exit Message"), MB_YESNO);
			if (retVal == IDNO)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), No Button Is Pressed\n\n");
				break;
			}
				
			else if(retVal == IDYES)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), Yes Button Is Pressed. Program Is Now Exitting !!!\n\n");
				DestroyWindow(hwnd);
				break;
			}

		default:
			break;
		}
	break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		default:
			break;
		}
	break;

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam)); // resize(width, height)
			if (FAILED(hr))
			{
				fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
				fprintf(gpFile, "WndProc : resize failed\n\n");
				fclose(gpFile);
				return(hr);
			}
		}
		break;

	// occurs only when we clicks close button not when we close using keyboard accelerater
	case WM_CLOSE: // close button - discipline mhanun lihane, WM_CLOSE cha call WM_DESTROY chya aadhi yeto
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle; // static to sustain values accross this function
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT); // like wndclass.cbSize =  sizeof(WNDCLASSEX)

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) // if (1 & WS_OVERLAPPEDWINDOW) -> if (1 & 1) then if ya madhe
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) // if(1 && 1) then in if
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW); // WS_OVERLAPPEDWINDOW che 5 style(WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_THICKFRAME,  WS_SYSMENU, WS_CAPTION) nako except WS_OVERLAPPED 
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW); // kadhayla ~WS_OVERLAPPED kela, Add karnyasathi Bitwise Or Waparala
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED); // x, y, width, height ignore -> Zero Why? - wp ne already SetWindowPlacement pahije tithe aaplya window la place kelay so 0 

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

HRESULT initialize(void)
{
	// function declarations
	HRESULT PrintD3DInfo(void);
	HRESULT resize(int, int);
	
	// variable declarations
	HRESULT hr = S_OK;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_Required = D3D_FEATURE_LEVEL_11_0; // 11 required aahe 
	D3D_FEATURE_LEVEL d3dFeatureLevel_Aquired = D3D_FEATURE_LEVEL_10_0; // 11 nasel tar 10 de at least, 10 ha 0 nasnar kadhic
	UINT numDrivers = 0; // 3
	UINT createDeviceFlags = 0;
	UINT numFeatureLevels = 1;

	// code
	hr = PrintD3DInfo();

	// initialize swap chain descriptor
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDescriptor;

	ZeroMemory((void*)&dxgiSwapChainDescriptor, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDescriptor.BufferCount = 1; // 2 nahi karan DirectX 1 buffer swtah deto aapan 1 create karto ase 2 buffer zhale
	dxgiSwapChainDescriptor.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDescriptor.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDescriptor.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // 32 bit colors
	dxgiSwapChainDescriptor.BufferDesc.RefreshRate.Numerator = 60; 
	dxgiSwapChainDescriptor.BufferDesc.RefreshRate.Denominator = 1; 
	dxgiSwapChainDescriptor.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDescriptor.OutputWindow = ghwnd;
	dxgiSwapChainDescriptor.SampleDesc.Count = 1;
	dxgiSwapChainDescriptor.SampleDesc.Quality = 0; // 0 means tu deshill ti default quality
	dxgiSwapChainDescriptor.Windowed = TRUE; // Windowed full screen yeil

	// Call D3D11CreateDeviceAndSwapChain() for required driver
	numDrivers = sizeof(d3dDriverTypes) / sizeof (d3dDriverTypes[0]);

	for (UINT i = 0; i < numDrivers; i++) // 3 iteration honar as per graphic device
	{
		d3dDriverType = d3dDriverTypes[i];

		hr = D3D11CreateDeviceAndSwapChain(NULL, // primary jo aahe toh de graphics, ithe adapter pass karu shakto
										   d3dDriverType,
										   NULL,
										   createDeviceFlags, // default flag ghe
										   &d3dFeatureLevel_Required,
									       numFeatureLevels, 
										   D3D11_SDK_VERSION,
										   &dxgiSwapChainDescriptor,
										   &gpIDXGISwapChain,
										   &gpID3D11Device,
										   &d3dFeatureLevel_Aquired,
										   &gpID3D11DeviceContext);

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : D3D11CreateDeviceAndSwapChain() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 

		// Print obtained driver type
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "initialize : D3D11 obtained harware driver Successfully\n\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "initialize : D3D11 obtained driver type warp Successfully\n\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "initialize : D3D11 obtained driver type reference Successfully\n\n");
		}
		else
		{
			fprintf(gpFile, "initialize : unknown d3dDriverType\n\n");
		}

		// Print obtained D3D11 feature level
		if (d3dFeatureLevel_Aquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 11.0 feature level\n\n");
		}
		else if (d3dFeatureLevel_Aquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 10.1 feature level\n\n");
		}
		else if (d3dFeatureLevel_Aquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 10.0 feature level\n\n");
		}
		else
		{
			fprintf(gpFile, "initialize : D3D11 obtained unknown feature level\n\n");
		}

		fclose(gpFile);
	}

	// initialize clear color array
	clearColor[0] = 0.0f;
	clearColor[1] = 0.0f;
	clearColor[2] = 1.0f; // Blue
	clearColor[3] = 1.0f;

	// warm up resize
	hr = resize(WIN_WIDTH, WIN_HEIGHT);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : resize() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : resize() done successfully\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	return(hr);
}

HRESULT PrintD3DInfo(void)
{
	// variable declarations
	HRESULT hr = S_OK; // (SUCESS_OK)
	IDXGIFactory* pIDXGIFactory = NULL;
	IDXGIAdapter* pIDXGIAdapter = NULL;
	DXGI_ADAPTER_DESC dxgiAdapterDescriptor; // DESC - descriptor
	char str[255];

	// code

	// CreateDXGIFactory 
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	// 1. GUID no ghe IDXGIFactory che
	// 2. rekama interface variable madhye te guid bharun de

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "PrintD3DInfo : CreateDXGIFactory failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "PrintD3DInfo : CreateDXGIFactory done successfully\n\n");
		fclose(gpFile); 
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// Get IDXGIAdapter -  graphic cards chya tya company shi concern data structure or adapter de
	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) != DXGI_ERROR_NOT_FOUND)
	{
		// Get dscription of found adapter
		ZeroMemory((void*)&dxgiAdapterDescriptor, sizeof(DXGI_ADAPTER_DESC));
		pIDXGIAdapter->GetDesc(&dxgiAdapterDescriptor);
		WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDescriptor.Description, 255, str, 255, NULL, NULL); // ACP - ANSI CODE PAGE
		// 2nd is 0 means no need to convert in laguages
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "Graphics Card Name = %s \n", str);
		fprintf(gpFile, "Graphics Card Memory (VRAM) = %d GB \n\n", (int)ceil(dxgiAdapterDescriptor.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0));
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "PrintD3DInfo : pIDXGIFactory->EnumAdapters() Failed \n\n", str);
		fclose(gpFile);
	}
	
	// release
	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	return(hr);
}

HRESULT resize(int width, int height)
{
	// variable declarations
	HRESULT hr = S_OK;

	// code

	// kitihi height kami keli ki height 0 nasayla havi kamit kami 1 havi
	// To Avoid divided by 0(illegal instruction / code) in future code - Aspect Ratio Code
	if (height >= 0) 
		height = 1;

	// Step 1 : Release existing RTV - Render Target View
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// Step 2 : Tell Swap Chain to resize buffer according to new width and height
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// Step 3 : Get new resized buffer from swap chain into a dummy texture(color)
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer = NULL;

	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);
	// or gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pID3D11Texture2D_BackBuffer);

	// Step 4 : Now create new render target view using above buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	// 2nd parameter structure deto NULL nasel tar D3D11_RENDER_TARGET_VIEW_DESC

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "resize failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	// else nahi karan resize call hoto repeatedly

	// relase dummy texture interface
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// Step 5 : Set this render target view in pipeline
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL); // depth, stencil

	// Step 6 : Initialize Viewport structure
	D3D11_VIEWPORT d3d11Viewport; // zero memory karnar nahi karan sagle member

	d3d11Viewport.TopLeftX = 0.0f; // OpenGl cha ulta tikde left bottom hota 0.0f
	d3d11Viewport.TopLeftY = 0.0f;
	d3d11Viewport.Width = (float)width;
	d3d11Viewport.Height = (float)height;
	d3d11Viewport.MinDepth = 0.0f;
	d3d11Viewport.MaxDepth = 1.0f;
	
	// Step 7 : Set This Viewport in this pipeline ~parallel to viewport
	gpID3D11DeviceContext->RSSetViewports(1, &d3d11Viewport);

	return(hr);
}

void display(void)
{
	// code
	
	// Clear Render Target View by clearColor
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, clearColor);

	// Here There Should be drawing code

	// Swap Buffers by presenting them
	gpIDXGISwapChain->Present(0, 0); 
	// 1 - No need of sync monitor refresh rate (aapan numerator 60 kela aahe var) 
	// 2 - Giving 0 means telling D3D to allow swaping of all buffers
}

void update(void)
{
	// code
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	// FullScreen astana escape press karun close karaych try kela tar window orginal state 
	// madhye yeun tyacha sukhad mrutyu hoil aapla window application 
	if (gbFullScreen)
	{
		ToggleFullScreen(); // important so taken in curly brace
	}

	if (ghwnd) // ghwnd ajun asel tar DestroyWindow() la call karun ghwnd destroy kar
	{
		DestroyWindow(ghwnd); 	
		ghwnd = NULL;
	}
}

