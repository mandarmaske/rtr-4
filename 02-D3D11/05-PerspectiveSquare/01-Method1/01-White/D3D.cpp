// header file
#include <Windows.h>
#include <stdio.h> // For File I/O (fopen(), fclose(), fprintf())
#include <stdlib.h> // For exit()
#include <math.h>
#include "D3D.h" // our header file

// D3D11 related header files
#include <dxgi.h> // DirectX Graphics Infrastructure
#include <d3d11.h>
#include <d3dcompiler.h>
#pragma warning(disable:4838) // warning supress karto hi line
#include "xnamath/xnamath.h"

// D3D related libraries
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

// D3D11 related global variables
IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;
float clearColor[4];

ID3D11VertexShader* gpID3D11VertexShader = NULL;
ID3D11PixelShader* gpID3D11PixelShader = NULL;
ID3D11InputLayout* gpID3D11InputLayout = NULL; // in, out and layout aatribute sathi DirectX madhye InputLayout Ahe
ID3D11Buffer* gpID3D11Buffer_PositionBuffer = NULL; // like glGenBuffer()
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer = NULL; // like uniforms

struct CBUFFER // uniform buffer object
{
	XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX perspectiveProjectionMatrix;

// macro defination
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
BOOL gbActiveWindow = FALSE; // by default aapli window active nahi ahe
FILE* gpFile = NULL;
BOOL gbFullScreen = FALSE;
HWND ghwnd = NULL;

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations -> not important in sequence as prototype but it is defined in which one gets called first
	HRESULT initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	HRESULT hr = S_OK;

	// code

	if (fopen_s(&gpFile, "Log.txt", "w") != 0) // if(file open secured() != 0)
	// aadhi fopen hota nusta
	{
		MessageBox(NULL, TEXT("Creation Of Log File Failed. Exitting..."), TEXT("File I/O Error"), MB_OK); // 1st parameter is NULL because we dont have our hwnd yet, so OS la ticha handle magitla message dakhvnyasathi ani aapan NULL/HWND_DESKTOP dila
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created.\n\n"); // printf prints on console. fprintf prints in file
		fclose(gpFile);
	}

	// initilizing WNDCLASSEX wndclass structure members
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	// Registering wndclass in OS's Class Registry
	RegisterClassEx(&wndclass);

	// creating our window in memory
	hwnd = CreateWindow(szAppName,
						TEXT("Mandar Maske RTR2021-116"),
						WS_OVERLAPPEDWINDOW,
						((GetSystemMetrics(SM_CXSCREEN) - WIN_WIDTH) / 2),
						((GetSystemMetrics(SM_CYSCREEN) - WIN_HEIGHT) / 2),
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	if (hwnd == NULL)
	{
		fprintf(gpFile, "hwnd Failed !!!\n\n");
	}
	else
	{
		fprintf(gpFile, "hwnd Created Successfully\n\n");
	}

	ghwnd = hwnd;

	// initialize
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize function failed\n\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize function loaded successfully\n\n");
		fclose(gpFile); 
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// Showing Window On Screen
	ShowWindow(hwnd, iCmdShow);

	// foregrounding(Z Order - Top) and focusing the window
	SetForegroundWindow(hwnd);

	SetFocus(hwnd); // sends message to our Window(wndproc) WM_SETFOCUS

	// Game Loop -> glutMainLoop() in glut
	while (bDone == FALSE) //  while (TRUE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) // manually loop false kela
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else // Kontahi message nasel tevha ha else block run hoil
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				update();
			}
		}
	}

	uninitialize(); // indra and takshaq saap

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	HRESULT resize(int, int);
	void uninitialize(void);

	// variable declarations
	int retVal;
	HRESULT hr = S_OK;

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);// bluscreen chya pudhe saglya programs madhye -> return(0); 
		break; // as this is retained mode graphics there is WM_PAINT to Paint, OpenGL is not came in the picture now

	case WM_KEYDOWN:

		switch (wParam)
		{
		case 27:
			retVal = MessageBox(hwnd, TEXT("Are You Sure, You Want To Exit ?"), TEXT("Exit Message"), MB_YESNO);
			if (retVal == IDNO)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), No Button Is Pressed\n\n");
				break;
			}
				
			else if(retVal == IDYES)
			{
				fprintf(gpFile, "In WM_KEYDOWN (Exit On Escape), Yes Button Is Pressed. Program Is Now Exitting !!!\n\n");
				DestroyWindow(hwnd);
				break;
			}

		default:
			break;
		}
	break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		default:
			break;
		}
	break;

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam)); // resize(width, height)
			if (FAILED(hr))
			{
				fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
				fprintf(gpFile, "WndProc : resize failed\n\n");
				fclose(gpFile);
				return(hr);
			}
		}
		break;

	// occurs only when we clicks close button not when we close using keyboard accelerater
	case WM_CLOSE: // close button - discipline mhanun lihane, WM_CLOSE cha call WM_DESTROY chya aadhi yeto
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle; // static to sustain values accross this function
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT); // like wndclass.cbSize =  sizeof(WNDCLASSEX)

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) // if (1 & WS_OVERLAPPEDWINDOW) -> if (1 & 1) then if ya madhe
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) // if(1 && 1) then in if
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW); // WS_OVERLAPPEDWINDOW che 5 style(WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_THICKFRAME,  WS_SYSMENU, WS_CAPTION) nako except WS_OVERLAPPED 
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW); // kadhayla ~WS_OVERLAPPED kela, Add karnyasathi Bitwise Or Waparala
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED); // x, y, width, height ignore -> Zero Why? - wp ne already SetWindowPlacement pahije tithe aaplya window la place kelay so 0 

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

HRESULT initialize(void)
{
	// function declarations
	HRESULT PrintD3DInfo(void);
	HRESULT resize(int, int);
	
	// variable declarations
	HRESULT hr = S_OK;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_Required = D3D_FEATURE_LEVEL_11_0; // 11 required aahe 
	D3D_FEATURE_LEVEL d3dFeatureLevel_Aquired = D3D_FEATURE_LEVEL_10_0; // 11 nasel tar 10 de at least, 10 ha 0 nasnar kadhic
	UINT numDrivers = 0; // 3
	UINT createDeviceFlags = 0;
	UINT numFeatureLevels = 1;

	// code
	hr = PrintD3DInfo();

	// initialize swap chain descriptor
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDescriptor;

	ZeroMemory((void*)&dxgiSwapChainDescriptor, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDescriptor.BufferCount = 1; // 2 nahi karan DirectX 1 buffer swtah deto aapan 1 create karto ase 2 buffer zhale
	dxgiSwapChainDescriptor.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDescriptor.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDescriptor.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // 32 bit colors
	dxgiSwapChainDescriptor.BufferDesc.RefreshRate.Numerator = 60; 
	dxgiSwapChainDescriptor.BufferDesc.RefreshRate.Denominator = 1; 
	dxgiSwapChainDescriptor.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDescriptor.OutputWindow = ghwnd;
	dxgiSwapChainDescriptor.SampleDesc.Count = 1;
	dxgiSwapChainDescriptor.SampleDesc.Quality = 0; // 0 means tu deshill ti default quality
	dxgiSwapChainDescriptor.Windowed = TRUE; // Windowed full screen yeil

	// Call D3D11CreateDeviceAndSwapChain() for required driver
	numDrivers = sizeof(d3dDriverTypes) / sizeof (d3dDriverTypes[0]);

	for (UINT i = 0; i < numDrivers; i++) // 3 iteration honar as per graphic device
	{
		d3dDriverType = d3dDriverTypes[i];

		hr = D3D11CreateDeviceAndSwapChain(NULL, // primary jo aahe toh de graphics, ithe adapter pass karu shakto
										   d3dDriverType,
										   NULL,
										   createDeviceFlags, // default flag ghe
										   &d3dFeatureLevel_Required,
									       numFeatureLevels, 
										   D3D11_SDK_VERSION,
										   &dxgiSwapChainDescriptor,
										   &gpIDXGISwapChain,
										   &gpID3D11Device,
										   &d3dFeatureLevel_Aquired,
										   &gpID3D11DeviceContext);

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : D3D11CreateDeviceAndSwapChain() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 

		// Print obtained driver type
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "initialize : D3D11 obtained harware driver Successfully\n\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "initialize : D3D11 obtained driver type warp Successfully\n\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "initialize : D3D11 obtained driver type reference Successfully\n\n");
		}
		else
		{
			fprintf(gpFile, "initialize : unknown d3dDriverType\n\n");
		}

		// Print obtained D3D11 feature level
		if (d3dFeatureLevel_Aquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 11.0 feature level\n\n");
		}
		else if (d3dFeatureLevel_Aquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 10.1 feature level\n\n");
		}
		else if (d3dFeatureLevel_Aquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 10.0 feature level\n\n");
		}
		else
		{
			fprintf(gpFile, "initialize : D3D11 obtained unknown feature level\n\n");
		}

		fclose(gpFile);
	}

	// ************ Vertex Shader ************
	const char* vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldViewProjectionMatrix;" \
		"}" \
		"float4 main(float4 pos: POSITION): SV_POSITION" \
		"{" \
		"float4 position;" \
		"position = mul(worldViewProjectionMatrix, pos);" \
		"return(position);" \
		"}";

	ID3DBlob* pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob* pID3DBlob_Error = NULL;

	// ************ compile vertex shader ************
	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS", // VertexShader detoy he sangto ha parameter
		NULL, // linkage
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main", // ithe naav kahihi deu shakto
		"vs_5_0", // vertex shader 5.0
		0, // compile constant dyache aahe ka - OpenCl madhye fast math use kelay as compile constant
		0, // effect constant dyache aahe ka
		&pID3DBlob_VertexShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		if (pID3DBlob_Error)
		{
			fprintf(gpFile, "initialize : D3DCompile() failed for vertex shader : %s\n\n", (char*)pID3DBlob_Error->GetBufferPointer());
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else
		{
			fprintf(gpFile, "initialize : D3DCompile() failed for vertex shader : Unknown\n\n");
		}
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : D3DCompile() done successfully for vertex shader\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// ************ Create Vertex Shader ************
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader
	);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateVertexShader() failed\n\n"); // kontya class madhla sangyasathi :: use hoto
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateVertexShader() done successfully\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// Set This Vertex Shader in Vertex Shader Stage Of Pipeline
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);
	// 2nd para - jar linkage wala use kela asta tar class instance dyava lagla asta pan nahi use kela so 0
	// 3rd is length of array of 2nd para

	// ************ Pixel Shader ************
	const char* pixelShaderSourceCode =
		"float4 main(void): SV_TARGET" \
		"{" \
		"float4 color;" \
		"color = float4(1.0, 1.0, 1.0, 1.0);" \
		"return(color);" \
		"}";

	ID3DBlob* pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	// ************ compile pixel shader ************
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS", // PixelShader detoy he sangto ha parameter
		NULL, // linkage
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main", // ithe naav kahihi deu shakto
		"ps_5_0", // pixel shader 5.0
		0, // compile constant dyache aahe ka - OpenCl madhye fast math use kelay as compile constant
		0, // effect constant dyache aahe ka
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		if (pID3DBlob_Error)
		{
			fprintf(gpFile, "initialize : D3DCompile() failed for pixel shader : %s\n\n", (char*)pID3DBlob_Error->GetBufferPointer());
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else
		{
			fprintf(gpFile, "initialize : D3DCompile() failed for pixel shader : Unknown\n\n");
		}
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : D3DCompile() done successfully for pixel shader\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// ************ Create Pixel Shader ************
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader
	);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreatePixelShader() failed\n\n"); // kontya class madhla sangyasathi :: use hoto
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreatePixelShader() done successfully\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// Set This Pixel Shader in Pixel Shader Stage Of Pipeline
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);

	// ************ Create Input Layout ************
	D3D11_INPUT_ELEMENT_DESC d3d11InputElementDescriptor;
	ZeroMemory((void*)&d3d11InputElementDescriptor, sizeof(D3D11_INPUT_ELEMENT_DESC));

	d3d11InputElementDescriptor.SemanticName = "POSITION";
	d3d11InputElementDescriptor.SemanticIndex = 0;
	d3d11InputElementDescriptor.Format = DXGI_FORMAT_R32G32B32_FLOAT; // this is nothing but 3 float not rgb
	d3d11InputElementDescriptor.InputSlot = 0;
	d3d11InputElementDescriptor.AlignedByteOffset = 0;
	d3d11InputElementDescriptor.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	d3d11InputElementDescriptor.InstanceDataStepRate = 0; // stride in PP

	// ************ Input Layout ************

	// initilaize input layout structure
	// create input layout
	hr = gpID3D11Device->CreateInputLayout(&d3d11InputElementDescriptor, // array 
		1, // single element aahe array madhye
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateInputLayout failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateInputLayout done successfully\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// set this input layout in input assembly state of pipeline
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	// Geometry
	// DirectX is Clockwise, left hand rule - z aat positive and z baaher negative
	const float squareVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
	};

	// create vertex buffer for above position vertices
	// 1) initialize buffer descriptor structure (glGenBuffer anf glBindBuffer)
	D3D11_BUFFER_DESC d3d11BufferDescriptor;
	ZeroMemory((void*)&d3d11BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // like static draw
	d3d11BufferDescriptor.ByteWidth = sizeof(float) * ARRAYSIZE(squareVertices);
	d3d11BufferDescriptor.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	// 2) initialize sub resource data structure to put data into the buffer (glBufferData)
	D3D11_SUBRESOURCE_DATA d3d11SubResourceData;
	ZeroMemory((void*)&d3d11SubResourceData, sizeof(D3D11_BUFFER_DESC));

	d3d11SubResourceData.pSysMem = squareVertices;

	// 3) Create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11BufferDescriptor, &d3d11SubResourceData, &gpID3D11Buffer_PositionBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateBuffer() failed for position buffer\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateBuffer() done successfully for position buffer\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// ************** Constant Buffer **************

	// 1) initialize buffer descriptor structure (glGenBuffer anf glBindBuffer)
	ZeroMemory((void*)&d3d11BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // like static draw
	d3d11BufferDescriptor.ByteWidth = sizeof(CBUFFER);
	d3d11BufferDescriptor.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	// 2) Create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11BufferDescriptor, NULL, &gpID3D11Buffer_ConstantBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateBuffer() failed for constant buffer\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : gpID3D11Device::CreateBuffer() done successfully for constant buffer\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// 3) Set Constant buffer into Constant Buffer Stage Pipeline
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// initialize clear color array
	clearColor[0] = 0.0f;
	clearColor[1] = 0.0f;
	clearColor[2] = 1.0f; // Blue
	clearColor[3] = 1.0f;

	perspectiveProjectionMatrix = XMMatrixIdentity();

	// warm up resize
	hr = resize(WIN_WIDTH, WIN_HEIGHT);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : resize() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "initialize : resize() done successfully\n\n");
		fclose(gpFile);
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	return(hr);
}

HRESULT PrintD3DInfo(void)
{
	// variable declarations
	HRESULT hr = S_OK; // (SUCESS_OK)
	IDXGIFactory* pIDXGIFactory = NULL;
	IDXGIAdapter* pIDXGIAdapter = NULL;
	DXGI_ADAPTER_DESC dxgiAdapterDescriptor; // DESC - descriptor
	char str[255];

	// code

	// CreateDXGIFactory 
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	// 1. GUID no ghe IDXGIFactory che
	// 2. rekama interface variable madhye te guid bharun de

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "PrintD3DInfo : CreateDXGIFactory failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "PrintD3DInfo : CreateDXGIFactory done successfully\n\n");
		fclose(gpFile); 
		// directx madhye com chi error aste, program crash hoto file close naslyamule error disnaar nahi,
		// uninitialize chya fclose la janaar ch nahi jar program crash zhala tar madhun tar fopen_s, fprintf, fclose karne error check saathi
	}

	// Get IDXGIAdapter -  graphic cards chya tya company shi concern data structure or adapter de
	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) != DXGI_ERROR_NOT_FOUND)
	{
		// Get dscription of found adapter
		ZeroMemory((void*)&dxgiAdapterDescriptor, sizeof(DXGI_ADAPTER_DESC));
		pIDXGIAdapter->GetDesc(&dxgiAdapterDescriptor);
		WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDescriptor.Description, 255, str, 255, NULL, NULL); // ACP - ANSI CODE PAGE
		// 2nd is 0 means no need to convert in laguages
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "Graphics Card Name = %s \n", str);
		fprintf(gpFile, "Graphics Card Memory (VRAM) = %d GB \n\n", (int)ceil(dxgiAdapterDescriptor.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0));
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // append sathi open kar 
		fprintf(gpFile, "PrintD3DInfo : pIDXGIFactory->EnumAdapters() Failed \n\n", str);
		fclose(gpFile);
	}
	
	// release
	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	return(hr);
}

HRESULT resize(int width, int height)
{
	// variable declarations
	HRESULT hr = S_OK;

	// code
	if (height == 0)
		height = 1; // to avoid divided by 0 illegal instruction for feature code

	// step1: release exting RTV(Render Target View)
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// step2: tellswap chain to resize buffers according to new width and height
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0); // (kiti buffer resize karayche,width,height, format ky aahe, no flag(0))

	// step3: get new resized buffer from swap chain into a dummy texture
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer = NULL;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer); //0th index mhanun(0), interface chi guid,konamade pahije

	// step4 : now create new RTV using above buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView); //konapasun tayar karaycha aahe,RTV cha descriptor aahe ka,konamade pahije

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "resize() failed \n");
		fclose(gpFile);
		return(hr);
	}

	// release dummy texture interface
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// step5: set this new RTV in pipeline
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL); // kiti,konta,depth nahi tya mule NULL

	// step6: initialize viewpoart structure 
	D3D11_VIEWPORT d3d11Viewport;
	d3d11Viewport.TopLeftX = 0.0f;
	d3d11Viewport.TopLeftY = 0.0f;
	d3d11Viewport.Width = (float)width;
	d3d11Viewport.Height = (float)height;
	d3d11Viewport.MinDepth = 0.0f;
	d3d11Viewport.MaxDepth = 1.0f;

	// step7: tell device context to set this viewport in pipeline(similar like GLViewPoart)
	gpID3D11DeviceContext->RSSetViewports(1, &d3d11Viewport); //kiti set karayche

	// set projection matrix
	perspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f); // FOV - field of view, LH - left hand
	// ithe angle radians madhya dyave lagtaat

	return(hr);
}

void display(void)
{
	// code

	// Clear Render Target View by clearColor
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, clearColor);

	// 1) Set Position buffer in input stage of assembly line
	// like glVertexAtrribPointer shevatch 2 para
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_PositionBuffer, &stride, &offset);

	// 2) Set primitive topology in input pipeline stage
	// glDrawArrays cha 1st para
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// 3) Transformations
	// a) initialize matrices
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix = XMMatrixIdentity();

	translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 4.0f); // z +ve aat madhye left hand rule
	worldMatrix = translationMatrix;

	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * perspectiveProjectionMatrix;

	// b) put them into constant buffer (glUniformMatrix4fv)
	CBUFFER cBuffer;
	ZeroMemory((void*)&cBuffer, sizeof(CBUFFER));

	cBuffer.WorldViewProjectionMatrix = wvpMatrix;
	
	// c) push them into the shader 
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &cBuffer, 0, 0);
	gpID3D11DeviceContext->Draw(4, 0);
	
	// Swap Buffers by presenting them
	gpIDXGISwapChain->Present(0, 0); 
	// 1 - No need of sync monitor refresh rate (aapan numerator 60 kela aahe var) 
	// 2 - Giving 0 means telling D3D to allow swaping of all buffers
}

void update(void)
{
	// code
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11Buffer_PositionBuffer)
	{
		gpID3D11Buffer_PositionBuffer->Release();
		gpID3D11Buffer_PositionBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	// FullScreen astana escape press karun close karaych try kela tar window orginal state 
	// madhye yeun tyacha sukhad mrutyu hoil aapla window application 
	if (gbFullScreen)
	{
		ToggleFullScreen(); // important so taken in curly brace
	}

	if (ghwnd) // ghwnd ajun asel tar DestroyWindow() la call karun ghwnd destroy kar
	{
		DestroyWindow(ghwnd); 	
		ghwnd = NULL;
	}
}

