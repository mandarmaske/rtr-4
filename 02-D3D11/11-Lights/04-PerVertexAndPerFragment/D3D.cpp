// header file
#include<windows.h>
#include<stdio.h> // for file io functions
#include<stdlib.h> // for exit()
#include<math.h>
#include<d3dcompiler.h>
#include"D3D.h"//icon
#pragma warning(disable:4838)
#include"XNAMath/xnamath.h"
#include "Sphere.h"

// D3D11 related header files
#include<dxgi.h> // dxgi : directx graphics infrastructure
#include<D3D11.h>

// D3D related libraries
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"D3D11.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global veriable declarations
BOOL gbActiveWindow = FALSE;
HWND ghwnd;
BOOL gbFullScreen = FALSE;
FILE* gpFile = NULL;

HRESULT hr = S_OK;

// D3D11 related global veriable declarations
IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;
float clearColor[4];

ID3D11VertexShader* gpID3D11VertexShader;
ID3D11PixelShader* gpID3D11PixelShader;
ID3D11InputLayout* gpID3D11InputLayout; //in-out attribute sathi he aahe
ID3D11Buffer* gpID3D11Buffer_Sphere_PositionBuffer = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_NormalBuffer = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_IndexBuffer = NULL;
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer = NULL; // ha uniform sthi aasto

ID3D11VertexShader* gpID3D11VertexShader_PP;
ID3D11PixelShader* gpID3D11PixelShader_PP;
ID3D11InputLayout* gpID3D11InputLayout_PP; //in-out attribute sathi he aahe
ID3D11Buffer* gpID3D11Buffer_Sphere_PositionBuffer_PP = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_NormalBuffer_PP = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_IndexBuffer_PP = NULL;
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer_PP = NULL; // ha uniform sthi aasto

ID3D11RasterizerState* gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView* gpID3D11DepthStencilView = NULL;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned short gNumElements;
unsigned int gNumVertices;


struct CBUFFER // ubo:uniform buffer object
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;

	XMVECTOR la;
	XMVECTOR ld;
	XMVECTOR ls;
	XMVECTOR lightPosition;

	XMVECTOR ka;
	XMVECTOR kd;
	XMVECTOR ks;
	float materialShininess;
	unsigned int lightingEnabled;
};

float lightAmbient[] = { 0.1f, 0.1f, 0.1f, 1.0f };
float lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[] = { 100.0f, 100.0f, -100.0f, 1.0f };

float materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float materialDiffuse[] = { 0.5f, 0.2f, 0.7f, 1.0f };
float materialSpecular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
float materialShininess = 128.0f;

BOOL bLight = FALSE;

int counter = 1;

XMMATRIX perspectiveProjectionMatrix;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	HRESULT initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iRetval = 0;

	HRESULT hr = S_OK;

	// code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation Of Log File Faild. Exitting"), TEXT("File IO ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		//fopen(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "Log File Successfully Created\n\n");
		fclose(gpFile);
	}

	// initialization of WNDCLASS structure
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// register above class or wndclass 
	RegisterClassEx(&wndclass);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Mandar Dilip Maske RTR2021-116"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 
		(GetSystemMetrics(SM_CXSCREEN) / 2 - (WIN_WIDTH / 2)),
		(GetSystemMetrics(SM_CYSCREEN) / 2 - (WIN_HEIGHT / 2)),
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialize
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initialize function failed\n\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initialize function successful\n\n");
		fclose(gpFile);
	}

	// show window
	ShowWindow(hwnd, iCmdShow);

	// foregrounding and focusing the window
	SetForegroundWindow(hwnd);//here we use ghwnd and hwnd,but here ghwnd used for global function so we use hwnd
	//SetForegroundWindow: mala top la thev 
	SetFocus(hwnd);//tuchi widow active karnya sathi for good programing

	// game loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render the seen
				display();

				// update the seen
				update();
			}
		}
	}
	uninitialize();//ha change kala 

	return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration
	void ToggleFullScreen(void);
	HRESULT resize(int, int);

	// veriable declarations
	HRESULT hr = S_OK;

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;//yacha fayda game loop made honar if active window aasel kr game chalu aahe 
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;//yacha fayda game loop made honar if game band aahe 
		break;

	case WM_ERASEBKGND:
		//return(0); //ha bluescreen chya pude chalanar aahe
		break; //as this is retained mode graphics there is WM_PAINT to paint.

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			counter = 2;
			
			break;

		case 'L':
		case 'l':
			if (bLight == FALSE)
			{
				bLight = TRUE;
			}
			else
			{
				bLight = FALSE;
			}
			break;

		case 'V':
		case 'v':
			counter = 1;
			break;

		case 'Q':
		case 'q':
			DestroyWindow(hwnd);
			break;

		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 27:
			ToggleFullScreen();
			break;

		default:
			break;
		}
		break;

	case WM_SIZE://Window chya size varti graphics chi size avalabun rahanar aahe
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam)); //resize(width , hight);
		}
		if (FAILED(hr))
		{
			fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
			fprintf(gpFile, "resize() in WndProc failed\n\n");
			fclose(gpFile);
			return(hr);
		}
		break;

	case WM_CLOSE://yacha aartha hoto window close keli aahe 
		DestroyWindow(hwnd);//close kelelya window made thabun karnar ky mhanun tya window la destroy kr=>internally WM_DESTROY la call jatoch
		break;

	case WM_DESTROY:
		//cut kela varti WndProc made  return chya aadi past kela
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//varible declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	//code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = FALSE;

	}

}

HRESULT initialize(void) //convetion follow karayche mhanun initialize small
{
	// function declarations
	HRESULT printD3DInfo(void);
	HRESULT resize(int, int);
	HRESULT initializePerVertex(void);
	HRESULT initializePerPixel(void);

	// veriable declarations
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_Required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_Acquired = D3D_FEATURE_LEVEL_10_0;
	UINT numDrivers = 0;
	UINT createDeviceFlags = 0;
	UINT numFeatureLevels = 1;

	// code
	hr = printD3DInfo();

	// initialize swapchain descriptor structure
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDescriptor;
	//ZeroMemory((void*)&dxgiSwapChainDescriptor, sizeof(DXGI_SWAP_CHAIN_DESC));
	ZeroMemory((void*)&dxgiSwapChainDescriptor, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDescriptor.BufferCount = 1; // directx kade 1 dedicated buffer aasto ha aapan tayar katoy
	dxgiSwapChainDescriptor.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDescriptor.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDescriptor.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDescriptor.BufferDesc.RefreshRate.Numerator = 16;
	dxgiSwapChainDescriptor.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDescriptor.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDescriptor.OutputWindow = ghwnd;
	dxgiSwapChainDescriptor.SampleDesc.Count = 1;
	dxgiSwapChainDescriptor.SampleDesc.Quality = 0; // tu deshil to Quality
	dxgiSwapChainDescriptor.Windowed = TRUE;

	// call D3D11CreateDevice&SwapChain() for required driver
	numDrivers = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);
	for (UINT i = 0; i < numDrivers; i++)
	{
		d3dDriverType = d3dDriverTypes[i];

		hr = D3D11CreateDeviceAndSwapChain(NULL,//Primary aahe to de (Multiple ghaphics card aastil tr ekade veglya value pass karu shakto)
			d3dDriverType,
			NULL,
			createDeviceFlags,// default flag ghe mhanun 0
			&d3dFeatureLevel_Required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDescriptor,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_Acquired,
			&gpID3D11DeviceContext);

		if (SUCCEEDED(hr))
			break;

	}

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initialize:CreateDeviceAndSwapChain() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)

		// Print obtained driver type
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "initialize : D3D11 obtained harware driver Successfully\n\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "initialize : D3D11 obtained driver type warp Successfully\n\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "initialize : D3D11 obtained driver type reference Successfully\n\n");
		}
		else
		{
			fprintf(gpFile, "initialize : unknown d3dDriverType\n\n");
		}

		// Print obtained D3D11 feature level
		if (d3dFeatureLevel_Acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 11.0 feature level\n\n");
		}
		else if (d3dFeatureLevel_Acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 10.1 feature level\n\n");
		}
		else if (d3dFeatureLevel_Acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "initialize : D3D11 obtained 10.0 feature level\n\n");
		}
		else
		{
			fprintf(gpFile, "initialize : D3D11 obtained unknown feature level\n\n");
		}

		fclose(gpFile);
	}

	initializePerPixel();
	initializePerVertex();

	// c: set the state into rastrizer pipeline
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	clearColor[0] = 0.0f; // red
	clearColor[1] = 0.0f; // green
	clearColor[2] = 0.0f; // blue
	clearColor[3] = 1.0f; // alpha

	perspectiveProjectionMatrix = XMMatrixIdentity();

	// warmup resize
	hr = resize(WIN_WIDTH, WIN_HEIGHT);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initialize:resize() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initialize:resize() successful\n\n");
		fclose(gpFile);
	}

	return(hr);
}

HRESULT initializePerPixel(void)
{
	// vertex shader
	const char* vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldMatrix;" \
		"float4x4 viewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la;" \
		"float4 ld;" \
		"float4 ls;" \
		"float4 lightPosition;" \
		"float4 ka;" \
		"float4 kd;" \
		"float4 ks;" \
		"float materialShininess;" \
		"uint lightingEnabled;" \
		"}" \
		"struct vertex" \
		"{" \
		"float4 position:SV_POSITION;" \
		"float3 transformedNormals:NORMAL0;" \
		"float3 lightDirection:NORMAL1;" \
		"float3 viewerVector:NORMAL2;" \
		"};" \
		"vertex main(float4 position:POSITION, float4 normal:NORMAL)" \
		"{" \
		"vertex output;" \
		"if(lightingEnabled==1)" \
		"{" \
		"float4 eyeCoordinates=mul(worldMatrix,position);" \
		"eyeCoordinates=mul(viewMatrix,eyeCoordinates);" \
		"float3x3 normalMatrix=(float3x3)worldMatrix;" \
		"output.transformedNormals=mul(normalMatrix,(float3)normal);" \
		"output.lightDirection=(float3)lightPosition-(float3)eyeCoordinates;" \
		"output.viewerVector=-eyeCoordinates.xyz;" \
		"}" \
		"float4 pos=mul(worldMatrix,position);" \
		"pos=mul(viewMatrix,pos);" \
		"pos=mul(projectionMatrix,pos);" \
		"output.position=pos;" \
		"return output;" \
		"}";

	// compile vertex shader
	ID3DBlob* pID3DBlob_VertexShaderSourceCode = NULL;
	ID3DBlob* pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode, // source code
		lstrlenA(vertexShaderSourceCode) + 1, // last /0 consider hot nahi mhanun+1
		"VS", // vertex shader det aahe
		NULL, // shader # define che struct aahe
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // header file pn include karu shakto ya parameter mule by default include hotat
		"main", // entry point function che nav dya
		"vs_5_0", // ha vertex shader aasun 5.0 tyachi feature level aahe 
		0, // comiple constants dyaycha nahi
		0, // effect constants dyaycha nahi
		&pID3DBlob_VertexShaderSourceCode, // blob=(void*) compile karun zalela code ya blob made thev 
		&pID3DBlob_Error); // error aali tr ekade tak

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		if (pID3DBlob_Error)
		{
			fprintf(gpFile, "initializePerPixel : D3DCompile() failed for vertex shader : %s\n\n", (char*)pID3DBlob_Error->GetBufferPointer()); // shader made error aasel tr he print honar
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else
		{
			fprintf(gpFile, "initializePerPixel : D3DCompile() failed for vertex shader : Unknown\n\n"); // shader made error aasel tr he print honar
		}
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : D3DCompile() successful for vertex shader\n\n");
		fclose(gpFile);
	}

	// create vertex shader 
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderSourceCode->GetBufferPointer(),// vertex shader cha compiled code de
		pID3DBlob_VertexShaderSourceCode->GetBufferSize(), // ya compiled vertex shader cha size de
		NULL,// veribale multiple shader share karnar aaslo tr aat madli function vaprun include karave lagte
		&gpID3D11VertexShader_PP); // kasha made harayche te

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateVertexShader() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateVertexShader() successful\n\n");
		fclose(gpFile);
	}

	// set this vertex shader in vertex shader of pipe line
	//gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PP, NULL, 0); // konala set karu, class linkage vaprla nahi tya nule null(share kelele veriables),2nd parameter chi length

	// pixel shader
	const char* pixelShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldMatrix;" \
		"float4x4 viewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la;" \
		"float4 ld;" \
		"float4 ls;" \
		"float4 lightPosition;" \
		"float4 ka;" \
		"float4 kd;" \
		"float4 ks;" \
		"float materialShininess;" \
		"uint lightingEnabled;" \
		"}" \
		"struct vertex" \
		"{" \
		"float4 position:SV_POSITION;" \
		"float3 transformedNormals:NORMAL0;" \
		"float3 lightDirection:NORMAL1;" \
		"float3 viewerVector:NORMAL2;" \
		"};" \
		"float4 main(vertex input):SV_TARGET" \
		"{" \
		"float3 phong_ads_light;" \
		"if(lightingEnabled==1)" \
		"{" \
		"float3 normalised_transformed_normals=normalize(input.transformedNormals);" \
		"float3 normalised_light_direction=normalize(input.lightDirection);" \
		"float3 normalised_viewerVector=normalize(input.viewerVector);" \
		"float3 ambient=la*ka;" \
		"float3 diffuse=ld*kd*max(dot(normalised_light_direction,normalised_transformed_normals),0.0);" \
		"float3 reflectionVector=reflect(-normalised_light_direction,normalised_transformed_normals);" \
		"float specular=ls*ks*pow(max(dot(reflectionVector,normalised_viewerVector),0.0),materialShininess);" \
		"phong_ads_light=ambient+diffuse+specular;" \
		"}" \
		"else" \
		"{"
		"phong_ads_light=float3(1.0,1.0,1.0);" \
		"}"
		"return float4(phong_ads_light,1.0);" \
		"}";

	// compile pixel shader
	ID3DBlob* pID3DBlob_PixelShaderSourceCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode, // source code
		lstrlenA(pixelShaderSourceCode) + 1, // last /0 consider hot nahi mhanun+1
		"PS", // vertex shader det aahe
		NULL, // shader # define che struct aahe
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // header file pn include karu shakto ya parameter mule by default include hotat
		"main", // entry point function che nav dya
		"ps_5_0", // ha vertex shader aasun 5.0 tyachi feature level aahe 
		0, // comiple constants dyaycha nahi
		0, // effect constants dyaycha nahi
		&pID3DBlob_PixelShaderSourceCode, // blob=(void*) compile karun zalela code ya blob made thev 
		&pID3DBlob_Error); // error aali tr ekade tak

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		if (pID3DBlob_Error)
		{
			fprintf(gpFile, "initializePerPixel : D3DCompile() failed for pixel shader : %s\n\n", (char*)pID3DBlob_Error->GetBufferPointer()); // shader made error aasel tr he print honar
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else
		{
			fprintf(gpFile, "D3DCompile() failed for pixel shader : Unknown\n\n"); // shader made error aasel tr he print honar
		}
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : D3DCompile() successful for pixel shader\n\n");
		fclose(gpFile);
	}

	// create pixel shader 
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderSourceCode->GetBufferPointer(),// vertex shader cha compiled code de
		pID3DBlob_PixelShaderSourceCode->GetBufferSize(), // ya compiled vertex shader cha size de
		NULL,// veribale multiple shader share karnar aaslo tr aat madli function vaprun include karave lagte
		&gpID3D11PixelShader_PP); // kasha made harayche te

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateVertexShader() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateVertexShader() successful\n\n");
		fclose(gpFile);
	}

	// set this pixel shader in vertex shader of pipe line
	//gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PP, NULL, 0); // konala set karu, class linkage vaprla nahi tya nule null(share kelele veriables),2nd parameter chi length

	// input layout ( shader la patavyache in aani out sangne)
	D3D11_INPUT_ELEMENT_DESC d3d11_Input_Element_Descriptor[2];
	ZeroMemory((void*)d3d11_Input_Element_Descriptor, sizeof(D3D11_INPUT_ELEMENT_DESC) * _ARRAYSIZE(d3d11_Input_Element_Descriptor));

	// initialize input layout structure
	d3d11_Input_Element_Descriptor[0].SemanticName = "POSITION";
	d3d11_Input_Element_Descriptor[0].SemanticIndex = 0;
	d3d11_Input_Element_Descriptor[0].Format = DXGI_FORMAT_R32G32B32_FLOAT; // RGB color sathi nasun to 3 float sarkha aahe similar  
	d3d11_Input_Element_Descriptor[0].InputSlot = 0; // ha pude badalnar aahe same like created by enum created for attributes in OpenGL
	d3d11_Input_Element_Descriptor[0].AlignedByteOffset = 0; // multiple buffer denar aaslo tr madlya buffer cha byte offset dya
	d3d11_Input_Element_Descriptor[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	d3d11_Input_Element_Descriptor[0].InstanceDataStepRate = 0; // ha valid per instance aasta tr aasta (mhanje kiti chi gap takaychi te)

	d3d11_Input_Element_Descriptor[1].SemanticName = "NORMAL";
	d3d11_Input_Element_Descriptor[1].SemanticIndex = 0;
	d3d11_Input_Element_Descriptor[1].Format = DXGI_FORMAT_R32G32B32_FLOAT; // RGB color sathi nasun to 3 float sarkha aahe similar  
	d3d11_Input_Element_Descriptor[1].InputSlot = 1; // ha pude badalnar aahe same like created by enum created for attributes in OpenGL
	d3d11_Input_Element_Descriptor[1].AlignedByteOffset = 0; // multiple buffer denar aaslo tr madlya buffer cha byte offset dya
	d3d11_Input_Element_Descriptor[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	d3d11_Input_Element_Descriptor[1].InstanceDataStepRate = 0; // ha valid per instance aasta tr aasta (mhanje kiti chi gap takaychi te)

	// create input layout
	hr = gpID3D11Device->CreateInputLayout(d3d11_Input_Element_Descriptor, _ARRAYSIZE(d3d11_Input_Element_Descriptor), pID3DBlob_VertexShaderSourceCode->GetBufferPointer(), pID3DBlob_VertexShaderSourceCode->GetBufferSize(), &gpID3D11InputLayout_PP);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateInputLayout() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateInputLayout() successful\n\n");
		fclose(gpFile);
	}

	// set this input layout in input assembly state of pipeline
	//gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout_PP);

	pID3DBlob_VertexShaderSourceCode->Release();
	pID3DBlob_VertexShaderSourceCode = NULL;

	pID3DBlob_PixelShaderSourceCode->Release();
	pID3DBlob_PixelShaderSourceCode = NULL;

	// geometry
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// create vertex buffer for above position vertices

	// position
	// a: initialize buffer descriptor structure(glGenBuffers,glBindBuffer)
	D3D11_BUFFER_DESC d3d11_BufferDescriptor;
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = sizeof(float) * gNumVertices * 3;
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	// b: initialize subresource data structure to put data into the buffer(glBufferData)
	D3D11_SUBRESOURCE_DATA d3d11_SubresourceData;
	ZeroMemory((void*)&d3d11_SubresourceData, sizeof(D3D11_SUBRESOURCE_DATA));

	d3d11_SubresourceData.pSysMem = sphere_vertices;

	// c: create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, &d3d11_SubresourceData, &gpID3D11Buffer_Sphere_PositionBuffer_PP);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() failed for position\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() successful for position\n\n");
		fclose(gpFile);
	}

	// normal
	// a: initialize buffer descriptor structure(glGenBuffers,glBindBuffer)
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = sizeof(float) * _ARRAYSIZE(sphere_normals);
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	// b: initialize subresource data structure to put data into the buffer(glBufferData)
	ZeroMemory((void*)&d3d11_SubresourceData, sizeof(D3D11_SUBRESOURCE_DATA));

	d3d11_SubresourceData.pSysMem = sphere_normals;

	// c: create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, &d3d11_SubresourceData, &gpID3D11Buffer_Sphere_NormalBuffer_PP);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() failed for normal\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() successful for normal\n\n");
		fclose(gpFile);
	}

	// index buffer
	// a: initialize buffer descriptor structure(glGenBuffers,glBindBuffer)
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = gNumElements * sizeof(short);
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_INDEX_BUFFER;

	// b: initialize subresource data structure to put data into the buffer(glBufferData)
	ZeroMemory((void*)&d3d11_SubresourceData, sizeof(D3D11_SUBRESOURCE_DATA));

	d3d11_SubresourceData.pSysMem = sphere_elements;

	// c: create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, &d3d11_SubresourceData, &gpID3D11Buffer_Sphere_IndexBuffer_PP);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() failed for sphere index\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() successful for sphere index\n\n");
		fclose(gpFile);
	}

	// constant buffer

	// a: initialize buffer descriptor structure
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = sizeof(CBUFFER);
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	// b : create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, NULL, &gpID3D11Buffer_ConstantBuffer_PP);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() failed for constant\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateBuffer() successful for constant\n\n");
		fclose(gpFile);
	}

	// c : set constant buffer into vertex shader pipline
	//gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PP); // kitva aahe slot,array made buffer kiti aahet, //hi line vertex shader stage made constant buffer set karte
	//gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PP); // ya line ne pixel shader stage made constant buffer set karte
	// second line is imp for pixel shader	

	// enabling rastrizer stage
	// a: initialize rastrizer descriptor
	D3D11_RASTERIZER_DESC d3d11RastrizerDescrptor;
	ZeroMemory((void*)&d3d11RastrizerDescrptor, sizeof(D3D11_RASTERIZER_DESC));

	d3d11RastrizerDescrptor.CullMode = D3D11_CULL_NONE; // no culling
	d3d11RastrizerDescrptor.FillMode = D3D11_FILL_SOLID; //same like gl_polygongmode
	d3d11RastrizerDescrptor.FrontCounterClockwise = false; // Opengl sarkhe FrontCounterClockwise sarkhe karu ka
	d3d11RastrizerDescrptor.MultisampleEnable = FALSE;
	d3d11RastrizerDescrptor.ScissorEnable = FALSE;
	d3d11RastrizerDescrptor.DepthClipEnable = TRUE; // depth detoy tevdech dakhav
	d3d11RastrizerDescrptor.AntialiasedLineEnable = FALSE;
	d3d11RastrizerDescrptor.DepthBias = 0;
	d3d11RastrizerDescrptor.DepthBiasClamp = 0.0; // depth pahije tr kiti
	d3d11RastrizerDescrptor.SlopeScaledDepthBias = FALSE; // shados la slope deu shakto

	// b: CreateRasterizerState accordingly 
	hr = gpID3D11Device->CreateRasterizerState(&d3d11RastrizerDescrptor, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateRasterizerState() failed for constant\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerPixel : gpID3D11Device:CreateRasterizerState() successful for constant\n\n");
		fclose(gpFile);
	}

	return(hr);
}


HRESULT initializePerVertex(void)
{
	// vertex shader
	const char* vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldMatrix;" \
		"float4x4 viewMatrix;" \
		"float4x4 projectionMatrix;" \
		"float4 la;" \
		"float4 ld;" \
		"float4 ls;" \
		"float4 lightPosition;" \
		"float4 ka;" \
		"float4 kd;" \
		"float4 ks;" \
		"float materialShininess;" \
		"uint lightingEnabled;" \
		"}" \
		"struct vertex" \
		"{" \
		"float4 position:SV_POSITION;" \
		"float3 phong_ads_light:COLOR;" \
		"};" \
		"vertex main(float4 position:POSITION, float4 normal:NORMAL)" \
		"{" \
		"vertex output;" \
		"if(lightingEnabled==1)" \
		"{" \
		"float3 ambient=la*ka;" \
		"float4 eyeCoordinates=mul(worldMatrix,position);" \
		"eyeCoordinates=mul(viewMatrix,eyeCoordinates);" \
		"float3x3 normalMatrix=(float3x3)worldMatrix;" \
		"float3 transformedNormals=normalize(mul(normalMatrix,(float3)normal));" \
		"float3 lightDirection=normalize((float3)lightPosition-(float3)eyeCoordinates);" \
		"float3 diffuse=ld*kd*max(dot(lightDirection,transformedNormals),0.0);" \
		"float3 reflectionVector=reflect(-lightDirection,transformedNormals);" \
		"float3 viewerVector=normalize(-eyeCoordinates.xyz);" \
		"float3 specular=ls*ks*pow(max(dot(reflectionVector,viewerVector),0.0),materialShininess);" \
		"output.phong_ads_light=ambient+diffuse+specular;" \
		"}" \
		"else" \
		"{" \
		"output.phong_ads_light=float3(1.0,1.0,1.0);" \
		"}" \
		"float4 pos=mul(worldMatrix,position);" \
		"pos=mul(viewMatrix,pos);" \
		"pos=mul(projectionMatrix,pos);" \
		"output.position=pos;" \
		"return output;" \
		"}";

	// compile vertex shader
	ID3DBlob* pID3DBlob_VertexShaderSourceCode = NULL;
	ID3DBlob* pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode, // source code
		lstrlenA(vertexShaderSourceCode) + 1, // last /0 consider hot nahi mhanun+1
		"VS", // vertex shader det aahe
		NULL, // shader # define che struct aahe
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // header file pn include karu shakto ya parameter mule by default include hotat
		"main", // entry point function che nav dya
		"vs_5_0", // ha vertex shader aasun 5.0 tyachi feature level aahe 
		0, // comiple constants dyaycha nahi
		0, // effect constants dyaycha nahi
		&pID3DBlob_VertexShaderSourceCode, // blob=(void*) compile karun zalela code ya blob made thev 
		&pID3DBlob_Error); // error aali tr ekade tak

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		if (pID3DBlob_Error)
		{
			fprintf(gpFile, "initializePerVertex : D3DCompile() failed for vertex shader : %s\n\n", (char*)pID3DBlob_Error->GetBufferPointer()); // shader made error aasel tr he print honar
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else
		{
			fprintf(gpFile, "initializePerVertex : D3DCompile() failed for vertex shader : Unknown\n\n"); // shader made error aasel tr he print honar
		}
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : D3DCompile() successful for vertex shader\n\n");
		fclose(gpFile);
	}

	// create vertex shader 
	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderSourceCode->GetBufferPointer(),// vertex shader cha compiled code de
		pID3DBlob_VertexShaderSourceCode->GetBufferSize(), // ya compiled vertex shader cha size de
		NULL,// veribale multiple shader share karnar aaslo tr aat madli function vaprun include karave lagte
		&gpID3D11VertexShader); // kasha made harayche te

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateVertexShader() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateVertexShader() successful\n\n");
		fclose(gpFile);
	}

	// set this vertex shader in vertex shader of pipe line
	//gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, NULL, 0); // konala set karu, class linkage vaprla nahi tya nule null(share kelele veriables),2nd parameter chi length

	// pixel shader
	const char* pixelShaderSourceCode =
		"struct vertex" \
		"{" \
		"float4 position:SV_POSITION;" \
		"float3 phong_ads_light:COLOR;" \
		"};" \
		"float4 main(vertex input):SV_TARGET" \
		"{" \
		"return float4(input.phong_ads_light,1.0);" \
		"}";

	// compile pixel shader
	ID3DBlob* pID3DBlob_PixelShaderSourceCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode, // source code
		lstrlenA(pixelShaderSourceCode) + 1, // last /0 consider hot nahi mhanun+1
		"PS", // vertex shader det aahe
		NULL, // shader # define che struct aahe
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // header file pn include karu shakto ya parameter mule by default include hotat
		"main", // entry point function che nav dya
		"ps_5_0", // ha vertex shader aasun 5.0 tyachi feature level aahe 
		0, // comiple constants dyaycha nahi
		0, // effect constants dyaycha nahi
		&pID3DBlob_PixelShaderSourceCode, // blob=(void*) compile karun zalela code ya blob made thev 
		&pID3DBlob_Error); // error aali tr ekade tak

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		if (pID3DBlob_Error)
		{
			fprintf(gpFile, "initializePerVertex : D3DCompile() failed for pixel shader : %s\n\n", (char*)pID3DBlob_Error->GetBufferPointer()); // shader made error aasel tr he print honar
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else
		{
			fprintf(gpFile, "initializePerVertex : D3DCompile() failed for pixel shader : Unknown\n\n"); // shader made error aasel tr he print honar
		}
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : D3DCompile() successful for pixel shader\n\n");
		fclose(gpFile);
	}

	// create pixel shader 
	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderSourceCode->GetBufferPointer(),// vertex shader cha compiled code de
		pID3DBlob_PixelShaderSourceCode->GetBufferSize(), // ya compiled vertex shader cha size de
		NULL,// veribale multiple shader share karnar aaslo tr aat madli function vaprun include karave lagte
		&gpID3D11PixelShader); // kasha made harayche te

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateVertexShader() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateVertexShader() successful\n\n");
		fclose(gpFile);
	}

	// set this pixel shader in vertex shader of pipe line
	//gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, NULL, 0); // konala set karu, class linkage vaprla nahi tya nule null(share kelele veriables),2nd parameter chi length

	// input layout ( shader la patavyache in aani out sangne)
	D3D11_INPUT_ELEMENT_DESC d3d11_Input_Element_Descriptor[2];
	ZeroMemory((void*)d3d11_Input_Element_Descriptor, sizeof(D3D11_INPUT_ELEMENT_DESC) * _ARRAYSIZE(d3d11_Input_Element_Descriptor));

	// initialize input layout structure
	d3d11_Input_Element_Descriptor[0].SemanticName = "POSITION";
	d3d11_Input_Element_Descriptor[0].SemanticIndex = 0;
	d3d11_Input_Element_Descriptor[0].Format = DXGI_FORMAT_R32G32B32_FLOAT; // RGB color sathi nasun to 3 float sarkha aahe similar  
	d3d11_Input_Element_Descriptor[0].InputSlot = 0; // ha pude badalnar aahe same like created by enum created for attributes in OpenGL
	d3d11_Input_Element_Descriptor[0].AlignedByteOffset = 0; // multiple buffer denar aaslo tr madlya buffer cha byte offset dya
	d3d11_Input_Element_Descriptor[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	d3d11_Input_Element_Descriptor[0].InstanceDataStepRate = 0; // ha valid per instance aasta tr aasta (mhanje kiti chi gap takaychi te)

	d3d11_Input_Element_Descriptor[1].SemanticName = "NORMAL";
	d3d11_Input_Element_Descriptor[1].SemanticIndex = 0;
	d3d11_Input_Element_Descriptor[1].Format = DXGI_FORMAT_R32G32B32_FLOAT; // RGB color sathi nasun to 3 float sarkha aahe similar  
	d3d11_Input_Element_Descriptor[1].InputSlot = 1; // ha pude badalnar aahe same like created by enum created for attributes in OpenGL
	d3d11_Input_Element_Descriptor[1].AlignedByteOffset = 0; // multiple buffer denar aaslo tr madlya buffer cha byte offset dya
	d3d11_Input_Element_Descriptor[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	d3d11_Input_Element_Descriptor[1].InstanceDataStepRate = 0; // ha valid per instance aasta tr aasta (mhanje kiti chi gap takaychi te)

	// create input layout
	hr = gpID3D11Device->CreateInputLayout(d3d11_Input_Element_Descriptor, _ARRAYSIZE(d3d11_Input_Element_Descriptor), pID3DBlob_VertexShaderSourceCode->GetBufferPointer(), pID3DBlob_VertexShaderSourceCode->GetBufferSize(), &gpID3D11InputLayout);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateInputLayout() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateInputLayout() successful\n\n");
		fclose(gpFile);
	}

	// set this input layout in input assembly state of pipeline
	//gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	pID3DBlob_VertexShaderSourceCode->Release();
	pID3DBlob_VertexShaderSourceCode = NULL;

	pID3DBlob_PixelShaderSourceCode->Release();
	pID3DBlob_PixelShaderSourceCode = NULL;

	// geometry
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// create vertex buffer for above position vertices

	// position
	// a: initialize buffer descriptor structure(glGenBuffers,glBindBuffer)
	D3D11_BUFFER_DESC d3d11_BufferDescriptor;
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = sizeof(float) * gNumVertices * 3;
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	// b: initialize subresource data structure to put data into the buffer(glBufferData)
	D3D11_SUBRESOURCE_DATA d3d11_SubresourceData;
	ZeroMemory((void*)&d3d11_SubresourceData, sizeof(D3D11_SUBRESOURCE_DATA));

	d3d11_SubresourceData.pSysMem = sphere_vertices;

	// c: create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, &d3d11_SubresourceData, &gpID3D11Buffer_Sphere_PositionBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() failed for position\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() successful for position\n\n");
		fclose(gpFile);
	}

	// normal
	// a: initialize buffer descriptor structure(glGenBuffers,glBindBuffer)
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = sizeof(float) * _ARRAYSIZE(sphere_normals);
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	// b: initialize subresource data structure to put data into the buffer(glBufferData)
	ZeroMemory((void*)&d3d11_SubresourceData, sizeof(D3D11_SUBRESOURCE_DATA));

	d3d11_SubresourceData.pSysMem = sphere_normals;

	// c: create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, &d3d11_SubresourceData, &gpID3D11Buffer_Sphere_NormalBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() failed for normal\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() successful for normal\n\n");
		fclose(gpFile);
	}

	// index buffer
	// a: initialize buffer descriptor structure(glGenBuffers,glBindBuffer)
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = gNumElements * sizeof(short);
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_INDEX_BUFFER;

	// b: initialize subresource data structure to put data into the buffer(glBufferData)
	ZeroMemory((void*)&d3d11_SubresourceData, sizeof(D3D11_SUBRESOURCE_DATA));

	d3d11_SubresourceData.pSysMem = sphere_elements;

	// c: create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, &d3d11_SubresourceData, &gpID3D11Buffer_Sphere_IndexBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() failed for sphere index\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() successful for sphere index\n\n");
		fclose(gpFile);
	}

	// constant buffer

	// a: initialize buffer descriptor structure
	ZeroMemory((void*)&d3d11_BufferDescriptor, sizeof(D3D11_BUFFER_DESC));

	d3d11_BufferDescriptor.Usage = D3D11_USAGE_DEFAULT; // kasa vapraycha aahe(DEFAULT : gpu readable writeable) // like gl_static_draw
	d3d11_BufferDescriptor.ByteWidth = sizeof(CBUFFER);
	d3d11_BufferDescriptor.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	// b : create the actual buffer
	hr = gpID3D11Device->CreateBuffer(&d3d11_BufferDescriptor, NULL, &gpID3D11Buffer_ConstantBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() failed for constant\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateBuffer() successful for constant\n\n");
		fclose(gpFile);
	}

	// c : set constant buffer into vertex shader pipline
	//gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer); // kitva aahe slot,array made buffer kiti aahet, //hi line vertex shader stage made constant buffer set karte
	//gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer); // ya line ne pixel shader stage made constant buffer set karte

	// enabling rastrizer stage
	// a: initialize rastrizer descriptor
	D3D11_RASTERIZER_DESC d3d11RastrizerDescrptor;
	ZeroMemory((void*)&d3d11RastrizerDescrptor, sizeof(D3D11_RASTERIZER_DESC));

	d3d11RastrizerDescrptor.CullMode = D3D11_CULL_NONE; // no culling
	d3d11RastrizerDescrptor.FillMode = D3D11_FILL_SOLID; //same like gl_polygongmode
	d3d11RastrizerDescrptor.FrontCounterClockwise = false; // Opengl sarkhe FrontCounterClockwise sarkhe karu ka
	d3d11RastrizerDescrptor.MultisampleEnable = FALSE;
	d3d11RastrizerDescrptor.ScissorEnable = FALSE;
	d3d11RastrizerDescrptor.DepthClipEnable = TRUE; // depth detoy tevdech dakhav
	d3d11RastrizerDescrptor.AntialiasedLineEnable = FALSE;
	d3d11RastrizerDescrptor.DepthBias = 0;
	d3d11RastrizerDescrptor.DepthBiasClamp = 0.0; // depth pahije tr kiti
	d3d11RastrizerDescrptor.SlopeScaledDepthBias = FALSE; // shados la slope deu shakto

	// b: CreateRasterizerState accordingly 
	hr = gpID3D11Device->CreateRasterizerState(&d3d11RastrizerDescrptor, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateRasterizerState() failed for constant\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "initializePerVertex : gpID3D11Device:CreateRasterizerState() successful for constant\n\n");
		fclose(gpFile);
	}
}

HRESULT printD3DInfo(void)
{
	// veriable declarations
	HRESULT hr = S_OK; // S_OK : Success_OK
	IDXGIFactory* pIDXGIFactory = NULL;
	IDXGIAdapter* pIDXGIAdapter = NULL;
	DXGI_ADAPTER_DESC dxgiAdapterDescriptor; // all DESC in directx are descriptor
	char str[255];

	// code
	// createDXGIFactory
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory); //__uuidof(IDXGIFactory) ya function la idxgi navachya interface chi guid de
	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "CreateDXGIFactory() failed\n\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "CreateDXGIFactory() successful\n\n");
		fclose(gpFile);
	}

	// get IDXGIAdaptor
	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) != DXGI_ERROR_NOT_FOUND) // EnumAdapters(0// 0th graphics card cha adapter de,pIDXGIAdaptor// ya made bharun de)
	{
		// get description of found adapter
		ZeroMemory((void*)&dxgiAdapterDescriptor, sizeof(DXGI_ADAPTER_DESC));
		pIDXGIAdapter->GetDesc(&dxgiAdapterDescriptor);
		WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDescriptor.Description, 255, str, 255, NULL, NULL); // ACP - ANSI CODE PAGE
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "GraphicsCardName = %s\n", str);
		fprintf(gpFile, "GraphicsMemory(VRAM) = %dGB\n\n", (int)ceil(dxgiAdapterDescriptor.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0));
		fclose(gpFile);
	}
	else
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "IDXGI_ADAPTER_DESC() failed\n\n");
		fclose(gpFile);
	}

	// release
	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	return(hr);
}

HRESULT resize(int width, int height)
{
	// veriable declarations
	HRESULT hr = S_OK;

	// code
	if (height == 0)
		height = 1; // to avoid divided by 0 illegal instruction for feature code

	// step1: release exting RTV(Render Target View)
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// step2: tellswap chain to resize buffers according to new width and height
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0); // (kiti buffer resize karayche,width,height, format ky aahe, no flag(0))

	// step3: get new resized buffer from swap chain into a dummy texture
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer = NULL;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer); //0th index mhanun(0), interface chi guid,konamade pahije

	// step4 : now create new RTV using above buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView); //konapasun tayar karaycha aahe,RTV cha descriptor aahe ka,konamade pahije

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "resize() failed\n\n");
		fclose(gpFile);
		return(hr);
	}

	// release dummy texture interface
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// Initialize texture 2D Descriptor
	D3D11_TEXTURE2D_DESC d3d11Texture2DDescriptor;

	ZeroMemory((void*)&d3d11Texture2DDescriptor, sizeof(D3D11_TEXTURE2D_DESC));

	d3d11Texture2DDescriptor.Width = (UINT)width;
	d3d11Texture2DDescriptor.Height = (UINT)height;
	d3d11Texture2DDescriptor.ArraySize = 1;
	d3d11Texture2DDescriptor.MipLevels = 1;
	d3d11Texture2DDescriptor.SampleDesc.Count = 1;
	d3d11Texture2DDescriptor.SampleDesc.Quality = 0; // DirectX Quality tu thrav
	d3d11Texture2DDescriptor.Usage = D3D11_USAGE_DEFAULT;
	d3d11Texture2DDescriptor.Format = DXGI_FORMAT_D32_FLOAT; // consider depth is 32 bit float
	d3d11Texture2DDescriptor.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3d11Texture2DDescriptor.CPUAccessFlags = 0; // CPU la default right de
	d3d11Texture2DDescriptor.MiscFlags = 0; // no miscelenous flags

	// declare a 2D texture which is converted into depth buffer
	ID3D11Texture2D* pID3D11Texture2D_DepthBuffer = NULL;

	hr = gpID3D11Device->CreateTexture2D(&d3d11Texture2DDescriptor, NULL, &pID3D11Texture2D_DepthBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "resize()::CreateTexture2D failed\n\n");
		fclose(gpFile);
		return(hr);
	}

	// Initialize depth stencil view descriptor
	D3D11_DEPTH_STENCIL_VIEW_DESC d3d11DepthStencilViewDescriptor;
	ZeroMemory((void*)&d3d11DepthStencilViewDescriptor, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	d3d11DepthStencilViewDescriptor.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11DepthStencilViewDescriptor.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS; // MS - Multi - Sampled

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &d3d11DepthStencilViewDescriptor, &gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, "Log.txt", "a+"); // file open kr pn write karay nahi append karnya sathi open kr(append pahila data thevne new data add karne) (a+=wirte apend)
		fprintf(gpFile, "resize()::CreateDepthStencilView failed\n\n");
		fclose(gpFile);
		pID3D11Texture2D_DepthBuffer->Release();
		pID3D11Texture2D_DepthBuffer = NULL;
		return(hr);
	}

	// Release the local depth buffer
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	// step5: set this new RTV in pipeline
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView); // kiti,konta,depth nahi tya mule NULL

	// step6: initialize viewpoart structure 
	D3D11_VIEWPORT d3d11Viewport;
	d3d11Viewport.TopLeftX = 0.0f;
	d3d11Viewport.TopLeftY = 0.0f;
	d3d11Viewport.Width = (float)width;
	d3d11Viewport.Height = (float)height;
	d3d11Viewport.MinDepth = 0.0f;
	d3d11Viewport.MaxDepth = 1.0f;

	// step7: tell device context to set this viewport in pipeline(similar like GLViewPoart)
	gpID3D11DeviceContext->RSSetViewports(1, &d3d11Viewport); //kiti set karayche

	// set projection matrix
	perspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);
}

void display(void)
{
	// code
	// function declaration
	void displayPerVertex(void);
	void displayPerPixel(void);

	//step1: clear the RTV using clear color(glClear())
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, clearColor); //konta,kontya ranga ne,
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0); // 2 and 3rd para anologouse to clearDepth(1.0);

	if (counter == 1)
	{
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, NULL, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, NULL, 0);

		gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

		gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
		//gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer); // pv aaslya mule ha lagat nahi thevla tr error yet nahi logic sathi he thevle aahe
	}
	else if (counter == 2)
	{
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PP, NULL, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PP, NULL, 0);

		gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout_PP);

		gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PP);
		gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PP);
	}

	if (counter == 1)
	{
		displayPerVertex();
	}
	else //(counter == 2)
	{
		displayPerPixel();
	}

	// swap buffers by presinting them
	gpIDXGISwapChain->Present(0, 0); // monitor chya refresh rate shi,all buffer la swap karayche aahr
}

void displayPerVertex(void)
{
	// set position buffer in input assembly stage of pipline
	UINT stride = sizeof(float) * 3;
	UINT offset = 0; // glVertexAttribPointer last che 2 parameter

	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_Sphere_PositionBuffer, &stride, &offset); // multiple vertex buffer aastil tr, kiti aahet, address aaray cha

	// set color buffer in input assembly stage of pipline
	stride = sizeof(float) * 3;
	offset = 0; // glVertexAttribPointer last che 2 parameter

	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_Sphere_NormalBuffer, &stride, &offset); // multiple vertex buffer aastil tr, kiti aahet, address aaray cha

	// set index
	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_Sphere_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	// set PrimitiveTopology in input oipeline stage
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// trasformations
	// a: initialize matrices
	XMMATRIX translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 2.0f);
	XMMATRIX viewMatrix = XMMatrixIdentity();

	XMMATRIX worldMatrix = translationMatrix; // order is imp
	XMMATRIX mvpMatrix = worldMatrix * viewMatrix * perspectiveProjectionMatrix;

	// b: put them into constant buffer
	CBUFFER cBuffer;
	ZeroMemory((void*)&cBuffer, sizeof(CBUFFER));

	cBuffer.WorldMatrix = worldMatrix;
	cBuffer.ViewMatrix = viewMatrix;
	cBuffer.ProjectionMatrix = perspectiveProjectionMatrix;

	//light related code
	if (bLight == TRUE)
	{
		cBuffer.la = XMVectorSet(lightAmbient[0], lightAmbient[1], lightAmbient[2], lightAmbient[3]);
		cBuffer.ld = XMVectorSet(lightDiffuse[0], lightDiffuse[1], lightDiffuse[2], lightDiffuse[3]);
		cBuffer.ls = XMVectorSet(lightSpecular[0], lightSpecular[1], lightSpecular[2], lightSpecular[3]);
		cBuffer.lightPosition = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

		cBuffer.ka = XMVectorSet(materialAmbient[0], materialAmbient[1], materialAmbient[2], materialAmbient[3]);
		cBuffer.kd = XMVectorSet(materialDiffuse[0], materialDiffuse[1], materialDiffuse[2], materialDiffuse[3]);
		cBuffer.ks = XMVectorSet(materialSpecular[0], materialSpecular[1], materialSpecular[2], materialSpecular[3]);
		cBuffer.materialShininess = materialShininess;
		cBuffer.lightingEnabled = 1;
	}
	else
	{
		cBuffer.lightingEnabled = 0;
	}

	// c: push them into the shader
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &cBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);
}

void displayPerPixel(void)
{
	// set position buffer in input assembly stage of pipline
	UINT stride = sizeof(float) * 3;
	UINT offset = 0; // glVertexAttribPointer last che 2 parameter

	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_Sphere_PositionBuffer_PP, &stride, &offset); // multiple vertex buffer aastil tr, kiti aahet, address aaray cha

	// set color buffer in input assembly stage of pipline
	stride = sizeof(float) * 3;
	offset = 0; // glVertexAttribPointer last che 2 parameter

	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_Sphere_NormalBuffer_PP, &stride, &offset); // multiple vertex buffer aastil tr, kiti aahet, address aaray cha

	// set index
	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_Sphere_IndexBuffer_PP, DXGI_FORMAT_R16_UINT, 0);

	// set PrimitiveTopology in input oipeline stage
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// trasformations
	// a: initialize matrices
	XMMATRIX translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 2.0f);
	XMMATRIX viewMatrix = XMMatrixIdentity();

	XMMATRIX worldMatrix = translationMatrix; // order is imp
	XMMATRIX mvpMatrix = worldMatrix * viewMatrix * perspectiveProjectionMatrix;

	// b: put them into constant buffer
	CBUFFER cBuffer;
	ZeroMemory((void*)&cBuffer, sizeof(CBUFFER));

	cBuffer.WorldMatrix = worldMatrix;
	cBuffer.ViewMatrix = viewMatrix;
	cBuffer.ProjectionMatrix = perspectiveProjectionMatrix;

	//light related code
	if (bLight == TRUE)
	{
		cBuffer.la = XMVectorSet(lightAmbient[0], lightAmbient[1], lightAmbient[2], lightAmbient[3]);
		cBuffer.ld = XMVectorSet(lightDiffuse[0], lightDiffuse[1], lightDiffuse[2], lightDiffuse[3]);
		cBuffer.ls = XMVectorSet(lightSpecular[0], lightSpecular[1], lightSpecular[2], lightSpecular[3]);
		cBuffer.lightPosition = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);

		cBuffer.ka = XMVectorSet(materialAmbient[0], materialAmbient[1], materialAmbient[2], materialAmbient[3]);
		cBuffer.kd = XMVectorSet(materialDiffuse[0], materialDiffuse[1], materialDiffuse[2], materialDiffuse[3]);
		cBuffer.ks = XMVectorSet(materialSpecular[0], materialSpecular[1], materialSpecular[2], materialSpecular[3]);
		cBuffer.materialShininess = materialShininess;
		cBuffer.lightingEnabled = 1;
	}
	else
	{
		cBuffer.lightingEnabled = 0;
	}

	// c: push them into the shader
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_PP, 0, NULL, &cBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);
}

void update(void)
{
	// code
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11Buffer_Sphere_IndexBuffer)
	{
		gpID3D11Buffer_Sphere_IndexBuffer->Release();
		gpID3D11Buffer_Sphere_IndexBuffer = NULL;
	}

	if (gpID3D11Buffer_Sphere_NormalBuffer)
	{
		gpID3D11Buffer_Sphere_NormalBuffer->Release();
		gpID3D11Buffer_Sphere_NormalBuffer = NULL;
	}

	if (gpID3D11Buffer_Sphere_PositionBuffer)
	{
		gpID3D11Buffer_Sphere_PositionBuffer->Release();
		gpID3D11Buffer_Sphere_PositionBuffer = NULL;
	}

	if (gpID3D11Buffer_ConstantBuffer_PP)
	{
		gpID3D11Buffer_ConstantBuffer_PP->Release();
		gpID3D11Buffer_ConstantBuffer_PP = NULL;
	}

	if (gpID3D11Buffer_Sphere_IndexBuffer_PP)
	{
		gpID3D11Buffer_Sphere_IndexBuffer_PP->Release();
		gpID3D11Buffer_Sphere_IndexBuffer_PP = NULL;
	}

	if (gpID3D11Buffer_Sphere_NormalBuffer_PP)
	{
		gpID3D11Buffer_Sphere_NormalBuffer_PP->Release();
		gpID3D11Buffer_Sphere_NormalBuffer_PP = NULL;
	}

	if (gpID3D11Buffer_Sphere_PositionBuffer_PP)
	{
		gpID3D11Buffer_Sphere_PositionBuffer_PP->Release();
		gpID3D11Buffer_Sphere_PositionBuffer_PP = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}


	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	// code
	if (gbFullScreen == TRUE)//full sceen tashich maraychi nahi ti normal kaychi aani mg maraychi
	{
		ToggleFullScreen();
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

}

