Step for adding depth in DirectX
// Global
1) Declare DSV (Depth Stencil View) Interface globally

// Resize
2) Depth Buffer related steps
	a) Initialize texture 2D Descriptor
	b) Create a 2D texture by using above Depth Buffer
3) a) Uninitialize old depth stencil view 
   b) Initialize a depth stencil view descriptor
   c) now create depth stencil view using above depth buffer ((2) (b)) and depth stencil view descriptor ((3) (1))
4) Uninitialize local depth buffer (2 b)
5) Use this depth stencil view as 3rd parameter to OMSetTargets

// display
6) Clear the depth stencil view

// uninitialize
7) release depth stencil view

